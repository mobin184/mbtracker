﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="StyleWiseDetailsReportV2.aspx.cs" Inherits="MBTracker.Pages.Reports.StyleWiseDetailsReportV2" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <asp:UpdatePanel ID="orderUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>

            <style>
                .widget-body {
                    margin-top: -1px;
                }
                th, td {
                    text-align: center;
                     padding: 10px;
                }
                #cpMain_cblBanks td {
                    display: inline-flex;
                    margin-right: 10px;
                    min-width: 125px;
                }
                    #cpMain_cblBanks td > label {
                        display: inline !important;
                        line-height: 19px;
                        margin-left: 5px;
                    }

                #cpMain_cblBanks {
                    margin-top: 10px
                }
            </style>

            <div class="row-fluid">
            <div class="span8">
            <div class="widget">
                        <div class="widget-header">
                            <div class="title">
                                <asp:Label ID="Label7" runat="server" Text="Style Wise Details Report"></asp:Label>
                            </div>
                        </div>
                <div class="form-horizontal">
                <div class="widget-body" style="min-height: 175px;">
                    <div class="row" style="padding-left: 5px; padding-right: 5px;">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                            <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="form-horizontal">
                            
                            <div class="col-md-6">

                                <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="lblFromDate" runat="server" Text="From Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxPIFromDate" runat="server" placeholder="Enter date" Width="80%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxPIFromDate"><span style="font-weight: 700; color: #CC0000">Please enter From Date.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="Label1" runat="server" Text="To Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxPIToDate" runat="server" placeholder="Enter date" Width="80%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxPIToDate"><span style="font-weight: 700; color: #CC0000">Please enter To Date Date.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                 <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label14" runat="server" Text="Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlBuyers" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyer_SelectedIndexChanged" runat="server" Display="Dynamic" Width="80%" CssClass="form-control"></asp:DropDownList>
                                        
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label2" runat="server" Text="Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlStyles" AutoPostBack="true" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" runat="server" Display="Dynamic" Width="80%" CssClass="form-control"></asp:DropDownList>
                                        
                                    </div>
                                </div> 


                                

                               
                            </div>
                            <div class="col-md-6">

                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="lblOrderSeason" runat="server" Text="Order Season:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlSeason" runat="server" Display="Dynamic" CssClass="form-control" Width="80%"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="Label4" runat="server" Text="Order Year:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlYears" runat="server" Display="Dynamic" CssClass="form-control" Width="80%"></asp:DropDownList>
                                    </div>
                                </div>

                                
                                <div class="control-group">
                                    <label for="inputOrder" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="lblOrders" runat="server" Text="PO:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlOrders" AutoPostBack="true" runat="server" Display="Dynamic" Width="80%" CssClass="form-control"></asp:DropDownList>
                                        
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputItem" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="lblItem" runat="server" Text="Item:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlItems" AutoPostBack="true" runat="server" Display="Dynamic" Width="80%" CssClass="form-control"></asp:DropDownList>
                                        
                                    </div>
                                </div>


                                

                            </div>

                            <!--For PI End-->




                            <div class="col-md-12" style="display: flex; justify-content: center; padding-top: 15px;">
                                        <br />
                                        <asp:Button ID="btnViewReport" runat="server" class="btn btn-success btn-midium pull-left btnStyle" OnClick="btnViewReport_Click" Text="View Report" ValidationGroup="save" />
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
               </div>
            </div>
        </div>
      </div>


            <br />

            <div class="span12" style="margin-left: 0px;">
                <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="<br/>No Data found." BackColor="#ffff00"></asp:Label>
            </div>
            <div class="row-fluid" runat="server" id="divReport" visible="true">
                <div class="span12">
                    <div class="widget" style="overflow-x: scroll; overflow-y: scroll">
                        <div class="widget-body" style="border: 0px; padding: 0px;">
                            <div>
                                <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%" PageCountMode="Actual">
                               
                                    </rsweb:ReportViewer>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left: 0px;">
                    <asp:Button ID="btnExportToEXCEL" runat="server" class="btn btn-info pull-left" Visible="true" Text="Export to Excel" OnClick="btnExportToExcel_Click" />
                    <asp:Button ID="btnExportToPDF" runat="server" class="btn btn-info pull-right" Visible="true" Text="PDF" OnClick="btnExportToPdf_Click" />
                </div>
            </div>
        </ContentTemplate>
         <Triggers>
           <asp:PostBackTrigger ControlID="btnExportToEXCEL" />
             <asp:PostBackTrigger ControlID="btnExportToPDF" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
