﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class YarnStockReport : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        List<string> selectedStores = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindFactory();
            }
            else
            {
                if (Tools.HideReportViewerControl(this))
                {
                    divReport.Visible = false;
                }
            }
        }

        private void BindFactory()
        {
            var dt = unitOfWork.GetDataTableFromSql("EXEC usp_GetKnittingFactories");
            //var dt = unitOfWork.GenericRepositories<ProductionUnits>().Get(x=>x.Id > 10).ToList();
            //dt.Add(new ProductionUnits() { Id = 99, UnitName = "Jacquard" });
            //dt = dt.OrderBy(x => x.UnitName).ToList();
            ddlFactory.DataValueField = "Id"; 
            ddlFactory.DataTextField = "UnitName";
            ddlFactory.DataSource = dt;
            ddlFactory.DataBind();
        }



        protected void btnViewIssues_Click(object sender, EventArgs e)
        {
            try
            {

                if (ddlFactory.SelectedValue != "")
                {
                    if (string.IsNullOrEmpty(tbxFromDate.Text))
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select To Date.')", true);
                        return;
                    }
                    else if (string.IsNullOrEmpty(tbxToDate.Text))
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select To Date.')", true);
                        return;
                    }
                    else if (!string.IsNullOrEmpty(tbxFromDate.Text) && !string.IsNullOrEmpty(tbxToDate.Text))
                    {
                        if(Convert.ToDateTime(tbxToDate.Text).Date < Convert.ToDateTime(tbxFromDate.Text).Date)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('From date can not be greater than To date.')", true);
                            return;
                        }
                    }

                    ShowReport();

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a factory.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }


        private void ShowReport()
        {
            
            var fromDate = "";
            var ToDate = "";
            if (!string.IsNullOrEmpty(tbxFromDate.Text) && !string.IsNullOrEmpty(tbxToDate.Text))
            {
                fromDate = Convert.ToDateTime(tbxFromDate.Text).ToString("yyyy-MM-dd");
                ToDate = Convert.ToDateTime(tbxToDate.Text).ToString("yyyy-MM-dd");

                var userId = CommonMethods.SessionInfo.UserId;
                //DataTable dt = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_YarnIssueHistoryByStyleId {styleId}");
                DataTable dt = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetYarnStockReport '{fromDate}','{ToDate}','{ddlFactory.SelectedValue}'");
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/YarnStockReport.rdlc");
                rptViewer.PageCountMode = PageCountMode.Actual;

                var param = unitOfWork.GetDataTableFromSql($"SELECT '{ Convert.ToDateTime(tbxFromDate.Text).ToString("dd-MMM-yyyy")}' as FromDate,'{Convert.ToDateTime(tbxToDate.Text).ToString("dd-MMM-yyyy")}' as ToDate, '{ddlFactory.SelectedItem.Text}' as Factory");
                //ReportParameter fd = new ReportParameter("fromDate", fromDate);
                //ReportParameter td = new ReportParameter("toDate", fromDate);
                //ReportParameter factory = new ReportParameter("factory", ddlFactory.SelectedItem.Text);
                ReportDataSource ds = new ReportDataSource("dsYarnStockReport", dt);
                ReportDataSource dp = new ReportDataSource("dsParam", param);
                //rptViewer.LocalReport.SetParameters(fd);
                //rptViewer.LocalReport.SetParameters(td);
                //rptViewer.LocalReport.SetParameters(factory);
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);
                rptViewer.LocalReport.DataSources.Add(dp);
                rptViewer.LocalReport.Refresh();
                if (dt.Rows.Count > 0)
                {
                    divReport.Visible = true;
                    divReport.Attributes.Remove("class");

                    lblNoDataFound.Visible = false;
                    btnExportToEXCEL.Visible = true;
                    //rptCriteria.Visible = false;
                }
                else
                {
                    divReport.Attributes.Add("class", "hidden");
                    // divReport.Visible = false;
                    lblNoDataFound.Visible = true;
                    btnExportToEXCEL.Visible = false;

                }
            }

        }

        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            DoExport("EXCEL", "Yarn_Issue_Details");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            DoExport("PDF", "Yarn_Issue_Details");
        }
    }
}