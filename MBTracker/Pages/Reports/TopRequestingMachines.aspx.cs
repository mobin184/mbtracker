﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Reports
{
    public partial class TopRequestingMachines : System.Web.UI.Page
    {

        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
               


            }
        }

        protected void btn_showReport(object sender, EventArgs e)
        {
            try
            {
                if (tbxRptStartDate.Text != "" && tbxRptEndDate.Text != "")
                {
                    var fromDate = DateTime.Parse(tbxRptStartDate.Text);
                    var toDate = DateTime.Parse(tbxRptEndDate.Text);
                    if (fromDate > toDate)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Start Date can not be greater than End Date');", true);
                    }
                    else
                    {
                        ShowReport(fromDate, toDate);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select report start & end dates.');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }

        }


        private void ShowReport(DateTime fromDate, DateTime toDate)
        {

            var requestedFor = int.Parse(ddlRequestedFor.SelectedValue);
            var requestReason = int.Parse(ddlRequestReason.SelectedValue);

            DataTable dt = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_300TopRequestingMachines '{fromDate}','{toDate}',{requestedFor},{requestReason}");


            if (dt.Rows.Count > 0)
            {
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/TopRequestingMachines.rdlc");
                rptViewer.PageCountMode = PageCountMode.Actual;

                ReportDataSource ds = new ReportDataSource("dsTopRequestingMachines", dt);

                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);

                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
                btnExportToPDF.Visible = true;
            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
                btnExportToPDF.Visible = false;
            }

        }



        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            DoExport("EXCEL", $"Top Machines");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            DoExport("PDF", $"Top Machines");
        }

    }
}