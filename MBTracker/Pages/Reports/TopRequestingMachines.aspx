﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="TopRequestingMachines.aspx.cs" Inherits="MBTracker.Pages.Reports.TopRequestingMachines" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <asp:UpdatePanel ID="orderUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>

            <div class="row-fluid">
                <div class="span9">

                    <div class="widget">

                        <div class="widget-header">
                            <div class="title">
                                <span class="fs1" aria-hidden="true" data-icon=""></span>
                                <asp:Label runat="server" ID="lblSpareReceiveEntry" Text="Top Requesting Machines:"></asp:Label>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="row-fluid" runat="server">
                                <div class="form-horizontal">

                                    <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px;">
                                        <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field
                                    </div>

                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: right">
                                                <asp:Label ID="lblFilterDate" runat="server" Text="Report Start Date:"></asp:Label>
                                                <span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;</span>
                                            </label>
                                            <div class="controls controls-row" style="margin-left: 150px">
                                                <asp:TextBox ID="tbxRptStartDate" runat="server" placeholder="Enter date" Width="60%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxRptStartDate"><span style="font-weight: 700; color: #CC0000">Please select a date.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: right">
                                                <asp:Label ID="Label2" runat="server" Text="Report End Date:"></asp:Label>
                                                <span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;</span>
                                            </label>
                                            <div class="controls controls-row" style="margin-left: 150px">
                                                <asp:TextBox ID="tbxRptEndDate" runat="server" placeholder="Enter date" Width="60%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxRptEndDate"><span style="font-weight: 700; color: #CC0000">Please select a date.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>


                                        <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="Label1" runat="server" Text="&nbsp;"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                            <div class="controls controls-row">
                                                <br />
                                                <asp:Button ID="btnSubmit" runat="server" ValidationGroup="save" class="btn btn-info pull-right" Text="Show Report" OnClick="btn_showReport" />
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <span style="font-weight: 700; color: #CC0000">*</span>
                                                <asp:Label ID="Label3" runat="server" Text="Requested For:"></asp:Label></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlRequestedFor" runat="server" CssClass="form-control" Width="60%" Style="min-width: 125px">
                                                    <asp:ListItem Text="Needle" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Spare Parts" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                         <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <span style="font-weight: 700; color: #CC0000">*</span>
                                                <asp:Label ID="Label4" runat="server" Text="Request Reason:"></asp:Label></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlRequestReason" runat="server" CssClass="form-control" Width="60%" Style="min-width: 125px">
                                                    <asp:ListItem Text="Missing" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Broke Down" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                    </div>
                                
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>


            <div class="row-fluid">
            </div>


            <div class="span12" style="margin-left: 0px;">
                <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="<br/>No data found." BackColor="#ffff00"></asp:Label>
            </div>
            <div class="row-fluid" runat="server" id="divReport" visible="true">
                <div class="span12">
                    <div class="widget" style="border: 0px;">
                        <div class="widget-body" style="border: 0px; padding: 0px;">
                            <div style="margin-left: 0%">
                                <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%" ShowToolBar="false" SizeToReportContent="true" Style="overflow-x: auto; min-height: 210px">
                                </rsweb:ReportViewer>
                                <div class="span12" style="margin-top: 20px; margin-left: 0px;">
                                    <asp:Button ID="btnExportToEXCEL" runat="server" Visible="false" class="btn btn-info pull-left" Text="Export to Excel" OnClick="btnExportToExcel_Click" />
                                    <asp:Button ID="btnExportToPDF" runat="server" Visible="false" class="btn btn-info pull-left hidden" Text="Export to Pdf" OnClick="btnExportToPdf_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToEXCEL" />
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
