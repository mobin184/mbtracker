﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="DayWiseKnittingAndDelivery.aspx.cs" Inherits="MBTracker.Pages.Reports.DayWiseKnittingAndDelivery" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
        <style>
        #cpMain_cblProductionUnits td {
            display: inline-flex;
            margin-right: 20px;
        }

            #cpMain_cblProductionUnits td > label {
                display: inline !important;
                line-height: 19px;
                margin-left: 5px;
            }
    </style>
    <asp:UpdatePanel ID="orderUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div class="row-fluid">
                <div class="span6">
                    <div class="widget">

                        <div class="widget-header">
                            <div class="title">
                                <span class="fs1" aria-hidden="true" data-icon=""></span>
                                <asp:Label ID="lblReportPageHeading" runat="server" Text="Day Wise Knitting & Delivery Report:"></asp:Label>
                            </div>
                        </div>

                        <div class="widget-body">

                            <div class="row-fluid" runat="server" visible="true" id="FilterDiv">
                                <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                                    <%--<span style="font-weight: 700; padding-right: 400px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field--%>
                                </div>
                                <div class="control-group">
                                    <div class="col-md-12 col-sm-12">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                    </div>
                                </div>
                                <div class="col-md-10" style="padding-left: 0px">

                                    <div class="col-md-9" style="padding-left: 0px">
                                        <div class="form-horizontal">
                                            <div class="control-group">
                                                <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: right">
                                                    <asp:Label ID="lblFilterDate" runat="server" Text="From Date:"></asp:Label>
                                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                                </label>
                                                <div class="controls controls-row" style="margin-left: 160px">
                                                    <asp:TextBox ID="tbxFromDate" runat="server" placeholder="Enter date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbxFromDate"><span style="font-weight: 700; color: #CC0000">Please select a date.</span></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: right">
                                                    <asp:Label ID="Label2" runat="server" Text="To Date:"></asp:Label>
                                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                                </label>
                                                <div class="controls controls-row" style="margin-left: 160px">
                                                    <asp:TextBox ID="tbxToDate" runat="server" placeholder="Enter date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbxToDate"><span style="font-weight: 700; color: #CC0000">Please select a date.</span></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label for="inputBuyer" class="control-label">
                                                    <asp:Label ID="Label3" runat="server" Text="Finishing Unit (Optional):"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                                <div class="controls controls-row" style="width: 105%">
                                                    <%--<asp:DropDownList ID="ddlKnittingUnits" runat="server" Display="Dynamic" Width="65%" CssClass="form-control"></asp:DropDownList>--%>
                                                    <asp:CheckBoxList ID="cblProductionUnits" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="3" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <br />
                                                <label for="inputBuyerName" class="control-label" style="width: 160px; text-align: left">
                                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                                </label>
                                                <div class="contents controls-row">
                                                    <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="Show Report" CssClass="btn btn-info pull-left" OnClick="btn_showReport" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>

            <br />
            <br />

            <div class="row-fluid" runat="server" id="divReport" visible="true">
                <div class="span12">
                    <div class="widget" style="border: none">
                        <div class="widget-body" style="border: none">
                            <div style="margin-left: 0%">
                                <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%" ShowToolBar="false" SizeToReportContent="false" Style="overflow-x: auto; min-height: 210px">
                                </rsweb:ReportViewer>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left: 0px;">
                    <asp:Button ID="btnExportToEXCEL" runat="server" class="btn btn-info pull-left" Visible="false" Text="Export to Excel" OnClick="btnExportToExcel_Click" />
                    <asp:Button ID="btnExportToPDF" runat="server" class="btn btn-info pull-left hidden" Visible="false" Text="PDF" OnClick="btnExportToPdf_Click" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToEXCEL" />
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
