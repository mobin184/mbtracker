﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class MonthlyProductionPlan : Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var dtProductionMonths = unitOfWork.GetDataTableFromSql("Exec usp_GetProductionMonths");
                ddlProductionMonths.DataSource = dtProductionMonths;
                ddlProductionMonths.DataTextField = "ProductionMonth";
                ddlProductionMonths.DataValueField = "ProductionMonthId";
                ddlProductionMonths.DataBind();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlProductionMonths.SelectedValue != "")
                {
                    divReport.Visible = false;
                    var monthId = int.Parse(ddlProductionMonths.SelectedValue);
                    ShowReport(monthId);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a buyer.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }


        private void ShowReport(int productionMonthId)
        {
            DataTable dtMonthlyProductionPlan = unitOfWork.GetDataTableFromSql($"Exec usp_rpt_GetMonthlyProductionPlan {productionMonthId}");
            if(dtMonthlyProductionPlan.Rows.Count > 0)
            {
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/MonthlyProductionPlan.rdlc");
                rptViewer.PageCountMode = PageCountMode.Actual;

                ReportDataSource ds = new ReportDataSource("dsMonthlyProductionPlan", dtMonthlyProductionPlan);

                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);

                divReport.Visible = true;
                lblNoDataFound.Visible = false;
            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
            }
            
        }
    }
}