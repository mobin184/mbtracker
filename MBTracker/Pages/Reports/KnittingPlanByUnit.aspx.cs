﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class KnittingPlanByUnit : Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var dtUserUnits = new CommonManager().GetProductionUnitsByUser(Convert.ToInt32(CommonMethods.SessionInfo.UserId));
                DataRow row = dtUserUnits.NewRow();
                row["UnitName"] = "All Units";
                row["ProductionUnitId"] = 0;
                dtUserUnits.Rows.InsertAt(row, 0);
                ddlUserKnittingUnit.DataTextField = "UnitName";
                ddlUserKnittingUnit.DataValueField = "ProductionUnitId";
                ddlUserKnittingUnit.DataSource = dtUserUnits;
                ddlUserKnittingUnit.DataBind();
            }
        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbxFromDate.Text != "" && tbxToDate.Text != "")
                {
                    var fromDate = DateTime.Parse(tbxFromDate.Text);
                    var toDate = DateTime.Parse(tbxToDate.Text);
                    if(fromDate > toDate)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('From Date can not be greater than To Date');", true);
                    }
                    else
                    {
                        ShowReport(fromDate, toDate);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select from date & to date.');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }


        private void ShowReport(DateTime fromDate, DateTime toDate)
        {
            var unitId = int.Parse(ddlUserKnittingUnit.SelectedValue);
            DataTable dt = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetKnittingPlan '{fromDate.ToString("yyyy-MM-dd")}','{toDate.ToString("yyyy-MM-dd")}',{unitId},{0},{CommonMethods.SessionInfo.UserId}");
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/KnittingPlanByUnit.rdlc");
            rptViewer.PageCountMode = PageCountMode.Actual;
            var dtDates = unitOfWork.GetDataTableFromSql($"SELECT '{fromDate.ToString("dd-MMM-yyyy")}' as FromDate,'{toDate.ToString("dd-MMM-yyyy")}' as ToDate");

            ReportDataSource ds = new ReportDataSource("dsknittingPlan", dt);
            ReportDataSource ds1 = new ReportDataSource("dsFromDateAndToDate", dtDates);

            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(ds);
            rptViewer.LocalReport.DataSources.Add(ds1);
            rptViewer.LocalReport.Refresh();
            if (dt.Rows.Count > 0)
            {
                divReport.Attributes.Remove("class");               
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
            }
            else
            {
                divReport.Attributes.Add("class", "hidden");
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
            }            
        }

       
        private void DoExport(string exportType,string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
            
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            var fromDate = DateTime.Parse(tbxFromDate.Text).ToString("dd-MMM-yyyy");
            var toDate = DateTime.Parse(tbxToDate.Text).ToString("dd-MMM-yyyy");
            DoExport("EXCEL",$"knitting_Plan_By_Unit_{fromDate}_TO_{toDate}");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            var fromDate = DateTime.Parse(tbxFromDate.Text).ToString("dd-MMM-yyyy");
            var toDate = DateTime.Parse(tbxToDate.Text).ToString("dd-MMM-yyyy");
            DoExport("PDF", $"knitting_Plan_By_Unit_{fromDate}_TO_{toDate}");
        }
    }
}