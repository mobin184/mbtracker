﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Reports
{
    public partial class CurrentSpareStockStatus : System.Web.UI.Page
    {

        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_showReport(object sender, EventArgs e)
        {
            try
            {
                    if (ddlSpareTypes.SelectedValue == "")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select spare type.');", true);
                    }
                    else
                    {
                        ShowReport();
                    }
                

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }

        }


        private void ShowReport()
        {

            var selectedSpareType = int.Parse(ddlSpareTypes.SelectedValue);
          

            DataTable dt = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_KnittingSpareCurrentStocks '{selectedSpareType}'");


            if (dt.Rows.Count > 0)
            {
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/KnittingSpareCurrentStocks.rdlc");
                rptViewer.PageCountMode = PageCountMode.Actual;

                ReportDataSource ds = new ReportDataSource("dsKnittingSpareCurrentStocks", dt);

                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);

                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
                btnExportToPDF.Visible = true;
            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
                btnExportToPDF.Visible = false;
            }

        }


        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            DoExport("EXCEL", $"Current Stocks");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            DoExport("PDF", $"Current Stocks");
        }

    }
}