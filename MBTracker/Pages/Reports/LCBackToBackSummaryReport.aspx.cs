﻿using MBTracker.Code_Folder;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class LCBackToBackSummaryReport : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        List<int> selectedBanks = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindAdvisingBank();
            }
            else
            {
                if (Tools.HideReportViewerControl(this))
                {
                    divReport.Visible = false;
                }
            }
        }

        private void BindAdvisingBank()
        {
            var dt = unitOfWork.GetDataTableFromSql($"SELECT  Id, BankName FROM ApplicantBanks");
            cblBanks.DataValueField = "Id";
            cblBanks.DataTextField = "BankName";
            cblBanks.DataSource = dt;
            cblBanks.DataBind();
        }

       

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            try
            {
                ShowReport();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }


        private void ShowReport()
        {
            selectedBanks.Clear();
            for (int i = 0; i < cblBanks.Items.Count; i++)
            {
                if (cblBanks.Items[i].Selected)
                {
                    selectedBanks.Add(int.Parse(cblBanks.Items[i].Value));
                }
            }
            string bankIds = "";
            if (selectedBanks.Count != 0)
            {
                bankIds = String.Join(",", selectedBanks);
            }
            var fromDate = "";
            var ToDate = "";
            
            if(!string.IsNullOrEmpty(tbxFromDate.Text) && !string.IsNullOrEmpty(tbxToDate.Text))
            {
                fromDate = Convert.ToDateTime(tbxFromDate.Text).ToString("yyyy-MM-dd");
                ToDate = Convert.ToDateTime(tbxToDate.Text).ToString("yyyy-MM-dd");
            }
            if (!string.IsNullOrEmpty(tbxFromDate.Text) && !string.IsNullOrEmpty(tbxToDate.Text) && Convert.ToDateTime(tbxFromDate.Text) > Convert.ToDateTime(tbxToDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('From date can not be grater than To date.')", true);
            }
            else if (selectedBanks.Count() == 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a bank.')", true);
            }
            else
            {
                var userId = CommonMethods.SessionInfo.UserId;
                DataTable dt = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetBTBLCSummary '{fromDate}','{ToDate}','{bankIds}'");

                var dtParam = unitOfWork.GetDataTableFromSql($"SELECT '{Convert.ToDateTime(fromDate).ToString("dd-MMM-yyyy")}' as FromDate, '{Convert.ToDateTime(ToDate).ToString("dd-MMM-yyyy")}' as ToDate ");

                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/LCBackToBackSummaryReport.rdlc");
                rptViewer.PageCountMode = PageCountMode.Actual;
                ReportDataSource ds = new ReportDataSource("dsLCSummary", dt);               
                ReportDataSource ds1 = new ReportDataSource("dsParam", dtParam);               

                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);
                rptViewer.LocalReport.DataSources.Add(ds1);
                rptViewer.LocalReport.Refresh();
                if (dt.Rows.Count > 0)
                {
                    divReport.Visible = true;
                    divReport.Attributes.Remove("class");

                    lblNoDataFound.Visible = false;
                    btnExportToEXCEL.Visible = true;
                    //rptCriteria.Visible = false;
                }
                else
                {
                    divReport.Attributes.Add("class", "hidden");
                    // divReport.Visible = false;
                    lblNoDataFound.Visible = true;
                    btnExportToEXCEL.Visible = false;

                }
            }

        }

        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0} Report.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            DoExport("EXCEL", "Summary Of Export LC");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            DoExport("PDF", "Summary Of Export LC t");
        }

    }
}