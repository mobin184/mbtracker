﻿using MBTracker.Code_Folder;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class KnittingBalanceReport : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        List<string> selectedUnits = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                //CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
                BindKnittingUnit();
                if (Request.QueryString["styleId"] != null)
                {
                    var styleId = int.Parse(Request.QueryString["styleId"]);
                    ShowReport(styleId);
                    filterDiv.Visible = false;
                }
            }
            else
            {
                if (Tools.HideReportViewerControl(this))
                {
                    divReport.Visible = false;
                }
            }
        }

        private void BindKnittingUnit()
        {
            DataTable dt = new CommonManager().GetProductionUnitsByUser(Convert.ToInt32(CommonMethods.SessionInfo.UserId));
            cblProductionUnits.DataValueField = "ProductionUnitId";
            cblProductionUnits.DataTextField = "UnitName";
            cblProductionUnits.DataSource = dt;
            cblProductionUnits.DataBind();
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void SetColorDropdown(DropDownList ddlColor, int styleId)
        {
            var dtColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetStyleColorsByStyleId {styleId}");
            ddlColor.Items.Add(new ListItem("", ""));
            ddlColor.DataSource = dtColors;
            ddlColor.DataTextField = "ColorDescription";
            ddlColor.DataValueField = "BuyerColorId";
            ddlColor.DataBind();
            ddlColor.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlColor.SelectedIndex = 0;
        }

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < cblProductionUnits.Items.Count; i++)
                {
                    if (cblProductionUnits.Items[i].Selected)
                    {
                        selectedUnits.Add(cblProductionUnits.Items[i].Value);
                    }
                }

                if (ddlBuyers.SelectedValue != "" || ddlStyles.SelectedValue != "" || selectedUnits.Count() > 0 || !string.IsNullOrEmpty(tbxFromDate.Text) || !string.IsNullOrEmpty(tbxToDate.Text))
                {
                    if(ddlBuyers.SelectedValue != "" && ddlStyles.SelectedValue == "")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a style.')", true);
                    }
                    else
                    {
                        var styleId = 0;
                        if(ddlStyles.SelectedValue != "")
                        {
                            styleId = int.Parse(ddlStyles.SelectedValue);
                        }
                        ShowReport(styleId);
                    }                  
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select at least a search criteria.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }


        private void ShowReport(int styleId = 0)
        {
            selectedUnits.Clear();
            //var styleId = 0;
            //if(ddlStyles.SelectedValue != "")
            //{
            //    styleId = int.Parse(ddlStyles.SelectedValue);
            //}
            var colorId = 0;
            if (ddlColors.SelectedValue != "")
            {
                colorId = int.Parse(ddlColors.SelectedValue);
            }
            for (int i = 0; i < cblProductionUnits.Items.Count; i++)
            {
                if (cblProductionUnits.Items[i].Selected)
                {
                    selectedUnits.Add(cblProductionUnits.Items[i].Value);
                }
            }
            string knittingUnitId = "";
            if (selectedUnits.Count != 0)
            {
                //knittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue);
                knittingUnitId = String.Join(",", selectedUnits);
            }
            var fromDate = "";
            var ToDate = "";
            
            if(!string.IsNullOrEmpty(tbxFromDate.Text) && !string.IsNullOrEmpty(tbxToDate.Text))
            {
                fromDate = Convert.ToDateTime(tbxFromDate.Text).ToString("yyyy-MM-dd");
                ToDate = Convert.ToDateTime(tbxToDate.Text).ToString("yyyy-MM-dd");
            }
            if (!string.IsNullOrEmpty(tbxFromDate.Text) && !string.IsNullOrEmpty(tbxToDate.Text) && Convert.ToDateTime(tbxFromDate.Text) > Convert.ToDateTime(tbxToDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('From date can not be grater than To date.')", true);
            }
            else
            {
                var userId = CommonMethods.SessionInfo.UserId;
                //DataTable dt = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_YarnIssueHistoryByStyleId {styleId}");
                DataTable dt = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_KnittingBalanceReport {styleId},{colorId},'{knittingUnitId}',{userId},'{fromDate}','{ToDate}'");
               
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/KnittingBalanceReport.rdlc");
                rptViewer.PageCountMode = PageCountMode.Actual;
                ReportDataSource ds = new ReportDataSource("dsKnittingBalance", dt);               

                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);
                rptViewer.LocalReport.Refresh();
                if (dt.Rows.Count > 0)
                {
                    divReport.Visible = true;
                    divReport.Attributes.Remove("class");

                    lblNoDataFound.Visible = false;
                    btnExportToEXCEL.Visible = true;
                    //rptCriteria.Visible = false;
                }
                else
                {
                    divReport.Attributes.Add("class", "hidden");
                    // divReport.Visible = false;
                    lblNoDataFound.Visible = true;
                    btnExportToEXCEL.Visible = false;

                }
            }

        }

        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            DoExport("EXCEL", "Knitting_And_Delivery_Balance");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            DoExport("PDF", "Knitting_And_Delivery_Balance");
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlColors.Items.Clear();
            if (ddlStyles.SelectedValue != "")
            {
                SetColorDropdown(ddlColors, Convert.ToInt32(ddlStyles.SelectedValue));
            }
        }
    }
}