﻿using MBTracker.Code_Folder;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class BudgetBookingDetails : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                CommonMethods.LoadDropdown(ddlBudgetYear, "Years WHERE IsActive = 1", 1, 0);
            }
            else
            {
                if (Tools.HideReportViewerControl(this))
                {
                    divReport.Visible = false;
                }
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                divReport.Visible = false;

                if (ddlBudgetYear.SelectedValue != "" && ddlBuyers.SelectedValue != "")
                {
                    ShowReport();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select Year And Buyer')", true);
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private void ShowReport()
        {
            int BudgetYearId = 0;
            if (ddlBudgetYear.SelectedValue != "")
            {
                BudgetYearId = int.Parse(ddlBudgetYear.SelectedValue);
            }
           
            int buyerId = 0;
            if (ddlBuyers.SelectedValue != "")
            {
                buyerId = int.Parse(ddlBuyers.SelectedValue);
            }
           
            DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC Yearly_Budget_Booking_Details  {BudgetYearId}, {buyerId} ");

            if (dt.Rows.Count > 0)
            {
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.EnableHyperlinks = true;
                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/BudgetBookingDetails.rdlc");
                rptViewer.PageCountMode = PageCountMode.Actual;
                ReportDataSource ds = new ReportDataSource("YearlyBudgetBooking", dt);
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);
                rptViewer.LocalReport.Refresh();

                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
                btnExportToPDF.Visible = true;
            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
                btnExportToPDF.Visible = false;
            }

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            DoExport("EXCEL", $"YearlyBudgetBooking_Details");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            DoExport("PDF", $"YearlyBudgetBooking_Details");
        }

        private void DoExport(string exportType, string reportName)
        {
            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);

            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }


    }
}