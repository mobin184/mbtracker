﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Reports;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class PaymentOutstandingReport : System.Web.UI.Page
    {
        ReportManager reportManager = new ReportManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        List<int> selectedBanks = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
               // CommonMethods.LoadApplicantBanksDropdown(ddlApplicantBanks, 1, 0);
               // CommonMethods.LoadMerchandiserLeaderDropDown(ddlMerchandisingLeaders, 1, 0);
                CommonMethods.LoadYarnAndAccessoriesSupplierDropdown(ddlYarnAndAccssSuppliers, 1, 0);
            }
            else
            {

            }
        }

       

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlYarnAndAccssSuppliers.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select Supplier Name')", true);
                    
                }
                else if (tbxPIFromDate.Text == "" && tbxPIToDate.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select date range (From Date & To Date).')", true);
                }
                else
                {
                    ShowReport();
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private void ShowReport()
        {
            int buyerId = 0;
            if (ddlBuyers.SelectedValue != "")
            {
                buyerId = int.Parse(ddlBuyers.SelectedValue);
            }

            int supplierId = 0;
            if (ddlYarnAndAccssSuppliers.SelectedValue != "")
            {
                supplierId = int.Parse(ddlYarnAndAccssSuppliers.SelectedValue);
            }


            // int userId = CommonMethods.SessionInfo.UserId;

            DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC Sp_Payment_OutStanding_Report '{tbxPIFromDate.Text}', '{tbxPIToDate.Text}',{buyerId},{supplierId} ");
           // DataTable LCInfo = unitOfWork.GetDataTableFromSql($"Select TOP 1 LCNumber, LCDate, LCAmount from LCInfoBackToBack WHERE BeneficiaryId = {supplierId} ORDER BY Id DESC");

            if (dt.Rows.Count > 0)
            {
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.EnableHyperlinks = true;

                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/PaymentOutstandingReport.rdlc");
                rptViewer.PageCountMode = PageCountMode.Actual;

                //var dtDate = unitOfWork.GetDataTableFromSql($"SELECT '{DateTime.Now}' as Date");
                

                ReportDataSource ds = new ReportDataSource("PaymentOutStanding", dt);
                //ReportDataSource ds1 = new ReportDataSource("PaymentOutStanding", LCInfo);
                // ReportDataSource ds1 = new ReportDataSource("dsDate", dtDate);


                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);
                //rptViewer.LocalReport.DataSources.Add(ds1);
                rptViewer.LocalReport.Refresh();
                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
                //btnExportToPDF.Visible = true;

            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
               // btnExportToPDF.Visible = false;
            }

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            DoExport("EXCEL", $"PAYMENT_OUTSTANDING_REPORT");
        }

        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);

            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }

    }
}