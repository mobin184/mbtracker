﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using MBTracker.Code_Folder.Reports;
using MBTracker.Code_Folder;
using Repositories;

namespace MBTracker.Pages.Reports
{
    public partial class BookingByUnitsAndMonths : System.Web.UI.Page
    {

        ReportManager reportManager = new ReportManager();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                CommonMethods.LoadDropdown(ddlProducitonYear, "Years WHERE IsActive = 1", 1, 0); 
            }

        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlProducitonYear.SelectedValue != "")
                {
                    divReport.Visible = false;
                    var yearId = int.Parse(ddlProducitonYear.SelectedValue);
                    ShowReport(yearId);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a year.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }


        private void ShowReport(int productionYearId)
        {
            DataTable dt = reportManager.GetBookingByShipmentMonthAndUnit(productionYearId);
            if (dt.Rows.Count > 0)
            {
                DateTime reportDate = DateTime.Today.AddDays(-1);
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/BookingByUnitsAndMonths.rdlc");

                ReportDataSource datasource = new ReportDataSource("BookingByUnitAndMonth", dt);

                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(datasource);

                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
            }  
        }


        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            DoExport("EXCEL", $"Booking_By_Finishing_Unit");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            DoExport("PDF", $"Booking_By_Finishing_Unit");
        }
    }
}