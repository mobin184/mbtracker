﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class BookingByBuyerAndStyle : Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            //var flag = false;
            //int yearId = 0;
            if (!IsPostBack)
            {
                CommonMethods.LoadDropdown(ddlProducitonYear, "Years WHERE IsActive = 1", 1, 0);
                if (Request.QueryString["bookingYear"] != null)
                {
                    var bookingYear = int.Parse(Tools.UrlDecode(Request.QueryString["bookingYear"]));
                    //divReport.Visible = false;
                    var yearInfo = unitOfWork.GenericRepositories<Years>().Get(x => x.Year == bookingYear+"").FirstOrDefault();
                    ddlProducitonYear.SelectedIndex = CommonMethods.MatchDropDownItem(ddlProducitonYear, yearInfo.Id);
                    //yearId = yearInfo.Id;
                    //flag = true;
                    ShowReport(yearInfo.Id);
                }
            }

            //if (flag)
            //{
            //    ShowReport(yearId);
            //}
        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlProducitonYear.SelectedValue != "")
                {
                    //divReport.Visible = false;
                    var yearId = int.Parse(ddlProducitonYear.SelectedValue);
                    ShowReport(yearId);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a year.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }


        private void ShowReport(int productionYearId)
        {
            DataTable dtBookings = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetBookingsByBuyerAndStyle {productionYearId}," + CommonMethods.SessionInfo.UserId);
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/BookingByBuyerAndStyle.rdlc");
            rptViewer.PageCountMode = PageCountMode.Actual;

            ReportDataSource ds = new ReportDataSource("dsBookingsByBuyerAndStyle", dtBookings);

            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(ds);
            rptViewer.LocalReport.Refresh();
            if (dtBookings.Rows.Count > 0)
            {
                //divReport.Visible = true;
                divReport.Attributes.Remove("class");
               
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;

            }
            else
            {
                divReport.Attributes.Add("class", "hidden");
                // divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;

            }

        }

        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            DoExport("EXCEL","Booking_By_Buyer_And_Style");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            DoExport("PDF", "Booking_By_Buyer_And_Style");
        }
    }
}