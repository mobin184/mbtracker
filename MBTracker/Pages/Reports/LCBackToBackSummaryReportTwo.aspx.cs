﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Reports
{
    public partial class LCBackToBackSummaryReportTwo : System.Web.UI.Page
    {

        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                CommonMethods.LoadApplicantBanksDropdown(ddlApplicantBanks, 1, 0);
                CommonMethods.LoadYarnAndAccessoriesSupplierDropdown(ddlSuppliers, 1, 0);
                CommonMethods.LoadYarnAndAccessoriesBuyerDropdown(ddlItemBuyers, 1, 0);
                CommonMethods.LoadYarnAndAccessoriesItemTypeDropdown(ddlItemTypes, 1, 0);

                //CommonMethods.LoadLCBToBStatusDropdown(ddlLCStatus, 1, 0);
                CommonMethods.LoadLCTypeDropdownForRpt(ddlLCType, 1, 0);
                CommonMethods.LoadLCPurposeDropdown(ddlLCPurpose, 1, 0);

                CommonMethods.LoadMerchandiserLeaderDropDown(ddlMerchandisingLeaders, 1, 0);
                CommonMethods.LoadCommercialConceredDropDown(ddlCommercialConcerned, 1, 0);

                tbxLCToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
            else
            {
                if (Tools.HideReportViewerControl(this))
                {
                    divReport.Visible = false;
                }
            }

        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            divReport.Visible = false;
            ddlStyles.Items.Clear();
            ddlSeason.Items.Clear();

            if (ddlBuyers.SelectedValue != "")
            {

                CommonMethods.LoadDropdownById(ddlStyles, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
                CommonMethods.LoadDropdownById(ddlSeason, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerSeasons", 1, 0);
            }
        }

        protected void ddlItemTypes_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlItemTypes.SelectedValue != "")
            {
                ddlItems.Items.Clear();
                CommonMethods.LoadYarnAndAccessoriesItemDropdownByType(ddlItems, int.Parse(ddlItemTypes.SelectedValue), 1, 0);
            }
            else
            {
                ddlItems.Items.Clear();
            }

        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                divReport.Visible = false;

                if (tbxLCFromDate.Text != "" && tbxLCToDate.Text != "")
                {

                    ShowReport();

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select date range (From Date & To Date).')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }


        private void ShowReport()
        {

            int buyerId = 0;
            if (ddlBuyers.SelectedValue != "")
            {
                buyerId = int.Parse(ddlBuyers.SelectedValue);
            }

            int styleId = 0;
            if (ddlStyles.SelectedValue != "")
            {
                styleId = int.Parse(ddlStyles.SelectedValue);
            }

            int seasonId = 0;
            if (ddlSeason.SelectedValue != "")
            {
                seasonId = int.Parse(ddlSeason.SelectedValue);
            }

            int applicantBankId = 0;
            if (ddlApplicantBanks.SelectedValue != "")
            {
                applicantBankId = int.Parse(ddlApplicantBanks.SelectedValue);
            }

            int lcTypeId = 0;
            if (ddlLCType.SelectedValue != "")
            {
                lcTypeId = int.Parse(ddlLCType.SelectedValue);
            }

            int lcPurposeId = 100;
            if (ddlLCPurpose.SelectedValue != "")
            {
                lcPurposeId = int.Parse(ddlLCPurpose.SelectedValue);
            }

            int itemBuyerId = 0;
            if (ddlItemBuyers.SelectedValue != "")
            {
                itemBuyerId = int.Parse(ddlItemBuyers.SelectedValue);
            }

            int itemSupplierId = 0;
            if (ddlSuppliers.SelectedValue != "")
            {
                itemSupplierId = int.Parse(ddlSuppliers.SelectedValue);
            }

            int itemTypeId = 0;
            if (ddlItemTypes.SelectedValue != "")
            {
                itemTypeId = int.Parse(ddlItemTypes.SelectedValue);
            }

            int itemId = 0;
            if (ddlItems.SelectedValue != "")
            {
                itemId = int.Parse(ddlItems.SelectedValue);
            }

            int merchandiserLeaderId = 0;
            if (ddlMerchandisingLeaders.SelectedValue != "")
            {
                merchandiserLeaderId = int.Parse(ddlMerchandisingLeaders.SelectedValue);
            }

            int commercialConcernedId = 0;
            if (ddlCommercialConcerned.SelectedValue != "")
            {
                commercialConcernedId = int.Parse(ddlCommercialConcerned.SelectedValue);
            }

            
            int userId = CommonMethods.SessionInfo.UserId;

            DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_Rpt_GetBackToBackLCSummary   '{tbxLCFromDate.Text}', '{tbxLCToDate.Text}',{buyerId}, {styleId}, {seasonId},{applicantBankId},{lcTypeId}, {lcPurposeId},{itemBuyerId},{itemSupplierId},{itemTypeId},{itemId},{merchandiserLeaderId},{commercialConcernedId},{userId} ");


            if (dt.Rows.Count > 0)
            {
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.EnableHyperlinks = true;


                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/LCBackToBackSummaryReportTwo.rdlc");
                rptViewer.PageCountMode = PageCountMode.Actual;

                var dtDate = unitOfWork.GetDataTableFromSql($"SELECT '{DateTime.Now}' as Date");

                ReportDataSource ds = new ReportDataSource("dsLCBackToBackSummary", dt);
                ReportDataSource ds1 = new ReportDataSource("dsDate", dtDate);


                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);
                rptViewer.LocalReport.DataSources.Add(ds1);
                rptViewer.LocalReport.Refresh();

                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
                btnExportToPDF.Visible = true;

                //rptCriteria.Visible = false;
            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
                btnExportToPDF.Visible = false;
            }

        }



        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);

            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            // var productionDate = DateTime.Parse(tbxFilterDate.Text).ToString("dd-MMM-yyyy");
            DoExport("EXCEL", $"BToB_LC_Summary");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            // var productionDate = DateTime.Parse(tbxFilterDate.Text).ToString("dd-MMM-yyyy");
            DoExport("PDF", $"BToB_LC_Summary");
        }


    }
}