﻿using MBTracker.Code_Folder;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class FinishingStatus : Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);                
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
        }
        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if(ddlBuyers.SelectedValue != "")
                {
                    var buyerId = int.Parse(ddlBuyers.SelectedValue);
                    if(ddlStyles.SelectedValue != "")
                    {
                        var styleId = int.Parse(ddlStyles.SelectedValue);

                        rptViewer.ProcessingMode = ProcessingMode.Local;
                        rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/FinishingStatus.rdlc");

                        DataTable dtShipmentSummary = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetExFactorySummary {styleId}");
                        DataTable dtRemainsInFinishingSection = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetRamainsInFinishingSection {styleId}");
                        DataTable dtShipmentDetails = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetShipmentDetailsByStyleId {styleId}");
                        DataTable dtKnittedItemReceivedSummary = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetKnittedItemReceivedSummaryInLinkingSectionByStyleId {styleId}");
                        DataTable dtLinkedItemDeliveredSummary = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetLinkedItemDeliverdToFinishingSectionSummaryByStyleId {styleId}");
                        DataTable dtRemainsInLinkingSection = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetRemainingSummaryInLinkingSectionByStyleId {styleId}");
                        DataTable dtKnittedItemReceivedDetails = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetKnittedItemReceivedDetailsInLinkingSectionByStyleId {styleId}");
                        DataTable dtLinkedItemDeliveredDetails = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetLinkedItemDeliverdDetailsToFinishingSectionByStyleId {styleId}");
                        DataTable dtKnittedItemReceivedSummaryInBooth = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetKnittedItemReceivedSummaryInBoothByStyleId {styleId}");
                        DataTable dtKnittedItemDeliveredSummaryInBooth = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetKnittedItemDeliveredSummaryFromBoothByStyleId {styleId}");
                        DataTable dtRemainsInBooth = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetKnittedItemRemainsInBoothByStyleId {styleId}");

                        DataTable dtKnittedItemReceivedDetailsInBooth = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetKnittedItemReceivedDetailsInBoothByStyleId {styleId}");
                        DataTable dtKnittedItemDeliveredDetailsInBooth = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetKnittedItemDeliveredDetailsFromBoothByStyleId {styleId}");

                        DataTable dtKnittedItemDeliveredSummaryFromKnittingSection = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetKnittedItemDeliveredSummaryFromKnittingSectionByStyleId {styleId}");
                        DataTable dtKnittedItemDeliveredDetailsFromKnittingSection = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetKnittedItemDeliveredDetailsFromKnittingSectionByStyleId {styleId}");



                        ReportDataSource ds1 = new ReportDataSource("dsShipmentSummary", dtShipmentSummary);
                        ReportDataSource ds2 = new ReportDataSource("dsRemainsInFinishingSection", dtRemainsInFinishingSection);
                        ReportDataSource ds3 = new ReportDataSource("dsShipmentDetails", dtShipmentDetails);
                        ReportDataSource ds4 = new ReportDataSource("dsKnittedItemReceivedSummary", dtKnittedItemReceivedSummary);
                        ReportDataSource ds5 = new ReportDataSource("dsLinkedItemDeliveredSummary", dtLinkedItemDeliveredSummary);
                        ReportDataSource ds6 = new ReportDataSource("dsRemainsInLinkingSection", dtRemainsInLinkingSection);
                        ReportDataSource ds7 = new ReportDataSource("dsKnittedItemReceivedDetails", dtKnittedItemReceivedDetails);
                        ReportDataSource ds8 = new ReportDataSource("dsLinkedItemDeliveredDetails", dtLinkedItemDeliveredDetails);
                        ReportDataSource ds9 = new ReportDataSource("dsKnittedItemReceivedSummaryInBooth", dtKnittedItemReceivedSummaryInBooth);
                        ReportDataSource ds10 = new ReportDataSource("dsKnittedItemDeliveredSummaryInBooth", dtKnittedItemDeliveredSummaryInBooth);
                        ReportDataSource ds11 = new ReportDataSource("dsRemainsInBooth", dtRemainsInBooth);
                        ReportDataSource ds12 = new ReportDataSource("dsKnittedItemReceivedDetailsInBooth", dtKnittedItemReceivedDetailsInBooth);
                        ReportDataSource ds13 = new ReportDataSource("dsKnittedItemDeliveredDetailsInBooth", dtKnittedItemDeliveredDetailsInBooth);

                        ReportDataSource ds14 = new ReportDataSource("dsKnittedItemDeliveredSummaryFromKnittingSection", dtKnittedItemDeliveredSummaryFromKnittingSection);
                        ReportDataSource ds15 = new ReportDataSource("dsKnittedItemDeliveredDetailsFromKnittingSection", dtKnittedItemDeliveredDetailsFromKnittingSection);


                        rptViewer.LocalReport.DataSources.Clear();
                        rptViewer.LocalReport.DataSources.Add(ds1);
                        rptViewer.LocalReport.DataSources.Add(ds2);
                        rptViewer.LocalReport.DataSources.Add(ds3);
                        rptViewer.LocalReport.DataSources.Add(ds4);
                        rptViewer.LocalReport.DataSources.Add(ds5);
                        rptViewer.LocalReport.DataSources.Add(ds6);
                        rptViewer.LocalReport.DataSources.Add(ds7);
                        rptViewer.LocalReport.DataSources.Add(ds8);
                        rptViewer.LocalReport.DataSources.Add(ds9);
                        rptViewer.LocalReport.DataSources.Add(ds10);
                        rptViewer.LocalReport.DataSources.Add(ds11);
                        rptViewer.LocalReport.DataSources.Add(ds12);
                        rptViewer.LocalReport.DataSources.Add(ds13);
                        rptViewer.LocalReport.DataSources.Add(ds14);
                        rptViewer.LocalReport.DataSources.Add(ds15);
                        divReport.Visible = true;

                        //lblReportPageHeading.Text = "Knitting Report for date: " + String.Format("{0:dd-MMM-yyyy}", reportDate);

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please select a style.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please select a buyer.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }
    }
}