﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using MBTracker.Code_Folder.Reports;
using Repositories;
using MBTracker.Code_Folder;

namespace MBTracker.Pages.Reports
{
    public partial class TargetVSAchievement : System.Web.UI.Page
    {

        ReportManager reportManager = new ReportManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            divReport.Visible = false;
            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
        }
        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void btn_showReport(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(tbxFromDate.Text) && !string.IsNullOrEmpty(tbxToDate.Text))
                {

                    DateTime FromDate = Convert.ToDateTime(tbxFromDate.Text);
                    DateTime ToDate = Convert.ToDateTime(tbxToDate.Text);
                    int buyerId = 0;
                    int styleId = 0;

                    if(ddlBuyers.SelectedValue != "")
                    {
                        buyerId = int.Parse(ddlBuyers.SelectedValue);
                    }
                    if (ddlStyles.SelectedValue != "")
                    {
                        styleId = int.Parse(ddlStyles.SelectedValue);
                    }

                    if (FromDate <= ToDate)
                    {
                        var dateDiff = (ToDate - FromDate).TotalDays;
                        if (dateDiff <= 31)
                        {

                            var userId = CommonMethods.SessionInfo.UserId;
                            var dt = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetTargetVsAchievementV6 '{FromDate.ToString("yyyy-MM-dd")}','{ToDate.ToString("yyyy-MM-dd")}','{buyerId}','{styleId}','{userId}'");
                            var dtDateRange = unitOfWork.GetDataTableFromSql($"SELECT '{FromDate.ToString("dd-MMM-yyyy")}' as FromDate, '{ToDate.ToString("dd-MMM-yyyy")}' as ToDate ");
                            if (dt.Rows.Count < 1)
                            {
                                lblNoDataFound.Visible = true;
                                divReport.Attributes.Add("class", "hidden");
                                btnExportToEXCEL.Visible = false;
                                lblNoDataFound.Text = "No data found";
                            }
                            else
                            {
                                rptViewer.ProcessingMode = ProcessingMode.Local;
                                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/TargetVSAchievement.rdlc");
                                rptViewer.PageCountMode = PageCountMode.Actual;

                                //ReportDataSource datasource = new ReportDataSource("DailyKnittingData", dt);
                                ReportDataSource datasource1 = new ReportDataSource("dsTargetVsAchievementV6", dt);
                                ReportDataSource datasource2 = new ReportDataSource("dsParam", dtDateRange);

                                rptViewer.LocalReport.DataSources.Clear();
                                //rptViewer.LocalReport.DataSources.Add(datasource);
                                rptViewer.LocalReport.DataSources.Add(datasource1);
                                rptViewer.LocalReport.DataSources.Add(datasource2);
                                //rptViewer.LocalReport.Refresh();

                                lblNoDataFound.Visible = false;
                                //rptWidget.Visible = true;
                                divReport.Attributes.Remove("class");
                                btnExportToEXCEL.Visible = true;
                                divReport.Visible = true;

                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select the date range for a month.')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('From date can not be greater than to date.')", true);
                    }

                    // lblReportPageHeading.Text = "Knitting Report for date: " + String.Format("{0:dd-MMM-yyyy}", reportDate);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select from date and to date.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }


        }

        private void DoExport(string exportType, string reportName, string extension)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            //string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            //DoExport("EXCEL", $"Target VS Achievment","csv");
            DoExport("EXCELOPENXML", $"Target VS Achievment","xslx");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            DoExport("PDF", $"Target VS Achievment","");
        }


    }
}