﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class SummaryOfOrdersV2 : Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        List<string> selectedBuyers = new List<string>();
        List<string> selectedStyles = new List<string>();
        List<string> selectedColors = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindBuyerCbl();
                //CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                if (Request.QueryString["styleId"] != null)
                {
                    var styleId = int.Parse(Tools.UrlDecode(Request.QueryString["styleId"]));
                    var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(styleId);

                    ShowReport(styleId + "", "", "2001-01-01", "2100-01-01");
                    divCriteria.Visible = false;
                    //ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
                    //BindStylesByBuyer(styleInfo.BuyerId);
                    //ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleId);
                    //ShowReport(styleId);
                }
            }
        }

        private void BindBuyerCbl()
        {
            DataTable dt = new CommonManager().GetBuyersByUser(CommonMethods.SessionInfo.UserId);
            cblBuyers.DataValueField = "BuyerId";
            cblBuyers.DataTextField = "BuyerName";
            cblBuyers.DataSource = dt;
            cblBuyers.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //if (selectedBuyers.Count() > 0)
                //{
                //divReport.Visible = false;
                for (int i = 0; i < cblStyles.Items.Count; i++)
                {
                    if (cblStyles.Items[i].Selected)
                    {
                        selectedStyles.Add(cblStyles.Items[i].Value);
                    }
                }

                for (int i = 0; i < cblColors.Items.Count; i++)
                {
                    if (cblColors.Items[i].Selected)
                    {
                        selectedColors.Add(cblColors.Items[i].Value);
                    }
                }

                if (selectedStyles.Count != 0)
                {
                    if (!string.IsNullOrEmpty(tbxFromDate.Text) && !string.IsNullOrEmpty(tbxToDate.Text))
                    {
                        if (Convert.ToDateTime(tbxFromDate.Text) > Convert.ToDateTime(tbxToDate.Text))
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('From date cannot be greater than To date')", true);
                        }
                        else
                        {
                            var fromDate = Convert.ToDateTime(tbxFromDate.Text).ToString("yyyy-MM-dd");
                            var toDate = Convert.ToDateTime(tbxToDate.Text).ToString("yyyy-MM-dd");
                            var styles = string.Join(",", selectedStyles);
                            var colors = string.Join(",", selectedColors);
                            divReport.Visible = true;
                            ShowReport(styles, colors, fromDate, toDate);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter reporting date range.')", true);
                    }
                }
                else
                {
                    divReport.Visible = false;
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select styles.')", true);
                }
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select buyers.')", true);
                //}
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }


        private void ShowReport(string styleIds, string colorIds, string fromDate, string toDate)
        {

            DataTable dtStyleSummary = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_StyleSummaryReportV3 '{styleIds}','{colorIds}','{fromDate}','{toDate}'");
            var dtDateRange = unitOfWork.GetDataTableFromSql($"SELECT '{Convert.ToDateTime(fromDate).ToString("dd-MMM-yyyy")}' as FromDate, '{Convert.ToDateTime(toDate).ToString("dd-MMM-yyyy")}' as ToDate ");
            if (dtStyleSummary.Rows.Count > 0)
            {

                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.EnableHyperlinks = true;

                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/SummaryOfOrdersV4.rdlc");

                rptViewer.PageCountMode = PageCountMode.Actual;

                ReportDataSource ds = new ReportDataSource("dsStyleSummary", dtStyleSummary);
                ReportDataSource ds2 = new ReportDataSource("dsParam", dtDateRange);
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);
                rptViewer.LocalReport.DataSources.Add(ds2);
                rptViewer.LocalReport.Refresh();
                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
                //divCriteria.Visible = false;
            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
            }
        }


        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            DoExport("EXCEL", $"Ex-Factory");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            DoExport("PDF", $"Ex-Factory");
        }

        protected void btnCheckAllBuyers_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in cblBuyers.Items)
            {
                li.Selected = true;
            }
        }

        protected void btnUnCheckAllBuyers_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in cblBuyers.Items)
            {
                li.Selected = false;
            }
        }

        protected void btnShowStyles_Click(object sender, EventArgs e)
        {
            divReport.Visible = false;
            lblNoDataFound.Visible = false;
            btnExportToEXCEL.Visible = false;
            divColors.Visible = false;
            foreach (ListItem li in cblColors.Items)
            {
                li.Selected = false;
            }

            for (int i = 0; i < cblBuyers.Items.Count; i++)
            {
                if (cblBuyers.Items[i].Selected)
                {
                    selectedBuyers.Add(cblBuyers.Items[i].Value);
                }
            }
            string buyers = "";
            if (selectedBuyers.Count != 0)
            {
                buyers = string.Join(",", selectedBuyers);
                LoadStylesBuyerBuyerIds(buyers);
                btnSubmit.Visible = true;
            }
            else
            {
                styleDiv.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select buyers.')", true);
            }
        }

        private void LoadStylesBuyerBuyerIds(string buyerIds)
        {
            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetStylesByBuyers '{buyerIds}'");
            if (dt.Rows.Count > 0)
            {
                cblStyles.DataValueField = "Id";
                cblStyles.DataTextField = "StyleName";
                cblStyles.DataSource = dt;
                cblStyles.DataBind();
                styleDiv.Visible = true;
                lblNoStyle.Visible = false;
            }
            else
            {
                lblNoStyle.Visible = true;
            }
        }

        protected void btnCheckAllStyles_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in cblStyles.Items)
            {
                li.Selected = true;
            }
        }

        protected void btnUnCheckAllStyles_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in cblStyles.Items)
            {
                li.Selected = false;
            }
        }

        protected void btnShowColors_Click(object sender, EventArgs e)
        {
            divReport.Visible = false;
            for (int i = 0; i < cblStyles.Items.Count; i++)
            {
                if (cblStyles.Items[i].Selected)
                {
                    selectedStyles.Add(cblStyles.Items[i].Value);
                }
            }
            string styles = "";
            if (selectedStyles.Count != 0)
            {
                styles = string.Join(",", selectedStyles);
                LoadColorsByStyleIds(styles);
                btnSubmit.Visible = true;
            }
            else
            {
                styleDiv.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select Style.')", true);
            }
        }

        private void LoadColorsByStyleIds(string styleIds)
        {
            divColors.Visible = true;
            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetColorsByStyles '{styleIds}'");
            if (dt.Rows.Count > 0)
            {
                cblColors.DataValueField = "ColorId";
                cblColors.DataTextField = "ColorDescription";
                cblColors.DataSource = dt;
                cblColors.DataBind();
                btnCheckAllColors.Visible = true;
                btnUnCheckAllColors.Visible = true;

                lblColorsNotFound.Visible = false;
            }
            else
            {
                btnCheckAllColors.Visible = false;
                btnUnCheckAllColors.Visible = false;
                lblColorsNotFound.Visible = true;
            }
        }

        protected void btnCheckAllColors_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in cblColors.Items)
            {
                li.Selected = true;
            }
        }

        protected void btnUnCheckAllColors_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in cblColors.Items)
            {
                li.Selected = false;
            }
        }
    }
}