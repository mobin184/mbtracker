﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Reports
{
    public partial class DaillyKnittingProduction : System.Web.UI.Page
    {

        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyerName, 1, 0);
                CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
            }
                
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbxFilterDate.Text != "")
                {
                    divReport.Visible = false;
                    var productionDate = DateTime.Parse(tbxFilterDate.Text).ToString("yyyy-MM-dd");
                    ShowReport(productionDate);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a production date.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private void ShowReport(string prductionDate)
        {

            int knittingUnitId = 0;
            string BuyerId = "";
            if (ddlKnittingUnits.SelectedValue != "")
            {
                knittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue);
            }
            if (ddlBuyerName.SelectedValue != "")
            {
                //BuyerId = int.Parse(ddlBuyerName.SelectedValue);
                BuyerId = ddlBuyerName.SelectedValue.ToString();
            }
            else
            {

                // DataTable dtable = unitOfWork.GetDataTableFromSql("SELECT DISTINCT b.Id as [BuyerId] ,b.BuyerName FROM [dbo].[DailyKnittings] dk INNER JOIN [dbo].[BuyerStyles] bs ON dk.StyleId = bs.Id AND (bs.IsRunning IS NULL OR bs.IsRunning = 1) INNER JOIN [Buyers] b ON bs.BuyerId = b.Id WHERE bs.IsActive = 1 AND dk.KnittingDate = " { prductionDate.ToString("yyyy-MM-dd") } "");
                DataTable dtable = unitOfWork.GetDataTableFromSql($"SELECT DISTINCT b.Id as [BuyerId], b.BuyerName FROM [dbo].[DailyKnittings] dk INNER JOIN [dbo].[BuyerStyles] bs ON dk.StyleId = bs.Id AND (bs.IsRunning IS NULL OR bs.IsRunning = 1) INNER JOIN [Buyers] b ON bs.BuyerId = b.Id WHERE bs.IsActive = 1 AND dk.KnittingDate = '{prductionDate:yyyy-MM-dd}'");


                if (dtable.Rows.Count > 0)
                {
                    string concatenatedBuyerIds = "";
                    foreach (DataRow row in dtable.Rows)
                    {
                        if (dtable.Columns.Contains("BuyerId"))
                        {
                            string buyerId = row["BuyerId"].ToString();
                            concatenatedBuyerIds += buyerId + ",";
                        }
                    }

                    concatenatedBuyerIds = concatenatedBuyerIds.TrimEnd(',');
                    BuyerId =  concatenatedBuyerIds;
                    
                }
                else
                {
                    BuyerId = "0";
                }
            }

            string sqlQuery = "Exec usp_Rpt_DailyKnitting_Production_V3 '" + prductionDate + "', " + CommonMethods.SessionInfo.UserId + ", " + knittingUnitId + ", '" + BuyerId + "'";
            DataTable dt = unitOfWork.GetDataTableFromSql(sqlQuery);

            //DataTable dt = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_DailyKnitting_Production_V3 '{prductionDate}'," + CommonMethods.SessionInfo.UserId + $", {knittingUnitId}" + $",'{BuyerId}'");
            if (dt.Rows.Count > 0)
            {
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.EnableHyperlinks = true;
                var productionDate = DateTime.Parse(tbxFilterDate.Text);

                var orderQty = dt.Compute("Sum(ColorOrderQuantity)", "RowN = 1").ToString();
                var issuedQty = dt.Compute("Sum(ColorTotalIssueQuantity)", "RowN = 1").ToString();
                var nightMachine = dt.Compute("Sum(NightMachine)", "RowN = 1").ToString();
                var dayMachine = dt.Compute("Sum(DayMachine)", "RowN = 1").ToString();
                var previousKnittingQty = dt.Compute("Sum(PreviousKnittingQty)", "RowN = 1").ToString();
                var dayProduction = dt.Compute("Sum(DayProduction) ", "RowN = 1").ToString();
                var nightProduction = dt.Compute("Sum(NightProduction) ", "RowN = 1").ToString();
                var deliveryBalance = dt.Compute("Sum(DeliveryBalance) ", "RowN = 1").ToString();
                var dateTotal = (decimal.Parse(dayProduction) + decimal.Parse(nightProduction));               
                var grandTotal = decimal.Parse(previousKnittingQty) + dateTotal;
                var balanceQty =  grandTotal - decimal.Parse(issuedQty);
                //ReportParameter[] rptParams = new ReportParameter[]
                //{
                //    new ReportParameter("KnittingDate",DateTime.Parse(prductionDate).ToString("dd-MMM-yyyy")),
                //};
                //ReportParameter reportParameter = new ReportParameter("KnittingDate", DateTime.Parse(prductionDate).ToString("dd-MMM-yyyy"));

                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/DailyKnittingProductionV2.rdlc");
                //rptViewer.LocalReport.DataSources.Clear();
                //rptViewer.LocalReport.SetParameters(rptParams);
                rptViewer.PageCountMode = PageCountMode.Actual;
                var dtDate = unitOfWork.GetDataTableFromSql($"SELECT '{productionDate.ToString("dd-MMM-yyyy")}' as Date,'{orderQty}' as orderQty,'{issuedQty}' as issuedQty,'{nightMachine}' as nightMachine,'{dayMachine}' as dayMachine,'{previousKnittingQty}' as previousKnittingQty,'{dayProduction}' as dayProduction,'{nightProduction}' as nightProduction,'{balanceQty}' as balanceQty,'{dateTotal}' as dateTotal,'{grandTotal}' as grandTotal,{deliveryBalance} as deliveryBalance");

                ReportDataSource ds = new ReportDataSource("dsDailyKnittingProduction", dt);
                ReportDataSource ds1 = new ReportDataSource("dsDate", dtDate);

                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);
                rptViewer.LocalReport.DataSources.Add(ds1);
                rptViewer.LocalReport.Refresh();
               // rptViewer.LocalReport.SetParameters(new ReportParameter("ExecutionTimeout", "600"));

                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
                btnExportToPDF.Visible = true;

                //rptCriteria.Visible = false;

            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
                btnExportToPDF.Visible = false;
            }

        }

            private void DoExport(string exportType, string reportName)
            {

                Warning[] warnings;
                string[] streamIds;
                string contentType;
                string encoding;
                string extension;

                //Export the RDLC Report to Byte Array.
                byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);

                var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
                //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = contentType;
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
                Response.BinaryWrite(bytes);
                Response.Flush();
                Response.End();

            }

            protected void btnExportToExcel_Click(object sender, EventArgs e)
            {
                var productionDate = DateTime.Parse(tbxFilterDate.Text).ToString("dd-MMM-yyyy");
                DoExport("EXCEL", $"Knitting_And_Delivery Summary_{productionDate}");
            }

            protected void btnExportToPdf_Click(object sender, EventArgs e)
            {
                var productionDate = DateTime.Parse(tbxFilterDate.Text).ToString("dd-MMM-yyyy");
                DoExport("PDF", $"Knitting_And_Delivery Summary_{productionDate}");
            }

        }
}