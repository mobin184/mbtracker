﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Reports
{
    public partial class DetailsOfKnittedItemsByStyle : System.Web.UI.Page
    {

        UnitOfWork unitOfWork = new UnitOfWork();
        List<string> selectedUnits = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                //CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
                BindKnittingUnit();
                if (Request.QueryString["styleId"] != null)
                {
                    //var styleId = int.Parse(Request.QueryString["styleId"]);

                    var styleId = int.Parse(Tools.UrlDecode(Request.QueryString["styleId"]));

                    //var reportDate = DateTime.Parse(Request.QueryString["date"]);

                    var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(styleId);
                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
                    BindStylesByBuyer(styleInfo.BuyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleId);
                    //tbxFilterDate.Text = reportDate.ToString("yyyy-MM-dd");

                    rptCriteria.Visible = false;
                    ShowReport(styleId);
                }

            }

        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            divReport.Visible = false;
            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
        }
        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }


        private void BindKnittingUnit()
        {
            DataTable dt = new CommonManager().GetProductionUnitsByUser(Convert.ToInt32(CommonMethods.SessionInfo.UserId));
            cblProductionUnits.DataValueField = "ProductionUnitId";
            cblProductionUnits.DataTextField = "UnitName";
            cblProductionUnits.DataSource = dt;
            cblProductionUnits.DataBind();
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            divReport.Visible = false;

            if (ddlStyles.SelectedValue != "")
            {

                var dtStyleColors = new CommonManager().GetStyleColorsByStyle(int.Parse(ddlStyles.SelectedValue));
                ddlColors.DataTextField = "ColorDescription";
                ddlColors.DataValueField = "BuyerColorId";
                ddlColors.DataSource = dtStyleColors;
                ddlColors.DataBind();

                ddlColors.Items.Insert(0, new ListItem("---Select---", string.Empty));
                ddlColors.SelectedIndex = 0;

            }
        }




        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                divReport.Visible = false;
                for (int i = 0; i < cblProductionUnits.Items.Count; i++)
                {
                    if (cblProductionUnits.Items[i].Selected)
                    {
                        selectedUnits.Add(cblProductionUnits.Items[i].Value);
                    }
                }
                if (ddlBuyers.SelectedValue != "" || selectedUnits.Count() != 0 || (tbxFromDate.Text != "" && tbxToDate.Text != ""))
                {
                    if (tbxFromDate.Text != "")
                    {
                        if (tbxToDate.Text != "")
                        {
                            var fromDate = Convert.ToDateTime(tbxFromDate.Text);
                            var toDate = Convert.ToDateTime(tbxToDate.Text);
                            if (fromDate > toDate)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('To date must be grater than from date.')", true);
                                return;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter to date.')", true);
                            return;
                        }
                    }
                    int styleId = 0;
                    if (ddlStyles.SelectedValue != "")
                    {
                        styleId = int.Parse(ddlStyles.SelectedValue);
                    }
                    ShowReport(styleId);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select at least a search criteria.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }


        private void ShowReport(int styleId)
        {

            int buyerId = 0;
            if (ddlBuyers.SelectedValue != "")
            {
                buyerId = int.Parse(ddlBuyers.SelectedValue);
            }

            //int knittingUnitId = 0;
            //if (ddlKnittingUnits.SelectedValue != "")
            //{
            //    knittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue);
            //}
            string knittingUnitId = "";
            if (selectedUnits.Count != 0)
            {
                //knittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue);
                knittingUnitId = String.Join(",", selectedUnits);
            }

            int colorId = 0;
            if (ddlColors.SelectedValue != "")
            {
                colorId = int.Parse(ddlColors.SelectedValue);
            }

            //DateTime knittingDate = DateTime.Parse("2100-01-01");
            //if (tbxKnittingDate.Text != "")
            //{
            //    knittingDate = DateTime.Parse(tbxKnittingDate.Text);
            //}

            string fromDate = "";
            string toDate = "";
            if (tbxFromDate.Text != "")
            {
                fromDate = DateTime.Parse(tbxFromDate.Text).ToString("yyyy-MM-dd");
            }
            if (tbxToDate.Text != "")
            {
                toDate = DateTime.Parse(tbxToDate.Text).ToString("yyyy-MM-dd");
            }
            int userId = CommonMethods.SessionInfo.UserId;

            DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_Rpt_GetDetailsOfKnittedItemsByStyle '{buyerId}','{styleId}','{knittingUnitId}','{userId}','{colorId}','{fromDate}','{toDate}'");


            if (dt.Rows.Count > 0)
            {
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.EnableHyperlinks = true;

                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/DetailsOfKnittedItemsByStyleV2.rdlc");

                rptViewer.PageCountMode = PageCountMode.Actual;

                ReportDataSource ds = new ReportDataSource("DetailsOfKnittedItemsHistory", dt);

                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);

                rptViewer.LocalReport.Refresh();

                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
                btnExportToPDF.Visible = true;

                //rptCriteria.Visible = false;
            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
                btnExportToPDF.Visible = false;
            }

        }

        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);

            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            // var productionDate = DateTime.Parse(tbxFilterDate.Text).ToString("dd-MMM-yyyy");
            DoExport("EXCEL", $"Knitting_Details");

            //ExportOnly();
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            // var productionDate = DateTime.Parse(tbxFilterDate.Text).ToString("dd-MMM-yyyy");
            DoExport("PDF", $"Knitting_Details");
        }

        //private void ExportOnly()
        //{

        //    int knittingUnitId = 0;
        //    if (ddlKnittingUnits.SelectedValue != "")
        //    {
        //        knittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue);
        //    }

        //    int colorId = 0;
        //    if (ddlColors.SelectedValue != "")
        //    {
        //        colorId = int.Parse(ddlColors.SelectedValue);
        //    }

        //    DateTime knittingDate = DateTime.Parse("2100-01-01");
        //    if (tbxKnittingDate.Text != "")
        //    {
        //        knittingDate = DateTime.Parse(tbxKnittingDate.Text);
        //    }

        //    var styleId = int.Parse(ddlStyles.SelectedValue);
        //    int userId = CommonMethods.SessionInfo.UserId;

        //    DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_Rpt_GetDetailsOfKnittedItemsByStyle {styleId},{knittingUnitId}, {userId},{colorId},'{knittingDate}'");


        //    if (dt.Rows.Count > 0)
        //    {
        //        rptViewer.ProcessingMode = ProcessingMode.Local;
        //        rptViewer.LocalReport.EnableHyperlinks = true;

        //        var buyerName = ddlBuyers.SelectedItem.Text;
        //        var styleName = ddlStyles.SelectedItem.Text;

        //        rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/DetailsOfKnittedItemsByStyleV2.rdlc");


        //        rptViewer.PageCountMode = PageCountMode.Actual;
        //        var dtBuyerAndStyle = unitOfWork.GetDataTableFromSql($"SELECT 'Buyer: {buyerName}, Style: {styleName}' as BuyerAndStyle");

        //        ReportDataSource ds = new ReportDataSource("DetailsOfKnittedItemsHistory", dt);
        //        ReportDataSource ds1 = new ReportDataSource("BuyerAndStyle", dtBuyerAndStyle);

        //        rptViewer.LocalReport.DataSources.Clear();
        //        rptViewer.LocalReport.DataSources.Add(ds);
        //        rptViewer.LocalReport.DataSources.Add(ds1);


        //        //rptViewer.LocalReport.Refresh();

        //        //divReport.Visible = false;
        //        //lblNoDataFound.Visible = false;
        //        //btnExportToEXCEL.Visible = true;
        //        //btnExportToPDF.Visible = true;

        //        //rptCriteria.Visible = false;
        //    }
           
        //    DoExport("EXCEL", $"Knitting_History_By_Style");

        //}

    }
}