﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="FinishingStatusReport.aspx.cs" Inherits="MBTracker.Pages.Reports.FinishingStatusReport" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <style>
        .table th, .table td {
            text-align: center;
        }

        .showminus:before {
            content: '(\2212) ';
        }

        .showplus:before {
            content: '(\2b) ';
        }
    </style>

    <div class="row-fluid">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <asp:Label ID="lblReportPageHeading" runat="server" Text="Production Status By Style:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 10px">
                            <%--<span style="font-weight: 700; color: #CC0000">*</span> Required Field--%>
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select a style.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label1" runat="server" Text="&nbsp;"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                            <div class="controls controls-row">
                                <br />
                                <asp:Button ID="btnSubmit" runat="server" ValidationGroup="save" class="btn btn-info pull-left" Text="Show Report" OnClick="btnSubmit_Click" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <br />
    <br />


    <div class="row-fluid" runat="server" id="divReport" visible="false">
        <div class="span9">
            <label class="control-label showplus" onclick="showHideDiv('divFinishingSection',this)" style="cursor: pointer; font-weight: bold; font-size: 16px">Packing Status:</label>
        </div>
        <div class="span9" id="divFinishingSection" style="margin-left: 0px; display: none">
            <div class="widget">
                <div class="widget-body">
                    <label class="control-label">
                        <asp:Label ID="lbl1" runat="server" Text="Packing Summary:" Font-Bold="true"></asp:Label></label>
                    <asp:Repeater ID="rptFinishingSectionSummary" runat="server">
                        <HeaderTemplate>
                            <table id="data-table" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            <asp:Label ID="lblColor" runat="server" Text="Color Names"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="lblTotalIssue" runat="server" Text="Already Packed"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label3" runat="server" Text="Quantity Yet to Pack"></asp:Label></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="text-align: center;"><%#Eval("ColorDescription") %>
                                                 
                                </td>
                                <td style="text-align: center;">
                                    <%#Eval("AlreadyPacked") %>
                                </td>
                                <td style="text-align: center;">
                                    <%#Eval("RemainsInHand") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                                 </table>
                        </FooterTemplate>
                    </asp:Repeater>

                    <label class="control-label showplus" onclick="showHideDiv('divPackingDetails',this)" style="width: 100%; cursor: pointer; font-weight: bold;">Packing Details:</label>

                    <div id="divPackingDetails" style="display: none">
                        <asp:Repeater ID="rptPackingDetails" runat="server" OnItemDataBound="rptPackingDetails_ItemDataBound">
                            <HeaderTemplate>
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="lbl2" runat="server" Text="Order Number"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lbl22" runat="server" Text="Packing Date"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label10" runat="server" Text="Finishing Unit"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label11" runat="server" Text="Finishing Floor"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label3" runat="server" Text="Colors & Quantity"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle"><%#Eval("OrderNumber") %>
                                        <asp:Label ID="lblPackingId" runat="server" Text='<%#Eval("PackingId") %>'></asp:Label>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#string.Format("{0:dd-MMM-yyyy}",Eval("PackedDate")) %>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("UnitName") %>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("FloorName") %>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Repeater runat="server" ID="rptPackingColors" OnItemDataBound="rptPackingColors_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table" class="table table-bordered table-hover">
                                                    <thead>
                                                        <%-- <tr>
                                                            <th>
                                                                <asp:Label ID="lbl2" runat="server" Text="Colors"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="Label2" runat="server" Text="Shipment Quantity"></asp:Label></th>
                                                        </tr>--%>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center; vertical-align: middle"><%#Eval("ColorDescription") %>
                                                        <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%#Eval("BuyerColorId") %>'></asp:Label>
                                                        <asp:Label ID="lblPackingId" runat="server" Visible="false" Text='<%#Eval("PackingId") %>'></asp:Label>
                                                        <asp:Label ID="lblBuyerId" runat="server" Visible="false" Text='<%#Eval("BuyerId") %>'></asp:Label>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:GridView ID="gvSizeQuantity" Style="width: 100%" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity_RowCreated">
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                 </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                 </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>

                    <label class="control-label showplus" onclick="showHideDiv('divRemainInHandDetails',this)" style="width: 100%; cursor: pointer; font-weight: bold;">Quantity Yet to Pack Details:</label>

                    <div id="divRemainInHandDetails" style="display: none">
                        <asp:Repeater runat="server" ID="rptFinishingUnitFloors" OnItemDataBound="rptFinishingUnitFloors_ItemDataBound">
                            <HeaderTemplate>
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="lbl2" runat="server" Text="Finishing Unit"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label2" runat="server" Text="Finishing Floor"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label12" runat="server" Text="Color & Quantity"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle"><%#Eval("UnitName") %>
                                        
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("FloorName") %>                                        
                                    </td>
                                    <td>
                                        <asp:Repeater runat="server" ID="rptReaminInHandColors" OnItemDataBound="rptReaminInHandColors_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table" class="table table-bordered table-hover">
                                                    <%--<thead>
                                                        <tr>
                                                            <th>
                                                                <asp:Label ID="lbl2" runat="server" Text="Colors"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="Label2" runat="server" Text="Quantity in Hand"></asp:Label></th>
                                                        </tr>
                                                    </thead>--%>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center; vertical-align: middle"><%#Eval("ColorDescription") %>
                                                        <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%#Eval("BuyerColorId") %>'></asp:Label>
                                                        <asp:Label ID="lblStyleId" runat="server" Visible="false" Text='<%#Eval("StyleId") %>'></asp:Label>
                                                        <asp:Label ID="lblBuyerId" runat="server" Visible="false" Text='<%#Eval("BuyerId") %>'></asp:Label>
                                                        <asp:Label ID="lblFinishingUnitFloorId" runat="server" Visible="false" Text='<%#Eval("FinishingUnitFloorId") %>'></asp:Label>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:GridView ID="gvRemainInSizeQuantity" Style="width: 100%" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvRemainInSizeQuantity_RowCreated">
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                 </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                 </table>
                            </FooterTemplate>
                        </asp:Repeater>

                    </div>
                </div>
            </div>
        </div>


        <div class="span9" style="margin-left: 0px;">
            <label class="control-label showplus" onclick="showHideDiv('divLabelSection',this)" style="cursor: pointer; font-weight: bold; font-size: 16px">Labeling Status:</label>
        </div>
        <div class="span9" id="divLabelSection" style="margin-left: 0px; display: none">
            <div class="widget">
                <div class="widget-body">
                    <label class="control-label">
                        <asp:Label ID="Label19" runat="server" Text="Label Attachment Summary:" Font-Bold="true"></asp:Label></label>
                    <asp:Repeater ID="rptLabeledSummary" runat="server">
                        <HeaderTemplate>
                            <table id="data-table" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            <asp:Label ID="lblColor" runat="server" Text="Country Code"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label20" runat="server" Text="Country Name"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="lblTotalIssue" runat="server" Text="Label Attachment Quantity"></asp:Label></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="text-align: center;"><%#Eval("DeliveryCountryCode") %>
                                </td>
                                <td style="text-align: center;"><%#Eval("DeliveryCountryName") %>
                                </td>
                                <td style="text-align: center;">
                                    <%#Eval("LabeledQuantity") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                                 </table>
                        </FooterTemplate>
                    </asp:Repeater>

                    <label class="control-label showplus" onclick="showHideDiv('divLabelDetails',this)" style="width: 100%; cursor: pointer; font-weight: bold;">Label Attachment Details:</label>
                    <div id="divLabelDetails" style="display: none">
                        <asp:Repeater ID="rptLabeledDetails" runat="server" OnItemDataBound="rptLabeledDetails_ItemDataBound">
                            <HeaderTemplate>
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="lbl22" runat="server" Text="Label Attachment<br />Date"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lbl2" runat="server" Text="Order <br />Number"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label10" runat="server" Text="Finishing<br />Unit"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label11" runat="server" Text="Finishing<br />Floor"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label3" runat="server" Text="Colors, Country, Size & Quantity"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                     <td style="text-align: center; vertical-align: middle">
                                        <%#string.Format("{0:dd-MMM-yyyy}",Eval("LabelAttachmentDate")) %>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle"><%#Eval("OrderNumber") %>
                                        <asp:Label ID="lblAttachmentId" runat="server" Text='<%#Eval("AttachmentId") %>'></asp:Label>
                                    </td>                                   
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("UnitName") %>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("FloorName") %>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Repeater runat="server" ID="rptAttachmentColors" OnItemDataBound="rptAlltachmentColors_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table" class="table table-bordered table-hover">
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center; vertical-align: middle"><%#Eval("ColorDescription") %>
                                                        <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%#Eval("BuyerColorId") %>'></asp:Label>
                                                        <asp:Label ID="lblAttachmentId" runat="server" Visible="false" Text='<%#Eval("AttachmentId") %>'></asp:Label>
                                                        <asp:Label ID="lblBuyerId" runat="server" Visible="false" Text='<%#Eval("BuyerId") %>'></asp:Label>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:Repeater ID="rptDeliveryCountryAndQty" runat="server" OnItemDataBound="rptDeliveryCountryAndQty_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="data-table" class="table table-bordered table-hover">
                                                                    <tbody>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td style="vertical-align: middle">
                                                                        <asp:Label ID="lblDeliveryCountryId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryCountryId")%>'></asp:Label><asp:Label ID="lblDeliveryCountryName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryCountryCode")%>'></asp:Label></th></td>
                                                                    <td>
                                                                        <asp:GridView ID="gvSizeAndQty" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeAndQty_RowCreated"></asp:GridView>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tbody>
                                                                </table>
                                                                <div class="clearfix">
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                        <asp:Label ID="Label21" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                 </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                 </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>


        <div class="span9" style="margin-left: 0px;">
            <label class="control-label showplus" onclick="showHideDiv('divLinkingSection',this)" style="cursor: pointer; font-weight: bold; font-size: 16px">Linking Status:</label>
        </div>
        <div class="span9" id="divLinkingSection" style="margin-left: 0px; display: none">
            <div class="widget">
                <div class="widget-body">
                    <label class="control-label">
                        <asp:Label ID="Label4" runat="server" Text="Linking Summary:" Font-Bold="true"></asp:Label></label>
                    <asp:Repeater ID="rptLinkingSectionSummary" runat="server">
                        <HeaderTemplate>
                            <table id="data-table" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            <asp:Label ID="lblColor" runat="server" Text="Color Names"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label6" runat="server" Text="Items Received From Booth"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label5" runat="server" Text="Items Delivered For Finishing"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label3" runat="server" Text="Quantity with Linking"></asp:Label></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="text-align: center;"><%#Eval("ColorDescription") %>
                                                 
                                </td>
                                <td style="text-align: center;">
                                    <%#Eval("KnittedReceived") %>
                                </td>
                                <td style="text-align: center;">
                                    <%#Eval("LinkedDelivered") %>
                                </td>
                                <td style="text-align: center;">
                                    <%#Eval("RemainsInHand") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                                 </table>
                        </FooterTemplate>
                    </asp:Repeater>

                    <label class="control-label showplus" onclick="showHideDiv('divKnittedItemReceivedDetails',this)" style="cursor: pointer; font-weight: bold;">Items Received From Booth Details:</label>
                    <div id="divKnittedItemReceivedDetails" style="display: none">
                        <asp:Repeater ID="rptKnittedReceivedDetails" runat="server" OnItemDataBound="rptKnittedReceivedDetails_ItemDataBound">
                            <HeaderTemplate>
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="Label13" runat="server" Text="Finishing Unit"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label14" runat="server" Text="Finishing Floor"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lbl22" runat="server" Text="Received Date"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label3" runat="server" Text="Colors & Quantity"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("UnitName") %>                                        
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("FloorName") %>                                        
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#string.Format("{0:dd-MMM-yyyy}",Eval("ReceivedDate")) %>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Repeater runat="server" ID="rptKnittedReceivedColors" OnItemDataBound="rptKnittedReceivedColors_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table" class="table table-bordered table-hover">
                                                    <thead>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center; vertical-align: middle"><%#Eval("ColorDescription") %>
                                                        <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%#Eval("BuyerColorId") %>'></asp:Label>
                                                        <asp:Label ID="lblKnittedReceivedDate" runat="server" Visible="false" Text='<%#Eval("ReceivedDate") %>'></asp:Label>
                                                        <asp:Label ID="lblStyleId" runat="server" Visible="false" Text='<%#Eval("StyleId") %>'></asp:Label>
                                                        <asp:Label ID="lblBuyerId" runat="server" Visible="false" Text='<%#Eval("BuyerId") %>'></asp:Label>
                                                        <asp:Label runat="server" ID="lblFinishingUnitFloorId" Visible="false" Text='<%#Eval("FinishingUnitFloorId") %>'></asp:Label>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:GridView ID="gvSizeQuantityKnittedReceivedDetails" Style="width: 100%" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantityKnittedReceivedDetails_RowCreated">
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                 </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                 </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>

                    <label class="control-label showplus" onclick="showHideDiv('divLinkedItemDeliveredDetails',this)" style="cursor: pointer; font-weight: bold;">Items Delivered for Finishing Details:</label>
                    <div id="divLinkedItemDeliveredDetails" style="display: none">
                        <asp:Repeater ID="rptLinkedItemDeliveredDetails" runat="server" OnItemDataBound="rptLinkedItemDeliveredDetails_ItemDataBound">
                            <HeaderTemplate>
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="Label13" runat="server" Text="Finishing Unit"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label14" runat="server" Text="Finishing Floor"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lbl22" runat="server" Text="Delivered Date"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label3" runat="server" Text="Colors & Quantity"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("UnitName") %>                                        
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("FloorName") %>                                        
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#string.Format("{0:dd-MMM-yyyy}",Eval("DeliveredDate")) %>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Repeater runat="server" ID="rptLinkedDeliveredColors" OnItemDataBound="rptLinkedDeliveredColors_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table" class="table table-bordered table-hover">
                                                    <thead>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center; vertical-align: middle"><%#Eval("ColorDescription") %>
                                                        <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%#Eval("BuyerColorId") %>'></asp:Label>
                                                        <asp:Label ID="lblLinkedDeliveredDate" runat="server" Visible="false" Text='<%#Eval("DeliveredDate") %>'></asp:Label>
                                                        <asp:Label ID="lblStyleId" runat="server" Visible="false" Text='<%#Eval("StyleId") %>'></asp:Label>
                                                        <asp:Label ID="lblBuyerId" runat="server" Visible="false" Text='<%#Eval("BuyerId") %>'></asp:Label>
                                                        <asp:Label runat="server" ID="lblFinishingUnitFloorId" Visible="false" Text='<%#Eval("FinishingUnitFloorId") %>'></asp:Label>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:GridView ID="gvSizeQuantityLinkedDeliveredDetails" Style="width: 100%" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantityLinkedDeliveredDetails_RowCreated">
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                 </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                 </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>

                    <label class="control-label showplus" onclick="showHideDiv('divRemainInLinkingSectionDetails',this)" style="cursor: pointer; font-weight: bold;">Quantity with Linking Details:</label>
                    <div id="divRemainInLinkingSectionDetails" style="display: none">
                        <asp:Repeater runat="server" ID="rptFinishingUnitLinkingFloor" OnItemDataBound="rptFinishingUnitLinkingFloor_ItemDataBound">
                            <HeaderTemplate>
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="lbl2" runat="server" Text="Finishing Unit"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label2" runat="server" Text="Finishing Floor"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label12" runat="server" Text="Color & Quantity"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle"><%#Eval("UnitName") %>
                                        
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("FloorName") %>                                        
                                    </td>
                                    <td>
                                        <asp:Repeater runat="server" ID="rptRemainInLinkingSectionDetails" OnItemDataBound="rptRemainInLinkingSectionDetails_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table" class="table table-bordered table-hover">
                                                    <%--<thead>
                                                        <tr>
                                                            <th>
                                                                <asp:Label ID="lbl2" runat="server" Text="Colors"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="Label2" runat="server" Text="Quantity in Hand"></asp:Label></th>
                                                        </tr>
                                                    </thead>--%>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center; vertical-align: middle"><%#Eval("ColorDescription") %>
                                                        <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%#Eval("BuyerColorId") %>'></asp:Label>
                                                        <asp:Label ID="lblStyleId" runat="server" Visible="false" Text='<%#Eval("StyleId") %>'></asp:Label>
                                                        <asp:Label ID="lblBuyerId" runat="server" Visible="false" Text='<%#Eval("BuyerId") %>'></asp:Label>
                                                        <asp:Label ID="lblFinishingUnitFloorId" runat="server" Visible="false" Text='<%#Eval("FinishingUnitFloorId") %>'></asp:Label>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:GridView ID="gvRemainInLinkingSectionSizeQuantity" Style="width: 100%" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvRemainInLinkingSectionSizeQuantity_RowCreated">
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                 </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                 </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>


        <div class="span9" style="margin-left: 0px;">
            <label class="control-label showplus" onclick="showHideDiv('divFinishingBoothSection',this)" style="cursor: pointer; font-weight: bold; font-size: 16px">Booth Status:</label>
        </div>
        <div class="span9" id="divFinishingBoothSection" style="margin-left: 0px; display: none">
            <div class="widget">
                <div class="widget-body">
                    <label class="control-label">
                        <asp:Label ID="Label7" runat="server" Text="Finishing Booth Summary:" Font-Bold="true"></asp:Label></label>
                    <asp:Repeater ID="rptFinishingUnitBoothSummary" runat="server">
                        <HeaderTemplate>
                            <table id="data-table" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            <asp:Label ID="lblColor" runat="server" Text="Color Names"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label6" runat="server" Text="Knitted Items Received"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label5" runat="server" Text="Items Delivered for Linking"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label3" runat="server" Text="Quantity in Booth"></asp:Label></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="text-align: center;"><%#Eval("ColorDescription") %>
                                                 
                                </td>
                                <td style="text-align: center;">
                                    <%#Eval("KnittedReceived") %>
                                </td>
                                <td style="text-align: center;">
                                    <%#Eval("KnittedDelivered") %>
                                </td>
                                <td style="text-align: center;">
                                    <%#Eval("RemainsInHand") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                                 </table>
                        </FooterTemplate>
                    </asp:Repeater>

                    <label class="control-label showplus" onclick="showHideDiv('divKnittedItemReceivedInBoothDetails',this)" style="cursor: pointer; font-weight: bold;">Items Received in Booth Details:</label>
                    <div id="divKnittedItemReceivedInBoothDetails" style="display: none">
                        <asp:Repeater ID="rptKnittedItemReceivedInBoothDetails" runat="server" OnItemDataBound="rptKnittedItemReceivedInBoothDetails_ItemDataBound">
                            <HeaderTemplate>
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="Label13" runat="server" Text="Finishing Unit"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lbl22" runat="server" Text="Received Date"></asp:Label></th>
                                             <th>
                                                <asp:Label ID="Label24" runat="server" Text="Chalan No."></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label3" runat="server" Text="Colors & Quantity"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("UnitName") %>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#string.Format("{0:dd-MMM-yyyy}",Eval("ReceivedDate")) %>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("ChalanNo") %>
                                    <td style="text-align: center;">
                                        <asp:Repeater runat="server" ID="rptKnittedReceivedInBoothColors" OnItemDataBound="rptKnittedReceivedInBoothColors_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table" class="table table-bordered table-hover">
                                                    <thead>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center; vertical-align: middle"><%#Eval("ColorDescription") %>
                                                        <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%#Eval("BuyerColorId") %>'></asp:Label>
                                                        <asp:Label ID="lblKnittedReceivedDate" runat="server" Visible="false" Text='<%#Eval("ReceivedDate") %>'></asp:Label>
                                                        <asp:Label ID="lblStyleId" runat="server" Visible="false" Text='<%#Eval("StyleId") %>'></asp:Label>
                                                        <asp:Label ID="lblBuyerId" runat="server" Visible="false" Text='<%#Eval("BuyerId") %>'></asp:Label>
                                                        <asp:Label ID="lblFinishingUnitId" runat="server" Visible="false" Text='<%#Eval("FinishingUnitId") %>'></asp:Label>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:GridView ID="gvSizeQuantityKnittedReceivedInBoothDetails" Style="width: 100%" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantityKnittedReceivedInBoothDetails_RowCreated">
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                 </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                 </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>

                    <label class="control-label showplus" onclick="showHideDiv('divKnittedItemDeliveredToLinkingSectionDetails',this)" style="cursor: pointer; font-weight: bold">Items Delivered for Linking Details:</label>
                    <div id="divKnittedItemDeliveredToLinkingSectionDetails" style="display: none">
                        <asp:Repeater ID="rptKnittedItemDeliveredToLinkingSectionDetails" runat="server" OnItemDataBound="rptKnittedItemDeliveredToLinkingSectionDetails_ItemDataBound">
                            <HeaderTemplate>
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="Label13" runat="server" Text="Finishing Unit"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label14" runat="server" Text="Finishing Floor"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lbl22" runat="server" Text="Delivered Date"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label3" runat="server" Text="Colors & Quantity"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("UnitName") %>                                        
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("FloorName") %>                                        
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#string.Format("{0:dd-MMM-yyyy}",Eval("DeliveredDate")) %>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Repeater runat="server" ID="rptKnittedItemDeliverToLinkingSectionColors" OnItemDataBound="rptKnittedItemDeliverToLinkingSectionColors_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table" class="table table-bordered table-hover">
                                                    <thead>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center; vertical-align: middle"><%#Eval("ColorDescription") %>
                                                        <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%#Eval("BuyerColorId") %>'></asp:Label>
                                                        <asp:Label ID="lblKnittedDeliveredDate" runat="server" Visible="false" Text='<%#Eval("DeliveredDate") %>'></asp:Label>
                                                        <asp:Label ID="lblStyleId" runat="server" Visible="false" Text='<%#Eval("StyleId") %>'></asp:Label>
                                                        <asp:Label ID="lblBuyerId" runat="server" Visible="false" Text='<%#Eval("BuyerId") %>'></asp:Label>
                                                        <asp:Label runat="server" ID="lblFinishingUnitFloorId" Visible="false" Text='<%#Eval("FinishingUnitFloorId") %>'></asp:Label>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:GridView ID="gvSizeQuantityKnittedDeliveredToLinkingSectionDetails" Style="width: 100%" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantityKnittedDeliveredToLinkingSectionDetails_RowCreated">
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                 </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                 </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>

                    <label class="control-label showplus" onclick="showHideDiv('divRemainInBoothDetails',this)" style="cursor: pointer; font-weight: bold">Remains in Booth Details:</label>
                    <div id="divRemainInBoothDetails" style="display: none">
                        <asp:Repeater runat="server" ID="rptFinishingUnits" OnItemDataBound="rptFinishingUnits_ItemDataBound">
                            <HeaderTemplate>
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="lbl2" runat="server" Text="Finishing Unit"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label12" runat="server" Text="Color & Quantity"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle"><%#Eval("UnitName") %>                                        
                                    </td>
                                    <td>
                                        <asp:Repeater runat="server" ID="rptRemainInBoothDetails" OnItemDataBound="rptRemainInBoothDetails_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table" class="table table-bordered table-hover">
                                                    <%-- <thead>
                                                        <tr>
                                                            <th>
                                                                <asp:Label ID="lbl2" runat="server" Text="Colors"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="Label2" runat="server" Text="Quantity in Booth"></asp:Label></th>
                                                        </tr>
                                                    </thead>--%>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center; vertical-align: middle"><%#Eval("ColorDescription") %>
                                                        <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%#Eval("BuyerColorId") %>'></asp:Label>
                                                        <asp:Label ID="lblStyleId" runat="server" Visible="false" Text='<%#Eval("StyleId") %>'></asp:Label>
                                                        <asp:Label ID="lblBuyerId" runat="server" Visible="false" Text='<%#Eval("BuyerId") %>'></asp:Label>
                                                        <asp:Label ID="lblFinishingUnitId" runat="server" Visible="false" Text='<%#Eval("FinishingUnitId") %>'></asp:Label>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:GridView ID="gvRemainInBoothSizeQuantity" Style="width: 100%" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvRemainInBoothSizeQuantity_RowCreated">
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                 </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                 </table>
                            </FooterTemplate>
                        </asp:Repeater>



                    </div>
                </div>
            </div>
        </div>


        <div class="span9" style="margin-left: 0px;">
            <label class="control-label showplus" onclick="showHideDiv('divKnittingSection',this)" style="cursor: pointer; font-weight: bold; font-size: 16px">Knitting Status:</label>
        </div>
        <div class="span9" id="divKnittingSection" style="margin-left: 0px; display: none">
            <div class="widget">
                <div class="widget-body">
                    <label class="control-label">
                        <asp:Label ID="Label8" runat="server" Text="Knitting Section Summary:" Font-Bold="true"></asp:Label></label>
                    <asp:Repeater ID="rptKnittingSectionSummary" runat="server">
                        <HeaderTemplate>
                            <table id="data-table" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            <asp:Label ID="lblColor" runat="server" Text="Color Names"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label9" runat="server" Text="Knitted Till Today"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label5" runat="server" Text="Items Delivered to Booth"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label3" runat="server" Text="Quantity with Knitting Unit"></asp:Label></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="text-align: center;"><%#Eval("ColorDescription") %>
                                                 
                                </td>
                                <td style="text-align: center;">
                                    <%#Eval("KnittedQty") %>
                                </td>
                                <td style="text-align: center;">
                                    <%#Eval("KnittedDelivered") %>
                                </td>
                                <td style="text-align: center;">
                                    <%#Eval("RemainsInHand") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                                 </table>
                        </FooterTemplate>
                    </asp:Repeater>

                    <label class="control-label showplus" onclick="showHideDiv('divKnittedItemDeliveredToFinishingUnitDetails',this)" style="cursor: pointer; font-weight: bold">Items Delivered to Booth Details:</label>
                    <div id="divKnittedItemDeliveredToFinishingUnitDetails" style="display: none">
                        <asp:Repeater ID="rptKnittedItemDeliveredToFinishingUnitDetails" runat="server" OnItemDataBound="rptKnittedItemDeliveredToFinishingUnitDetails_ItemDataBound">
                            <HeaderTemplate>
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="Label17" runat="server" Text="Knitting Unit"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lbl23" runat="server" Text="Delivered Date"></asp:Label></th>
                                             <th>
                                                <asp:Label ID="Label24" runat="server" Text="Chalan No."></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label22" runat="server" Text="Finishing Unit"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label3" runat="server" Text="Colors & Quantity"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("UnitName") %>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#string.Format("{0:dd-MMM-yyyy}",Eval("DeliveredDate")) %>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("ChalanNo") %>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("FinishingUnit") %>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Repeater runat="server" ID="rptKnittedToFinishingUnitColors" OnItemDataBound="rptKnittedToFinishingUnitColors_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table" class="table table-bordered table-hover">
                                                    <thead>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center; vertical-align: middle"><%#Eval("ColorDescription") %>
                                                        <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%#Eval("BuyerColorId") %>'></asp:Label>
                                                        <asp:Label ID="lblKnittedDeliveredDate" runat="server" Visible="false" Text='<%#Eval("DeliveredDate") %>'></asp:Label>
                                                        <asp:Label ID="lblStyleId" runat="server" Visible="false" Text='<%#Eval("StyleId") %>'></asp:Label>
                                                        <asp:Label ID="lblBuyerId" runat="server" Visible="false" Text='<%#Eval("BuyerId") %>'></asp:Label>
                                                        <asp:Label ID="lblKnittingUnitId" runat="server" Visible="false" Text='<%#Eval("KnittingUnitId") %>'></asp:Label>

                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:GridView ID="gvSizeQuantityKnittedDeliveredToFinishingUnitDetails" Style="width: 100%" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantityKnittedDeliveredToFinishingUnitDetails_RowCreated">
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                 </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                 </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>


        <div class="span9" style="margin-left: 0px;">
            <label class="control-label showplus" onclick="showHideDiv('divYarnIssueSection',this)" style="cursor: pointer; font-weight: bold; font-size: 16px">Yarn Issue Status:</label>
        </div>
        <div class="span9" id="divYarnIssueSection" style="margin-left: 0px; display: none">
            <div class="widget">
                <div class="widget-body">
                    <label class="control-label">
                        <asp:Label ID="Label15" runat="server" Text="Yarn Issue Summary:" Font-Bold="true"></asp:Label></label>
                    <asp:Repeater ID="rptYarnIssueSummary" runat="server">
                        <HeaderTemplate>
                            <table id="data-table" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            <asp:Label ID="lblColor" runat="server" Text="Color Names"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label9" runat="server" Text="Order Quantity"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label18" runat="server" Text="Yarn Received"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label17" runat="server" Text="Yarn Issued"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label3" runat="server" Text="Quantity with Store"></asp:Label></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="text-align: center;"><%#Eval("ColorDescription") %>
                                                 
                                </td>
                                <td style="text-align: center;">
                                    <%#Eval("OrderQuantity") %>
                                </td>
                                <td style="text-align: center;">
                                    <%#Eval("YarnReceived") %>
                                </td>
                                <td style="text-align: center;">
                                    <%#Eval("YarnIssued") %>
                                </td>
                                <td style="text-align: center;">
                                    <%#Eval("RemainsInHand") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                                 </table>
                        </FooterTemplate>
                    </asp:Repeater>

                    <label class="control-label showplus" onclick="showHideDiv('divYarnIssueDetails',this)" style="cursor: pointer; font-weight: bold">Yarn Issue Details:</label>
                    <div id="divYarnIssueDetails" style="display: none">
                        <asp:Repeater ID="rptYarnIssued" runat="server" OnItemDataBound="rptYarnIssued_ItemDataBound">
                            <HeaderTemplate>
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="lbl22" runat="server" Text="Issue Date"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label16" runat="server" Text="Knitting Unit"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label17" runat="server" Text="Issued Quantity"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label3" runat="server" Text="Colors & Quantity"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#string.Format("{0:dd-MMM-yyyy}",Eval("IssueDate")) %>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <%#Eval("UnitName") %>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle"><%#Eval("IssuedQuantity") %> </td>
                                    <td style="text-align: center;">
                                        <asp:Repeater runat="server" ID="rptYarnIssuedUnitColors" OnItemDataBound="rptYarnIssuedUnitColors_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table" class="table table-bordered table-hover">
                                                    <thead>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center; vertical-align: middle"><%#Eval("ColorDescription") %>
                                                        <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%#Eval("BuyerColorId") %>'></asp:Label>
                                                        <asp:Label ID="lblYarnIssueDate" runat="server" Visible="false" Text='<%#Eval("DeliveredDate") %>'></asp:Label>
                                                        <asp:Label ID="lblStyleId" runat="server" Visible="false" Text='<%#Eval("StyleId") %>'></asp:Label>
                                                        <asp:Label ID="lblBuyerId" runat="server" Visible="false" Text='<%#Eval("BuyerId") %>'></asp:Label>
                                                        <asp:Label ID="lblKnittingUnitId" runat="server" Visible="false" Text='<%#Eval("KnittingUnitId") %>'></asp:Label>

                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:GridView ID="gvSizeQuantityYarnIssueDetails" Style="width: 100%" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantityYarnIssueDetails_RowCreated">
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                 </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                 </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
    <script>
        function showHideDiv(divId, label) {
            //$("#" + divId).toggleClass('hidden');

            if ($('#' + divId + ':visible').length)
                $('#' + divId).slideUp("slow");
            // $('#' + divId).hide("slide", { direction: "up" }, 1000);
            else
                // $('#' + divId).show("slide", { direction: "down" }, 1000);
                $('#' + divId).slideDown("slow");

            $(label).toggleClass("showminus");
            $(label).toggleClass("showplus");
        }
    </script>
</asp:Content>
