﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class SummaryOfOrderColors : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                if (Request.QueryString["styleId"] != null)
                {
                    var styleId = int.Parse(Tools.UrlDecode(Request.QueryString["styleId"]));
                    var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(styleId);
                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
                    BindStylesByBuyer(styleInfo.BuyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleId);
                    ShowReport(styleInfo.BuyerId, styleId);
                    divCriteria.Visible = false;
                }
            }
            else {
                if (Tools.HideReportViewerControl(this))
                {
                    divReport.Visible = false;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            divReport.Visible = false;
            lblNoDataFound.Visible = false;
            ddlStyles.Items.Clear();
            ddlDepartment.Items.Clear();
            ddlOrderSeason.Items.Clear();
            ddlOrderYear.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
        }
        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
            CommonMethods.LoadDropdownById(ddlDepartment, buyerId, "BuyerDepartments", 1, 0);
            CommonMethods.LoadDropdownById(ddlOrderSeason, buyerId, "BuyerSeasons", 1, 0);
            CommonMethods.LoadDropdown(ddlOrderYear, "Years WHERE IsActive = 1", 1, 0);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlBuyers.SelectedValue != "")
                {
                    divReport.Visible = false;
                    var buyerId = int.Parse(ddlBuyers.SelectedValue);
                    var styleId = 0;
                    if (ddlStyles.SelectedValue != "")
                    {
                        styleId = int.Parse(ddlStyles.SelectedValue);
                    }
                    ShowReport(buyerId, styleId);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a buyer.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }


        private void BindOrdersByStyle()
        {
            ddlOrders.Items.Clear();
            if (ddlStyles.SelectedValue != "")
            {
                CommonMethods.LoadOrderDropdownByStyle(ddlOrders, Convert.ToInt32(ddlStyles.SelectedValue), 1, 0);
            }
        }


        private void ShowReport(int buyerId, int styleId)
        {
            var colorId = 0;
            if (ddlColors.SelectedValue != "")
            {
                colorId = int.Parse(ddlColors.SelectedValue);
            }
            var orderYearId = 0;
            if (ddlOrderYear.SelectedValue != "")
            {
                orderYearId = int.Parse(ddlOrderYear.SelectedValue);
            }

            var departmentId = 0;
            if (ddlDepartment.SelectedValue != "")
            {
                departmentId = int.Parse(ddlDepartment.SelectedValue);
            }

            var seasonId = 0;
            if (ddlOrderSeason.SelectedValue != "")
            {
                seasonId = int.Parse(ddlOrderSeason.SelectedValue);
            }
            var poId = 0;
            if (ddlOrders.SelectedValue != "")
            {
                poId = int.Parse(ddlOrders.SelectedValue);
            }

            DataTable dtStyleSummary = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_StyleSummaryReport '{buyerId}','{styleId}','{colorId}','{orderYearId}','{seasonId}','{departmentId}','{poId}'");
            if (dtStyleSummary.Rows.Count > 0)
            {
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/SummaryOfOrderColorsV2.rdlc");
                rptViewer.PageCountMode = PageCountMode.Actual;

                ReportDataSource ds = new ReportDataSource("dsStyleSummary", dtStyleSummary);

                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);

                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
                //divCriteria.Visible = false;
                rptViewer.AsyncRendering = false;
            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
            }
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindColorsByStyle();
            BindOrdersByStyle();
        }


        private void BindColorsByStyle()
        {
            if (ddlStyles.SelectedValue != "")
            {
                var dtStyleColors = new CommonManager().GetStyleColorsByStyle(int.Parse(ddlStyles.SelectedValue));
                ddlColors.DataTextField = "ColorDescription";
                ddlColors.DataValueField = "BuyerColorId";
                ddlColors.DataSource = dtStyleColors;
                ddlColors.DataBind();

                ddlColors.Items.Insert(0, new ListItem("---Select---", string.Empty));
                ddlColors.SelectedIndex = 0;

            }
            else
            {
                ddlColors.Items.Clear();
            }
        }

        private void BindColorsByStyleAndPO()
        {
            ddlColors.Items.Clear();
            if (ddlStyles.SelectedValue != "" && ddlOrders.SelectedValue != "")
            {
                //var dtStyleColors = new CommonManager().GetStyleColorsByStyle(int.Parse(ddlStyles.SelectedValue));
                var dtStyleColors = unitOfWork.GetDataTableFromSql($"EXEC GetColorsByStyleAndPO '{ddlStyles.SelectedValue}','{ddlOrders.SelectedValue}'");
                ddlColors.DataTextField = "ColorDescription";
                ddlColors.DataValueField = "BuyerColorId";
                ddlColors.DataSource = dtStyleColors;
                ddlColors.DataBind();

                ddlColors.Items.Insert(0, new ListItem("---Select---", string.Empty));
                ddlColors.SelectedIndex = 0;

            }
            else if(ddlStyles.SelectedValue != "")
            {
                BindColorsByStyle();
            }
        }

        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            DoExport("EXCEL", $"Purchase_Order");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            DoExport("PDF", $"Purchase_Order");
        }

        protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindColorsByStyleAndPO();
        }
    }
}