﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class MachineBrandEfficiency : Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //CommonMethods.LoadDropdown(ddlProducitonYear, "Years WHERE IsActive = 1", 1, 0);
                //if (Request.QueryString["bookingYear"] != null)
                //{
                //    var bookingYear = int.Parse(Tools.UrlDecode(Request.QueryString["bookingYear"]));
                //    var yearInfo = unitOfWork.GenericRepositories<Years>().Get(x => x.Year == bookingYear+"").FirstOrDefault();
                //    ddlProducitonYear.SelectedIndex = CommonMethods.MatchDropDownItem(ddlProducitonYear, yearInfo.Id);
                //    ShowReport(yearInfo.Id);
                //}
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbxFilterDate.Text != "")
                {
                    divReport.Visible = false;
                    var productionDate = DateTime.Parse(tbxFilterDate.Text).ToString("yyyy-MM-dd");
                    ShowReport(productionDate);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a production date.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }


        private void ShowReport(string prductionDate)
        {
            DataTable dt = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_MachineBrandEfficiency '{prductionDate}'");
            if (dt.Rows.Count > 0)
            {
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/MachineBrandEfficiency.rdlc");
                rptViewer.PageCountMode = PageCountMode.Actual;
                var productionDate = DateTime.Parse(tbxFilterDate.Text);
                var dtDate = unitOfWork.GetDataTableFromSql($"SELECT '{productionDate.ToString("dd-MMM-yyyy")}' as Date");

                ReportDataSource ds = new ReportDataSource("dsMachineBrandEfficiency", dt);
                ReportDataSource ds1 = new ReportDataSource("dsDate", dtDate);

                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);
                rptViewer.LocalReport.DataSources.Add(ds1);

                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
                btnExportToPDF.Visible = true;
            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
                btnExportToPDF.Visible = false;
            }

        }

        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            var productionDate = DateTime.Parse(tbxFilterDate.Text).ToString("dd-MMM-yyyy");
            DoExport("EXCEL", $"Knitting_Machine_Efficiency_{productionDate}");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            var productionDate = DateTime.Parse(tbxFilterDate.Text).ToString("dd-MMM-yyyy");
            DoExport("PDF", $"Knitting_Machine_Efficiency_{productionDate}");
        }
    }
}