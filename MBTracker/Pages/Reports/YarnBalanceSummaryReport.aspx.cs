﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class YarnBalanceSummaryReport : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
            else
            {
                if (Tools.HideReportViewerControl(this))
                {
                    divReport.Visible = false;
                }
            }
        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                divReport.Visible = false;

                ShowReport();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private void ShowReport()
        {
            int buyerId = 0;
            if (ddlBuyers.SelectedValue != "")
            {
                buyerId = int.Parse(ddlBuyers.SelectedValue);
            }

            var productionDate = DateTime.Now;

            if (string.IsNullOrEmpty(tbxDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a date.')", true);
                return;
            }
            else
            {
                productionDate = Convert.ToDateTime(tbxDate.Text);
            }

            //int userId = CommonMethods.SessionInfo.UserId;
            DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetYarnBalanceSummaryReport '{buyerId}','{productionDate.ToString("yyyy-MM-dd")}'");

            if (dt.Rows.Count > 0)
            {
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.EnableHyperlinks = true;

                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/YarnBalanceSummary.rdlc");

                rptViewer.PageCountMode = PageCountMode.Actual;

                ReportDataSource ds = new ReportDataSource("dsYarnReceivedAtStore", dt);


                var dtDate = unitOfWork.GetDataTableFromSql($"SELECT '{productionDate.ToString("dd-MMM-yyyy")}' as Date");
                ReportDataSource ds1 = new ReportDataSource("dsDate", dtDate);


                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);
                rptViewer.LocalReport.DataSources.Add(ds1);


                rptViewer.LocalReport.Refresh();
                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
                btnExportToPDF.Visible = true;
                //rptCriteria.Visible = false;

            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
                btnExportToPDF.Visible = false;
            }
        }

        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);

            var filename = string.Format("{0}_Report.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            // var productionDate = DateTime.Parse(tbxFilterDate.Text).ToString("dd-MMM-yyyy");
            DoExport("EXCEL", $"Yarn_Balance_Summary");

            //ExportOnly();
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            // var productionDate = DateTime.Parse(tbxFilterDate.Text).ToString("dd-MMM-yyyy");
            DoExport("PDF", $"Yarn_Balance_Summary");
        }

        public class IssuType
        {
            public string IssueTypeId { get; set; }
            public string IssueTypeName { get; set; }
        }
    }
}