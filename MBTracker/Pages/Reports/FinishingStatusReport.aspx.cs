﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class FinishingStatusReport : Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        DataTable dtSizes;
        BuyerManager buyerManager = new BuyerManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                if (Request.QueryString["styleId"] != null)
                {
                    var styleId = int.Parse(Tools.UrlDecode(Request.QueryString["styleId"]));
                    var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(styleId);
                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
                    BindStylesByBuyer(styleInfo.BuyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleId);
                    ShowReport(styleId, styleInfo.BuyerId);
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            divReport.Visible = false;
            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
        }
        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                divReport.Visible = false;
                if (ddlBuyers.SelectedValue != "")
                {
                    var buyerId = int.Parse(ddlBuyers.SelectedValue);
                    if (ddlStyles.SelectedValue != "")
                    {
                        var styleId = int.Parse(ddlStyles.SelectedValue);
                        ShowReport(styleId, buyerId);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a style.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a buyer.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void LoadSizeInfo(int buyerId)
        {
            dtSizes = buyerManager.GetSizes(buyerId);
        }

        protected void ShowReport(int styleId, int buyerId)
        {
            var dtStyleWithBuyerColor = unitOfWork.GetDataTableFromSql($"EXEC usp_GetPackingSummaryAndRemainInHandByStyleId {styleId}");
            rptFinishingSectionSummary.DataSource = dtStyleWithBuyerColor;
            rptFinishingSectionSummary.DataBind();


            var dtPackingDetails = unitOfWork.GetDataTableFromSql($"EXEC usp_GetPackingInfoByStyleId {styleId}");
            rptPackingDetails.DataSource = dtPackingDetails;
            rptPackingDetails.DataBind();

            var dtFinishingFloors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetFinishingUnitAndFloorByStyleId {styleId}");
            rptFinishingUnitFloors.DataSource = dtFinishingFloors;
            rptFinishingUnitFloors.DataBind();

            var dtRemainInColors = unitOfWork.GetDataTableFromSql($"SELECT {buyerId} as BuyerId,{styleId} as StyleId, Id as BuyerColorId, ColorDescription FROM BuyerColors WHERE BuyerId = {buyerId} AND IsActive = 1");
            //rptReaminInHandColors.DataSource = dtRemainInColors;
            //rptReaminInHandColors.DataBind();

            var dtLinkingSectionSummary = unitOfWork.GetDataTableFromSql($"EXEC usp_GetLinkingSectionSummaryByStyleId {styleId}");
            rptLinkingSectionSummary.DataSource = dtLinkingSectionSummary;
            rptLinkingSectionSummary.DataBind();

            var dtKnittedReceivedDetails = unitOfWork.GetDataTableFromSql($"SELECT DISTINCT {buyerId} as BuyerId,{styleId} as StyleId,ls.DeliveryDate as ReceivedDate,fu.UnitName,fuf.FloorName,ls.FinishingUnitFloorId FROM KnittedItemsDeliveredToLinkingSection ls INNER JOIN FinishingUnits fu ON ls.FinishingUnitId = fu.Id INNER JOIN FinishingUnitFloors fuf ON ls.FinishingUnitFloorId = fuf.Id WHERE ls.StyleId = {styleId} AND ls.IsReceived = 1");
            rptKnittedReceivedDetails.DataSource = dtKnittedReceivedDetails;
            rptKnittedReceivedDetails.DataBind();

            var dtLinkedDeliverdDetails = unitOfWork.GetDataTableFromSql($"SELECT DISTINCT {buyerId} as BuyerId,{styleId} as StyleId,fs.DeliveryDate as DeliveredDate,fu.UnitName,fuf.FloorName,fs.FinishingUnitFloorId FROM LinkedItemsDeliveredToFinishingSection fs INNER JOIN FinishingUnits fu ON fs.FinishingUnitId = fu.Id INNER JOIN FinishingUnitFloors fuf ON fs.FinishingUnitFloorId = fuf.Id WHERE fs.StyleId = {styleId} AND fs.IsReceived = 1");
            rptLinkedItemDeliveredDetails.DataSource = dtLinkedDeliverdDetails;
            rptLinkedItemDeliveredDetails.DataBind();


            rptFinishingUnitLinkingFloor.DataSource = dtFinishingFloors;
            rptFinishingUnitLinkingFloor.DataBind();

            //rptRemainInLinkingSectionDetails.DataSource = dtRemainInColors;
            //rptRemainInLinkingSectionDetails.DataBind();

            var dtBoothSummary = unitOfWork.GetDataTableFromSql($"EXEC usp_GetFinishingUnitBoothSummaryByStyleId {styleId}");
            rptFinishingUnitBoothSummary.DataSource = dtBoothSummary;
            rptFinishingUnitBoothSummary.DataBind();

           // var dtKnittedReceivedInBoothDetails = unitOfWork.GetDataTableFromSql($"SELECT DISTINCT {buyerId} as BuyerId,{styleId} as StyleId,ls.ReceivedDate, ls.FinishingUnitId,fs.UnitName  FROM DailyKnittingReceive ls INNER JOIN FinishingUnits fs ON ls.FinishingUnitId = fs.Id WHERE ls.StyleId = {styleId}");
            var dtKnittedReceivedInBoothDetails = unitOfWork.GetDataTableFromSql($"EXEC usp_GetBoothReceivedEvents {buyerId},{styleId}");

            rptKnittedItemReceivedInBoothDetails.DataSource = dtKnittedReceivedInBoothDetails;
            rptKnittedItemReceivedInBoothDetails.DataBind();

            var dtKnittedDeliverdToLinkingSectionDetails = unitOfWork.GetDataTableFromSql($"SELECT DISTINCT {buyerId} as BuyerId,{styleId} as StyleId,ls.DeliveryDate as DeliveredDate,fu.UnitName,fuf.FloorName,ls.FinishingUnitFloorId FROM KnittedItemsDeliveredToLinkingSection ls INNER JOIN FinishingUnits fu ON ls.FinishingUnitId = fu.Id INNER JOIN FinishingUnitFloors fuf ON ls.FinishingUnitFloorId= fuf.Id WHERE ls.StyleId = {styleId} AND ls.IsReceived = 1");
            rptKnittedItemDeliveredToLinkingSectionDetails.DataSource = dtKnittedDeliverdToLinkingSectionDetails;
            rptKnittedItemDeliveredToLinkingSectionDetails.DataBind();

            var dtFinishingUnit = unitOfWork.GetDataTableFromSql($"SELECT DISTINCT kr.FinishingUnitId,fu.UnitName FROM DailyKnittingReceive kr INNER JOIN FinishingUnits fu ON kr.FinishingUnitId = fu.Id WHERE kr.StyleId = {styleId}");
            rptFinishingUnits.DataSource = dtFinishingUnit;
            rptFinishingUnits.DataBind();

            //rptRemainInBoothDetails.DataSource = dtRemainInColors;
            //rptRemainInBoothDetails.DataBind();


            var dtKnittingSectionSummary = unitOfWork.GetDataTableFromSql($"EXEC usp_GetKnittingSectionSummaryByStyleId {styleId}");
            rptKnittingSectionSummary.DataSource = dtKnittingSectionSummary;
            rptKnittingSectionSummary.DataBind();

           // var dtKnittedDeliverdFinishingUnitDetails = unitOfWork.GetDataTableFromSql($"SELECT DISTINCT {buyerId} as BuyerId,{styleId} as StyleId,ls.DeliveredDate,ls.KnittingUnitId,pu.UnitName FROM KnittedItemDelvierdToFinishingUnit ls INNER JOIN ProductionUnits pu ON ls.KnittingUnitId = pu.Id  WHERE ls.StyleId = {styleId} ");

            var dtKnittedDeliverdFinishingUnitDetails = unitOfWork.GetDataTableFromSql($"EXEC usp_GetKnittingsDeliveredEvents {buyerId},{styleId}");
            rptKnittedItemDeliveredToFinishingUnitDetails.DataSource = dtKnittedDeliverdFinishingUnitDetails;
            rptKnittedItemDeliveredToFinishingUnitDetails.DataBind();

            var dtYarnIssueSummary = unitOfWork.GetDataTableFromSql($"EXEC usp_Rpt_GetYarnIssueSummaryByStyleId {styleId}");
            rptYarnIssueSummary.DataSource = dtYarnIssueSummary;
            rptYarnIssueSummary.DataBind();

            var dtYarnIssueDetails = unitOfWork.GetDataTableFromSql($"EXEC usp_Rpt_GetYarnIssueDetailsByStyle {styleId}");
            rptYarnIssued.DataSource = dtYarnIssueDetails;
            rptYarnIssued.DataBind();

            var dtLabeledSummary = unitOfWork.GetDataTableFromSql($"EXEC usp_GetCountryWiseLabledSummary {styleId}");
            rptLabeledSummary.DataSource = dtLabeledSummary;
            rptLabeledSummary.DataBind();

            var dtLabelAttachmentDetails = unitOfWork.GetDataTableFromSql($"EXEC usp_GetLabelAttachmetnInfoByStyleId {styleId}");
            rptLabeledDetails.DataSource = dtLabelAttachmentDetails;
            rptLabeledDetails.DataBind();

            divReport.Visible = true;


        }

        protected void rptPackingDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptPackingColors = (Repeater)e.Item.FindControl("rptPackingColors");
                var packingId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "PackingId").ToString());
                var buyerId = int.Parse(ddlBuyers.SelectedValue);

                // var dtBuyerColors = unitOfWork.GetDataTableFromSql($"SELECT {packingId} as PackingId,{buyerId} as BuyerId, Id as BuyerColorId, ColorDescription FROM BuyerColors WHERE BuyerId = {buyerId} AND IsActive = 1");

                var dtBuyerColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetBuyerColorsForPackedItems {packingId},{buyerId}");

                rptPackingColors.DataSource = dtBuyerColors;
                rptPackingColors.DataBind();
            }
        }

        protected void rptPackingColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gvSizeQuantity = (GridView)e.Item.FindControl("gvSizeQuantity");
                var buyerId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BuyerId").ToString());
                LoadSizeInfo(buyerId);
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantity.Columns.Add(templateField);
                    }
                }

                gvSizeQuantity.DataSource = dtOneRow;
                gvSizeQuantity.DataBind();

            }
        }

        protected void gvSizeQuantity_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblPackingId = (Label)((GridView)sender).DataItemContainer.FindControl("lblPackingId");
                var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                var lblBuyerId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId");

                var buyerId = int.Parse(lblBuyerId.Text);
                var buyerColorId = int.Parse(lblBuyerColorId.Text);
                var packingId = int.Parse(lblPackingId.Text);
                if (dtSizes == null || dtSizes.Rows.Count == 0)
                {
                    LoadSizeInfo(buyerId);
                }

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());
                    var sizeInfo = unitOfWork.GenericRepositories<PackedItemsColorSizes>().Get(x => x.PackedId == packingId && x.BuyerColorId == buyerColorId && x.SizeId == sizeId).FirstOrDefault();
                    if (sizeInfo != null)
                    {
                        var lblQty = new Label();
                        lblQty.Text = sizeInfo.PackedQty + "";
                        e.Row.Cells[i].Controls.Add(lblQty);
                    }

                }
            }
        }

        protected void rptReaminInHandColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gvRemainInSizeQuantity = (GridView)e.Item.FindControl("gvRemainInSizeQuantity");
                var buyerId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BuyerId").ToString());
                LoadSizeInfo(buyerId);
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvRemainInSizeQuantity.Columns.Add(templateField);
                    }
                }

                gvRemainInSizeQuantity.DataSource = dtOneRow;
                gvRemainInSizeQuantity.DataBind();
            }
        }


        protected void gvRemainInSizeQuantity_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblStyleId = (Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId");
                var lblBuyerId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId");
                var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                var lblFinishingUnitFloorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblFinishingUnitFloorId");

                var styleId = int.Parse(lblStyleId.Text);
                var buyerId = int.Parse(lblBuyerId.Text);
                var buyerColorId = int.Parse(lblBuyerColorId.Text);
                var floorId = int.Parse(lblFinishingUnitFloorId.Text);
                if (dtSizes == null || dtSizes.Rows.Count == 0)
                {
                    LoadSizeInfo(buyerId);
                }

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    var sizeId = dtSizes.Rows[i][0].ToString();
                    var sizeInfo = unitOfWork.GetSingleValue($"EXEC usp_GetRemainsQtyByStyleColorAndSizeId {styleId},{buyerColorId},{sizeId},{floorId}");
                    if (sizeInfo != "0")
                    {
                        var lblQty = new Label();
                        lblQty.Text = sizeInfo;
                        e.Row.Cells[i].Controls.Add(lblQty);
                    }
                }
            }
        }

        protected void rptKnittedReceivedDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptKnittedReceivedColors = (Repeater)e.Item.FindControl("rptKnittedReceivedColors");
                //var receivedDate = DataBinder.Eval(e.Item.DataItem, "ReceivedDate").ToString();
                DateTime receivedDate = DateTime.Parse(DataBinder.Eval(e.Item.DataItem, "ReceivedDate").ToString());
                string datePart = "'" + receivedDate.ToString("yyyy-MM-dd") + "'";

                var finishingUnitFloorId = DataBinder.Eval(e.Item.DataItem, "FinishingUnitFloorId").ToString();
                var styleId = int.Parse(DataBinder.Eval(e.Item.DataItem, "StyleId").ToString());
                var buyerId = int.Parse(ddlBuyers.SelectedValue);

                // var dtBuyerColors = unitOfWork.GetDataTableFromSql($"SELECT {finishingUnitFloorId} as FinishingUnitFloorId,'{receivedDate}' as ReceivedDate,{buyerId} as BuyerId, {styleId} as StyleId, Id as BuyerColorId, ColorDescription FROM BuyerColors WHERE BuyerId = {buyerId} AND IsActive = 1");

                var dtBuyerColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetBuyerColorsForItemReceivedFromBooth {finishingUnitFloorId},{datePart},{buyerId},{styleId}");

                rptKnittedReceivedColors.DataSource = dtBuyerColors;
                rptKnittedReceivedColors.DataBind();
            }
        }

        protected void gvSizeQuantityKnittedReceivedDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblStyleId = (Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId");
                var lblBuyerId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId");
                var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                var lblKnittedReceivedDate = (Label)((GridView)sender).DataItemContainer.FindControl("lblKnittedReceivedDate");
                var lblFinishingUnitFloorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblFinishingUnitFloorId");

                var styleId = int.Parse(lblStyleId.Text);
                var buyerId = int.Parse(lblBuyerId.Text);
                var buyerColorId = int.Parse(lblBuyerColorId.Text);
                var receivedDate = DateTime.Parse(lblKnittedReceivedDate.Text);
                var floorId = int.Parse(lblFinishingUnitFloorId.Text);
                if (dtSizes == null || dtSizes.Rows.Count == 0)
                {
                    LoadSizeInfo(buyerId);
                }

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    var sizeId = dtSizes.Rows[i][0].ToString();
                    var sizeInfo = unitOfWork.GetSingleValue($"EXEC usp_GetKnittedReceivedInLinkingSectionByStyleIdReceivedDateAndColorId {styleId},'{receivedDate}',{buyerColorId}, {sizeId},{floorId}");
                    if (sizeInfo != "0")
                    {
                        var lblQty = new Label();
                        lblQty.Text = sizeInfo;
                        e.Row.Cells[i].Controls.Add(lblQty);
                    }
                }
            }
        }

        protected void rptKnittedReceivedColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gvSizeQuantityKnittedReceivedDetails = (GridView)e.Item.FindControl("gvSizeQuantityKnittedReceivedDetails");
                var buyerId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BuyerId").ToString());
                LoadSizeInfo(buyerId);
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantityKnittedReceivedDetails.Columns.Add(templateField);
                    }
                }

                gvSizeQuantityKnittedReceivedDetails.DataSource = dtOneRow;
                gvSizeQuantityKnittedReceivedDetails.DataBind();
            }
        }

        protected void rptLinkedItemDeliveredDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptLinkedDeliveredColors = (Repeater)e.Item.FindControl("rptLinkedDeliveredColors");
                //var deliveredDate = DataBinder.Eval(e.Item.DataItem, "DeliveredDate").ToString();

                DateTime deliveredDate = DateTime.Parse(DataBinder.Eval(e.Item.DataItem, "DeliveredDate").ToString());
                string datePart = "'" + deliveredDate.ToString("yyyy-MM-dd") + "'";


                var finishingUnitFloorId = int.Parse(DataBinder.Eval(e.Item.DataItem, "FinishingUnitFloorId").ToString());
                var styleId = int.Parse(DataBinder.Eval(e.Item.DataItem, "StyleId").ToString());
                var buyerId = int.Parse(ddlBuyers.SelectedValue);

                // var dtBuyerColors = unitOfWork.GetDataTableFromSql($"SELECT {finishingUnitFloorId} as FinishingUnitFloorId,'{deliveredDate}' as DeliveredDate,{buyerId} as BuyerId, {styleId} as StyleId, Id as BuyerColorId, ColorDescription FROM BuyerColors WHERE BuyerId = {buyerId} AND IsActive = 1");

                var dtBuyerColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetBuyerColorsForLinkedItemsDelivered {finishingUnitFloorId},{datePart},{buyerId},{styleId}");


                rptLinkedDeliveredColors.DataSource = dtBuyerColors;
                rptLinkedDeliveredColors.DataBind();
            }
        }

        protected void rptLinkedDeliveredColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gvSizeQuantityLinkedDeliveredDetails = (GridView)e.Item.FindControl("gvSizeQuantityLinkedDeliveredDetails");
                var buyerId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BuyerId").ToString());
                LoadSizeInfo(buyerId);
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantityLinkedDeliveredDetails.Columns.Add(templateField);
                    }
                }

                gvSizeQuantityLinkedDeliveredDetails.DataSource = dtOneRow;
                gvSizeQuantityLinkedDeliveredDetails.DataBind();
            }
        }

        protected void gvSizeQuantityLinkedDeliveredDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblStyleId = (Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId");
                var lblBuyerId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId");
                var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                var lblLinkedDeliveredDate = (Label)((GridView)sender).DataItemContainer.FindControl("lblLinkedDeliveredDate");
                var lblFinishingUnitFloorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblFinishingUnitFloorId");

                var styleId = int.Parse(lblStyleId.Text);
                var buyerId = int.Parse(lblBuyerId.Text);
                var buyerColorId = int.Parse(lblBuyerColorId.Text);
                var deliveredDate = DateTime.Parse(lblLinkedDeliveredDate.Text);
                var floorId = int.Parse(lblFinishingUnitFloorId.Text);
                if (dtSizes == null || dtSizes.Rows.Count == 0)
                {
                    LoadSizeInfo(buyerId);
                }

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    var sizeId = dtSizes.Rows[i][0].ToString();
                    var sizeInfo = unitOfWork.GetSingleValue($"EXEC usp_GetLinkedDeliveredToFinishingSectionByStyleIdDeliveredDateAndColorId {styleId},'{deliveredDate}',{buyerColorId}, {sizeId},{floorId}");
                    if (sizeInfo != "0")
                    {
                        var lblQty = new Label();
                        lblQty.Text = sizeInfo;
                        e.Row.Cells[i].Controls.Add(lblQty);
                    }
                }
            }
        }

        protected void rptRemainInLinkingSectionDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gvRemainInLinkingSectionSizeQuantity = (GridView)e.Item.FindControl("gvRemainInLinkingSectionSizeQuantity");
                var buyerId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BuyerId").ToString());
                LoadSizeInfo(buyerId);
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvRemainInLinkingSectionSizeQuantity.Columns.Add(templateField);
                    }
                }

                gvRemainInLinkingSectionSizeQuantity.DataSource = dtOneRow;
                gvRemainInLinkingSectionSizeQuantity.DataBind();
            }
        }

        protected void gvRemainInLinkingSectionSizeQuantity_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblStyleId = (Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId");
                var lblBuyerId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId");
                var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                var lblFinishingUnitFloorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblFinishingUnitFloorId");

                var styleId = int.Parse(lblStyleId.Text);
                var buyerId = int.Parse(lblBuyerId.Text);
                var buyerColorId = int.Parse(lblBuyerColorId.Text);
                var floorId = int.Parse(lblFinishingUnitFloorId.Text);
                if (dtSizes == null || dtSizes.Rows.Count == 0)
                {
                    LoadSizeInfo(buyerId);
                }

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    var sizeId = dtSizes.Rows[i][0].ToString();
                    var sizeInfo = unitOfWork.GetSingleValue($"EXEC usp_GetRemainsQtyInLinkingSectionByStyleColorAndSizeId {styleId},{buyerColorId},{sizeId},{floorId}");
                    if (sizeInfo != "0")
                    {
                        var lblQty = new Label();
                        lblQty.Text = sizeInfo;
                        e.Row.Cells[i].Controls.Add(lblQty);
                    }
                }
            }
        }

        protected void rptKnittedItemReceivedInBoothDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptKnittedReceivedInBoothColors = (Repeater)e.Item.FindControl("rptKnittedReceivedInBoothColors");
               // var receivedDate = DataBinder.Eval(e.Item.DataItem, "ReceivedDate").ToString();

                DateTime receivedDate = DateTime.Parse(DataBinder.Eval(e.Item.DataItem, "ReceivedDate").ToString());
                string datePart = "'" + receivedDate.ToString("yyyy-MM-dd") + "'";

                var finishingUnitId = int.Parse(DataBinder.Eval(e.Item.DataItem, "FinishingUnitId").ToString());
                var styleId = int.Parse(DataBinder.Eval(e.Item.DataItem, "StyleId").ToString());
                var buyerId = int.Parse(ddlBuyers.SelectedValue);

                //var dtBuyerColors = unitOfWork.GetDataTableFromSql($"SELECT {finishingUnitId} as FinishingUnitId, '{receivedDate}' as ReceivedDate,{buyerId} as BuyerId, {styleId} as StyleId, Id as BuyerColorId, ColorDescription FROM BuyerColors WHERE BuyerId = {buyerId} AND IsActive = 1");

                var dtBuyerColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetBuyerColorsForBoothReceivedKnittings {finishingUnitId},{datePart},{buyerId},{styleId}");


                rptKnittedReceivedInBoothColors.DataSource = dtBuyerColors;
                rptKnittedReceivedInBoothColors.DataBind();
            }
        }

        protected void rptKnittedReceivedInBoothColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gvSizeQuantityKnittedReceivedInBoothDetails = (GridView)e.Item.FindControl("gvSizeQuantityKnittedReceivedInBoothDetails");
                var buyerId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BuyerId").ToString());
                LoadSizeInfo(buyerId);
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantityKnittedReceivedInBoothDetails.Columns.Add(templateField);
                    }
                }

                gvSizeQuantityKnittedReceivedInBoothDetails.DataSource = dtOneRow;
                gvSizeQuantityKnittedReceivedInBoothDetails.DataBind();
            }
        }

        protected void gvSizeQuantityKnittedReceivedInBoothDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblStyleId = (Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId");
                var lblBuyerId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId");
                var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                var lblKnittedReceivedDate = (Label)((GridView)sender).DataItemContainer.FindControl("lblKnittedReceivedDate");
                var lblFinishingUnitId = (Label)((GridView)sender).DataItemContainer.FindControl("lblFinishingUnitId");

                var styleId = int.Parse(lblStyleId.Text);
                var buyerId = int.Parse(lblBuyerId.Text);
                var buyerColorId = int.Parse(lblBuyerColorId.Text);
                var receivedDate = DateTime.Parse(lblKnittedReceivedDate.Text);
                var unitId = int.Parse(lblFinishingUnitId.Text);
                if (dtSizes == null || dtSizes.Rows.Count == 0)
                {
                    LoadSizeInfo(buyerId);
                }

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    var sizeId = dtSizes.Rows[i][0].ToString();
                    var sizeInfo = unitOfWork.GetSingleValue($"EXEC usp_GetKnittedReceivedInBoothByStyleIdReceivedDateColorIdAndSizeId {styleId},'{receivedDate}',{buyerColorId}, {sizeId},{unitId}");
                    if (sizeInfo != "0")
                    {
                        var lblQty = new Label();
                        lblQty.Text = sizeInfo;
                        e.Row.Cells[i].Controls.Add(lblQty);
                    }
                }
            }
        }

        protected void rptKnittedItemDeliveredToLinkingSectionDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptKnittedItemDeliverToLinkingSectionColors = (Repeater)e.Item.FindControl("rptKnittedItemDeliverToLinkingSectionColors");
                //var deliveredDate = DataBinder.Eval(e.Item.DataItem, "DeliveredDate").ToString();

                DateTime deliveredDate = DateTime.Parse(DataBinder.Eval(e.Item.DataItem, "DeliveredDate").ToString());
                string datePart = "'" + deliveredDate.ToString("yyyy-MM-dd") + "'";


                var finishingUnitFloorId = int.Parse(DataBinder.Eval(e.Item.DataItem, "FinishingUnitFloorId").ToString());
                var styleId = int.Parse(DataBinder.Eval(e.Item.DataItem, "StyleId").ToString());
                var buyerId = int.Parse(ddlBuyers.SelectedValue);

                //var dtBuyerColors = unitOfWork.GetDataTableFromSql($"SELECT {finishingUnitFloorId} as FinishingUnitFloorId, '{deliveredDate}' as DeliveredDate,{buyerId} as BuyerId, {styleId} as StyleId, Id as BuyerColorId, ColorDescription FROM BuyerColors WHERE BuyerId = {buyerId} AND IsActive = 1");

                var dtBuyerColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetBuyerColorsForKnittingsDeliveredToLinking {finishingUnitFloorId},{datePart},{buyerId},{styleId}");


                rptKnittedItemDeliverToLinkingSectionColors.DataSource = dtBuyerColors;
                rptKnittedItemDeliverToLinkingSectionColors.DataBind();
            }
        }

        protected void rptKnittedItemDeliverToLinkingSectionColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gvSizeQuantityKnittedDeliveredToLinkingSectionDetails = (GridView)e.Item.FindControl("gvSizeQuantityKnittedDeliveredToLinkingSectionDetails");
                var buyerId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BuyerId").ToString());
                LoadSizeInfo(buyerId);
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantityKnittedDeliveredToLinkingSectionDetails.Columns.Add(templateField);
                    }
                }

                gvSizeQuantityKnittedDeliveredToLinkingSectionDetails.DataSource = dtOneRow;
                gvSizeQuantityKnittedDeliveredToLinkingSectionDetails.DataBind();
            }
        }

        protected void gvSizeQuantityKnittedDeliveredToLinkingSectionDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblStyleId = (Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId");
                var lblBuyerId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId");
                var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                var lblKnittedDeliveredDate = (Label)((GridView)sender).DataItemContainer.FindControl("lblKnittedDeliveredDate");
                var lblFinishingUnitFloorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblFinishingUnitFloorId");

                var styleId = int.Parse(lblStyleId.Text);
                var buyerId = int.Parse(lblBuyerId.Text);
                var buyerColorId = int.Parse(lblBuyerColorId.Text);
                var deliveredDate = DateTime.Parse(lblKnittedDeliveredDate.Text);
                var floorId = int.Parse(lblFinishingUnitFloorId.Text);
                if (dtSizes == null || dtSizes.Rows.Count == 0)
                {
                    LoadSizeInfo(buyerId);
                }

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    var sizeId = dtSizes.Rows[i][0].ToString();
                    var sizeInfo = unitOfWork.GetSingleValue($"EXEC usp_GetKnittedReceivedInLinkingSectionByStyleIdReceivedDateAndColorId {styleId},'{deliveredDate}',{buyerColorId}, {sizeId},{floorId}");
                    if (sizeInfo != "0")
                    {
                        var lblQty = new Label();
                        lblQty.Text = sizeInfo;
                        e.Row.Cells[i].Controls.Add(lblQty);
                    }
                }
            }
        }

        protected void rptRemainInBoothDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gvRemainInBoothSizeQuantity = (GridView)e.Item.FindControl("gvRemainInBoothSizeQuantity");
                var buyerId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BuyerId").ToString());
                LoadSizeInfo(buyerId);
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvRemainInBoothSizeQuantity.Columns.Add(templateField);
                    }
                }

                gvRemainInBoothSizeQuantity.DataSource = dtOneRow;
                gvRemainInBoothSizeQuantity.DataBind();
            }
        }

        protected void gvRemainInBoothSizeQuantity_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblStyleId = (Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId");
                var lblBuyerId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId");
                var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                var lblFinishingUnitId = (Label)((GridView)sender).DataItemContainer.FindControl("lblFinishingUnitId");

                var styleId = int.Parse(lblStyleId.Text);
                var buyerId = int.Parse(lblBuyerId.Text);
                var buyerColorId = int.Parse(lblBuyerColorId.Text);
                var unitId = int.Parse(lblFinishingUnitId.Text);
                if (dtSizes == null || dtSizes.Rows.Count == 0)
                {
                    LoadSizeInfo(buyerId);
                }

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    var sizeId = dtSizes.Rows[i][0].ToString();
                    var sizeInfo = unitOfWork.GetSingleValue($"EXEC usp_GetRemainsQtyInBoothByStyleColorAndSizeId {styleId},{buyerColorId},{sizeId},{unitId}");
                    if (sizeInfo != "0")
                    {
                        var lblQty = new Label();
                        lblQty.Text = sizeInfo;
                        e.Row.Cells[i].Controls.Add(lblQty);
                    }
                }
            }
        }

        protected void rptKnittedItemDeliveredToFinishingUnitDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptKnittedToFinishingUnitColors = (Repeater)e.Item.FindControl("rptKnittedToFinishingUnitColors");
                DateTime deliveredDate = DateTime.Parse(DataBinder.Eval(e.Item.DataItem, "DeliveredDate").ToString());
                string datePart = "'" + deliveredDate.ToString("yyyy-MM-dd") + "'";


                var dnittingUnitId = int.Parse(DataBinder.Eval(e.Item.DataItem, "KnittingUnitId").ToString());
                var styleId = int.Parse(DataBinder.Eval(e.Item.DataItem, "StyleId").ToString());
                var buyerId = int.Parse(ddlBuyers.SelectedValue);

                //var dtBuyerColors = unitOfWork.GetDataTableFromSql($"SELECT {dnittingUnitId} as KnittingUnitId,'{deliveredDate}' as DeliveredDate,{buyerId} as BuyerId, {styleId} as StyleId, Id as BuyerColorId, ColorDescription FROM BuyerColors WHERE BuyerId = {buyerId} AND IsActive = 1");

                var dtBuyerColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetBuyerColorsForDeliveredKnittings {dnittingUnitId},{datePart},{buyerId},{styleId}");

                rptKnittedToFinishingUnitColors.DataSource = dtBuyerColors;
                rptKnittedToFinishingUnitColors.DataBind();
            }
        }

        protected void rptKnittedToFinishingUnitColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gvSizeQuantityKnittedDeliveredToFinishingUnitDetails = (GridView)e.Item.FindControl("gvSizeQuantityKnittedDeliveredToFinishingUnitDetails");
                var buyerId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BuyerId").ToString());
                LoadSizeInfo(buyerId);
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantityKnittedDeliveredToFinishingUnitDetails.Columns.Add(templateField);
                    }
                }

                gvSizeQuantityKnittedDeliveredToFinishingUnitDetails.DataSource = dtOneRow;
                gvSizeQuantityKnittedDeliveredToFinishingUnitDetails.DataBind();
            }
        }

        protected void gvSizeQuantityKnittedDeliveredToFinishingUnitDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblStyleId = (Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId");
                var lblBuyerId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId");
                var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                var lblKnittedDeliveredDate = (Label)((GridView)sender).DataItemContainer.FindControl("lblKnittedDeliveredDate");
                var lblKnittingUnitId = (Label)((GridView)sender).DataItemContainer.FindControl("lblKnittingUnitId");

                var styleId = int.Parse(lblStyleId.Text);
                var buyerId = int.Parse(lblBuyerId.Text);
                var buyerColorId = int.Parse(lblBuyerColorId.Text);
                var deliveredDate = DateTime.Parse(lblKnittedDeliveredDate.Text);
                var knittingUnitId = int.Parse(lblKnittingUnitId.Text);
                if (dtSizes == null || dtSizes.Rows.Count == 0)
                {
                    LoadSizeInfo(buyerId);
                }

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    var sizeId = dtSizes.Rows[i][0].ToString();
                    var sizeInfo = unitOfWork.GetSingleValue($"EXEC usp_GetKnittedDeliveredToFinishingUnitByStyleIdDeliveredDateAndColorId {styleId},'{deliveredDate}',{buyerColorId}, {sizeId}, {knittingUnitId}");
                    if (sizeInfo != "0")
                    {
                        var lblQty = new Label();
                        lblQty.Text = sizeInfo;
                        e.Row.Cells[i].Controls.Add(lblQty);
                    }
                }
            }
        }

        protected void rptFinishingUnitFloors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptReaminInHandColors = (Repeater)e.Item.FindControl("rptReaminInHandColors");
                var finishingUnitFloorId = int.Parse(DataBinder.Eval(e.Item.DataItem, "FinishingUnitFloorId").ToString());
                var buyerId = int.Parse(ddlBuyers.SelectedValue);
                var styleId = int.Parse(ddlStyles.SelectedValue);

                //var dtBuyerColors = unitOfWork.GetDataTableFromSql($"SELECT '{finishingUnitFloorId}' as FinishingUnitFloorId,{buyerId} as BuyerId, {styleId} as StyleId, Id as BuyerColorId, ColorDescription FROM BuyerColors WHERE BuyerId = {buyerId} AND IsActive = 1");

                var dtBuyerColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetBuyerColorsForPackingBalance {finishingUnitFloorId},{buyerId},{styleId}");


                rptReaminInHandColors.DataSource = dtBuyerColors;
                rptReaminInHandColors.DataBind();
            }
        }

        protected void rptFinishingUnitLinkingFloor_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptRemainInLinkingSectionDetails = (Repeater)e.Item.FindControl("rptRemainInLinkingSectionDetails");
                var finishingUnitFloorId = int.Parse(DataBinder.Eval(e.Item.DataItem, "FinishingUnitFloorId").ToString());
                var buyerId = int.Parse(ddlBuyers.SelectedValue);
                var styleId = int.Parse(ddlStyles.SelectedValue);


                //var dtBuyerColors = unitOfWork.GetDataTableFromSql($"SELECT '{finishingUnitFloorId}' as FinishingUnitFloorId,{buyerId} as BuyerId, {styleId} as StyleId, Id as BuyerColorId, ColorDescription FROM BuyerColors WHERE BuyerId = {buyerId} AND IsActive = 1");

                var dtBuyerColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetBuyerColorsForLinkingBalance {finishingUnitFloorId},{buyerId},{styleId}");


                rptRemainInLinkingSectionDetails.DataSource = dtBuyerColors;
                rptRemainInLinkingSectionDetails.DataBind();
            }
        }

        protected void rptFinishingUnits_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptRemainInBoothDetails = (Repeater)e.Item.FindControl("rptRemainInBoothDetails");
                var finishingUnitId = int.Parse(DataBinder.Eval(e.Item.DataItem, "FinishingUnitId").ToString());
                var buyerId = int.Parse(ddlBuyers.SelectedValue);
                var styleId = int.Parse(ddlStyles.SelectedValue);

                //var dtBuyerColors = unitOfWork.GetDataTableFromSql($"SELECT '{finishingUnitId}' as FinishingUnitId,{buyerId} as BuyerId, {styleId} as StyleId, Id as BuyerColorId, ColorDescription FROM BuyerColors WHERE BuyerId = {buyerId} AND IsActive = 1");

                var dtBuyerColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetBuyerColorsForBoothBalance {finishingUnitId},{buyerId},{styleId}");


                rptRemainInBoothDetails.DataSource = dtBuyerColors;
                rptRemainInBoothDetails.DataBind();
            }
        }

        protected void rptYarnIssued_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptYarnIssuedUnitColors = (Repeater)e.Item.FindControl("rptYarnIssuedUnitColors");
                var unitId = int.Parse(DataBinder.Eval(e.Item.DataItem, "UnitId").ToString());
                DateTime issueDate = DateTime.Parse(DataBinder.Eval(e.Item.DataItem, "IssueDate").ToString());
                string datePart = "'" + issueDate.ToString("yyyy-MM-dd") + "'";

                var buyerId = int.Parse(ddlBuyers.SelectedValue);
                var styleId = int.Parse(ddlStyles.SelectedValue);
                // var dtBuyerColors = unitOfWork.GetDataTableFromSql($"SELECT '{issueDate}' as DeliveredDate, '{unitId}' as KnittingUnitId,{buyerId} as BuyerId, {styleId} as StyleId, BuyerColors.Id as BuyerColorId, ColorDescription FROM BuyerColors INNER JOIN YarnIssuedForKnitting ON BuyerColors.Id=YarnIssuedForKnitting.BuyerColorId WHERE BuyerId = {buyerId} AND IsActive = 1");

                var dtBuyerColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetBuyerColorsForIssuedYarn {datePart},{unitId},{buyerId},{styleId}");

                rptYarnIssuedUnitColors.DataSource = dtBuyerColors;
                rptYarnIssuedUnitColors.DataBind();
            }
        }

        protected void rptYarnIssuedUnitColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gvSizeQuantityYarnIssueDetails = (GridView)e.Item.FindControl("gvSizeQuantityYarnIssueDetails");
                var buyerId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BuyerId").ToString());
                LoadSizeInfo(buyerId);
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantityYarnIssueDetails.Columns.Add(templateField);
                    }
                }

                gvSizeQuantityYarnIssueDetails.DataSource = dtOneRow;
                gvSizeQuantityYarnIssueDetails.DataBind();
            }
        }

        protected void gvSizeQuantityYarnIssueDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblStyleId = (Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId");
                var lblBuyerId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId");
                var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                var lblYarnIssueDate = (Label)((GridView)sender).DataItemContainer.FindControl("lblYarnIssueDate");
                var lblKnittingUnitId = (Label)((GridView)sender).DataItemContainer.FindControl("lblKnittingUnitId");

                var styleId = int.Parse(lblStyleId.Text);
                var buyerId = int.Parse(lblBuyerId.Text);
                var buyerColorId = int.Parse(lblBuyerColorId.Text);
                var issueDate = DateTime.Parse(lblYarnIssueDate.Text);
                var knittingUnitId = int.Parse(lblKnittingUnitId.Text);
                if (dtSizes == null || dtSizes.Rows.Count == 0)
                {
                    LoadSizeInfo(buyerId);
                }

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    var sizeId = dtSizes.Rows[i][0].ToString();
                    var sizeInfo = unitOfWork.GetSingleValue($"EXEC usp_GetYarnIssuedToUnitByStyleIdIssueDateAndColorId {styleId},'{issueDate}',{buyerColorId}, {sizeId}, {knittingUnitId}");
                    if (sizeInfo != "0")
                    {
                        var lblQty = new Label();
                        lblQty.Text = sizeInfo;
                        e.Row.Cells[i].Controls.Add(lblQty);
                    }
                }
            }
        }

        protected void rptLabeledDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptAttachmentColors = (Repeater)e.Item.FindControl("rptAttachmentColors");
                var attachmentId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "AttachmentId").ToString());
                var buyerId = int.Parse(ddlBuyers.SelectedValue);

                //var dtBuyerColors = unitOfWork.GetDataTableFromSql($"SELECT {attachmentId} as AttachmentId,{buyerId} as BuyerId, Id as BuyerColorId, ColorDescription FROM BuyerColors WHERE BuyerId = {buyerId} AND IsActive = 1");

                var dtBuyerColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetBuyerColorsForAttachedLabel {attachmentId},{buyerId}");

                rptAttachmentColors.DataSource = dtBuyerColors;
                rptAttachmentColors.DataBind();
            }
        }

        protected void rptAlltachmentColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //var shipmnetId = int.Parse(ddlShipment.SelectedValue);
                var rptDeliveryCountryAndQty = (Repeater)e.Item.FindControl("rptDeliveryCountryAndQty");
                var lblBuyerColorId = (Label)e.Item.FindControl("lblBuyerColorId");
                var lblAttachmentId = (Label)e.Item.FindControl("lblAttachmentId");
                var attachmentId = int.Parse(lblAttachmentId.Text);
                var buyerColorId = int.Parse(lblBuyerColorId.Text);
                var dtDeliveryCountries = unitOfWork.GetDataTableFromSql($"EXEC usp_GetDeliveryCountryByLabelAttachmentId {attachmentId},{buyerColorId}");
                rptDeliveryCountryAndQty.DataSource = dtDeliveryCountries;
                rptDeliveryCountryAndQty.DataBind();
            }
        }

        protected void rptDeliveryCountryAndQty_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var lblBuyerId = (Label)((Repeater)sender).NamingContainer.FindControl("lblBuyerId");
                var buyerId = int.Parse(lblBuyerId.Text);
                GridView gvSizeAndQty = (GridView)e.Item.FindControl("gvSizeAndQty");
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);
                if (dtSizes == null || dtSizes.Rows.Count == 0)
                {
                    LoadSizeInfo(buyerId);
                }
                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeAndQty.Columns.Add(templateField);

                    }

                }

                gvSizeAndQty.DataSource = dtOneRow;
                gvSizeAndQty.DataBind();
            }
        }

        protected void gvSizeAndQty_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label txtSizeQty = null;
                Label lblSizeId = null;
                var lblBuyerId = (Label)((RepeaterItem)((GridView)sender).NamingContainer).NamingContainer.NamingContainer.FindControl("lblBuyerId");
                var lblAttachmentId = (Label)((RepeaterItem)((GridView)sender).NamingContainer).NamingContainer.NamingContainer.FindControl("lblAttachmentId");
                var lblBuyerColorId = (Label)((RepeaterItem)((GridView)sender).NamingContainer).NamingContainer.NamingContainer.FindControl("lblBuyerColorId");
                var lblDeliveryCountryId = (Label)((GridView)sender).DataItemContainer.FindControl("lblDeliveryCountryId");
                var deleveryCountryId = Convert.ToInt32(lblDeliveryCountryId.Text);
                var buyerId = int.Parse(lblBuyerId.Text);
                var attachmentId = int.Parse(lblAttachmentId.Text);
                var buyerColorId = int.Parse(lblBuyerColorId.Text);
                if (dtSizes == null || dtSizes.Rows.Count == 0)
                {
                    LoadSizeInfo(buyerId);
                }

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new Label();
                    txtSizeQty.ID = "txtSizeQty1" + i.ToString();

                    var sizeId = Convert.ToInt32(dtSizes.Rows[i][0].ToString());
                    
                    var shipmentColorCountrySize = unitOfWork.GenericRepositories<LabelAttachmentColorSizes>().Get(x => x.LabelAttachmentId == attachmentId && x.DeliveryCountryId == deleveryCountryId && x.BuyerColorId == buyerColorId && x.SizeId == sizeId).FirstOrDefault();

                    if (shipmentColorCountrySize != null)
                    {
                        txtSizeQty.Text = shipmentColorCountrySize.LabelAttachmentQty + "";
                        var lblShipmentColorCountrySize = new Label();
                        lblShipmentColorCountrySize.ID = "lblShipmentColorCountrySize1" + i.ToString();
                        lblShipmentColorCountrySize.Text = shipmentColorCountrySize.Id + "";
                        lblShipmentColorCountrySize.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblShipmentColorCountrySize);
                    }
                    else
                    {
                        txtSizeQty.Text = "";
                        var lblShipmentColorCountrySize = new Label();
                        lblShipmentColorCountrySize.ID = "lblShipmentColorCountrySize1" + i.ToString();
                        lblShipmentColorCountrySize.Text = "";
                        lblShipmentColorCountrySize.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblShipmentColorCountrySize);
                    }


                    txtSizeQty.Width = 30;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);

                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId1" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);
                }
            }
        }
    }
}
