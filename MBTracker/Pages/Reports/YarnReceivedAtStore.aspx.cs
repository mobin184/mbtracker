﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class YarnReceivedAtStore : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
              
                LoadMerchandiserList();
               
                LoadYarnItems();
                //LoadLcNumber();
                //LoadDyeingOrder();
                if (Request.QueryString["styleId"] != null)
                {
                    var styleId = int.Parse(Request.QueryString["styleId"]);

                    //var styleId = int.Parse(Tools.UrlDecode(Request.QueryString["styleId"]));

                    //var reportDate = DateTime.Parse(Request.QueryString["date"]);

                    var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(styleId);
                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
                    BindStylesByBuyer(styleInfo.BuyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleId);
                    //tbxFilterDate.Text = reportDate.ToString("yyyy-MM-dd");

                    rptCriteria.Visible = false;
                    ShowReport();
                }
            }
            else
            {
                if (Tools.HideReportViewerControl(this))
                {
                    divReport.Visible = false;
                }
            }
        }

        private void LoadMerchandiserList()
        {
            var dt = unitOfWork.GetDataTableFromSql("EXEC usp_GetMerchandiserList");
            ddlMerchandiser.Items.Add(new ListItem("", ""));
            ddlMerchandiser.DataSource = dt;
            ddlMerchandiser.DataTextField = "Name";
            ddlMerchandiser.DataValueField = "Id";
            ddlMerchandiser.DataBind();
            ddlMerchandiser.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlMerchandiser.SelectedIndex = 0;
        }


        private void LoadLcNumber(int buyerId)
        {
            //var dt = unitOfWork.GetDataTableFromSql($"SELECT Id as LcId, LCNumber FROM LCInfoBackToBack WHERE Status = 3");
            var dt = unitOfWork.GetDataTableFromSql($@"SELECT DISTINCT
                        lc.Id as LcId, LCNumber
                        FROM LCInfoBackToBack lc
                        INNER JOIN LCPIs lcp ON lc.Id = lcp.LCId
                        INNER JOIN PIInfo p ON lcp.PIId = p.Id
                        WHERE p.BuyerId = '{buyerId}'");

            ddlLCNumber.Items.Add(new ListItem("", ""));
            ddlLCNumber.DataSource = dt;
            ddlLCNumber.DataTextField = "LCNumber";
            ddlLCNumber.DataValueField = "LCNumber";
            ddlLCNumber.DataBind();
            ddlLCNumber.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlLCNumber.SelectedIndex = 0;
        }

        private void LoadDyeingOrder(int buyerId)
        {
            var dt = unitOfWork.GetDataTableFromSql($"SELECT DyeingOrderId, DyeingOrderNumber FROM DyeingOrder WHERE BuyerId = '{buyerId}'");
            ddlDyeingOrder.Items.Add(new ListItem("", ""));
            ddlDyeingOrder.DataSource = dt;
            ddlDyeingOrder.DataTextField = "DyeingOrderNumber";
            ddlDyeingOrder.DataValueField = "DyeingOrderNumber";
            ddlDyeingOrder.DataBind();
            ddlDyeingOrder.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlDyeingOrder.SelectedIndex = 0;
        }


        private void LoadYarnItems()
        {
            var dt = unitOfWork.GetDataTableFromSql("SELECT Id as ItemId, ItemName FROM YarnAccessoriesItems WHERE IsActive = 1 AND ItemTypeId = 2");
            ddlItems.Items.Add(new ListItem("", ""));
            ddlItems.DataSource = dt;
            ddlItems.DataTextField = "ItemName";
            ddlItems.DataValueField = "ItemId";
            ddlItems.DataBind();
            ddlItems.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlItems.SelectedIndex = 0;
        }


        private void LoadStore()
        {
            var dt = unitOfWork.GetDataTableFromSql("SELECT StoreId, StoreName FROM Stores WHERE StoreType = 2");
            ddlStore.Items.Add(new ListItem("", ""));
            ddlStore.DataSource = dt;
            ddlStore.DataTextField = "StoreName";
            ddlStore.DataValueField = "StoreId";
            ddlStore.DataBind();
            ddlStore.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlStore.SelectedIndex = 0;
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            divReport.Visible = false;
            ddlStyles.Items.Clear();
            ddlStore.Items.Clear();
            ddlReceivedType.Items.Clear();
            ddlIssueType.Items.Clear();
            ddlLCNumber.Items.Clear();
            ddlDyeingOrder.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                var buyerId = Convert.ToInt32(ddlBuyers.SelectedValue);
                BindStylesByBuyer(buyerId);
                CommonMethods.LoadYarnAndAccessoriesPurchaseType(ddlReceivedType, 1, 0);
                LoadStore();
                LoadIssueType();
                LoadLcNumber(buyerId);
                LoadDyeingOrder(buyerId);
            }
        }


        protected void LoadIssueType()
        {
            //<asp:ListItem Text="---Select---" Value="" />
            //<asp:ListItem Text="Running" Value="1" />
            //<asp:ListItem Text="Leftover" Value="99" />
            List<IssuType> lstIssueType = new List<IssuType>();
            lstIssueType.Add(new IssuType() { IssueTypeId = "", IssueTypeName = "----Select---" });
            lstIssueType.Add(new IssuType() { IssueTypeId = "1", IssueTypeName = "Running" });
            lstIssueType.Add(new IssuType() { IssueTypeId = "2", IssueTypeName = "On hold" });
            lstIssueType.Add(new IssuType() { IssueTypeId = "99", IssueTypeName = "Leftover" });
            
            ddlIssueType.DataSource = lstIssueType;
            ddlIssueType.DataTextField = "IssueTypeName";
            ddlIssueType.DataValueField = "IssueTypeId";
            ddlIssueType.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                divReport.Visible = false;

                if (ddlBuyers.SelectedValue == "" && ddlMerchandiser.SelectedValue == "" && ddlStore.SelectedValue == "" && ddlItems.SelectedValue == "" && ddlIssueType.SelectedValue == "" && ddlLCNumber.SelectedValue == "" && ddlDyeingOrder.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select at least a search criteria.')", true);
                }
                else
                {
                    ShowReport();
                    //if (ddlBuyers.SelectedValue != "")
                    //{
                    //    //if (ddlStyles.SelectedValue != "")
                    //    //{
                    //        ShowReport();
                    //    //}
                    //    //else
                    //    //{
                    //    //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a style.')", true);
                    //    //}
                    //}
                    //else
                    //{
                    //    ShowReport();
                    //}
                }                
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private void ShowReport()
        {
            int buyerId = 0;
            if(ddlBuyers.SelectedValue != "")
            {
                buyerId = int.Parse(ddlBuyers.SelectedValue);
            }

            int styleId = 0;
            if (ddlStyles.SelectedValue != "")
            {
                styleId = int.Parse(ddlStyles.SelectedValue);
            }

            int colorId = 0;
            if (ddlColors.SelectedValue != "")
            {
                colorId = int.Parse(ddlColors.SelectedValue);
            }

            int receivedType = 0;
            if (ddlReceivedType.SelectedValue != "")
            {
                receivedType = int.Parse(ddlReceivedType.SelectedValue);
            }

            int storeId = 0;
            if(ddlStore.SelectedValue != "")
            {
                storeId = int.Parse(ddlStore.SelectedValue);
            }
            int merchandiserId = 0;
            if (ddlMerchandiser.SelectedValue != "")
            {
                merchandiserId = int.Parse(ddlMerchandiser.SelectedValue);
            }

            int itemId = 0;
            if (ddlItems.SelectedValue != "")
            {
                itemId = int.Parse(ddlItems.SelectedValue);
            }

            int issueType = 0;
            if (ddlIssueType.SelectedValue != "")
            {
                issueType = int.Parse(ddlIssueType.SelectedValue);
            }

            string lcId = "";
            if (ddlLCNumber.SelectedValue != "")
            {
                lcId = ddlLCNumber.SelectedValue;
            }

            string dyeingOrderId = "";
            if (ddlDyeingOrder.SelectedValue != "")
            {
                dyeingOrderId = ddlDyeingOrder.SelectedValue;
            }
            //int userId = CommonMethods.SessionInfo.UserId;
            DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_Rpt_YarnReceivedAtStore {buyerId},{styleId},{colorId},{receivedType},{storeId},{merchandiserId},{itemId},{issueType},'{lcId}','{dyeingOrderId}'");
            
            if (dt.Rows.Count > 0)
            {
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.EnableHyperlinks = true;

                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/YarnBalance.rdlc");

                rptViewer.PageCountMode = PageCountMode.Actual;

                ReportDataSource ds = new ReportDataSource("dsYarnReceivedAtStore", dt);

                var productionDate = DateTime.Now;
                var dtDate = unitOfWork.GetDataTableFromSql($"SELECT '{productionDate.ToString("dd-MMM-yyyy")}' as Date");
                ReportDataSource ds1 = new ReportDataSource("dsDate", dtDate);


                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);
                rptViewer.LocalReport.DataSources.Add(ds1);


                rptViewer.LocalReport.Refresh();
                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
                btnExportToPDF.Visible = true;
                //rptCriteria.Visible = false;

            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
                btnExportToPDF.Visible = false;
            }
        }

        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);

            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            // var productionDate = DateTime.Parse(tbxFilterDate.Text).ToString("dd-MMM-yyyy");
            DoExport("EXCEL", $"Yarn_Balance");

            //ExportOnly();
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            // var productionDate = DateTime.Parse(tbxFilterDate.Text).ToString("dd-MMM-yyyy");
            DoExport("PDF", $"Yarn_Balance");
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindColorsByStyle();
        }


        private void BindColorsByStyle()
        {
            if (ddlStyles.SelectedValue != "")
            {
                var dtStyleColors = new CommonManager().GetStyleColorsByStyle(int.Parse(ddlStyles.SelectedValue));
                ddlColors.DataTextField = "ColorDescription";
                ddlColors.DataValueField = "BuyerColorId";
                ddlColors.DataSource = dtStyleColors;
                ddlColors.DataBind();

                ddlColors.Items.Insert(0, new ListItem("---Select---", string.Empty));
                ddlColors.SelectedIndex = 0;

            }
            else
            {
                ddlColors.Items.Clear();
            }
        }

        public class IssuType
        {
            public string IssueTypeId { get; set; }
            public string IssueTypeName { get; set; }
        }
    }
}