﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using MBTracker.Code_Folder.Reports;
using Repositories;
using MBTracker.Code_Folder;

namespace MBTracker.Pages.Reports
{
    public partial class ProjectionVSProduction : System.Web.UI.Page
    {

        ReportManager reportManager = new ReportManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                LoadFinishingUnit();
            }
        }

        protected void LoadFinishingUnit()
        {
            var dtFinishingUnit = new CommonManager().LoadFinishingUnits();
            ddlFinishingUnit.DataTextField = "FinishingUnitName";
            ddlFinishingUnit.DataValueField = "FinishingUnitId";
            ddlFinishingUnit.DataSource = dtFinishingUnit;
            ddlFinishingUnit.DataBind();
            ddlFinishingUnit.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlFinishingUnit.SelectedIndex = 0;
        }


        protected void btn_showReport(object sender, EventArgs e)
        {
            //rptViewer.Reset();
            try
            {
                if (tbxKnittingDate.Text != "")
                {

                    DateTime reportDate = Convert.ToDateTime(tbxKnittingDate.Text);
                    //DataTable dt = reportManager.GetDailyKnittingInfo(reportDate);
                    var finUnitId = 0;
                    if (!string.IsNullOrEmpty(ddlFinishingUnit.SelectedValue))
                    {
                        finUnitId = int.Parse(ddlFinishingUnit.SelectedValue);
                    }
       

                    var dtTargetVsAchievement = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetTargetVsAchievementReport '{reportDate}','{finUnitId}'");
                    var dtDate = unitOfWork.GetDataTableFromSql($"SELECT '{reportDate.ToString("dd-MMM-yyyy")}' as Date");
                    if (dtTargetVsAchievement.Rows.Count < 1)
                    {
                        lblNoDataFound.Visible = true;
                        divReport.Attributes.Add("class", "hidden");
                        btnExportToEXCEL.Visible = false;
                        lblNoDataFound.Text = "No data found for the date: " + String.Format("{0:dd-MMM-yyyy}", reportDate);

                    }
                    else
                    {

                        rptViewer.ProcessingMode = ProcessingMode.Local;
                        rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/ProjectionVSProduction.rdlc");
                        rptViewer.PageCountMode = PageCountMode.Actual;

                        //ReportDataSource datasource = new ReportDataSource("DailyKnittingData", dt);
                        ReportDataSource datasource1 = new ReportDataSource("dsTargetVsAchievement", dtTargetVsAchievement);
                        ReportDataSource datasource2 = new ReportDataSource("dsDate", dtDate);

                        rptViewer.LocalReport.DataSources.Clear();
                        //rptViewer.LocalReport.DataSources.Add(datasource);
                        rptViewer.LocalReport.DataSources.Add(datasource1);
                        rptViewer.LocalReport.DataSources.Add(datasource2);
                        //rptViewer.LocalReport.Refresh();

                        lblNoDataFound.Visible = false;
                        //rptWidget.Visible = true;
                        divReport.Attributes.Remove("class");
                        btnExportToEXCEL.Visible = true;

                    }

                   // lblReportPageHeading.Text = "Knitting Report for date: " + String.Format("{0:dd-MMM-yyyy}", reportDate);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select the date.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }


        }

        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            var knittingDate = DateTime.Parse(tbxKnittingDate.Text).ToString("dd-MMM-yyyy");
            DoExport("EXCEL", $"TargetAndKnittingSummary_{knittingDate}");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            var knittingDate = DateTime.Parse(tbxKnittingDate.Text).ToString("dd-MMM-yyyy");
            DoExport("PDF", $"TargetAndKnittingSummary_{knittingDate}");
        }


    }
}