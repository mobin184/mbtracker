﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="SummaryOfOrdersV2.aspx.cs" Inherits="MBTracker.Pages.Reports.SummaryOfOrdersV2" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        input[type="radio"], input[type="checkbox"] {
            margin: 0px 0 0;
        }

        #cpMain_cblBuyers td {
            /*display: inline-flex;*/
            margin-right: 20px;
            padding: 0px 5px;
        }

            #cpMain_cblBuyers td > label {
                display: inline !important;
                line-height: 14px;
                margin-left: 5px;
                font-size: 11px;
            }


        #cpMain_cblStyles td {
            /*display: inline-flex;*/
            margin-right: 20px;
            padding: 0px 5px;
        }

            #cpMain_cblStyles td > label {
                display: inline !important;
                line-height: 14px;
                margin-left: 5px;
                font-size: 11px;
            }

        #cpMain_cblColors td {
            /*display: inline-flex;*/
            margin-right: 20px;
            padding: 0px 5px;
        }

            #cpMain_cblColors td > label {
                display: inline !important;
                line-height: 14px;
                margin-left: 5px;
                font-size: 11px;
            }
    </style>
    <asp:UpdatePanel ID="orderUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div class="row-fluid">
                <div class="span10" id="divCriteria" runat="server">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title">
                                <asp:Label ID="lblReportPageHeading" runat="server" Text="Ex-Factory Report"></asp:Label>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="form-horizontal">
                                <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 10px">
                                    <%-- <span style="font-weight: 700; color: #CC0000">*</span> Required Field--%>
                                </div>
                                <div class="control-group">
                                    <div class="col-md-12 col-sm-12">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:CheckBoxList ID="cblBuyers" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="5" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                        <%--<asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                                <div class="controls controls-row">
                                    <asp:Button ID="btnCheckAllBuyers" runat="server" Text="Select All Buyers" OnClick="btnCheckAllBuyers_Click" />
                                    <asp:Button ID="btnUnCheckAllBuyers" runat="server" Text="Unselect All Buyers" OnClick="btnUnCheckAllBuyers_Click" />
                                    <asp:Button ID="btnShowStyles" Style="min-height: 35px; border: 1px solid black;" runat="server" class="btn btn-info" Text="Show Styles" OnClick="btnShowStyles_Click" />
                                </div>
                                <div visible="false" runat="server" id="styleDiv" style="margin-top: 30px;">


                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:CheckBoxList ID="cblStyles" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="4" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                            <asp:Label ID="lblNoStyle" runat="server" Visible="false" Text="<br/>No data found." BackColor="#ffff00"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="controls controls-row">
                                        <asp:Button ID="btnCheckAllStyles" runat="server" Text="Select All Styles" OnClick="btnCheckAllStyles_Click" />
                                        <asp:Button ID="btnUnCheckAllStyles" runat="server" Text="Unselect All Styles" OnClick="btnUnCheckAllStyles_Click" />
                                        <asp:Button ID="btnShowColors" Style="min-height: 35px; border: 1px solid black;" runat="server" class="btn btn-info" Text="Show Colors" OnClick="btnShowColors_Click" />
                                    </div>
                                    <div id="divColors" runat="server" visible="false">
                                        <div class="control-group" style="margin-top: 25px">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="Label2" runat="server" Text="Select Colors:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                            <div class="controls controls-row">
                                                <asp:CheckBoxList ID="cblColors" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="5" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                                <asp:Label ID="lblColorsNotFound" runat="server" Visible="false" Text="<br/>No data found." BackColor="#ffff00"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="controls controls-row">
                                            <asp:Button ID="btnCheckAllColors" runat="server"  Text="Select All Colors" OnClick="btnCheckAllColors_Click" />
                                            <asp:Button ID="btnUnCheckAllColors" runat="server"  Text="Unselect All Colors" OnClick="btnUnCheckAllColors_Click" />
                                        </div>
                                    </div>
                                    <div class="control-group" style="margin-top: 30px; width: 38%; float: left">
                                        <%--<div class="controls controls-row">
                                            <label><strong>Select reporting date range.</strong> </label>
                                        </div>--%>
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="Label5" runat="server" Text="Select Date Range   From:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;</span></label>
                                        <div class="controls controls-row">
                                            <asp:TextBox ID="tbxFromDate" Style="z-index: 99; position: relative;"
                                                runat="server" placeholder="Enter from date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group" style="width: 38%; float: left; margin-top: 30px; margin-left: -50px">
                                        <%--  <div class="controls controls-row">
                                            <label>&nbsp;</label>
                                        </div>--%>
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="Label6" runat="server" Text="To:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;</span></label>
                                        <div class="controls controls-row">
                                            <asp:TextBox ID="tbxToDate" runat="server" placeholder="Enter to date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group" style="width: 100%">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label1" runat="server" Text="&nbsp;"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row" style="width: 100%; float: left">
                                        <br />
                                        <asp:Button ID="btnSubmit" runat="server" Visible="false" ValidationGroup="save" class="btn btn-info pull-left" Text="Show Report" OnClick="btnSubmit_Click" />
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="span12" style="margin-left: 0px;margin-top: -25px;">
                <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="<br/>No data found." BackColor="#ffff00"></asp:Label>
            </div>
            <br />
            <div class="row-fluid" runat="server" id="divReport" visible="true">
                <div class="span12">
                    <div class="widget" style="border: 0px;">
                        <div class="widget-body" style="border: 0px; padding: 0px;">
                            <div style="margin-left: 0%">
                                <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%" ShowToolBar="false" SizeToReportContent="false" Style="overflow-x: auto; min-height: 210px" Height="500px">
                                </rsweb:ReportViewer>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left: 0px;">
                    <asp:Button ID="btnExportToEXCEL" runat="server" class="btn btn-info pull-left" Visible="false" Text="Export to Excel" OnClick="btnExportToExcel_Click" />
                    <asp:Button ID="btnExportToPDF" runat="server" class="btn btn-info pull-left hidden" Visible="false" Text="PDF" OnClick="btnExportToPdf_Click" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToEXCEL" />
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">

    <%--<script type="text/javascript">

        var ddlText, ddlValue, ddl, lblMesg;

        function CacheItems() {


            ddlText = new Array();
            ddlValue = new Array();
            ddl = document.getElementById("<%=ddlStyles.ClientID %>");
            for (var i = 0; i < ddl.options.length; i++) {
                ddlText[ddlText.length] = ddl.options[i].text;
                ddlValue[ddlValue.length] = ddl.options[i].value;
            }

            //alert("The length of ddl: " + ddl.options.length);
        }





        function FilterItems(value) {
            ddl.options.length = 0;

            for (var i = 0; i < ddlText.length; i++) {

                if (ddlText[i].toLowerCase().indexOf(value) != -1) {

                    AddItem(ddlText[i], ddlValue[i]);
                }
            }


            function AddItem(text, value) {

                var opt = document.createElement("option");

                opt.text = text;
                opt.value = value;
                ddl.options.add(opt);

            }
        }

    </script>--%>
</asp:Content>
