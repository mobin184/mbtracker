﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using MBTracker.Code_Folder.Reports;
using Repositories;
using MBTracker.Code_Folder;

namespace MBTracker.Pages.Reports
{
    public partial class MachineReport : System.Web.UI.Page
    {

        ReportManager reportManager = new ReportManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        List<string> selectedUnits = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //LoadFinishingUnit();
                BindKnittingUnit();
            }
        }



        private void BindKnittingUnit()
        {
            DataTable dt = new CommonManager().GetProductionUnitsByUser(Convert.ToInt32(CommonMethods.SessionInfo.UserId));
            cblProductionUnits.DataValueField = "ProductionUnitId";
            cblProductionUnits.DataTextField = "UnitName";
            cblProductionUnits.DataSource = dt;
            cblProductionUnits.DataBind();
        }
        protected void btn_showReport(object sender, EventArgs e)
        {
            //rptViewer.Reset();
            try
            {
                if (tbxFromDate.Text != "" && tbxToDate.Text != "")
                {

                    DateTime fromDate = Convert.ToDateTime(tbxFromDate.Text);
                    DateTime toDate = Convert.ToDateTime(tbxToDate.Text);
                   
                    if(fromDate > toDate)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('From date can not be greater than To date.')", true);
                        return;
                    }
                    for (int i = 0; i < cblProductionUnits.Items.Count; i++)
                    {
                        if (cblProductionUnits.Items[i].Selected)
                        {
                            selectedUnits.Add(cblProductionUnits.Items[i].Value);
                        }
                    }
                    string knittingUnitId = "";
                    if (selectedUnits.Count != 0)
                    {
                        //knittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue);
                        knittingUnitId = String.Join(",", selectedUnits);
                    }
                    int userId = CommonMethods.SessionInfo.UserId;

                    var dtMCData = unitOfWork.GetDataTableFromSql($"Exec usp_RtpGetMCReport '{fromDate}','{toDate}','{userId}','{knittingUnitId}'");
                    var dsParam = unitOfWork.GetDataTableFromSql($"SELECT '{fromDate.ToString("dd-MMM-yyyy")}' as FromDate,  '{toDate.ToString("dd-MMM-yyyy")}' as ToDate");
                    if (dtMCData.Rows.Count < 1)
                    {
                        lblNoDataFound.Visible = true;
                        divReport.Attributes.Add("class", "hidden");
                        btnExportToEXCEL.Visible = false;
                        lblNoDataFound.Text = "No data found!";

                    }
                    else
                    {

                        rptViewer.ProcessingMode = ProcessingMode.Local;
                        rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/MachineReport.rdlc");
                        rptViewer.PageCountMode = PageCountMode.Actual;

                        //ReportDataSource datasource = new ReportDataSource("DailyKnittingData", dt);
                        ReportDataSource datasource1 = new ReportDataSource("dsMCReport", dtMCData);
                        ReportDataSource datasource2 = new ReportDataSource("dsParam", dsParam);

                        rptViewer.LocalReport.DataSources.Clear();
                        //rptViewer.LocalReport.DataSources.Add(datasource);
                        rptViewer.LocalReport.DataSources.Add(datasource1);
                        rptViewer.LocalReport.DataSources.Add(datasource2);
                        //rptViewer.LocalReport.Refresh();

                        lblNoDataFound.Visible = false;
                        //rptWidget.Visible = true;
                        divReport.Attributes.Remove("class");
                        btnExportToEXCEL.Visible = true;

                    }

                   // lblReportPageHeading.Text = "Knitting Report for date: " + String.Format("{0:dd-MMM-yyyy}", reportDate);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select from date and to date.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }


        }

        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            //var knittingDate = DateTime.Parse(tbxKnittingDate.Text).ToString("dd-MMM-yyyy");
            DoExport("EXCEL", $"Machine_Report");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            //var knittingDate = DateTime.Parse(tbxKnittingDate.Text).ToString("dd-MMM-yyyy");
            DoExport("PDF", $"Machine_Report");
        }


    }
}