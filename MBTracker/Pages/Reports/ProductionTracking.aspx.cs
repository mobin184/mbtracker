﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class ProductionTracking : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                LoadWeekDropdown();
                if (Request.QueryString["styleId"] != null)
                {
                    var styleId = int.Parse(Request.QueryString["styleId"]);

                    //var styleId = int.Parse(Tools.UrlDecode(Request.QueryString["styleId"]));

                    //var reportDate = DateTime.Parse(Request.QueryString["date"]);

                    var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(styleId);
                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
                    BindStylesByBuyer(styleInfo.BuyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleId);
                    //tbxFilterDate.Text = reportDate.ToString("yyyy-MM-dd");

                    rptCriteria.Visible = false;
                    ShowReport(styleId);
                }
            }
            else
            {
                if (Tools.HideReportViewerControl(this))
                {
                    divReport.Visible = false;
                }
            }
        }

        private void LoadWeekDropdown()
        {
            var dt = unitOfWork.GenericRepositories<Week>().Get().ToList();
            ddlWeek.Items.Add(new ListItem("", ""));
            ddlWeek.DataSource = dt;
            ddlWeek.DataTextField = "WeekName";
            ddlWeek.DataValueField = "Id";
            ddlWeek.DataBind();
            ddlWeek.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlWeek.SelectedIndex = 0;
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            divReport.Visible = false;
            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                divReport.Visible = false;

                if (ddlBuyers.SelectedValue != "")
                {
                    if(ddlStyles.SelectedValue != "")
                    {
                        int styleId = 0;
                        if (ddlStyles.SelectedValue != "")
                        {
                            styleId = int.Parse(ddlStyles.SelectedValue);
                        }
                        ShowReport(styleId);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a style.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a buyer.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private void ShowReport(int styleId)
        {

            int colorId = 0;
            if (ddlColors.SelectedValue != "")
            {
                colorId = int.Parse(ddlColors.SelectedValue);
            }
            var exFactoryDate = "";
            if (ddlShipmentDates.SelectedValue != "")
            {
                exFactoryDate = DateTime.Parse(ddlShipmentDates.SelectedValue).ToString("yyyy-MM-dd");
            }
            var weekNo = 0;
            if (ddlWeek.SelectedValue != "")
            {
                weekNo = int.Parse(ddlWeek.SelectedValue);
            }
            //int userId = CommonMethods.SessionInfo.UserId;
            DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_Rpt_ProductionTackingReport {styleId},{colorId},'{exFactoryDate}',{weekNo}");
            //var maxDate = dt.AsEnumerable().Max(y=>DateTime.Parse(string.IsNullOrEmpty(y.Field<string>("ActualActivitiesDate")) ? "1900-01-01" : y.Field<string>("ActualActivitiesDate")));
           
            if (dt.Rows.Count > 0)
            {
                var maxDate = dt.AsEnumerable().Max(y => y.Field<DateTime>("ActivitiesDate"));

                var dtSummary = dt.AsEnumerable().Where(x => x.Field<DateTime>("ActivitiesDate") == maxDate).ToList().CopyToDataTable();

                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.EnableHyperlinks = true;

                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/ProductionTracking.rdlc");

                rptViewer.PageCountMode = PageCountMode.Actual;

                ReportDataSource ds = new ReportDataSource("dsProductionTracking", dt);
                ReportDataSource dsSummary = new ReportDataSource("dsProductionSummary", dtSummary);
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);
                rptViewer.LocalReport.DataSources.Add(dsSummary);
                rptViewer.LocalReport.Refresh();
                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
                btnExportToPDF.Visible = true;
                //rptCriteria.Visible = false;

            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
                btnExportToPDF.Visible = false;
            }

        }

        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);

            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            // var productionDate = DateTime.Parse(tbxFilterDate.Text).ToString("dd-MMM-yyyy");
            DoExport("EXCEL", $"Shipment_Target_Tracking");

            //ExportOnly();
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            // var productionDate = DateTime.Parse(tbxFilterDate.Text).ToString("dd-MMM-yyyy");
            DoExport("PDF", $"Shipment_Target_Tracking");
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindColorsByStyle();
            BindShipmentDateDDL();
        }

        private void BindShipmentDateDDL()
        {
            if (ddlStyles.SelectedValue != "")
            {
                var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetShipmentDatesByStyle {int.Parse(ddlStyles.SelectedValue)}");
                ddlShipmentDates.DataTextField = "ShipmentDateText";
                ddlShipmentDates.DataValueField = "ShipmentDate";
                ddlShipmentDates.DataSource = dt;
                ddlShipmentDates.DataBind();

                ddlShipmentDates.Items.Insert(0, new ListItem("---Select---", string.Empty));
                ddlShipmentDates.SelectedIndex = 0;

            }
            else
            {
                ddlShipmentDates.Items.Clear();
            }
        }

        private void BindColorsByStyle()
        {
            if (ddlStyles.SelectedValue != "")
            {
                var dtStyleColors = new CommonManager().GetStyleColorsByStyle(int.Parse(ddlStyles.SelectedValue));
                ddlColors.DataTextField = "ColorDescription";
                ddlColors.DataValueField = "BuyerColorId";
                ddlColors.DataSource = dtStyleColors;
                ddlColors.DataBind();

                ddlColors.Items.Insert(0, new ListItem("---Select---", string.Empty));
                ddlColors.SelectedIndex = 0;

            }
            else
            {
                ddlColors.Items.Clear();
            }
        }
    }
}