﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="KnittingPlanByUnit.aspx.cs" Inherits="MBTracker.Pages.Reports.KnittingPlanByUnit" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <asp:UpdatePanel ID="orderUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div class="row-fluid">
                <div class="span6">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title">
                                <asp:Label ID="lblReportPageHeading" runat="server" Text="Knitting Plan By Unit"></asp:Label>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="form-horizontal">
                                <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 10px">
                                    <%-- <span style="font-weight: 700; color: #CC0000">*</span> Indicates Required Field--%>
                                </div>
                                <div class="control-group">
                                    <div class="col-md-12 col-sm-12">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblBuyer" runat="server" Text="Start Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox runat="server" ID="tbxFromDate" Style="min-width: 200px;" TextMode="Date"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxFromDate"><span style="font-weight: 700; color: #CC0000">Please select from date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label2" runat="server" Text="End Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox runat="server" ID="tbxToDate" Style="min-width: 200px;" TextMode="Date"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxToDate"><span style="font-weight: 700; color: #CC0000">Please select to date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label3" runat="server" Text="Select a Unit:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList runat="server" ID="ddlUserKnittingUnit" Style="width: 200px;"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label1" runat="server" Text="&nbsp;"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row">
                                        <br />
                                        <asp:Button ID="btnSubmit" runat="server" ValidationGroup="save" class="btn btn-info pull-left" Text="Show Report" OnClick="btnSubmit_Click" />
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <br />

            <div class="span12" style="margin-left: 0px;">
                <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="<br/>No plan found." BackColor="#ffff00"></asp:Label>
            </div>
            <div class="row-fluid" runat="server" id="divReport" visible="true">
                <div class="span12">
                    <div class="widget" style="border: 0px;">
                        <div class="widget-body" style="border: 0px; padding: 0px;">
                            <div style="margin-left: 0%">
                                <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%" ShowToolBar="false" SizeToReportContent="false" Style="overflow-x: auto; min-height: 210px">
                                </rsweb:ReportViewer>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left:0px;">
                    <asp:Button ID="btnExportToEXCEL" runat="server" class="btn btn-info pull-left" Visible="false" Text="Export to Excel" OnClick="btnExportToExcel_Click" />
                    <asp:Button ID="btnExportToPDF" runat="server" class="btn btn-info pull-left hidden" Visible="false" Text="PDF" OnClick="btnExportToPdf_Click" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToEXCEL" />
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
