﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="LCBackToBackSummaryReport.aspx.cs" Inherits="MBTracker.Pages.Reports.LCBackToBackSummaryReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <asp:UpdatePanel ID="orderUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <style>
                .widget-body {
                    margin-top: -1px;
                }

                .table th, .table td {
                    text-align: center;
                }

                #cpMain_cblBanks td {
                    display: inline-flex;
                    margin-right: 10px;
                    min-width: 125px;
                }

                    #cpMain_cblBanks td > label {
                        display: inline !important;
                        line-height: 19px;
                        margin-left: 5px;
                    }

                #cpMain_cblBanks {
                    margin-top: 10px
                }
            </style>
            <div class="row-fluid" runat="server" id="filterDiv">
                <div class="span12">
                    <div class="span6" id="rptCriteria" runat="server">
                        <div class="widget">
                            <div class="widget-header">
                                <div class="title">
                                    <span class="fs1" aria-hidden="true" data-icon=""></span>
                                    <asp:Label runat="server" ID="lblMachinePlanning" Text="Export LC Summary Report:"></asp:Label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-horizontal">
                                <div class="widget-body">
                                    <div class="control-group">
                                        <div class="col-md-12">
                                            <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                                                <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="control-group">
                                                <label for="inputBuyer" class="control-label">
                                                    <asp:Label ID="Label5" runat="server" Text="From Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                <div class="controls controls-row">
                                                    <asp:TextBox ID="tbxFromDate" runat="server" placeholder="Enter from date" Width="60%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbxFromDate"><span style="font-weight: 700; color: #CC0000">Please select from date.</span></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label for="inputBuyer" class="control-label">
                                                    <asp:Label ID="Label6" runat="server" Text="To Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                <div class="controls controls-row">
                                                    <asp:TextBox ID="tbxToDate" runat="server" placeholder="Enter to date" Width="60%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbxToDate"><span style="font-weight: 700; color: #CC0000">Please select to date.</span></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="col-md-12">
                                        <div class="control-group">
                                            <label for="inputStyle" class="control-label">
                                                <asp:Label ID="Label3" runat="server" Text="Banks:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:CheckBoxList ID="cblBanks" runat="server" CellPadding="10" CellSpacing="10" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="cblBanks"><span style="font-weight: 700; color: #CC0000">Please select a bank.</span></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="display: flex; justify-content: center; padding-top: 15px;">
                                        <br />
                                        <asp:Button ID="btnViewReport" runat="server" ValidationGroup="save" class="btn btn-success btn-midium pull-left btnStyle" Text="View Report" OnClick="btnViewReport_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <br />

            <div class="span12" style="margin-left: 0px;">
                <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="<br/>No data found." BackColor="#ffff00"></asp:Label>
            </div>
            <div class="row-fluid" runat="server" id="divReport" visible="true">
                <div class="span12">
                    <div class="widget" style="border: 0px;">
                        <div class="widget-body" style="border: 0px; padding: 0px;">
                            <div style="margin-left: 0%">
                                <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%" ShowToolBar="false" SizeToReportContent="false" Style="overflow-x: auto; min-height: 210px" Height="500px">
                                </rsweb:ReportViewer>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left: 0px;">
                    <asp:Button ID="btnExportToEXCEL" runat="server" class="btn btn-info pull-left" Visible="false" Text="Export to Excel" OnClick="btnExportToExcel_Click" />
                    <asp:Button ID="btnExportToPDF" runat="server" class="btn btn-info pull-left hidden" Visible="false" Text="PDF" OnClick="btnExportToPdf_Click" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToEXCEL" />
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
