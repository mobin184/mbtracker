﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using MBTracker.Code_Folder.Reports;
using Repositories;
using MBTracker.Code_Folder;

namespace MBTracker.Pages.Reports
{
    public partial class KnittingDeliveryRawData : System.Web.UI.Page
    {

        ReportManager reportManager = new ReportManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
        }

        //protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    divReport.Visible = false;
        //    ddlStyles.Items.Clear();
        //    if (ddlBuyers.SelectedValue != "")
        //    {
        //        BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
        //    }
        //}
        //private void BindStylesByBuyer(int buyerId)
        //{
        //    CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        //}

        DateTime DeliveryDate
        {
            set { ViewState["deliveryDate"] = value; }
            get
            {
                try
                {
                    return Convert.ToDateTime(ViewState["deliveryDate"]);
                }
                catch
                {
                    return DateTime.Now;
                }
            }
        }

        protected void btn_showReport(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(tbxFromDate.Text))
                {

                    DateTime FromDate = Convert.ToDateTime(tbxFromDate.Text);
                    DeliveryDate = FromDate;
                    var userId = CommonMethods.SessionInfo.UserId;
                    var dt = unitOfWork.GetDataTableFromSql($"Exec usp_Rpt_GetDailyDeliveryRawData '{FromDate.ToString("yyyy-MM-dd")}'");
                    //var dtDateRange = unitOfWork.GetDataTableFromSql($"SELECT '{FromDate.ToString("dd-MMM-yyyy")}' as FromDate, '{ToDate.ToString("dd-MMM-yyyy")}' as ToDate ");
                    if (dt.Rows.Count < 1)
                    {
                        lblNoDataFound.Visible = true;
                        divReport.Attributes.Add("class", "hidden");
                        btnExportToEXCEL.Visible = false;
                        lblNoDataFound.Text = "No data found";
                    }
                    else
                    {
                        rptViewer.ProcessingMode = ProcessingMode.Local;
                        rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/KnittingDeliveryRawData.rdlc");
                        rptViewer.PageCountMode = PageCountMode.Actual;
                        ReportDataSource datasource1 = new ReportDataSource("dsDeliveryRawData", dt);

                        rptViewer.LocalReport.DataSources.Clear();
                        rptViewer.LocalReport.DataSources.Add(datasource1);

                        lblNoDataFound.Visible = false;
                        divReport.Attributes.Remove("class");
                        btnExportToEXCEL.Visible = true;
                        divReport.Visible = true;

                    }

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select delivery date.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }


        }

        private void DoExport(string exportType, string reportName, string extension)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            //string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            //DoExport("EXCEL", $"Target VS Achievment","csv");
            DoExport("EXCELOPENXML", $"Delivery_Raw_{DeliveryDate.ToString("dd_MMM_yyyy")}", "xslx");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            DoExport("PDF", $"Delivery_Raw_{DeliveryDate.ToString("dd_MMM_yyyy")}", "");
        }

    }
}