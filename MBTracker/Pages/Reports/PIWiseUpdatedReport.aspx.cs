﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Reports;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class PIWiseUpdatedReport : System.Web.UI.Page
    {
        ReportManager reportManager = new ReportManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        List<int> selectedBanks = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                CommonMethods.LoadApplicantBanksDropdown(ddlApplicantBanks, 1, 0);
                CommonMethods.LoadMerchandiserLeaderDropDown(ddlMerchandisingLeaders, 1, 0);
            }
            else
            {

            }

        }

        //protected void btnSearch_Click(object sender, EventArgs e)
        //{
        //    LoadPiInfoList();
        //}

        //private void LoadPiInfoList()
        //{
        //    try
        //    {
        //        UnitOfWork objUnitOfWork = new UnitOfWork();
        //        DataTable objDataTable = objUnitOfWork.GetPiListByDateSearch(0, 0, Convert.ToDateTime(tbxFromDate.Text), Convert.ToDateTime(tbxToDate.Text), txtSearch.Text.Trim());
        //        if (objDataTable.Rows.Count > 0)
        //        {
        //            ddlPiList.DataSource = objDataTable;
        //            ddlPiList.DataTextField = "PINumber";
        //            ddlPiList.DataValueField = "Id";
        //            ddlPiList.DataBind();

        //        }
        //        else
        //        {
        //            ddlPiList.DataSource = null;

        //            ddlPiList.DataBind();

        //        }
        //    }
        //    catch (Exception)
        //    {


        //    }
        //}

        protected void ddlBuyer_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue == "")
            {
                ddlStyles.Items.Clear();
            }
            else
            {
                BindStylesByBuyer(ddlStyles, int.Parse(ddlBuyers.SelectedValue));
            }


        }

        private void BindStylesByBuyer(DropDownList ddlStyles, int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
            CommonMethods.LoadYarnAndAccessoriesSupplierDropdown(ddlYarnAndAccssSuppliers, 1, 0);
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlStyles.SelectedValue != "")
            //{
               // CommonMethods.LoadYarnAndAccessoriesSupplierDropdown(ddlYarnAndAccssSuppliers, 1, 0);
            //}
            //else
            //{
            //    ddlYarnAndAccssSuppliers.Items.Clear();

            //}

        }

        protected void ddlYarnAndAccssSuppliers_SelectedIndexChanged(object sender, EventArgs e)
        {
            var supplierIdTxt = ddlYarnAndAccssSuppliers.SelectedValue;
            if (supplierIdTxt != "")
            {
                BindData(Convert.ToInt32(ddlBuyers.SelectedValue == "" ? "0" : ddlBuyers.SelectedValue), Convert.ToInt32(ddlStyles.SelectedValue == "" ? "0" : ddlStyles.SelectedValue), Convert.ToInt32(ddlYarnAndAccssSuppliers.SelectedValue), Convert.ToInt32(ddlApplicantBanks.SelectedValue == "" ? "0" : ddlApplicantBanks.SelectedValue));
            }
            else
            {

                pnlBuyerPIs.Visible = false;

            }
        }

        private void BindData(int buyerId, int StyleId, int SupplierId, int BankId)
        {
            LoadBuyerLCS(buyerId, StyleId, SupplierId,BankId);
            //LoadBuyerPIS(buyerId, StyleId, SupplierId);

        }

        protected void LoadBuyerLCS(int buyerId, int StyleId, int SupplierId, int BankId)
        {
            string ApplicantBankIdString = "";
            if (BankId > 0)
            {
                ApplicantBankIdString = "AND ApplicantBankId= " + BankId.ToString();
            }

            pnlBuyerLCs.Visible = true;

            DataTable dt = unitOfWork.GetDataTableFromSql("SELECT Id,LCNumber  FROM LCInfoBackToBack WHERE BeneficiaryId = "+SupplierId + ApplicantBankIdString + "");

            if (dt.Rows.Count > 0)
            {
                cblBuyerLCs.DataValueField = "Id";
                cblBuyerLCs.DataTextField = "LCNumber";
                cblBuyerLCs.DataSource = dt;
                cblBuyerLCs.DataBind();
            }
            else
            {
                pnlBuyerLCs.Visible = false;
            }
        }

        protected void LoadBuyerPIS(int buyerId, int StyleId, int SupplierId)
        {

            pnlBuyerPIs.Visible = true;

            DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC GetPIInfoByBuyerOrStyleAndSupplier '{buyerId}','{StyleId}','{SupplierId}'");

            if (dt.Rows.Count > 0)
            {
                cblBuyerPIs.DataValueField = "PIId";
                cblBuyerPIs.DataTextField = "PINumber";
                cblBuyerPIs.DataSource = dt;
                cblBuyerPIs.DataBind();
            }
            else
            {
                pnlBuyerPIs.Visible = false;
            }
        }

        protected void btnViewPIByLC_Click(object sender, EventArgs e)
        {
            try
            {
                cblBuyerPIs.Items.Clear();
                string LCValue = "";
                int NumberofLcCount = 0;
                 NumberofLcCount = cblBuyerLCs.Items.Count;
                if(NumberofLcCount > 0) { 
                for (int i = 0; i < NumberofLcCount; i++)
                {
                    if (cblBuyerLCs.Items[i].Selected)
                    {
                        LCValue += cblBuyerLCs.Items[i].Value + ",";
                    }
                }

                LCValue = LCValue.TrimEnd(',');
                LoadBuyerPISBYALLLC(LCValue);
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void LoadBuyerPISBYALLLC(string LCValue)
        {

            pnlBuyerPIs.Visible = true;

            DataTable dt = unitOfWork.GetDataTableFromSql("Select LPS.LCId, LPS.PIId, PIf.PINumber from LCPIs LPS left join PIInfo PIf on PIf.Id = LPS.PIId where LPS.LCId IN(" + LCValue + ")");

            if (dt.Rows.Count > 0)
            {
                cblBuyerPIs.DataValueField = "PIId";
                cblBuyerPIs.DataTextField = "PINumber";
                cblBuyerPIs.DataSource = dt;
                cblBuyerPIs.DataBind();
            }
            else
            {
                pnlBuyerPIs.Visible = false;
            }
        }

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            try
            {
                String Serchingparameter = "";
                string PIValue = "";
                string LCValue = "";


                for (int i = 0; i < cblBuyerPIs.Items.Count; i++)
                    {
                        if (cblBuyerPIs.Items[i].Selected)
                        {
                            PIValue += cblBuyerPIs.Items[i].Value + ",";
                        }
                    }

                    PIValue = PIValue.TrimEnd(',');
                    
               if(PIValue != "")
                {
                     Serchingparameter = "AND p.id IN(" + PIValue + ")";
                    //Serchingparameter = PIValue;
                }
                else
                {
                    int NumberofLcCount = 0;
                    NumberofLcCount = cblBuyerLCs.Items.Count;
                    if (NumberofLcCount > 0)
                    {
                        for (int i = 0; i < NumberofLcCount; i++)
                        {
                            if (cblBuyerLCs.Items[i].Selected)
                            {
                                LCValue += cblBuyerLCs.Items[i].Value + ",";
                            }
                        }
                        LCValue = LCValue.TrimEnd(',');
                    }

                    if (LCValue != "")
                    {
                        Serchingparameter = " AND LIBB.Id IN(" + LCValue + ")";
                    }
                    else
                    {
                       if(int.Parse(ddlMerchandisingLeaders.SelectedValue == "" ? "0" : ddlMerchandisingLeaders.SelectedValue) >0)
                        {
                            int MerchandiserId = int.Parse(ddlMerchandisingLeaders.SelectedValue);
                            Serchingparameter = Serchingparameter + " AND U.Id = " + MerchandiserId;
                        }

                        if (int.Parse(ddlApplicantBanks.SelectedValue == "" ? "0" : ddlApplicantBanks.SelectedValue) > 0)
                        {
                            int ApplicantBank = int.Parse(ddlApplicantBanks.SelectedValue);
                            Serchingparameter = Serchingparameter + " AND LIBB.ApplicantBankId = " + ApplicantBank;
                        }

                        if (int.Parse(ddlStyles.SelectedValue == "" ? "0" : ddlStyles.SelectedValue) > 0)
                        {
                            int StyleId = int.Parse(ddlStyles.SelectedValue);
                            Serchingparameter = Serchingparameter + " AND BS.Id = " + StyleId;
                        }
                        if (int.Parse(ddlYarnAndAccssSuppliers.SelectedValue == "" ? "0" : ddlYarnAndAccssSuppliers.SelectedValue) > 0)
                        {
                            int YarnAndAccssSupplierId = int.Parse(ddlYarnAndAccssSuppliers.SelectedValue);
                            Serchingparameter = Serchingparameter + " AND p.BeneficiaryId = " + YarnAndAccssSupplierId;
                        }

                    }

                }

                string AllParameter = Serchingparameter;
                ShowReport(Convert.ToDateTime(tbxPIFromDate.Text), Convert.ToDateTime(tbxPIToDate.Text), AllParameter);


            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private void ShowReport(DateTime dateTime1, DateTime dateTime2, String AllParameter)
        {
            DataTable dt = reportManager.GetPIWiseStoreReceivedUpdatedReport(dateTime1.ToString("yyyy-MM-dd"), dateTime2.ToString("yyyy-MM-dd"), AllParameter);
            if (dt.Rows.Count > 0)
            {
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/PIWiseUpdatedReport.rdlc");

                ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(datasource);
                //rptViewer.RepeatOnNewPage = True;
                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
               // btnExportToPDF.Visible = true;
            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
               // btnExportToPDF.Visible = false;
            }
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            DoExport("EXCEL", $"STORE_RECEIVE_SUMMARY");
        }

        //protected void btnExportToPdf_Click(object sender, EventArgs e)
        //{
        //    DoExport("PDF", $"STORE_RECEIVE_SUMMARY");
        //}

        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);

            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }

    }
}