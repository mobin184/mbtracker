﻿using MBTracker.Code_Folder;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class SalableStockReportForMega : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadYarnForMegaDropdown(ddlItems, 1, 0);
            }
            else
            {
                if (Tools.HideReportViewerControl(this))
                {
                    divReport.Visible = false;
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                divReport.Visible = false;
                 ShowReport();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private void ShowReport()
        {
            int itemId = 0;
            if (ddlItems.SelectedValue != "")
            {
                itemId = int.Parse(ddlItems.SelectedValue);
            }

            int userId = CommonMethods.SessionInfo.UserId;
            DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC Sp_RDLC_SalableStock_Report_ForMega   {itemId} ");

            if (dt.Rows.Count > 0)
            {
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.EnableHyperlinks = true;
                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/SalableStockReportForMega.rdlc");
                rptViewer.PageCountMode = PageCountMode.Actual;
                ReportDataSource ds = new ReportDataSource("SalableStockForMega", dt);
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);
                rptViewer.LocalReport.Refresh();
                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
                btnExportToPDF.Visible = true;
            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
                btnExportToPDF.Visible = false;
            }

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            DoExport("EXCEL", $"Salable_Stock_Report");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            DoExport("PDF", $"Salable_Stock_Report");
        }
        private void DoExport(string exportType, string reportName)
        {
            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);

            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
}