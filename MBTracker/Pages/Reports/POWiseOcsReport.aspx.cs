﻿using MBTracker.Code_Folder;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class POWiseOcsReport : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                
            }
            else
            {
                if (Tools.HideReportViewerControl(this))
                {
                    divReport.Visible = false;
                }
            }

        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            divReport.Visible = false;
            ddlStyles.Items.Clear();
            ddlSeason.Items.Clear();

            if (ddlBuyers.SelectedValue != "")
            {

                CommonMethods.LoadDropdownById(ddlStyles, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
                CommonMethods.LoadDropdownById(ddlSeason, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerSeasons", 1, 0);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                divReport.Visible = false;

                if (ddlBuyers.SelectedValue != "")
                {

                    ShowReport();

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select Buyer Name')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private void ShowReport()
        {

            int buyerId = 0;
            if (ddlBuyers.SelectedValue != "")
            {
                buyerId = int.Parse(ddlBuyers.SelectedValue);
            }

            int styleId = 0;
            if (ddlStyles.SelectedValue != "")
            {
                styleId = int.Parse(ddlStyles.SelectedValue);
            }

            int seasonId = 0;
            if (ddlSeason.SelectedValue != "")
            {
                seasonId = int.Parse(ddlSeason.SelectedValue);
            }


            int userId = CommonMethods.SessionInfo.UserId;

            DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC Sp_PoWise_OCS_Report   {buyerId}, {styleId}, {seasonId} ");


            if (dt.Rows.Count > 0)
            {
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.EnableHyperlinks = true;


                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/PoWiseOcsReport.rdlc");
                rptViewer.PageCountMode = PageCountMode.Actual;

                //var dtDate = unitOfWork.GetDataTableFromSql($"SELECT '{DateTime.Now}' as Date");

                ReportDataSource ds = new ReportDataSource("PoWiseOCsReport", dt);
                // ReportDataSource ds1 = new ReportDataSource("dsDate", dtDate);


                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(ds);
                // rptViewer.LocalReport.DataSources.Add(ds1);
                rptViewer.LocalReport.Refresh();

                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                btnExportToEXCEL.Visible = true;
                btnExportToPDF.Visible = true;

            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;
                btnExportToPDF.Visible = false;
            }

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            DoExport("EXCEL", $"OCS_Details_Report");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            DoExport("PDF", $"OCS_Details_Report");
        }

        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);

            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }


    }
}