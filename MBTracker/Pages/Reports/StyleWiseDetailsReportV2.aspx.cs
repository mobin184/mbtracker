﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Reports;
using Microsoft.Reporting.WebForms;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Reports
{
    public partial class StyleWiseDetailsReportV2 : System.Web.UI.Page
    {
        ReportManager reportManager = new ReportManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        List<int> selectedBanks = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
            else
            {

            }

        }

        protected void ddlBuyer_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue == "")
            {
                ddlStyles.Items.Clear();
                ddlItems.Items.Clear();
            }
            else
            {
                BindStylesByBuyer(ddlStyles, int.Parse(ddlBuyers.SelectedValue));
                CommonMethods.LoadDropdownById(ddlSeason, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerSeasons", 1, 0);
                CommonMethods.LoadDropdown(ddlYears, "Years WHERE IsActive = 1", 1, 0);
                CommonMethods.LoadYarnAndAccessoriesItemsDropdown(ddlItems, 1, 0);

            }


        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlStyles.SelectedValue == "")
            {
                ddlStyles.Items.Clear();
                ddlOrders.Items.Clear();
            }
            else
            {
                int selectedStyleId = 0;
                if (ddlStyles.SelectedValue != "")
                {
                    selectedStyleId = int.Parse(ddlStyles.SelectedValue);
                }

                CommonMethods.LoadDropdownById(ddlSeason, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerSeasons", 1, 0);
                CommonMethods.LoadDropdown(ddlYears, "Years WHERE IsActive = 1", 1, 0);
                CommonMethods.LoadOrderDropdownByStyle(ddlOrders, selectedStyleId, 1, 0);
                
            }


        }

        private void BindStylesByBuyer(DropDownList ddlStyles, int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }


        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            try
            {
                int SeasonId = 0;
                if (ddlSeason.SelectedValue != "")
                {
                    SeasonId = int.Parse(ddlSeason.SelectedValue);
                }

                int YearsId = 0;
                if (ddlYears.SelectedValue != "")
                {
                    YearsId = int.Parse(ddlYears.SelectedValue);
                }

                int OrdersId = 0;
                if (ddlOrders.SelectedValue != "")
                {
                    OrdersId = int.Parse(ddlOrders.SelectedValue);
                }
                int ItemsId = 0;
                if (ddlItems.SelectedValue != "")
                {
                    ItemsId = int.Parse(ddlItems.SelectedValue);
                }


                ShowReport(int.Parse(ddlBuyers.SelectedValue), int.Parse(ddlStyles.SelectedValue), Convert.ToDateTime(tbxPIFromDate.Text), Convert.ToDateTime(tbxPIToDate.Text), SeasonId, YearsId, OrdersId, ItemsId);

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }


        private void ShowReport(int BuyerId, int StyleId, DateTime dateTime1, DateTime dateTime2, int SeasonId, int YearsId, int OrdersId, int ItemsId)
        {

            DataTable dt = reportManager.GetStyleWisePIAndLCDetailsReport(BuyerId, StyleId, dateTime1.ToString("yyyy-MM-dd"), dateTime2.ToString("yyyy-MM-dd"), SeasonId, YearsId, OrdersId, ItemsId);
            if (dt.Rows.Count > 0)
            {
                rptViewer.ProcessingMode = ProcessingMode.Local;
                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Pages/Reports/StyleWiseDetailsReportV2.rdlc");
                //rptViewer.PageCountMode = PageCountMode.Actual;
                ReportDataSource datasource = new ReportDataSource("MHBData", dt);
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(datasource);
                // Set command timeout
                // rptViewer.LocalReport.SetParameters(new ReportParameter("ExecutionTimeout", "600"));

                divReport.Visible = true;
                lblNoDataFound.Visible = false;
                //divReport.Attributes.Remove("class");
                btnExportToEXCEL.Visible = true;

            }
            else
            {
                divReport.Visible = false;
                lblNoDataFound.Visible = true;
                btnExportToEXCEL.Visible = false;

            }

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            DoExport("EXCEL", $"StyleWisePILCDetailsReport_SUMMARY");
        }

        protected void btnExportToPdf_Click(object sender, EventArgs e)
        {
            DoExport("PDF", $"StyleWiseReport_SUMMARY");
        }

        private void DoExport(string exportType, string reportName)
        {

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = rptViewer.LocalReport.Render(exportType, null, out contentType, out encoding, out extension, out streamIds, out warnings);
            var filename = string.Format("{0}_REPORT.{1}", reportName, extension);
            //var filename = string.Format("{0}_REPORT_{1:yyyy-MM-dd}.{2}", reportName, DateTime.Now, extension);

            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();

        }


    }
}