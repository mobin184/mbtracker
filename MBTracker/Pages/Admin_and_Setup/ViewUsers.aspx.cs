﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using Repositories;
using MBTracker.EF;

namespace MBTracker.Pages.Admin_and_Setup
{
    public partial class ViewUsers : System.Web.UI.Page
    {

        UserManager userManager = new UserManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                BindData();
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmResetPassword")
                {
                    ResetPassword();
                }
                else if (parameter == "confirmDelete")
                {
                    DeleteOperation();
                }
            }

        }

        protected void BindData()
        {
            DataTable dt = userManager.GetUsers();
            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();

            }
            else
            {
                rpt.DataSource = null;
                rpt.DataBind();
            }
        }


        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {

            int userId = Convert.ToInt32(e.CommandArgument.ToString());
            Response.Redirect("AddNewUser.aspx?userId=" + userId);

        }

        int UserId
        {
            set { ViewState["userId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["userId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {

            try
            {
                UserId = Convert.ToInt32(e.CommandArgument.ToString());
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('"+Tools.GetErrorMessage(ex.Message)+"');", true);
            }
        }

        protected void DeleteOperation()
        {
            try
            {
                DateTime currentTime = DateTime.Now;
                int result = userManager.DeleteUser(UserId, CommonMethods.SessionInfo.UserName, currentTime.ToString("MM/dd/yyyy hh:mm:ss"));

                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
                BindData();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "returnToPage('ViewUsers.aspx');", true);

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }


        protected void lnkResetPassword_Command(object sender, CommandEventArgs e)
        {
            try
            {
                UserId = Convert.ToInt32(e.CommandArgument.ToString());
                var title = "Warning";
                var msg = "Are you sure you want to reset password?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmResetPassword','350');", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        protected void ResetPassword()
        {
            try
            {
                string pass = EncryptionDecryption.Encrypt("12345", true);
                var userInfo = unitOfWork.GenericRepositories<Users>().GetByID(UserId);
                if(userInfo != null)
                {
                    userInfo.Password = pass;
                    unitOfWork.GenericRepositories<Users>().Update(userInfo);
                    unitOfWork.Save();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Password reset succcessfully.');", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "returnToPage('ViewUsers.aspx');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('User not found.');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }
    }



}