﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewUsers.aspx.cs" Inherits="MBTracker.Pages.Admin_and_Setup.ViewUsers" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>Users
                    </div>
                </div>
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <asp:Repeater ID="rpt" runat="server">
                            <HeaderTemplate>
                                <table id="data-table2" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="Label2" runat="server" Text="Full Name"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label5" runat="server" Text="User Name"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label1" runat="server" Text="Role Name"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label9" runat="server" Text="Department"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label4" runat="server" Text="Designation"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label6" runat="server" Text="Employee ID"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label7" runat="server" Text="Email Address"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label3" runat="server" Text="Action"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="hidden"><%#Eval("Id") %>></td>
                                    <td style="text-align:left"><%#Eval("FullName") %> </td>
                                    <td style="text-align:left"><%#Eval("UserName") %></td>
                                    <td style="text-align:left"><%#Eval("RoleName") %></td>
                                    <td style="text-align:left"><%#Eval("DepartmentName") %></td>
                                    <td style="text-align:left"><%#Eval("DesignationName") %></td>
                                    <td width="10%"><%#Eval("EmployeeId") %></td>
                                    <td style="text-align:left"><%#Eval("Email") %></td>

                                    <td>
                                        <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-small hidden-phone" OnCommand="btnDelete_Command" Text="Delete"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkResetPassword" runat="server" CommandName="ResetPassword" CommandArgument='<%#Eval("Id") %>' class="btn btn-warning btn-small hidden-phone" style="color:#212529" OnCommand="lnkResetPassword_Command" Text="Reset Pass"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" OnCommand="btnEdit_Command" Text="Edit"></asp:LinkButton>
                                    </td>

                                    <%--                                    <td>
                                        <asp:HiddenField runat="server" ID="hfId" Value='<%#Eval("Id") %>' />
                                        <asp:LinkButton ID="lnkbtnEdit" runat="server" OnClick="lnkbtnEdit_OnClick" CssClass="fa fa-2x fa-edit" />
                                        <asp:LinkButton ID="lnkbtnDelete" runat="server" OnClick="lnkbtnDelete_OnClick" CssClass="fa fa-2x fa-trash-o" OnClientClick="return confirm('Are you sure You want to Delete?')" />
                                    </td>--%>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                         </table>
                            <div class="clearfix">
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
  <%--  <script src="../../js/jquery.dataTables.js"></script>--%>
</asp:Content>
