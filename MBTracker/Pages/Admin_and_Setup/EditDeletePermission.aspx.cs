﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using System.Web.Services;
using Repositories;
using MBTracker.EF;
using System.Web.Script.Serialization;

namespace MBTracker.Pages.Admin_and_Setup
{
    public partial class EditDeletePermission : System.Web.UI.Page
    {

        UnitOfWork unitOfWork = new UnitOfWork();
        MenuTaskManager objTask = new MenuTaskManager();

        RoleManager objRole = new RoleManager();
        public int parentCount = 0;
        public int childCount = 0;

        public static int RoleId;



        protected void Page_Load(object sender, EventArgs e)
        {
            if (CommonMethods.SessionInfo.UserName == null)
                Response.Redirect("~/Pages/Login.aspx");

            if (!IsPostBack)
            {
                CommonMethods.LoadDropdown(ddlRole, "UserRoles WHERE IsActive = 1 ORDER BY 2", 1, 0);
                LoadParent();
                LoadEditDeletePermission();
            }

        }

        protected void LoadParent()
        {
            var dt = unitOfWork.GetDataTableFromSql($"EXEC GetTaskListForEditDeletePermission");
            rptTasks.DataSource = dt;
            rptTasks.DataBind();
        }

        protected void LoadEditDeletePermission()
        {
            Clear();
            if (ddlRole.SelectedValue != "")
            {
                var roleId = int.Parse(ddlRole.SelectedValue);
                var permissions = unitOfWork.GenericRepositories<EditDeleteButtonsForRoles>().Get(x => x.RoleId == roleId).ToList();
                if (permissions.Count > 0)
                {
                    for (int i = 0; i < rptTasks.Items.Count; i++)
                    {
                        var tbxEditDuration = (TextBox)rptTasks.Items[i].FindControl("tbxEditDuration");
                        var tbxDeleteDuration = (TextBox)rptTasks.Items[i].FindControl("tbxDeleteDuration");
                        var lblTaskId = (Label)rptTasks.Items[i].FindControl("lblTaskId");
                        int taskId = 0;
                        if (lblTaskId.Text != "")
                            taskId = int.Parse(lblTaskId.Text);


                        tbxEditDuration.Text = permissions.Where(x => x.TaskId == taskId && x.ButtonType == 1).FirstOrDefault()?.EnableDurationInDays + "";
                        tbxDeleteDuration.Text = permissions.Where(x => x.TaskId == taskId && x.ButtonType == 2).FirstOrDefault()?.EnableDurationInDays + "";
                    }
                }
            }
        }



        private void Clear()
        {
            for (int i = 0; i < rptTasks.Items.Count; i++)
            {
                var tbxEditDuration = (TextBox)rptTasks.Items[i].FindControl("tbxEditDuration");
                var tbxDeleteDuration = (TextBox)rptTasks.Items[i].FindControl("tbxDeleteDuration");

                tbxEditDuration.Text = "";
                tbxDeleteDuration.Text = "";
            }
        }


        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadEditDeletePermission();
        }


        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Pages/Admin_and_Setup/TaskPermission.aspx");
        }


        //public DataTable TaskToRole()
        //{
        //    DataTable dt = new DataTable("TaskToRole");
        //    dt.Columns.Add("TId", typeof(int));
        //    dt.Columns.Add("RId", typeof(int));
        //    dt.Columns.Add("CdBy", typeof(string));
        //    dt.Columns.Add("CDate", typeof(string));
        //    return dt;
        //}


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (ddlRole.SelectedValue != "")
            {
                var roleId = int.Parse(ddlRole.SelectedValue);
                //var oldPermissions = unitOfWork.GenericRepositories<EditDeleteButtonsForRoles>().Get(x => x.RoleId == roleId && (x.ButtonType == 1 || x.ButtonType == 2)).ToList();
                //foreach (var item in oldPermissions)
                //{
                //    unitOfWork.GenericRepositories<EditDeleteButtonsForRoles>().Delete(item);
                //}
                for (int i = 0; i < rptTasks.Items.Count; i++)
                {
                    var tbxEditDuration = (TextBox)rptTasks.Items[i].FindControl("tbxEditDuration");
                    var tbxDeleteDuration = (TextBox)rptTasks.Items[i].FindControl("tbxDeleteDuration");
                    var lblTaskId = (Label)rptTasks.Items[i].FindControl("lblTaskId");
                    var lblUrl = (Label)rptTasks.Items[i].FindControl("lblUrl");
                    int taskId = 0;
                    if (lblTaskId.Text != "")
                        taskId = int.Parse(lblTaskId.Text);

                    if (!string.IsNullOrEmpty(tbxEditDuration.Text))
                    {
                        var oldPermissions = unitOfWork.GenericRepositories<EditDeleteButtonsForRoles>().Get(x => x.RoleId == roleId && x.ButtonType == 1 && x.TaskId == taskId).FirstOrDefault();
                        if (oldPermissions == null)
                        {
                            var aEditDeleteButtonsForRoles = new EditDeleteButtonsForRoles()
                            {
                                RoleId = roleId,
                                TaskId = taskId,
                                ButtonType = 1,
                                ButtonNumberOnPage = 1,
                                PageName = lblUrl.Text.Split('/').LastOrDefault().Replace(".aspx", ""),
                                CreatedBy = CommonMethods.SessionInfo.UserName,
                                EnableDurationInDays = int.Parse(tbxEditDuration.Text),
                                CreateDate = DateTime.Now
                            };
                            unitOfWork.GenericRepositories<EditDeleteButtonsForRoles>().Insert(aEditDeleteButtonsForRoles);
                        }
                        else
                        {
                            oldPermissions.UpdatedBy = CommonMethods.SessionInfo.UserName;
                            oldPermissions.EnableDurationInDays = int.Parse(tbxEditDuration.Text);
                            oldPermissions.UpdateDate = DateTime.Now;
                            unitOfWork.GenericRepositories<EditDeleteButtonsForRoles>().Update(oldPermissions);
                        }

                    }
                    if (!string.IsNullOrEmpty(tbxDeleteDuration.Text))
                    {
                        var oldPermissions = unitOfWork.GenericRepositories<EditDeleteButtonsForRoles>().Get(x => x.RoleId == roleId && x.ButtonType == 2 && x.TaskId == taskId).FirstOrDefault();
                        if (oldPermissions == null)
                        {
                            var aEditDeleteButtonsForRoles = new EditDeleteButtonsForRoles()
                            {
                                RoleId = roleId,
                                TaskId = taskId,
                                ButtonType = 2,
                                ButtonNumberOnPage = 1,
                                PageName = lblUrl.Text.Split('/').LastOrDefault().Replace(".aspx", ""),
                                CreatedBy = CommonMethods.SessionInfo.UserName,
                                EnableDurationInDays = int.Parse(tbxDeleteDuration.Text),
                                CreateDate = DateTime.Now
                            };
                            unitOfWork.GenericRepositories<EditDeleteButtonsForRoles>().Insert(aEditDeleteButtonsForRoles);
                        }
                        else
                        {
                            oldPermissions.UpdatedBy = CommonMethods.SessionInfo.UserName;
                            oldPermissions.EnableDurationInDays = int.Parse(tbxDeleteDuration.Text);
                            oldPermissions.UpdateDate = DateTime.Now;
                            unitOfWork.GenericRepositories<EditDeleteButtonsForRoles>().Update(oldPermissions);
                        }
                    }
                }
                unitOfWork.Save();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select role first.')", true);
            }

        }

        [WebMethod]
        public void GetAllTaskItems()
        {
            var res = unitOfWork.GenericRepositories<TaskItems>().Get(x => x.Status == 1).OrderBy(x => x.Order).ToList();
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(res));
        }

    }
}