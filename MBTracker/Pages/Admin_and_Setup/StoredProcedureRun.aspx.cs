﻿using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Repositories;
using MBTracker.EF;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.Remoting.Contexts;
using System.Configuration;

namespace MBTracker.Pages.Admin_and_Setup
{
    public partial class StoredProcedureRun : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        private MBTrackerEntities context = new MBTrackerEntities();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            // Retrieve the connection string from your web.config file
            string connectionString = ConfigurationManager.ConnectionStrings["ErpServer"].ConnectionString;

            // Create a new SqlConnection object using the connection string
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a new SqlCommand object with your stored procedure code
                SqlCommand command = new SqlCommand(tbxStoredProcedure.Text.Trim(), connection);

                // Set the command type to be a stored procedure
                command.CommandType = CommandType.StoredProcedure;

                // Execute the command to update the stored procedure
                command.ExecuteNonQuery();

                // Close the connection
                connection.Close();
            }

            // Redirect the user to another page after the stored procedure has been updated
            Response.Redirect("AnotherPage.aspx");


        }


        public string GetSingleStringValue(string sql)
        {
            string strValue = "";

            try
            {
                using (var context = new MBTrackerEntities())
                {
                    using (var command = context.Database.Connection.CreateCommand())
                    {
                        command.CommandText = sql;
                        command.CommandType = CommandType.Text;
                        command.CommandTimeout = 300;

                        if (context.Database.Connection.State != ConnectionState.Open)
                        {
                            context.Database.Connection.Open();
                        }

                        using (var result = command.ExecuteReader())
                        {
                            while (result.Read())
                            {
                                strValue = result[0].ToString();
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle any exceptions that occur during the execution of the stored procedure
                // For example, you could log the error message to a file or display it in a message box
                strValue = $"Error executing stored procedure: {ex.Message}";
            }

            return strValue;
        }

    }
}