﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddNewRole.aspx.cs" Inherits="MBTracker.Pages.Admin_and_Security.AddNewRole" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
<div class="row-fluid">
         <%--<div class="span2"></div>--%>
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>  <asp:Label runat="server" ID="roleActionTitle" Text="Add a New Role"></asp:Label>
                    </div>
                </div>
                <div class="widget-body" >
                    <div class="form-horizontal no-margin">
                        <div style="text-align: right; font-size: 12px; line-height: 50px;padding-bottom: 7px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Role Name<span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxRoleName" required="true" CssClass="span12 form-control" type="text" placeholder="Role Name"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Role Description
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxDescription" CssClass="span12 form-control" TextMode="MultiLine" placeholder="Role Description"></asp:TextBox>
                            </div>
                        </div>

                         <div class="control-group">
                                <br />
                                <div class="controls-row">
                                    <asp:button id="btnSubmit" runat="server" text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" cssclass="btn btn-info pull-right" validationgroup="save" onclick="btnSave_Click" />
                                    <asp:button id="btnUpdate" runat="server" text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" Visible="false" cssclass="btn btn-info pull-right" validationgroup="save" onclick="btnUpdate_Click" />
                                </div>
                            </div>


                    </div>
                </div>
            </div>
        </div>
  <%--   <div class="span4"></div>--%>

    </div>
    
</asp:Content>


