﻿using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder;
using MBTracker.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using MBTracker.Code_Folder.Admin_and_Setup;
using Microsoft.ReportingServices.ReportProcessing.ReportObjectModel;
using Repositories;
using System.Drawing;

namespace MBTracker.Pages.Admin_and_Setup
{
    public partial class AddNewEditDeleteButtonPermission : System.Web.UI.Page
    {
        ButtonManager buttonManager = new ButtonManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            BindData();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string actionTaker = CommonMethods.SessionInfo.UserName;
            int RoleId = int.Parse(tbxRoleId.Text);
            string PageName = tbxPageName.Text;
            int ButtonType = int.Parse(tbxButtonType.Text);
            int ButtonNumberOnPage = int.Parse(tbxButtonNumberOnPage.Text);
            int EnableDurationInDays = int.Parse(tbxEnableDurationInDays.Text);
           

            var newButton = new EditDeleteButtonsForRoles()
            {
                RoleId = RoleId,
                PageName = PageName,
                ButtonType = ButtonType,
                ButtonNumberOnPage = ButtonNumberOnPage,
                EnableDurationInDays = EnableDurationInDays,
                CreatedBy = actionTaker,
                CreateDate = DateTime.Now
            };
            unitOfWork.GenericRepositories<EditDeleteButtonsForRoles>().Insert(newButton);
            unitOfWork.Save();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);

            ClearFormData();
            BindData();

        }

        //protected void ClearAll()
        //{
        //    tbxRoleId.Text = "";
        //    tbxPageName.Text = "";
        //    tbxButtonType.Text = "";
        //    tbxButtonNumberOnPage.Text = "";
        //    tbxEnableDurationInDays.Text = "";

        //}

        int ButtonId
        {
            set { ViewState["buttonId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["buttonId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void rptButtonPermission_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {

                ButtonId = Convert.ToInt32(e.CommandArgument.ToString());
                PopulateEditDeleteButtonPermission(ButtonId);


            }
            else if (e.CommandName == "Delete")
            {


                try
                {
                    //BuyerId = Convert.ToInt32(e.CommandArgument.ToString());
                   // int result = buyerManager.DeleteBuyer(BuyerId, CommonMethods.SessionInfo.UserName, DateTime.Now);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                }
                catch (Exception ex)
                {

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Error - something went wrong.')", true);
                }

                BindData();

               
            }
        }

        protected void PopulateEditDeleteButtonPermission(int ButtonId)
        {
            var Button = unitOfWork.GenericRepositories<EditDeleteButtonsForRoles>().GetByID(ButtonId);
            if (Button != null)
            {
                tbxRoleId.Text   = Button.RoleId.ToString();
                tbxPageName.Text = Button.PageName.ToString();
                tbxButtonType.Text = Button.ButtonType.ToString();
                tbxButtonNumberOnPage.Text = Button.ButtonNumberOnPage.ToString();
                tbxEnableDurationInDays.Text = Button.EnableDurationInDays.ToString();
               // ddlIsActive.SelectedValue = beneficiary.IsActive + "";
                //roleActionTitle.Text = "Update Beneficiary";
                btnSubmit.Visible = false;
                btnUpdate.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "onEdit()", true);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Invalid beneficiary id.')", true);
            }
        }

        protected void BindData()
        {

            DataTable dt = new CommonManager().GetButtonPermission(CommonMethods.SessionInfo.UserId);

            if (dt.Rows.Count > 0)
            {
                rptButton.DataSource = dt;
                rptButton.DataBind();
            }
            else
            {
                rptButton.DataSource = null;
                rptButton.DataBind();
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
                var EditDeleteButtonInfo = unitOfWork.GenericRepositories<EditDeleteButtonsForRoles>().GetByID(ButtonId);
                if (EditDeleteButtonInfo != null)
                {
                    EditDeleteButtonInfo.RoleId = int.Parse(tbxRoleId.Text);
                    EditDeleteButtonInfo.PageName = tbxPageName.Text;
                    EditDeleteButtonInfo.ButtonType = int.Parse(tbxButtonType.Text);
                    EditDeleteButtonInfo.ButtonNumberOnPage = int.Parse(tbxButtonNumberOnPage.Text);
                    EditDeleteButtonInfo.EnableDurationInDays = int.Parse(tbxEnableDurationInDays.Text);
                    EditDeleteButtonInfo.UpdateDate = DateTime.Now;
                    EditDeleteButtonInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                    unitOfWork.GenericRepositories<EditDeleteButtonsForRoles>().Update(EditDeleteButtonInfo);
                    unitOfWork.Save();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                    BindData();
                    ClearFormData();

                    btnSubmit.Visible = true;
                    btnUpdate.Visible = false;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Invalid color Id.')", true);
                }
           
        }

        private void ClearFormData()
        {
            tbxRoleId.Text = string.Empty;
            tbxPageName.Text = string.Empty;
            tbxButtonType.Text = string.Empty;
            tbxButtonNumberOnPage.Text = string.Empty;
            tbxEnableDurationInDays.Text = string.Empty;
        }

    }
}