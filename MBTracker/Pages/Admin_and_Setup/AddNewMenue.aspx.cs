﻿using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using System.Data;
using Repositories;

namespace MBTracker.Pages.Admin_and_Setup
{
    public partial class AddNewMenue : System.Web.UI.Page
    {
        MenuTaskManager MenuTaskManager = new MenuTaskManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                

               // EnableDisableDeleteButton();
                BindData();

            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    TaskItems(MenuID);
                }
            }

        }

        private void TaskItems(int MenuID)
        {

            int returnValue = MenuTaskManager.DeleteMenu(MenuID, CommonMethods.SessionInfo.UserName, DateTime.Now);

            if (returnValue > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Menue Item deletion was failed.')", true);
            }
            ClientScript.RegisterClientScriptBlock(this.GetType(), "refresh", "setTimeout('window.location.href=window.location.href', 3000);", true);


        }

        int MenuID
        {
            set { ViewState["Id"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["Id"]);
                }
                catch
                {
                    return 0;
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            string actionTaker = CommonMethods.SessionInfo.UserName;

            MenuID = MenuTaskManager.InsertTaskMenu(tbxTextEng.Text, tbxURL.Text, Convert.ToInt32(tbxParentId.Text), Convert.ToInt32(tbxOrder.Text), actionTaker, DateTime.Now);
            BindData();
            if (MenuID > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Role name already exists. Please try another name')", true);
            }

            ClearAll();


        }

        protected void ClearAll()
        {
            tbxTextEng.Text = "";
            tbxURL.Text = "";
            tbxParentId.Text = "";
            tbxOrder.Text = "";
            
        }

        protected void rptMenu_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {

               // BuyerId = Convert.ToInt32(e.CommandArgument.ToString());
               // PopulateBuyer(BuyerId);


            }
            else if (e.CommandName == "Delete")
            {


                try
                {
                    MenuID = Convert.ToInt32(e.CommandArgument.ToString());
                    var title = "Warning";
                    var msg = "Are you sure you want to delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);

                    // int result = MenuTaskManager.DeleteMenu(MenuID, CommonMethods.SessionInfo.UserName, DateTime.Now);

                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                }
                catch (Exception ex)
                {

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Error - something went wrong.')", true);
                }

                BindData();


            }
        }

        protected void BindData()
        {
            //var lc = unitOfWork.GetDataTableFromSql($"SELECT * FROM ExportLCInfo WHERE BuyerId = '{buyerId}'")
            DataTable dt = unitOfWork.GetDataTableFromSql($"SELECT * FROM TaskItems");

            if (dt.Rows.Count > 0)
            {
                rptMenu.DataSource = dt;
                rptMenu.DataBind();
            }
            else
            {
                rptMenu.DataSource = null;
                rptMenu.DataBind();
            }
        }

    }
}