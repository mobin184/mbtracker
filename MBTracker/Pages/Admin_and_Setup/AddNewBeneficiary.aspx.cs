﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using Repositories;
using MBTracker.EF;

namespace MBTracker.Pages.Admin_and_Setup
{
    public partial class AddNewBeneficiary : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                EnableDisableDeleteButton();
                BindData();

            }

        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");

            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("AddNewBeneficiary", 2, 1);

        }


        protected void BindData()
        {

            DataTable dt = unitOfWork.GetDataTableFromSql("SELECT Id, SupplierName, IsActive FROM YarnAndAccessoriesSuppliers");

            if (dt.Rows.Count > 0)
            {
                rptBuyers.DataSource = dt;
                rptBuyers.DataBind();
            }
            else
            {
                rptBuyers.DataSource = null;
                rptBuyers.DataBind();
            }
        }



        int BeneficiaryId
        {
            set { ViewState["beneficiaryId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["beneficiaryId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }


        protected void PopulateBeneficiary(int beneficiaryId)
        {
            var beneficiary = unitOfWork.GenericRepositories<YarnAndAccessoriesSuppliers>().GetByID(beneficiaryId);
            if (beneficiary != null)
            {
                tbxBeneficiaryName.Text = beneficiary.SupplierName;
                ddlIsActive.SelectedValue = beneficiary.IsActive+"";
                roleActionTitle.Text = "Update Beneficiary";
                btnSubmit.Visible = false;
                btnUpdate.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "onEdit()", true);
                
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Invalid beneficiary id.')", true);
            }           
        }


        protected void rptBuyer_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {

                BeneficiaryId = Convert.ToInt32(e.CommandArgument.ToString());
                PopulateBeneficiary(BeneficiaryId);
            }
            else if (e.CommandName == "Delete")
            {
                try
                {
                    BeneficiaryId = Convert.ToInt32(e.CommandArgument.ToString());
                    var result = unitOfWork.GenericRepositories<YarnAndAccessoriesSuppliers>().GetByID(BeneficiaryId);
                    if(result != null)
                    {
                        unitOfWork.GenericRepositories<YarnAndAccessoriesSuppliers>().Delete(result);
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Invalid beneficiary id.')", true);
                    }                 
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Error - something went wrong.')", true);
                }

                BindData();

                //var styleCannotBeDeleted = unitOfWork.GenericRepositories<viewStylesCanNotBeDeleted>().GetByID(BuyerStyleId);
                //if (styleCannotBeDeleted == null)
                //{
                //    var title = "Warning";
                //    var msg = "Are you sure you want to delete?";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                //}
                //else
                //{
                //    var title = "Warning";
                //    var msg = "Sorry, you can not delete it.<br />There was some dependencies of this style.";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                //}
            }
        }



        protected void btnSave_Click(object sender, EventArgs e)
        {

            string actionTaker = CommonMethods.SessionInfo.UserName;
            var benName = tbxBeneficiaryName.Text.Trim();
            var isActive = int.Parse(ddlIsActive.SelectedValue);

            var isExist = unitOfWork.GenericRepositories<YarnAndAccessoriesSuppliers>().IsExist(x => x.SupplierName == benName);

            if (!isExist)
            {
                string user = CommonMethods.SessionInfo.UserName;
                var newBan = new YarnAndAccessoriesSuppliers()
                {
                    SupplierName = benName,
                    IsActive = isActive,
                    CreatedBy = user,
                    CreateDate = DateTime.Now
                };
                unitOfWork.GenericRepositories<YarnAndAccessoriesSuppliers>().Insert(newBan);
                unitOfWork.Save();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
               // SetPermissionForUser(BeneficiaryId);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Beneficiary already exists.')", true);
            }

            ClearAll();
            BindData();

        }


        protected void ClearAll()
        {
            tbxBeneficiaryName.Text = "";
            ddlIsActive.SelectedValue = "1";
            //tbxDescription.Text = "";

        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string actionTaker = CommonMethods.SessionInfo.UserName;

            var beneficiary = unitOfWork.GenericRepositories<YarnAndAccessoriesSuppliers>().GetByID(BeneficiaryId);
            if (beneficiary!=null)
            {
                beneficiary.SupplierName = tbxBeneficiaryName.Text.Trim();
                beneficiary.IsActive = int.Parse(ddlIsActive.SelectedValue);
                beneficiary.UpdatedBy = actionTaker;
                beneficiary.UpdateDate = DateTime.Now;
                unitOfWork.GenericRepositories<YarnAndAccessoriesSuppliers>().Update(beneficiary);
                unitOfWork.Save();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Beneficiary update was failed.')", true);
            }

            //Response.AddHeader("REFRESH", "2;URL=ViewRoles.aspx");
            ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('AddNewBeneficiary.aspx');", true);
        }


    }
}