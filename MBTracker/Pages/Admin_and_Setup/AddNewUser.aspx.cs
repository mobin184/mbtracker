﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using Repositories;
using MBTracker.EF;

namespace MBTracker.Pages.Admin_and_Setup
{
    public partial class AddNewUser : System.Web.UI.Page
    {

        UserManager userManager = new UserManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {


                CommonMethods.LoadDropdown(ddlRoles, "UserRoles WHERE IsActive = 1 ORDER BY 2", 1, 0);
                CommonMethods.LoadDropdown(ddlDepartments, "UserDepartments", 1, 0);
                CommonMethods.LoadDropdown(ddlDesignations, "UserDesignations", 1, 0);
                CommonMethods.LoadBuyersCheckBoxList(cblBuyers);
                CommonMethods.LoadProductionUnitsCheckBoxList(cblProductionUnits);
                CommonMethods.LoadFinishingUnits(rptFinishingUnits);
                LoadNotificationEvents();



                if (Request.QueryString["userId"] != null)
                {
                    UserId = int.Parse(Request.QueryString["userId"]);

                    PopulateUser(UserId);
                }

            }
        }


        int UserId
        {
            set { ViewState["userId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["userId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void LoadNotificationEvents()
        {
            var dt = unitOfWork.GetDataTableFromSql("SELECT Id,EventName FROM NotificationEvents");
            if (dt.Rows.Count > 0)
            {
                cblEvents.DataValueField = "Id";
                cblEvents.DataTextField = "EventName";
                cblEvents.DataSource = dt;
                cblEvents.DataBind();
            }
        }



        protected void PopulateUser(int userId)
        {

            DataTable dt = userManager.GetUserById(userId);
            if (dt.Rows.Count > 0)
            {
                tbxName.Text = dt.Rows[0]["FullName"].ToString();
                tbxMobile.Text = dt.Rows[0]["Phone"].ToString();
                tbxEmail.Text = dt.Rows[0]["Email"].ToString();
                tbxEmployeeID.Text = dt.Rows[0]["EmployeeId"].ToString();
                tbxUserName.Text = dt.Rows[0]["UserName"].ToString();


                tbxpassword.Attributes.Add("value", "pqrxyz");
                tbxConfirmPass.Attributes.Add("value", "pqrxyz");

                RequiredFieldValidator7.Text = "";
                RequiredFieldValidator2.Text = "";


                ddlRoles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlRoles, Convert.ToInt32(dt.Rows[0]["RoleId"]));
                ddlDepartments.SelectedIndex = CommonMethods.MatchDropDownItem(ddlDepartments, Convert.ToInt32(dt.Rows[0]["Department"]));
                ddlDesignations.SelectedIndex = CommonMethods.MatchDropDownItem(ddlDesignations, Convert.ToInt32(dt.Rows[0]["Designation"]));

                tbxName.Enabled = false;
                tbxEmployeeID.Enabled = false;
                tbxUserName.Enabled = false;
                tbxpassword.Enabled = false;
                tbxConfirmPass.Enabled = false;


                //if (ddlRoles.SelectedItem.Text.Contains("Merchandiser"))
                //{
                //    //pnlBuyers.Visible = true;
                //    //pnlProductionUnits.Visible = false;
                //    //pnlFinsihingUnits.Visible = false;
                //    //SelectAllFinishingFloors();
                //    //SelectAllProductionUnits();
                //    CommonMethods.SelectBuyersCheckBoxListItems(cblBuyers, UserId);
                //    CommonMethods.SelectProductionUnitsCheckBoxListItems(cblProductionUnits, UserId);
                //    CommonMethods.SelectFinishingUnitFloorsCheckBoxListItems(rptFinishingUnits, UserId);
                //}
                //else if (ddlRoles.SelectedItem.Text.Contains("Knitting"))
                //{
                //    //pnlProductionUnits.Visible = true;
                //    //pnlBuyers.Visible = false;
                //    //pnlFinsihingUnits.Visible = false;
                //    CommonMethods.SelectProductionUnitsCheckBoxListItems(cblProductionUnits, UserId);
                //    //SelectAllBuyers();
                //    //SelectAllFinishingFloors();
                //}
                //else if (ddlRoles.SelectedItem.Text.Contains("Finishing"))
                //{
                //    //pnlProductionUnits.Visible = false;
                //    //pnlBuyers.Visible = false;
                //    //pnlFinsihingUnits.Visible = true;
                //    CommonMethods.SelectFinishingUnitFloorsCheckBoxListItems(rptFinishingUnits, UserId);
                //    //SelectAllBuyers();
                //    //SelectAllProductionUnits();
                //}
                //else
                //{

                //    SelectAllBuyers();
                //    SelectAllProductionUnits();
                //    SelectAllFinishingFloors();
                //    SelectAllNotificationEvents();


                //    //pnlBuyers.Visible = false;
                //    //pnlProductionUnits.Visible = false;
                //    //pnlFinsihingUnits.Visible = false;

                //}

                CommonMethods.SelectBuyersCheckBoxListItems(cblBuyers, UserId);
                CommonMethods.SelectProductionUnitsCheckBoxListItems(cblProductionUnits, UserId);
                CommonMethods.SelectFinishingUnitFloorsCheckBoxListItems(rptFinishingUnits, UserId);
                
                var userNotificationEvents = unitOfWork.GenericRepositories<UserNotificationEvents>().Get(x => x.UserId == userId).ToList();
                foreach (ListItem li in cblEvents.Items)
                {
                    var id = int.Parse(li.Value);
                    var isPermited = userNotificationEvents.Any(x => x.NotificationEventId == id);
                    if (isPermited)
                    {
                        li.Selected = true;
                    }
                }

            }

            userActionTitle.Text = "Update User";
            btnSave.Visible = false;
            btnUpdate.Visible = true;

        }

        private void SelectCheckBoxItems()
        {

            CommonMethods.SelectBuyersCheckBoxListItems(cblBuyers, UserId);
            CommonMethods.SelectProductionUnitsCheckBoxListItems(cblProductionUnits, UserId);
            CommonMethods.SelectFinishingUnitFloorsCheckBoxListItems(rptFinishingUnits, UserId);

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            string pass = EncryptionDecryption.Encrypt(tbxpassword.Text.Trim(), true);
            if (cblBuyers.SelectedIndex == -1)
            {
                lblNoBuyerSelected.Visible = true;
                lblNoProdUnitSelected.Visible = false;
            }
            else if (cblProductionUnits.SelectedIndex == -1)
            {
                lblNoProdUnitSelected.Visible = true;
                lblNoBuyerSelected.Visible = false;
            }
            else
            {

                lblNoBuyerSelected.Visible = false;
                lblNoProdUnitSelected.Visible = false;

                UserId = userManager.CreateUser(tbxUserName.Text, pass, tbxEmail.Text, tbxMobile.Text, tbxName.Text, Convert.ToInt32(ddlRoles.SelectedValue), Convert.ToInt32(ddlDepartments.SelectedValue), Convert.ToInt32(ddlDesignations.SelectedValue), Convert.ToInt32(tbxEmployeeID.Text), CommonMethods.SessionInfo.UserName, DateTime.Now);
                if (UserId > 0)
                {
                    int saveBuyersResult = SaveBuyers();
                    if (saveBuyersResult > 0)
                    {
                        int saveProductionUnitsResult = SaveProductionUnits();

                        if (saveProductionUnitsResult > 0)
                        {

                            int saveFinishingUnitFloorsResult = SaveFinishingUnitFloors();

                            if (saveFinishingUnitFloorsResult > 0)
                            {
                                foreach (ListItem item in cblEvents.Items)
                                {
                                    if (item.Selected)
                                    {
                                        var un = new UserNotificationEvents()
                                        {
                                            UserId = UserId,
                                            NotificationEventId = int.Parse(item.Value),
                                            CreatedBy = CommonMethods.SessionInfo.UserName,
                                            CreateDate = DateTime.Now
                                        };
                                        unitOfWork.GenericRepositories<UserNotificationEvents>().Insert(un);
                                    }
                                }
                                unitOfWork.Save();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Finishing unit floors could not be saved.')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Production Units could not be saved.')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Buyers could not be saved.')", true);
                    }


                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('The user already exists. Please try different one.')", true);
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                ClearAll();

            }

        }


        public int SaveBuyers()
        {


            DataTable dt = BuyersToUser();
            for (int i = 0; i < cblBuyers.Items.Count; i++)
            {
                if (cblBuyers.Items[i].Selected)
                {
                    dt.Rows.Add(UserId, cblBuyers.Items[i].Value, CommonMethods.SessionInfo.UserName, DateTime.Now);
                }
            }

            userManager.DeleteUserBuyers(UserId);
            return userManager.SaveUserBuyers(dt);

        }


        public int SaveProductionUnits()
        {


            DataTable dt = ProductionUnitsToUser();

            for (int i = 0; i < cblProductionUnits.Items.Count; i++)
            {
                if (cblProductionUnits.Items[i].Selected)
                {
                    dt.Rows.Add(UserId, cblProductionUnits.Items[i].Value, CommonMethods.SessionInfo.UserName, DateTime.Now);
                }
            }

            userManager.DeleteUserProductionUnits(UserId);
            return userManager.SaveUserProductionUnits(dt);

        }


        public int SaveFinishingUnitFloors()
        {

            DataTable dt = FinishingUnitFloorsToUser();

            for (int i = 0; i < rptFinishingUnits.Items.Count; i++)
            {
                CheckBoxList cblFinishingUnit = (CheckBoxList)rptFinishingUnits.Items[i].FindControl("cblFinishingUnitFloors");


                for (int j = 0; j < cblFinishingUnit.Items.Count; j++)
                {
                    if (cblFinishingUnit.Items[j].Selected)
                    {
                        dt.Rows.Add(UserId, cblFinishingUnit.Items[j].Value, CommonMethods.SessionInfo.UserName, DateTime.Now);
                    }
                }

            }


            userManager.DeleteUserFinishingUnitFloors(UserId);
            return userManager.SaveUserFinishingUnitFloors(dt);

        }




        public DataTable BuyersToUser()
        {
            DataTable dt = new DataTable("BuyersToUser");
            dt.Columns.Add("UId", typeof(int));
            dt.Columns.Add("BId", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }

        public DataTable ProductionUnitsToUser()
        {
            DataTable dt = new DataTable("ProductionUnitsToUser");
            dt.Columns.Add("UId", typeof(int));
            dt.Columns.Add("PUId", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }

        public DataTable FinishingUnitFloorsToUser()
        {
            DataTable dt = new DataTable("FinishingUnitFloorsToUser");
            dt.Columns.Add("UId", typeof(int));
            dt.Columns.Add("FUFId", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }


        private void ClearAll()
        {
            tbxName.Text = string.Empty;
            tbxMobile.Text = string.Empty;
            tbxUserName.Text = string.Empty;
            ddlRoles.ClearSelection();
            tbxpassword.Text = string.Empty;
            tbxConfirmPass.Text = string.Empty;
            tbxEmail.Text = string.Empty;
            ddlDepartments.ClearSelection();
            ddlDesignations.ClearSelection();

        }

        protected void btnSelectAllBuyers_Click(object sender, EventArgs e)
        {
            SelectAllBuyers();
        }

        protected void btnUnselectAllBuyers_Click(object sender, EventArgs e)
        {
            UnSelectAllBuyers();
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            if (cblBuyers.SelectedIndex == -1)
            {
                lblNoBuyerSelected.Visible = true;
                lblNoProdUnitSelected.Visible = false;
                lblNoFinishingUnitSelected.Visible = false;
            }
            else if (cblProductionUnits.SelectedIndex == -1)
            {
                lblNoProdUnitSelected.Visible = true;
                lblNoBuyerSelected.Visible = false;
                lblNoFinishingUnitSelected.Visible = false;
            }
            else if (!HasSelectedFinishingUnitFloors())
            {
                lblNoProdUnitSelected.Visible = false;
                lblNoBuyerSelected.Visible = false;
                lblNoFinishingUnitSelected.Visible = true;
            }
            else
            {
                lblNoBuyerSelected.Visible = false;
                lblNoProdUnitSelected.Visible = false;
                lblNoFinishingUnitSelected.Visible = false;

                int value = userManager.UpdateUser(UserId, tbxMobile.Text, tbxEmail.Text, Convert.ToInt32(ddlRoles.SelectedValue), Convert.ToInt32(ddlDepartments.SelectedValue), Convert.ToInt32(ddlDesignations.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now);
                if (value > 0)
                {
                    int result = SaveBuyers();
                    if (result > 0)
                    {
                        int saveBuyersResult = SaveBuyers();
                        if (saveBuyersResult > 0)
                        {
                            int saveProductionUnitsResult = SaveProductionUnits();

                            if (saveProductionUnitsResult > 0)
                            {
                                int saveFinishingUnitFloorsResult = SaveFinishingUnitFloors();

                                if (saveFinishingUnitFloorsResult > 0)
                                {
                                    var oldUNEvent = unitOfWork.GenericRepositories<UserNotificationEvents>().Get(x => x.UserId == UserId).ToList();
                                    foreach (var item in oldUNEvent)
                                    {
                                        unitOfWork.GenericRepositories<UserNotificationEvents>().Delete(item);
                                    }

                                    foreach (ListItem item in cblEvents.Items)
                                    {
                                        if (item.Selected)
                                        {
                                            var un = new UserNotificationEvents()
                                            {
                                                UserId = UserId,
                                                NotificationEventId = int.Parse(item.Value),
                                                CreatedBy = CommonMethods.SessionInfo.UserName,
                                                CreateDate = DateTime.Now
                                            };
                                            unitOfWork.GenericRepositories<UserNotificationEvents>().Insert(un);
                                        }
                                    }
                                    unitOfWork.Save();
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Finishing unit floors could not be updated.')", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Production Units could not be updated.')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Buyers could not be updated.')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('User update was failed.')", true);
                    }

                }

                //Response.AddHeader("REFRESH", "2;URL=ViewUsers.aspx");
                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewUsers.aspx');", true);
                // ClearAll();
            }

        }


        protected void ddlRoles_SelectedIndexChanged(object sender, EventArgs e)
        {

            UnSelectAllBuyers();
            UnSelectAllFinishingFloors();
            UnSelectAllProductionUnits();
            UnSelectAllNotificationEvents();


            if (ddlRoles.SelectedValue != "")
            {
                if (ddlRoles.SelectedItem.Text.Contains("Merchandiser"))
                {
                    //pnlBuyers.Visible = true;
                    //pnlProductionUnits.Visible = false;
                    //pnlFinsihingUnits.Visible = false;

                    //foreach (ListItem li in cblProductionUnits.Items)
                    //{
                    //    li.Selected = true;
                    //}
                    SelectAllProductionUnits();
                    SelectAllFinishingFloors();

                }
                else if (ddlRoles.SelectedItem.Text.Contains("Knitting"))
                {
                    //pnlProductionUnits.Visible = true;
                    //pnlBuyers.Visible = false;
                    //pnlFinsihingUnits.Visible = false;

                    SelectAllBuyers();
                    SelectAllFinishingFloors();


                }
                else if (ddlRoles.SelectedItem.Text.Contains("Finishing"))
                {
                    //pnlProductionUnits.Visible = false;
                    //pnlBuyers.Visible = false;
                    //pnlFinsihingUnits.Visible = true;

                    SelectAllBuyers();
                    SelectAllProductionUnits();
                }
                else
                {
                    //pnlBuyers.Visible = false;
                    //pnlProductionUnits.Visible = false;
                    //pnlFinsihingUnits.Visible = false;

                    SelectAllBuyers();
                    SelectAllProductionUnits();
                    SelectAllFinishingFloors();
                    SelectAllNotificationEvents();
                }

            }
            else
            {
                pnlBuyers.Visible = false;
                pnlProductionUnits.Visible = false;
                pnlFinsihingUnits.Visible = false;
                pnlNotification.Visible = false;
            }

        }


        protected void rptFinishingUnits_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBoxList finishingFloors = (CheckBoxList)e.Item.FindControl("cblFinishingUnitFloors");
                int finishingUnitId = int.Parse(DataBinder.Eval(e.Item.DataItem, "FinishingUnitId").ToString());
                CommonMethods.LoadFinishingUnitFloorsCheckBoxList(finishingFloors, finishingUnitId);

            }
        }



        private void SelectAllFinishingFloors()
        {

            for (int i = 0; i < rptFinishingUnits.Items.Count; i++)
            {
                CheckBoxList cblFinishingUnit = (CheckBoxList)rptFinishingUnits.Items[i].FindControl("cblFinishingUnitFloors");

                foreach (ListItem li in cblFinishingUnit.Items)
                {
                    li.Selected = true;
                }
            }

        }


        private void UnSelectAllFinishingFloors()
        {

            for (int i = 0; i < rptFinishingUnits.Items.Count; i++)
            {
                CheckBoxList cblFinishingUnit = (CheckBoxList)rptFinishingUnits.Items[i].FindControl("cblFinishingUnitFloors");

                foreach (ListItem li in cblFinishingUnit.Items)
                {
                    li.Selected = false;
                }
            }

        }


        private void SelectAllBuyers()
        {
            foreach (ListItem li in cblBuyers.Items)
            {
                li.Selected = true;
            }
        }

        private void UnSelectAllBuyers()
        {

            foreach (ListItem li in cblBuyers.Items)
            {
                li.Selected = false;
            }
        }



        private void SelectAllProductionUnits()
        {
            foreach (ListItem li in cblProductionUnits.Items)
            {
                li.Selected = true;
            }

        }

        private void UnSelectAllProductionUnits()
        {
            foreach (ListItem li in cblProductionUnits.Items)
            {
                li.Selected = false;
            }

        }

        private void SelectAllNotificationEvents()
        {
            foreach (ListItem li in cblEvents.Items)
            {
                li.Selected = true;
            }

        }

        private void UnSelectAllNotificationEvents()
        {
            foreach (ListItem li in cblEvents.Items)
            {
                li.Selected = false;
            }

        }


        private Boolean HasSelectedFinishingUnitFloors()
        {
            int count = 0;

            for (int i = 0; i < rptFinishingUnits.Items.Count; i++)
            {
                CheckBoxList cblFinishingUnit = (CheckBoxList)rptFinishingUnits.Items[i].FindControl("cblFinishingUnitFloors");

                foreach (ListItem li in cblFinishingUnit.Items)
                {
                    if (li.Selected)
                    {
                        count++;
                    }
                }
            }

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void btnCheckAllNoticiationEvents_Click(object sender, EventArgs e)
        {
            SelectAllNotificationEvents();
        }

        protected void btnUnCheckAllNoticiationEvents_Click(object sender, EventArgs e)
        {
            UnSelectAllNotificationEvents();
        }
    }
}


