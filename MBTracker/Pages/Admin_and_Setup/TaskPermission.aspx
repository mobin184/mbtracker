﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="TaskPermission.aspx.cs" Inherits="MBTracker.Pages.Admin_and_Setup.TaskPermission" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <%--<asp:UpdatePanel ID="Updatepanel1" runat="server">
        <ContentTemplate>--%>
    <style>
        input[type="radio"], input[type="checkbox"] {
            margin: -2px 5px 0px 0;
        }
    </style>
    <script>
        Sys.Application.add_load(load);
    </script>
    <div class="row-fluid">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>Setup Permissions
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal no-margin">
                        <div class="form-horizontal">
                            <div class="control-group">
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                    <asp:Label ID="lblError" runat="server" Font-Bold="True" SkinID="message"></asp:Label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="Label5" runat="server" Text="Role"></asp:Label></label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddlRole" DataTextField="RoleName" DataValueField="Id" runat="server" CssClass="form-control dropdown" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="Label1" runat="server" Text="TaskList"></asp:Label></label>
                                <div class="control-label">
                                    <asp:TreeView ID="TreeView1" runat="server" ShowCheckBoxes="All">
                                    </asp:TreeView>
                                </div>
                            </div>


                            <div class="control-group">
                                <br />
                                <div class="controls-row">
                                    <asp:Button ID="btnRefresh" runat="server" class="btn btn-info pull-right" Text="Refresh" OnClick="btnRefresh_Click" />
                                    <asp:Button ID="btnSave" runat="server" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" CssClass="btn btn-info pull-right" ValidationGroup="save"
                                        OnClick="btnSave_Click" />
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--</ContentTemplate>--%>
    <%-- <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlRole" />
        </Triggers>--%>
    <%--</asp:UpdatePanel>--%>
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
    <script type="text/javascript">
        function load() {
            $("[id*=TreeView1] input[type=checkbox]").on("click", function () {
                var table = $(this).closest("table");
                if (table.next().length > 0 && table.next()[0].tagName == "DIV") {
                    //Is Parent CheckBox
                    var childDiv = table.next();
                    var isChecked = $(this).is(":checked");
                    $("input[type=checkbox]", childDiv).each(function () {
                        if (isChecked) {
                            $(this).prop("checked", true);
                        } else {
                            $(this).prop("checked", false);
                        }
                    });
                } else {
                    //Is Child CheckBox
                    var parentDIV = $(this).closest("DIV");
                    if ($("input[type=checkbox]", parentDIV).length == $("input[type=checkbox]:checked", parentDIV).length) {
                        $("input[type=checkbox]", parentDIV.prev()).prop("checked", true);
                    } else {
                        $("input[type=checkbox]", parentDIV.prev()).prop("checked", false);
                    }
                }
            });
        }

        $(function () {
            load();
        });
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                load();
            });
        };
    </script>
</asp:Content>
