﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;


namespace MBTracker.Pages.Admin_and_Security
{
    public partial class AddNewRole : System.Web.UI.Page
    {

        RoleManager roleManager = new RoleManager();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["roleId"] != null)
                {
                    RoleID = int.Parse(Request.QueryString["roleId"]);

                    PopulateRole(RoleID);
                }
            }
        }

        int RoleID
        {
            set { ViewState["roleID"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["roleID"]);
                }
                catch
                {
                    return 0;
                }
            }
        }



        protected void PopulateRole(int roleId)
        {

            DataTable dt = roleManager.GetRoleById(roleId);
            if (dt.Rows.Count > 0)
            {
                tbxRoleName.Text = dt.Rows[0]["RoleName"].ToString();
                tbxDescription.Text = dt.Rows[0]["Description"].ToString();
            }
            //btnSubmit.Text = " Update ";
            roleActionTitle.Text = "Update Role";
            btnSubmit.Visible = false;
            btnUpdate.Visible = true;
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {

            string actionTaker = CommonMethods.SessionInfo.UserName;

            RoleID = roleManager.InsertRole(tbxRoleName.Text, tbxDescription.Text, actionTaker, DateTime.Now);
            if (RoleID > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Role name already exists. Please try another name')", true);
            }

            ClearAll();


        }


        protected void ClearAll()
        {
            tbxRoleName.Text = "";
            tbxDescription.Text = "";

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string actionTaker = CommonMethods.SessionInfo.UserName;

            int value = roleManager.UpdateRole(RoleID, tbxRoleName.Text, tbxDescription.Text, actionTaker, DateTime.Now);
            if (value > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Role update was failed.')", true);
            }

            //Response.AddHeader("REFRESH", "2;URL=ViewRoles.aspx");
            ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewRoles.aspx');", true);
        }
    }
}