﻿using MBTracker.Code_Folder.Knitting_Parts;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Admin_and_Setup
{
    public partial class AuditLog : System.Web.UI.Page
    {
        PartsManager partManager = new PartsManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               // BindData();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (tbxFromDate.Text != "" && tbxToDate.Text != "")
            {
                try
                {
                    DataTable dt = new DataTable();
                    DateTime dateTime1 = Convert.ToDateTime(tbxFromDate.Text);
                    DateTime dateTime2 = Convert.ToDateTime(tbxToDate.Text);
                    var AuditLogInfoDelete = unitOfWork.GetDataTableFromSql($"DELETE FROM Auditlog WHERE EventDate < DATEADD(MONTH, -5, GETDATE()) ");
                    dt = partManager.AuditLogList(dateTime1, dateTime2);

                    if (dt.Rows.Count > 0)
                    {
                        rptKnittingSpareSuppliers.DataSource = dt;
                        rptKnittingSpareSuppliers.DataBind();
                        lblNoSpareSupplierFound.Visible = false;
                        pnlViewSpareSuppliers.Visible = true;
                    }
                    else
                    {
                        lblNoSpareSupplierFound.Visible = true;
                        pnlViewSpareSuppliers.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
        }

       private void BindData()
        {

            DataTable dt = new DataTable();
            DateTime dateTime1 = Convert.ToDateTime(tbxFromDate.Text);
            DateTime dateTime2 = Convert.ToDateTime(tbxToDate.Text);
            dt = partManager.AuditLogList(dateTime1, dateTime2);

            if (dt.Rows.Count > 0)
            {
                rptKnittingSpareSuppliers.DataSource = dt;
                rptKnittingSpareSuppliers.DataBind();
                lblNoSpareSupplierFound.Visible = false;
                pnlViewSpareSuppliers.Visible = true;
            }
            else
            {
                lblNoSpareSupplierFound.Visible = true;
                pnlViewSpareSuppliers.Visible = false;
            }

        }

    }
}