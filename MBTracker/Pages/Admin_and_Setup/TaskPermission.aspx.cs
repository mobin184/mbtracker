﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;



namespace MBTracker.Pages.Admin_and_Setup
{
    public partial class TaskPermission : System.Web.UI.Page
    {


        MenuTaskManager objTask = new MenuTaskManager();

        RoleManager objRole = new RoleManager();
        public int parentCount = 0;
        public int childCount = 0;
        
        public static int RoleId;


        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CommonMethods.SessionInfo.UserName == null)
                Response.Redirect("~/Pages/Login.aspx");
            
            if (!IsPostBack)
            {
                CommonMethods.LoadDropdown(ddlRole, "UserRoles WHERE IsActive = 1 ORDER BY 2", 1, 0);
                LoadParent();
                LoadTaskToRole();
            }

        }

        protected void LoadParent()
        {
            DataTable dt = objTask.GetTaskParentItemsForPermissions(CommonMethods.SessionInfo.RoleId);
            TreeView1.Nodes.Clear();

            foreach (DataRow dr in dt.Rows)
            {
                TreeNode tnParent = new TreeNode();

                tnParent.Text = dr["TextEng"].ToString();

                tnParent.Value = dr["Id"].ToString();

                //  tnParent.PopulateOnDemand = true;

                tnParent.ToolTip = "Click to get Child";

                tnParent.SelectAction = TreeNodeSelectAction.SelectExpand;

                tnParent.CollapseAll();

                tnParent.Selected = true;

                TreeView1.Nodes.Add(tnParent);

                FillChild(tnParent, Convert.ToInt32(tnParent.Value));
            }
        }


        public void FillChild(TreeNode parent, int ParentId)
        {

            DataTable dtChild = objTask.GetTaskChildItems(ParentId, CommonMethods.SessionInfo.RoleId);
            parent.ChildNodes.Clear();

            foreach (DataRow dr in dtChild.Rows)
            {
                parent.ShowCheckBox = true;
                TreeNode tnChild = new TreeNode();

                tnChild.Text = dr["TextEng"].ToString().Trim();

                tnChild.Value = dr["Id"].ToString().Trim();

                tnChild.ToolTip = "Click to get Child";
                tnChild.SelectAction = TreeNodeSelectAction.SelectExpand;
                tnChild.CollapseAll();
                tnChild.Selected = true;
                parent.ChildNodes.Add(tnChild);

                FillGrandChild(tnChild, Convert.ToInt32(tnChild.Value));

            }

        }

        public void FillGrandChild(TreeNode tnChild, int ParentId)
        {

            DataTable dtChild = objTask.GetTaskChildItems(ParentId, CommonMethods.SessionInfo.RoleId);
            tnChild.ChildNodes.Clear();

            foreach (DataRow dr in dtChild.Rows)
            {
                tnChild.ShowCheckBox = true;
                TreeNode tnGrandChild = new TreeNode();

                tnGrandChild.Text = dr["TextEng"].ToString().Trim();

                tnGrandChild.Value = dr["Id"].ToString().Trim();

                tnGrandChild.ToolTip = "Click to get Child";
                tnGrandChild.SelectAction = TreeNodeSelectAction.SelectExpand;
                tnGrandChild.CollapseAll();
                tnGrandChild.Selected = true;
                tnChild.ChildNodes.Add(tnGrandChild);
            }

        }


        protected void LoadTaskToRole()
        {
            Clear();
            if (ddlRole.SelectedValue != "")
            {
                DataTable dt = objTask.GetTaskParentItemsForPermissions(Convert.ToInt32(ddlRole.SelectedValue));
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        for (int i = 0; i < TreeView1.Nodes.Count; i++)
                        {
                            if (TreeView1.Nodes[i].Value == dr["Id"].ToString())
                            {
                                TreeView1.Nodes[i].Checked = true;
                                DataTable dtChild = objTask.GetTaskChildItems(Convert.ToInt32(dr["Id"].ToString()), Convert.ToInt32(ddlRole.SelectedValue));
                                if (dtChild.Rows.Count > 0)
                                {
                                    foreach (DataRow drc in dtChild.Rows)
                                    {
                                        for (int ii = 0; ii < TreeView1.Nodes[i].ChildNodes.Count; ii++)
                                        {
                                            if (TreeView1.Nodes[i].ChildNodes[ii].Value == drc["TaskId"].ToString())
                                            {
                                                TreeView1.Nodes[i].ChildNodes[ii].Checked = true;
                                                DataTable dtGrandChild = objTask.GetTaskChildItems(Convert.ToInt32(drc["TaskId"].ToString()), Convert.ToInt32(ddlRole.SelectedValue));
                                                if (dtGrandChild.Rows.Count > 0)
                                                {
                                                    foreach (DataRow drgc in dtGrandChild.Rows)
                                                    {
                                                        for (int iii = 0; iii < TreeView1.Nodes[i].ChildNodes[ii].ChildNodes.Count; iii++)
                                                        {
                                                            if (TreeView1.Nodes[i].ChildNodes[ii].ChildNodes[iii].Value == drgc["TaskId"].ToString())
                                                            {
                                                                TreeView1.Nodes[i].ChildNodes[ii].ChildNodes[iii].Checked = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        private void Clear()
        {
            for (int i = 0; i < TreeView1.Nodes.Count; i++)
            {
                TreeView1.Nodes[i].Checked = false;
                for (int ii = 0; ii < TreeView1.Nodes[i].ChildNodes.Count; ii++)
                {
                    TreeView1.Nodes[i].ChildNodes[ii].Checked = false;

                    for (int iii = 0; iii < TreeView1.Nodes[i].ChildNodes[ii].ChildNodes.Count; iii++)
                    {
                        TreeView1.Nodes[i].ChildNodes[ii].ChildNodes[iii].Checked = false;
                    }
                }
            }
        }


        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadTaskToRole();
        }


        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Pages/Admin_and_Setup/TaskPermission.aspx");
        }


        public DataTable TaskToRole()
        {
            DataTable dt = new DataTable("TaskToRole");
            dt.Columns.Add("TId", typeof(int));
            dt.Columns.Add("RId", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(string));
            return dt;
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            objTask.DeleteTaskToRole(Convert.ToInt32(ddlRole.SelectedValue));
            DataTable dt = TaskToRole();
            
            
            for (int i = 0; i < TreeView1.Nodes.Count; i++)
            {
                if (TreeView1.Nodes[i].Checked == true && TreeView1.Nodes[i].ChildNodes.Count > 0)
                {
                    dt.Rows.Add(TreeView1.Nodes[i].Value, Convert.ToInt32(ddlRole.SelectedValue), Page.User.Identity.Name, DateTime.Now);
                    for (int ii = 0; ii < TreeView1.Nodes[i].ChildNodes.Count; ii++)
                    {
                        if (TreeView1.Nodes[i].ChildNodes[ii].Checked == true && TreeView1.Nodes[i].ChildNodes[ii].ChildNodes.Count > 0)
                        {
                            dt.Rows.Add(TreeView1.Nodes[i].ChildNodes[ii].Value, Convert.ToInt32(ddlRole.SelectedValue), Page.User.Identity.Name, DateTime.Now);
                            for (int iii = 0; iii < TreeView1.Nodes[i].ChildNodes[ii].ChildNodes.Count; iii++)
                            {
                                if (TreeView1.Nodes[i].ChildNodes[ii].ChildNodes[iii].Checked == true)
                                {
                                    dt.Rows.Add(TreeView1.Nodes[i].ChildNodes[ii].ChildNodes[iii].Value, Convert.ToInt32(ddlRole.SelectedValue), Page.User.Identity.Name, DateTime.Now);
                                }
                            }
                        }
                        else if (TreeView1.Nodes[i].ChildNodes[ii].Checked == false && TreeView1.Nodes[i].ChildNodes[ii].ChildNodes.Count > 0)
                        {
                            for (int iii = 0; iii < TreeView1.Nodes[i].ChildNodes[ii].ChildNodes.Count; iii++)
                            {
                                if (TreeView1.Nodes[i].ChildNodes[ii].ChildNodes[iii].Checked == true)
                                {
                                    dt.Rows.Add(TreeView1.Nodes[i].ChildNodes[ii].ChildNodes[iii].Value, Convert.ToInt32(ddlRole.SelectedValue), Page.User.Identity.Name, DateTime.Now);
                                    childCount++;
                                }
                            }
                            if (childCount > 0)
                            {
                                dt.Rows.Add(TreeView1.Nodes[i].ChildNodes[ii].Value, Convert.ToInt32(ddlRole.SelectedValue), Page.User.Identity.Name, DateTime.Now);
                                childCount = 0;
                            }
                        }
                        else if (TreeView1.Nodes[i].ChildNodes[ii].Checked == true)
                        {
                            dt.Rows.Add(TreeView1.Nodes[i].ChildNodes[ii].Value, Convert.ToInt32(ddlRole.SelectedValue), Page.User.Identity.Name, DateTime.Now);
                           
                        }
                    }
                }
                else if (TreeView1.Nodes[i].Checked == false && TreeView1.Nodes[i].ChildNodes.Count > 0)
                {
                    for (int ii = 0; ii < TreeView1.Nodes[i].ChildNodes.Count; ii++)
                    {
                        if (TreeView1.Nodes[i].ChildNodes[ii].Checked == true && TreeView1.Nodes[i].ChildNodes[ii].ChildNodes.Count > 0)
                        {
                            dt.Rows.Add(TreeView1.Nodes[i].ChildNodes[ii].Value, Convert.ToInt32(ddlRole.SelectedValue), Page.User.Identity.Name, DateTime.Now);
                            parentCount++;

                            for (int iii = 0; iii < TreeView1.Nodes[i].ChildNodes[ii].ChildNodes.Count; iii++)
                            {
                                if (TreeView1.Nodes[i].ChildNodes[ii].ChildNodes[iii].Checked == true)
                                {
                                    dt.Rows.Add(TreeView1.Nodes[i].ChildNodes[ii].ChildNodes[iii].Value, Convert.ToInt32(ddlRole.SelectedValue), Page.User.Identity.Name, DateTime.Now);
                                }
                            }
                                                    }
                        else if (TreeView1.Nodes[i].ChildNodes[ii].Checked == false && TreeView1.Nodes[i].ChildNodes[ii].ChildNodes.Count > 0)
                        {
                            for (int iii = 0; iii < TreeView1.Nodes[i].ChildNodes[ii].ChildNodes.Count; iii++)
                            {
                                if (TreeView1.Nodes[i].ChildNodes[ii].ChildNodes[iii].Checked == true)
                                {
                                    dt.Rows.Add(TreeView1.Nodes[i].ChildNodes[ii].ChildNodes[iii].Value, Convert.ToInt32(ddlRole.SelectedValue), Page.User.Identity.Name, DateTime.Now);
                                    parentCount++;
                                    childCount++;
                                }
                            }
                            if (childCount > 0)
                            {
                                dt.Rows.Add(TreeView1.Nodes[i].ChildNodes[ii].Value, Convert.ToInt32(ddlRole.SelectedValue), Page.User.Identity.Name, DateTime.Now);
                                childCount = 0;
                            }
                        }
                        else if (TreeView1.Nodes[i].ChildNodes[ii].Checked == true)
                        {
                            dt.Rows.Add(TreeView1.Nodes[i].ChildNodes[ii].Value, Convert.ToInt32(ddlRole.SelectedValue), Page.User.Identity.Name, DateTime.Now);
                            parentCount++;
                        }

                    }
                    if (parentCount > 0)
                    {
                        dt.Rows.Add(TreeView1.Nodes[i].Value, Convert.ToInt32(ddlRole.SelectedValue), Page.User.Identity.Name, DateTime.Now);
                        parentCount = 0;
                    }
                }
                else if (TreeView1.Nodes[i].Checked == true)
                {
                    dt.Rows.Add(TreeView1.Nodes[i].Value, Convert.ToInt32(ddlRole.SelectedValue), Page.User.Identity.Name, DateTime.Now);
                }
            }
            objTask.TaskToRoleInsert(dt);
            ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
        }

    }
}