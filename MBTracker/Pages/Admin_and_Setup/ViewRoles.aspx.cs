﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;



namespace MBTracker.Pages.Admin_and_Setup
{
    public partial class ViewRoles : System.Web.UI.Page
    {

        RoleManager roleManager = new RoleManager();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                BindData();
            }

        }


        protected void BindData()
        {
            DataTable dt = roleManager.GetAllRoles("UserRoles WHERE IsActive = 1");
            if (dt.Rows.Count > 0)
            {
                rptrRole.DataSource = dt;
                rptrRole.DataBind();
            }
            else
            {
                rptrRole.DataSource = null;
                rptrRole.DataBind();
            }
        }


        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            
            int roleId = Convert.ToInt32(e.CommandArgument.ToString());
            Response.Redirect("AddNewRole.aspx?roleId=" + roleId);

        }


        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {

            try
            {
                int roleId = Convert.ToInt32(e.CommandArgument.ToString());
                int result = roleManager.DeleteRole(roleId, CommonMethods.SessionInfo.UserName, DateTime.Now);

                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
            }
            catch (Exception ex)
            {

                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Error - something went wrong.')", true);
            }
            
            BindData();
            
        }



    }
}