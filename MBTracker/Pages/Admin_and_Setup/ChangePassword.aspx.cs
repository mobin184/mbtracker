﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;


namespace MBTracker.Pages.Admin_and_Setup
{
    public partial class ChangePassword : System.Web.UI.Page
    {

        UserManager userManager = new UserManager();

        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (Request.QueryString["UserId"] != null)
                //{
                //    UserId = Request.QueryString["UserId"].ToString();
                //}
                //else
                //{
                //    if (CommonMethods.SessionInfo != null)
                //    {
                //        UserId = CommonMethods.SessionInfo.UserName;
                //    }

                //}

                 tbxResetUser.Text = CommonMethods.SessionInfo.UserName;
                 UserId = CommonMethods.SessionInfo.UserId.ToString();

            }

        }


        string UserId
        {
            set { ViewState["UserId"] = value; }
            get
            {
                try
                {
                    return ViewState["UserId"].ToString();
                }
                catch
                {
                    return "";
                }
            }
        }


        protected void btnResetPassword_Click(object sender, EventArgs e)
        {
            if (tbxPass.Text == tbxConPass.Text && tbxPass.Text != "")
            {

                    int rowAffected = userManager.ChangePassword(Convert.ToInt32(UserId), EncryptionDecryption.Encrypt(tbxPass.Text, true));
                    if (rowAffected > 0)
                    {
                        ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Password change was failed.')", true);
                    }
          
                }

            CommonMethods.SessionInfo = null;

        }
           
   }

}