﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using Repositories;
using MBTracker.EF;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Admin_and_Setup
{
    public partial class AddNewItem : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                EnableDisableDeleteButton();
                BindData();
                //var ddlUnit = (DropDownList)e.Item.FindControl("ddlUnit");
                CommonMethods.LoadDropdown(ddlItemSubType, "YarnAccessoriesItemSubTypes", 1, 0);
                CommonMethods.LoadDropdown(ddlUnit, "Units WHERE IsActive = 1", 1, 0);

            }

        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");

            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("AddNewItem", 2, 1);

        }


        protected void BindData()
        {

            DataTable dt = unitOfWork.GetDataTableFromSql("SELECT Id, ItemName, ISNULL(ItemTypeId,0) ItemTypeId, ISNULL(IsActive,1) IsActive FROM YarnAccessoriesItems");

            if (dt.Rows.Count > 0)
            {
                rptBuyers.DataSource = dt;
                rptBuyers.DataBind();
            }
            else
            {
                rptBuyers.DataSource = null;
                rptBuyers.DataBind();
            }
        }



        int ItemId
        {
            set { ViewState["itemId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["itemId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }


        protected void PopulateItemInfo(int itemId)
        {
            var item = unitOfWork.GenericRepositories<YarnAccessoriesItems>().GetByID(itemId);
            if (item != null)
            {
                tbxItemName.Text = item.ItemName;
                ddlItemType.SelectedValue = item.ItemTypeId + "";
                ddlItemSubType.SelectedValue = ddlItemSubType.Items.FindByValue(item.ItemSubType.ToString()) != null ? item.ItemSubType.ToString() : ddlItemSubType.Items[0].Value;
                ddlUnit.SelectedValue = item.UnitIds + "";
                ddlIsActive.SelectedValue = item.IsActive+"";
                roleActionTitle.Text = "Update Item";
                btnSubmit.Visible = false;
                btnUpdate.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "onEdit()", true);
                
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Invalid item id.')", true);
            }           
        }


        protected void rptBuyer_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {

                ItemId = Convert.ToInt32(e.CommandArgument.ToString());
                PopulateItemInfo(ItemId);
            }
            else if (e.CommandName == "Delete")
            {
                try
                {
                    ItemId = Convert.ToInt32(e.CommandArgument.ToString());
                    var result = unitOfWork.GenericRepositories<YarnAccessoriesItems>().GetByID(ItemId);
                    if(result != null)
                    {
                        unitOfWork.GenericRepositories<YarnAndAccessoriesSuppliers>().Delete(result);
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Invalid item id.')", true);
                    }                 
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Error - something went wrong.')", true);
                }

                BindData();

                //var styleCannotBeDeleted = unitOfWork.GenericRepositories<viewStylesCanNotBeDeleted>().GetByID(BuyerStyleId);
                //if (styleCannotBeDeleted == null)
                //{
                //    var title = "Warning";
                //    var msg = "Are you sure you want to delete?";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                //}
                //else
                //{
                //    var title = "Warning";
                //    var msg = "Sorry, you can not delete it.<br />There was some dependencies of this style.";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                //}
            }
        }



        protected void btnSave_Click(object sender, EventArgs e)
        {

            string actionTaker = CommonMethods.SessionInfo.UserName;
            var itemName = tbxItemName.Text.Trim();
            var itemType = int.Parse(ddlItemType.SelectedValue);
            var ItemSubType = int.Parse(ddlItemSubType.SelectedValue);
            var Unit = int.Parse(ddlUnit.SelectedValue == "" ? "17" : ddlUnit.SelectedValue);
            //var Unit = int.Parse(ddlUnit.SelectedValue);
            var isActive = int.Parse(ddlIsActive.SelectedValue);

            var isExist = unitOfWork.GenericRepositories<YarnAccessoriesItems>().IsExist(x => x.ItemName == itemName && x.ItemTypeId == itemType);

            if (!isExist)
            {
                string user = CommonMethods.SessionInfo.UserName;
                var newitem = new YarnAccessoriesItems()
                {
                    ItemName = itemName,
                    ItemTypeId = itemType,
                    ItemSubType = ItemSubType,
                    UnitIds = Unit,
                    IsActive = isActive,
                    CreatedBy = user,
                    CreateDate = DateTime.Now
                };
                unitOfWork.GenericRepositories<YarnAccessoriesItems>().Insert(newitem);
                unitOfWork.Save();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('item already exists.')", true);
            }

            ClearAll();
            BindData();

        }


        protected void ClearAll()
        {
            tbxItemName.Text = "";
            ddlIsActive.SelectedValue = "1";
            ddlItemType.SelectedValue = "";
            ddlItemSubType.SelectedValue = "";
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string actionTaker = CommonMethods.SessionInfo.UserName;

            var item = unitOfWork.GenericRepositories<YarnAccessoriesItems>().GetByID(ItemId);
            if (item != null)
            {
                item.ItemName = tbxItemName.Text.Trim();
                item.IsActive = int.Parse(ddlIsActive.SelectedValue);
                item.ItemTypeId = int.Parse(ddlItemType.SelectedValue);
                item.ItemSubType = int.Parse(ddlItemSubType.SelectedValue);
                item.UnitIds = int.Parse(ddlUnit.SelectedValue == "" ? "17" : ddlUnit.SelectedValue);
                item.UpdatedBy = actionTaker;
                item.UpdateDate = DateTime.Now;

                unitOfWork.GenericRepositories<YarnAccessoriesItems>().Update(item);
                unitOfWork.Save();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Item update was failed.')", true);
            }

            //Response.AddHeader("REFRESH", "2;URL=ViewRoles.aspx");
            ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('AddNewItem.aspx');", true);
        }


    }
}