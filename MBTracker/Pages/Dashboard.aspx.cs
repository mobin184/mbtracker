﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using Repositories;
using MBTracker.EF;


namespace MBTracker.Pages
{
    public partial class Dashboard : System.Web.UI.Page
    {

        DataTable dtOrderStatus;
        DataTable dtOrders;
        DataTable dtSizeInfo;
        DataTable dtOrderColors;
        int shipmentDateId;
        int buyerColorId;
        int deliveryCountryId;

        OrderManager orderManager = new OrderManager();
        CommonManager commonManager = new CommonManager();
        BuyerManager buyerManager = new BuyerManager();

        UnitOfWork unitOfWork = new UnitOfWork();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                var user = CommonMethods.SessionInfo;

                if (user != null)
                {                  

                    if (Request.QueryString["showInfo"] != null)
                    {
                        var orderId = int.Parse(Tools.UrlDecode(Request.QueryString["showInfo"]));
                        var orderInfo = unitOfWork.GenericRepositories<Orders>().GetByID(orderId);
                       
                        divSearch.Visible = false;
                        listOfOrder.Visible = false;

                        if (CommonMethods.HasBuyerAssignedToUser(orderInfo.BuyerId, CommonMethods.SessionInfo.UserId))
                        {
                            var shipmentYear = DateTime.Now.Year;
                            var minShipment = unitOfWork.GenericRepositories<OrderShipmentDates>().Get(x => x.OrderId == orderId).ToList();
                            if (minShipment.Count() > 0)
                            {
                                shipmentYear = minShipment.Min(x => x.ShipmentDate).Year;
                            }
                            viewDtails(orderId, orderInfo.StyleId, shipmentYear);
                        }
                        else
                        {
                            lblNotAuthorizedToView.Visible = true;
                         
                        }
                    }
                    else
                    {
                        listOfOrder.Visible = true;
                        BindData("Shipmentsin60days");
                    }
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }

            //if (Request.QueryString["Result"] == "NotAuthorized")
            //{

            //    ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('You are not Authorized to access the page!')", true);

            //}

        }

        protected void viewDtails(int orderId, int styleId, int shipmentYear)
        {
            PupulateOrderForView(orderId);
            //pnlDetails.Visible = true;
            //pnlDetails2.Visible = true;

            // lblColorInfoDetails.Text = $"Color & Size Information <br />Order Number: <span style=''>{orderNumber}</span><br /> Buyer: <span style=''>{buyerName}</span> <br /> Style:<span style=''>{style}</span>";
            //lblColorInfoDetails.Visible = true;
            //  lblOrderInfo.Text = $"Details information <br />Order Number: <span style=''>{orderNumber}</span> <br /> Buyer: <span style=''>{buyerName}</span><br /> Style:<span style=''>{style}</span>";


            pnlColorDeliveryCountryAndQuantity.Visible = true;

            LoadOrderColors(orderId);
            PopulateShipmentDates(orderId);
            BindStyleInfoLinks(styleId, shipmentYear);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);
        }


        string OrderDisplayOption
        {
            set { ViewState["orderDisplayOption"] = value; }
            get
            {
                try
                {
                    return ViewState["orderDisplayOption"].ToString();
                }
                catch
                {
                    return "";
                }
            }
        }

        int BuyerId
        {
            set { ViewState["buyerId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["buyerId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        int OrderId
        {
            set { ViewState["orderId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["orderId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }


        string StyleName
        {
            set { ViewState["styleName"] = value; }
            get
            {
                try
                {
                    return ViewState["styleName"].ToString();
                }
                catch
                {
                    return "";
                }
            }
        }



        protected void BindData(string selectedValue)
        {

            if (selectedValue == "Buyer")
            {
                if (ddlBuyers.SelectedValue != "")
                {
                    dtOrders = orderManager.GetOrdersByBuyerId(Convert.ToInt32(ddlBuyers.SelectedValue));
                    LoadDataToControl();
                    LabelWidgetHeader.Text = "Orders of Buyer: " + ddlBuyers.SelectedItem.Text;
                }
                else
                {
                    if (BuyerId != 0)
                    {
                        dtOrders = orderManager.GetOrdersByBuyerId(BuyerId);
                        LoadDataToControl();

                        DataTable dt = buyerManager.GetBuyerById(BuyerId);
                        LabelWidgetHeader.Text = "Orders of Buyer: " + dt.Rows[0][1].ToString();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('You forgot to select a buyer!')", true);
                    }
                   
                }
            }
            else if (selectedValue == "Style")
            {

                if (tbxStyle.Text != "")
                {
                    dtOrders = orderManager.GetOrdersByStyleName(tbxStyle.Text, CommonMethods.SessionInfo.UserId);
                    LoadDataToControl();
                    LabelWidgetHeader.Text = "Orders of Style: " + tbxStyle.Text;
                }
                else
                {

                    if (StyleName != "")
                    {
                        dtOrders = orderManager.GetOrdersByStyleName(StyleName, CommonMethods.SessionInfo.UserId);
                        LoadDataToControl();
                        LabelWidgetHeader.Text = "Orders of Style: " + StyleName;

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('You forget to enter style!')", true);
                    }

                    
                }
                
            }
            else
            {
                dtOrders = orderManager.GetOrders(CommonMethods.SessionInfo.UserId);
                LoadDataToControl();
                LabelWidgetHeader.Text = "Orders - Shipments in 60 days:";
            }
        } 


        protected void LoadDataToControl()
        {

            if (dtOrders.Rows.Count > 0)
            {
                dtOrderStatus = commonManager.LoadOrderStatusDropdown(CommonMethods.SessionInfo.DepartmentId);
                rptOrders.DataSource = dtOrders;
                rptOrders.DataBind();
                lblNoOrderFound.Visible = false;
            }
            else
            {
                rptOrders.DataSource = null;
                rptOrders.DataBind();
                lblNoOrderFound.Visible = true;

            }

        }


            protected void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlOrderStatus = (DropDownList)e.Item.FindControl("ddlOrderStatus");
                ddlOrderStatus.DataTextField = "OrderStatusName";
                ddlOrderStatus.DataValueField = "Id";
                ddlOrderStatus.DataSource = dtOrderStatus;
                ddlOrderStatus.DataBind();

                ddlOrderStatus.SelectedValue = DataBinder.Eval(e.Item.DataItem, "StatusId").ToString();
            }
        }


        protected void rptOrders_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {
                e.Item.FindControl("lblOrderStatus").Visible = false;
                e.Item.FindControl("ddlOrderStatus").Visible = true;
                e.Item.FindControl("lnkbtnEdit").Visible = false;
                e.Item.FindControl("lnkbtnUpdate").Visible = true;
                e.Item.FindControl("lnkbtnCancel").Visible = true;

            }
            else if (e.CommandName == "Update")
            {
                int orderId = Convert.ToInt32(e.CommandArgument.ToString());
                DropDownList ddlOrderStatus = (DropDownList)e.Item.FindControl("ddlOrderStatus");
                int statusId = Convert.ToInt32(ddlOrderStatus.SelectedValue);

                UpdateOrderStatus(orderId, statusId);
                BindData(OrderDisplayOption);

            }
            else if (e.CommandName == "Cancel")
            {
                BindData(OrderDisplayOption);
            }

        }


        private void UpdateOrderStatus(int orderId, int statusId)
        {

            int updateResult = orderManager.UpdateOrderStatus(orderId, statusId, CommonMethods.SessionInfo.UserName, DateTime.Now);
            if (updateResult > 0)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
            }
        }


        protected void ddlOrderDisplayOptions_SelectedIndexChanged(object sender, EventArgs e)
        {

            pnlOrderSearch.Visible = false;
            ddlBuyers.Visible = false;
            tbxStyle.Visible = false;
            lblDisplayInput.Visible = false;
            //btnViewOrders.Visible = false;

            if (ddlOrderDisplayOptions.SelectedValue == "Buyer")
            {
                lblDisplayInput.Text = "Select buyer: ";
                lblDisplayInput.Visible = true;
                ddlBuyers.Visible = true;
                //btnViewOrders.Visible = true;
                pnlOrderSearch.Visible = true;
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);


            }
            else if (ddlOrderDisplayOptions.SelectedValue == "Style")
            {
                lblDisplayInput.Text = "Enter style: ";
                lblDisplayInput.Visible = true;
                tbxStyle.Visible = true;
                //btnViewOrders.Visible = true;
                pnlOrderSearch.Visible = true;
            }
            else
            {
                BindData(ddlOrderDisplayOptions.SelectedValue);
                OrderDisplayOption = ddlOrderDisplayOptions.SelectedValue;
            }


        }

        protected void btnViewOrders2_Click(object sender, EventArgs e)
        {
            OrderDisplayOption = ddlOrderDisplayOptions.SelectedValue;

            if (OrderDisplayOption == "Buyer")
            {
                if(ddlBuyers.SelectedValue != "")
                {
                    BuyerId = Convert.ToInt32(ddlBuyers.SelectedValue);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('You forgot to select a buyer!')", true);
                }                
            }
            else if (OrderDisplayOption == "Style")
            {
                StyleName = tbxStyle.Text;
            }
  


            BindData(ddlOrderDisplayOptions.SelectedValue);
            pnlOrderSearch.Visible = false;
            ddlOrderDisplayOptions.SelectedIndex = 0;
        }

        protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        {
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            int orderId = int.Parse(commandArgs[0]);
            var buyerName = commandArgs[1];
            var styleName = commandArgs[2];
            var orderNumber = commandArgs[3];
            int shipmentYear = DateTime.Parse(commandArgs[4]).Year;
            int styleId = int.Parse(commandArgs[5]);

            //int orderId = Convert.ToInt32(e.CommandArgument.ToString());
            PupulateOrderForView(orderId);
            //pnlDetails.Visible = true;
            //pnlDetails2.Visible = true;
    
            // lblColorInfoDetails.Text = $"Color & Size Information <br />Order Number: <span style=''>{orderNumber}</span><br /> Buyer: <span style=''>{buyerName}</span> <br /> Style:<span style=''>{style}</span>";
            //lblColorInfoDetails.Visible = true;
            //  lblOrderInfo.Text = $"Details information <br />Order Number: <span style=''>{orderNumber}</span> <br /> Buyer: <span style=''>{buyerName}</span><br /> Style:<span style=''>{style}</span>";


            pnlColorDeliveryCountryAndQuantity.Visible = true;
           
            //LoadOrderColors(orderId);
            PopulateShipmentDates(orderId);
            BindStyleInfoLinks(styleId, shipmentYear);




            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);
        }


        private void BindStyleInfoLinks(int styleId, int shipmentYear)
        {
            divBookingDetails.Visible = true;
            divActivePOs.Visible = true;
            divConsumptionInfo.Visible = true;
            divYarnReceive.Visible = true;
            divKnittingPlans.Visible = true;
            divFinishingPlans.Visible = true;
            divYarnIssued.Visible = true;
            divProductionStatus.Visible = true;
            divKnittingStatus.Visible = true;



            divBookingDetails.InnerHtml = $"<div &nbsp;&nbsp;<asp:LinkButton ID='lnkbtnView' onClick='window.open(\"../Pages/Reports/BookingByBuyerAndStyle.aspx?bookingYear={Tools.UrlEncode(shipmentYear.ToString() + "")}\",\"_blank\")' class='btn btn-success btn-mini hidden-phone pull-left' Text=''>Veiw Details</asp:LinkButton></div>";
            divActivePOs.InnerHtml = $"<div &nbsp;&nbsp;<asp:LinkButton ID='lnkbtnView' onClick='window.open(\"../Pages/Reports/SummaryOfOrderColors.aspx?styleId={Tools.UrlEncode(styleId + "")}\",\"_blank\")' class='btn btn-success btn-mini hidden-phone pull-left' Text='View Details'>View Details</asp:LinkButton></div>";
            divConsumptionInfo.InnerHtml = $"<div &nbsp;&nbsp;<asp:LinkButton ID='lnkbtnView' onClick='window.open(\"../Pages/Buyers_and_Orders/ViewConsumptionSheets.aspx?styleId={Tools.UrlEncode(styleId + "")}\",\"_blank\")' class='btn btn-success btn-mini hidden-phone pull-left' Text='View Details'>View Details</asp:LinkButton></div>";
            divYarnReceive.InnerHtml = $"<div &nbsp;&nbsp;<asp:LinkButton ID='lnkbtnView' onClick='window.open(\"../Pages/Reports/YarnReceivedHistoryAtStore.aspx?styleId={Tools.UrlEncode(styleId + "")}\",\"_blank\")' class='btn btn-success btn-mini hidden-phone pull-left' Text='View Details'>View Details</asp:LinkButton></div>";


            divKnittingPlans.InnerHtml = $"<div &nbsp;&nbsp;<asp:LinkButton ID='lnkbtnView' onClick='window.open(\"../Pages/Productions/ViewKnittingPlansV2.aspx?styleId={Tools.UrlEncode(styleId + "")}\",\"_blank\")' class='btn btn-success btn-mini hidden-phone pull-left' Text='View Details'>View Details</asp:LinkButton></div>";

            divFinishingPlans.InnerHtml = $"<div &nbsp;&nbsp;<asp:LinkButton ID='lnkbtnView' onClick='window.open(\"../Pages/Productions/ViewFinishingPlans.aspx?styleId={Tools.UrlEncode(styleId + "")}\",\"_blank\")' class='btn btn-success btn-mini hidden-phone pull-left' Text='View Details'>View Details</asp:LinkButton></div>";

            divYarnIssued.InnerHtml = $"<div &nbsp;&nbsp;<asp:LinkButton ID='lnkbtnView' onClick='window.open(\"../Pages/Reports/YarnIssuedByStyleAndColors.aspx?styleId={Tools.UrlEncode(styleId + "")}\",\"_blank\")' class='btn btn-success btn-mini hidden-phone pull-left' Text='View Details'>View Details</asp:LinkButton></div>";

            divProductionStatus.InnerHtml = $"<div &nbsp;&nbsp;<asp:LinkButton ID='lnkbtnView' onClick='window.open(\"../Pages/Reports/DetailsOfKnittedItemsDeliveredByStyle.aspx?styleId={Tools.UrlEncode(styleId + "")}\",\"_blank\")' class='btn btn-success btn-mini hidden-phone pull-left' Text='View Details'>View Details</asp:LinkButton></div>";
            divKnittingStatus.InnerHtml = $"<div &nbsp;&nbsp;<asp:LinkButton ID='lnkbtnView' onClick='window.open(\"../Pages/Reports/DetailsOfKnittedItemsByStyle.aspx?styleId={Tools.UrlEncode(styleId + "")}\",\"_blank\")' class='btn btn-success btn-mini hidden-phone pull-left' Text='View Details'>View Details</asp:LinkButton></div>";


        }



        private void PupulateOrderForView(int orderId)
        {
            //unitOfWork = new UnitOfWork(); 
            var orderInfo = unitOfWork.GenericRepositories<Orders>().GetByID(orderId);
            //var orderColors = unitOfWork.GenericRepositories<OrderColors>().Get(x => x.OrderId == orderId).ToList(); ;
            //var dtColors = Tools.ToDataTable(orderColors);
            var dtColors = orderManager.GetOrderColorsByOrderId(orderId);
            var dtBasicInfo = unitOfWork.GetDataTableFromSql($"Exec usp_GetOrderInfoByOrderId {orderInfo.Id}");
            var dtOrderDocuments = unitOfWork.GetDataTableFromSql($"SELECT Id, FileName FROM OrderDocuments WHERE OrderId={orderInfo.Id}");
            BuyerId = orderInfo.BuyerId;
            OrderId = orderInfo.Id;

            var dtCountries = unitOfWork.GetDataTableFromSql($"EXEC usp_GetDeliveryCountries_ByOrderId {orderId}");
            //SetDeliveryCounties(dtCountries);
            SetBasicInfo(dtBasicInfo);
            //PopulateOrderDocuments(dtOrderDocuments);

            loadSizeInfo(orderId);

            if (dtColors.Rows.Count > 0)
            {
                rptColorInfo.DataSource = dtColors;
                rptColorInfo.DataBind();
                lblNoDataFoundForColor.Visible = false;
                rptColorInfo.Visible = true;
            }
            else
            {
                rptColorInfo.Visible = false;
                lblNoDataFoundForColor.Visible = true;
            }
        }

        //private void SetDeliveryCounties(DataTable dtCountries)
        //{
        //    lblDeliveryCountry.Visible = true;
        //    if (dtCountries.Rows.Count > 0)
        //    {
        //        rptDeliveryCountries.DataSource = dtCountries;
        //        rptDeliveryCountries.DataBind();
        //        rptDeliveryCountries.Visible = true;
        //        lblNoCountryFound.Visible = false;
        //    }
        //    else
        //    {
        //        rptDeliveryCountries.DataSource = null;
        //        rptDeliveryCountries.DataBind();
        //        rptDeliveryCountries.Visible = false;
        //        lblNoCountryFound.Visible = true;
        //    }
        //}

        private void SetBasicInfo(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                lblOrderInfo.Visible = true;
                divBasicInfo.Visible = true;
                //lblBasicInfoNotFound.Visible = false;
                lblBuyer.Text = dt.Rows[0].Field<string>("BuyerName");
                lblDepartment.Text = dt.Rows[0].Field<string>("DepartmentName");
                lblStyle.Text = dt.Rows[0].Field<string>("StyleName");
                lblSeason.Text = dt.Rows[0].Field<string>("SeasonName");
                lblYear.Text = dt.Rows[0].Field<string>("SeasonYear");
                lblOrderNumber.Text = dt.Rows[0].Field<string>("OrderNumber");
                lblOrderDate.Text = dt.Rows[0].Field<DateTime>("OrderDate").ToString("dd-MM-yyyy");
                lblOrderItem.Text = dt.Rows[0].Field<string>("OrderItem");
                lblLcVlaue.Text = dt.Rows[0].Field<Decimal>("LCValue").ToString();
                lblOrderStatus.Text = dt.Rows[0].Field<string>("OrderStatusName");
                //lblDeleveryCountry.Text = dt.Rows[0].Field<string>("DeleveryCountry");
                lblPaymentTerm.Text = dt.Rows[0].Field<string>("PaymentTerms");
            }
            else
            {
                lblOrderInfo.Visible = true;
                divBasicInfo.Visible = false;
                //lblBasicInfoNotFound.Visible = true;
            }

        }


        private void PopulateOrderDocuments(DataTable dtOrderDocuments)
        {
            //lblOrderDocuments.Visible = true;
            //if (dtOrderDocuments.Rows.Count > 0)
            //{
            //    lblNoDocumnet.Visible = false;
            //    rptOrderDocument.Visible = true;
            //    rptOrderDocument.DataSource = dtOrderDocuments;
            //    rptOrderDocument.DataBind();
            //}
            //else
            //{
            //    lblNoDocumnet.Visible = true;
            //    rptOrderDocument.Visible = false;
            //}
        }


        private void loadSizeInfo(int orderId)
        {
            //dtSizeInfo = buyerManager.GetSizes(BuyerId);

            dtSizeInfo = buyerManager.GetOrderSizes(orderId);


        }

        protected void gvSizeInfo_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                loadSizeInfo(OrderId);

                var unitOfWork = new UnitOfWork();
                Label lblSizeId = null;
                Label lblSizeQty = null;
                for (int i = 0; i < dtSizeInfo.Rows.Count; i++)
                {
                    lblSizeQty = new Label();
                    lblSizeQty.ID = "txtSizeQty2" + i.ToString();
                    lblSizeQty.Text = "";
                    lblSizeQty.Width = 60;



                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId2" + i.ToString();
                    lblSizeId.Text = dtSizeInfo.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);

                    //var lblOrderId = (Label)((GridView)sender).DataItemContainer.FindControl("lblOrderId");
                    var lblOrderColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblColorId");
                    //var orderId = int.Parse(lblOrderId.Text);
                    var colorId = int.Parse(lblOrderColorId.Text);
                    var sizeId = int.Parse(dtSizeInfo.Rows[i][0].ToString());

                    //var pInfo = unitOfWork.GenericRepositories<OrderColorSizes>().Get(x => x.OrderColorId == colorId && x.SizeId == sizeId).FirstOrDefault();
                    //if (pInfo != null)
                    //{
                    //    lblSizeQty.Text = pInfo.Quantity + "";
                    //}
                    e.Row.Cells[i].Controls.Add(lblSizeQty);
                }
            }

        }


        protected void rptColorInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    var gvColorSizes = (GridView)e.Item.FindControl("gvSizeInfo");
            //    DataTable dtOneRow = new DataTable();
            //    DataRow dr = null;
            //    dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            //    dr = dtOneRow.NewRow();
            //    dr["RowNumber"] = 1;
            //    dtOneRow.Rows.Add(dr);
            //    loadSizeInfo();
            //    if (dtSizeInfo.Rows.Count > 0)
            //    {
            //        TemplateField templateField = null;

            //        for (int i = 0; i < dtSizeInfo.Rows.Count; i++)
            //        {
            //            templateField = new TemplateField();
            //            templateField.HeaderText = dtSizeInfo.Rows[i][1].ToString();
            //            gvColorSizes.Columns.Add(templateField);

            //        }
            //    }
            //    gvColorSizes.DataSource = dtOneRow;
            //    gvColorSizes.DataBind();
            //}
        }


        private void LoadOrderColors(int orderId)
        {
            dtOrderColors = orderManager.GetOrderColorsByOrderId(orderId);
        }

        private void PopulateShipmentDates(int orderId)
        {
            DataTable dt = new DataTable();

            dt = orderManager.GetOrderShipmentDatesByOrderId(orderId);

            if (dt.Rows.Count > 0)
            {
                rptOrderShipmentDates.DataSource = dt;
                rptOrderShipmentDates.DataBind();
                rptOrderShipmentDates.Visible = true;
                pnlColorDeliveryCountryAndQuantity.Visible = true;
            }
            else
            {
                rptOrderShipmentDates.DataSource = null;
                rptOrderShipmentDates.DataBind();
                rptOrderShipmentDates.Visible = false;
                pnlColorDeliveryCountryAndQuantity.Visible = false;
            }

        }

        protected void rptOrderShipmentDates_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptOrderColors = (Repeater)e.Item.FindControl("rptOrderColors");
                Label lblShipmentDateId = (Label)e.Item.FindControl("lblShipmenDatetId");
                shipmentDateId = Convert.ToInt32(lblShipmentDateId.Text);

                DataTable dtShipmentColors = orderManager.GetShipmentColorsByShipmentDateId(shipmentDateId);

                if (dtShipmentColors.Rows.Count > 0)
                {
                    rptOrderColors.DataSource = dtShipmentColors;
                    rptOrderColors.DataBind();
                }
            }
        }

        protected void rptOrderColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptShipmentCountry = (Repeater)e.Item.FindControl("rptShipmentCountry");

                Label lblOrderColorId = (Label)e.Item.FindControl("lblOrderColorId");
                buyerColorId = Convert.ToInt32(lblOrderColorId.Text);

                DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetDeliveryCountries_ByShipmentAndColorForView {shipmentDateId},{buyerColorId}");
                //orderManager.GetShipmentCountriesByDateAndColor(shipmentDateId, BuyerColorId);

                if (dt.Rows.Count > 0)
                {
                    rptShipmentCountry.DataSource = dt;
                    rptShipmentCountry.DataBind();
                }
            }
        }

        protected void rptShipmentCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                //Repeater rptSizeAndQty = (Repeater)e.Item.FindControl("rptSizeAndQty"); //

                Label lblDeliveryCountryId = (Label)e.Item.FindControl("lblDeliveryCountryId");
                deliveryCountryId = Convert.ToInt32(lblDeliveryCountryId.Text);

                //DataTable dt = orderManager.GetSizesAndQuantityByShipmentColorCountry(shipmentDateId, OrderColorId, deliveryCountryId); //

                //if (dt.Rows.Count > 0)
                //{
                //    rptSizeAndQty.DataSource = dt;
                //    rptSizeAndQty.DataBind();
                //}
                DataTable dtOneRow = new DataTable();
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));


                DataTable dt = orderManager.GetSizesAndQuantityByShipmentColorCountry(shipmentDateId, buyerColorId, deliveryCountryId);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dtOneRow.Columns.Add(new DataColumn(dt.Rows[i][0].ToString(), typeof(string)));
                }
                DataRow dr = null;

                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                GridView gvSizeAndQty = (GridView)e.Item.FindControl("gvSizeAndQty");
                if (dt.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dt.Rows[i][0].ToString();
                        gvSizeAndQty.Columns.Add(templateField);
                        dr[dt.Rows[i][0].ToString()] = dt.Rows[i][1].ToString();
                    }
                }

                dtOneRow.Rows.Add(dr);
                gvSizeAndQty.DataSource = dtOneRow;
                gvSizeAndQty.DataBind();

            }
        }

        protected void gvSizeAndQty_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //var index = sende
                Label lblSizeQuantity = null;
                //LoadYarnIssueForSizes();
                var dtSizes = (DataTable)((GridView)sender).DataSource;
                if (dtSizes != null && dtSizes.Rows.Count > 0)
                {
                    for (int i = 1; i < dtSizes.Columns.Count; i++)
                    {

                        lblSizeQuantity = new Label();
                        lblSizeQuantity.ID = "lblSizeQuantity" + i.ToString();
                        lblSizeQuantity.Text = int.Parse(dtSizes.Rows[0][i].ToString()) > 0 ? dtSizes.Rows[0][i].ToString() : "";
                        lblSizeQuantity.Width = 50;
                        e.Row.Cells[i - 1].Controls.Add(lblSizeQuantity);

                    }
                }

            }
        }



    }

}