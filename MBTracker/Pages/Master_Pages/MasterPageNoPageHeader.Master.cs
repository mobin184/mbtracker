﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Master_Pages
{
    public partial class MasterPageNoPageHeader : System.Web.UI.MasterPage
    {

        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_Init(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                var user = CommonMethods.SessionInfo;
                if (user == null)
                {
                    HttpContext.Current.Session["returnUrl"] = HttpContext.Current.Request.Url.PathAndQuery;
                    Response.Redirect("~/Login.aspx");
                }


                //string url = Request.Url.AbsoluteUri;
                //string[] parts = url.Split(new string[] { "Pages" }, StringSplitOptions.None);
                //string pageUrl;

                //if (parts[1].Contains("?"))
                //{
                //    string[] updatedParts = parts[1].Split(new string[] { "?" }, StringSplitOptions.None);
                //    pageUrl = "/Pages" + updatedParts[0];
                //}
                //else
                //{
                //    pageUrl = "/Pages" + parts[1];
                //}

                string url = Request.Url.AbsolutePath;


                var task = unitOfWork.GenericRepositories<TaskItems>().Get(x => x.URL == url).FirstOrDefault();


                if (task != null && user != null)
                {
                    var permission = unitOfWork.GenericRepositories<TaskRolePermissions>().Get(x => x.TaskId == task.Id && x.RoleId == user.RoleId).ToList();


                    if (!permission.Any())
                    {
                        if (url.Contains("DashBoard"))
                        {
                            var rolePermisison = unitOfWork.GenericRepositories<RolesDashboard>().Get(x => x.RoleId == CommonMethods.SessionInfo.RoleId).FirstOrDefault();
                            if (rolePermisison != null)
                            {
                                Response.Redirect(rolePermisison.Dashboards.DashboardURL);
                            }
                            else
                            {
                                Response.Redirect("~/Pages/Dashboard.aspx");
                            }
                            //if (CommonMethods.SessionInfo.RoleName.Contains("Knitting") || CommonMethods.SessionInfo.RoleName.Contains("Store"))
                            //{
                            //    Response.Redirect("~/Pages/DashboardKnitting.aspx");
                            //}
                            //else if (CommonMethods.SessionInfo.RoleName.Contains("Finishing"))
                            //{
                            //    Response.Redirect("~/Pages/DashboardFinishing.aspx");
                            //}
                            //else
                            //{
                            //    Response.Redirect("~/Pages/Dashboard.aspx");
                            //}
                        }
                        else
                        {
                            Response.Redirect("~/Pages/AccessDenied.aspx", false);
                        }
                    }
                }
            }
        }

    }
}