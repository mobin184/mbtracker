﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AccessDenied.aspx.cs" Inherits="MBTracker.Pages.AccessDenied" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="span4"></div>
        <div class="span4">
            <h1><font face="NewRoman"> Access Denied!</font></h1>
            <h6><font face="NewRoman"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You are not authorised to access the page.</font></h6>
            </div>

        <div class="span4"></div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
