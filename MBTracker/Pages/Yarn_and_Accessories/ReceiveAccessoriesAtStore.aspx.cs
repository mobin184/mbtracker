﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class ReceiveAccessoriesAtStore : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        decimal totalGoods = 0;
        decimal grandTotalAmount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadYarnAndAccessoriesSupplierDropdown(ddlYarnAndAccssSuppliers, 1, 0);
                CommonMethods.LoadYarnAndAccessoriesPurchaseType(ddlPurchaseType, 1, 0);
                ddlPurchaseType.Items.RemoveAt(1);
                LoadStore();
                LoadMerchandiserList();
                //CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                if (Request.QueryString["receivedId"] != null)
                {
                    ReceivedId = int.Parse(Tools.UrlDecode(Request.QueryString["receivedId"]));
                    var recInfo = unitOfWork.GenericRepositories<AccessoriesReceivedAtStore>().Get(x => x.AccessoriesReceivedId == ReceivedId).FirstOrDefault();
                    tbxReceiveDate.Text = recInfo.ReceiveDate.ToString("yyyy-MM-dd");
                    ddlYarnAndAccssSuppliers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlYarnAndAccssSuppliers, recInfo.SupplierId);
                    ddlPurchaseType.SelectedIndex = CommonMethods.MatchDropDownItem(ddlPurchaseType, recInfo.PurchaseType);
                    ddlStore.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStore, recInfo.StoreId);
                    ddlMerchandiser.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMerchandiser, recInfo.MerchandiserId ?? 0);
                    tbxInvoiceNumber.Text = recInfo.InvoiceNumber;
                    if (recInfo.PurchaseType == 2)
                    {
                        tbxDyeingOrderNumber.Text = recInfo.DyeingOrderNumber;
                        dyeingOrderDiv.Visible = true;
                        lcDiv.Visible = false;
                        lnkBtnSave.Visible = false;
                        lnkBtnUpdate.Visible = true;


                        //var itemDetails = unitOfWork.GenericRepositories<YarnReceivedAtStoreItems>().Get(x => x.YarnReceivedId == recInfo.YarnReceivedId).ToList();
                        var itemDetails = unitOfWork.GetDataTableFromSql($"SELECT CAST(BuyerId as nvarchar(max)) BuyerId,CAST(StyleId as nvarchar(max)) as StyleId,CAST(ItemId as nvarchar(max)) as ItemId,LOTNumber,CAST(ReceivedQty as nvarchar(max))  as Quantity FROM AccessoriesReceivedAtStoreItems WHERE AccessoriesReceivedId = {recInfo.AccessoriesReceivedId}");
                        ViewState["CurrentTable"] = itemDetails;
                        rptItemRecDyeingEntryInfo.DataSource = itemDetails;
                        rptItemRecDyeingEntryInfo.DataBind();
                        SetPreviousDataDyeingOrder();
                        pnlDetails.Visible = false;
                        divItemInfo.Visible = false;
                        divItemInfoDyeingOrder.Visible = true;
                    }
                    else
                    {
                        CommonMethods.LoadLCBySupplier(ddlLCNumber, 1, 0, recInfo.SupplierId);
                        ddlLCNumber.SelectedIndex = CommonMethods.MatchDropDownItem(ddlLCNumber, recInfo.LCId ?? 0);
                        ShowPIInformationByLCId(recInfo.LCId ?? 0);
                        pnlDetails.Visible = true;
                        divItemInfo.Visible = true;
                        divItemInfoDyeingOrder.Visible = false;

                        var itemDetail = unitOfWork.GetDataTableFromSql($"SELECT CAST(StyleId as nvarchar(max)) as StyleId,CAST(ItemId as nvarchar(max)) as ItemId,LOTNumber,CAST(ReceivedQty as nvarchar(max))  as Quantity FROM AccessoriesReceivedAtStoreItems WHERE AccessoriesReceivedId = {recInfo.AccessoriesReceivedId}");
                        ViewState["CurrentTable"] = itemDetail;
                        rptItemRecEntryInfo.DataSource = itemDetail;
                        rptItemRecEntryInfo.DataBind();
                        SetPreviousData();

                        lnkbtnSaveEntries.Visible = false;
                        lnkbtnUpdateEntries.Visible = true;
                        dyeingOrderDiv.Visible = false;
                        lcDiv.Visible = true;
                    }

                    sizeActionTitle.Text = "Update Accessories Received Information";
                }
            }
        }

        private void LoadMerchandiserList()
        {
            var dt = unitOfWork.GetDataTableFromSql("EXEC usp_GetMerchandiserList");
            ddlMerchandiser.Items.Add(new ListItem("", ""));
            ddlMerchandiser.DataSource = dt;
            ddlMerchandiser.DataTextField = "Name";
            ddlMerchandiser.DataValueField = "Id";
            ddlMerchandiser.DataBind();
            ddlMerchandiser.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlMerchandiser.SelectedIndex = 0;
        }

        int ReceivedId
        {
            set { ViewState["receivedId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["receivedId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        private void LoadStore()
        {
            var dt = unitOfWork.GetDataTableFromSql("SELECT StoreId, StoreName FROM Stores");
            ddlStore.Items.Add(new ListItem("", ""));
            ddlStore.DataSource = dt;
            ddlStore.DataTextField = "StoreName";
            ddlStore.DataValueField = "StoreId";
            ddlStore.DataBind();
            ddlStore.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlStore.SelectedIndex = 0;
        }

        protected void ddlPurchaseType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPurchaseType.SelectedValue != "")
            {
                if (int.Parse(ddlPurchaseType.SelectedValue) == 2)
                {
                    dyeingOrderDiv.Visible = true;
                    lcDiv.Visible = false;
                    pnlDetails.Visible = false;
                    divItemInfo.Visible = false;
                    divItemInfoDyeingOrder.Visible = true;
                    SetInitialRowCountDyeingOrder();
                    lnkBtnSave.Visible = true;
                }
                else
                {
                    var supplierIdTxt = ddlYarnAndAccssSuppliers.SelectedValue;
                    if (supplierIdTxt != "")
                    {
                        var supplierId = int.Parse(supplierIdTxt);
                        CommonMethods.LoadLCBySupplier(ddlLCNumber, 1, 0, supplierId);
                        dyeingOrderDiv.Visible = false;
                        lcDiv.Visible = true;
                        divItemInfoDyeingOrder.Visible = false;
                        divItemInfo.Visible = false;
                        lnkBtnSave.Visible = false;
                    }
                    else
                    {
                        ddlPurchaseType.ClearSelection();
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a supplier.')", true);
                    }
                }
            }
            else
            {
                dyeingOrderDiv.Visible = false;
                lcDiv.Visible = false;
                pnlDetails.Visible = false;
                divItemInfoDyeingOrder.Visible = false;
                lnkBtnSave.Visible = false;
            }
        }

        protected void ddlYarnAndAccssSuppliers_SelectedIndexChanged(object sender, EventArgs e)
        {
            var supplierIdTxt = ddlYarnAndAccssSuppliers.SelectedValue;
            if (supplierIdTxt != "")
            {
                var supplierId = int.Parse(supplierIdTxt);
                CommonMethods.LoadLCBySupplier(ddlLCNumber, 1, 0, supplierId);
            }
            else
            {
                ddlLCNumber.Items.Clear();
                //ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a supplier.')", true);
            }
        }

        protected void ddlLCNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            var lcIdTxt = ddlLCNumber.SelectedValue;
            if (lcIdTxt != "")
            {
                var LCId = int.Parse(lcIdTxt);
                ShowPIInformationByLCId(LCId);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);
            }
            else
            {
                pnlDetails.Visible = false;
                divItemInfo.Visible = false;
                lnkbtnSaveEntries.Visible = false;
            }
        }


        private void ShowPIInformationByLCId(int LCId)
        {
            pnlDetails.Visible = true;

            string sql = $"Exec usp_GetPIInfoByLCIds '{LCId}'";
            var dt = unitOfWork.GetDataTableFromSql(sql);

            if (dt.Rows.Count > 0)
            {
                rptPISummary.Visible = true;
                rptPISummary.DataSource = dt;
                rptPISummary.DataBind();
                SetInitialRowCount();
                divItemInfo.Visible = true;
                lnkbtnSaveEntries.Visible = true;
            }
            else
            {
                divItemInfo.Visible = false;
                rptPISummary.Visible = false;
                lnkbtnSaveEntries.Visible = false;
            }
        }

        protected void rptPISummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                totalGoods = 0;
                grandTotalAmount = 0;

                string PIId = DataBinder.Eval(e.Item.DataItem, "PIId").ToString();

                Repeater rptPIItemInfo = (Repeater)e.Item.FindControl("rptPIItemInfo");
                Label lblNoItemFound = (Label)e.Item.FindControl("lblNoItemFound");

                string sql2 = $"Exec usp_GetPIItemsByPIId '{PIId}'";
                var dt2 = unitOfWork.GetDataTableFromSql(sql2);

                if (dt2.Rows.Count > 0)
                {
                    lblNoItemFound.Visible = false;

                    rptPIItemInfo.DataSource = dt2;
                    rptPIItemInfo.DataBind();

                }
                else
                {
                    lblNoItemFound.Visible = true;

                }

            }
        }


        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("StyleId", typeof(string)));
            //dt.Columns.Add(new DataColumn("ColorId", typeof(string)));
            //dt.Columns.Add(new DataColumn("ItemTypeId", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));

            dt.Columns.Add(new DataColumn("LOTNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));

            dr = dt.NewRow();

            dr["StyleId"] = string.Empty;
            //dr["ColorId"] = string.Empty;
            //dr["ItemTypeId"] = string.Empty;
            dr["ItemId"] = string.Empty;
            dr["LOTNumber"] = string.Empty;
            dr["Quantity"] = string.Empty;


            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptItemRecEntryInfo.DataSource = dt;
            rptItemRecEntryInfo.DataBind();
        }
        private void SetInitialRowCountDyeingOrder()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("BuyerId", typeof(string)));
            dt.Columns.Add(new DataColumn("StyleId", typeof(string)));
            //dt.Columns.Add(new DataColumn("ColorId", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));
            dt.Columns.Add(new DataColumn("LOTNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));

            dr = dt.NewRow();

            dr["BuyerId"] = string.Empty;
            dr["StyleId"] = string.Empty;
            //dr["ColorId"] = string.Empty;
            //dr["ItemTypeId"] = string.Empty;
            dr["ItemId"] = string.Empty;
            dr["LOTNumber"] = string.Empty;
            dr["Quantity"] = string.Empty;


            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptItemRecDyeingEntryInfo.DataSource = dt;
            rptItemRecDyeingEntryInfo.DataBind();
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {


                        DropDownList ddlStyles = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                        //DropDownList ddlColors = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        // DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                        DropDownList ddlItems = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlItems");


                        TextBox tbxLOTNumber = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxLOTNumber");
                        TextBox tbxQty = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxQty");

                        dtCurrentTable.Rows[rowIndex]["StyleId"] = ddlStyles.SelectedValue;
                        //dtCurrentTable.Rows[rowIndex]["ColorId"] = ddlColors.SelectedValue;
                        //dtCurrentTable.Rows[rowIndex]["ItemTypeId"] = ddlItemTypes.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["ItemId"] = ddlItems.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["LOTNumber"] = tbxLOTNumber.Text;
                        dtCurrentTable.Rows[rowIndex]["Quantity"] = tbxQty.Text;


                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["StyleId"] = string.Empty;
                            //drCurrentRow["ColorId"] = string.Empty;
                            //drCurrentRow["ItemTypeId"] = string.Empty;
                            drCurrentRow["ItemId"] = string.Empty;
                            drCurrentRow["LOTNumber"] = string.Empty;
                            drCurrentRow["Quantity"] = string.Empty;

                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptItemRecEntryInfo.DataSource = dtCurrentTable;
                    this.rptItemRecEntryInfo.DataBind();

                }
            }
            this.SetPreviousData();
        }


        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {


                        DropDownList ddlStyles = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                        //DropDownList ddlColors = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        // DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                        DropDownList ddlItems = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlItems");



                        TextBox tbxLOTNumber = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxLOTNumber");
                        TextBox tbxQty = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxQty");
                        if (ddlLCNumber.SelectedValue != "")
                        {
                            SetStyleDropdown(ddlStyles, int.Parse(ddlLCNumber.SelectedValue));
                        }

                        // CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddl.SelectedValue), "BuyerStyles", 1, 0);
                        ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["StyleId"].ToString()) ? "0" : dt.Rows[i]["StyleId"].ToString())));

                        // CommonMethods.LoadYarnAndAccessoriesItemTypeDropdown(ddlItemTypes, 1, 0);
                        // ddlItemTypes.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItemTypes, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ItemTypeId"].ToString()) ? "0" : dt.Rows[i]["ItemTypeId"].ToString())));


                        int selectedStyleId = 0;
                        if (ddlStyles.SelectedValue != "")
                        {
                            selectedStyleId = int.Parse(ddlStyles.SelectedValue);
                        }

                        //SetColorDropdown(ddlColors, selectedStyleId);
                        //ddlColors.SelectedIndex = CommonMethods.MatchDropDownItem(ddlColors, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ColorId"].ToString()) ? "0" : dt.Rows[i]["ColorId"].ToString())));


                        //int selectedColorId = 0;

                        //if (ddlColors.SelectedValue != "")
                        //{
                        //    selectedColorId = int.Parse(ddlColors.SelectedValue);
                        //}


                        //CommonMethods.LoadYarnAndAccessoriesItemDropdownByOrderOrStyle(ddlItems, selectedStyleId, selectedOrderId, 1, 0);
                        //CommonMethods.LoadYarnAndAccessoriesItemsDropdown(ddlItems, 1, 0);
                        SetItemDropdown(ddlItems, selectedStyleId);
                        ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ItemId"].ToString()) ? "0" : dt.Rows[i]["ItemId"].ToString())));


                        tbxLOTNumber.Text = dt.Rows[i]["LOTNumber"].ToString();
                        tbxQty.Text = dt.Rows[i]["Quantity"].ToString();

                        rowIndex++;
                    }
                }
            }
        }
        private void SetPreviousDataDyeingOrder()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {


                        DropDownList ddlBuyers = (DropDownList)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("ddlBuyers");
                        DropDownList ddlStyles = (DropDownList)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                        //DropDownList ddlColors = (DropDownList)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        // DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                        DropDownList ddlItems = (DropDownList)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("ddlItems");



                        TextBox tbxLOTNumber = (TextBox)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("tbxLOTNumber");
                        TextBox tbxQty = (TextBox)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("tbxQty");

                        CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                        ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["BuyerId"].ToString()) ? "0" : dt.Rows[i]["BuyerId"].ToString())));
                        if (ddlBuyers.SelectedValue != "")
                        {
                            BindStylesByBuyer(ddlStyles, int.Parse(ddlBuyers.SelectedValue));
                        }

                        //SetStyleDropdown(ddlStyles, int.Parse(ddlLCNumber.SelectedValue));
                        // CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddl.SelectedValue), "BuyerStyles", 1, 0);
                        ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["StyleId"].ToString()) ? "0" : dt.Rows[i]["StyleId"].ToString())));

                        // CommonMethods.LoadYarnAndAccessoriesItemTypeDropdown(ddlItemTypes, 1, 0);
                        // ddlItemTypes.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItemTypes, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ItemTypeId"].ToString()) ? "0" : dt.Rows[i]["ItemTypeId"].ToString())));


                        int selectedStyleId = 0;
                        if (ddlStyles.SelectedValue != "")
                        {
                            selectedStyleId = int.Parse(ddlStyles.SelectedValue);
                        }

                        //SetColorDropdown(ddlColors, selectedStyleId);
                        //ddlColors.SelectedIndex = CommonMethods.MatchDropDownItem(ddlColors, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ColorId"].ToString()) ? "0" : dt.Rows[i]["ColorId"].ToString())));


                        //int selectedColorId = 0;

                        //if (ddlColors.SelectedValue != "")
                        //{
                        //    selectedColorId = int.Parse(ddlColors.SelectedValue);
                        //}


                        //CommonMethods.LoadYarnAndAccessoriesItemDropdownByOrderOrStyle(ddlItems, selectedStyleId, selectedOrderId, 1, 0);
                        //CommonMethods.LoadYarnAndAccessoriesItemsDropdown(ddlItems, 1, 0);
                        SetItemDropdown(ddlItems, selectedStyleId);
                        ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ItemId"].ToString()) ? "0" : dt.Rows[i]["ItemId"].ToString())));


                        tbxLOTNumber.Text = dt.Rows[i]["LOTNumber"].ToString();
                        tbxQty.Text = dt.Rows[i]["Quantity"].ToString();

                        rowIndex++;
                    }
                }
            }
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlStyle = (DropDownList)sender;
            //DropDownList ddlColors = (DropDownList)((RepeaterItem)ddlStyle.NamingContainer).FindControl("ddlColors");
            DropDownList ddlItems = (DropDownList)((RepeaterItem)ddlStyle.NamingContainer).FindControl("ddlItems");
            if (ddlStyle.SelectedValue == "")
            {
                ddlItems.Items.Clear();
                //ddlColors.Items.Clear();

            }
            else
            {
                //SetColorDropdown(ddlColors, int.Parse(ddlStyle.SelectedValue));
                SetItemDropdown(ddlItems, int.Parse(ddlStyle.SelectedValue));
            }
        }

        protected void rptPIItemInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                var qty = DataBinder.Eval(e.Item.DataItem, "Quantity").ToString();
                var totalAmount = DataBinder.Eval(e.Item.DataItem, "TotalAmount").ToString();

                totalGoods += decimal.Parse(qty);
                grandTotalAmount += decimal.Parse(totalAmount);


            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {

                Label lblTotalQuantity = (Label)e.Item.FindControl("lblTotalQuantity");
                Label lblGrandTotalAmount = (Label)e.Item.FindControl("lblGrandTotalAmount");


                lblTotalQuantity.Text = totalGoods.ToString();
                lblGrandTotalAmount.Text = grandTotalAmount.ToString();

            }

        }

        protected void SetStyleDropdown(DropDownList ddlStyle, int lcId)
        {
            var dtStyles = unitOfWork.GetDataTableFromSql($"EXEC usp_GetAllStyleByLC {lcId}");
            ddlStyle.Items.Add(new ListItem("", ""));
            ddlStyle.DataSource = dtStyles;
            ddlStyle.DataTextField = "StyleName";
            ddlStyle.DataValueField = "StyleId";
            ddlStyle.DataBind();
            ddlStyle.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlStyle.SelectedIndex = 0;
        }

        protected void SetColorDropdown(DropDownList ddlColor, int styleId)
        {
            var dtColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetStyleColorsByStyleId {styleId}");
            ddlColor.Items.Add(new ListItem("", ""));
            ddlColor.DataSource = dtColors;
            ddlColor.DataTextField = "ColorDescription";
            ddlColor.DataValueField = "BuyerColorId";
            ddlColor.DataBind();
            ddlColor.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlColor.SelectedIndex = 0;
        }

        protected void SetItemDropdown(DropDownList ddlItem, int styleId)
        {
            if (ddlPurchaseType.SelectedValue == "2")
            {
                var dtColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetYarnOrAccessoriesItems 1");
                ddlItem.Items.Add(new ListItem("", ""));
                ddlItem.DataSource = dtColors;
                ddlItem.DataTextField = "ItemName";
                ddlItem.DataValueField = "ItemId";
                ddlItem.DataBind();
                ddlItem.Items.Insert(0, new ListItem("---Select---", string.Empty));
                ddlItem.SelectedIndex = 0;
            }
            else
            {
                var lcId = int.Parse(ddlLCNumber.SelectedValue);
                var dtColors = unitOfWork.GetDataTableFromSql($"EXEC GetLCItemsByLCAndStyle {lcId},{styleId},1"); //item type id  1 = accessories,
                ddlItem.Items.Add(new ListItem("", ""));
                ddlItem.DataSource = dtColors;
                ddlItem.DataTextField = "ItemName";
                ddlItem.DataValueField = "ItemId";
                ddlItem.DataBind();
                ddlItem.Items.Insert(0, new ListItem("---Select---", string.Empty));
                ddlItem.SelectedIndex = 0;
            }
        }

        protected void rptItemRecEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var ddlStyles = (DropDownList)e.Item.FindControl("ddlStyles");
                var lcId = ddlLCNumber.SelectedValue;
                if (lcId != "")
                {
                    SetStyleDropdown(ddlStyles, int.Parse(lcId));
                }
            }
        }

        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(tbxReceiveDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter receive date.')", true);
            }
            else if (ddlStore.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a store.')", true);
            }
            else if (String.IsNullOrEmpty(tbxInvoiceNumber.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter invoice number.')", true);
            }
            else if (ddlMerchandiser.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a merchandiser.')", true);
            }
            else if (ddlYarnAndAccssSuppliers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a supplier.')", true);
            }
            else if (ddlPurchaseType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select purchase type.')", true);
            }
            else if (ddlPurchaseType.SelectedValue != "2" && ddlLCNumber.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select L/C number.')", true);
            }
            else if (ddlPurchaseType.SelectedValue == "2" && String.IsNullOrEmpty(tbxDyeingOrderNumber.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter dyeing order number.')", true);
            }
            else
            {
                try
                {

                    var accRecAtStore = new AccessoriesReceivedAtStore()
                    {
                        ReceiveDate = Convert.ToDateTime(tbxReceiveDate.Text),
                        SupplierId = int.Parse(ddlYarnAndAccssSuppliers.SelectedValue),
                        PurchaseType = int.Parse(ddlPurchaseType.SelectedValue),
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName,
                        StoreId = int.Parse(ddlStore.SelectedValue),
                        InvoiceNumber = tbxInvoiceNumber.Text,
                        MerchandiserId = int.Parse(ddlMerchandiser.SelectedValue)
                    };
                    if (accRecAtStore.PurchaseType != 2)
                    {
                        accRecAtStore.LCId = int.Parse(ddlLCNumber.SelectedValue);
                    }
                    else
                    {
                        accRecAtStore.DyeingOrderNumber = tbxDyeingOrderNumber.Text;
                    }


                    List<AccessoriesReceivedAtStoreItems> listItems = new List<AccessoriesReceivedAtStoreItems>();
                    if (accRecAtStore.PurchaseType == 2)
                    {
                        for (int rowIndex = 0; rowIndex < rptItemRecDyeingEntryInfo.Items.Count; rowIndex++)
                        {
                            //TextBox tbxDescription = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxDescription");

                            DropDownList ddlStyles = (DropDownList)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                            //DropDownList ddlColors = (DropDownList)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("ddlColors");
                            //DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                            DropDownList ddlItems = (DropDownList)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("ddlItems");

                            //int selectedColorId = 0;
                            //if (ddlColors.SelectedValue != "")
                            //{
                            //    selectedColorId = int.Parse(ddlColors.SelectedValue);
                            //}

                            TextBox tbxLOTNumber = (TextBox)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("tbxLOTNumber");
                            TextBox tbxQty = (TextBox)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("tbxQty");

                            if (ddlStyles.SelectedValue != "" && ddlItems.SelectedValue != "" && !string.IsNullOrEmpty(tbxQty.Text))
                            {
                                var styleId = int.Parse(ddlStyles.SelectedValue);
                                var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().Get(x => x.Id == styleId).FirstOrDefault();
                                var item = new AccessoriesReceivedAtStoreItems()
                                {
                                    AccessoriesReceivedId = accRecAtStore.AccessoriesReceivedId,
                                    StyleId = styleId,
                                    //ColorId = selectedColorId,
                                    BuyerId = styleInfo.BuyerId,
                                    ItemId = int.Parse(ddlItems.SelectedValue),
                                    LOTNumber = tbxLOTNumber.Text,
                                    ReceivedQty = Convert.ToDecimal(tbxQty.Text),
                                    CreateDate = DateTime.Now,
                                    CreatedBy = CommonMethods.SessionInfo.UserName
                                };

                                listItems.Add(item);
                            }
                        }
                    }
                    else
                    {
                        for (int rowIndex = 0; rowIndex < rptItemRecEntryInfo.Items.Count; rowIndex++)
                        {
                            //TextBox tbxDescription = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxDescription");

                            DropDownList ddlStyles = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                            //DropDownList ddlColors = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlColors");
                            //DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                            DropDownList ddlItems = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlItems");

                            //int selectedColorId = 0;
                            //if (ddlColors.SelectedValue != "")
                            //{
                            //    selectedColorId = int.Parse(ddlColors.SelectedValue);
                            //}

                            TextBox tbxLOTNumber = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxLOTNumber");
                            TextBox tbxQty = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxQty");

                            if (ddlStyles.SelectedValue != "" && ddlItems.SelectedValue != "" && !string.IsNullOrEmpty(tbxQty.Text))
                            {
                                var styleId = int.Parse(ddlStyles.SelectedValue);
                                var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().Get(x => x.Id == styleId).FirstOrDefault();
                                var item = new AccessoriesReceivedAtStoreItems()
                                {
                                    AccessoriesReceivedId = accRecAtStore.AccessoriesReceivedId,
                                    StyleId = styleId,
                                    //ColorId = selectedColorId,
                                    BuyerId = styleInfo.BuyerId,
                                    ItemId = int.Parse(ddlItems.SelectedValue),
                                    LOTNumber = tbxLOTNumber.Text,
                                    ReceivedQty = Convert.ToDecimal(tbxQty.Text),
                                    CreateDate = DateTime.Now,
                                    CreatedBy = CommonMethods.SessionInfo.UserName
                                };

                                listItems.Add(item);
                            }
                        }
                    }

                    accRecAtStore.AccessoriesReceivedAtStoreItems = listItems;
                    unitOfWork.GenericRepositories<AccessoriesReceivedAtStore>().Insert(accRecAtStore);
                    unitOfWork.Save();

                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ReceiveAccessoriesAtStore.aspx');", true);

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }

        }

        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(tbxReceiveDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter receive date.')", true);
            }
            else if (ddlStore.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a store.')", true);
            }
            else if (String.IsNullOrEmpty(tbxInvoiceNumber.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter invoice number.')", true);
            }
            else if (ddlMerchandiser.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a merchandiser.')", true);
            }
            else if (ddlYarnAndAccssSuppliers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a supplier.')", true);
            }
            else if (ddlPurchaseType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select purchase type.')", true);
            }
            else if (ddlPurchaseType.SelectedValue != "2" && ddlLCNumber.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select L/C number.')", true);
            }
            else if (ddlPurchaseType.SelectedValue == "2" && String.IsNullOrEmpty(tbxDyeingOrderNumber.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter dyeing order number.')", true);
            }
            else
            {
                try
                {

                    var recInfo = unitOfWork.GenericRepositories<AccessoriesReceivedAtStore>().GetByID(ReceivedId);
                    var recItems = unitOfWork.GenericRepositories<AccessoriesReceivedAtStoreItems>().Get(x => x.AccessoriesReceivedId == ReceivedId).ToList();
                    foreach (var item in recItems)
                    {
                        unitOfWork.GenericRepositories<AccessoriesReceivedAtStoreItems>().Delete(item);
                    }

                    recInfo.ReceiveDate = Convert.ToDateTime(tbxReceiveDate.Text);
                    recInfo.SupplierId = int.Parse(ddlYarnAndAccssSuppliers.SelectedValue);
                    recInfo.PurchaseType = int.Parse(ddlPurchaseType.SelectedValue);
                    recInfo.UpdateDate = DateTime.Now;
                    recInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                    recInfo.StoreId = int.Parse(ddlStore.SelectedValue);
                    recInfo.InvoiceNumber = tbxInvoiceNumber.Text;
                    recInfo.MerchandiserId = int.Parse(ddlMerchandiser.SelectedValue);

                    if (recInfo.PurchaseType != 2)
                    {
                        recInfo.LCId = int.Parse(ddlLCNumber.SelectedValue);
                    }
                    else
                    {
                        recInfo.DyeingOrderNumber = tbxDyeingOrderNumber.Text;
                    }


                    List<AccessoriesReceivedAtStoreItems> listItems = new List<AccessoriesReceivedAtStoreItems>();
                    if (recInfo.PurchaseType == 2)
                    {
                        for (int rowIndex = 0; rowIndex < rptItemRecDyeingEntryInfo.Items.Count; rowIndex++)
                        {
                            //TextBox tbxDescription = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxDescription");

                            DropDownList ddlStyles = (DropDownList)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                            //DropDownList ddlColors = (DropDownList)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("ddlColors");
                            //DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                            DropDownList ddlItems = (DropDownList)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("ddlItems");

                            //int selectedColorId = 0;
                            //if (ddlColors.SelectedValue != "")
                            //{
                            //    selectedColorId = int.Parse(ddlColors.SelectedValue);
                            //}

                            TextBox tbxLOTNumber = (TextBox)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("tbxLOTNumber");
                            TextBox tbxQty = (TextBox)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("tbxQty");

                            if (ddlStyles.SelectedValue != "" && ddlItems.SelectedValue != "" && !string.IsNullOrEmpty(tbxQty.Text))
                            {
                                var styleId = int.Parse(ddlStyles.SelectedValue);
                                var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().Get(x => x.Id == styleId).FirstOrDefault();
                                var item = new AccessoriesReceivedAtStoreItems()
                                {
                                    AccessoriesReceivedId = recInfo.AccessoriesReceivedId,
                                    StyleId = styleId,
                                    //ColorId = selectedColorId,
                                    BuyerId = styleInfo.BuyerId,
                                    ItemId = int.Parse(ddlItems.SelectedValue),
                                    LOTNumber = tbxLOTNumber.Text,
                                    ReceivedQty = Convert.ToDecimal(tbxQty.Text),
                                    CreateDate = DateTime.Now,
                                    CreatedBy = CommonMethods.SessionInfo.UserName
                                };

                                listItems.Add(item);
                            }
                        }
                    }
                    else
                    {
                        for (int rowIndex = 0; rowIndex < rptItemRecEntryInfo.Items.Count; rowIndex++)
                        {
                            //TextBox tbxDescription = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxDescription");

                            DropDownList ddlStyles = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                            //DropDownList ddlColors = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlColors");
                            //DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                            DropDownList ddlItems = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlItems");

                            //int selectedColorId = 0;
                            //if (ddlColors.SelectedValue != "")
                            //{
                            //    selectedColorId = int.Parse(ddlColors.SelectedValue);
                            //}

                            TextBox tbxLOTNumber = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxLOTNumber");
                            TextBox tbxQty = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxQty");

                            if (ddlStyles.SelectedValue != "" && ddlItems.SelectedValue != "" && !string.IsNullOrEmpty(tbxQty.Text))
                            {
                                var styleId = int.Parse(ddlStyles.SelectedValue);
                                var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().Get(x => x.Id == styleId).FirstOrDefault();
                                var item = new AccessoriesReceivedAtStoreItems()
                                {
                                    AccessoriesReceivedId = recInfo.AccessoriesReceivedId,
                                    StyleId = styleId,
                                    //ColorId = selectedColorId,
                                    BuyerId = styleInfo.BuyerId,
                                    ItemId = int.Parse(ddlItems.SelectedValue),
                                    LOTNumber = tbxLOTNumber.Text,
                                    ReceivedQty = Convert.ToDecimal(tbxQty.Text),
                                    CreateDate = DateTime.Now,
                                    CreatedBy = CommonMethods.SessionInfo.UserName
                                };

                                listItems.Add(item);
                            }
                        }
                    }

                    recInfo.AccessoriesReceivedAtStoreItems = listItems;
                    unitOfWork.GenericRepositories<AccessoriesReceivedAtStore>().Insert(recInfo);
                    unitOfWork.Save();

                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewReceivedAccessoriesAtStore.aspx');", true);

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
        }

        protected void ddlBuyer_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlBuyers = (DropDownList)sender;
            DropDownList ddlStyle = (DropDownList)((RepeaterItem)ddlBuyers.NamingContainer).FindControl("ddlStyles");
            //DropDownList ddlColors = (DropDownList)((RepeaterItem)ddlBuyers.NamingContainer).FindControl("ddlColors");
            DropDownList ddlItems = (DropDownList)((RepeaterItem)ddlBuyers.NamingContainer).FindControl("ddlItems");
            if (ddlBuyers.SelectedValue == "")
            {
                ddlStyle.Items.Clear();
                ddlItems.Items.Clear();
                //ddlColors.Items.Clear();
            }
            else
            {
                BindStylesByBuyer(ddlStyle, int.Parse(ddlBuyers.SelectedValue));
            }
        }

        protected void rptItemRecDyeingEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var ddlBuyers = (DropDownList)e.Item.FindControl("ddlBuyers");
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
        }

        private void BindStylesByBuyer(DropDownList ddlStyles, int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void btnAddDyeingRow_Click(object sender, EventArgs e)
        {
            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {


                        DropDownList ddlBuyers = (DropDownList)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("ddlBuyers");
                        DropDownList ddlStyles = (DropDownList)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                        //DropDownList ddlColors = (DropDownList)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        // DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                        DropDownList ddlItems = (DropDownList)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("ddlItems");


                        TextBox tbxLOTNumber = (TextBox)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("tbxLOTNumber");
                        TextBox tbxQty = (TextBox)this.rptItemRecDyeingEntryInfo.Items[rowIndex].FindControl("tbxQty");

                        dtCurrentTable.Rows[rowIndex]["BuyerId"] = ddlBuyers.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["StyleId"] = ddlStyles.SelectedValue;
                        //dtCurrentTable.Rows[rowIndex]["ColorId"] = ddlColors.SelectedValue;
                        //dtCurrentTable.Rows[rowIndex]["ItemTypeId"] = ddlItemTypes.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["ItemId"] = ddlItems.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["LOTNumber"] = tbxLOTNumber.Text;
                        dtCurrentTable.Rows[rowIndex]["Quantity"] = tbxQty.Text;


                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["BuyerId"] = string.Empty;
                            drCurrentRow["StyleId"] = string.Empty;
                            //drCurrentRow["ColorId"] = string.Empty;
                            //drCurrentRow["ItemTypeId"] = string.Empty;
                            drCurrentRow["ItemId"] = string.Empty;
                            drCurrentRow["LOTNumber"] = string.Empty;
                            drCurrentRow["Quantity"] = string.Empty;

                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptItemRecDyeingEntryInfo.DataSource = dtCurrentTable;
                    this.rptItemRecDyeingEntryInfo.DataBind();

                }
            }
            this.SetPreviousDataDyeingOrder();
        }
    }
}