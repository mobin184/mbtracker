﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class ViewReceivedYarnAtStoreV2 : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //EnableDisableEditButton();
                //EnableDisableDeleteButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                CommonMethods.LoadYarnAndAccessoriesSupplierDropdown(ddlSupplier, 1, 0);
                if (Request.QueryString["styleId"] != null)
                {
                    var styleId = int.Parse(Request.QueryString["styleId"]);
                    var reportDate = "";
                    //GetYarnReceived(styleId, reportDate);
                    pnlEntry.Visible = false;

                }
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteYarnReceive(ReceivedId);
                }
            }
        }

        private void DeleteYarnReceive(int yarnReceivedId)
        {
            var receivedInfo = unitOfWork.GenericRepositories<YarnReceivedAtStore>().GetByID(yarnReceivedId);
            var lstItems = unitOfWork.GenericRepositories<YarnReceivedAtStoreItems>().Get(x => x.YarnReceivedId == yarnReceivedId).ToList();
            foreach (var item in lstItems)
            {
                unitOfWork.GenericRepositories<YarnReceivedAtStoreItems>().Delete(item);
            }
            unitOfWork.GenericRepositories<YarnReceivedAtStore>().Delete(receivedInfo);
            unitOfWork.Save();
            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
            LoadData();
        }

        int ReceivedId
        {
            set { ViewState["id"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["id"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            else
            {
                ddlStyles.Items.Clear();
            }
            pnlSummary.Visible = false;
            lblSummryNotFound.Visible = false;
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewReceivedYarnAtStore", 2, 1);
        }
        private void EnableDisableEditButton()
        {
            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,QCE");
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewReceivedYarnAtStore", 1, 1);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            try
            {
                var styleId = 0;
                var supplierId = 0;
                var receivedDate = "";
                var lcId = 0;
                var doNumber = "";
                if (ddlStyles.SelectedValue == "" && ddlSupplier.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select buyer with style or supplier.')", true);
                }
                else
                {
                    if (ddlStyles.SelectedValue != "")
                        styleId = int.Parse(ddlStyles.SelectedValue);
                    if (ddlSupplier.SelectedValue != "")
                        supplierId = int.Parse(ddlSupplier.SelectedValue);
                    if (!string.IsNullOrEmpty(tbxReceivedDate.Text))
                        receivedDate = DateTime.Parse(tbxReceivedDate.Text).ToString("yyyy-MM-dd");
                    if (ddlLCNumber.SelectedValue != "")
                        lcId = int.Parse(ddlLCNumber.SelectedValue);
                    if (ddlDyeingOrderNumber.SelectedValue != "")
                        doNumber = ddlDyeingOrderNumber.SelectedValue;

                    var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetYarnReceivedAtStoreList {styleId},{supplierId},'{receivedDate}',{lcId},'{doNumber}'");
                    if (dt.Rows.Count > 0)
                    {
                        dt = dt.CheckEditDeletePermission(CommonMethods.SessionInfo.RoleId, "ViewReceivedYarnAtStore");
                        rptSummary.DataSource = dt;
                        rptSummary.DataBind();
                        pnlSummary.Visible = true;
                        rptSummary.Visible = true;
                        lblSummryNotFound.Visible = false;
                        lblNoDetails.Visible = false;
                    }
                    else
                    {
                        pnlSummary.Visible = false;
                        rptSummary.Visible = false;
                        pnlDetails.Visible = false;
                        rptDetails.Visible = false;
                        lblSummryNotFound.Visible = true;
                    }

                    pnlDetails.Visible = false;
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            var id = int.Parse(e.CommandArgument.ToString());
            Response.Redirect("ReceiveYarnAtStoreV2.aspx?receivedId=" + Tools.UrlEncode(id + ""));
        }


        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            ReceivedId = int.Parse(e.CommandArgument.ToString());
            if (ReceivedId != 0)
            {
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
            }
            else
            {
                var title = "Error";
                var msg = "Sorry, you can not delete it.<br />Something went wrong.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            }
        }

        protected void ddlSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSupplier.SelectedValue != "")
            {
                BindLCAndDyeingOrder(Convert.ToInt32(ddlSupplier.SelectedValue));
            }
            else
            {
                ddlLCNumber.Items.Clear();
                ddlDyeingOrderNumber.Items.Clear();
            }
        }

        private void BindLCAndDyeingOrder(int supplierId)
        {
            CommonMethods.LoadLCBySupplier(ddlLCNumber, 1, 0, supplierId);
            CommonMethods.LoadDyeingOrderBySupplier(ddlDyeingOrderNumber, 0, 0, supplierId);
        }

        protected void lnkbtnDetails_Command(object sender, CommandEventArgs e)
        {
            var receivedId = int.Parse(e.CommandArgument.ToString());
            var dtDetails = unitOfWork.GetDataTableFromSql($"EXEC usp_GetYarnReceivedAtStoreDetailsByReceivedId {receivedId}");

            if(dtDetails.Rows.Count > 0)
            {
                rptDetails.DataSource = dtDetails;
                rptDetails.DataBind();
                pnlDetails.Visible = true;
                rptDetails.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(0)", true);
                lblNoDetails.Visible = false;
            }
            else
            {
                pnlDetails.Visible = false;
                rptDetails.Visible = false;
                lblNoDetails.Visible = true;
            }
        }
    }
}