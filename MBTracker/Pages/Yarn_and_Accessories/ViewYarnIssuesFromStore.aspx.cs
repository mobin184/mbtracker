﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class ViewYarnIssuesFromStore : System.Web.UI.Page
    {


    //    BuyerManager buyerManager = new BuyerManager();
    //    OrderManager orderManager = new OrderManager();
    //    ProductionManager productionManager = new ProductionManager();
    //    DataTable dtOrderColors;
    //    DataTable dtSizes;
    //    UnitOfWork unitOfWork = new UnitOfWork();

    //    protected void Page_Load(object sender, EventArgs e)
    //    {
    //        if (!IsPostBack)
    //        {
    //            EnableDisableEditButton();
    //            EnableDisableDeleteButton();
    //            CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);

    //            if (Request.QueryString["styleId"] != null)
    //            {
    //                var styleId = int.Parse(Tools.UrlDecode(Request.QueryString["styleId"]));
    //                var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(styleId);
    //                ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
    //                BindStylesByBuyer(styleInfo.BuyerId);
    //                ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleId);
    //                LoadPlanInfo();
    //                LoadYarnIssues(styleId);
    //                pnlAssignMachinesToOrder.Visible = false;
    //                //pnlSummaryView.Visible = true;
    //            }
    //        }
    //        else
    //        {
    //            string parameter = Request["__EVENTARGUMENT"];
    //            if (parameter == "confirmDelete")
    //            {
    //                DeleteYarnIssue(IssueNumber);
    //            }
    //        }
    //    }

    //    private void DeleteYarnIssue(int issuNumber)
    //    {
    //        try
    //        {
    //            var lstYarnIssues = unitOfWork.GenericRepositories<YarnIssuedForKnitting>().Get(x => x.IssueNumber == issuNumber).ToList();
    //            foreach (var item in lstYarnIssues)
    //            {
    //                unitOfWork.GenericRepositories<YarnIssuedForKnitting>().Delete(item);
    //            }
    //            unitOfWork.Save();
    //            var styleId = Int32.Parse(ddlStyles.SelectedValue);
    //            LoadYarnIssues(styleId);
    //            pnlAssignMachinesToOrder.Visible = false;
    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
    //        }
    //        catch (Exception ex)
    //        {
    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
    //        }
    //    }

    //    private void EnableDisableDeleteButton()
    //    {
    //        ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");
    //    }
    //    private void EnableDisableEditButton()
    //    {
    //        ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Yarn Store");
    //    }

    //    int KnittingTime
    //    {
    //        set { ViewState["knittingTime"] = value; }
    //        get
    //        {
    //            try
    //            {
    //                return Convert.ToInt32(ViewState["knittingTime"]);
    //            }
    //            catch
    //            {
    //                return 0;
    //            }
    //        }
    //    }

    //    protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
    //    {

    //        ddlStyles.Items.Clear();
    //        //ddlOrders.Items.Clear();

    //        if (ddlBuyers.SelectedValue != "")
    //        {
    //            BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
    //        }

    //        pnlPlanningInfo.Visible = false;
    //        pnlAssignMachinesToOrder.Visible = false;
    //        pnlSummaryView.Visible = false;
    //        rptPlanInfo.DataSource = null;
    //        rptPlanInfo.DataBind();
    //        rptYarnIssueInfo.DataSource = null;
    //        rptYarnIssueInfo.DataBind();
    //        rpt.DataSource = null;
    //        rpt.DataBind();           
    //    }

    //    private void BindStylesByBuyer(int buyerId)
    //    {
    //        CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
    //    }

    //    //protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
    //    //{
    //    //    //ddlOrders.Items.Clear();
    //    //    if (ddlStyles.SelectedValue != "")
    //    //    {
    //    //        // BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
    //    //        LoadPlanInfo();
    //    //        //ddlBuyers.Enabled = false;
    //    //        //ddlStyles.Enabled = false;
    //    //        //ddlOrders.Enabled = false;
    //    //        var styleId = Int32.Parse(ddlStyles.SelectedValue);
    //    //        LoadYarnIssues(styleId);
    //    //        pnlAssignMachinesToOrder.Visible = false;
    //    //        pnlSummaryView.Visible = true;
    //    //    }
    //    //    else
    //    //    {
    //    //        pnlPlanningInfo.Visible = false;
    //    //        pnlAssignMachinesToOrder.Visible = false;
    //    //        pnlSummaryView.Visible = false;
    //    //        rptPlanInfo.DataSource = null;
    //    //        rptPlanInfo.DataBind();
    //    //        rptYarnIssueInfo.DataSource = null;
    //    //        rptYarnIssueInfo.DataBind();
    //    //        rpt.DataSource = null;
    //    //        rpt.DataBind();
    //    //    }          

    //    //}

    //    //private void BindOrdersByStyle(int styleId)
    //    //{

    //    //    CommonMethods.LoadOrderDropdownByStyle(ddlOrders, styleId, 1, 0);

    //    //}

    //    //protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
    //    //{


    //    //    if (ddlOrders.SelectedValue != "")
    //    //    {
    //    //        LoadPlanInfo();
    //    //        //ddlBuyers.Enabled = false;
    //    //        //ddlStyles.Enabled = false;
    //    //        //ddlOrders.Enabled = false;
    //    //        var orderId = Int32.Parse(ddlOrders.SelectedValue);
    //    //        LoadYarnIssues(orderId);
    //    //        pnlAssignMachinesToOrder.Visible = false;
    //    //        pnlSummaryView.Visible = true;

    //    //    }
    //    //    else
    //    //    {
    //    //        pnlPlanningInfo.Visible = false;
    //    //        pnlSummaryView.Visible = false;
    //    //       // pnlAssignMachinesToOrder.Visible = false;
    //    //    }
    //    //}

    //    private void LoadYarnIssues(int styleId)
    //    {

    //        var date = "";
    //        if (!string.IsNullOrEmpty(tbxDateIssued.Text))
    //        {
    //            date = DateTime.Parse(tbxDateIssued.Text).ToString("yyyy-MM-dd");
    //        }

    //        string sql = $" Exec usp_GetYarnIssueSummaryByStyleId {styleId},'{date}'";
    //        var dt = unitOfWork.GetDataTableFromSql(sql);
    //        if(dt.Rows.Count > 0)
    //        {
    //            rptYarnIssueInfo.DataSource = dt;
    //            rptYarnIssueInfo.DataBind();
    //            lblNoIssueFound.Visible = false;
    //            pnlSummaryView.Visible = true;
    //            pnlPlanningInfo.Visible = true;
    //        }
    //        else
    //        {
    //            pnlSummaryView.Visible = false;
    //            rptYarnIssueInfo.DataSource = null;
    //            rptYarnIssueInfo.DataBind();
    //            lblNoIssueFound.Visible = true;
    //            pnlPlanningInfo.Visible = false;
    //        }
           
    //    }


    //    private void LoadPlanInfo()
    //    {
    //        DataTable dt = new DataTable();
    //        pnlPlanningInfo.Visible = true;


    //        dt = productionManager.GetProductionPlanningInfo(Convert.ToInt32(ddlStyles.SelectedValue));
    //        if (dt.Rows.Count < 1)
    //        {
    //            dt = orderManager.GetProductionPlanningInfo(Convert.ToInt32(ddlStyles.SelectedValue));
    //        }

    //        if (dt.Rows.Count > 0)
    //        {
    //            KnittingTime = Convert.ToInt32(dt.Rows[0][8].ToString());

    //            rptPlanInfo.DataSource = dt;
    //            rptPlanInfo.DataBind();
    //            lblNoPlanningInfo.Visible = false;
    //            rptPlanInfo.Visible = true;          

    //        }
    //        else
    //        {
    //            rptPlanInfo.DataSource = null;
    //            rptPlanInfo.DataBind();
    //            rptPlanInfo.Visible = false;
    //            lblNoPlanningInfo.Visible = true;

    //           // pnlAssignMachinesToOrder.Visible = false;


    //        }

    //    }




    //    private void LoadYarnIssueInfoByIssueNumber(int issueNumber)
    //    {

    //        DataTable dt = new DataTable();
    //        pnlAssignMachinesToOrder.Visible = true;

    //        dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetYarnIssueInfoByIssueNumber {issueNumber}");

    //        if (dt.Rows.Count > 0)
    //        {
    //            lblMachineNotAssigned.Visible = false;
    //            divKnittingPart.Visible = true;
    //            divYarnDes.Visible = true;
    //            dtOrderColors = orderManager.GetColorsByStyleId(Convert.ToInt32(ddlStyles.SelectedValue));

    //            if (dtOrderColors.Rows.Count > 0)
    //            {
    //                LoadSizeInfo();
    //            }

    //            rpt.DataSource = dt;
    //            rpt.DataBind();
    //            rpt.Visible = true;
    //            pnlAssignMachinesToOrder.Visible = true;

    //        }
    //        else
    //        {

    //            rpt.DataSource = null;
    //            rpt.DataBind();
    //            rpt.Visible = false;
    //            lblMachineNotAssigned.Visible = true;
    //            divKnittingPart.Visible = false;
    //            divYarnDes.Visible = false;

    //        }

    //    }


    //    protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //    {

    //        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //        {
    //            Repeater rptOrderColors = (Repeater)e.Item.FindControl("rptOrderColors");
    //            if (dtOrderColors.Rows.Count > 0)
    //            {
    //                rptOrderColors.DataSource = dtOrderColors;
    //                rptOrderColors.DataBind();
    //            }
    //        }
    //    }

    //    protected void rptOrderColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //    {


    //        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //        {

    //            GridView gvSizeQuantity = (GridView)e.Item.FindControl("gvSizeQuantity");

    //            DataTable dtOneRow = new DataTable();
    //            DataRow dr = null;
    //            dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
    //            dr = dtOneRow.NewRow();
    //            dr["RowNumber"] = 1;
    //            dtOneRow.Rows.Add(dr);

    //            if (dtSizes.Rows.Count > 0)
    //            {
    //                TemplateField templateField = null;

    //                for (int i = 0; i < dtSizes.Rows.Count; i++)
    //                {
    //                    templateField = new TemplateField();
    //                    templateField.HeaderText = dtSizes.Rows[i][1].ToString();
    //                    gvSizeQuantity.Columns.Add(templateField);
    //                }

    //            }


    //            gvSizeQuantity.DataSource = dtOneRow;
    //            gvSizeQuantity.DataBind();

    //        }

    //    }



    //    protected void gvSizeQuantity_RowCreated(object sender, GridViewRowEventArgs e)
    //    {
    //        if (e.Row.RowType == DataControlRowType.DataRow)
    //        {

    //            Label txtSizeQty = null;
    //            Label lblSizeId = null;
    //            LoadSizeInfo();
    //            if (IssueNumber != 0)
    //            {
    //                var styleId = Int32.Parse(string.IsNullOrEmpty(ddlStyles.SelectedValue) ? "0" : ddlStyles.SelectedValue);
    //                var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
    //                var buyerColorId = int.Parse(lblBuyerColorId.Text);

    //                var lblUnitId = (Label)((GridView)sender).DataItemContainer.NamingContainer.NamingContainer.FindControl("lblUnitId");                    
    //                var UnitId = Convert.ToInt32(lblUnitId.Text);
    //                var lblMachinAssignedId = (Label)((GridView)sender).DataItemContainer.NamingContainer.NamingContainer.FindControl("lblMachinAssignedId");
    //                var machinAssignedId = Convert.ToInt32(lblMachinAssignedId.Text);


    //                for (int i = 0; i < dtSizes.Rows.Count; i++)
    //                {
    //                    var sizeId = Int32.Parse(dtSizes.Rows[i][0].ToString());
    //                    var info = unitOfWork.GenericRepositories<YarnIssuedForKnitting>().Get(x => x.StyleId == styleId && x.MachinAssignedId == machinAssignedId && x.BuyerColorId == buyerColorId && x.UnitId == UnitId && x.SizeId == sizeId && x.IsNewRecord == true && x.IssueNumber == IssueNumber).FirstOrDefault();

    //                    if (info != null)
    //                    {
    //                        txtSizeQty = new Label();
    //                        txtSizeQty.ID = "txtSizeQty" + i.ToString();
    //                        txtSizeQty.Text = info.YarnIssuedQuantity + "";
    //                        txtSizeQty.Width = 60;
    //                        e.Row.Cells[i].Controls.Add(txtSizeQty);

    //                        lblSizeId = new Label();
    //                        lblSizeId.ID = "lblSizeId" + i.ToString();
    //                        lblSizeId.Text = dtSizes.Rows[i][0].ToString();
    //                        lblSizeId.Visible = false;
    //                        e.Row.Cells[i].Controls.Add(lblSizeId);

    //                        var lblYarnIssuedForKnitting = new Label();
    //                        lblYarnIssuedForKnitting.ID = "lblYarnIssuedForKnitting" + i.ToString();
    //                        lblYarnIssuedForKnitting.Text = info.Id + "";
    //                        lblYarnIssuedForKnitting.Visible = false;
    //                        e.Row.Cells[i].Controls.Add(lblYarnIssuedForKnitting);

    //                    }
    //                    else
    //                    {
    //                        txtSizeQty = new Label();
    //                        txtSizeQty.ID = "txtSizeQty" + i.ToString();
    //                        txtSizeQty.Text = "";
    //                        txtSizeQty.Width = 60;
    //                        e.Row.Cells[i].Controls.Add(txtSizeQty);

    //                        lblSizeId = new Label();
    //                        lblSizeId.ID = "lblSizeId" + i.ToString();
    //                        lblSizeId.Text = dtSizes.Rows[i][0].ToString();
    //                        lblSizeId.Visible = false;
    //                        e.Row.Cells[i].Controls.Add(lblSizeId);
    //                    }
    //                }
    //            }
    //            else
    //            {
    //                for (int i = 0; i < dtSizes.Rows.Count; i++)
    //                {
    //                    txtSizeQty = new Label();
    //                    txtSizeQty.ID = "txtSizeQty" + i.ToString();
    //                    txtSizeQty.Text = "";
    //                    txtSizeQty.Width = 60;
    //                    e.Row.Cells[i].Controls.Add(txtSizeQty);

    //                    lblSizeId = new Label();
    //                    lblSizeId.ID = "lblSizeId" + i.ToString();
    //                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
    //                    lblSizeId.Visible = false;
    //                    e.Row.Cells[i].Controls.Add(lblSizeId);
    //                }
    //            }
    //        }
    //    }



    //    protected void LoadSizeInfo()
    //    {
    //        dtSizes = buyerManager.GetSizes(Convert.ToInt32(ddlBuyers.SelectedValue));

    //    }

    //    public DataTable YarnIssuedForKnitting()
    //    {
    //        DataTable dt = new DataTable("YarnIssuedForKnitting");
    //        dt.Columns.Add("StyleId", typeof(int));
    //        dt.Columns.Add("UnitId", typeof(int));
    //        dt.Columns.Add("BuyerColorId", typeof(int));
    //        dt.Columns.Add("SizeId", typeof(int));
    //        dt.Columns.Add("YarnIssuedQuantity", typeof(int));
    //        dt.Columns.Add("KnittingPart", typeof(string));
    //        dt.Columns.Add("YarnDescription", typeof(string));
    //        dt.Columns.Add("CdBy", typeof(string));
    //        dt.Columns.Add("CDate", typeof(DateTime));
    //        return dt;
    //    } 

    //    protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
    //    {
    //        int issueNumber = Convert.ToInt32(e.CommandArgument.ToString());
    //        pnlAssignMachinesToOrder.Visible = false;
    //        Response.Redirect("IssueYarnForKnitting.aspx?issueNumber=" + Tools.UrlEncode(issueNumber+""));
    //    }

    //    protected void lnkbtnView_Command(object sender, CommandEventArgs e)
    //    {
    //        IssueNumber = Convert.ToInt32(e.CommandArgument.ToString());
    //        var issueInfo = unitOfWork.GenericRepositories<YarnIssuedForKnitting>().Get(x => x.IssueNumber == IssueNumber && x.IsNewRecord == true).FirstOrDefault();
    //        LoadYarnIssueInfoByIssueNumber(IssueNumber);


    //        pnlAssignMachinesToOrder.Visible = true;
    //        lblKnittingPart.Text = issueInfo.KnittingPart;
    //        lblYarnDecription.Text = issueInfo.YarnDescription;
    //        ScriptManager.RegisterStartupScript(this,this.GetType(), "message", "scrollDown()", true);
    //    }

    //    int IssueNumber
    //    {
    //        set { ViewState["issueNumber"] = value; }
    //        get
    //        {
    //            try
    //            {
    //                return Convert.ToInt32(ViewState["issueNumber"]);
    //            }
    //            catch
    //            {
    //                return 0;
    //            }
    //        }
    //    }

    //    protected void btnViewIssues_Click(object sender, EventArgs e)
    //    {
    //        if (ddlBuyers.SelectedValue != "")
    //        {
    //            if (ddlStyles.SelectedValue != "")
    //            {
    //                // BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
    //                LoadPlanInfo();
    //                //ddlBuyers.Enabled = false;
    //                //ddlStyles.Enabled = false;
    //                //ddlOrders.Enabled = false;
    //                var styleId = Int32.Parse(ddlStyles.SelectedValue);
    //                LoadYarnIssues(styleId);
    //                pnlAssignMachinesToOrder.Visible = false;
    //               // pnlSummaryView.Visible = true;
    //            }
    //            else
    //            {
    //                pnlPlanningInfo.Visible = false;
    //                pnlAssignMachinesToOrder.Visible = false;
    //                pnlSummaryView.Visible = false;
    //                rptPlanInfo.DataSource = null;
    //                rptPlanInfo.DataBind();
    //                rptYarnIssueInfo.DataSource = null;
    //                rptYarnIssueInfo.DataBind();
    //                rpt.DataSource = null;
    //                rpt.DataBind();
    //                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a style');", true);
    //            }
    //        }
    //        else
    //        {
    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a buyer');", true);
    //        }
    //    }

    //    protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
    //    {
    //        IssueNumber = Convert.ToInt32(e.CommandArgument.ToString());
    //        if (IssueNumber != 0)
    //        {
    //            var title = "Warning";
    //            var msg = "Are you sure you want to delete?";
    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
    //        }
    //        else
    //        {
    //            var title = "Error";
    //            var msg = "Sorry, you can not delete it.<br />Something went wrong.";
    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
    //        }
    //    }
    }
}