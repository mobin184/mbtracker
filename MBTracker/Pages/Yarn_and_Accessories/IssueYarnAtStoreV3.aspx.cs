﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class IssueYarnAtStoreV3 : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        UnitOfWork unitOfWork = new UnitOfWork();


        DataTable dtIssuedYarnAtStoreDetails;
        int initialLoadForUpdate = 0;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var user = CommonMethods.SessionInfo;
                if (user == null)
                {
                    HttpContext.Current.Session["returnUrl"] = HttpContext.Current.Request.Url.PathAndQuery;
                    Response.Redirect("~/Login.aspx");
                }

                string url = Request.Url.AbsolutePath;
                var task = unitOfWork.GenericRepositories<TaskItems>().Get(x => x.URL == url).FirstOrDefault();

                if (task != null && user != null)
                {
                    var permission = unitOfWork.GenericRepositories<TaskRolePermissions>().Get(x => x.TaskId == task.Id && x.RoleId == user.RoleId).ToList();

                    if (!permission.Any())
                    {
                        Response.Redirect("~/Pages/AccessDenied.aspx", false);
                    }
                }

                var dtKnittingUnit = new CommonManager().GetProductionUnitsByUser(CommonMethods.SessionInfo.UserId);
                //dtKnittingUnit.DefaultView.Sort = "UnitName ASC";
                //dtKnittingUnit = dtKnittingUnit.DefaultView.ToTable();

                var lstOtherUnits = unitOfWork.GenericRepositories<OtherKnittingUnits>().Get().ToList();

                foreach (var item in lstOtherUnits)
                {
                    var drow = dtKnittingUnit.NewRow();
                    drow["ProductionUnitId"] = item.ProductionUnitId;
                    drow["UnitName"] = item.UnitName;
                    dtKnittingUnit.Rows.Add(drow);
                }


                //var dr = dtKnittingUnit.NewRow();
                //dr["ProductionUnitId"] = 99;
                //dr["UnitName"] = "Leftover";
                //dtKnittingUnit.Rows.Add(dr);

                //var dr1 = dtKnittingUnit.NewRow();
                //dr1["ProductionUnitId"] = 100;
                //dr1["UnitName"] = "On Hold";
                //dtKnittingUnit.Rows.Add(dr1);

                //var dr2 = dtKnittingUnit.NewRow();
                //dr2["ProductionUnitId"] = 101;
                //dr2["UnitName"] = "Mega Dyeing";
                //dtKnittingUnit.Rows.Add(dr2);




                this.ViewState["KnittingUnit"] = dtKnittingUnit;

                if (Request.QueryString["yarnIssueId"] != null)
                {
                    YarnIssueId = int.Parse(Tools.UrlDecode(Request.QueryString["yarnIssueId"]));
                    initialLoadForUpdate = YarnIssueId;
                    lblTitle.Text = "Update Yarn Issued at Store:";

                    //lblMessage.Text = "Received Spare Information:";

                    btnSaveEntries.Visible = false;
                    btnUpdateEntries.Visible = true;

                    PopulateEntryInfo();
                }
                else
                {
                    //CommonMethods.LoadProductionUnitsByUser(ddlUserProductionUnit);
                    //CommonMethods.LoadProductionUnitsForIssueByUser(ddlUserProductionUnit);

                    CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                    LoadStore();
                    tbxIssueDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                }
            }
        }



        private void LoadStore()
        {
            var dt = unitOfWork.GetDataTableFromSql("SELECT StoreId, StoreName FROM Stores WHERE StoreType = 2");
            ddlStore.Items.Add(new ListItem("", ""));
            ddlStore.DataSource = dt;
            ddlStore.DataTextField = "StoreName";
            ddlStore.DataValueField = "StoreId";
            ddlStore.DataBind();
            ddlStore.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlStore.SelectedIndex = 0;
        }

        private void PopulateEntryInfo()
        {
            if (YarnIssueId > 0)
            {
                LoadIssuedYarn();
            }
        }

        private void LoadIssuedYarn()
        {
            //string sql = $"Exec usp_GetYarnReceivedAtUnitById '{YarnIssueId}'";
            //var dtYarnReceive = unitOfWork.GetDataTableFromSql(sql);
            var yarnIssued = unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().GetByID(YarnIssueId);

            tbxIssueDate.Text = yarnIssued.IssueDate.ToString("yyyy-MM-dd");

            LoadStore();
            ddlStore.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStore, yarnIssued.StoreId);

            //CommonMethods.LoadProductionUnitsForIssueByUser(ddlUserProductionUnit);
            //ddlUserProductionUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUserProductionUnit, yarnIssued.KnittingUnitId);

            CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            //int buyerId = int.Parse((string.IsNullOrEmpty(dtYarnReceive.Rows[0]["BuyerId"].ToString()) ? "0" : dtYarnReceive.Rows[0]["BuyerId"].ToString()));
            ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, yarnIssued.BuyerId);
            ddlBuyers.Enabled = false;

            CommonMethods.LoadDropdownById(ddlStyles, yarnIssued.BuyerId, "BuyerStyles", 1, 0);
            //int styleId = int.Parse((string.IsNullOrEmpty(dtYarnReceive.Rows[0]["StyleId"].ToString()) ? "0" : dtYarnReceive.Rows[0]["StyleId"].ToString()));
            ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, yarnIssued.StyleId);
            ddlStyles.Enabled = false;


            string sql2 = $"Exec usp_GetYarnIssuedAtUnitDetailsByIssueId '{YarnIssueId}'";
            dtIssuedYarnAtStoreDetails = unitOfWork.GetDataTableFromSql(sql2);


            if (dtIssuedYarnAtStoreDetails.Rows.Count > 0)
            {
                rptEntryInfo.DataSource = dtIssuedYarnAtStoreDetails;
                rptEntryInfo.DataBind();

            }

            ShowColorAndStyleTotal();

            CreateDataTableForViewState();

            divColors.Visible = true;
            rptEntryInfo.Visible = true;

        }

        private void CreateDataTableForViewState()
        {
            DataTable dtCurrentTable = new DataTable();

            DataRow dr = null;

            //dtCurrentTable.Columns.Add(new DataColumn("BuyerColorId", typeof(string)));
            //dtCurrentTable.Columns.Add(new DataColumn("Lot", typeof(string)));
            //dtCurrentTable.Columns.Add(new DataColumn("SizeAndQty", typeof(string)));
            //dtCurrentTable.Columns.Add(new DataColumn("ColorTotal", typeof(string)));

            dtCurrentTable.Columns.Add(new DataColumn("BuyerColorId", typeof(string)));
            dtCurrentTable.Columns.Add(new DataColumn("ItemId", typeof(string)));
            dtCurrentTable.Columns.Add(new DataColumn("Lot", typeof(string)));
            dtCurrentTable.Columns.Add(new DataColumn("UnitAndQty", typeof(string)));
            //dtCurrentTable.Columns.Add(new DataColumn("ConPerDozen", typeof(string)));
            dtCurrentTable.Columns.Add(new DataColumn("AvailableBalance", typeof(string)));
            dtCurrentTable.Columns.Add(new DataColumn("ColorTotalLbs", typeof(string)));
            //dtCurrentTable.Columns.Add(new DataColumn("ColorTotal", typeof(string)));
            dtCurrentTable.Columns.Add(new DataColumn("Remarks", typeof(string)));





            List<DataTable> unitAndQtyTableList = new List<DataTable>();

            for (int rowIndex = 0; rowIndex < dtIssuedYarnAtStoreDetails.Rows.Count; rowIndex++)
            {


                DropDownList ddlColors = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlColors");
                DropDownList ddlItems = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlItems");
                DropDownList ddlLotNumber = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlLotNumber");
                //Label lblColorTotalValue = (Label)this.rptEntryInfo.Items[rowIndex].FindControl("lblColorTotalValue");
                GridView gvUnitQuantity1 = (GridView)this.rptEntryInfo.Items[rowIndex].FindControl("gvUnitQuantity1");

                Label lblAvailableBalance = (Label)this.rptEntryInfo.Items[rowIndex].FindControl("lblAvailableBalance");
                //TextBox tbxConPerDozen = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxConPerDozen");
                TextBox tbxRemarks = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxRemarks");
                Label lblColorTotalLBS = (Label)this.rptEntryInfo.Items[rowIndex].FindControl("lblColorTotalLBS");

                dr = dtCurrentTable.NewRow();


                dr["BuyerColorId"] = (ddlColors.SelectedValue == "" ? "0" : ddlColors.SelectedValue);
                dr["ItemId"] = (ddlItems.SelectedValue == "" ? "0" : ddlItems.SelectedValue);
                dr["Lot"] = ddlLotNumber.SelectedValue;
                //dr["ColorTotal"] = lblColorTotalValue.Text;
                //dr["ConPerDozen"] = tbxConPerDozen.Text;
                dr["AvailableBalance"] = lblAvailableBalance.Text;
                dr["ColorTotalLbs"] = lblColorTotalLBS.Text;
                dr["Remarks"] = tbxRemarks.Text;



                DataTable unitAndQtyTable = new DataTable();
                unitAndQtyTable.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                unitAndQtyTable.Columns.Add(new DataColumn("Column0", typeof(string)));

                for (int j = 0; j < gvUnitQuantity1.Columns.Count; j++)
                {
                    DataRow unitAndQtyRow = null;
                    unitAndQtyRow = unitAndQtyTable.NewRow();
                    unitAndQtyRow["RowNumber"] = j + 1;
                    unitAndQtyRow["Column0"] = string.Empty;
                    unitAndQtyTable.Rows.Add(unitAndQtyRow);
                    string controlId = string.Concat("tbxReceivQuantity", j.ToString());
                    TextBox tbxSizeQty = (TextBox)gvUnitQuantity1.Rows[0].Cells[j].FindControl(controlId);
                    unitAndQtyTable.Rows[j]["Column0"] = tbxSizeQty.Text;
                }
                unitAndQtyTableList.Add(unitAndQtyTable);
                dtCurrentTable.Rows.InsertAt(dr, dtCurrentTable.Rows.Count);
            }

            this.ViewState["CurrentTable"] = dtCurrentTable;
            this.ViewState["UnitAndQtyTableList"] = unitAndQtyTableList;

        }


        int YarnIssueId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["yarnIssueId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["yarnIssueId"] = value; }
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblStyleTotal.Text = "";

            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
            }
            divColors.Visible = false;
            rptEntryInfo.Visible = false;
            rptEntryInfo.DataSource = null;
            rptEntryInfo.DataBind();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "CacheItems()", true);
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblStyleTotal.Text = "";
            divColors.Visible = false;
            rptEntryInfo.Visible = false;
            rptEntryInfo.DataSource = null;
            rptEntryInfo.DataBind();

            if (ddlStyles.SelectedValue != "")
            {
                SetInitialRowCount();
                divColors.Visible = true;
                rptEntryInfo.Visible = true;
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "CacheItems()", true);
        }


        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;


            dt.Columns.Add(new DataColumn("BuyerColorId", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));
            dt.Columns.Add(new DataColumn("Lot", typeof(string)));
            dt.Columns.Add(new DataColumn("UnitAndQty", typeof(string)));
            //dt.Columns.Add(new DataColumn("ConPerDozen", typeof(string)));
            dt.Columns.Add(new DataColumn("AvailableBalance", typeof(string)));
            dt.Columns.Add(new DataColumn("ColorTotalLbs", typeof(string)));
            //dt.Columns.Add(new DataColumn("ColorTotal", typeof(string)));
            dt.Columns.Add(new DataColumn("Remarks", typeof(string)));


            dr = dt.NewRow();


            dr["BuyerColorId"] = string.Empty;
            dr["ItemId"] = string.Empty;
            dr["Lot"] = string.Empty;
            dr["UnitAndQty"] = string.Empty;
            //dr["ConPerDozen"] = string.Empty;
            dr["AvailableBalance"] = string.Empty;
            dr["ColorTotalLbs"] = string.Empty;
            //dr["ColorTotal"] = string.Empty;
            dr["Remarks"] = string.Empty;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptEntryInfo.DataSource = dt;
            rptEntryInfo.DataBind();
        }



        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var styleId = int.Parse(ddlStyles.SelectedValue);
                //var dtSizes = buyerManager.GetStyleSizes(styleId);
                var dtUnits = (DataTable)this.ViewState["KnittingUnit"];
                var ddlColors = (DropDownList)e.Item.FindControl("ddlColors");
                var ddlItems = (DropDownList)e.Item.FindControl("ddlItems");
                var ddlLotNumber = (DropDownList)e.Item.FindControl("ddlLotNumber");
                var lblAvailableBalance = (Label)e.Item.FindControl("lblAvailableBalance");
                //var tbxConPerDozen = (TextBox)e.Item.FindControl("tbxConPerDozen");
                var tbxRemarks = (TextBox)e.Item.FindControl("tbxRemarks");
                int storeId = 0;
                int selectedColorId = 0;
                if (ddlStore.SelectedValue != "")
                {
                    storeId = int.Parse(ddlStore.SelectedValue);
                }
                LoadColorDropdown(styleId, ddlColors);
                //LoadLOTDropdown(ddlLotNumber, styleId, storeId);
                SetItemDropdown(ddlItems, styleId, storeId);
                var colorSt = DataBinder.Eval(e.Item.DataItem, "BuyerColorId").ToString();
                if (colorSt != "")
                {
                    selectedColorId = int.Parse(colorSt);
                }
                LoadLOTDropdownByColor(ddlLotNumber, styleId, selectedColorId, storeId);
                if (initialLoadForUpdate > 0)
                {
                    var colorId = int.Parse(DataBinder.Eval(e.Item.DataItem, "BuyerColorId").ToString());
                    var itemId = int.Parse(DataBinder.Eval(e.Item.DataItem, "ItemId").ToString());
                    var lotNumber = DataBinder.Eval(e.Item.DataItem, "LotNumber").ToString();
                    ddlColors.SelectedIndex = CommonMethods.MatchDropDownItem(ddlColors, colorId);
                    ddlLotNumber.SelectedValue = lotNumber.ToString();
                    ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, itemId);
                    ShowAvailableBalance(ddlColors, ddlItems, ddlLotNumber, lblAvailableBalance);
                    //tbxConPerDozen.Text = DataBinder.Eval(e.Item.DataItem, "ConPerDozen").ToString();
                    tbxRemarks.Text = DataBinder.Eval(e.Item.DataItem, "Remarks").ToString();
                }


                GridView gvUnitQuantity1 = (GridView)e.Item.FindControl("gvUnitQuantity1");
                Label lblNoSizeFound = (Label)e.Item.FindControl("lblNoSizeFound");


                if (dtUnits.Rows.Count > 0)
                {
                    DataTable dtOneRow = new DataTable();
                    DataRow dr = null;
                    dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                    dr = dtOneRow.NewRow();
                    dr["RowNumber"] = 1;
                    dtOneRow.Rows.Add(dr);

                    TemplateField templateField = null;

                    for (int i = 0; i < dtUnits.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtUnits.Rows[i][1].ToString();
                        gvUnitQuantity1.Columns.Add(templateField);
                    }

                    gvUnitQuantity1.DataSource = dtOneRow;
                    gvUnitQuantity1.DataBind();
                    gvUnitQuantity1.Visible = true;
                    lblNoSizeFound.Visible = false;

                }
                else
                {
                    gvUnitQuantity1.Visible = false;
                    lblNoSizeFound.Visible = true;
                }
            }
        }


        private void LoadColorDropdown(int styleId, DropDownList ddl)
        {
            var dt = this.orderManager.GetColorsByStyleId(styleId);

            var dr = dt.NewRow();
            dr["BuyerColorId"] = "";
            dr["ColorDescription"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);
            ddl.DataSource = dt;
            ddl.DataTextField = "ColorDescription";
            ddl.DataValueField = "BuyerColorId";
            ddl.DataBind();
        }

        private void LoadLOTDropdown(DropDownList ddl, int styleId, int storeId)
        {
            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetYarnReceivedLotNumberByStyleId {styleId},{storeId}");
            var dr = dt.NewRow();
            dr["Value"] = "";
            dr["Text"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);
            ddl.DataSource = dt;
            ddl.DataTextField = "Text";
            ddl.DataValueField = "Value";
            ddl.DataBind();
        }

        private void LoadLOTDropdownByColor(DropDownList ddl, int styleId, int colorId, int storeId)
        {
            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetYarnReceivedLotNumberByStyleIdAndColor '{styleId}','{storeId}','{colorId}'");
            var dr = dt.NewRow();
            dr["Value"] = "";
            dr["Text"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);
            ddl.DataSource = dt;
            ddl.DataTextField = "Text";
            ddl.DataValueField = "Value";
            ddl.DataBind();
        }

        protected void SetItemDropdown(DropDownList ddlItem, int styleId, int storeId)
        {
            var dtItems = unitOfWork.GetDataTableFromSql($"EXEC usp_GetYarnItemsByStyleId {styleId},{storeId}");
            ddlItem.Items.Add(new ListItem("", ""));
            ddlItem.DataSource = dtItems;
            ddlItem.DataTextField = "ItemName";
            ddlItem.DataValueField = "ItemId";
            ddlItem.DataBind();
            ddlItem.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlItem.SelectedIndex = 0;
        }

        protected void gvUnitQuantity1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //var buyerId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId")).Text);
                //var styleId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId")).Text);

                //var dtSizes = buyerManager.GetStyleSizes(int.Parse(ddlStyles.SelectedValue));
                var dtUnits = (DataTable)this.ViewState["KnittingUnit"];
                TextBox txtUnitQty = null;
                Label lblUnitId = null;

                for (int i = 0; i < dtUnits.Rows.Count; i++)
                {
                    txtUnitQty = new TextBox();
                    txtUnitQty.ID = "tbxReceivQuantity" + i.ToString();
                    txtUnitQty.Text = "";
                    txtUnitQty.Width = 60;
                    txtUnitQty.TextMode = TextBoxMode.Number;
                    txtUnitQty.Attributes["min"] = "0";
                    e.Row.Cells[i].Controls.Add(txtUnitQty);


                    lblUnitId = new Label();
                    lblUnitId.ID = "lblUnitId" + i.ToString();
                    lblUnitId.Text = dtUnits.Rows[i][0].ToString();
                    lblUnitId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblUnitId);

                    var entryId = new Label();
                    entryId.ID = "lblEntryId" + i.ToString();
                    entryId.Text = "";
                    entryId.Visible = false;
                    e.Row.Cells[i].Controls.Add(entryId);

                    var unitId = int.Parse(dtUnits.Rows[i][0].ToString());

                    if (YarnIssueId != 0)
                    {
                        var ddlColors = (DropDownList)((GridView)sender).DataItemContainer.FindControl("ddlColors");
                        var ddlItems = (DropDownList)((GridView)sender).DataItemContainer.FindControl("ddlItems");
                        var buyerColorId = int.Parse(string.IsNullOrEmpty(ddlColors.SelectedValue) ? "0" : ddlColors.SelectedValue);
                        var itemId = int.Parse(string.IsNullOrEmpty(ddlItems.SelectedValue) ? "0" : ddlItems.SelectedValue);
                        var ddlLotNumber = (DropDownList)((GridView)sender).DataItemContainer.FindControl("ddlLotNumber");
                        var lotNumber = ddlLotNumber.SelectedValue;

                        //var sizeInfo = unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().Get(x => x.IssueId == YarnIssueId && x.ColorId == buyerColorId && x.ItemId == itemId && x.LotNumber == lotNumber && x.KnittingUnitId == unitId).FirstOrDefault();
                        var unitInfo = unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().Get(x => x.IssueId == YarnIssueId && x.ColorId == buyerColorId && x.ItemId == itemId && x.LotNumber == lotNumber && x.KnittingUnitId == unitId).FirstOrDefault();

                        if (unitInfo != null)
                        {
                            txtUnitQty.Text = unitInfo.IssuedQuantityLbs + "";
                            entryId.Text = unitInfo.IssueId + "";
                        }
                    }

                }
            }
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            this.AddNewRowToGrid();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "CacheItems()", true);
        }

        private void AddNewRowToGrid()
        {


            List<DataTable> unitAndQtyTableList = new List<DataTable>();


            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];

                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {

                        DropDownList ddlColors = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        DropDownList ddlItems = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlItems");

                        DropDownList ddlLotNumber = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlLotNumber");
                        //Label lblColorTotalValue = (Label)this.rptEntryInfo.Items[rowIndex].FindControl("lblColorTotalValue");

                        GridView gvUnitQuantity1 = (GridView)this.rptEntryInfo.Items[rowIndex].FindControl("gvUnitQuantity1");
                        Label lblAvailableBalance = (Label)this.rptEntryInfo.Items[rowIndex].FindControl("lblAvailableBalance");
                        //TextBox tbxConPerDozen = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxConPerDozen");
                        TextBox tbxRemarks = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxRemarks");
                        Label lblColorTotalLBS = (Label)this.rptEntryInfo.Items[rowIndex].FindControl("lblColorTotalLBS");


                        dtCurrentTable.Rows[rowIndex]["BuyerColorId"] = (ddlColors.SelectedValue == "" ? "0" : ddlColors.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["ItemId"] = (ddlItems.SelectedValue == "" ? "0" : ddlItems.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["Lot"] = ddlLotNumber.SelectedValue;
                        //dtCurrentTable.Rows[rowIndex]["ColorTotal"] = lblColorTotalValue.Text;
                        //dtCurrentTable.Rows[rowIndex]["ConPerDozen"] = tbxConPerDozen.Text;
                        dtCurrentTable.Rows[rowIndex]["AvailableBalance"] = lblAvailableBalance.Text;
                        dtCurrentTable.Rows[rowIndex]["ColorTotalLbs"] = lblColorTotalLBS.Text;
                        dtCurrentTable.Rows[rowIndex]["Remarks"] = tbxRemarks.Text;


                        DataTable unitAndQtyTable = new DataTable();
                        unitAndQtyTable.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                        unitAndQtyTable.Columns.Add(new DataColumn("Column0", typeof(string)));

                        for (int j = 0; j < gvUnitQuantity1.Columns.Count; j++)
                        {
                            DataRow unitAndQtyRow = null;
                            unitAndQtyRow = unitAndQtyTable.NewRow();
                            unitAndQtyRow["RowNumber"] = j + 1;
                            unitAndQtyRow["Column0"] = string.Empty;
                            unitAndQtyTable.Rows.Add(unitAndQtyRow);
                            string controlId = string.Concat("tbxReceivQuantity", j.ToString());
                            TextBox tbxSizeQty = (TextBox)gvUnitQuantity1.Rows[0].Cells[j].FindControl(controlId);
                            unitAndQtyTable.Rows[j]["Column0"] = tbxSizeQty.Text;
                        }
                        unitAndQtyTableList.Add(unitAndQtyTable);

                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["BuyerColorId"] = string.Empty;
                            drCurrentRow["ItemId"] = string.Empty;
                            drCurrentRow["Lot"] = string.Empty;
                            drCurrentRow["UnitAndQty"] = string.Empty;
                            //drCurrentRow["ColorTotal"] = string.Empty;
                            //drCurrentRow["ConPerDozen"] = string.Empty;
                            drCurrentRow["AvailableBalance"] = string.Empty;
                            drCurrentRow["ColorTotalLbs"] = string.Empty;
                            drCurrentRow["Remarks"] = string.Empty;
                        }
                    }

                    dtCurrentTable.Rows.InsertAt(drCurrentRow, dtCurrentTable.Rows.Count);

                    this.ViewState["CurrentTable"] = dtCurrentTable;
                    this.ViewState["UnitAndQtyTableList"] = unitAndQtyTableList;
                    this.rptEntryInfo.DataSource = dtCurrentTable;
                    this.rptEntryInfo.DataBind();
                }
            }
            SetPreviousData();
            ShowColorAndStyleTotal();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if ((this.ViewState["CurrentTable"] == null ? false : this.ViewState["UnitAndQtyTableList"] != null))
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                List<DataTable> unitAndQtyTableList = (List<DataTable>)this.ViewState["UnitAndQtyTableList"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {


                        DropDownList ddlColors = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        DropDownList ddlItems = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlItems");
                        DropDownList ddlLotNumber = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlLotNumber");
                        //Label lblColorTotalValue = (Label)this.rptEntryInfo.Items[rowIndex].FindControl("lblColorTotalValue");
                        GridView gvUnitQuantity1 = (GridView)this.rptEntryInfo.Items[rowIndex].FindControl("gvUnitQuantity1");

                        Label lblAvailableBalance = (Label)this.rptEntryInfo.Items[rowIndex].FindControl("lblAvailableBalance");
                        //TextBox tbxConPerDozen = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxConPerDozen");
                        TextBox tbxRemarks = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxRemarks");
                        Label lblColorTotalLBS = (Label)this.rptEntryInfo.Items[rowIndex].FindControl("lblColorTotalLBS");


                        ddlColors.SelectedIndex = CommonMethods.MatchDropDownItem(ddlColors, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["BuyerColorId"].ToString()) ? "0" : dt.Rows[i]["BuyerColorId"].ToString())));
                        ddlItems.SelectedValue = dt.Rows[i]["ItemId"].ToString();
                        ddlLotNumber.SelectedValue = dt.Rows[i]["Lot"].ToString();
                        //lblColorTotalValue.Text = dt.Rows[i]["ColorTotal"].ToString();
                        lblAvailableBalance.Text = dt.Rows[i]["AvailableBalance"].ToString();
                        //tbxConPerDozen.Text = dt.Rows[i]["ConPerDozen"].ToString();
                        tbxRemarks.Text = dt.Rows[i]["Remarks"].ToString();
                        lblColorTotalLBS.Text = dt.Rows[i]["ColorTotalLbs"].ToString();


                        if (i < unitAndQtyTableList.Count)
                        {
                            DataTable tableFromList = unitAndQtyTableList[i];
                            for (int j = 0; j < tableFromList.Rows.Count; j++)
                            {
                                TextBox tbxUnitQty = (TextBox)gvUnitQuantity1.Rows[0].Cells[j].FindControl(string.Concat("tbxReceivQuantity", j.ToString()));
                                tbxUnitQty.Text = tableFromList.Rows[j]["Column0"].ToString();
                            }
                        }
                        rowIndex++;
                    }
                }
            }
        }


        protected void btnSaveEntries_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(tbxIssueDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter issue date.')", true);
            }
            else if (ddlStore.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a store.')", true);
            }
            //else if (ddlUserProductionUnit.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a knitting unit.')", true);
            //}
            else if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select an style.')", true);
            }
            else
            {
                try
                {
                    var issueDate = DateTime.Parse(tbxIssueDate.Text);
                    //int gg = 0;
                    ShowColorAndStyleTotal();
                    decimal styleTotallbs = CalculateStyleTotal();
                    if (styleTotallbs > 0)
                    {
                        // btnSaveEntries.Enabled = false;

                        if (issueDate <= DateTime.Now)
                        {
                            List<YarnIssuedAtStoreV2> lstYarnIssuedAtStore = new List<YarnIssuedAtStoreV2>();
                            //var yias = new YarnIssuedAtStore()
                            //{
                            //    KnittingUnitId = int.Parse(ddlUserProductionUnit.SelectedValue),
                            //    IssueDate = issueDate,
                            //    StoreId = int.Parse(ddlStore.SelectedValue),
                            //    BuyerId = int.Parse(ddlBuyers.SelectedValue),
                            //    StyleId = int.Parse(ddlStyles.SelectedValue),
                            //    IssuedQuantity = styleTotal,
                            //    IssuedQuantityLbs = styleTotallbs,
                            //    CreateDate = DateTime.Now,
                            //    CreatedBy = CommonMethods.SessionInfo.UserName
                            //};

                            var flag = true;

                            for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                            {

                                var ddlColors = (DropDownList)rptEntryInfo.Items[i].FindControl("ddlColors");
                                var ddlItems = (DropDownList)rptEntryInfo.Items[i].FindControl("ddlItems");
                                var ddlLotNumber = (DropDownList)rptEntryInfo.Items[i].FindControl("ddlLotNumber");
                                var lblAvailableBalance = (Label)rptEntryInfo.Items[i].FindControl("lblAvailableBalance");
                                //var tbxConPerDozen = (TextBox)rptEntryInfo.Items[i].FindControl("tbxConPerDozen");
                                var tbxRemarks = (TextBox)rptEntryInfo.Items[i].FindControl("tbxRemarks");
                                var lblColorTotalLBS = (Label)rptEntryInfo.Items[i].FindControl("lblColorTotalLBS");

                                //decimal conPerDozen = 0;
                                //decimal conPerPcs = 0;
                                //if (!string.IsNullOrEmpty(tbxConPerDozen.Text))
                                //{
                                //    conPerDozen = decimal.Parse(tbxConPerDozen.Text);
                                //    conPerPcs = conPerDozen / 12;
                                //}
                                if (ddlColors.SelectedValue != "" && ddlItems.SelectedValue != "" && ddlLotNumber.SelectedValue != "" && lblAvailableBalance.Text != "")
                                {

                                    var buyerColorId = int.Parse(ddlColors.SelectedValue);
                                    if (decimal.Parse(lblAvailableBalance.Text) < decimal.Parse(lblColorTotalLBS.Text))
                                    {
                                        flag = false;
                                    }

                                    //var lotNumber = ((TextBox)rptEntryInfo.Items[i].FindControl("tbxLotNumber")).Text;

                                    var gvUnitQuantity1 = (GridView)rptEntryInfo.Items[i].FindControl("gvUnitQuantity1");
                                    for (int j = 0; j < gvUnitQuantity1.Columns.Count; j++)
                                    {
                                        var unitId = int.Parse(((Label)gvUnitQuantity1.Rows[0].Cells[j].FindControl("lblUnitId" + j)).Text);
                                        var recQty = ((TextBox)gvUnitQuantity1.Rows[0].Cells[j].FindControl("tbxReceivQuantity" + j)).Text;
                                        if (!string.IsNullOrEmpty(recQty))
                                        {
                                            var yarnIssuedAtStore = new YarnIssuedAtStoreV2()
                                            {
                                                StoreId = int.Parse(ddlStore.SelectedValue),
                                                KnittingUnitId = unitId,
                                                IssueDate = issueDate,
                                                BuyerId = int.Parse(ddlBuyers.SelectedValue),
                                                StyleId = int.Parse(ddlStyles.SelectedValue),
                                                ColorId = buyerColorId,
                                                ItemId = int.Parse(ddlItems.SelectedValue),
                                                LotNumber = ddlLotNumber.SelectedValue,
                                                IssuedQuantityLbs = decimal.Parse(recQty),
                                                CreateDate = DateTime.Now,
                                                CreatedBy = CommonMethods.SessionInfo.UserName
                                            };
                                            lstYarnIssuedAtStore.Add(yarnIssuedAtStore);
                                        }
                                    }
                                }
                            }
                            if (flag)
                            {
                                if (lstYarnIssuedAtStore != null && lstYarnIssuedAtStore.Count > 0)
                                {
                                    foreach (var item in lstYarnIssuedAtStore)
                                    {
                                        unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().Insert(item);
                                    }
                                    unitOfWork.Save();
                                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                                    //Response.AddHeader("REFRESH", "2;URL=IssueYarnAtStoreV2.aspx");
                                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('IssueYarnAtStoreV3.aspx');", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter all valid information.')", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You can not issue more that available balance.')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cannot issue in a future date')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('No data was entered.')", true);
                    }

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }

        }

        protected void btnUpdateEntries_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(tbxIssueDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter issue date.')", true);
            }
            else if (ddlStore.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a store.')", true);
            }
            //else if (ddlUserProductionUnit.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a knitting unit.')", true);
            //}
            else if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select an style.')", true);
            }
            else
            {
                try
                {
                    var issueDate = DateTime.Parse(tbxIssueDate.Text);
                    decimal styleTotalLbs = CalculateStyleTotal();
                    if (styleTotalLbs > 0)
                    {
                        //btnUpdateEntries.Enabled = false;
                        if (issueDate <= DateTime.Now)
                        {
                            List<YarnIssuedAtStoreV2> lstYarnIssuedUpdate = new List<YarnIssuedAtStoreV2>();
                            List<YarnIssuedAtStoreV2> lstYarnIssuedInsert = new List<YarnIssuedAtStoreV2>();
                            //var yias = unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().GetByID(YarnIssueId);
                            //List<YarnIssuedAtStoreColorSize> lstColorSizes = new List<YarnIssuedAtStoreColorSize>();
                            //var oldColorSize = unitOfWork.GenericRepositories<YarnIssuedAtStoreColorSize>().Get(x => x.IssueId == YarnIssueId).ToList();
                            //foreach (var item in oldColorSize)
                            //{
                            //    unitOfWork.GenericRepositories<YarnIssuedAtStoreColorSize>().Delete(item);
                            //}

                            //yias.KnittingUnitId = int.Parse(ddlUserProductionUnit.SelectedValue);
                            //yias.StoreId = int.Parse(ddlStore.SelectedValue);
                            //yias.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                            //yias.StyleId = int.Parse(ddlStyles.SelectedValue);
                            //yias.IssuedQuantity = styleTotal;
                            //yias.IssueDate = DateTime.Parse(tbxIssueDate.Text);
                            //yias.IssuedQuantityLbs = styleTotalLbs;
                            //yias.UpdateDate = DateTime.Now;
                            //yias.UpdatedBy = CommonMethods.SessionInfo.UserName;

                            var flag = true;

                            for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                            {

                                var ddlColors = (DropDownList)rptEntryInfo.Items[i].FindControl("ddlColors");
                                var ddlItems = (DropDownList)rptEntryInfo.Items[i].FindControl("ddlItems");
                                var ddlLotNumber = (DropDownList)rptEntryInfo.Items[i].FindControl("ddlLotNumber");
                                var lblAvailableBalance = (Label)rptEntryInfo.Items[i].FindControl("lblAvailableBalance");
                                //var tbxConPerDozen = (TextBox)rptEntryInfo.Items[i].FindControl("tbxConPerDozen");
                                var tbxRemarks = (TextBox)rptEntryInfo.Items[i].FindControl("tbxRemarks");
                                var lblColorTotalLBS = (Label)rptEntryInfo.Items[i].FindControl("lblColorTotalLBS");

                                //decimal conPerDozen = 0;
                                //decimal conPerPcs = 0;
                                //if (!string.IsNullOrEmpty(tbxConPerDozen.Text))
                                //{
                                //    conPerDozen = decimal.Parse(tbxConPerDozen.Text);
                                //    conPerPcs = conPerDozen / 12;
                                //}
                                if (ddlColors.SelectedValue != "" && ddlLotNumber.SelectedValue != "" && lblAvailableBalance.Text != "")
                                {

                                    var buyerColorId = int.Parse(ddlColors.SelectedValue);
                                    if (decimal.Parse(lblAvailableBalance.Text) < decimal.Parse(lblColorTotalLBS.Text))
                                    {
                                        flag = false;
                                    }

                                    //var lotNumber = ((TextBox)rptEntryInfo.Items[i].FindControl("tbxLotNumber")).Text;

                                    var gvUnitQuantity1 = (GridView)rptEntryInfo.Items[i].FindControl("gvUnitQuantity1");
                                    for (int j = 0; j < gvUnitQuantity1.Columns.Count; j++)
                                    {
                                        var unitId = int.Parse(((Label)gvUnitQuantity1.Rows[0].Cells[j].FindControl("lblUnitId" + j)).Text);
                                        var recQty = ((TextBox)gvUnitQuantity1.Rows[0].Cells[j].FindControl("tbxReceivQuantity" + j)).Text;
                                        var lblIssueId = ((Label)gvUnitQuantity1.Rows[0].Cells[j].FindControl("lblEntryId" + j)).Text;
                                        if (!string.IsNullOrEmpty(recQty))
                                        {
                                            if (!string.IsNullOrEmpty(lblIssueId))
                                            {
                                                var yarnIssueId = int.Parse(lblIssueId);
                                                var yias = unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().GetByID(yarnIssueId);

                                                yias.StoreId = int.Parse(ddlStore.SelectedValue);
                                                yias.KnittingUnitId = unitId;
                                                yias.IssueDate = issueDate;
                                                yias.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                                                yias.StyleId = int.Parse(ddlStyles.SelectedValue);
                                                yias.ColorId = buyerColorId;
                                                yias.ItemId = int.Parse(ddlItems.SelectedValue);
                                                yias.LotNumber = ddlLotNumber.SelectedValue;
                                                yias.IssuedQuantityLbs = decimal.Parse(recQty);
                                                yias.UpdateDate = DateTime.Now;
                                                yias.UpdatedBy = CommonMethods.SessionInfo.UserName;
                                                lstYarnIssuedUpdate.Add(yias);
                                            }
                                            else
                                            {
                                                var yarnIssuedAtStore = new YarnIssuedAtStoreV2()
                                                {
                                                    StoreId = int.Parse(ddlStore.SelectedValue),
                                                    KnittingUnitId = unitId,
                                                    IssueDate = issueDate,
                                                    BuyerId = int.Parse(ddlBuyers.SelectedValue),
                                                    StyleId = int.Parse(ddlStyles.SelectedValue),
                                                    ColorId = buyerColorId,
                                                    ItemId = int.Parse(ddlItems.SelectedValue),
                                                    LotNumber = ddlLotNumber.SelectedValue,
                                                    IssuedQuantityLbs = decimal.Parse(recQty),
                                                    CreateDate = DateTime.Now,
                                                    CreatedBy = CommonMethods.SessionInfo.UserName
                                                };
                                                lstYarnIssuedInsert.Add(yarnIssuedAtStore);
                                            }

                                        }
                                    }
                                }
                            }
                            if (flag)
                            {
                                foreach (var item in lstYarnIssuedUpdate)
                                {
                                    unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().Update(item);
                                }
                                foreach (var item in lstYarnIssuedInsert)
                                {
                                    unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().Insert(item);
                                }
                                unitOfWork.Save();
                                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                                //Response.AddHeader("REFRESH", "2;URL=ViewIssuedYarnAtStore.aspx");
                                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewIssuedYarnAtStoreV3.aspx');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You can not issue more that available balance.')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cannot issue in future date')", true);
                        }

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter size quantity!')", true);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }

            }

        }

        protected void lnkbtnCalculateColorTotal_Click(object sender, EventArgs e)
        {
            ShowColorAndStyleTotal();
        }

        private void ShowColorAndStyleTotal()
        {
            decimal colorTotal = 0;
            decimal styleTotal = 0;
            decimal styleTotalLbs = 0;

            for (int i = 0; i < rptEntryInfo.Items.Count; i++)
            {

                //Label lblColorTotalValue = (Label)rptEntryInfo.Items[i].FindControl("lblColorTotalValue");

                GridView gvUnitQuantity1 = (GridView)rptEntryInfo.Items[i].FindControl("gvUnitQuantity1");
                var lblTotalInLBS = (Label)rptEntryInfo.Items[i].FindControl("lblColorTotalLBS");
                //var tbxConPerDozen = (TextBox)rptEntryInfo.Items[i].FindControl("tbxConPerDozen");

                colorTotal = 0;

                for (int j = 0; j < gvUnitQuantity1.Columns.Count; j++)
                {
                    string sizeQuantityTextBox2Id = string.Concat("tbxReceivQuantity", j.ToString());
                    TextBox tbxSizeQty = (TextBox)gvUnitQuantity1.Rows[0].Cells[j].FindControl(sizeQuantityTextBox2Id);

                    if (tbxSizeQty.Text != "")
                    {
                        colorTotal += decimal.Parse(tbxSizeQty.Text);
                    }
                }
                lblTotalInLBS.Text = colorTotal.ToString();
                //if(i == 0)
                //{
                styleTotal += colorTotal;
                //}

                //lblTotalInLBS.Text = colorTotal + "";
            }

            //lblStyleTotal.Text = "Style Total: " + styleTotal.ToString() + "(pcs)";
            lblStyleTotalLbs.Text = "Style Total: " + styleTotal.ToString() + "(lbs)";
        }

        private void CalculateRowColorTotalAndShowIntoLabel(GridView gvUnitQuantity, Label label)
        {
            var colorTotal = 0;
            for (int j = 0; j < gvUnitQuantity.Columns.Count; j++)
            {
                string unitQuantityTextBox2Id = string.Concat("tbxReceivQuantity", j.ToString());
                TextBox tbxUnitQty = (TextBox)gvUnitQuantity.Rows[0].Cells[j].FindControl(unitQuantityTextBox2Id);

                if (tbxUnitQty.Text != "")
                {
                    colorTotal += int.Parse(tbxUnitQty.Text);
                }
            }
            label.Text = colorTotal + "";
        }


        private decimal CalculateStyleTotal()
        {

            //int styleTotal = 0;
            decimal styleTotalLbs = 0;
            for (int i = 0; i < this.rptEntryInfo.Items.Count; i++)
            {
                GridView gvUnitQuantity1 = (GridView)this.rptEntryInfo.Items[i].FindControl("gvUnitQuantity1");
                //TextBox tbxConPerDozen = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxConPerDozen");
                for (int j = 0; j < gvUnitQuantity1.Columns.Count; j++)
                {
                    string unitQuantityTextBox2Id = string.Concat("tbxReceivQuantity", j.ToString());
                    TextBox tbxUnitQty = (TextBox)gvUnitQuantity1.Rows[0].Cells[j].FindControl(unitQuantityTextBox2Id);

                    if (tbxUnitQty.Text != "")
                    {
                        styleTotalLbs += int.Parse(tbxUnitQty.Text);
                    }
                }
            }
            return styleTotalLbs;
        }

        private void ShowAvailableBalance(DropDownList ddlColor, DropDownList ddlItems, DropDownList ddlLotNumber, Label lblAB)
        {
            if (ddlColor.SelectedValue != "" && ddlItems.SelectedValue != "" && ddlLotNumber.SelectedValue != "")
            {
                var colorId = int.Parse(ddlColor.SelectedValue);
                int storeId = 0;
                if (ddlStore.SelectedValue != "")
                {
                    storeId = int.Parse(ddlStore.SelectedValue);
                }
                var itemId = 0;
                if (ddlItems.SelectedValue != "")
                {
                    itemId = int.Parse(ddlItems.SelectedValue);
                }
                var availableBalance = decimal.Parse(unitOfWork.GetSingleStringValue($"EXEC usp_GetAvailableBalanceByStyleLotColorV2 {ddlStyles.SelectedValue},'{ddlLotNumber.SelectedValue}',{colorId},{storeId},{itemId}"));

                if (YarnIssueId > 0)
                {
                    var issuedQty = unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().Get(x => x.IssueId == YarnIssueId).FirstOrDefault().IssuedQuantityLbs;
                    availableBalance += issuedQty;
                }
                lblAB.Text = availableBalance + "";
            }
            else
            {
                lblAB.Text = "";
            }
        }

        protected void SelectedIndexChanged(object sender, EventArgs e)
        {
            var ddl = (DropDownList)sender;
            var ddlColors = (DropDownList)ddl.NamingContainer.FindControl("ddlColors");
            var ddlItems = (DropDownList)ddl.NamingContainer.FindControl("ddlItems");
            var ddlLotNumber = (DropDownList)ddl.NamingContainer.FindControl("ddlLotNumber");
            var lblAvailableBalance = (Label)ddl.NamingContainer.FindControl("lblAvailableBalance");
            ShowAvailableBalance(ddlColors, ddlItems, ddlLotNumber, lblAvailableBalance);
        }

        protected void ddlStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblStyleTotal.Text = "";
            divColors.Visible = false;
            rptEntryInfo.Visible = false;
            rptEntryInfo.DataSource = null;
            rptEntryInfo.DataBind();

            if (ddlStyles.SelectedValue != "")
            {
                SetInitialRowCount();
                divColors.Visible = true;
                rptEntryInfo.Visible = true;
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "CacheItems()", true);
        }

        protected void ddlColors_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ddlColor = (DropDownList)sender;
            var ddlItems = (DropDownList)ddlColor.NamingContainer.FindControl("ddlItems");
            var ddlLotNumber = (DropDownList)ddlColor.NamingContainer.FindControl("ddlLotNumber");
            var lblAvailableBalance = (Label)ddlColor.NamingContainer.FindControl("lblAvailableBalance");
            ShowAvailableBalance(ddlColor, ddlItems, ddlLotNumber, lblAvailableBalance);
            if (ddlColor.SelectedValue != "" && ddlStyles.SelectedValue != "" && ddlStore.SelectedValue != "")
            {
                var styleId = Int32.Parse(ddlStyles.SelectedValue);
                var storeId = Int32.Parse(ddlStore.SelectedValue);
                var colorId = Int32.Parse(ddlColor.SelectedValue);
                LoadLOTDropdownByColor(ddlLotNumber, styleId, colorId, storeId);
            }
            else
            {
                ddlLotNumber.Items.Clear();
            }
        }

        //protected void tbxConPerDozen_TextChanged(object sender, EventArgs e)
        //{
        //    var tbxConPerDozen = (TextBox)sender;
        //    var lblTotalInLBS = (Label)((RepeaterItem)tbxConPerDozen.NamingContainer).FindControl("lblColorTotalLBS");
        //    if (string.IsNullOrEmpty(tbxConPerDozen.Text))
        //    {
        //        CalculateRowColorTotalAndShowIntoLabel
        //    }
        //}
    }
}