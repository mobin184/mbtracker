﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class ViewReceivedYarnAtUnit : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        int grandTotal = 0;


        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                //EnableDisableEditButton();
                //EnableDisableDeleteButton();
                CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteKnittedPartsDeliveryInfo(YarnReceiveId);
                }
            }

        }



        private void DeleteKnittedPartsDeliveryInfo(int yarnReceiveId)
        {
            try
            {
                var lstColorSizes = unitOfWork.GenericRepositories<YarnReceivedAtKnittingUnitColorSizes>().Get(x => x.YarnReceivedAtKnittingUnitId == yarnReceiveId).ToList();
                foreach (var item in lstColorSizes)
                {
                    unitOfWork.GenericRepositories<YarnReceivedAtKnittingUnitColorSizes>().Delete(item);
                }
                unitOfWork.GenericRepositories<YarnReceivedAtKnittingUnit>().Delete(yarnReceiveId);
                unitOfWork.Save();
                LoadData();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewReceivedYarnAtUnit", 2, 1);

        }
        private void EnableDisableEditButton()
        {

            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Knitting - Mgmt,Knitting - Floor");

            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewReceivedYarnAtUnit", 1, 1);



        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
            }
        }
        protected void btnViewEntries_Click(object sender, EventArgs e)
        {
            try
            {
                LoadData();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void LoadData()
        {

            divSummary.Visible = false;
            divColors.Visible = false;
            rptDetailInfo.DataSource = null;
            rptDetailInfo.DataBind();


            if (!string.IsNullOrEmpty(tbxReceivedDate.Text) && ddlKnittingUnits.SelectedValue != "")
            {
                var knittingUnit = int.Parse(ddlKnittingUnits.SelectedValue);
                var receivedDate = DateTime.Parse(tbxReceivedDate.Text);

                var buyerId = 0;
                var styleId = 0;
                if (!string.IsNullOrEmpty(ddlBuyers.SelectedValue))
                {
                    buyerId = int.Parse(ddlBuyers.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlStyles.SelectedValue))
                {
                    styleId = int.Parse(ddlStyles.SelectedValue);
                }

                var dtEntries = unitOfWork.GetDataTableFromSql($"Exec usp_GetYarnReceivedAtKnittingUnitByUserAndDate {CommonMethods.SessionInfo.UserId}, '{knittingUnit}', '{receivedDate}','{buyerId}','{styleId}'");

                if (dtEntries.Rows.Count > 0)
                {
                    dtEntries = dtEntries.CheckEditDeletePermission(CommonMethods.SessionInfo.RoleId, "ViewReceivedYarnAtUnit");
                    rptSummary.DataSource = dtEntries;
                    rptSummary.DataBind();
                    rptSummary.Visible = true;
                    lblEntryNotFound.Visible = false;
                    dataDiv.Visible = true;


                    lblHeaderMessage.Text = "Yarn Received at: " + ddlKnittingUnits.SelectedItem.Text;

                    lblGrandTotal.Text = "Total Received Quantity: " + grandTotal.ToString();
                }
                else
                {
                    rptSummary.DataSource = null;
                    rptSummary.DataBind();
                    rptSummary.Visible = false;
                    lblEntryNotFound.Visible = true;
                    dataDiv.Visible = false;
                }
                divSummary.Visible = true;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select knitting unit and delivery date.')", true);
            }
        }


        protected void rptSummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                int receivedQty = int.Parse(DataBinder.Eval(e.Item.DataItem, "ReceivedQty").ToString());
                grandTotal = grandTotal + receivedQty;
            }
        }



        protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        {
            YarnReceiveId = int.Parse(e.CommandArgument.ToString());

            var dtRecColors = unitOfWork.GetDataTableFromSql($"Exec usp_GetYarnReceivedAtUnitDetailsByReceiveId {YarnReceiveId}");

            
            if (dtRecColors.Rows.Count > 0)
            {
                lblEntry.Text = "Details of Received Yarn: Buyer: " + dtRecColors.Rows[0][0].ToString() + ", Style: " + dtRecColors.Rows[0][1].ToString() + ", Quantity: " + dtRecColors.Rows[0][7].ToString();


                divColors.Visible = true;
                rptDetailInfo.DataSource = dtRecColors;
                rptDetailInfo.DataBind();
                rptDetailInfo.Visible = true;
                lblNoDataFound.Visible = false;
            }
            else
            {
                rptDetailInfo.DataSource = null;
                rptDetailInfo.DataBind();
                rptDetailInfo.Visible = false;
                lblNoDataFound.Visible = true;
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);

        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            var yarnReceiveId = int.Parse(e.CommandArgument.ToString());
            Response.Redirect("ReceiveYarnAtUnit.aspx?YarnReceiveId=" + Tools.UrlEncode(yarnReceiveId + ""));
        }

        int YarnReceiveId
        {
            set { ViewState["yarnReceiveId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["yarnReceiveId"]);
                }
                catch
                {
                    return 0;
                }
            }

        }


        protected void rptDetailInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var buyerId = int.Parse(((Label)e.Item.FindControl("lblBuyerId")).Text);
                var styleId = int.Parse(((Label)e.Item.FindControl("lblStyleId")).Text);
                var dtSizes = buyerManager.GetStyleSizes(styleId);

                GridView gvSizeQuantity1 = (GridView)e.Item.FindControl("gvSizeQuantity1");
                Label lblNoSizeFound = (Label)e.Item.FindControl("lblNoSizeFound");
                if (dtSizes.Rows.Count > 0)
                {
                    DataTable dtOneRow = new DataTable();
                    DataRow dr = null;
                    dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                    dr = dtOneRow.NewRow();
                    dr["RowNumber"] = 1;
                    dtOneRow.Rows.Add(dr);

                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantity1.Columns.Add(templateField);
                    }

                    gvSizeQuantity1.DataSource = dtOneRow;
                    gvSizeQuantity1.DataBind();
                    gvSizeQuantity1.Visible = true;
                    lblNoSizeFound.Visible = false;

                }
                else
                {
                    gvSizeQuantity1.Visible = false;
                    lblNoSizeFound.Visible = true;
                }
            }
        }

        protected void gvSizeQuantity1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var buyerId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId")).Text);
                var styleId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId")).Text);
                //var dtSizes = buyerManager.GetSizes(buyerId);
                var dtSizes = buyerManager.GetStyleSizes(styleId);

                Label txtSizeQty = null;
                Label lblSizeId = null;

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new Label();
                    txtSizeQty.ID = "tbxReceivQuantity" + i.ToString();
                    txtSizeQty.Text = "";
                    txtSizeQty.Width = 70;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);


                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);

                    var entryId = new Label();
                    entryId.ID = "lblentryId" + i.ToString();
                    entryId.Text = "";
                    entryId.Visible = false;
                    e.Row.Cells[i].Controls.Add(entryId);


                    var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());

                    var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                    var buyerColorId = int.Parse(string.IsNullOrEmpty(lblBuyerColorId.Text) ? "0" : lblBuyerColorId.Text);

                    var lblLotNumber = (Label)((GridView)sender).DataItemContainer.FindControl("lblLotNumber");

                    if (buyerColorId != 0)
                    {
                        var sizeInfo = unitOfWork.GenericRepositories<YarnReceivedAtKnittingUnitColorSizes>().Get(x => x.YarnReceivedAtKnittingUnitId == YarnReceiveId && x.ColorId == buyerColorId && x.LotNumber == lblLotNumber.Text && x.SizeId == sizeId).FirstOrDefault();
                        if (sizeInfo != null)
                        {
                            txtSizeQty.Text = sizeInfo.ReceivedQty + "";
                            entryId.Text = sizeInfo.Id + "";
                        }
                    }
                }
            }
        }

        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            YarnReceiveId = int.Parse(e.CommandArgument.ToString());
            if (YarnReceiveId != 0)
            {
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);

                //Page.ClientScript.RegisterStartupScript(this.GetType(), "toastr_message", "toastr.success('Are you working?')", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "toastr_message", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);

            }
            else
            {
                var title = "Error";
                var msg = "Sorry, you can not delete it.<br />Something went wrong.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            }
        }


    }
}