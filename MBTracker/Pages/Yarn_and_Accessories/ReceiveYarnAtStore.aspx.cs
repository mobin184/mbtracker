﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class ReceiveYarnAtStore : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        DataTable dtColors = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                if (Request.QueryString["receivedId"] != null)
                {
                    ReceivedId = int.Parse(Tools.UrlDecode(Request.QueryString["receivedId"]));
                    var recInfo = unitOfWork.GenericRepositories<YarnReceived>().Get(x => x.Id == ReceivedId).FirstOrDefault();
                    BuyerColorId = recInfo.BuyerColorId;
                    BindStylesByBuyer(recInfo.BuyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, recInfo.StyleId);
                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, recInfo.BuyerId);

                    ddlStyles.Enabled = false;
                    ddlBuyers.Enabled = false;

                    SetInitialRow(recInfo.StyleId);
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                    sizeActionTitle.Text = "Update Yarn Received";
                }
            }
        }

        int ReceivedId
        {
            set { ViewState["receivedId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["receivedId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }
        int BuyerColorId
        {
            set { ViewState["buyerColorId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["buyerColorId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }
        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            else
            {
                ddlStyles.Items.Clear();
            }

            pnlYarnReceive.Visible = false;
            lblNoColorFound.Visible = false;
            rpt.DataSource = null;
            rpt.DataBind();
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlYarnReceive.Visible = false;
            lblNoColorFound.Visible = false;
            if (ddlStyles.SelectedValue != "")
            {
                var styleId = Convert.ToInt32(ddlStyles.SelectedValue);
                SetInitialRow(styleId);
            }
        }

        private void SetInitialRow(int styleId)
        {
            var dt = unitOfWork.GetDataTableFromSql($"SELECT 1 as RowNumber,{styleId} as StyleId,'' as YarnComposition, '' as LCNumber");
            dtColors = unitOfWork.GetDataTableFromSql($"Exec usp_GetStyleColorsByStyleId {styleId}");
            if (ReceivedId > 0)
            {
                DataRow[] result = dtColors.Select();
                foreach (DataRow row in result)
                {
                    if (int.Parse(row["BuyerColorId"].ToString()) != BuyerColorId)
                        dtColors.Rows.Remove(row);
                }
            }
            if (dtColors.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                rpt.Visible = true;
                pnlYarnReceive.Visible = true;
                lblNoColorFound.Visible = false;
            }
            else
            {
                pnlYarnReceive.Visible = false;
                lblNoColorFound.Visible = true;
            }

            ViewState["rpt"] = dt;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<YarnReceived> lstYarnReceived = new List<YarnReceived>();
                var isValid = true;
                var buyerId = int.Parse(ddlBuyers.SelectedValue);
                var styleId = int.Parse(ddlStyles.SelectedValue);
                var userName = CommonMethods.SessionInfo.UserName;
                for (int i = 0; i < rpt.Items.Count; i++)
                {
                    var tbxYarnComposition = (TextBox)rpt.Items[i].FindControl("tbxYarnComposition");
                    var tbxLCNumber = (TextBox)rpt.Items[i].FindControl("tbxLCNumber");
                    var rptColors = (Repeater)rpt.Items[i].FindControl("rptColors");
                    if (string.IsNullOrEmpty(tbxYarnComposition.Text))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter yarn compostion.');", true);
                        isValid = false;
                        break;
                    }
                    else
                    {

                        for (int j = 0; j < rptColors.Items.Count; j++)
                        {
                            var tbxReceivedQty = (TextBox)rptColors.Items[j].FindControl("tbxReceivedQty");
                            var lblBuyerColorId = (Label)rptColors.Items[j].FindControl("lblBuyerColorId");
                            if (!string.IsNullOrEmpty(tbxReceivedQty.Text))
                            {
                                var yarnRec = new YarnReceived()
                                {
                                    BuyerId = buyerId,
                                    StyleId = styleId,
                                    YarnComposition = tbxYarnComposition.Text,
                                    LCNumber = tbxLCNumber.Text,
                                    BuyerColorId = int.Parse(lblBuyerColorId.Text),
                                    ReceivedQty = int.Parse(tbxReceivedQty.Text),
                                    ReceivedDate = DateTime.Now,
                                    CreatedBy = userName,
                                    CreateDate = DateTime.Now
                                };
                                lstYarnReceived.Add(yarnRec);
                            }
                        }
                    }
                }

                if (isValid)
                {
                    if (lstYarnReceived.Count > 0)
                    {
                        foreach (var item in lstYarnReceived)
                        {
                            unitOfWork.GenericRepositories<YarnReceived>().Insert(item);
                        }
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter received quantity.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                var recInfo = unitOfWork.GenericRepositories<YarnReceived>().GetByID(ReceivedId);
                if (recInfo != null)
                {
                    var tbxYarnComposition = (TextBox)rpt.Items[0].FindControl("tbxYarnComposition");
                    var tbxLCNumber = (TextBox)rpt.Items[0].FindControl("tbxLCNumber");
                    var rptColors = (Repeater)rpt.Items[0].FindControl("rptColors");
                    var tbxReceivedQty = (TextBox)rptColors.Items[0].FindControl("tbxReceivedQty");
                    if (!string.IsNullOrEmpty(tbxYarnComposition.Text))
                    {
                        if (!string.IsNullOrEmpty(tbxReceivedQty.Text))
                        {
                            recInfo.YarnComposition = tbxYarnComposition.Text;
                            recInfo.LCNumber = tbxLCNumber.Text;
                            recInfo.ReceivedQty = int.Parse(tbxReceivedQty.Text);
                            recInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                            recInfo.UpdateDate = DateTime.Now;
                            unitOfWork.GenericRepositories<YarnReceived>().Update(recInfo);
                            unitOfWork.Save();

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                            ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewReceivedYarn.aspx');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter received quantity.');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter yarn composition.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Invalid received id.');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (dtColors == null || dtColors.Rows.Count <= 0)
                {
                    var styleId = int.Parse(ddlStyles.SelectedValue);
                    dtColors = unitOfWork.GetDataTableFromSql($"Exec usp_GetStyleColorsByStyleId {styleId}");
                }

                if (ReceivedId > 0)
                {
                    DataRow[] result = dtColors.Select();
                    foreach (DataRow row in result)
                    {
                        if (int.Parse(row["BuyerColorId"].ToString()) != BuyerColorId)
                            dtColors.Rows.Remove(row);
                    }

                    ((LinkButton)e.Item.FindControl("lnkbtnDelete")).Visible = false;
                }

                var rptColors = (Repeater)e.Item.FindControl("rptColors");
                rptColors.DataSource = dtColors;
                rptColors.DataBind();
            }
            if (e.Item.ItemType == ListItemType.Footer)
            {
                if (ReceivedId > 0)
                {
                    ((Button)e.Item.FindControl("btnAddRow")).Visible = false;
                }

            }
        }

        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {

            var rpt = (Repeater)((LinkButton)sender).Parent.Parent;
            int rowIndex = Convert.ToInt32(e.CommandArgument.ToString());
            DataTable dtCurrentTable = (DataTable)ViewState["rpt"];
            DataRow drDeleteRow = dtCurrentTable.Rows[rowIndex];
            drDeleteRow.Delete();
            dtCurrentTable.AcceptChanges();

            //for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
            //{
            //    dtCurrentTable.Rows[i - 1]["RowNumber"] = i;
            //}

            ViewState["rpt"] = dtCurrentTable;

            rpt.DataSource = dtCurrentTable;
            rpt.DataBind();
            SetPreviousData();
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            if (ViewState["rpt"] != null)
            {
                int rowIndex;
                DataTable dtCurrentTable = (DataTable)ViewState["rpt"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {
                        TextBox tbxYarnComposition = (TextBox)rpt.Items[rowIndex].FindControl("tbxYarnComposition");
                        TextBox tbxLCNumber = (TextBox)rpt.Items[rowIndex].FindControl("tbxLCNumber");
                        Repeater rptColors = (Repeater)rpt.Items[rowIndex].FindControl("rptColors");

                        dtCurrentTable.Rows[rowIndex]["StyleId"] = int.Parse(ddlStyles.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["YarnComposition"] = tbxYarnComposition.Text;
                        dtCurrentTable.Rows[rowIndex]["LCNumber"] = tbxLCNumber.Text;
                        var dtcolor = new DataTable();
                        dtcolor.Columns.Add("ReceivedQty", typeof(string));
                        for (int i = 0; i < rptColors.Items.Count; i++)
                        {
                            var drcolor = dtcolor.NewRow();
                            drcolor["ReceivedQty"] = ((TextBox)rptColors.Items[i].FindControl("tbxReceivedQty")).Text;
                            dtcolor.Rows.Add(drcolor);
                        }

                        ViewState[rptColors.UniqueID] = dtcolor;
                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["RowNumber"] = rowIndex + 1;

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["rpt"] = dtCurrentTable;


                    rpt.DataSource = dtCurrentTable;
                    rpt.DataBind();
                }
                else
                {
                    SetInitialRow(int.Parse(ddlStyles.SelectedValue));
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
            SetPreviousData();
        }

        private void SetPreviousData()
        {

            if (ViewState["rpt"] != null)
            {
                DataTable dt = (DataTable)ViewState["rpt"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        TextBox tbxYarnComposition = (TextBox)rpt.Items[i].FindControl("tbxYarnComposition");
                        TextBox tbxLCNumber = (TextBox)rpt.Items[i].FindControl("tbxLCNumber");
                        Repeater rptColors = (Repeater)rpt.Items[i].FindControl("rptColors");

                        tbxYarnComposition.Text = dt.Rows[i]["YarnComposition"].ToString();
                        tbxLCNumber.Text = dt.Rows[i]["LCNumber"].ToString();

                        if (ViewState[rptColors.UniqueID] != null)
                        {
                            var dtcolor = (DataTable)ViewState[rptColors.UniqueID];
                            for (int j = 0; j < dtcolor.Rows.Count; j++)
                            {
                                var tbxReceivedQty = (TextBox)rptColors.Items[j].FindControl("tbxReceivedQty");
                                tbxReceivedQty.Text = dtcolor.Rows[j]["ReceivedQty"].ToString();
                            }
                        }
                    }
                }
            }
        }

        protected void rptColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (ReceivedId > 0)
                {
                    var recInfo = unitOfWork.GenericRepositories<YarnReceived>().GetByID(ReceivedId);
                    var tbxReceivedQty = (TextBox)e.Item.FindControl("tbxReceivedQty");
                    var tbxYarnComposition = (TextBox)((RepeaterItem)((Repeater)sender).Parent).FindControl("tbxYarnComposition");
                    var tbxLCNumber = (TextBox)((RepeaterItem)((Repeater)sender).Parent).FindControl("tbxLCNumber");
                    tbxReceivedQty.Text = recInfo.ReceivedQty + "";
                    tbxYarnComposition.Text = recInfo.YarnComposition;
                    tbxLCNumber.Text = recInfo.LCNumber;
                }
            }
        }
    }
}