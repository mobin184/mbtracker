﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" MaintainScrollPositionOnPostback="true" CodeBehind="EnterDyeingOrder.aspx.cs" Inherits="MBTracker.Pages.Yarn_and_Accessories.EnterDyeingOrder" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        input[type="radio"], input[type="checkbox"] {
            margin: -3px 0 0;
        }

        label {
            display: inline;
            padding-left: 3px !important;
        }

        td, th {
            padding: 5px;
        }

        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 7px;
        }

        .table {
            width: 100%;
            margin-bottom: 5px;
        }
    </style>
    <div class="row-fluid">
        <div class="span8">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="Dyeing Order Entry:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body" style="min-height: 175px;">
                    <div class="row" style="padding-left: 5px; padding-right: 5px;">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                            <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="form-horizontal">
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label14" runat="server" Text="Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlBuyers" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyer_SelectedIndexChanged" runat="server" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a purchase type.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label15" runat="server" Text="Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlStyles" AutoPostBack="true" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" runat="server" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select a purchase type.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group" runat="server">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label12" runat="server" Text="Select Merchandiser:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlMerchandiser" runat="server" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlMerchandiser"><span style="font-weight: 700; color: #CC0000">Please select merchandiser.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="lblBuyer" runat="server" Text="Select PI:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlPINumber" AutoPostBack="true" OnSelectedIndexChanged="ddlPINumber_SelectedIndexChanged" runat="server" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlPINumber"><span style="font-weight: 700; color: #CC0000">Please select a PI.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label2" runat="server" Text="Dyeing Order #:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:TextBox ID="tbxDyeingOrderNumber" runat="server" placeholder="Enter dyeing order number" CssClass="form-control" Width="90%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxDyeingOrderNumber"><span style="font-weight: 700; color: #CC0000">Enter dyeing order number.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="divItemInfo" runat="server" visible="false">
            <div class="span12">
                <div class="widget">
                    <div class="widget-body" style="padding-bottom: 40px;">
                        <div class="form-horizontal">
                            <div class="control-group">
                                <div class="controls controls-row" style="text-align: right">
                                    <span style="font-weight: 700; color: #CC0000">*</span>Row will not be saved if color, Item and Quantity fields don't have values.
                                </div>
                            </div>
                            <label for="inputOrder" class="control-label" style="text-align:left"> 
                                <asp:Label ID="lblItemRecEntry" runat="server" Text="Item(s):"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div style="width: 100%">
                                <asp:Repeater ID="rptItemRecEntryInfo" runat="server" OnItemDataBound="rptItemRecEntryInfo_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <%--<th>
                                                            <asp:Label ID="lblStyles" runat="server" Text="Style"></asp:Label></th>--%>
                                                    <th>
                                                        <asp:Label ID="lblColors" runat="server" Text="Color"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblItems" runat="server" Text="Item Description"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblLabDipRef" runat="server" Text="Lab Dif Ref"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblShade" runat="server" Text="Shade"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblLOTNumber" runat="server" Text="LOT Number"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblQty" runat="server" Text="Qty (lbs)"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label1" runat="server" Text="Remarks"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width: 15%; padding-top: 18px">
                                                <asp:DropDownList ID="ddlColors" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                            </td>
                                            <td style="width: 21%; padding-top: 18px">
                                                <asp:DropDownList ID="ddlItems" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                            </td>
                                            <td style="width: 8%; padding-top: 18px">
                                                <asp:TextBox ID="tbxLabDipRef" Width="100%" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 8%; padding-top: 18px">
                                                <asp:TextBox ID="tbxShade" Width="100%" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 8%; padding-top: 18px">
                                                <asp:TextBox ID="tbxLotNumber" Width="100%" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 8%; padding-top: 18px">
                                                <asp:TextBox ID="tbxQty" Width="100%" runat="server" TextMode="Number" step=""></asp:TextBox>
                                            </td>
                                            <td style="width: 8%; padding-top: 18px">
                                                <asp:TextBox ID="tbxRemarks" Width="100%" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                 </table>
                                        <div class="pull-right" style="margin-right: 10px">
                                            <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="81px" Text="Add Row" OnClick="btnAddRow_Click" />
                                        </div>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="span12" style="margin-left: 0px; margin-bottom: 30px">
                <asp:LinkButton ID="lnkbtnSaveEntries" ValidationGroup="save" Visible="false" runat="server" class="btn btn-success btn-medium pull-left hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
                <asp:LinkButton ID="lnkbtnUpdateEntries" ValidationGroup="save" Visible="false" runat="server" class="btn btn-success btn-medium pull-left hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>

