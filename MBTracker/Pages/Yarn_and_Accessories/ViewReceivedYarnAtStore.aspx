﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewReceivedYarnAtStore.aspx.cs" Inherits="MBTracker.Pages.Yarn_and_Accessories.ViewReceivedYarnAtStore" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <div class="row-fluid" runat="server" id="pnlEntry">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="View/Edit Received Yarn at Store:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputStyle" class="control-label">
                                <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputStyle" class="control-label">
                                <asp:Label ID="Label4" runat="server" Text="Received Date (Optional):"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxReceivedDate" runat="server" Width="60%" TextMode="Date"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="&nbsp;"></asp:Label></label>
                            <div class="controls controls-row">
                                <br />
                                <asp:Button ID="btnSearch" ValidationGroup="save" runat="server" class="btn btn-success btn-midium pull-left btnStyle" Text="View Received Yarn" OnClick="btnSearch_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <asp:Label ID="lblSummryNotFound" runat="server" Text="No data found." Visible="false" BackColor="#ffff00"></asp:Label>
        <div class="span9" runat="server" id="pnlSummary" visible="false">
            <div class="widget">
                <div class="widget-body">
                    <div class="control-group">
                        <label for="inputBuyer" class="control-label">
                            <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Received Yarn Information:"></asp:Label>
                        </label>
                    </div>
                    <div class="controls controls-row">
                        <div id="dt_example" class="example_alt_pagination">
                            <asp:Repeater ID="rptSummary" Visible="false" runat="server">
                                <HeaderTemplate>
                                    <table id="data-table" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                
                                                 <th>
                                                    <asp:Label ID="Label5" runat="server" Text="Buyer"></asp:Label>
                                                </th>
                                                  <th>
                                                    <asp:Label ID="Label6" runat="server" Text="Style"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label7" runat="server" Text="Color"></asp:Label>
                                                </th>
                                                 <th>
                                                    <asp:Label ID="Label8" runat="server" Text="Received Date"></asp:Label>
                                                </th>
                                                 <th>
                                                    <asp:Label ID="Label9" runat="server" Text="Received Quantity"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="lbl1" runat="server" Text="Yarn Composition"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="lbl2" runat="server" Text="LC Number"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label3" runat="server" Text="Take  Action"></asp:Label>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                       
                                        <td>
                                            <%#Eval("BuyerName") %>
                                        </td>
                                        <td>
                                            <%#Eval("StyleName") %>
                                        </td>
                                        <td style="text-align:left">
                                            <%#Eval("ColorDescription") %>
                                        </td>
                                        <td>
                                            <%#Eval("ReceivedDate") %>
                                        </td>
                                        <td style="text-align:right">
                                            <%#Eval("ReceivedQty") %>
                                        </td>
                                         <td>
                                            <%#Eval("YarnComposition") %>
                                        </td>
                                       <td>
                                            <%#Eval("LCNumber") %>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" Visible='<%# ViewState["editEnabled"]%>' CommandName="Edit" CommandArgument='<%#Eval("Id")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete"  Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                                </table>                                                  
                                </FooterTemplate>
                            </asp:Repeater>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
