﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class ViewYarnBookings : System.Web.UI.Page
    {

        //BuyerManager buyerManager = new BuyerManager();
        //OrderManager orderManager = new OrderManager();
        //DataTable dtYarntStatus;
        //List<YarnProcurementStatus> lstYarnStatus = new List<YarnProcurementStatus>();
        //UnitOfWork unitOfWork = new UnitOfWork();


        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    if (!IsPostBack)
        //    {
        //        EnableDisableEditButton();
        //        EnableDisableDeleteButton();
        //        CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
        //        if (Request.QueryString["bookingId"] != null)
        //        {
        //            BookingId = int.Parse(Request.QueryString["bookingId"]);
        //            LoadColorsAndBookingInfo();
        //            pnlEntry.Visible = false;
        //            pnlSummary.Visible = false;
        //        }
        //        if (Request.QueryString["styleId"] != null)
        //        {
        //            var styleId = int.Parse(Tools.UrlDecode(Request.QueryString["styleId"]));
        //            var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(styleId);
        //            ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
        //            BindStylesByBuyer(styleInfo.BuyerId);
        //            ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleId);
        //            BindBookingSummaryByStyle(styleId);
        //        }
        //    }
        //    else
        //    {
        //        string parameter = Request["__EVENTARGUMENT"];
        //        if (parameter == "confirmDelete")
        //        {
        //            DeleteYarnBooking();
        //        }
        //    }
        //}

        //private void DeleteYarnBooking()
        //{
        //    try
        //    {
        //       var lstBooking= unitOfWork.GenericRepositories<OrderYarnBookings>().Get(x => x.BookingId == BookingId).ToList();
        //        foreach (var item in lstBooking)
        //        {
        //            unitOfWork.GenericRepositories<OrderYarnBookings>().Delete(item);
        //        }
        //        unitOfWork.Save();
        //        var styleId = Convert.ToInt32(ddlStyles.SelectedValue);
        //        BindBookingSummaryByStyle(styleId);
        //        pnlYarnBooking.Visible = false;
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
        //    }
        //}

        //private void EnableDisableDeleteButton()
        //{
        //    ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");
        //}

        //private void EnableDisableEditButton()
        //{
        //    ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Merchandiser");
        //}

        //int StyleId
        //{
        //    set { ViewState["styleId"] = value; }
        //    get
        //    {
        //        try
        //        {
        //            return Convert.ToInt32(ViewState["styleId"]);
        //        }
        //        catch
        //        {
        //            return 0;
        //        }
        //    }
        //}

        //int BookingId
        //{
        //    set { ViewState["bookingId"] = value; }
        //    get
        //    {
        //        try
        //        {
        //            return Convert.ToInt32(ViewState["bookingId"]);
        //        }
        //        catch
        //        {
        //            return 0;
        //        }
        //    }
        //}


        //protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddlBuyers.SelectedValue != "")
        //    {
        //        BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
        //    }
        //    else
        //    {
        //        ddlStyles.Items.Clear();
        //    }

        //    pnlYarnBooking.Visible = false;
        //    lblNoColorFound.Visible = false;
        //    rptColors.DataSource = null;
        //    rptColors.DataBind();
        //    pnlSummary.Visible = false;
        //    rptSummary.DataSource = null;
        //    rptSummary.DataBind();
        //}

        //private void BindStylesByBuyer(int buyerId)
        //{
        //    CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        //}

        ////protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        ////{
        ////    pnlYarnBooking.Visible = false;
        ////    lblNoColorFound.Visible = false;
        ////    pnlSummary.Visible = false;
        ////    lblSummryNotFound.Visible = false;
        ////    if (ddlStyles.SelectedValue != "")
        ////    {
        ////        var styleId = Convert.ToInt32(ddlStyles.SelectedValue);
        ////        //BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
        ////        //BindColorsByStyle(styleId);
        ////        BindBookingSummaryByStyle(styleId);
        ////    }
        ////}

        //private void BindBookingSummaryByStyle(int styleId)
        //{
        //    string sql = $"Exec usp_GetStyleWiseYarnBookingSummary {styleId}";
        //    var dtSummary = unitOfWork.GetDataTableFromSql(sql);
            
        //    if (dtSummary.Rows.Count > 0)
        //    {
        //        rptSummary.DataSource = dtSummary;
        //        rptSummary.DataBind();
        //        rptSummary.Visible = true;
        //        lblSummryNotFound.Visible = false;
        //        pnlSummary.Visible = true;
        //    }
        //    else
        //    {
        //        rptSummary.DataSource = null;
        //        rptSummary.DataBind();
        //        rptSummary.Visible = false;
        //        lblSummryNotFound.Visible = true;
        //        pnlSummary.Visible = false;
        //    }
        //}


        //public DataTable YarnBookingTable()
        //{
        //    DataTable dt = new DataTable("YarnBooking");
        //    dt.Columns.Add("OrderId", typeof(int));
        //    dt.Columns.Add("Description", typeof(string));
        //    dt.Columns.Add("BookingQty", typeof(string));
        //    dt.Columns.Add("Supplier", typeof(string));
        //    dt.Columns.Add("ConsPerDzn", typeof(string));
        //    dt.Columns.Add("StatusId", typeof(int));
        //    dt.Columns.Add("CdBy", typeof(string));
        //    dt.Columns.Add("CDate", typeof(DateTime));
        //    return dt;
        //}



        //protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        //{
        //    BookingId = int.Parse(e.CommandArgument.ToString());
        //    LoadColorsAndBookingInfo();
        //    ScriptManager.RegisterStartupScript(this,this.GetType(), "message", "scrollDown()", true);
        //}

        //private void LoadColorsAndBookingInfo()
        //{
        //    var bookingInfo = unitOfWork.GenericRepositories<OrderYarnBookings>().Get(x=>x.BookingId==BookingId).FirstOrDefault();
        //    var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(bookingInfo.StyleId);
        //    var buyerInfo = unitOfWork.GenericRepositories<Buyers>().GetByID(styleInfo.BuyerId);
        //    lblColorAndSizeTitle.Text = $"Buyer : {buyerInfo.BuyerName}<br />Style: {styleInfo.StyleName}<br />Booking Date: {bookingInfo.CreateDate?.ToString("dd-MMM-yyyy hh:mm tt")}";
        //    string sql = $"Exec usp_GetColorsByBookingId {BookingId}";
        //    var dtBookingColors = unitOfWork.GetDataTableFromSql(sql);
            
        //    if (dtBookingColors.Rows.Count > 0)
        //    {
        //        //dtYarntStatus = orderManager.GetYarnProcurementStatus();
        //        lstYarnStatus = unitOfWork.GenericRepositories<YarnProcurementStatus>().Get().ToList();
        //        rptColors.DataSource = dtBookingColors;
        //        rptColors.DataBind();
        //        rptColors.Visible = true;
        //        lblNoColorFound.Visible = false;
        //        pnlYarnBooking.Visible = true;
        //    }
        //    else
        //    {
        //        rptColors.DataSource = null;
        //        rptColors.DataBind();
        //        rptColors.Visible = false;
        //        lblNoColorFound.Visible = true;
        //        pnlYarnBooking.Visible = false;
        //    }
        //}

        //protected void rptColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        var buyerColorId = int.Parse(((Label)e.Item.FindControl("lblBuyerColorId")).Text);
        //        Repeater rpt = (Repeater)e.Item.FindControl("rpt");

        //        string sql = $"Exec [usp_GetBookingInfoByBookingIdAndColor] {BookingId}, '{buyerColorId}'";
        //        var dtbookingInfo = unitOfWork.GetDataTableFromSql(sql);
        //        rpt.DataSource = dtbookingInfo;
        //        rpt.DataBind();
        //    }
        //}
        //protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        //{
        //    var bookingId = int.Parse(e.CommandArgument.ToString());
        //    Response.Redirect("BookYarn.aspx?bookingId=" + Tools.UrlEncode(bookingId+""));
        //}

        //protected void btnViewBookings_Click(object sender, EventArgs e)
        //{
        //    if(ddlBuyers.SelectedValue != "")
        //    {
        //        pnlYarnBooking.Visible = false;
        //        lblNoColorFound.Visible = false;
        //        pnlSummary.Visible = false;
        //        lblSummryNotFound.Visible = false;
        //        if (ddlStyles.SelectedValue != "")
        //        {
        //            var styleId = Convert.ToInt32(ddlStyles.SelectedValue);
        //            //BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
        //            //BindColorsByStyle(styleId);
        //            BindBookingSummaryByStyle(styleId);
        //        }
        //        else
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a style.');", true);
        //        }
        //    }
        //    else
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a buyer');", true);
        //    }
            
        //}


        //protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        //{
        //    BookingId = int.Parse(e.CommandArgument.ToString());
        //    if (BookingId != 0)
        //    {
        //        var title = "Warning";
        //        var msg = "Are you sure you want to delete?";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
        //    }
        //    else
        //    {
        //        var title = "Error";
        //        var msg = "Sorry, you can not delete it.<br />Something went wrong.";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
        //    }
        //}
    }
}