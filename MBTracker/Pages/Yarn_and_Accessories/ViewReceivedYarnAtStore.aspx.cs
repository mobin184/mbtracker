﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class ViewReceivedYarnAtStore : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableEditButton();
                EnableDisableDeleteButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);

                if (Request.QueryString["styleId"] != null)
                {
                    var styleId = int.Parse(Request.QueryString["styleId"]);
                    var reportDate = "";
                    GetYarnReceived(styleId, reportDate);
                    pnlEntry.Visible = false;

                }                
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteYarnReceive();
                }
            }
        }

        int ReceivedId
        {
            set { ViewState["id"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["id"]);
                }
                catch
                {
                    return 0;
                }
            }
        }
        private void DeleteYarnReceive()
        {
            try
            {
                unitOfWork.GenericRepositories<YarnReceived>().Delete(ReceivedId);               
                unitOfWork.Save();
                var styleId = int.Parse(ddlStyles.SelectedValue);
                if (!string.IsNullOrEmpty(tbxReceivedDate.Text))
                {
                    var date = DateTime.Parse(tbxReceivedDate.Text).ToString("yyyy-MM-dd");
                    GetYarnReceived(styleId, date);
                }
                else
                {
                    GetYarnReceived(styleId, "");
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        private void EnableDisableDeleteButton()
        {
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");
        }
        private void EnableDisableEditButton()
        {
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,QCE");
        }
        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            else
            {
                ddlStyles.Items.Clear();
            }
            pnlSummary.Visible = false;
            lblSummryNotFound.Visible = false;
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                var styleId = int.Parse(ddlStyles.SelectedValue);
                if (!string.IsNullOrEmpty(tbxReceivedDate.Text))
                {
                    var date = DateTime.Parse(tbxReceivedDate.Text).ToString("yyyy-MM-dd");
                    GetYarnReceived(styleId, date);
                }
                else
                {
                    GetYarnReceived(styleId, "");
                }

                
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }


        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            var id = int.Parse(e.CommandArgument.ToString());
            Response.Redirect("ReceiveYarn.aspx?receivedId=" + Tools.UrlEncode(id + ""));
        }




        private void GetYarnReceived(int styleId, string date)
        {
            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetYarnReceivedByStyleAndDate {styleId},'{date}'");
            if (dt.Rows.Count > 0)
            {
                rptSummary.DataSource = dt;
                rptSummary.DataBind();
                lblSummryNotFound.Visible = false;
                rptSummary.Visible = true;
                pnlSummary.Visible = true;
            }
            else
            {
                lblSummryNotFound.Visible = true;
                rptSummary.Visible = false;
                pnlSummary.Visible = false;
                rptSummary.DataSource = null;
                rptSummary.DataBind();
            }
        }

        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            ReceivedId = int.Parse(e.CommandArgument.ToString());
            if (ReceivedId != 0)
            {
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
            }
            else
            {
                var title = "Error";
                var msg = "Sorry, you can not delete it.<br />Something went wrong.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            }
        }
    }
}