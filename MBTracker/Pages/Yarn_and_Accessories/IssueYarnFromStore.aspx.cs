﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class IssueYarnFromStore : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        ProductionManager productionManager = new ProductionManager();
        DataTable dtOrderColors;
        DataTable dtSizes;
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);

                if (Request.QueryString["issueNumber"] != null)
                {
                    IssueNumber = int.Parse(Tools.UrlDecode(Request.QueryString["issueNumber"]));
                    var issueInfo = unitOfWork.GenericRepositories<YarnIssuedForKnitting>().Get(x => x.IssueNumber == IssueNumber && x.IsNewRecord ==true).FirstOrDefault();
                    var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(issueInfo.StyleId);
                    //var orderInfo = unitOfWork.GenericRepositories<Orders>().GetByID(styleInfo.OrderId);
                    PopulateData(styleInfo, issueInfo);
                    lblMachinePlanning.Text = "Update Issued Yarn";
                }
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmQtySave")
                {
                    SaveOperation();
                }
                if (parameter == "confirmQtyUpdate")
                {
                    UpdateOperation();
                }
            }
        }

        private void PopulateData(BuyerStyles styleInfo, YarnIssuedForKnitting yarnIssuedForKnitting)
        {
            BindStylesByBuyer(styleInfo.BuyerId);
            //BindOrdersByStyle(orderInfo.StyleId);
            ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
            ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleInfo.Id);
            //ddlOrders.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrders, orderInfo.Id);
            tbxKnittingPart.Text = yarnIssuedForKnitting.KnittingPart;
            tbxYarnDecription.Text = yarnIssuedForKnitting.YarnDescription;
            LoadPlanInfo();
            lnkbtnSaveYarnIssued.Visible = false;
            lnkbtnUpdateYarnIssued.Visible = true;
            ddlBuyers.Enabled = false;
            ddlStyles.Enabled = false;
            //ddlOrders.Enabled = false;
        }

        int IssueNumber
        {
            set { ViewState["issueNumber"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["issueNumber"]);
                }
                catch
                {
                    return 0;
                }
            }
        }


        int KnittingTime
        {
            set { ViewState["knittingTime"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["knittingTime"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlStyles.Items.Clear();
            //ddlOrders.Items.Clear();

            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
                rptPlanInfo.DataSource = null;
                rptPlanInfo.DataBind();

                rpt.DataSource = null;
                rpt.DataBind();
            }
            //else
            //{
            //    pnlPlanningInfo.Visible = false;
            //    pnlAssignMachinesToOrder.Visible = false;
            //    lblNoKnittingPlan.Visible = false;
            //}
            pnlPlanningInfo.Visible = false;
            pnlAssignMachinesToOrder.Visible = false;
            lblNoKnittingPlan.Visible = false;
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {

            //ddlOrders.Items.Clear();
            if (ddlStyles.SelectedValue != "")
            {
                LoadPlanInfo();
   

                //BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
            }
            else
            {
                pnlPlanningInfo.Visible = false;
                pnlAssignMachinesToOrder.Visible = false;
                lblNoKnittingPlan.Visible = false;
            }
           

        }

        //private void BindOrdersByStyle(int styleId)
        //{
        //    CommonMethods.LoadOrderDropdownByStyle(ddlOrders, styleId, 1, 0);
        //}

        //protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //    if (ddlOrders.SelectedValue != "")
        //    {
        //        LoadPlanInfo();
        //        //ddlBuyers.Enabled = false;
        //        //ddlStyles.Enabled = false;
        //        //ddlOrders.Enabled = false;
        //    }
        //    else
        //    {
        //        pnlPlanningInfo.Visible = false;
        //        pnlAssignMachinesToOrder.Visible = false;
        //    }
        //}


        private void LoadPlanInfo()
        {
            DataTable dt = new DataTable();
            pnlPlanningInfo.Visible = true;

            var styleId = Convert.ToInt32(ddlStyles.SelectedValue);
            dt = productionManager.GetProductionPlanningInfo(styleId);
            if (dt.Rows.Count < 1)
            {
                dt = orderManager.GetProductionPlanningInfo(styleId);
            }

            if (dt.Rows.Count > 0)
            {
                KnittingTime = Convert.ToInt32(dt.Rows[0][8].ToString());

                rptPlanInfo.DataSource = dt;
                rptPlanInfo.DataBind();
                rptPlanInfo.Visible = true;                
                LoadExistingMCAssignedToOrder();

                LoadShipmentInfo(styleId);
            }
            else
            {
                rptPlanInfo.DataSource = null;
                rptPlanInfo.DataBind();
                rptPlanInfo.Visible = false;

            }

    
            BindTotalOrderQuantity(styleId);

        }

        private void BindTotalOrderQuantity(int styleId)
        {
            divTotalOrderQuantity.Visible = true;
            var totalOrderQty = unitOfWork.GetSingleValue($"Exec usp_GetActiveOrderQuantityByStyleId {styleId}");
            divTotalOrderQuantity.InnerHtml = $"<div style='width:100%; padding-bottom:9px; margin-bottom:5px; font-size:13px; padding:10px; background-color:#ffff00aa; font-weight:bold'>Order Quantity: <span style='font-size:15px;'>{totalOrderQty}</span>    &nbsp;&nbsp;<asp:LinkButton ID='lnkbtnView' onClick='window.open(\"../Reports/SummaryOfOrders.aspx?styleId={Tools.UrlEncode(styleId+"")}\",\"_blank\")' class='btn btn-success btn-mini hidden-phone pull-right' Text='View Details'>View Details</asp:LinkButton></div>";
        }
        protected void rptShipmentSummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label machinesRequired = (Label)e.Item.FindControl("lblMCRequired");
                double numMachinesRequired = Math.Ceiling((Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "ShipmentQuantity").ToString()) * KnittingTime) / 1320);
                machinesRequired.Text = numMachinesRequired.ToString();

            }
        }

        private void LoadShipmentInfo(int styleId)
        {

            pnlShipmentInfo.Visible = true;

            DataTable dt = new DataTable();
            dt = orderManager.GetShipmentInfoByStyleId(styleId);

            if (dt.Rows.Count > 0)
            {
                rptShipmentSummary.DataSource = dt;
                rptShipmentSummary.DataBind();

                rptShipmentSummary.Visible = true;
                lblNoShippingInfo.Visible = false;

                //pnlMonthYearSelection.Visible = true;

            }
            else
            {
                rptShipmentSummary.DataSource = null;
                rptShipmentSummary.DataBind();

                rptShipmentSummary.Visible = false;
                lblNoShippingInfo.Visible = true;

                pnlAssignMachinesToOrder.Visible = false;
                //pnlMonthYearSelection.Visible = false;

            }
        }


        private void LoadExistingMCAssignedToOrder()
        {

            DataTable dt = new DataTable();
           

            dt = productionManager.GetMachinesAssignedToStyle(Convert.ToInt32(ddlStyles.SelectedValue));

            if (dt.Rows.Count > 0)
            {

                dtOrderColors = orderManager.GetColorsByStyleId(Convert.ToInt32(ddlStyles.SelectedValue));

                if (dtOrderColors.Rows.Count > 0)
                {
                    LoadSizeInfo();
                }

                rpt.DataSource = dt;
                rpt.DataBind();
                rpt.Visible = true;
                pnlAssignMachinesToOrder.Visible = true;
                lblNoKnittingPlan.Visible = false;
                pnlPlanningInfo.Visible = true;

            }
            else
            {
                pnlAssignMachinesToOrder.Visible = false;
                lblNoKnittingPlan.Visible = true;
                pnlPlanningInfo.Visible = false;
                rpt.DataSource = null;
                rpt.DataBind();
                rpt.Visible = false;


            }

        }


        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptOrderColors = (Repeater)e.Item.FindControl("rptOrderColors");
                if (dtOrderColors.Rows.Count > 0)
                {
                    rptOrderColors.DataSource = dtOrderColors;
                    rptOrderColors.DataBind();
                }
            }
        }

        protected void rptOrderColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {


            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                GridView gvSizeQuantity = (GridView)e.Item.FindControl("gvSizeQuantity");

                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantity.Columns.Add(templateField);
                    }

                }


                gvSizeQuantity.DataSource = dtOneRow;
                gvSizeQuantity.DataBind();

            }

        }



        protected void gvSizeQuantity_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                TextBox txtSizeQty = null;
                Label lblSizeId = null;
                LoadSizeInfo();
                if (IssueNumber != 0)
                {
                    var styleId = Int32.Parse(ddlStyles.SelectedValue);
                    var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                    var buyerColorId = int.Parse(lblBuyerColorId.Text);

                    var lblUnitId = (Label)((GridView)sender).DataItemContainer.NamingContainer.NamingContainer.FindControl("lblUnitId");
                    var UnitId = Convert.ToInt32(lblUnitId.Text);

                    var lblMachinAssignedId = (Label)((GridView)sender).DataItemContainer.NamingContainer.NamingContainer.FindControl("lblMachinAssignedId");
                    var machinAssignedId = Convert.ToInt32(lblMachinAssignedId.Text);

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        var sizeId = Int32.Parse(dtSizes.Rows[i][0].ToString());

                        var sizeQty = CommonMethods.GetSizeQuantityByStyleAndColor(styleId, buyerColorId, sizeId);

                        var info = unitOfWork.GenericRepositories<YarnIssuedForKnitting>().Get(x => x.StyleId == styleId && x.MachinAssignedId == machinAssignedId && x.BuyerColorId == buyerColorId && x.UnitId == UnitId && x.SizeId == sizeId && x.IsNewRecord == true && x.IssueNumber == IssueNumber).FirstOrDefault();

                        if (info != null)
                        {
                            txtSizeQty = new TextBox();
                            if(sizeQty == 0)
                            {
                                txtSizeQty.Enabled = false;
                            }

                            txtSizeQty.TextMode = TextBoxMode.Number;
                            txtSizeQty.ID = "txtSizeQty" + i.ToString();
                            txtSizeQty.Text = info.YarnIssuedQuantity + "";
                            txtSizeQty.Width = 60;
                            e.Row.Cells[i].Controls.Add(txtSizeQty);

                            lblSizeId = new Label();
                            lblSizeId.ID = "lblSizeId" + i.ToString();
                            lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                            lblSizeId.Visible = false;
                            e.Row.Cells[i].Controls.Add(lblSizeId);

                            var lblYarnIssuedForKnitting = new Label();
                            lblYarnIssuedForKnitting.ID = "lblYarnIssuedForKnitting" + i.ToString();
                            lblYarnIssuedForKnitting.Text = info.Id + "";
                            lblYarnIssuedForKnitting.Visible = false;
                            e.Row.Cells[i].Controls.Add(lblYarnIssuedForKnitting);

                        }
                        else
                        {
                            txtSizeQty = new TextBox();

                            if (sizeQty == 0)
                            {
                                txtSizeQty.Enabled = false;
                            }

                            txtSizeQty.TextMode = TextBoxMode.Number;
                            txtSizeQty.ID = "txtSizeQty" + i.ToString();
                            txtSizeQty.Text = "";
                            txtSizeQty.Width = 60;
                            e.Row.Cells[i].Controls.Add(txtSizeQty);

                            lblSizeId = new Label();
                            lblSizeId.ID = "lblSizeId" + i.ToString();
                            lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                            lblSizeId.Visible = false;
                            e.Row.Cells[i].Controls.Add(lblSizeId);
                        }
                    }
                }
                else
                {
                    var styleId = Int32.Parse(ddlStyles.SelectedValue);
                    var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                    var buyerColorId = int.Parse(lblBuyerColorId.Text);

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        var sizeId = Int32.Parse(dtSizes.Rows[i][0].ToString());
                        //var sizeQty = CommonMethods.GetSizeQuantityByStyleAndColor(styleId, buyerColorId, sizeId);
                        var sizeQty = 0;

                         txtSizeQty = new TextBox();

                        if(sizeQty == 0)
                        {
                            txtSizeQty.Enabled = false;
                        }

                        txtSizeQty.TextMode = TextBoxMode.Number;
                        txtSizeQty.ID = "txtSizeQty" + i.ToString();
                        txtSizeQty.Text = "";
                        txtSizeQty.Width = 60;
                        e.Row.Cells[i].Controls.Add(txtSizeQty);

                        lblSizeId = new Label();
                        lblSizeId.ID = "lblSizeId" + i.ToString();
                        lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                        lblSizeId.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblSizeId);
                    }
                }
            }
        }


        //protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        //{

        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {

        //        TextBox txtSizeQty = null;
        //        Label lblSizeId = null;
        //        // LoadSizeInfo();

        //        for (int i = 0; i < dtSizes.Rows.Count; i++)
        //        {
        //            txtSizeQty = new TextBox();
        //            txtSizeQty.ID = "txtSizeQty" + i.ToString();
        //            txtSizeQty.Text = "";
        //            txtSizeQty.Width = 60;
        //            e.Row.Cells[i].Controls.Add(txtSizeQty);

        //            lblSizeId = new Label();
        //            lblSizeId.ID = "lblSizeId" + i.ToString();
        //            lblSizeId.Text = dtSizes.Rows[i][0].ToString();
        //            lblSizeId.Visible = false;
        //            e.Row.Cells[i].Controls.Add(lblSizeId);
        //        }
        //    }
        //}



        protected void LoadSizeInfo()
        {
            dtSizes = buyerManager.GetStyleSizes(Convert.ToInt32(ddlStyles.SelectedValue));

        }



        protected void lnkbtnSaveYarnIssued_Click(object sender, EventArgs e)
        {
            CheckYarnIssue("confirmQtySave");
        }

        public void CheckYarnIssue(string callBackFunctionName)
        {
            string msg = "";
            string title = $"Information:";
            for (int i = 0; i < rpt.Items.Count; i++)
            {
                Repeater rptOrderColors = (Repeater)rpt.Items[i].FindControl("rptOrderColors");
                Label lblUnitId = (Label)rpt.Items[i].FindControl("lblUnitId");
                Label lblUnitName = (Label)rpt.Items[i].FindControl("lblUnitName");
                Label lblProductionDate = (Label)rpt.Items[i].FindControl("lblProductionDate");
                Label lblMachinAssignedId = (Label)rpt.Items[i].FindControl("lblMachinAssignedId");
                Label lblIssuedQty = (Label)rpt.Items[i].FindControl("lblIssuedQty");
                var PlannedQty = int.Parse(lblIssuedQty.Text);

                var sizeWiseIssueQty = 0;

                for (int j = 0; j < rptOrderColors.Items.Count; j++)
                {
                    Label lblBuyerColorId = (Label)rptOrderColors.Items[j].FindControl("lblBuyerColorId");
                    GridView gvSizeQuantity = (GridView)rptOrderColors.Items[j].FindControl("gvSizeQuantity");

                    foreach (GridViewRow row in gvSizeQuantity.Rows)
                    {
                        for (int k = 0; k < gvSizeQuantity.Columns.Count; k++)
                        {
                            string sizeIdLabelId = "lblSizeId" + k.ToString();
                            string sizeQuantityTextBoxId = "txtSizeQty" + k.ToString();

                            Label sizeId = (Label)row.Cells[k].FindControl(sizeIdLabelId);
                            TextBox tbxSizeQty = (TextBox)row.FindControl(sizeQuantityTextBoxId);


                            if (tbxSizeQty.Text != "")
                            {
                                sizeWiseIssueQty += int.Parse(tbxSizeQty.Text);
                            }
                        }
                    }
                }

                decimal totalPercentage = ((sizeWiseIssueQty * 100) / PlannedQty);
                if (totalPercentage > 100)
                {
                    msg += $"&#10003; {lblUnitName.Text} ({lblProductionDate.Text}) issue quantity is {totalPercentage}%<br />";
                }
            }

            if (msg != "")
            {
                msg += "<br /><br />Do you want to issue this yarn?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','"+callBackFunctionName+"','350','false');", true);
            }
            else
            {
                if(IssueNumber != 0)
                {
                    UpdateOperation();
                }
                else
                {
                    SaveOperation();
                }
            }
        }

        public void SaveOperation()
        {
            DataTable dtYarnIssuedForKnitting = YarnIssuedForKnitting();
            int hasRow = 0;
            int YarnIssuedResult = 0;

            for (int i = 0; i < rpt.Items.Count; i++)
            {
                Repeater rptOrderColors = (Repeater)rpt.Items[i].FindControl("rptOrderColors");
                Label lblUnitId = (Label)rpt.Items[i].FindControl("lblUnitId");
                Label lblMachinAssignedId = (Label)rpt.Items[i].FindControl("lblMachinAssignedId");
                Label lblIssuedQty = (Label)rpt.Items[i].FindControl("lblIssuedQty");
                var IssuedQty = int.Parse(lblIssuedQty.Text);

                for (int j = 0; j < rptOrderColors.Items.Count; j++)
                {

                    Label lblBuyerColorId = (Label)rptOrderColors.Items[j].FindControl("lblBuyerColorId");
                    GridView gvSizeQuantity = (GridView)rptOrderColors.Items[j].FindControl("gvSizeQuantity");

                    foreach (GridViewRow row in gvSizeQuantity.Rows)
                    {

                        for (int k = 0; k < gvSizeQuantity.Columns.Count; k++)
                        {

                            string sizeIdLabelId = "lblSizeId" + k.ToString();
                            string sizeQuantityTextBoxId = "txtSizeQty" + k.ToString();

                            Label sizeId = (Label)row.Cells[k].FindControl(sizeIdLabelId);
                            TextBox tbxSizeQty = (TextBox)row.FindControl(sizeQuantityTextBoxId);


                            if (tbxSizeQty.Text != "")
                            {
                                dtYarnIssuedForKnitting.Rows.Add(Convert.ToInt32(ddlStyles.SelectedValue), Convert.ToInt32(lblMachinAssignedId.Text), Convert.ToInt32(lblUnitId.Text), int.Parse(lblBuyerColorId.Text), Convert.ToInt32(sizeId.Text), Convert.ToInt32(tbxSizeQty.Text), tbxKnittingPart.Text, tbxYarnDecription.Text, CommonMethods.SessionInfo.UserName, DateTime.Now);
                                hasRow++;
                            }
                        }
                    }
                }
            }

            if (hasRow > 0)
            {
                YarnIssuedResult = productionManager.SaveYarnIssuedForKnitting(dtYarnIssuedForKnitting);
                if (YarnIssuedResult > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                }
                
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter correct information.');", true);
            }            
        }

        public void UpdateOperation()
        {
            try
            {
                var lstIssuInfo = unitOfWork.GenericRepositories<YarnIssuedForKnitting>().Get(x => x.IssueNumber == IssueNumber && x.IsNewRecord == true).ToList();
                foreach (var item in lstIssuInfo)
                {
                    item.IsNewRecord = false;
                    item.UpdatedBy = CommonMethods.SessionInfo.UserName;
                    item.UpdateDate = DateTime.Now;
                    unitOfWork.GenericRepositories<YarnIssuedForKnitting>().Update(item);
                }

                var knitingPart = tbxKnittingPart.Text;
                var yarnDescription = tbxYarnDecription.Text;
                for (int i = 0; i < rpt.Items.Count; i++)
                {
                    Repeater rptOrderColors = (Repeater)rpt.Items[i].FindControl("rptOrderColors");
                    Label lblUnitId = (Label)rpt.Items[i].FindControl("lblUnitId");
                    Label lblMachinAssignedId = (Label)rpt.Items[i].FindControl("lblMachinAssignedId");

                    for (int j = 0; j < rptOrderColors.Items.Count; j++)
                    {

                        Label lblBuyerColorId = (Label)rptOrderColors.Items[j].FindControl("lblBuyerColorId");
                        GridView gvSizeQuantity = (GridView)rptOrderColors.Items[j].FindControl("gvSizeQuantity");

                        foreach (GridViewRow row in gvSizeQuantity.Rows)
                        {
                            for (int k = 0; k < gvSizeQuantity.Columns.Count; k++)
                            {

                                string sizeIdLabelId = "lblSizeId" + k.ToString();
                                string sizeQuantityTextBoxId = "txtSizeQty" + k.ToString();
                                string yarnIssuedForKnitting = "lblYarnIssuedForKnitting" + k.ToString();

                                Label lblSizeId = (Label)row.Cells[k].FindControl(sizeIdLabelId);
                                TextBox tbxSizeQty = (TextBox)row.FindControl(sizeQuantityTextBoxId);
                                if (tbxSizeQty.Text != "")
                                {
                                    var yarnIssue = new YarnIssuedForKnitting()
                                    {
                                        IssueNumber = IssueNumber,
                                        MachinAssignedId = int.Parse(lblMachinAssignedId.Text),
                                        StyleId = Int32.Parse(ddlStyles.SelectedValue),
                                        UnitId = Int32.Parse(lblUnitId.Text),
                                        BuyerColorId = int.Parse(lblBuyerColorId.Text),
                                        SizeId = Int32.Parse(lblSizeId.Text),
                                        YarnIssuedQuantity = Int32.Parse(tbxSizeQty.Text),
                                        KnittingPart = knitingPart,
                                        YarnDescription = yarnDescription,
                                        CreatedBy = CommonMethods.SessionInfo.UserName,
                                        CreateDate = DateTime.Now,
                                        IsNewRecord = true
                                    };
                                    unitOfWork.GenericRepositories<YarnIssuedForKnitting>().Insert(yarnIssue);
                                }
                            }
                        }
                    }
                }

                unitOfWork.Save();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                //Response.AddHeader("REFRESH", "2;URL=ViewYarnIssues.aspx");
                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewYarnIssues.aspx');", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('" + ex.Message + "');", true);
            }
        }
        public DataTable YarnIssuedForKnitting()
        {
            DataTable dt = new DataTable("YarnIssuedForKnitting");
            dt.Columns.Add("StyleId", typeof(int));
            dt.Columns.Add("MachinAssignedId", typeof(int));
            dt.Columns.Add("UnitId", typeof(int));
            dt.Columns.Add("BuyerColorId", typeof(int));
            dt.Columns.Add("SizeId", typeof(int));
            dt.Columns.Add("YarnIssuedQuantity", typeof(int));
            dt.Columns.Add("KnittingPart", typeof(string));
            dt.Columns.Add("YarnDescription", typeof(string));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(string));
            return dt;
        }

        protected void lnkbtnUpdateYarnIssued_Click(object sender, EventArgs e)
        {
            CheckYarnIssue("confirmQtyUpdate");
        }
    }
}