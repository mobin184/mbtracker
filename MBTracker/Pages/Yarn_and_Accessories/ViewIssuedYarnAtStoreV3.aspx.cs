﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class ViewIssuedYarnAtStoreV3 : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        OrderManager orderManager = new OrderManager();
        decimal grandTotal = 0;


        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                //EnableDisableEditButton();
                //EnableDisableDeleteButton();
                //CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
                LoadProductionUnitsForIssueByUser(ddlKnittingUnits);
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                LoadStore();
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteYarnIssueInfo(YarnIssueId);
                }
            }
        }

        public static void LoadProductionUnitsForIssueByUser(DropDownList ddlUserProductionUnit)
        {

            var dtKnittingUnit = new CommonManager().GetProductionUnitsByUser(CommonMethods.SessionInfo.UserId);

            var lstOtherUnits = new UnitOfWork().GenericRepositories<OtherKnittingUnits>().Get().ToList();
            foreach (var item in lstOtherUnits)
            {
                var drow = dtKnittingUnit.NewRow();
                drow["ProductionUnitId"] = item.ProductionUnitId;
                drow["UnitName"] = item.UnitName;
                dtKnittingUnit.Rows.Add(drow);
            }

            //var dr = dtKnittingUnit.NewRow();
            //dr["ProductionUnitId"] = 99;
            //dr["UnitName"] = "Leftover";

            //var dr2 = dtKnittingUnit.NewRow();
            //dr2["ProductionUnitId"] = 100;
            //dr2["UnitName"] = "On Hold";


            //var dr3 = dtKnittingUnit.NewRow();
            //dr3["ProductionUnitId"] = 101;
            //dr3["UnitName"] = "Mega Dyeing";

            var dr1 = dtKnittingUnit.NewRow();
            dr1["ProductionUnitId"] = 0;
            dr1["UnitName"] = "---Select---";
            //dtKnittingUnit.Rows.Add(dr);
            //dtKnittingUnit.Rows.Add(dr2);
            //dtKnittingUnit.Rows.Add(dr3);
            dtKnittingUnit.Rows.InsertAt(dr1, 0);
            ddlUserProductionUnit.DataTextField = "UnitName";
            ddlUserProductionUnit.DataValueField = "ProductionUnitId";
            ddlUserProductionUnit.DataSource = dtKnittingUnit;
            ddlUserProductionUnit.DataBind();
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
            }
        }

        private void LoadStore()
        {
            var dt = unitOfWork.GetDataTableFromSql("SELECT StoreId, StoreName FROM Stores WHERE StoreType = 2");
            ddlStore.Items.Add(new ListItem("", ""));
            ddlStore.DataSource = dt;
            ddlStore.DataTextField = "StoreName";
            ddlStore.DataValueField = "StoreId";
            ddlStore.DataBind();
            ddlStore.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlStore.SelectedIndex = 0;
        }

        private void DeleteYarnIssueInfo(int yarnIssueId)
        {
            var issueInfo = unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().GetByID(yarnIssueId);
            //var lstColorSize = unitOfWork.GenericRepositories<YarnIssuedAtStoreColorSize>().Get(x => x.IssueId == yarnIssueId).ToList();
            //foreach (var item in lstColorSize)
            //{
            //    unitOfWork.GenericRepositories<YarnIssuedAtStoreColorSize>().Delete(item);
            //}
            unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().Delete(issueInfo);
            unitOfWork.Save();
            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
            LoadData();
        }


        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewIssuedYarnAtStore", 2, 1);
        }

        private void EnableDisableEditButton()
        {
            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Knitting - Mgmt,Knitting - Floor");
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewIssuedYarnAtStore", 1, 1);
        }

        protected void btnViewEntries_Click(object sender, EventArgs e)
        {
            try
            {
                LoadData();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void LoadData()
        {

            divSummary.Visible = false;
            divColors.Visible = false;
            rptDetailInfo.DataSource = null;
            rptDetailInfo.DataBind();

            //if(string.IsNullOrEmpty(tbxIssuedDate.Text))
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter issue date.')", true);
            //}
            //else
            //{
            var knittingUnit = 0;
            var issueDate = "";
            if (!string.IsNullOrEmpty(tbxIssuedDate.Text))
            {
                issueDate = DateTime.Parse(tbxIssuedDate.Text).ToString("yyyy-MM-dd");
            }

            var storeId = 0;
            if (ddlStore.SelectedValue != "")
            {
                storeId = int.Parse(ddlStore.SelectedValue);
            }

            if (ddlKnittingUnits.SelectedValue != "")
            {
                knittingUnit = int.Parse(ddlKnittingUnits.SelectedValue);
            }

            var buyerId = 0;
            if (ddlBuyers.SelectedValue != "")
            {
                buyerId = int.Parse(ddlBuyers.SelectedValue);
            }

            var styleId = 0;
            if (ddlStyles.SelectedValue != "")
            {
                styleId = int.Parse(ddlStyles.SelectedValue);
            }

            if (string.IsNullOrEmpty(issueDate) && buyerId == 0 && styleId == 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter issue date or select buyer with style.')", true);
            }
            else
            {
                if (string.IsNullOrEmpty(issueDate) && buyerId != 0 && styleId == 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select style.')", true);
                }
                else
                {
                    //var dtEntries = unitOfWork.GenericRepositories<YarnIssuedAtStore>().Get(x => (x.StoreId == storeId || storeId == 0) && x.IssueDate == issueDate && (x.KnittingUnitId == knittingUnit || knittingUnit == 0)).FirstOrDefault();

                    var dtEntries = unitOfWork.GetDataTableFromSql($"EXEC usp_YarnIssuedAtStoreByUserAndDateV2 {storeId},{knittingUnit}, '{issueDate}','{buyerId}','{styleId}'");

                    if (dtEntries.Rows.Count > 0)
                    {
                        dtEntries = dtEntries.CheckEditDeletePermission(CommonMethods.SessionInfo.RoleId, "ViewIssuedYarnAtStore");
                        rptSummary.DataSource = dtEntries;
                        rptSummary.DataBind();
                        rptSummary.Visible = true;
                        lblEntryNotFound.Visible = false;
                        dataDiv.Visible = true;


                        // lblHeaderMessage.Text = "Yarn Issued at: " + ddlKnittingUnits.SelectedItem.Text;

                        lblGrandTotal.Text = "Total Issued Quantity: " + grandTotal.ToString();
                    }
                    else
                    {
                        rptSummary.DataSource = null;
                        rptSummary.DataBind();
                        rptSummary.Visible = false;
                        lblEntryNotFound.Visible = true;
                        dataDiv.Visible = false;
                    }
                    divSummary.Visible = true;
                }
            }
            //}

        }


        protected void rptSummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var issuedQty = decimal.Parse(DataBinder.Eval(e.Item.DataItem, "IssuedQuantityLbs").ToString());
                grandTotal = grandTotal + issuedQty;
            }
        }



        protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        {
            YarnIssueId = int.Parse(e.CommandArgument.ToString());

            var dtRecColors = unitOfWork.GetDataTableFromSql($"Exec usp_GetYarnIssuedAtUnitDetailsByIssueId {YarnIssueId}");


            if (dtRecColors.Rows.Count > 0)
            {
                lblEntry.Text = "Details of Issued Yarn: Buyer: " + dtRecColors.Rows[0]["BuyerName"].ToString() + ", Style: " + dtRecColors.Rows[0]["StyleName"].ToString() + ", Quantity: " + dtRecColors.Rows[0]["IssuedQuantity"].ToString();


                divColors.Visible = true;
                rptDetailInfo.DataSource = dtRecColors;
                rptDetailInfo.DataBind();
                rptDetailInfo.Visible = true;
                lblNoDataFound.Visible = false;
            }
            else
            {
                rptDetailInfo.DataSource = null;
                rptDetailInfo.DataBind();
                rptDetailInfo.Visible = false;
                lblNoDataFound.Visible = true;
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown()", true);

        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            var yarnIssueId = int.Parse(e.CommandArgument.ToString());
            YarnIssueId = yarnIssueId;

            for (int i = 0; i < rptSummary.Items.Count; i++)
            {
                var tbxUpdatedQtyR = (TextBox)rptSummary.Items[i].FindControl("UpdatedIssuedQty");
                if (tbxUpdatedQtyR.Visible)
                {
                    var lnkbtnEditR = (LinkButton)rptSummary.Items[i].FindControl("lnkbtnEdit");
                    var lblIssuedQtyR = (Label)rptSummary.Items[i].FindControl("IssuedQty");
                    var btnUpdateR = (LinkButton)rptSummary.Items[i].FindControl("lnkbtnUpdated");
                    var lnkbtnCancelR = (LinkButton)rptSummary.Items[i].FindControl("lnkbtnCancel");
                    var btnDeleteR = (LinkButton)rptSummary.Items[i].FindControl("lnkbtnDelete");

                    tbxUpdatedQtyR.Visible = false;
                    btnUpdateR.Visible = false;
                    lnkbtnCancelR.Visible = false;
                    btnDeleteR.Visible = true;
                    lblIssuedQtyR.Visible = true;
                    lnkbtnEditR.Visible = true;

                    var rptIssueDateR = (TextBox)rptSummary.Items[i].FindControl("rptIssueDate");
                    var rptStylesR = (DropDownList)rptSummary.Items[i].FindControl("rptStyles");
                    var rptColorsR = (DropDownList)rptSummary.Items[i].FindControl("rptColors");
                    var rptItemsR = (DropDownList)rptSummary.Items[i].FindControl("rptItems");
                    var rptLOTNumberR = (DropDownList)rptSummary.Items[i].FindControl("rptLOTNumber");
                    var rptStoresR = (DropDownList)rptSummary.Items[i].FindControl("rptStores");
                    var rptUnitsR = (DropDownList)rptSummary.Items[i].FindControl("rptUnits");
                    var trAvailableBalanceR = (PlaceHolder)rptSummary.Items[i].FindControl("trAvailableBalance");

                    rptIssueDateR.Visible = false;
                    rptStylesR.Visible = false;
                    rptColorsR.Visible = false;
                    rptItemsR.Visible = false;
                    rptLOTNumberR.Visible = false;
                    rptStoresR.Visible = false;
                    rptUnitsR.Visible = false;
                    trAvailableBalanceR.Visible = false;

                    var lblIssueDateR = (Label)rptSummary.Items[i].FindControl("lblIssueDate");
                    var lblStylesR = (Label)rptSummary.Items[i].FindControl("lblStyles");
                    var lblColorsR = (Label)rptSummary.Items[i].FindControl("lblColors");
                    var lblItemsR = (Label)rptSummary.Items[i].FindControl("lblItems");
                    var lblLOTNumberR = (Label)rptSummary.Items[i].FindControl("lblLOTNumber");
                    var lblStoresR = (Label)rptSummary.Items[i].FindControl("lblStores");
                    var lblUnitsR = (Label)rptSummary.Items[i].FindControl("lblUnits");

                    lblIssueDateR.Visible = true;
                    lblStylesR.Visible = true;
                    lblColorsR.Visible = true;
                    lblItemsR.Visible = true;
                    lblLOTNumberR.Visible = true;
                    lblStoresR.Visible = true;
                    lblUnitsR.Visible = true;
                }
            }


            var tbxUpdatedQty = (TextBox)((RepeaterItem)((LinkButton)sender).Parent).FindControl("UpdatedIssuedQty");
            var lblIssuedQty = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("IssuedQty");
            var btnUpdate = (LinkButton)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lnkbtnUpdated");
            var lnkbtnCancel = (LinkButton)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lnkbtnCancel");
            var btnDelete = (LinkButton)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lnkbtnDelete");

            var rptIssueDate = (TextBox)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptIssueDate");
            var rptStyles = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptStyles");
            var rptColors = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptColors");
            var rptItems = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptItems");
            var rptLOTNumber = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptLOTNumber");
            var rptStores = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptStores");
            var rptUnits = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptUnits");

            var lblIssueDate = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblIssueDate");
            var lblStyles = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblStyles");
            var lblColors = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblColors");
            var lblItems = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblItems");
            var lblLOTNumber = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblLOTNumber");
            var lblStores = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblStores");
            var lblUnits = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblUnits");
            var trAvailableBalance = (PlaceHolder)((RepeaterItem)((LinkButton)sender).Parent).FindControl("trAvailableBalance");
            var rptAvailableBalance = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptAvailableBalance");


            var btnEdit = ((LinkButton)sender);

            tbxUpdatedQty.Visible = true;
            lblIssuedQty.Visible = false;
            btnUpdate.Visible = true;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            lnkbtnCancel.Visible = true;
            trAvailableBalance.Visible = true;
            //Response.Redirect("IssueYarnAtStoreV3.aspx?yarnIssueId=" + Tools.UrlEncode(yarnIssueId + ""));

            var issueInfo = unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().GetByID(yarnIssueId);

            LoadStyleDropdown(issueInfo.BuyerId, rptStyles);
            LoadColorDropdown(issueInfo.StyleId, rptColors);
            LoadItemDropdown(rptItems, issueInfo.StyleId, issueInfo.StoreId);
            LoadLOTDropdown(rptLOTNumber, issueInfo.StyleId, issueInfo.StoreId);
            LoadKnittingUnits(rptUnits);
            LoadStores(rptStores);

            //set data
            rptStyles.SelectedValue = issueInfo.StyleId + "";
            rptColors.SelectedValue = issueInfo.ColorId + "";
            rptItems.SelectedValue = issueInfo.ItemId + "";
            rptLOTNumber.SelectedValue = issueInfo.LotNumber + "";
            rptUnits.SelectedValue = issueInfo.KnittingUnitId + "";
            rptStores.SelectedValue = issueInfo.StoreId + "";
            rptIssueDate.Text = issueInfo.IssueDate.ToString("yyyy-MM-dd");

            rptIssueDate.Visible = true;
            rptColors.Visible = true;
            rptItems.Visible = true;
            rptLOTNumber.Visible = true;
            rptUnits.Visible = true;
            rptStores.Visible = true;
            rptStyles.Visible = true;

            lblIssueDate.Visible = false;
            lblStyles.Visible = false;
            lblColors.Visible = false;
            lblUnits.Visible = false;
            lblItems.Visible = false;
            lblLOTNumber.Visible = false;
            lblStores.Visible = false;
            lblAvailableBalance.Visible = true;
            ShowAvailableBalance(rptStyles, rptColors, rptItems, rptLOTNumber, rptStores, rptAvailableBalance);

        }
        private void LoadStores(DropDownList ddl)
        {
            var dt = unitOfWork.GetDataTableFromSql("SELECT StoreId, StoreName FROM Stores WHERE StoreType = 2");
            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = "StoreName";
            ddl.DataValueField = "StoreId";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;
        }

        private void LoadKnittingUnits(DropDownList ddl)
        {
            var dtKnittingUnit = new CommonManager().GetProductionUnitsByUser(CommonMethods.SessionInfo.UserId);
            var lstOtherUnits = new UnitOfWork().GenericRepositories<OtherKnittingUnits>().Get().ToList();
            foreach (var item in lstOtherUnits)
            {
                var drow = dtKnittingUnit.NewRow();
                drow["ProductionUnitId"] = item.ProductionUnitId;
                drow["UnitName"] = item.UnitName;
                dtKnittingUnit.Rows.Add(drow);
            }

            //var dr = dtKnittingUnit.NewRow();
            //dr["ProductionUnitId"] = 99;
            //dr["UnitName"] = "Leftover";
            //dtKnittingUnit.Rows.Add(dr);

            //var dr1 = dtKnittingUnit.NewRow();
            //dr1["ProductionUnitId"] = 100;
            //dr1["UnitName"] = "On hold";
            //dtKnittingUnit.Rows.Add(dr1);

            //var dr2 = dtKnittingUnit.NewRow();
            //dr2["ProductionUnitId"] = 101;
            //dr2["UnitName"] = "Mega Dyeing";
            //dtKnittingUnit.Rows.Add(dr2);


            ddl.DataSource = dtKnittingUnit;
            ddl.DataTextField = "UnitName";
            ddl.DataValueField = "ProductionUnitId";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;
        }

        private void LoadStyleDropdown(int buyerId, DropDownList ddlStyles)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        private void LoadColorDropdown(int styleId, DropDownList ddl)
        {
            var dt = orderManager.GetColorsByStyleId(styleId);

            var dr = dt.NewRow();
            dr["BuyerColorId"] = "";
            dr["ColorDescription"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);
            ddl.DataSource = dt;
            ddl.DataTextField = "ColorDescription";
            ddl.DataValueField = "BuyerColorId";
            ddl.DataBind();
        }

        private void LoadLOTDropdown(DropDownList ddl, int styleId, int storeId)
        {
            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetYarnReceivedLotNumberByStyleId {styleId},{storeId}");
            var dr = dt.NewRow();
            dr["Value"] = "";
            dr["Text"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);
            ddl.DataSource = dt;
            ddl.DataTextField = "Text";
            ddl.DataValueField = "Value";
            ddl.DataBind();
        }

        protected void LoadItemDropdown(DropDownList ddlItem, int styleId, int storeId)
        {
            var dtItems = unitOfWork.GetDataTableFromSql($"EXEC usp_GetYarnItemsByStyleId {styleId},{storeId}");
            ddlItem.Items.Add(new ListItem("", ""));
            ddlItem.DataSource = dtItems;
            ddlItem.DataTextField = "ItemName";
            ddlItem.DataValueField = "ItemId";
            ddlItem.DataBind();
            ddlItem.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlItem.SelectedIndex = 0;
        }

        int YarnIssueId
        {
            set { ViewState["yarnIssueId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["yarnIssueId"]);
                }
                catch
                {
                    return 0;
                }
            }

        }


        protected void rptDetailInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var buyerId = int.Parse(((Label)e.Item.FindControl("lblBuyerId")).Text);
                var styleId = int.Parse(((Label)e.Item.FindControl("lblStyleId")).Text);
                var dtSizes = buyerManager.GetStyleSizes(styleId);

                GridView gvSizeQuantity1 = (GridView)e.Item.FindControl("gvSizeQuantity1");
                Label lblNoSizeFound = (Label)e.Item.FindControl("lblNoSizeFound");
                if (dtSizes.Rows.Count > 0)
                {
                    DataTable dtOneRow = new DataTable();
                    DataRow dr = null;
                    dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                    dr = dtOneRow.NewRow();
                    dr["RowNumber"] = 1;
                    dtOneRow.Rows.Add(dr);

                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantity1.Columns.Add(templateField);
                    }

                    gvSizeQuantity1.DataSource = dtOneRow;
                    gvSizeQuantity1.DataBind();
                    gvSizeQuantity1.Visible = true;
                    lblNoSizeFound.Visible = false;

                }
                else
                {
                    gvSizeQuantity1.Visible = false;
                    lblNoSizeFound.Visible = true;
                }
            }
        }

        //protected void gvSizeQuantity1_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        var buyerId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId")).Text);
        //        var styleId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId")).Text);
        //        //var dtSizes = buyerManager.GetSizes(buyerId);
        //        var dtSizes = buyerManager.GetStyleSizes(styleId);

        //        Label txtSizeQty = null;
        //        Label lblSizeId = null;

        //        for (int i = 0; i < dtSizes.Rows.Count; i++)
        //        {
        //            txtSizeQty = new Label();
        //            txtSizeQty.ID = "tbxReceivQuantity" + i.ToString();
        //            txtSizeQty.Text = "";
        //            txtSizeQty.Width = 70;
        //            e.Row.Cells[i].Controls.Add(txtSizeQty);


        //            lblSizeId = new Label();
        //            lblSizeId.ID = "lblSizeId" + i.ToString();
        //            lblSizeId.Text = dtSizes.Rows[i][0].ToString();
        //            lblSizeId.Visible = false;
        //            e.Row.Cells[i].Controls.Add(lblSizeId);

        //            var entryId = new Label();
        //            entryId.ID = "lblentryId" + i.ToString();
        //            entryId.Text = "";
        //            entryId.Visible = false;
        //            e.Row.Cells[i].Controls.Add(entryId);


        //            var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());

        //            var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
        //            var buyerColorId = int.Parse(string.IsNullOrEmpty(lblBuyerColorId.Text) ? "0" : lblBuyerColorId.Text);

        //            var lblLotNumber = (Label)((GridView)sender).DataItemContainer.FindControl("lblLotNumber");

        //            if (buyerColorId != 0)
        //            {
        //                var sizeInfo = unitOfWork.GenericRepositories<YarnIssuedAtStoreColorSize>().Get(x => x.IssueId == YarnIssueId && x.ColorId == buyerColorId && x.LotNumber == lblLotNumber.Text && x.SizeId == sizeId).FirstOrDefault();
        //                if (sizeInfo != null)
        //                {
        //                    txtSizeQty.Text = sizeInfo.IssuedQty + "";
        //                    entryId.Text = sizeInfo.Id + "";
        //                }
        //            }
        //        }
        //    }
        //}

        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            YarnIssueId = int.Parse(e.CommandArgument.ToString());
            if (YarnIssueId != 0)
            {
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);

                //Page.ClientScript.RegisterStartupScript(this.GetType(), "toastr_message", "toastr.success('Are you working?')", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "toastr_message", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);

            }
            else
            {
                var title = "Error";
                var msg = "Sorry, you can not delete it.<br />Something went wrong.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            }
        }

        protected void lnkbtnUpdated_Command(object sender, CommandEventArgs e)
        {
            var yarnIssueId = int.Parse(e.CommandArgument.ToString());
            var tbxUpdatedQty = (TextBox)((RepeaterItem)((LinkButton)sender).Parent).FindControl("UpdatedIssuedQty");
            var lblIssuedQty = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("IssuedQty");
            var btnUpdate = (LinkButton)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lnkbtnUpdated");
            var btnEdit = ((LinkButton)sender);
            var ddl = (LinkButton)sender;
            var rptIssueDate = (TextBox)ddl.NamingContainer.FindControl("rptIssueDate");
            var rptStyles = (DropDownList)ddl.NamingContainer.FindControl("rptStyles");
            var rptColors = (DropDownList)ddl.NamingContainer.FindControl("rptColors");
            var rptItems = (DropDownList)ddl.NamingContainer.FindControl("rptItems");
            var rptLOTNumber = (DropDownList)ddl.NamingContainer.FindControl("rptLOTNumber");
            var rptStores = (DropDownList)ddl.NamingContainer.FindControl("rptStores");
            var rptUnits = (DropDownList)ddl.NamingContainer.FindControl("rptUnits");
            if (string.IsNullOrEmpty(rptIssueDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select style.')", true);
                return;
            }
            else if (rptStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select style.')", true);
                return;
            }
            else if (rptColors.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select color.')", true);
                return;
            }
            else if (rptItems.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select item.')", true);
                return;
            }
            else if (rptLOTNumber.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select LOT Number.')", true);
                return;
            }
            else if (rptStores.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select store.')", true);
                return;
            }
            else if (rptUnits.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select knitting unit.')", true);
                return;
            }
            if (!string.IsNullOrEmpty(tbxUpdatedQty.Text))
            {
                var issueInfo = unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().GetByID(yarnIssueId);
                if (issueInfo != null)
                {
                    //var issueInfo = unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().GetByID(yarnIssueId);

                    var updatedIssuedQty = decimal.Parse(tbxUpdatedQty.Text);
                    var styleId = 0;
                    if (rptStyles.SelectedValue != "")
                    {
                        styleId = int.Parse(rptStyles.SelectedValue);
                    }
                    var colorId = 0;
                    if (rptColors.SelectedValue != "")
                    {
                        colorId = int.Parse(rptColors.SelectedValue);
                    }

                    int storeId = 0;
                    if (rptStores.SelectedValue != "")
                    {
                        storeId = int.Parse(rptStores.SelectedValue);
                    }
                    var itemId = 0;
                    if (rptItems.SelectedValue != "")
                    {
                        itemId = int.Parse(rptItems.SelectedValue);
                    }
                    var lotNumber = "";
                    if (rptLOTNumber.SelectedValue != "")
                    {
                        lotNumber = rptLOTNumber.SelectedValue;
                    }

                    var availableBalance = decimal.Parse(unitOfWork.GetSingleStringValue($"EXEC usp_GetAvailableBalanceByStyleLotColorV2 {styleId},'{lotNumber}',{colorId},{storeId},{itemId}"));
                    //var issueInfo = unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().Get(x => x.IssueId == YarnIssueId).FirstOrDefault();
                    if (issueInfo.StoreId == storeId && issueInfo.ItemId == itemId && issueInfo.LotNumber == lotNumber && issueInfo.ColorId == colorId && issueInfo.StyleId == styleId)
                    {
                        availableBalance += issueInfo.IssuedQuantityLbs;
                    }
                    //var totalAvailableBalance = availableBalance + issueInfo.IssuedQuantityLbs;
                    if (updatedIssuedQty <= availableBalance)
                    {
                        tbxUpdatedQty.Visible = true;
                        lblIssuedQty.Visible = false;
                        btnUpdate.Visible = true;
                        btnEdit.Visible = false;

                        issueInfo.IssuedQuantityLbs = decimal.Parse(tbxUpdatedQty.Text);
                        issueInfo.UpdateDate = DateTime.Now;
                        issueInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                        issueInfo.StyleId = int.Parse(rptStyles.SelectedValue);
                        issueInfo.IssueDate = DateTime.Parse(rptIssueDate.Text);
                        issueInfo.ColorId = int.Parse(rptColors.SelectedValue);
                        issueInfo.ItemId = int.Parse(rptItems.SelectedValue);
                        issueInfo.LotNumber = rptLOTNumber.Text;
                        issueInfo.KnittingUnitId = int.Parse(rptUnits.SelectedValue);
                        issueInfo.StoreId = int.Parse(rptStores.SelectedValue);
                        unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().Update(issueInfo);
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                        LoadData();
                        lblAvailableBalance.Visible = false;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('You can not issue more than available balance. Available balance is {availableBalance}')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Unable to update.')", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter issue quantity.')", true);
            }

        }

        protected void SelectedIndexChanged(object sender, EventArgs e)
        {
            var ddl = (DropDownList)sender;
            var rptStyles = (DropDownList)ddl.NamingContainer.FindControl("rptStyles");
            var rptColors = (DropDownList)ddl.NamingContainer.FindControl("rptColors");
            var rptItems = (DropDownList)ddl.NamingContainer.FindControl("rptItems");
            var rptLOTNumber = (DropDownList)ddl.NamingContainer.FindControl("rptLOTNumber");
            var rptStores = (DropDownList)ddl.NamingContainer.FindControl("rptStores");
            var rptUnits = (DropDownList)ddl.NamingContainer.FindControl("rptUnits");
            //var trAvailableBalance = (PlaceHolder)ddl.NamingContainer.FindControl("trAvailableBalance");
            var rptAvailableBalance = (Label)ddl.NamingContainer.FindControl("rptAvailableBalance");

            //var rptIssueDate = (TextBox)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptIssueDate");
            //var rptStyles = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptStyles");
            //var rptColors = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptColors");
            //var rptItems = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptItems");
            //var rptLOTNumber = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptLOTNumber");
            //var rptStores = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptStores");
            //var rptUnits = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptUnits");

            ShowAvailableBalance(rptStyles, rptColors, rptItems, rptLOTNumber, rptStores, rptAvailableBalance);
        }

        private void ShowAvailableBalance(DropDownList ddlStyles, DropDownList ddlColor, DropDownList ddlItems, DropDownList ddlLotNumber, DropDownList ddlStore, Label lblAB)
        {
            if (ddlColor.SelectedValue != "" && ddlItems.SelectedValue != "" && ddlLotNumber.SelectedValue != "")
            {
                var colorId = int.Parse(ddlColor.SelectedValue);
                int styleId = 0;
                if (ddlStyles.SelectedValue != "")
                {
                    styleId = int.Parse(ddlStyles.SelectedValue);
                }
                int storeId = 0;
                if (ddlStore.SelectedValue != "")
                {
                    storeId = int.Parse(ddlStore.SelectedValue);
                }
                var itemId = 0;
                if (ddlItems.SelectedValue != "")
                {
                    itemId = int.Parse(ddlItems.SelectedValue);
                }
                var lotNumber = "";
                if (ddlLotNumber.SelectedValue != "")
                {
                    lotNumber = ddlLotNumber.SelectedValue;
                }
                var availableBalance = decimal.Parse(unitOfWork.GetSingleStringValue($"EXEC usp_GetAvailableBalanceByStyleLotColorV2 '{styleId}','{lotNumber}',{colorId},{storeId},{itemId}"));
                if (YarnIssueId > 0)
                {
                    var issueInfo = unitOfWork.GenericRepositories<YarnIssuedAtStoreV2>().Get(x => x.IssueId == YarnIssueId).FirstOrDefault();
                    if (issueInfo.StoreId == storeId && issueInfo.ItemId == itemId && issueInfo.LotNumber == lotNumber && issueInfo.ColorId == colorId && issueInfo.StyleId == styleId)
                    {
                        availableBalance += issueInfo.IssuedQuantityLbs;
                    }
                }
                lblAB.Text = "Available Balance: " + availableBalance + "";
            }
            else
            {
                lblAB.Text = "";
            }
        }


        protected void rptStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            var rptStyles = (DropDownList)sender;
            var rptColors = (DropDownList)((RepeaterItem)((DropDownList)sender).Parent).FindControl("rptColors");
            var rptItems = (DropDownList)((RepeaterItem)((DropDownList)sender).Parent).FindControl("rptItems");
            var rptLOTNumber = (DropDownList)((RepeaterItem)((DropDownList)sender).Parent).FindControl("rptLOTNumber");
            var rptStores = (DropDownList)((RepeaterItem)((DropDownList)sender).Parent).FindControl("rptStores");
            var rptUnits = (DropDownList)((RepeaterItem)((DropDownList)sender).Parent).FindControl("rptUnits");
            var rptAvailableBalance = (Label)((RepeaterItem)((DropDownList)sender).Parent).FindControl("rptAvailableBalance");
            rptColors.Items.Clear();
            rptItems.Items.Clear();
            rptLOTNumber.Items.Clear();

            if (rptStyles.SelectedValue != "")
            {
                var styleId = int.Parse(rptStyles.SelectedValue);
                LoadColorDropdown(styleId, rptColors);
                if (rptStores.SelectedValue != "")
                {
                    LoadLOTDropdown(rptLOTNumber, styleId, int.Parse(rptStores.SelectedValue));
                    LoadItemDropdown(rptItems, styleId, int.Parse(rptStores.SelectedValue));
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "toastr_message", "toastr.error('Please select store first.')", true);
                }

                ShowAvailableBalance(rptStyles, rptColors, rptItems, rptLOTNumber, rptStores, rptAvailableBalance);
            }
        }

        protected void lnkbtnCancel_Command(object sender, CommandEventArgs e)
        {
            //var yarnIssueId = int.Parse(e.CommandArgument.ToString());
            //var tbxUpdatedQty = (TextBox)((RepeaterItem)((LinkButton)sender).Parent).FindControl("UpdatedIssuedQty");
            //var lblIssuedQty = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("IssuedQty");
            //var btnUpdate = (LinkButton)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lnkbtnUpdated");
            //var btnEdit = ((LinkButton)sender);

            //tbxUpdatedQty.Visible = true;
            //lblIssuedQty.Visible = false;
            //btnUpdate.Visible = true;
            //btnEdit.Visible = false;
            LoadData();
            lblAvailableBalance.Visible = false;
        }
    }
}