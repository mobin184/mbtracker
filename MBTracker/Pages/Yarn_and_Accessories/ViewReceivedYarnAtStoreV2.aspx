﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" CodeBehind="ViewReceivedYarnAtStoreV2.aspx.cs" Inherits="MBTracker.Pages.Yarn_and_Accessories.ViewReceivedYarnAtStoreV2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 5px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }
    </style>
    <div class="row-fluid" runat="server" id="pnlEntry">
        <div class="span9">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="View/Edit Received Yarn at Store:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body" style="min-height: 250px">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="span-12" style="display: flex; justify-content: center; margin-top: -10px; padding-bottom: 20px;">
                            Please select buyer with style or supplier.
                        </div>
                        <div class="span6" style="width: 46%">
                            <div class="control-group">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputStyle" class="control-label">
                                    <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputStyle" class="control-label">
                                    <asp:Label ID="Label4" runat="server" Text="Received Date (Optional):"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxReceivedDate" runat="server" Width="100%" TextMode="Date"></asp:TextBox>
                                </div>
                            </div>

                        </div>

                        <div class="span6" style="width: 46%;">
                            <div class="control-group">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="Label10" runat="server" Text="Select Supplier:"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlSupplier" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlSupplier_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlSupplier"><span style="font-weight: 700; color: #CC0000">Please select a supplier.</span></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputStyle" class="control-label">
                                    <asp:Label ID="Label11" runat="server" Text="Select L/C (Optional):"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlLCNumber" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlLCNumber"><span style="font-weight: 700; color: #CC0000">Please select a L/C number.</span></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputStyle" class="control-label">
                                    <asp:Label ID="Label12" runat="server" Text="Dyeing Order # (Optional):"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlDyeingOrderNumber" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlLCNddlDyeingOrderNumberumber"><span style="font-weight: 700; color: #CC0000">Please select a L/C number.</span></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                        </div>
                        <div class="span-12" style="display: flex; justify-content: center; padding-top: 20px;">
                            <br />
                            <asp:Button ID="btnSearch" ValidationGroup="save" runat="server" class="btn btn-success btn-midium pull-left btnStyle" Text="View Received Yarn" OnClick="btnSearch_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <asp:Label ID="lblSummryNotFound" runat="server" Text="No data found." Visible="false" BackColor="#ffff00"></asp:Label>
        <div class="span12" runat="server" id="pnlSummary" visible="false">
            <div class="widget">
                <div class="widget-body">
                    <div class="control-group">
                        <label for="inputBuyer" class="control-label">
                            <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Received Yarn Information:"></asp:Label>
                        </label>
                    </div>
                    <div class="controls controls-row">
                        <div id="dt_example" class="example_alt_pagination">
                            <asp:Repeater ID="rptSummary" Visible="false" runat="server">
                                <HeaderTemplate>
                                    <table id="data-table" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>

                                                <th>
                                                    <asp:Label ID="Label5" runat="server" Text="Supplier"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label15" runat="server" Text="Merchandiser"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label6" runat="server" Text="Purchase Type"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label7" runat="server" Text="Store"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label8" runat="server" Text="Received Date"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label14" runat="server" Text="Invoice Number"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label16" runat="server" Text="Chalan Number"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="lbl2" runat="server" Text="L/C Number"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label2" runat="server" Text="Dyeing Order #"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label9" runat="server" Text="Quantity"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label3" runat="server" Text="Take  Action"></asp:Label>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <%#Eval("SupplierName") %>
                                        </td>
                                        <td>
                                            <%#Eval("MerchandiserName") %>
                                        </td>
                                        <td>
                                            <%#Eval("PurchaseType") %>
                                        </td>
                                        <td>
                                            <%#Eval("StoreName") %>
                                        </td>
                                        <td>
                                            <%#Eval("ReceivedDate") %>
                                        </td>
                                        <td>
                                            <%#Eval("InvoiceNumber") %>
                                        </td>
                                        <td>
                                            <%#Eval("ChalanNo") %>
                                        </td>
                                        <td>
                                            <%#Eval("LCNumber") %>
                                        </td>
                                        <td>
                                            <%#Eval("DyeingOrderNumber") %>
                                        </td>
                                        <td>
                                            <%#Eval("ReceivedQty") %>
                                        </td>
                                        <td>
                                            <%--<asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("YarnReceivedId") %>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" Visible='<%# ViewState["editEnabled"]%>' CommandName="Edit" CommandArgument='<%#Eval("YarnReceivedId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>--%>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#Eval("CanDelete")%>' CommandArgument='<%#Eval("YarnReceivedId") %>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" Visible='<%# Eval("CanEdit")%>' CommandName="Edit" CommandArgument='<%#Eval("YarnReceivedId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnDetails" runat="server" CommandName="ViewDetails" CommandArgument='<%#Eval("YarnReceivedId") %>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnDetails_Command" Text="View Items"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                                </table>                                                  
                                </FooterTemplate>
                            </asp:Repeater>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <asp:Label ID="lblNoDetails" runat="server" Text="No details found." Visible="false" BackColor="#ffff00"></asp:Label>
        <div class="span9" runat="server" id="pnlDetails" visible="false">
            <div class="widget">
                <div class="widget-body">
                    <div class="control-group">
                        <label for="inputBuyer" class="control-label">
                            <asp:Label ID="Label13" runat="server" Font-Bold="true" Text="Received Yarn Item Information:"></asp:Label>
                        </label>
                    </div>
                    <div class="controls controls-row">
                        <div id="dt_example" class="example_alt_pagination">
                            <asp:Repeater ID="rptDetails" Visible="false" runat="server">
                                <HeaderTemplate>
                                    <table id="data-table1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>

                                                <th>
                                                    <asp:Label ID="Label5" runat="server" Text="Buyer"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label6" runat="server" Text="Style"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label8" runat="server" Text="Color"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="lbl2" runat="server" Text="Item Description"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label2" runat="server" Text="LOT Number"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label9" runat="server" Text="Received Quantity"></asp:Label>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>

                                        <td>
                                            <%#Eval("BuyerName") %>
                                        </td>
                                        <td>
                                            <%#Eval("StyleName") %>
                                        </td>
                                        <td>
                                            <%#Eval("ColorName") %>
                                        </td>
                                        <td>
                                            <%#Eval("ItemName") %>
                                        </td>
                                        <td>
                                            <%#Eval("LOTNumber") %>
                                        </td>
                                        <td>
                                            <%#Eval("ReceivedQty") %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                                </table>                                                  
                                </FooterTemplate>
                            </asp:Repeater>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>

