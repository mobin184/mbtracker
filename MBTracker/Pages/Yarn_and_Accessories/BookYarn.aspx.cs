﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class BookYarn : System.Web.UI.Page
    {

        //BuyerManager buyerManager = new BuyerManager();
        //OrderManager orderManager = new OrderManager();
        //DataTable dtYarntStatus;

        //UnitOfWork unitOfWork = new UnitOfWork();
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    if (!IsPostBack)
        //    {
        //        CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
        //        if (Request.QueryString["bookingId"] != null)
        //        {
        //            BookingId = int.Parse(Tools.UrlDecode(Request.QueryString["bookingId"]));
        //            var bookingInfo = unitOfWork.GenericRepositories<OrderYarnBookings>().Get(x => x.BookingId == BookingId).FirstOrDefault();
        //            var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(bookingInfo.StyleId);

        //            BindStylesByBuyer(styleInfo.BuyerId);
        //            BindColorsByStyle(bookingInfo.StyleId);
        //            BindBookingSummaryByStyle(bookingInfo.StyleId);
        //            BindColorWiseSummary(bookingInfo.StyleId);
        //            BindTotalOrderQuantity(bookingInfo.StyleId);
        //            ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, bookingInfo.StyleId);
        //            ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);

        //            ddlStyles.Enabled = false;
        //            ddlBuyers.Enabled = false;                    
        //        }
        //    }
        //}

        //int BookingId
        //{
        //    set { ViewState["bookingId"] = value; }
        //    get
        //    {
        //        try
        //        {
        //            return Convert.ToInt32(ViewState["bookingId"]);
        //        }
        //        catch
        //        {
        //            return 0;
        //        }
        //    }
        //}

        //protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddlBuyers.SelectedValue != "")
        //    {
        //        BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
        //    }
        //    else
        //    {
        //        ddlStyles.Items.Clear();
        //    }

        //    pnlYarnBooking.Visible = false;
        //    lblNoColorFound.Visible = false;
        //    rptColors.DataSource = null;
        //    rptColors.DataBind();
        //    pnlSummary.Visible = false;
        //    rptSummary.DataSource = null;
        //    rptSummary.DataBind();
        //    lblColorWizeDataNotFound.Visible = false;
        //    DivColorSummary.Visible = false;
        //    rptColorWiseSummary.DataSource = null;
        //    rptColorWiseSummary.DataBind();
        //    divTotalOrderQuantity.Visible = false;
        //}

        //private void BindStylesByBuyer(int buyerId)
        //{
        //    CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        //}

        //protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    pnlYarnBooking.Visible = false;
        //    lblNoColorFound.Visible = false;
        //    pnlSummary.Visible = false;
        //    lblSummryNotFound.Visible = false;
        //    DivColorSummary.Visible = false;
        //    lblColorWizeDataNotFound.Visible = false;
        //    divTotalOrderQuantity.Visible = false;
        //    if (ddlStyles.SelectedValue != "")
        //    {
        //        var styleId = Convert.ToInt32(ddlStyles.SelectedValue);
        //        //BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
        //        BindColorsByStyle(styleId);
        //        BindBookingSummaryByStyle(styleId);
        //        BindColorWiseSummary(styleId);
        //        BindTotalOrderQuantity(styleId);
        //    }
        //}

        //private void BindTotalOrderQuantity(int styleId)
        //{
        //    divTotalOrderQuantity.Visible = true;
        //    var totalOrderQty = unitOfWork.GetSingleValue($"Exec usp_GetActiveOrderQuantityByStyleId {styleId}");
        //    divTotalOrderQuantity.InnerHtml = $"<div style='width:100%; padding-bottom:9px; margin-bottom:5px; font-size:13px; padding:10px; background-color:#ffff00aa; font-weight:bold'>Order Quantity: <span style='font-size:15px;'>{totalOrderQty}</span>    &nbsp;&nbsp;<asp:LinkButton ID='lnkbtnView' onClick='window.open(\"../Reports/SummaryOfOrders.aspx?styleId={Tools.UrlEncode(styleId+"")}\",\"_blank\")' class='btn btn-success btn-mini hidden-phone pull-right' Text='View Details'>View Details</asp:LinkButton></div>";
        //}

        //private void BindColorWiseSummary(int styleId)
        //{
        //    string sql = $"Exec usp_GetColorBookingSummaryByStyleId {styleId}";
        //    var dtSummary = unitOfWork.GetDataTableFromSql(sql);
        //    DivColorSummary.Visible = true;
        //    if (dtSummary.Rows.Count > 0)
        //    {
        //        rptColorWiseSummary.DataSource = dtSummary;
        //        rptColorWiseSummary.DataBind();
        //        rptColorWiseSummary.Visible = true;
        //        lblColorWizeDataNotFound.Visible = false;
        //    }
        //    else
        //    {
        //        rptColorWiseSummary.DataSource = null;
        //        rptColorWiseSummary.DataBind();
        //        rptColorWiseSummary.Visible = false;
        //        lblColorWizeDataNotFound.Visible = true;
        //    }
        //}

        //private void BindBookingSummaryByStyle(int styleId)
        //{
        //    string sql = $"Exec usp_GetStyleWiseYarnBookingSummary {styleId}";
        //    var dtSummary = unitOfWork.GetDataTableFromSql(sql);
        //    pnlSummary.Visible = true;
        //    if (dtSummary.Rows.Count > 0)
        //    {
        //        rptSummary.DataSource = dtSummary;
        //        rptSummary.DataBind();
        //        rptSummary.Visible = true;
        //        lblSummryNotFound.Visible = false;
        //    }
        //    else
        //    {
        //        rptSummary.DataSource = null;
        //        rptSummary.DataBind();
        //        rptSummary.Visible = false;
        //        lblSummryNotFound.Visible = true;
        //    }
        //}

        //private void BindColorsByStyle(int styleId)
        //{
        //    string sql = $"Exec usp_GetStyleColorsByStyleId {styleId}";
        //    var dtColors = unitOfWork.GetDataTableFromSql(sql);
        //    pnlYarnBooking.Visible = true;
        //    if (dtColors.Rows.Count > 0)
        //    {
        //        dtYarntStatus = orderManager.GetYarnProcurementStatus();
        //        rptColors.DataSource = dtColors;
        //        rptColors.DataBind();
        //        rptColors.Visible = true;
        //        lblNoColorFound.Visible = false;
        //        if (BookingId > 0)
        //        {
        //            btnSaveYarnBooking.Visible = false;
        //            btnUpdateYarnBooking.Visible = true;
        //        }
        //        else
        //        {
        //            btnSaveYarnBooking.Visible = true;
        //            btnUpdateYarnBooking.Visible = false;
        //        }
        //    }
        //    else
        //    {
        //        rptColors.DataSource = null;
        //        rptColors.DataBind();
        //        rptColors.Visible = false;
        //        btnSaveYarnBooking.Visible = false;
        //        lblNoColorFound.Visible = true;
        //    }
        //}

        //private void SetInitialRowCount(Repeater rpt)
        //{
        //    DataTable dt = new DataTable();
        //    DataRow dr = null;
        //    dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Column0", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Column1", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Column2", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Column3", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Column4", typeof(string)));


        //    dr = dt.NewRow();
        //    dr["RowNumber"] = 1;
        //    dr["Column0"] = string.Empty;
        //    dr["Column1"] = string.Empty;
        //    dr["Column2"] = string.Empty;
        //    dr["Column3"] = string.Empty;
        //    dr["Column4"] = string.Empty;

        //    dt.Rows.Add(dr);

        //    //Store the DataTable in ViewState
        //    ViewState[rpt.UniqueID] = dt;

        //    rpt.DataSource = dt;
        //    rpt.DataBind();
        //}

        //protected void btnAddRow_Click(object sender, EventArgs e)
        //{
        //    var rpt = (Repeater)((Button)sender).DataItemContainer.DataItemContainer.FindControl("rpt");
        //    AddNewRowToGrid(rpt);
        //}


        //private void AddNewRowToGrid(Repeater rpt)
        //{
        //    int rowIndex;
        //    dtYarntStatus = orderManager.GetYarnProcurementStatus();

        //    if (ViewState[rpt.UniqueID] != null)
        //    {
        //        DataTable dtCurrentTable = (DataTable)ViewState[rpt.UniqueID];
        //        DataRow drCurrentRow = null;

        //        if (dtCurrentTable.Rows.Count > 0)
        //        {
        //            for (rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
        //            {
        //                TextBox box1 = (TextBox)rpt.Items[rowIndex].FindControl("tbxYarnDesc");
        //                TextBox box2 = (TextBox)rpt.Items[rowIndex].FindControl("tbxBookingQty");
        //                TextBox box3 = (TextBox)rpt.Items[rowIndex].FindControl("tbxSupplierName");
        //                TextBox box4 = (TextBox)rpt.Items[rowIndex].FindControl("tbxConsumptionPerDzn");
        //                DropDownList ddlStatus = (DropDownList)rpt.Items[rowIndex].FindControl("ddlStatus");

        //                dtCurrentTable.Rows[rowIndex]["Column0"] = box1.Text;
        //                dtCurrentTable.Rows[rowIndex]["Column1"] = box2.Text;
        //                dtCurrentTable.Rows[rowIndex]["Column2"] = box3.Text;
        //                dtCurrentTable.Rows[rowIndex]["Column3"] = box4.Text;
        //                dtCurrentTable.Rows[rowIndex]["Column4"] = ddlStatus.SelectedValue;
        //            }

        //            drCurrentRow = dtCurrentTable.NewRow();
        //            drCurrentRow["RowNumber"] = rowIndex + 1;

        //            dtCurrentTable.Rows.Add(drCurrentRow);
        //            ViewState[rpt.UniqueID] = dtCurrentTable;


        //            rpt.DataSource = dtCurrentTable;
        //            rpt.DataBind();
        //        }
        //        else
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr = null;
        //            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
        //            dt.Columns.Add(new DataColumn("Column0", typeof(string)));
        //            dt.Columns.Add(new DataColumn("Column1", typeof(string)));
        //            dt.Columns.Add(new DataColumn("Column2", typeof(string)));
        //            dt.Columns.Add(new DataColumn("Column3", typeof(string)));
        //            dt.Columns.Add(new DataColumn("Column4", typeof(string)));


        //            dr = dt.NewRow();
        //            dr["RowNumber"] = 1;
        //            dr["Column0"] = string.Empty;
        //            dr["Column1"] = string.Empty;
        //            dr["Column2"] = string.Empty;
        //            dr["Column3"] = string.Empty;
        //            dr["Column4"] = string.Empty;
        //            dt.Rows.Add(dr);

        //            ViewState[rpt.UniqueID] = dt;

        //            rpt.DataSource = dt;
        //            rpt.DataBind();

        //        }
        //    }
        //    else
        //    {
        //        Response.Write("ViewState is null");
        //    }

        //    //Set Previous Data on Postbacks
        //    SetPreviousData(rpt);

        //}


        //private void SetPreviousData(Repeater rpt)
        //{

        //    if (ViewState[rpt.UniqueID] != null)
        //    {
        //        DataTable dt = (DataTable)ViewState[rpt.UniqueID];
        //        if (dt.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < dt.Rows.Count; i++)
        //            {
        //                int ddlStatusSelectedValue = 0;
        //                TextBox box1 = (TextBox)rpt.Items[i].FindControl("tbxYarnDesc");
        //                TextBox box2 = (TextBox)rpt.Items[i].FindControl("tbxBookingQty");
        //                TextBox box3 = (TextBox)rpt.Items[i].FindControl("tbxSupplierName");
        //                TextBox box4 = (TextBox)rpt.Items[i].FindControl("tbxConsumptionPerDzn");
        //                DropDownList ddlStatus = (DropDownList)rpt.Items[i].FindControl("ddlStatus");

        //                box1.Text = dt.Rows[i]["Column0"].ToString();
        //                box2.Text = dt.Rows[i]["Column1"].ToString();
        //                box3.Text = dt.Rows[i]["Column2"].ToString();
        //                box4.Text = dt.Rows[i]["Column3"].ToString();

        //                if (dt.Rows[i]["Column4"].ToString() != "")
        //                {
        //                    ddlStatusSelectedValue = Convert.ToInt32(dt.Rows[i]["Column4"].ToString());
        //                }
        //                ddlStatus.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStatus, ddlStatusSelectedValue);
        //            }
        //        }
        //    }
        //}


        //protected void btnDelete_Command(object sender, CommandEventArgs e)
        //{
        //    var rpt = (Repeater)((LinkButton)sender).Parent.Parent;
        //    int rowIndex = Convert.ToInt32(e.CommandArgument.ToString());
        //    DataTable dtCurrentTable = (DataTable)ViewState[rpt.UniqueID];
        //    DataRow drDeleteRow = dtCurrentTable.Rows[rowIndex];

        //    drDeleteRow.Delete();
        //    dtCurrentTable.AcceptChanges();
        //    //for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
        //    //{
        //    //    dtCurrentTable.Rows[i - 1]["RowNumber"] = i;
        //    //}

        //    ViewState[rpt.UniqueID] = dtCurrentTable;
        //    dtYarntStatus = orderManager.GetYarnProcurementStatus();

        //    rpt.DataSource = dtCurrentTable;
        //    rpt.DataBind();
        //    SetPreviousData(rpt);
        //}


        //protected void btnSaveYarnBooking_Click(object sender, EventArgs e)
        //{
        //    SaveYarnBooking();
        //}

        //private void SaveYarnBooking()
        //{
        //    try
        //    {
        //        int descNotEntered = 0;
        //        int bookingQtyNotEntered = 0;
        //        int supplierNotEntered = 0;
        //        int statusNotSelected = 0;
        //        var styleId = int.Parse(ddlStyles.SelectedValue);
        //        List<OrderYarnBookings> lstOrderYarnBooking = new List<OrderYarnBookings>();

        //        var bookingId = unitOfWork.GenericRepositories<OrderYarnBookings>().Get().Select(x => x.BookingId).DefaultIfEmpty(0).Max();
        //        bookingId++;

        //        for (int i = 0; i < rptColors.Items.Count; i++)
        //        {
        //            var buyerColorId = int.Parse(((Label)rptColors.Items[i].FindControl("lblBuyerColorId")).Text);
        //            var rpt = (Repeater)rptColors.Items[i].FindControl("rpt");
        //            for (int j = 0; j < rpt.Items.Count; j++)
        //            {
        //                TextBox yarnDescription = (TextBox)rpt.Items[j].FindControl("tbxYarnDesc");
        //                TextBox yarnBookingQty = (TextBox)rpt.Items[j].FindControl("tbxBookingQty");
        //                TextBox supplier = (TextBox)rpt.Items[j].FindControl("tbxSupplierName");
        //                TextBox consPerDzn = (TextBox)rpt.Items[j].FindControl("tbxConsumptionPerDzn");
        //                DropDownList ddlYarnStatus = (DropDownList)rpt.Items[j].FindControl("ddlStatus");

        //                if (string.IsNullOrEmpty(yarnDescription.Text))
        //                {
        //                    descNotEntered++;
        //                    break;
        //                }
        //                if (string.IsNullOrEmpty(yarnBookingQty.Text))
        //                {
        //                    bookingQtyNotEntered++;
        //                    break;
        //                }
        //                if (string.IsNullOrEmpty(supplier.Text))
        //                {
        //                    supplierNotEntered++;
        //                    break;
        //                }
        //                if (string.IsNullOrEmpty(ddlYarnStatus.SelectedValue))
        //                {
        //                    statusNotSelected++;
        //                    break;
        //                }
        //                else
        //                {
        //                    var yb = new OrderYarnBookings()
        //                    {
        //                        BookingId = bookingId,
        //                        BuyerColorId = buyerColorId,
        //                        StyleId = styleId,
        //                        YarnDescription = yarnDescription.Text,
        //                        BookingQuantity = int.Parse(yarnBookingQty.Text),
        //                        Supplier = supplier.Text,
        //                        ConsumptionPerDzn = string.IsNullOrEmpty(consPerDzn.Text) ? (decimal?)null : decimal.Parse(consPerDzn.Text),
        //                        YarnProcurementStatus = int.Parse(ddlYarnStatus.SelectedValue),
        //                        CreateDate = DateTime.Now,
        //                        CreatedBy = CommonMethods.SessionInfo.UserName
        //                    };
        //                    lstOrderYarnBooking.Add(yb);
        //                }
        //            }

        //            if (descNotEntered > 0 || bookingQtyNotEntered > 0 || supplierNotEntered > 0 || statusNotSelected > 0)
        //            {
        //                break;
        //            }
        //        }

        //        if (descNotEntered > 0)
        //        {
        //            ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please enter yarn description.')", true);
        //        }
        //        else if (bookingQtyNotEntered > 0)
        //        {
        //            ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please enter booking quantity.')", true);
        //        }
        //        else if (supplierNotEntered > 0)
        //        {
        //            ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please enter supplier name.')", true);
        //        }
        //        else if (statusNotSelected > 0)
        //        {
        //            ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please select booking status.')", true);
        //        }
        //        else
        //        {
        //            if (lstOrderYarnBooking.Count > 0)
        //            {
        //                foreach (var item in lstOrderYarnBooking)
        //                {
        //                    unitOfWork.GenericRepositories<OrderYarnBookings>().Insert(item);
        //                }
        //                unitOfWork.Save();
        //                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please add yarn booking info.')", true);
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('" + Regex.Replace(ex.Message, "[^0-9a-zA-Z_ ]+", "") + "')", true);
        //    }
        //}

        //private void UpdateYarnBooking()
        //{
        //    try
        //    {
        //        int descNotEntered = 0;
        //        int bookingQtyNotEntered = 0;
        //        int supplierNotEntered = 0;
        //        int statusNotSelected = 0;
        //        var styleId = int.Parse(ddlStyles.SelectedValue);
        //        List<OrderYarnBookings> lstOrderYarnBooking = new List<OrderYarnBookings>();
        //        if (BookingId == 0)
        //        {
        //            BookingId = unitOfWork.GenericRepositories<OrderYarnBookings>().Get().Select(x => x.BookingId).DefaultIfEmpty(0).Max();
        //            BookingId++;
        //        }

        //        for (int i = 0; i < rptColors.Items.Count; i++)
        //        {
        //            var buyerColorId = int.Parse(((Label)rptColors.Items[i].FindControl("lblBuyerColorId")).Text);
        //            var rpt = (Repeater)rptColors.Items[i].FindControl("rpt");
        //            for (int j = 0; j < rpt.Items.Count; j++)
        //            {
        //                TextBox yarnDescription = (TextBox)rpt.Items[j].FindControl("tbxYarnDesc");
        //                TextBox yarnBookingQty = (TextBox)rpt.Items[j].FindControl("tbxBookingQty");
        //                TextBox supplier = (TextBox)rpt.Items[j].FindControl("tbxSupplierName");
        //                TextBox consPerDzn = (TextBox)rpt.Items[j].FindControl("tbxConsumptionPerDzn");
        //                DropDownList ddlYarnStatus = (DropDownList)rpt.Items[j].FindControl("ddlStatus");

        //                if (string.IsNullOrEmpty(yarnDescription.Text))
        //                {
        //                    descNotEntered++;
        //                    break;
        //                }
        //                if (string.IsNullOrEmpty(yarnBookingQty.Text))
        //                {
        //                    bookingQtyNotEntered++;
        //                    break;
        //                }
        //                if (string.IsNullOrEmpty(supplier.Text))
        //                {
        //                    supplierNotEntered++;
        //                    break;
        //                }
        //                if (string.IsNullOrEmpty(ddlYarnStatus.SelectedValue))
        //                {
        //                    statusNotSelected++;
        //                    break;
        //                }
        //                else
        //                {
        //                    var yb = new OrderYarnBookings()
        //                    {
        //                        BookingId = BookingId,
        //                        BuyerColorId = buyerColorId,
        //                        StyleId = styleId,
        //                        YarnDescription = yarnDescription.Text,
        //                        BookingQuantity = int.Parse(yarnBookingQty.Text),
        //                        Supplier = supplier.Text,
        //                        ConsumptionPerDzn = string.IsNullOrEmpty(consPerDzn.Text) ? (decimal?)null : decimal.Parse(consPerDzn.Text),
        //                        YarnProcurementStatus = int.Parse(ddlYarnStatus.SelectedValue),
        //                        CreateDate = DateTime.Now,
        //                        CreatedBy = CommonMethods.SessionInfo.UserName
        //                    };
        //                    lstOrderYarnBooking.Add(yb);
        //                }
        //            }

        //            if (descNotEntered > 0 || bookingQtyNotEntered > 0 || supplierNotEntered > 0 || statusNotSelected > 0)
        //            {
        //                break;
        //            }
        //        }

        //        if (descNotEntered > 0)
        //        {
        //            ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please enter yarn description.')", true);
        //        }
        //        else if (bookingQtyNotEntered > 0)
        //        {
        //            ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please enter booking quantity.')", true);
        //        }
        //        else if (supplierNotEntered > 0)
        //        {
        //            ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please enter supplier name.')", true);
        //        }
        //        else if (statusNotSelected > 0)
        //        {
        //            ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please select booking status.')", true);
        //        }
        //        else
        //        {
        //            if (lstOrderYarnBooking.Count > 0)
        //            {
        //                foreach (var item in lstOrderYarnBooking)
        //                {
        //                    unitOfWork.GenericRepositories<OrderYarnBookings>().Insert(item);
        //                }
        //                unitOfWork.Save();
        //                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
        //                //Response.AddHeader("REFRESH", "2;URL=ViewYarnBookings.aspx");
        //                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewYarnBookings.aspx');", true);
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please add yarn booking info.')", true);
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('" + Regex.Replace(ex.Message, "[^0-9a-zA-Z_ ]+", "") + "')", true);
        //    }
        //}

        //public DataTable YarnBookingTable()
        //{
        //    DataTable dt = new DataTable("YarnBooking");
        //    dt.Columns.Add("OrderId", typeof(int));
        //    dt.Columns.Add("Description", typeof(string));
        //    dt.Columns.Add("BookingQty", typeof(string));
        //    dt.Columns.Add("Supplier", typeof(string));
        //    dt.Columns.Add("ConsPerDzn", typeof(string));
        //    dt.Columns.Add("StatusId", typeof(int));
        //    dt.Columns.Add("CdBy", typeof(string));
        //    dt.Columns.Add("CDate", typeof(string));
        //    return dt;
        //}


        //protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{

        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {

        //        DropDownList ddlYarnStatus = (DropDownList)e.Item.FindControl("ddlStatus");
        //        ddlYarnStatus.DataSource = dtYarntStatus;
        //        ddlYarnStatus.DataBind();

        //        ddlYarnStatus.Items.Add(new ListItem("", ""));
        //        ddlYarnStatus.DataSource = dtYarntStatus;
        //        ddlYarnStatus.DataTextField = dtYarntStatus.Columns[1].ToString();
        //        ddlYarnStatus.DataValueField = dtYarntStatus.Columns[0].ToString();
        //        ddlYarnStatus.DataBind();
        //        ddlYarnStatus.Items.Insert(0, new ListItem("---Select---", string.Empty));
        //        ddlYarnStatus.SelectedIndex = 0;

        //        if (BookingId > 0)
        //        {
        //            TextBox tbxYarnDesc = (TextBox)e.Item.FindControl("tbxYarnDesc");
        //            TextBox tbxBookingQty = (TextBox)e.Item.FindControl("tbxBookingQty");
        //            TextBox tbxSupplierName = (TextBox)e.Item.FindControl("tbxSupplierName");
        //            TextBox tbxConsumptionPerDzn = (TextBox)e.Item.FindControl("tbxConsumptionPerDzn");

        //            tbxYarnDesc.Text = DataBinder.Eval(e.Item.DataItem, "Column0").ToString();
        //            tbxBookingQty.Text = DataBinder.Eval(e.Item.DataItem, "Column1").ToString();
        //            tbxSupplierName.Text = DataBinder.Eval(e.Item.DataItem, "Column2").ToString();
        //            tbxConsumptionPerDzn.Text = DataBinder.Eval(e.Item.DataItem, "Column3").ToString();
        //            ddlYarnStatus.SelectedIndex = CommonMethods.MatchDropDownItem(ddlYarnStatus,int.Parse(string.IsNullOrEmpty(DataBinder.Eval(e.Item.DataItem, "Column4").ToString()) ? "0" : DataBinder.Eval(e.Item.DataItem, "Column4").ToString()));
        //        }
        //    }
        //}

        //protected void btnUpdateYarnBooking_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        var bookingsInfo = unitOfWork.GenericRepositories<OrderYarnBookings>().Get(x => x.BookingId == BookingId).ToList();
        //        foreach (var item in bookingsInfo)
        //        {
        //            unitOfWork.GenericRepositories<OrderYarnBookings>().Delete(item);
        //        }
        //        UpdateYarnBooking();
        //    }
        //    catch (Exception ex)
        //    {
        //        ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "')", true);
        //    }
        //}


        //protected void rptColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        var rpt = (Repeater)e.Item.FindControl("rpt");
        //        if (BookingId > 0)
        //        {
        //            var buyerColorId = int.Parse(((Label)e.Item.FindControl("lblBuyerColorId")).Text);
        //            string sql = $"Exec [usp_GetBookingInfoByBookingIdAndColor] {BookingId}, '{buyerColorId}'";
        //            var dtbookingInfo = unitOfWork.GetDataTableFromSql(sql);
        //            //rpt.DataSource = dtbookingInfo;
        //            //rpt.DataBind();
        //            //ViewState[rpt.UniqueID] = dtbookingInfo;

        //            DataTable dt = new DataTable();
        //            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
        //            dt.Columns.Add(new DataColumn("Column0", typeof(string)));
        //            dt.Columns.Add(new DataColumn("Column1", typeof(string)));
        //            dt.Columns.Add(new DataColumn("Column2", typeof(string)));
        //            dt.Columns.Add(new DataColumn("Column3", typeof(string)));
        //            dt.Columns.Add(new DataColumn("Column4", typeof(string)));

        //            for (int i = 0; i < dtbookingInfo.Rows.Count; i++)
        //            {
        //                DataRow dr = null;
        //                dr = dt.NewRow();
        //                dr["RowNumber"] = i + 1;
        //                dr["Column0"] = dtbookingInfo.Rows[i].Field<string>("YarnDescription");
        //                dr["Column1"] = dtbookingInfo.Rows[i].Field<int>("BookingQuantity")+"";
        //                dr["Column2"] = dtbookingInfo.Rows[i].Field<string>("Supplier");
        //                dr["Column3"] = dtbookingInfo.Rows[i].Field<decimal>("ConsumptionPerDzn")+"";
        //                dr["Column4"] = dtbookingInfo.Rows[i].Field<int>("YarnProcurementStatus")+"";
        //                dt.Rows.Add(dr);
        //            }

        //            ViewState[rpt.UniqueID] = dt;

        //            rpt.DataSource = dt;
        //            rpt.DataBind();

        //        }
        //        else
        //        {
        //            SetInitialRowCount(rpt);
        //        }

        //    }
        //}

        //protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        //{

        //}

    }
}