﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class ViewIssuedAccessoriesAtStore : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        int grandTotal = 0;


        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                EnableDisableEditButton();
                EnableDisableDeleteButton();
                CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
                LoadStore();
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteIssuedAccessoriesInfo(IssueId);
                }
            }
        }

        private void LoadStore()
        {
            var dt = unitOfWork.GetDataTableFromSql("SELECT StoreId, StoreName FROM Stores");
            ddlStore.Items.Add(new ListItem("", ""));
            ddlStore.DataSource = dt;
            ddlStore.DataTextField = "StoreName";
            ddlStore.DataValueField = "StoreId";
            ddlStore.DataBind();
            ddlStore.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlStore.SelectedIndex = 0;
        }

        private void DeleteIssuedAccessoriesInfo(int issueId)
        {
            var issueInfo = unitOfWork.GenericRepositories<AccessoriesIssuedAtStore>().GetByID(issueId);
            var lstOldItems = unitOfWork.GenericRepositories<AccessoriesIssuedAtStoreItems>().Get(x => x.IssueId == issueId).ToList();
            foreach (var item in lstOldItems)
            {
                unitOfWork.GenericRepositories<AccessoriesIssuedAtStoreItems>().Delete(item);
            }
            unitOfWork.GenericRepositories<AccessoriesIssuedAtStore>().Delete(issueInfo);
            unitOfWork.Save();
            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
            LoadData();
        }


        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewIssuedAccessoriesAtStore", 2, 1);
        }

        private void EnableDisableEditButton()
        {
            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Knitting - Mgmt,Knitting - Floor");
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewIssuedAccessoriesAtStore", 1, 1);
        }

        protected void btnViewEntries_Click(object sender, EventArgs e)
        {
            try
            {
                LoadData();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void LoadData()
        {

            divSummary.Visible = false;
            divColors.Visible = false;
            rptDetailInfo.DataSource = null;
            rptDetailInfo.DataBind();

            if(string.IsNullOrEmpty(tbxIssuedDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter issue date.')", true);
            }
            else
            {
                var knittingUnit = 0;
                var issueDate = DateTime.Parse(tbxIssuedDate.Text);
                var storeId = 0;
                if(ddlStore.SelectedValue != "")
                {
                    storeId = int.Parse(ddlStore.SelectedValue);
                }

                if (ddlKnittingUnits.SelectedValue != "")
                {
                    knittingUnit = int.Parse(ddlKnittingUnits.SelectedValue);
                }

                //var dtEntries = unitOfWork.GenericRepositories<AccessoriesIssuedAtStore>().Get(x => (x.StoreId == storeId || storeId == 0) && x.IssueDate == issueDate && (x.KnittingUnitId == knittingUnit || knittingUnit == 0)).FirstOrDefault();
                
                var dtEntries = unitOfWork.GetDataTableFromSql($"EXEC usp_AccessoriesIssuedAtStoreByUserAndDate {storeId},{knittingUnit}, '{issueDate}'");

                if (dtEntries.Rows.Count > 0)
                {
                    rptSummary.DataSource = dtEntries;
                    rptSummary.DataBind();
                    rptSummary.Visible = true;
                    lblEntryNotFound.Visible = false;
                    dataDiv.Visible = true;


                   // lblHeaderMessage.Text = "Yarn Issued at: " + ddlKnittingUnits.SelectedItem.Text;

                    lblGrandTotal.Text = "Total Issued Quantity: " + grandTotal.ToString();
                }
                else
                {
                    rptSummary.DataSource = null;
                    rptSummary.DataBind();
                    rptSummary.Visible = false;
                    lblEntryNotFound.Visible = true;
                    dataDiv.Visible = false;
                }
                divSummary.Visible = true;
            }
           
        }


        protected void rptSummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                int issuedQty = int.Parse(DataBinder.Eval(e.Item.DataItem, "IssuedQuantity").ToString());
                grandTotal = grandTotal + issuedQty;
            }
        }



        protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        {
            IssueId = int.Parse(e.CommandArgument.ToString());

            var dtRecColors = unitOfWork.GetDataTableFromSql($"Exec usp_GetAccessoriesIssuedAtUnitDetailsByIssueId {IssueId}");


            if (dtRecColors.Rows.Count > 0)
            {
                lblEntry.Text = "Details of Issued Accessories: Buyer: " + dtRecColors.Rows[0][0].ToString() + ", Style: " + dtRecColors.Rows[0][1].ToString() + ", Quantity: " + dtRecColors.Rows[0][8].ToString();


                divColors.Visible = true;
                rptDetailInfo.DataSource = dtRecColors;
                rptDetailInfo.DataBind();
                rptDetailInfo.Visible = true;
                lblNoDataFound.Visible = false;
            }
            else
            {
                rptDetailInfo.DataSource = null;
                rptDetailInfo.DataBind();
                rptDetailInfo.Visible = false;
                lblNoDataFound.Visible = true;
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);

        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            var issueId = int.Parse(e.CommandArgument.ToString());
            Response.Redirect("IssueAccessoriesAtStore.aspx?accIssueId=" + Tools.UrlEncode(issueId + ""));
        }

        int IssueId
        {
            set { ViewState["issueId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["issueId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }


        protected void rptDetailInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var buyerId = int.Parse(((Label)e.Item.FindControl("lblBuyerId")).Text);
                var styleId = int.Parse(((Label)e.Item.FindControl("lblStyleId")).Text);
            }
        }

        //protected void gvSizeQuantity1_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        var buyerId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId")).Text);
        //        var styleId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId")).Text);
        //        //var dtSizes = buyerManager.GetSizes(buyerId);
        //        var dtSizes = buyerManager.GetStyleSizes(styleId);

        //        Label txtSizeQty = null;
        //        Label lblSizeId = null;

        //        for (int i = 0; i < dtSizes.Rows.Count; i++)
        //        {
        //            txtSizeQty = new Label();
        //            txtSizeQty.ID = "tbxReceivQuantity" + i.ToString();
        //            txtSizeQty.Text = "";
        //            txtSizeQty.Width = 70;
        //            e.Row.Cells[i].Controls.Add(txtSizeQty);


        //            lblSizeId = new Label();
        //            lblSizeId.ID = "lblSizeId" + i.ToString();
        //            lblSizeId.Text = dtSizes.Rows[i][0].ToString();
        //            lblSizeId.Visible = false;
        //            e.Row.Cells[i].Controls.Add(lblSizeId);

        //            var entryId = new Label();
        //            entryId.ID = "lblentryId" + i.ToString();
        //            entryId.Text = "";
        //            entryId.Visible = false;
        //            e.Row.Cells[i].Controls.Add(entryId);


        //            var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());

        //            var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
        //            var buyerColorId = int.Parse(string.IsNullOrEmpty(lblBuyerColorId.Text) ? "0" : lblBuyerColorId.Text);

        //            var lblLotNumber = (Label)((GridView)sender).DataItemContainer.FindControl("lblLotNumber");

        //            if (buyerColorId != 0)
        //            {
        //                var sizeInfo = unitOfWork.GenericRepositories<AccessoriesIssuedAtStoreItems>().Get(x => x.IssueId == IssueId && x.ColorId == buyerColorId && x.LotNumber == lblLotNumber.Text && x.SizeId == sizeId).FirstOrDefault();
        //                if (sizeInfo != null)
        //                {
        //                    txtSizeQty.Text = sizeInfo.IssuedQty + "";
        //                    entryId.Text = sizeInfo.Id + "";
        //                }
        //            }
        //        }
        //    }
        //}

        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            IssueId = int.Parse(e.CommandArgument.ToString());
            if (IssueId != 0)
            {
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);

                //Page.ClientScript.RegisterStartupScript(this.GetType(), "toastr_message", "toastr.success('Are you working?')", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "toastr_message", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);

            }
            else
            {
                var title = "Error";
                var msg = "Sorry, you can not delete it.<br />Something went wrong.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            }
        }
    }
}