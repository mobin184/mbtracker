﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="IssueYarnFromStore.aspx.cs" Inherits="MBTracker.Pages.Yarn_and_Accessories.IssueYarnFromStore" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .span12 {
            margin-top: -1px;
        }

        .table th, .table td {
            text-align: center;
        }
    </style>
    <div class="row-fluid">
        <div class="span6">
            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblMachinePlanning" Text="Issue Yarn from Store:"></asp:Label>
                    </div>
                </div>
                <div class="clearfix"></div>
                <%--            </div>
      </div>
</div> --%>

                <div class="row-fluid">
                    <div class="span12">
                        <div class="widget-body">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <div class="col-md-12 col-sm-12">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                    </div>
                                </div>

                                <div style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 15px">
                                    <div class="pull-right" style="padding-bottom: 10px">
                                        <span style="font-weight: 700;"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field
                                    </div>
                                </div>


                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label">
                                        <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <%--<div class="control-group">
                            <label for="inputOrder" class="control-label">
                                <asp:Label ID="lblOrder" runat="server" Text="Select Order:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlOrders_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlOrders"><span style="font-weight: 700; color: #CC0000">Please select an order.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>    --%>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>


    <div class="row-fluid">
        <div class="span9">
            <div id="dt_example" class="example_alt_pagination">
                <div class="col-md-6" style="padding-left:0px;">
                    <asp:Label ID="lblNoKnittingPlan" runat="server" Text="Knitting was not planned for this style." Visible="false" BackColor="#ffff00"></asp:Label>
                </div>
                <asp:Panel ID="pnlAssignMachinesToOrder" runat="server" Visible="false">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="form-horizontal">
                                <div style="text-align: left; font-size: 12px; line-height: 20px; padding-bottom: 5px">
                                    <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                </div>
                                <div class="control-group">
                                    <%--<label for="inputBuyer" class="control-label"></label>--%>
                                    <div class="controls-row">
                                        <asp:Label ID="Label6" runat="server" Text="Enter Yarn Issue Information:" Font-Bold="true" Font-Underline="true" Font-Size="Larger"></asp:Label>
                                    </div>
                                </div>

                                <div style="text-align: left; font-size: 12px; line-height: 20px; padding-bottom: 5px">
                                    <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                </div>

                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="width: 100px; text-align: left">
                                        <asp:Label ID="Label1" runat="server" Text="Knitting Part:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-left: 110px">
                                        <asp:TextBox ID="tbxKnittingPart" runat="server" Width="250"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="width: 100px; text-align: left">
                                        <asp:Label ID="lblYarnDescription" runat="server" Text="Yarn Description:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-left: 110px">
                                        <asp:TextBox ID="tbxYarnDecription" runat="server" Width="250"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="Label3" runat="server" Font-Bold="true" Style="font-size: 12px" Font-Size="Larger" Text="Issue Yarn To:"></asp:Label></label>
                                <div class="controls controls-row" style="overflow-x: auto">
                                    <asp:Repeater ID="rpt" runat="server" OnItemDataBound="rpt_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>

                                                        <th>
                                                            <asp:Label ID="Label2" runat="server" Text="Unit<br/>Name"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Issued<br/> To Unit"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label4" runat="server" Text="Knitting<br/>Start Date"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label12" runat="server" Text="Yarn Quantity By Size"></asp:Label></th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>

                                                <td style="width: 80px">
                                                    <asp:Label ID="lblUnitName" runat="server" Text='<%#Eval("UnitName") %>'></asp:Label>
                                                    <asp:Label ID="lblUnitId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "UnitId")%>'></asp:Label>
                                                </td>
                                                <td style="width: 120px">
                                                    <asp:Label ID="lblIssuedQty" runat="server" Text='<%#Eval("IssueQuantity")%>'></asp:Label></td>

                                                <td style="width: 80px">
                                                    <asp:Label ID="lblProductionDate" runat="server" Text='<%#String.Format("{0:dd-MMM-yyyy}",Eval("ProdStartDate"))%>'></asp:Label>
                                                    <asp:Label ID="lblMachinAssignedId" runat="server" Visible="false" Text='<%#Eval("MCAssignedId")%>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Repeater ID="rptOrderColors" runat="server" OnItemDataBound="rptOrderColors_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="data-table" class="table table-bordered table-hover">
                                                                <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>'></asp:Label><asp:Label ID="lblOrderColorDescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ColorDescription")%>' Width="100px"></asp:Label></th></td>
                                                                <td>
                                                                    <asp:GridView ID="gvSizeQuantity" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" OnRowCreated="gvSizeQuantity_RowCreated">
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody>
                                                        </table>
                                                        <div class="clearfix">
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                 </table>

                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <br />

                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="control-row">
                        <asp:LinkButton ID="lnkbtnSaveYarnIssued" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveYarnIssued_Click"></asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnUpdateYarnIssued" Visible="false" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateYarnIssued_Click"></asp:LinkButton>
                    </div>
                </asp:Panel>
            </div>
        </div>

        <div class="span3" style="margin-left: 15px; width: 24%">
            <asp:Panel ID="pnlPlanningInfo" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-body" style="overflow-x: auto">
                        <div class="control-group">
                            <div style="width: 100%" id="divTotalOrderQuantity" runat="server" visible="false">
                            </div>
                            <br />
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label5" runat="server" Text="Knitting Information:" Font-Bold="true"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:Repeater ID="rptPlanInfo" runat="server">
                                    <HeaderTemplate>
                                        <table id="data-table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lblOrderQty" runat="server" Text="Order<br/>Qty"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblMachineBrand" runat="server" Text="Machine<br/>Brand"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblMachineGauge" runat="server" Text="Machine<br/>Gauge"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblKnittingTime" runat="server" Text="Knit.<br/>Time"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="text-align: center" width="15%"><%#Eval("OrderQuantity") %> </td>
                                            <td style="text-align: center" width="35%">
                                                <asp:Label ID="lblMCBrandName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BrandName")%>'></asp:Label><asp:Label ID="lblMCBrandId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "MachineBrandId")%>'></asp:Label>
                                                <asp:DropDownList ID="ddlMachineBrands" runat="server" Display="Dynamic" CssClass="form-control" Visible="false"></asp:DropDownList>
                                            </td>
                                            <td style="text-align: center" width="15%">
                                                <asp:Label ID="lblMCGauge" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingMachineGauge")%>'></asp:Label>
                                                <asp:TextBox ID="tbxKnittingMCGauge" runat="server" CssClass="form-control" placeholder="MC Gauge" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingMachineGauge")%>'></asp:TextBox>
                                            </td>
                                            <td style="text-align: center" width="15%">
                                                <asp:Label ID="lblKnittingTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingTime")%>'></asp:Label>
                                                <asp:TextBox ID="tbxKnittingTime" runat="server" placeholder="Knitting time" CssClass="form-control" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingTime")%>'></asp:TextBox>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                            </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Label ID="lblNoPlanningInfo" runat="server" Visible="false" Text="<br/><br/>No planning info found." BackColor="#ffff00"></asp:Label>
                            </div>
                        </div>

                        <br />
                        <asp:Panel ID="pnlShipmentInfo" runat="server" Visible="false">

                            <div class="control-group" style="line-height: 10px">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblShipmentDates" runat="server" Text="Shipment Summary:" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rptShipmentSummary" runat="server" OnItemDataBound="rptShipmentSummary_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblCol1" runat="server" Text="Shipment<br/>Dates"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblCol3" runat="server" Text="Total<br/> Shipment"></asp:Label></th>
                                                        <th class="hidden">
                                                            <asp:Label ID="lblCol4" runat="server" Text="Machines<br/> Required"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center; vertical-align: middle">
                                                    <asp:Label ID="lblShipmentDates" runat="server" Text=' <%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "ShipmentDate"))%>'> </asp:Label></td>
                                                <td style="text-align: center; vertical-align: middle">
                                                    <asp:Label ID="lblShipmentQuantity" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShipmentQuantity")%>'></asp:Label>
                                                </td>
                                                <td class="hidden" style="text-align: center; vertical-align: middle">
                                                    <asp:Label ID="lblMCRequired" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                            </table>
                                            <div class="clearfix">
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <asp:Label ID="lblNoShippingInfo" runat="server" Visible="false" Text="<br/><br/>No shipping info found." BackColor="#ffff00"></asp:Label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>

</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
