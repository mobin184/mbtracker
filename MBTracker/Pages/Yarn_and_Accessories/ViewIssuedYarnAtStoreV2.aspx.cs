﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class ViewIssuedYarnAtStoreV2 : System.Web.UI.Page
    {

        //BuyerManager buyerManager = new BuyerManager();
        //UnitOfWork unitOfWork = new UnitOfWork();
        //int grandTotal = 0;


        //protected void Page_Load(object sender, EventArgs e)
        //{


        //    if (!IsPostBack)
        //    {
        //        EnableDisableEditButton();
        //        EnableDisableDeleteButton();
        //        //CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
        //        LoadProductionUnitsForIssueByUser(ddlKnittingUnits);
        //        CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
        //        LoadStore();
        //    }
        //    else
        //    {
        //        string parameter = Request["__EVENTARGUMENT"];
        //        if (parameter == "confirmDelete")
        //        {
        //            DeleteYarnIssueInfo(YarnIssueId);
        //        }
        //    }
        //}

        //public static void LoadProductionUnitsForIssueByUser(DropDownList ddlUserProductionUnit)
        //{

        //    var dtKnittingUnit = new CommonManager().GetProductionUnitsByUser(CommonMethods.SessionInfo.UserId);
        //    var dr = dtKnittingUnit.NewRow();
        //    var dr1 = dtKnittingUnit.NewRow();
        //    dr["ProductionUnitId"] = 99;
        //    dr["UnitName"] = "Leftover";
        //    dr1["ProductionUnitId"] = 0;
        //    dr1["UnitName"] = "---Select---";
        //    dtKnittingUnit.Rows.Add(dr);
        //    dtKnittingUnit.Rows.InsertAt(dr1,0);
        //    ddlUserProductionUnit.DataTextField = "UnitName";
        //    ddlUserProductionUnit.DataValueField = "ProductionUnitId";
        //    ddlUserProductionUnit.DataSource = dtKnittingUnit;
        //    ddlUserProductionUnit.DataBind();
        //}

        //protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ddlStyles.Items.Clear();
        //    if (ddlBuyers.SelectedValue != "")
        //    {
        //        CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
        //    }
        //}

        //private void LoadStore()
        //{
        //    var dt = unitOfWork.GetDataTableFromSql("SELECT StoreId, StoreName FROM Stores WHERE StoreType = 2");
        //    ddlStore.Items.Add(new ListItem("", ""));
        //    ddlStore.DataSource = dt;
        //    ddlStore.DataTextField = "StoreName";
        //    ddlStore.DataValueField = "StoreId";
        //    ddlStore.DataBind();
        //    ddlStore.Items.Insert(0, new ListItem("---Select---", string.Empty));
        //    ddlStore.SelectedIndex = 0;
        //}

        //private void DeleteYarnIssueInfo(int yarnIssueId)
        //{
        //    var issueInfo = unitOfWork.GenericRepositories<YarnIssuedAtStore>().GetByID(yarnIssueId);
        //    var lstColorSize = unitOfWork.GenericRepositories<YarnIssuedAtStoreColorSize>().Get(x => x.IssueId == yarnIssueId).ToList();
        //    foreach (var item in lstColorSize)
        //    {
        //        unitOfWork.GenericRepositories<YarnIssuedAtStoreColorSize>().Delete(item);
        //    }
        //    unitOfWork.GenericRepositories<YarnIssuedAtStore>().Delete(issueInfo);
        //    unitOfWork.Save();
        //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
        //    LoadData();
        //}


        //private void EnableDisableDeleteButton()
        //{
        //    //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");
        //    ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewIssuedYarnAtStore", 2, 1);
        //}

        //private void EnableDisableEditButton()
        //{
        //    //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Knitting - Mgmt,Knitting - Floor");
        //    ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewIssuedYarnAtStore", 1, 1);
        //}

        //protected void btnViewEntries_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        LoadData();
        //    }
        //    catch (Exception ex)
        //    {
        //        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
        //    }
        //}

        //protected void LoadData()
        //{

        //    divSummary.Visible = false;
        //    divColors.Visible = false;
        //    rptDetailInfo.DataSource = null;
        //    rptDetailInfo.DataBind();

        //    //if(string.IsNullOrEmpty(tbxIssuedDate.Text))
        //    //{
        //    //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter issue date.')", true);
        //    //}
        //    //else
        //    //{
        //    var knittingUnit = 0;
        //    var issueDate = "";
        //    if (!string.IsNullOrEmpty(tbxIssuedDate.Text))
        //    {
        //        issueDate = DateTime.Parse(tbxIssuedDate.Text).ToString("yyyy-MM-dd");
        //    }

        //    var storeId = 0;
        //    if (ddlStore.SelectedValue != "")
        //    {
        //        storeId = int.Parse(ddlStore.SelectedValue);
        //    }

        //    if (ddlKnittingUnits.SelectedValue != "")
        //    {
        //        knittingUnit = int.Parse(ddlKnittingUnits.SelectedValue);
        //    }

        //    var buyerId = 0;
        //    if (ddlBuyers.SelectedValue != "")
        //    {
        //        buyerId = int.Parse(ddlBuyers.SelectedValue);
        //    }

        //    var styleId = 0;
        //    if (ddlStyles.SelectedValue != "")
        //    {
        //        styleId = int.Parse(ddlStyles.SelectedValue);
        //    }

        //    if (string.IsNullOrEmpty(issueDate) && buyerId == 0 && styleId == 0)
        //    {
        //        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter issue date or select buyer with style.')", true);
        //    }
        //    else
        //    {
        //        if (string.IsNullOrEmpty(issueDate) && buyerId != 0 && styleId == 0)
        //        {
        //            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select style.')", true);
        //        }
        //        else
        //        {
        //            //var dtEntries = unitOfWork.GenericRepositories<YarnIssuedAtStore>().Get(x => (x.StoreId == storeId || storeId == 0) && x.IssueDate == issueDate && (x.KnittingUnitId == knittingUnit || knittingUnit == 0)).FirstOrDefault();

        //            var dtEntries = unitOfWork.GetDataTableFromSql($"EXEC usp_YarnIssuedAtStoreByUserAndDate {storeId},{knittingUnit}, '{issueDate}','{buyerId}','{styleId}'");

        //            if (dtEntries.Rows.Count > 0)
        //            {
        //                rptSummary.DataSource = dtEntries;
        //                rptSummary.DataBind();
        //                rptSummary.Visible = true;
        //                lblEntryNotFound.Visible = false;
        //                dataDiv.Visible = true;


        //                // lblHeaderMessage.Text = "Yarn Issued at: " + ddlKnittingUnits.SelectedItem.Text;

        //                lblGrandTotal.Text = "Total Issued Quantity: " + grandTotal.ToString();
        //            }
        //            else
        //            {
        //                rptSummary.DataSource = null;
        //                rptSummary.DataBind();
        //                rptSummary.Visible = false;
        //                lblEntryNotFound.Visible = true;
        //                dataDiv.Visible = false;
        //            }
        //            divSummary.Visible = true;
        //        }
        //    }
        //    //}

        //}


        //protected void rptSummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        int issuedQty = int.Parse(DataBinder.Eval(e.Item.DataItem, "IssuedQuantity").ToString());
        //        grandTotal = grandTotal + issuedQty;
        //    }
        //}



        //protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        //{
        //    YarnIssueId = int.Parse(e.CommandArgument.ToString());

        //    var dtRecColors = unitOfWork.GetDataTableFromSql($"Exec usp_GetYarnIssuedAtUnitDetailsByIssueId {YarnIssueId}");


        //    if (dtRecColors.Rows.Count > 0)
        //    {
        //        lblEntry.Text = "Details of Issued Yarn: Buyer: " + dtRecColors.Rows[0]["BuyerName"].ToString() + ", Style: " + dtRecColors.Rows[0]["StyleName"].ToString() + ", Quantity: " + dtRecColors.Rows[0]["IssuedQuantity"].ToString();


        //        divColors.Visible = true;
        //        rptDetailInfo.DataSource = dtRecColors;
        //        rptDetailInfo.DataBind();
        //        rptDetailInfo.Visible = true;
        //        lblNoDataFound.Visible = false;
        //    }
        //    else
        //    {
        //        rptDetailInfo.DataSource = null;
        //        rptDetailInfo.DataBind();
        //        rptDetailInfo.Visible = false;
        //        lblNoDataFound.Visible = true;
        //    }

        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown()", true);

        //}

        //protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        //{
        //    var yarnIssueId = int.Parse(e.CommandArgument.ToString());
        //    Response.Redirect("IssueYarnAtStoreV2.aspx?yarnIssueId=" + Tools.UrlEncode(yarnIssueId + ""));
        //}

        //int YarnIssueId
        //{
        //    set { ViewState["yarnIssueId"] = value; }
        //    get
        //    {
        //        try
        //        {
        //            return Convert.ToInt32(ViewState["yarnIssueId"]);
        //        }
        //        catch
        //        {
        //            return 0;
        //        }
        //    }

        //}


        //protected void rptDetailInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        var buyerId = int.Parse(((Label)e.Item.FindControl("lblBuyerId")).Text);
        //        var styleId = int.Parse(((Label)e.Item.FindControl("lblStyleId")).Text);
        //        var dtSizes = buyerManager.GetStyleSizes(styleId);

        //        GridView gvSizeQuantity1 = (GridView)e.Item.FindControl("gvSizeQuantity1");
        //        Label lblNoSizeFound = (Label)e.Item.FindControl("lblNoSizeFound");
        //        if (dtSizes.Rows.Count > 0)
        //        {
        //            DataTable dtOneRow = new DataTable();
        //            DataRow dr = null;
        //            dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
        //            dr = dtOneRow.NewRow();
        //            dr["RowNumber"] = 1;
        //            dtOneRow.Rows.Add(dr);

        //            TemplateField templateField = null;

        //            for (int i = 0; i < dtSizes.Rows.Count; i++)
        //            {
        //                templateField = new TemplateField();
        //                templateField.HeaderText = dtSizes.Rows[i][1].ToString();
        //                gvSizeQuantity1.Columns.Add(templateField);
        //            }

        //            gvSizeQuantity1.DataSource = dtOneRow;
        //            gvSizeQuantity1.DataBind();
        //            gvSizeQuantity1.Visible = true;
        //            lblNoSizeFound.Visible = false;

        //        }
        //        else
        //        {
        //            gvSizeQuantity1.Visible = false;
        //            lblNoSizeFound.Visible = true;
        //        }
        //    }
        //}

        //protected void gvSizeQuantity1_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        var buyerId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId")).Text);
        //        var styleId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId")).Text);
        //        //var dtSizes = buyerManager.GetSizes(buyerId);
        //        var dtSizes = buyerManager.GetStyleSizes(styleId);

        //        Label txtSizeQty = null;
        //        Label lblSizeId = null;

        //        for (int i = 0; i < dtSizes.Rows.Count; i++)
        //        {
        //            txtSizeQty = new Label();
        //            txtSizeQty.ID = "tbxReceivQuantity" + i.ToString();
        //            txtSizeQty.Text = "";
        //            txtSizeQty.Width = 70;
        //            e.Row.Cells[i].Controls.Add(txtSizeQty);


        //            lblSizeId = new Label();
        //            lblSizeId.ID = "lblSizeId" + i.ToString();
        //            lblSizeId.Text = dtSizes.Rows[i][0].ToString();
        //            lblSizeId.Visible = false;
        //            e.Row.Cells[i].Controls.Add(lblSizeId);

        //            var entryId = new Label();
        //            entryId.ID = "lblentryId" + i.ToString();
        //            entryId.Text = "";
        //            entryId.Visible = false;
        //            e.Row.Cells[i].Controls.Add(entryId);


        //            var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());

        //            var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
        //            var buyerColorId = int.Parse(string.IsNullOrEmpty(lblBuyerColorId.Text) ? "0" : lblBuyerColorId.Text);

        //            var lblLotNumber = (Label)((GridView)sender).DataItemContainer.FindControl("lblLotNumber");

        //            if (buyerColorId != 0)
        //            {
        //                var sizeInfo = unitOfWork.GenericRepositories<YarnIssuedAtStoreColorSize>().Get(x => x.IssueId == YarnIssueId && x.ColorId == buyerColorId && x.LotNumber == lblLotNumber.Text && x.SizeId == sizeId).FirstOrDefault();
        //                if (sizeInfo != null)
        //                {
        //                    txtSizeQty.Text = sizeInfo.IssuedQty + "";
        //                    entryId.Text = sizeInfo.Id + "";
        //                }
        //            }
        //        }
        //    }
        //}

        //protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        //{
        //    YarnIssueId = int.Parse(e.CommandArgument.ToString());
        //    if (YarnIssueId != 0)
        //    {
        //        var title = "Warning";
        //        var msg = "Are you sure you want to delete?";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);

        //        //Page.ClientScript.RegisterStartupScript(this.GetType(), "toastr_message", "toastr.success('Are you working?')", true);
        //        //Page.ClientScript.RegisterStartupScript(this.GetType(), "toastr_message", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);

        //    }
        //    else
        //    {
        //        var title = "Error";
        //        var msg = "Sorry, you can not delete it.<br />Something went wrong.";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
        //    }
        //}
    }
}