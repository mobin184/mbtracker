﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ReceiveYarnAtStore.aspx.cs" Inherits="MBTracker.Pages.Yarn_and_Accessories.ReceiveYarnAtStore" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        input[type="radio"], input[type="checkbox"] {
            margin: -3px 0 0;
        }

        label {
            display: inline;
            padding-left: 3px !important;
        }

        td, th {
            padding: 5px;
        }

        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 7px;
        }

        .table {
            width: 100%;
            margin-bottom: 5px;
        }
    </style>
    <div class="row-fluid">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="Receive Yarn at Store:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 10px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputStyle" class="control-label">
                                <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <div class="span9" style="margin-left: 0px">
            <asp:Label ID="lblNoColorFound" runat="server" Text="This style does not have any colors!" Visible="false" BackColor="#ffff00"></asp:Label>
            <div class="widget" runat="server" id="pnlYarnReceive" visible="false" style="border:0px;">
                <div class="widget-body" style="overflow-x: auto">
                    <asp:Panel runat="server">
                        <div class="control-group">
                            <label class="control-label" style="text-align: left">
                                <asp:Label ID="lbl1" Font-Bold="true" runat="server" Text="Yarn Receive Info:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                        </div>
                        <div class=" controls-row">
                            <div id="dt_example" class="example_alt_pagination">
                                <asp:Repeater ID="rpt" Visible="false" OnItemDataBound="rpt_ItemDataBound" runat="server">
                                    <HeaderTemplate>
                                        <table id="data-table1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="width:475px">
                                                        <asp:Label ID="lblColorInof" runat="server" Text=""></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblBookingInfo" runat="server" Text="Receive Info"></asp:Label>
                                                    </th>
                                                    <th style="width: 50px">
                                                        <asp:Label ID="lblDeleteRow" runat="server" Text="Action"></asp:Label>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td >
                                                <asp:Label ID="lblStyleId" runat="server" Visible="false" Text='<%#Eval("StyleId") %>'></asp:Label>
                                                <div class="form-horizontal">
                                                    <div class="control-group">
                                                        <label for="inputBuyer" class="control-label">
                                                            <asp:Label ID="lbl22" runat="server" Text="Yarn Composition:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                        <div class="controls controls-row">
                                                            <asp:TextBox ID="tbxYarnComposition" Width="250" Text="" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label for="inputBuyer" class="control-label">
                                                            <asp:Label ID="Label1" runat="server" Text="LC Number:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                                        <div class="controls controls-row">
                                                            <asp:TextBox ID="tbxLCNumber" Width="250" Text="" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <asp:Repeater ID="rptColors" runat="server" OnItemDataBound="rptColors_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="data-table2" class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th style="">
                                                                        <asp:Label ID="lbl11" runat="server" Text="Color"></asp:Label>
                                                                    </th>
                                                                    <th style="">
                                                                        <asp:Label ID="lbl12" runat="server" Text="Received Qty"></asp:Label>
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblColorName" runat="server" Text='<%#Eval("ColorDescription") %>'></asp:Label>
                                                                <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%#Eval("BuyerColorId") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbxReceivedQty" CssClass="dummyClass" TextMode="Number" Height="30" Width="80" runat="server"></asp:TextBox>
                                                            </td>

                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                                </table>
                                                  
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Container.ItemIndex %>' class="btn btn-danger btn-mini pull-right " OnCommand="lnkbtnDelete_Command" Text="Delete Row"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                        </table> 
                                        <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-mini pull-right btnStyle" Style="margin-right: 8px" Text="&nbsp;&nbsp;Add Row&nbsp;&nbsp;" OnClick="btnAddRow_Click" />
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </asp:Panel>
                </div>
                <div class="control-group">
                    <div class="controls-row" style="padding-top: 40px;">
                        <asp:Button ID="btnSave" runat="server" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSave_Click" />
                        <asp:Button ID="btnUpdate" Visible="false" runat="server" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdate_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
