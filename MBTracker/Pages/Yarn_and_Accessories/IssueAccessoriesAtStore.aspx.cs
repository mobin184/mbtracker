﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class IssueAccessoriesAtStore : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        UnitOfWork unitOfWork = new UnitOfWork();


        DataTable dtIssueDetails;
        int initialLoadForUpdate = 0;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var user = CommonMethods.SessionInfo;
                if (user == null)
                {
                    HttpContext.Current.Session["returnUrl"] = HttpContext.Current.Request.Url.PathAndQuery;
                    Response.Redirect("~/Login.aspx");
                }

                string url = Request.Url.AbsolutePath;
                var task = unitOfWork.GenericRepositories<TaskItems>().Get(x => x.URL == url).FirstOrDefault();

                if (task != null && user != null)
                {
                    var permission = unitOfWork.GenericRepositories<TaskRolePermissions>().Get(x => x.TaskId == task.Id && x.RoleId == user.RoleId).ToList();

                    if (!permission.Any())
                    {
                        Response.Redirect("~/Pages/AccessDenied.aspx", false);
                    }
                }

                if (Request.QueryString["accIssueId"] != null)
                {
                    IssueId = int.Parse(Tools.UrlDecode(Request.QueryString["accIssueId"]));
                    initialLoadForUpdate = IssueId;
                    lblTitle.Text = "Update Issued Accessories at Store:";

                    //lblMessage.Text = "Received Spare Information:";

                    btnSaveEntries.Visible = false;
                    btnUpdateEntries.Visible = true;

                    PopulateEntryInfo();
                }
                else
                {
                    CommonMethods.LoadProductionUnitsByUser(ddlUserProductionUnit);
                    CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                    LoadStore();
                    tbxIssueDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                }


            }
        }

        private void LoadStore()
        {
            var dt = unitOfWork.GetDataTableFromSql("SELECT StoreId, StoreName FROM Stores");
            ddlStore.Items.Add(new ListItem("", ""));
            ddlStore.DataSource = dt;
            ddlStore.DataTextField = "StoreName";
            ddlStore.DataValueField = "StoreId";
            ddlStore.DataBind();
            ddlStore.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlStore.SelectedIndex = 0;
        }

        private void PopulateEntryInfo()
        {
            if (IssueId > 0)
            {
                LoadIssuedAccessories();
            }

        }

        private void LoadIssuedAccessories()
        {
            //string sql = $"Exec usp_GetYarnReceivedAtUnitById '{YarnIssueId}'";
            //var dtYarnReceive = unitOfWork.GetDataTableFromSql(sql);
            var yarnIssued = unitOfWork.GenericRepositories<AccessoriesIssuedAtStore>().GetByID(IssueId);

            tbxIssueDate.Text = yarnIssued.IssueDate.ToString("yyyy-MM-dd");

            LoadStore();
            ddlStore.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStore, yarnIssued.StoreId);

            CommonMethods.LoadProductionUnitsByUser(ddlUserProductionUnit);
            ddlUserProductionUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUserProductionUnit, yarnIssued.KnittingUnitId);

            CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            //int buyerId = int.Parse((string.IsNullOrEmpty(dtYarnReceive.Rows[0]["BuyerId"].ToString()) ? "0" : dtYarnReceive.Rows[0]["BuyerId"].ToString()));
            ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, yarnIssued.BuyerId);
            ddlBuyers.Enabled = false;

            CommonMethods.LoadDropdownById(ddlStyles, yarnIssued.BuyerId, "BuyerStyles", 1, 0);
            //int styleId = int.Parse((string.IsNullOrEmpty(dtYarnReceive.Rows[0]["StyleId"].ToString()) ? "0" : dtYarnReceive.Rows[0]["StyleId"].ToString()));
            ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, yarnIssued.StyleId);
            ddlStyles.Enabled = false;


            string sql2 = $"Exec usp_GetAccessoriesIssuedAtStoreDetailsByIssueId '{IssueId}'";
            dtIssueDetails = unitOfWork.GetDataTableFromSql(sql2);


            if (dtIssueDetails.Rows.Count > 0)
            {
                rptEntryInfo.DataSource = dtIssueDetails;
                rptEntryInfo.DataBind();

            }

            ShowColorAndStyleTotal();

            CreateDataTableForViewState();

            divColors.Visible = true;
            rptEntryInfo.Visible = true;

        }

        private void CreateDataTableForViewState()
        {
            DataTable dtCurrentTable = new DataTable();

            DataRow dr = null;

            dtCurrentTable.Columns.Add(new DataColumn("ItemId", typeof(string)));
            dtCurrentTable.Columns.Add(new DataColumn("Lot", typeof(string)));
            dtCurrentTable.Columns.Add(new DataColumn("Qty", typeof(string)));
            //dtCurrentTable.Columns.Add(new DataColumn("ColorTotal", typeof(string)));


            //List<DataTable> sizeAndQtyTableList = new List<DataTable>();

            for (int rowIndex = 0; rowIndex < dtIssueDetails.Rows.Count; rowIndex++)
            {


                DropDownList ddlItems = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlItems");
                TextBox tbxLotNumber = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxLotNumber");
                TextBox tbxIssueQuantity = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxIssueQuantity");
                //Label lblColorTotalValue = (Label)this.rptEntryInfo.Items[rowIndex].FindControl("lblColorTotalValue");

                //GridView gvSizeQuantity1 = (GridView)this.rptEntryInfo.Items[rowIndex].FindControl("gvSizeQuantity1");

                dr = dtCurrentTable.NewRow();
                dr["ItemId"] = (ddlItems.SelectedValue == "" ? "0" : ddlItems.SelectedValue);
                dr["Lot"] = tbxLotNumber.Text;
                dr["Qty"] = tbxIssueQuantity.Text;


                DataTable sizeAndQtyTable = new DataTable();
                sizeAndQtyTable.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                sizeAndQtyTable.Columns.Add(new DataColumn("Column0", typeof(string)));

                //for (int j = 0; j < gvSizeQuantity1.Columns.Count; j++)
                //{
                //    DataRow sizeAndQtyRow = null;
                //    sizeAndQtyRow = sizeAndQtyTable.NewRow();
                //    sizeAndQtyRow["RowNumber"] = j + 1;
                //    sizeAndQtyRow["Column0"] = string.Empty;
                //    sizeAndQtyTable.Rows.Add(sizeAndQtyRow);
                //    string controlId = string.Concat("tbxReceivQuantity", j.ToString());
                //    TextBox tbxSizeQty = (TextBox)gvSizeQuantity1.Rows[0].Cells[j].FindControl(controlId);
                //    sizeAndQtyTable.Rows[j]["Column0"] = tbxSizeQty.Text;
                //}
                //sizeAndQtyTableList.Add(sizeAndQtyTable);
                dtCurrentTable.Rows.InsertAt(dr, dtCurrentTable.Rows.Count);
            }

            this.ViewState["CurrentTable"] = dtCurrentTable;
            //this.ViewState["SizeAndQtyTableList"] = sizeAndQtyTableList;

        }


        int IssueId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["accIssueId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["accIssueId"] = value; }
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblStyleTotal.Text = "";

            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
            }
            divColors.Visible = false;
            rptEntryInfo.Visible = false;
            rptEntryInfo.DataSource = null;
            rptEntryInfo.DataBind();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "CacheItems()", true);
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblStyleTotal.Text = "";
            divColors.Visible = false;
            rptEntryInfo.Visible = false;
            rptEntryInfo.DataSource = null;
            rptEntryInfo.DataBind();

            if (ddlStyles.SelectedValue != "")
            {
                SetInitialRowCount();
                divColors.Visible = true;
                rptEntryInfo.Visible = true;
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "CacheItems()", true);
        }


        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;


            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));
            dt.Columns.Add(new DataColumn("Lot", typeof(string)));
            dt.Columns.Add(new DataColumn("Qty", typeof(string)));


            dr = dt.NewRow();


            dr["ItemId"] = string.Empty;
            dr["Lot"] = string.Empty;
            dr["Qty"] = string.Empty;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptEntryInfo.DataSource = dt;
            rptEntryInfo.DataBind();
        }



        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var styleId = int.Parse(ddlStyles.SelectedValue);
                //var dtSizes = buyerManager.GetStyleSizes(styleId);


                if (initialLoadForUpdate > 0)
                {

                    var ddlItems = (DropDownList)e.Item.FindControl("ddlItems");
                    var tbxLotNumber = (TextBox)e.Item.FindControl("tbxLotNumber");
                    var tbxIssueQuantity = (TextBox)e.Item.FindControl("tbxIssueQuantity");

                    var itemId = int.Parse(DataBinder.Eval(e.Item.DataItem, "ItemId").ToString());
                    var lotNumber = DataBinder.Eval(e.Item.DataItem, "LotNumber").ToString();
                    var issuedQty = DataBinder.Eval(e.Item.DataItem, "IssuedQty").ToString();

                    //Load Item Dropdown
                    this.LoadItemDropdown(styleId, ddlItems);
                    ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, itemId);

                    tbxLotNumber.Text = lotNumber.ToString();
                    tbxIssueQuantity.Text = issuedQty.ToString();
                }
                else
                {

                    var ddlItems = (DropDownList)e.Item.FindControl("ddlItems");
                    //Load Item Dropdown
                    this.LoadItemDropdown(styleId, ddlItems);
                }

            }
        }


        private void LoadItemDropdown(int styleId, DropDownList ddl)
        {
            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetAccessoriesItemsByStyleId {styleId}");

            var dr = dt.NewRow();
            dr["ItemId"] = "";
            dr["ItemName"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);
            ddl.DataSource = dt;
            ddl.DataTextField = "ItemName";
            ddl.DataValueField = "ItemId";
            ddl.DataBind();
        }


        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            this.AddNewRowToGrid();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "CacheItems()", true);
        }

        private void AddNewRowToGrid()
        {


            List<DataTable> sizeAndQtyTableList = new List<DataTable>();


            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];

                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {

                        DropDownList ddlItems = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlItems");

                        TextBox tbxLotNumber = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxLotNumber");
                        TextBox tbxIssueQuantity = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxIssueQuantity");

                        dtCurrentTable.Rows[rowIndex]["ItemId"] = (ddlItems.SelectedValue == "" ? "0" : ddlItems.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["Lot"] = tbxLotNumber.Text;
                        dtCurrentTable.Rows[rowIndex]["Qty"] = tbxIssueQuantity.Text;


                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["ItemId"] = string.Empty;
                            drCurrentRow["Lot"] = string.Empty;
                            drCurrentRow["Qty"] = string.Empty;
                        }
                    }

                    dtCurrentTable.Rows.InsertAt(drCurrentRow, dtCurrentTable.Rows.Count);

                    this.ViewState["CurrentTable"] = dtCurrentTable;
                    this.rptEntryInfo.DataSource = dtCurrentTable;
                    this.rptEntryInfo.DataBind();
                }
            }
            this.SetPreviousData();
            ShowColorAndStyleTotal();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        DropDownList ddlItems = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlItems");
                        TextBox tbxLotNumber = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxLotNumber");
                        TextBox tbxIssueQuantity = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxIssueQuantity");

                        ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ItemId"].ToString()) ? "0" : dt.Rows[i]["ItemId"].ToString())));

                        tbxLotNumber.Text = dt.Rows[i]["Lot"].ToString();
                        tbxIssueQuantity.Text = dt.Rows[i]["Qty"].ToString();

                        rowIndex++;
                    }
                }
            }
        }


        protected void btnSaveEntries_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(tbxIssueDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter issue date.')", true);
            }
            else if (ddlStore.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a store.')", true);
            }
            else if (ddlUserProductionUnit.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a knitting unit.')", true);
            }
            else if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select an style.')", true);
            }            
            else
            {
                try
                {
                    var issueDate = DateTime.Parse(tbxIssueDate.Text);
                    //int gg = 0;
                    int styleTotal = CalculateStyleTotal();
                    if (styleTotal > 0)
                    {
                        btnSaveEntries.Enabled = false;

                        if (issueDate <= DateTime.Now)
                        {

                            List<AccessoriesIssuedAtStoreItems> lstItems = new List<AccessoriesIssuedAtStoreItems>();
                            var yias = new AccessoriesIssuedAtStore()
                            {
                                KnittingUnitId = int.Parse(ddlUserProductionUnit.SelectedValue),
                                IssueDate = issueDate,
                                StoreId = int.Parse(ddlStore.SelectedValue),
                                BuyerId = int.Parse(ddlBuyers.SelectedValue),
                                StyleId = int.Parse(ddlStyles.SelectedValue),
                                IssuedQuantity = styleTotal,
                                CreateDate = DateTime.Now,
                                CreatedBy = CommonMethods.SessionInfo.UserName
                            };


                            for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                            {

                                var ddlItems = (DropDownList)rptEntryInfo.Items[i].FindControl("ddlItems");
                                if (ddlItems.SelectedValue != "")
                                {
                                    var itemId = int.Parse(ddlItems.SelectedValue);

                                    var lotNumber = ((TextBox)rptEntryInfo.Items[i].FindControl("tbxLotNumber")).Text;
                                    var issueQty = ((TextBox)rptEntryInfo.Items[i].FindControl("tbxIssueQuantity")).Text;

                                    var colorSize = new AccessoriesIssuedAtStoreItems()
                                    {
                                        IssueId = yias.IssueId,
                                        ItemId = itemId,
                                        LotNumber = lotNumber,
                                        IssuedQty = int.Parse(issueQty),
                                        CreateDate = DateTime.Now,
                                        CreatedBy = CommonMethods.SessionInfo.UserName
                                    };
                                    lstItems.Add(colorSize);
                                }
                            }

                            yias.AccessoriesIssuedAtStoreItems = lstItems;
                            unitOfWork.GenericRepositories<AccessoriesIssuedAtStore>().Insert(yias);
                            unitOfWork.Save();

                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                            //Response.AddHeader("REFRESH", "2;URL=IssueYarnAtStoreV2.aspx");
                            ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('IssueAccessoriesAtStore.aspx');", true);

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cannot issue in a future date')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('No data was entered.')", true);
                    }

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }

        }

        protected void btnUpdateEntries_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(tbxIssueDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter issue date.')", true);
            }
            else if (ddlStore.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a store.')", true);
            }
            else if (ddlUserProductionUnit.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a knitting unit.')", true);
            }
            else if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select an style.')", true);
            }
            else
            {
                try
                {
                    var issueDate = DateTime.Parse(tbxIssueDate.Text);
                    int styleTotal = CalculateStyleTotal();
                    if (styleTotal > 0)
                    {
                        btnUpdateEntries.Enabled = false;
                        if (issueDate <= DateTime.Now)
                        {
                            var yias = unitOfWork.GenericRepositories<AccessoriesIssuedAtStore>().GetByID(IssueId);
                            List<AccessoriesIssuedAtStoreItems> lstItems = new List<AccessoriesIssuedAtStoreItems>();
                            var oldItems = unitOfWork.GenericRepositories<AccessoriesIssuedAtStoreItems>().Get(x => x.IssueId == IssueId).ToList();
                            foreach (var item in oldItems)
                            {
                                unitOfWork.GenericRepositories<AccessoriesIssuedAtStoreItems>().Delete(item);
                            }

                            yias.KnittingUnitId = int.Parse(ddlUserProductionUnit.SelectedValue);
                            yias.StoreId = int.Parse(ddlStore.SelectedValue);
                            yias.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                            yias.StyleId = int.Parse(ddlStyles.SelectedValue);
                            yias.IssuedQuantity = styleTotal;
                            yias.UpdateDate = DateTime.Now;
                            yias.UpdatedBy = CommonMethods.SessionInfo.UserName;


                            for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                            {

                                var ddlItems = (DropDownList)rptEntryInfo.Items[i].FindControl("ddlItems");
                                if (ddlItems.SelectedValue != "")
                                {
                                    var itemId = int.Parse(ddlItems.SelectedValue);

                                    var lotNumber = ((TextBox)rptEntryInfo.Items[i].FindControl("tbxLotNumber")).Text;
                                    var issueQty = ((TextBox)rptEntryInfo.Items[i].FindControl("tbxIssueQuantity")).Text;

                                    var item = new AccessoriesIssuedAtStoreItems()
                                    {
                                        IssueId = yias.IssueId,
                                        ItemId = itemId,
                                        LotNumber = lotNumber,
                                        IssuedQty = int.Parse(issueQty),
                                        CreateDate = DateTime.Now,
                                        CreatedBy = CommonMethods.SessionInfo.UserName
                                    };
                                    lstItems.Add(item);
                                }
                            }
                            yias.AccessoriesIssuedAtStoreItems = lstItems;

                            unitOfWork.GenericRepositories<AccessoriesIssuedAtStore>().Update(yias);
                            unitOfWork.Save();

                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                            //Response.AddHeader("REFRESH", "2;URL=ViewIssuedYarnAtStore.aspx");
                            ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewIssuedAccessoriesAtStore.aspx');", true);

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cannot issue in future date')", true);
                        }

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter item quantity!')", true);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }

            }

        }

        protected void lnkbtnCalculateColorTotal_Click(object sender, EventArgs e)
        {
            ShowColorAndStyleTotal();
        }

        private void ShowColorAndStyleTotal()
        {
            int styleTotal = 0;

            for (int i = 0; i < this.rptEntryInfo.Items.Count; i++)
            {

                TextBox tbxIssueQuantity = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxIssueQuantity");
                if(tbxIssueQuantity.Text != "")
                {
                    styleTotal += int.Parse(tbxIssueQuantity.Text);
                }                
            }

            lblStyleTotal.Text = "Style Total: " + styleTotal.ToString();
        }


        private int CalculateStyleTotal()
        {
            int styleTotal = 0;
            for (int i = 0; i < this.rptEntryInfo.Items.Count; i++)
            {
                TextBox tbxIssueQuantity = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxIssueQuantity");
                if (tbxIssueQuantity.Text != "")
                {
                    styleTotal += int.Parse(tbxIssueQuantity.Text);
                }
            }

            lblStyleTotal.Text = "Style Total: " + styleTotal.ToString();
            return styleTotal;
        }


    }
}