﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web;


namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class ReceiveYarnAtUnit : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        UnitOfWork unitOfWork = new UnitOfWork();


        DataTable dtReceivedYarnAtUnitDetails;
        int initialLoadForUpdate = 0;
     

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var user = CommonMethods.SessionInfo;
                if (user == null)
                {
                    HttpContext.Current.Session["returnUrl"] = HttpContext.Current.Request.Url.PathAndQuery;
                    Response.Redirect("~/Login.aspx");
                }

                string url = Request.Url.AbsolutePath;
                var task = unitOfWork.GenericRepositories<TaskItems>().Get(x => x.URL == url).FirstOrDefault();

                if (task != null && user != null)
                {
                    var permission = unitOfWork.GenericRepositories<TaskRolePermissions>().Get(x => x.TaskId == task.Id && x.RoleId == user.RoleId).ToList();

                    if (!permission.Any())
                    {
                        Response.Redirect("~/Pages/AccessDenied.aspx", false);
                    }
                }

                if (Request.QueryString["yarnReceiveId"] != null)
                {
                    YarnReceiveId = int.Parse(Tools.UrlDecode(Request.QueryString["yarnReceiveId"]));
                    initialLoadForUpdate = YarnReceiveId;
                    lblTitle.Text = "Update Yarn Receive at Knitting Unit:";

                    //lblMessage.Text = "Received Spare Information:";

                    btnSaveEntries.Visible = false;
                    btnUpdateEntries.Visible = true;

                    PopulateEntryInfo();
                }
                else
                {
                    CommonMethods.LoadProductionUnitsByUser(ddlUserProductionUnit);
                    CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                    tbxReceiveDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                }


            }
        }


        private void PopulateEntryInfo()
        {
            if (YarnReceiveId > 0)
            {
                LoadReceivedYarn();
            }
           
        }

        private void LoadReceivedYarn()
        {
            string sql = $"Exec usp_GetYarnReceivedAtUnitById '{YarnReceiveId}'";
            var dtYarnReceive = unitOfWork.GetDataTableFromSql(sql);

            tbxReceiveDate.Text = DateTime.Parse(dtYarnReceive.Rows[0]["ReceivedDate"].ToString()).ToString("yyyy-MM-dd");

            CommonMethods.LoadProductionUnitsByUser(ddlUserProductionUnit);
            ddlUserProductionUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUserProductionUnit, int.Parse((string.IsNullOrEmpty(dtYarnReceive.Rows[0]["KnittingUnitId"].ToString()) ? "0" : dtYarnReceive.Rows[0]["KnittingUnitId"].ToString())));

            CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            int buyerId = int.Parse((string.IsNullOrEmpty(dtYarnReceive.Rows[0]["BuyerId"].ToString()) ? "0" : dtYarnReceive.Rows[0]["BuyerId"].ToString()));
            ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, buyerId);
            ddlBuyers.Enabled = false;

            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
            int styleId = int.Parse((string.IsNullOrEmpty(dtYarnReceive.Rows[0]["StyleId"].ToString()) ? "0" : dtYarnReceive.Rows[0]["StyleId"].ToString()));
            ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleId);
            ddlStyles.Enabled = false;


            tbxKnittingPart.Text = dtYarnReceive.Rows[0]["KnittingPart"].ToString();
            tbxYarnDescription.Text = dtYarnReceive.Rows[0]["YarnDescription"].ToString();
            tbxGG.Text = dtYarnReceive.Rows[0]["GG"].ToString();


            string sql2 = $"Exec usp_GetYarnReceivedAtUnitDetailsByReceiveId '{YarnReceiveId}'";
            dtReceivedYarnAtUnitDetails = unitOfWork.GetDataTableFromSql(sql2);


            if (dtReceivedYarnAtUnitDetails.Rows.Count > 0)
            {
                rptEntryInfo.DataSource = dtReceivedYarnAtUnitDetails;
                rptEntryInfo.DataBind();

            }

            ShowColorAndStyleTotal();

            CreateDataTableForViewState();

            divColors.Visible = true;
            rptEntryInfo.Visible = true;

        }

        private void CreateDataTableForViewState()
        {
            DataTable dtCurrentTable = new DataTable();

            DataRow dr = null;

            dtCurrentTable.Columns.Add(new DataColumn("BuyerColorId", typeof(string)));
            dtCurrentTable.Columns.Add(new DataColumn("Lot", typeof(string)));
            dtCurrentTable.Columns.Add(new DataColumn("SizeAndQty", typeof(string)));
            dtCurrentTable.Columns.Add(new DataColumn("ColorTotal", typeof(string)));


            List<DataTable> sizeAndQtyTableList = new List<DataTable>();

            for (int rowIndex = 0; rowIndex < dtReceivedYarnAtUnitDetails.Rows.Count; rowIndex++)
            {
               

                DropDownList ddlColors = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlColors");
                TextBox tbxLotNumber = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxLotNumber");
                Label lblColorTotalValue = (Label)this.rptEntryInfo.Items[rowIndex].FindControl("lblColorTotalValue");

                GridView gvSizeQuantity1 = (GridView)this.rptEntryInfo.Items[rowIndex].FindControl("gvSizeQuantity1");

                dr = dtCurrentTable.NewRow();
                dr["BuyerColorId"] = (ddlColors.SelectedValue == "" ? "0" : ddlColors.SelectedValue);
                dr["Lot"] = tbxLotNumber.Text;
                dr["ColorTotal"] = lblColorTotalValue.Text;


                DataTable sizeAndQtyTable = new DataTable();
                sizeAndQtyTable.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                sizeAndQtyTable.Columns.Add(new DataColumn("Column0", typeof(string)));

                for (int j = 0; j < gvSizeQuantity1.Columns.Count; j++)
                {
                    DataRow sizeAndQtyRow = null;
                    sizeAndQtyRow = sizeAndQtyTable.NewRow();
                    sizeAndQtyRow["RowNumber"] = j + 1;
                    sizeAndQtyRow["Column0"] = string.Empty;
                    sizeAndQtyTable.Rows.Add(sizeAndQtyRow);
                    string controlId = string.Concat("tbxReceivQuantity", j.ToString());
                    TextBox tbxSizeQty = (TextBox)gvSizeQuantity1.Rows[0].Cells[j].FindControl(controlId);
                    sizeAndQtyTable.Rows[j]["Column0"] = tbxSizeQty.Text;
                }
                sizeAndQtyTableList.Add(sizeAndQtyTable);
                dtCurrentTable.Rows.InsertAt(dr, dtCurrentTable.Rows.Count);
            }

            this.ViewState["CurrentTable"] = dtCurrentTable;
            this.ViewState["SizeAndQtyTableList"] = sizeAndQtyTableList;

        }


        int YarnReceiveId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["yarnReceiveId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["yarnReceiveId"] = value; }
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblStyleTotal.Text = "";

            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
               // CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
                CommonMethods.LoadOnlyRunningStyleDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
            }
            divColors.Visible = false;
            rptEntryInfo.Visible = false;
            rptEntryInfo.DataSource = null;
            rptEntryInfo.DataBind();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "CacheItems()", true);
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblStyleTotal.Text = "";
            divColors.Visible = false;
            rptEntryInfo.Visible = false;
            rptEntryInfo.DataSource = null;
            rptEntryInfo.DataBind();

            if (ddlStyles.SelectedValue != "")
            {
                SetInitialRowCount();
                divColors.Visible = true;
                rptEntryInfo.Visible = true;
            }

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "CacheItems()", true);
        }


        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;


            dt.Columns.Add(new DataColumn("BuyerColorId", typeof(string)));
            dt.Columns.Add(new DataColumn("Lot", typeof(string)));
            dt.Columns.Add(new DataColumn("SizeAndQty", typeof(string)));
            dt.Columns.Add(new DataColumn("ColorTotal", typeof(string)));


            dr = dt.NewRow();


            dr["BuyerColorId"] = string.Empty;
            dr["Lot"] = string.Empty;
            dr["SizeAndQty"] = string.Empty;
            dr["ColorTotal"] = string.Empty;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptEntryInfo.DataSource = dt;
            rptEntryInfo.DataBind();
        }



        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var styleId = int.Parse(ddlStyles.SelectedValue);
                var dtSizes = buyerManager.GetStyleSizes(styleId);


                if (initialLoadForUpdate > 0)
                {

                    var ddlColors = (DropDownList)e.Item.FindControl("ddlColors");
                    var tbxLotNumber = (TextBox)e.Item.FindControl("tbxLotNumber");

                    var colorId = int.Parse(DataBinder.Eval(e.Item.DataItem, "BuyerColorId").ToString());
                    var lotNumber = DataBinder.Eval(e.Item.DataItem, "LotNumber").ToString();

                    this.LoadColorDropdown(styleId, ddlColors);
                    ddlColors.SelectedIndex = CommonMethods.MatchDropDownItem(ddlColors, colorId);

                    tbxLotNumber.Text = lotNumber.ToString();

                }
                else
                {

                    var ddlColors = (DropDownList)e.Item.FindControl("ddlColors");
                    this.LoadColorDropdown(styleId, ddlColors);
                   
                }



                GridView gvSizeQuantity1 = (GridView)e.Item.FindControl("gvSizeQuantity1");
                Label lblNoSizeFound = (Label)e.Item.FindControl("lblNoSizeFound");


                if (dtSizes.Rows.Count > 0)
                {
                    DataTable dtOneRow = new DataTable();
                    DataRow dr = null;
                    dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                    dr = dtOneRow.NewRow();
                    dr["RowNumber"] = 1;
                    dtOneRow.Rows.Add(dr);

                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantity1.Columns.Add(templateField);
                    }

                    gvSizeQuantity1.DataSource = dtOneRow;
                    gvSizeQuantity1.DataBind();
                    gvSizeQuantity1.Visible = true;
                    lblNoSizeFound.Visible = false;

                }
                else
                {
                    gvSizeQuantity1.Visible = false;
                    lblNoSizeFound.Visible = true;
                }

            }
        }


        private void LoadColorDropdown(int styleId, DropDownList ddl)
        {
            var dt = this.orderManager.GetColorsByStyleId(styleId);

            var dr = dt.NewRow();
            dr["BuyerColorId"] = "";
            dr["ColorDescription"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);
            ddl.DataSource = dt;
            ddl.DataTextField = "ColorDescription";
            ddl.DataValueField = "BuyerColorId";
            ddl.DataBind();
        }


        protected void gvSizeQuantity1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //var buyerId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId")).Text);
                //var styleId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId")).Text);

                var dtSizes = buyerManager.GetStyleSizes(int.Parse(ddlStyles.SelectedValue));

                TextBox txtSizeQty = null;
                Label lblSizeId = null;

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new TextBox();
                    txtSizeQty.ID = "tbxReceivQuantity" + i.ToString();
                    txtSizeQty.Text = "";
                    txtSizeQty.Width = 60;
                    txtSizeQty.TextMode = TextBoxMode.Number;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);


                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);

                    var entryId = new Label();
                    entryId.ID = "lblentryId" + i.ToString();
                    entryId.Text = "";
                    entryId.Visible = false;
                    e.Row.Cells[i].Controls.Add(entryId);

                    var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());
                  
                    if (YarnReceiveId != 0)
                    {
                        var ddlColors = (DropDownList)((GridView)sender).DataItemContainer.FindControl("ddlColors");
                        var buyerColorId = int.Parse(string.IsNullOrEmpty(ddlColors.SelectedValue) ? "0" : ddlColors.SelectedValue);

                        var tbxLotNumber = (TextBox)((GridView)sender).DataItemContainer.FindControl("tbxLotNumber");
                        var lotNumber = tbxLotNumber.Text;

                        var sizeInfo = unitOfWork.GenericRepositories<YarnReceivedAtKnittingUnitColorSizes>().Get(x => x.YarnReceivedAtKnittingUnitId == YarnReceiveId && x.ColorId == buyerColorId && x.LotNumber == lotNumber && x.SizeId == sizeId).FirstOrDefault();

                        if (sizeInfo != null)
                        {
                            txtSizeQty.Text = sizeInfo.ReceivedQty + "";
                            entryId.Text = sizeInfo.Id + "";
                        }
                    }

                }
            }
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            this.AddNewRowToGrid();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "CacheItems()", true);
        }

        private void AddNewRowToGrid()
        {


            List<DataTable> sizeAndQtyTableList = new List<DataTable>();


            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];

                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {

                        DropDownList ddlColors = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlColors");

                        TextBox tbxLotNumber = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxLotNumber");
                        Label lblColorTotalValue = (Label)this.rptEntryInfo.Items[rowIndex].FindControl("lblColorTotalValue");

                        GridView gvSizeQuantity1 = (GridView)this.rptEntryInfo.Items[rowIndex].FindControl("gvSizeQuantity1");


                        dtCurrentTable.Rows[rowIndex]["BuyerColorId"] = (ddlColors.SelectedValue == "" ? "0" : ddlColors.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["Lot"] = tbxLotNumber.Text;
                        dtCurrentTable.Rows[rowIndex]["ColorTotal"] = lblColorTotalValue.Text;


                        DataTable sizeAndQtyTable = new DataTable();
                        sizeAndQtyTable.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                        sizeAndQtyTable.Columns.Add(new DataColumn("Column0", typeof(string)));

                        for (int j = 0; j < gvSizeQuantity1.Columns.Count; j++)
                        {
                            DataRow sizeAndQtyRow = null;
                            sizeAndQtyRow = sizeAndQtyTable.NewRow();
                            sizeAndQtyRow["RowNumber"] = j + 1;
                            sizeAndQtyRow["Column0"] = string.Empty;
                            sizeAndQtyTable.Rows.Add(sizeAndQtyRow);
                            string controlId = string.Concat("tbxReceivQuantity", j.ToString());
                            TextBox tbxSizeQty = (TextBox)gvSizeQuantity1.Rows[0].Cells[j].FindControl(controlId);
                            sizeAndQtyTable.Rows[j]["Column0"] = tbxSizeQty.Text;
                        }
                        sizeAndQtyTableList.Add(sizeAndQtyTable);

                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["BuyerColorId"] = string.Empty;
                            drCurrentRow["Lot"] = string.Empty;
                            drCurrentRow["SizeAndQty"] = string.Empty;
                            drCurrentRow["ColorTotal"] = string.Empty;
                        }
                    }

                    dtCurrentTable.Rows.InsertAt(drCurrentRow, dtCurrentTable.Rows.Count);

                    this.ViewState["CurrentTable"] = dtCurrentTable;
                    this.ViewState["SizeAndQtyTableList"] = sizeAndQtyTableList;
                    this.rptEntryInfo.DataSource = dtCurrentTable;
                    this.rptEntryInfo.DataBind();
                }
            }
            this.SetPreviousData();
            ShowColorAndStyleTotal();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if ((this.ViewState["CurrentTable"] == null ? false : this.ViewState["SizeAndQtyTableList"] != null))
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                List<DataTable> sizeAndQtyTableList = (List<DataTable>)this.ViewState["SizeAndQtyTableList"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {


                        DropDownList ddlColors = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        TextBox tbxLotNumber = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxLotNumber");
                        Label lblColorTotalValue = (Label)this.rptEntryInfo.Items[rowIndex].FindControl("lblColorTotalValue");
                        GridView gvSizeQuantity1 = (GridView)this.rptEntryInfo.Items[rowIndex].FindControl("gvSizeQuantity1");


                        ddlColors.SelectedIndex = CommonMethods.MatchDropDownItem(ddlColors, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["BuyerColorId"].ToString()) ? "0" : dt.Rows[i]["BuyerColorId"].ToString())));



                        tbxLotNumber.Text = dt.Rows[i]["Lot"].ToString();
                        lblColorTotalValue.Text = dt.Rows[i]["ColorTotal"].ToString();


                        if (i < sizeAndQtyTableList.Count)
                        {
                            DataTable tableFromList = sizeAndQtyTableList[i];
                            for (int j = 0; j < tableFromList.Rows.Count; j++)
                            {
                                TextBox tbxSizeQty = (TextBox)gvSizeQuantity1.Rows[0].Cells[j].FindControl(string.Concat("tbxReceivQuantity", j.ToString()));
                                tbxSizeQty.Text = tableFromList.Rows[j]["Column0"].ToString();
                            }
                        }
                        rowIndex++;
                    }
                }
            }
        }


        protected void btnSaveEntries_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(tbxReceiveDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter receive date.')", true);
            }
            else if (ddlUserProductionUnit.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a knitting unit.')", true);
            }
            else if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select an style.')", true);
            }
            //else if (String.IsNullOrEmpty(tbxKnittingPart.Text))
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter knitting part.')", true);
            //}
            //else if (String.IsNullOrEmpty(tbxYarnDescription.Text))
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter yarn description.')", true);
            //}
            //else if (String.IsNullOrEmpty(tbxGG.Text))
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter GG.')", true);
            //}
            else
            {

                try
                {
                    var receiveDate = DateTime.Parse(tbxReceiveDate.Text);
                    //int gg = 0;
                    int styleTotal = CalculateStyleTotal();

                    if (styleTotal > 0)
                    {
                        btnSaveEntries.Enabled = false;

                        if (receiveDate <= DateTime.Now)
                        {

                            List<YarnReceivedAtKnittingUnitColorSizes> lstColorSizes = new List<YarnReceivedAtKnittingUnitColorSizes>();




                            var yraku = new YarnReceivedAtKnittingUnit()
                            {
                                KnittingUnitId = int.Parse(ddlUserProductionUnit.SelectedValue),
                                ReceivedDate = receiveDate,

                                BuyerId = int.Parse(ddlBuyers.SelectedValue),
                                StyleId = int.Parse(ddlStyles.SelectedValue),
                                KnittingPart = tbxKnittingPart.Text,
                                YarnDescription = tbxYarnDescription.Text,
                                GG = tbxGG.Text,
                                ReceivedQty = styleTotal,
                                CreateDate = DateTime.Now,
                                CreatedBy = CommonMethods.SessionInfo.UserName
                            };


                            unitOfWork.GenericRepositories<YarnReceivedAtKnittingUnit>().Insert(yraku);
                            unitOfWork.Save();

                           

                            for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                            {

                                var ddlColors = (DropDownList)rptEntryInfo.Items[i].FindControl("ddlColors");
                                if (ddlColors.SelectedValue != "")
                                {
                                    var buyerColorId = int.Parse(ddlColors.SelectedValue);

                                    var lotNumber = ((TextBox)rptEntryInfo.Items[i].FindControl("tbxLotNumber")).Text;

                                    var gvSizeQty = (GridView)rptEntryInfo.Items[i].FindControl("gvSizeQuantity1");
                                    for (int j = 0; j < gvSizeQty.Columns.Count; j++)
                                    {
                                        var sizeId = int.Parse(((Label)gvSizeQty.Rows[0].Cells[j].FindControl("lblSizeId" + j)).Text);
                                        var recQty = ((TextBox)gvSizeQty.Rows[0].Cells[j].FindControl("tbxReceivQuantity" + j)).Text;
                                        if (!string.IsNullOrEmpty(recQty))
                                        {
                                            var colorSize = new YarnReceivedAtKnittingUnitColorSizes()
                                            {
                                                YarnReceivedAtKnittingUnitId = yraku.Id,
                                                ColorId = buyerColorId,
                                                LotNumber = lotNumber,
                                                SizeId = sizeId,
                                                ReceivedQty = int.Parse(recQty),
                                                CreateDate = DateTime.Now,
                                                CreatedBy = CommonMethods.SessionInfo.UserName
                                            };
                                            lstColorSizes.Add(colorSize);

                                        }
                                    }
                                }

                            }

                                foreach (var item in lstColorSizes)
                                {
                                    unitOfWork.GenericRepositories<YarnReceivedAtKnittingUnitColorSizes>().Insert(item);
                                }


                                unitOfWork.Save();

                                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                                Response.AddHeader("REFRESH", "2;URL=ReceiveYarnAtUnit.aspx");
                            
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cannot receive in a future date')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('No data was entered.')", true);
                    }

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }

        }

        protected void btnUpdateEntries_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(tbxReceiveDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter receive date.')", true);
            }
            else if (ddlUserProductionUnit.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a knitting unit.')", true);
            }
            else if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select an style.')", true);
            }
            else
            {

                try
                {

                    var receiveDate = DateTime.Parse(tbxReceiveDate.Text);
                    int styleTotal = CalculateStyleTotal();

                    if (styleTotal >= 0) // here zero allowed due to sometimes yarn transfared to another unit
                    {
                        btnUpdateEntries.Enabled = false;
                        if (receiveDate <= DateTime.Now)
                        {
                            var yraku = unitOfWork.GenericRepositories<YarnReceivedAtKnittingUnit>().GetByID(YarnReceiveId);
                            List<YarnReceivedAtKnittingUnitColorSizes> lstColorSizes = new List<YarnReceivedAtKnittingUnitColorSizes>();

                            yraku.KnittingUnitId = int.Parse(ddlUserProductionUnit.SelectedValue);
                            yraku.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                            yraku.StyleId = int.Parse(ddlStyles.SelectedValue);
                            yraku.KnittingPart = tbxKnittingPart.Text;
                            yraku.YarnDescription = tbxYarnDescription.Text;
                            yraku.GG = tbxGG.Text;
                            yraku.ReceivedQty = styleTotal;
                            yraku.UpdateDate = DateTime.Now;
                            yraku.UpdatedBy = CommonMethods.SessionInfo.UserName;


                            for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                            {

                                var ddlColors = (DropDownList)rptEntryInfo.Items[i].FindControl("ddlColors");
                                if (ddlColors.SelectedValue != "")
                                {
                                    var buyerColorId = int.Parse(ddlColors.SelectedValue);

                                    var lotNumber = ((TextBox)rptEntryInfo.Items[i].FindControl("tbxLotNumber")).Text;

                                    var gvSizeQty = (GridView)rptEntryInfo.Items[i].FindControl("gvSizeQuantity1");
                                    for (int j = 0; j < gvSizeQty.Columns.Count; j++)
                                    {
                                        var sizeId = int.Parse(((Label)gvSizeQty.Rows[0].Cells[j].FindControl("lblSizeId" + j)).Text);
                                        var recQty = ((TextBox)gvSizeQty.Rows[0].Cells[j].FindControl("tbxReceivQuantity" + j)).Text;
                                        if (!string.IsNullOrEmpty(recQty))
                                        {
                                            var colorSize = new YarnReceivedAtKnittingUnitColorSizes()
                                            {
                                                YarnReceivedAtKnittingUnitId = yraku.Id,
                                                ColorId = buyerColorId,
                                                LotNumber = lotNumber,
                                                SizeId = sizeId,
                                                ReceivedQty = int.Parse(recQty),
                                                CreateDate = DateTime.Now,
                                                CreatedBy = CommonMethods.SessionInfo.UserName
                                            };
                                            lstColorSizes.Add(colorSize);

                                        }
                                    }
                                }

                            }

                                unitOfWork.GenericRepositories<YarnReceivedAtKnittingUnit>().Update(yraku);

                                var previousColors = unitOfWork.GenericRepositories<YarnReceivedAtKnittingUnitColorSizes>().Get(x => x.YarnReceivedAtKnittingUnitId == YarnReceiveId).ToList();

                                foreach (var item in previousColors)
                                {
                                    unitOfWork.GenericRepositories<YarnReceivedAtKnittingUnitColorSizes>().Delete(item);
                                }
                                foreach (var item in lstColorSizes)
                                {
                                    unitOfWork.GenericRepositories<YarnReceivedAtKnittingUnitColorSizes>().Insert(item);
                                }
                                unitOfWork.Save();


                                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                                Response.AddHeader("REFRESH", "2;URL=ViewReceivedYarnAtUnit.aspx");

                    }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cannot deliver in future date')", true);
                        }

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter size quantity!')", true);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }

            }

        }

        protected void lnkbtnCalculateColorTotal_Click(object sender, EventArgs e)
        {
            ShowColorAndStyleTotal();
        }

        private void ShowColorAndStyleTotal()
        {
            int colorTotal = 0;
            int styleTotal = 0;

            for (int i = 0; i < this.rptEntryInfo.Items.Count; i++)
            {

                Label lblColorTotalValue = (Label)this.rptEntryInfo.Items[i].FindControl("lblColorTotalValue");

                GridView gvSizeQuantity1 = (GridView)this.rptEntryInfo.Items[i].FindControl("gvSizeQuantity1");
                colorTotal = 0;

                for (int j = 0; j < gvSizeQuantity1.Columns.Count; j++)
                {
                    string sizeQuantityTextBox2Id = string.Concat("tbxReceivQuantity", j.ToString());
                    TextBox tbxSizeQty = (TextBox)gvSizeQuantity1.Rows[0].Cells[j].FindControl(sizeQuantityTextBox2Id);

                    if (tbxSizeQty.Text != "")
                    {
                        colorTotal += int.Parse(tbxSizeQty.Text);
                    }

                }
                lblColorTotalValue.Text = colorTotal.ToString();
                styleTotal += colorTotal;
            }

            lblStyleTotal.Text = "Style Total: " + styleTotal.ToString();
        }


        private int CalculateStyleTotal()
        {

            int styleTotal = 0;

            for (int i = 0; i < this.rptEntryInfo.Items.Count; i++)
            {
                GridView gvSizeQuantity1 = (GridView)this.rptEntryInfo.Items[i].FindControl("gvSizeQuantity1");
                for (int j = 0; j < gvSizeQuantity1.Columns.Count; j++)
                {
                    string sizeQuantityTextBox2Id = string.Concat("tbxReceivQuantity", j.ToString());
                    TextBox tbxSizeQty = (TextBox)gvSizeQuantity1.Rows[0].Cells[j].FindControl(sizeQuantityTextBox2Id);

                    if (tbxSizeQty.Text != "")
                    {
                        styleTotal += int.Parse(tbxSizeQty.Text);
                    }

                }
            }

            return styleTotal;
        }



    }
}