﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReceiveYarnAtUnit.aspx.cs" Inherits="MBTracker.Pages.Yarn_and_Accessories.ReceiveYarnAtUnit" %>

<%@ Register Src="~/User_Controls/Header.ascx" TagPrefix="uc1" TagName="Header" %>
<%@ Register Src="~/User_Controls/MainMenu.ascx" TagPrefix="uc1" TagName="MainMenu" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>MB Tracker</title>
    <link href="../../css/web.css" rel="stylesheet" />
    <script type="text/javascript" src="../../js/gridviewscroll.js"></script>

    <script type="text/javascript">
        var gridViewScroll = null;
        window.onload = function () {
            gridViewScroll = new GridViewScroll({
                elementID: "entryTableNotUsing",
                width: 1440,
                height: 500,
                freezeColumn: true,
                freezeFooter: false,
                freezeColumnCssClass: "GridViewScrollItemFreeze",
                freezeFooterCssClass: "GridViewScrollFooterFreeze",
                freezeHeaderRowCount: 1,
                freezeColumnCount: 2,
                onscroll: function (scrollTop, scrollLeft) {
                    console.log(scrollTop + " - " + scrollLeft);
                }
            });
            gridViewScroll.enhance();
        }
        function getScrollPosition() {
            var position = gridViewScroll.scrollPosition;
            alert("scrollTop: " + position.scrollTop + ", scrollLeft: " + position.scrollLeft);
        }
        function setScrollPosition() {
            var scrollPosition = { scrollTop: 50, scrollLeft: 50 };

            gridViewScroll.scrollPosition = scrollPosition;
        }


    </script>

    <script src="../../js/jquery.dataTables.js"></script>

    <script src="../../js/html5-trunk.js"></script>
    <link href="~/css/bootstrap.min.css" rel="stylesheet" />
    <link type="text/css" href="~/icomoon/style.css" rel="stylesheet" />
    <%--<link type="text/css" href="/css/nvd-charts.css" rel="stylesheet" />--%>
    <link type="text/css" href="~/css/main.css" rel="stylesheet" />
    <%--   <link type="text/css" href='/css/fullcalendar/fullcalendar.css' rel='stylesheet' />
    <link type="text/css" href='/css/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />--%>

    <link type="text/css" href="~/css/custom.css" rel="stylesheet" />
    <link type="text/css" href="~/content/toastr.css" rel="stylesheet" />
    <script src="../../js/jquery.min.js"></script>
    <script src="../../js/bootstrap.js"></script>
    <script src="../../Scripts/toastr.js"></script>
    <%--  <link href="../../FancyBox/jquery.fancybox.css" rel="stylesheet" />--%>
    <link href="../../css/font-awesome.min.css" rel="stylesheet" />

    <%--<link href="../../css/all.css" rel="stylesheet" />--%>
    <link href="../../css/sweetalert.css" rel="stylesheet" />
    <script src="../../Scripts/sweetalert.min.js"></script>
    <link href="../../css/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.min.js"></script>
    <script src="../../js/jquery.blockUI.js"></script>
    <script src="../../Scripts/jquery.signalR-2.2.2.js"></script>
    <script src="/SignalR/hubs"></script>
    <link href="../../content/css/select2.css" rel="stylesheet" />
    <script src="../../Scripts/select2.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $("#main-nav ul li ul li").hover(function () {
                $(this).children("ul").toggleClass('test-SubMenu-open', 1, "easeOutSine");
            }, function () {
                $(this).children("ul").toggleClass('test-SubMenu-open', 1, "easeOutSine");
            });

        });

        try {

            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

        } catch (e) {

        }

        function beginRequest(sender, args) {

            $.blockUI({
                //message: '<img src="../../images/loader.gif" />',
                //message: '<div class="spinner"></div>',
                message: '<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>',
                //message: '<div class="lds-ripple"><div></div><div></div></div>',
                css: {
                    border: 'none',
                    backgroundColor: 'transparent'
                },
                overlayCSS: { backgroundColor: '#0d0d0d' }
            });
        }

        function endRequest(sender, args) {
            $.unblockUI();
        }

        function blockUI() {
            $.blockUI({
                message: '<img src="../../images/loader.gif" />',
                css: {
                    border: 'none',
                    backgroundColor: 'transparent'
                },
                overlayCSS: { backgroundColor: '#0d0d0d' }
            });
        }

        function unblockUI() {
            $.unblockUI();
        }

        function scrollDown(position) {
            console.log(position);
            var n = $(document).height();
            if (position != "" && position != 'undefined' && position != null) {
                n = position;
            }
            $('html, body').animate({ scrollTop: n }, 'slow');
        };

        function reloadPage() {
            setTimeout(function () { location.reload(); }, 1000);
        }

        function resetPage() {
            debugger
            $('#form1')[0].reset();
        }


        function returnToPage(url) {
            setTimeout(function () {
                $(location).attr('href', url);
            }, 2000);
        }

        function showConfirm(title, msg, purpose, width, onlyClose) {
            var dialogWidth = "auto";
            var dialogButtons = {
                Yes: function () {
                    $(this).dialog("close");
                    __doPostBack("Confirm", purpose);
                },
                No: function () {
                    $(this).dialog("close");
                }
            };

            if (width != null && width != 'undefined') {
                dialogWidth = width;
            }
            if (onlyClose == 'true') {
                dialogButtons = {
                    Close: function () {
                        $(this).dialog("close");
                    }
                }
            }

            $('<div></div>').appendTo('body')
                .html('<div><h6>' + msg + '</h6></div>')
                .dialog({
                    modal: true,
                    title: title,
                    zIndex: 10000,
                    autoOpen: true,
                    width: dialogWidth,
                    resizable: false,
                    buttons: dialogButtons,
                    close: function (event, ui) {
                        $(this).remove();
                    }
                });
        };


        function showConfirmSA(title, msg, purpose) {
            swal({
                title: title,
                text: msg,
                html: true,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        __doPostBack("Confirm", "ConfirmBtnClicked", purpose);
                    }
                });
        };


         //var ddlText, ddlValue, ddl;

        <%--function CacheItems() {


            ddlText = new Array();
            ddlValue = new Array();
            ddl = document.getElementById("<%=ddlStyles.ClientID %>");
            for (var i = 0; i < ddl.options.length; i++) {
                ddlText[ddlText.length] = ddl.options[i].text;
                ddlValue[ddlValue.length] = ddl.options[i].value;
            }

            //alert("The length of ddl: " + ddl.options.length);
        }--%>


        //function FilterItems(value) {
        //    ddl.options.length = 0;

        //    for (var i = 0; i < ddlText.length; i++) {

        //        if (ddlText[i].toLowerCase().indexOf(value) != -1) {

        //            AddItem(ddlText[i], ddlValue[i]);
        //        }
        //    }


        //    function AddItem(text, value) {

        //        var opt = document.createElement("option");

        //        opt.text = text;
        //        opt.value = value;
        //        ddl.options.add(opt);

        //    }
        //}





    </script>

</head>


<body style="margin: 0px; width: 100%; height: 100%;">
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <header id="header">
            <uc1:Header runat="server" ID="Header" />
        </header>

        <div class="container" id="menu" style="margin: 0px;">
            <div id="main-nav" class="hidden-phone hidden-tablet">
                <uc1:MainMenu runat="server" ID="MainMenu" />
                <div class="clearfix"></div>
            </div>
        </div>




        <div class="container-fluid">
            <div class="dashboard-wrapper">
                <div class="main-container" id="body">
                    <div class="page-header">
                        <div class="clearfix"></div>
                    </div>
                    <div>
                    </div>

                    <div class="row-fluid">
                        <div class="span9">
                            <div class="widget">
                                <div class="widget-header">
                                    <div class="title">
                                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                                        <asp:Label runat="server" ID="lblTitle" Text="Receive Yarn at Knitting Unit:"></asp:Label>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="row" style="padding-left: 5px; padding-right: 5px;">
                                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                                            <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field&nbsp;&nbsp;&nbsp;&nbsp;
                                        </div>
                                        <div class="control-group">
                                            <div class="col-md-12 col-sm-12">
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-horizontal">
                                                <div class="control-group">
                                                    <label for="inputStyle" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                                        <asp:Label ID="Label2" runat="server" Text="Receive Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                                    <div class="controls controls-row" style="margin-left: 140px">
                                                        <asp:TextBox ID="tbxReceiveDate" AutoPostBack="true" TextMode="Date" Width="100%" Text="" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                            ControlToValidate="tbxReceiveDate"><span style="font-weight: 700; color: #CC0000">Please select receive date.</span></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                                        <asp:Label ID="Label1" runat="server" Text="Knitting Unit:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                                    <div class="controls controls-row" style="margin-left: 140px">
                                                        <asp:DropDownList ID="ddlUserProductionUnit" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                                            ControlToValidate="ddlUserProductionUnit"><span style="font-weight: 700; color: #CC0000">Please select a knitting unit.</span></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                                        <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                                    <div class="controls controls-row" style="margin-left: 140px">
                                                        <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control custom-select2"></asp:DropDownList>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                            ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="inputStyle" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                                        <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                                    <div class="controls controls-row" style="margin-left: 140px">
                                                        <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control custom-select2"></asp:DropDownList>
                                                        <%--<asp:TextBox ID="tbxSearchCriteriaForStyle" runat="server" onkeyup="FilterItems(this.value)" Width="29%" PlaceHoder="Type to Search style(s)"></asp:TextBox>--%>

                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                                            ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select a style.</span></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-horizontal">
                                                <div class="control-group">
                                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                                        <asp:Label ID="Label4" runat="server" Text="Knitting Part:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                                    <div class="controls controls-row" style="margin-left: 140px">
                                                        <asp:TextBox ID="tbxKnittingPart" runat="server" placeholder="Enter knitting part" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                                        <asp:Label ID="Label6" runat="server" Text="Yarn Description:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                                    <div class="controls controls-row" style="margin-left: 140px">
                                                        <asp:TextBox ID="tbxYarnDescription" runat="server" placeholder="Enter yarn description" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>

                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                                        <asp:Label ID="Label5" runat="server" Text="GG (Only Number):"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                                    <div class="controls controls-row" style="margin-left: 140px">
                                                        <asp:TextBox ID="tbxGG" runat="server" placeholder="Enter GG" CssClass="form-control" TextMode="Number"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>




                    <%--                    <div class="row-fluid">
                        <asp:Label ID="lblNoDataFound" runat="server" Text="There is no color for this style." Visible="false" BackColor="#ffff00"></asp:Label>

                    </div>

                    <div class="span6" style="text-align: center">
                        <asp:Label ID="lblSaveMessage" runat="server" Text="Data was saved successfully." Visible="false" ForeColor="#006666"></asp:Label>
                    </div>--%>

                    <div class="row-fluid" runat="server" id="divColors" visible="false">
                        <div class="span12">
                            <div class="widget" style="overflow-x: auto">
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div id="dt_example" class="example_alt_pagination">
                                            <div class="control-group">
                                                <label for="inputOrder" class="control-label">
                                                    <asp:Label ID="lblEntry" runat="server" Text="" Font-Bold="true"></asp:Label></label>
                                                <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                                    <asp:Repeater ID="rptEntryInfo" OnItemDataBound="rptEntryInfo_ItemDataBound" runat="server">
                                                        <HeaderTemplate>
                                                            <table id="entryTable" class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr class="GridViewScrollHeader">
                                                                        <th style="text-align: center; width: 10%">
                                                                            <asp:Label ID="lblColor" runat="server" Text="Color Names"></asp:Label></th>
                                                                        <th style="text-align: center; width: 10%">
                                                                            <asp:Label ID="lblLotNumberMsg" runat="server" Text="Lot Number"></asp:Label></th>

                                                                        <th style="text-align: center">
                                                                            <asp:Label ID="lblSizeAndQuantity" runat="server" Text="Size & Quantity"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="lblColorTotal" runat="server" Text="Color Total"></asp:Label></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr class="GridViewScrollItem">

                                                                <td>
                                                                    <asp:DropDownList ID="ddlColors" runat="server" CssClass="custom-select2" Width="100%" Style="min-width: 200px" Display="Dynamic"></asp:DropDownList>
                                                                </td>

                                                                <td>
                                                                    <asp:TextBox ID="tbxLotNumber" runat="server" Width="100%" Style="min-width: 100px"></asp:TextBox>
                                                                </td>

                                                                <td>
                                                                    <asp:GridView ID="gvSizeQuantity1" RowStyle-Height="30" Style="margin-left: 5px" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity1_RowCreated">
                                                                    </asp:GridView>
                                                                    <asp:Label ID="lblNoSizeFound" runat="server" Text="No size found." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblColorTotalValue" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody>
                                </table>
                                                             <div class="pull-right" style="margin-right: 0px; padding-top: 10px">
                                                                 <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="71px" Text="Add Row" OnClick="btnAddRow_Click" />
                                                             </div>
                                                            <div class="clearfix"></div>
                                                        </FooterTemplate>
                                                    </asp:Repeater>

                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                            <label for="inputOrder" class="control-label" style="text-align: right; padding-top: 0px">
                                <asp:Label ID="lblStyleTotal" runat="server" Text="" Font-Bold="true"></asp:Label>
                                <br />
                                <asp:LinkButton ID="lnkbtnCalculateStyleTotal" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="Calculate Total" OnClick="lnkbtnCalculateColorTotal_Click"></asp:LinkButton>
                            </label>

                            <asp:Button ID="btnSaveEntries" runat="server" ValidationGroup="save" class="btn btn-success btn-medium pull-left hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSaveEntries_Click"></asp:Button>&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnUpdateEntries" Visible="false" ValidationGroup="save" runat="server" class="btn btn-success btn-medium pull-left hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdateEntries_Click"></asp:Button>
                        </div>
                    </div>




                </div>
            </div>
        </div>

        <div class="col-md-12" id="footer">
            <div class="row" style="text-align: center; color: #f79825">
                <b>Copyright © 2020 <a style="text-decoration: none; color: #f79825;" href="http://www.masihata.com/">Masihata Group</a> </b>
            </div>
        </div>

    </form>
    <script>
        $(function () {
            //$('.custom-select2').select2();
            $('select').select2();
            //$('select').select2({
            //    sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)),
            //});
        })
    </script>
</body>
</html>
