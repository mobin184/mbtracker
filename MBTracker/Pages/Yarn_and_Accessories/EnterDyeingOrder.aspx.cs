﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class EnterDyeingOrder : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        decimal totalQty = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadMerchandiserList();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                if (Request.QueryString["doId"] != null)
                {
                    doId = int.Parse(Tools.UrlDecode(Request.QueryString["doId"]));
                    var recInfo = unitOfWork.GenericRepositories<DyeingOrder>().Get(x => x.DyeingOrderId == doId).FirstOrDefault();
                    ddlMerchandiser.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMerchandiser, recInfo.MerchandiserId);
                    tbxDyeingOrderNumber.Text = recInfo.DyeingOrderNumber;
                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, recInfo.BuyerId);
                    BindStylesByBuyer(ddlStyles,recInfo.BuyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, recInfo.StyleId);
                    LoadPIInfo(recInfo.StyleId);
                    ddlPINumber.SelectedIndex = CommonMethods.MatchDropDownItem(ddlPINumber, recInfo.PIId);
                    var itemDetails = unitOfWork.GetDataTableFromSql($"SELECT CAST(ColorId as nvarchar(max)) as ColorId,CAST(ItemId as nvarchar(max)) as ItemId,LabDipRef,Shade,LOTNumber,CAST(Quantity as nvarchar(max))  as Quantity,Remarks FROM DyeingOrderItems WHERE DyeingOrderId = {recInfo.DyeingOrderId}");
                    ViewState["CurrentTable"] = itemDetails;
                    rptItemRecEntryInfo.DataSource = itemDetails;
                    rptItemRecEntryInfo.DataBind();
                    SetPreviousData();
                    divItemInfo.Visible = true;
                    lnkbtnUpdateEntries.Visible = true;
                    sizeActionTitle.Text = "Update Dyeing Order Information";
                }
            }
        }

        private void LoadPIInfo(int styleId)
        {
            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetPIListByStyleId {styleId}");
            ddlPINumber.Items.Add(new ListItem("", ""));
            ddlPINumber.DataSource = dt;
            ddlPINumber.DataTextField = "PINumber";
            ddlPINumber.DataValueField = "Id";
            ddlPINumber.DataBind();
            ddlPINumber.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlPINumber.SelectedIndex = 0;
        }

        private void LoadMerchandiserList()
        {
            var dt = unitOfWork.GetDataTableFromSql("EXEC usp_GetMerchandiserList");
            ddlMerchandiser.Items.Add(new ListItem("", ""));
            ddlMerchandiser.DataSource = dt;
            ddlMerchandiser.DataTextField = "Name";
            ddlMerchandiser.DataValueField = "Id";
            ddlMerchandiser.DataBind();
            ddlMerchandiser.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlMerchandiser.SelectedIndex = 0;
        }

        int doId
        {
            set { ViewState["doId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["doId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("ColorId", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));
            dt.Columns.Add(new DataColumn("LabDipRef", typeof(string)));
            dt.Columns.Add(new DataColumn("Shade", typeof(string)));
            dt.Columns.Add(new DataColumn("LOTNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("Remarks", typeof(string)));

            dr = dt.NewRow();
            dr["ColorId"] = string.Empty;
            dr["ItemId"] = string.Empty;
            dr["LabDipRef"] = string.Empty;
            dr["Shade"] = string.Empty;
            dr["LOTNumber"] = string.Empty;
            dr["Quantity"] = string.Empty;
            dr["Remarks"] = string.Empty;

            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;

            rptItemRecEntryInfo.DataSource = dt;
            rptItemRecEntryInfo.DataBind();
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {
                        DropDownList ddlColors = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        DropDownList ddlItems = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlItems");
                        TextBox tbxLOTNumber = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxLOTNumber");
                        TextBox tbxLabDipRef = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxLabDipRef");
                        TextBox tbxShade = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxShade");
                        TextBox tbxRemarks = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxRemarks");
                        TextBox tbxQty = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxQty");

                        dtCurrentTable.Rows[rowIndex]["ColorId"] = ddlColors.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["ItemId"] = ddlItems.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["LabDipRef"] = tbxLabDipRef.Text;
                        dtCurrentTable.Rows[rowIndex]["Shade"] = tbxShade.Text;
                        dtCurrentTable.Rows[rowIndex]["LOTNumber"] = tbxLOTNumber.Text;
                        dtCurrentTable.Rows[rowIndex]["Quantity"] = tbxQty.Text;
                        dtCurrentTable.Rows[rowIndex]["Remarks"] = tbxRemarks.Text;

                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["ColorId"] = string.Empty;
                            drCurrentRow["LabDipRef"] = string.Empty;
                            drCurrentRow["Shade"] = string.Empty;
                            drCurrentRow["ItemId"] = string.Empty;
                            drCurrentRow["LOTNumber"] = string.Empty;
                            drCurrentRow["Quantity"] = string.Empty;
                            drCurrentRow["Remarks"] = string.Empty;
                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptItemRecEntryInfo.DataSource = dtCurrentTable;
                    this.rptItemRecEntryInfo.DataBind();
                }
            }
            this.SetPreviousData();
        }


        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DropDownList ddlColors = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        DropDownList ddlItems = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlItems");
                        TextBox tbxLOTNumber = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxLOTNumber");
                        TextBox tbxLabDipRef = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxLabDipRef");
                        TextBox tbxShade = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxShade");
                        TextBox tbxQty = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxQty");
                        TextBox tbxRemarks = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxRemarks");

                        SetColorDropdown(ddlColors, int.Parse(ddlStyles.SelectedValue));
                        ddlColors.SelectedIndex = CommonMethods.MatchDropDownItem(ddlColors, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ColorId"].ToString()) ? "0" : dt.Rows[i]["ColorId"].ToString())));
                        SetItemDropdown(ddlItems);
                        ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ItemId"].ToString()) ? "0" : dt.Rows[i]["ItemId"].ToString())));

                        tbxLabDipRef.Text = dt.Rows[i]["LabDipRef"].ToString();
                        tbxShade.Text = dt.Rows[i]["Shade"].ToString();
                        tbxLOTNumber.Text = dt.Rows[i]["LOTNumber"].ToString();
                        tbxQty.Text = dt.Rows[i]["Quantity"].ToString();
                        tbxRemarks.Text = dt.Rows[i]["Remarks"].ToString();

                        rowIndex++;
                    }
                }
            }
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlStyles.SelectedValue != "")
            {
                LoadPIInfo(int.Parse(ddlStyles.SelectedValue));
                //dtColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetStyleColorsByStyleId {ddlStyles.SelectedValue}");
            }
            else
            {
                //dtColors = null;
                ddlPINumber.Items.Clear();
            }
        }

        protected void SetColorDropdown(DropDownList ddlColor, int styleId)
        {
            var dtColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetStyleColorsByStyleId {styleId}");
            ddlColor.Items.Add(new ListItem("", ""));
            ddlColor.DataSource = dtColors;
            ddlColor.DataTextField = "ColorDescription";
            ddlColor.DataValueField = "BuyerColorId";
            ddlColor.DataBind();
            ddlColor.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlColor.SelectedIndex = 0;
        }

        protected void SetItemDropdown(DropDownList ddlItem)
        {
            if (ddlPINumber.SelectedValue != "")
            {
                var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetYarnItemsByPIId {ddlPINumber.SelectedValue}");
                ddlItem.Items.Add(new ListItem("", ""));
                ddlItem.DataSource = dt;
                ddlItem.DataTextField = "ItemName";
                ddlItem.DataValueField = "ItemId";
                ddlItem.DataBind();
                ddlItem.Items.Insert(0, new ListItem("---Select---", string.Empty));
                ddlItem.SelectedIndex = 0;
            }
            else
            {
                ddlItem.Items.Clear();
            }
        }

        protected void rptItemRecEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var ddlColors = (DropDownList)e.Item.FindControl("ddlColors");
                var ddlItems = (DropDownList)e.Item.FindControl("ddlItems");
                if (ddlStyles.SelectedValue != "")
                {
                    SetColorDropdown(ddlColors, int.Parse(ddlStyles.SelectedValue));
                    SetItemDropdown(ddlItems);
                }
            }
        }

        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a style.')", true);
            }
            else if (ddlMerchandiser.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a merchandiser.')", true);
            }
            else if (ddlMerchandiser.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a merchandiser.')", true);
            }
            else if (ddlPINumber.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a PI.')", true);
            }
            else
            {
                try
                {                   
                    var dyeingOrder = new DyeingOrder()
                    {
                        BuyerId = int.Parse(ddlBuyers.SelectedValue),
                        StyleId = int.Parse(ddlStyles.SelectedValue),
                        PIId = int.Parse(ddlPINumber.SelectedValue),
                        MerchandiserId = int.Parse(ddlMerchandiser.SelectedValue),
                        DyeingOrderNumber = tbxDyeingOrderNumber.Text,
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName

                    };

                    List<DyeingOrderItems> listItems = new List<DyeingOrderItems>();

                    for (int rowIndex = 0; rowIndex < rptItemRecEntryInfo.Items.Count; rowIndex++)
                    {
                        DropDownList ddlColors = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        DropDownList ddlItems = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlItems");

                        TextBox tbxLOTNumber = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxLOTNumber");
                        TextBox tbxLabDipRef = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxLabDipRef");
                        TextBox tbxShade = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxShade");
                        TextBox tbxQty = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxQty");
                        TextBox tbxRemarks = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxRemarks");

                        if (ddlColors.SelectedValue != "" && ddlItems.SelectedValue != "" && !string.IsNullOrEmpty(tbxQty.Text))
                        {
                            var styleId = int.Parse(ddlStyles.SelectedValue);
                            var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().Get(x => x.Id == styleId).FirstOrDefault();
                            var item = new DyeingOrderItems()
                            {
                                DyeingOrderId = dyeingOrder.DyeingOrderId,
                                ColorId = int.Parse(ddlColors.SelectedValue),
                                ItemId = int.Parse(ddlItems.SelectedValue),
                                LOTNumber = tbxLOTNumber.Text,
                                Quantity = Convert.ToDecimal(tbxQty.Text),
                                LabDipRef = tbxLabDipRef.Text,
                                PIId = int.Parse(ddlPINumber.SelectedValue),
                                Shade = tbxShade.Text,
                                Remarks = tbxRemarks.Text,
                                CreateDate = DateTime.Now,
                                CreatedBy = CommonMethods.SessionInfo.UserName
                            };
                            totalQty += item.Quantity;
                            listItems.Add(item);
                        }
                    }

                    if (listItems.Count() > 0)
                    {
                        dyeingOrder.DyeingOrderQuantity = totalQty;
                        dyeingOrder.DyeingOrderItems = listItems;
                        unitOfWork.GenericRepositories<DyeingOrder>().Insert(dyeingOrder);
                        unitOfWork.Save();

                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('EnterDyeingOrder.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please add dyeing order items.')", true);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
        }

        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a style.')", true);
            }
            else if (ddlMerchandiser.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a merchandiser.')", true);
            }
            else if (ddlMerchandiser.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a merchandiser.')", true);
            }
            else if (ddlPINumber.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a PI.')", true);
            }
            else
            {
                try
                {
                    totalQty = 0;
                    var dyeingOrder = unitOfWork.GenericRepositories<DyeingOrder>().GetByID(doId);
                    var oldItems = unitOfWork.GenericRepositories<DyeingOrderItems>().Get(x => x.DyeingOrderId == doId).ToList();
                    dyeingOrder.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                    dyeingOrder.StyleId = int.Parse(ddlStyles.SelectedValue);
                    dyeingOrder.PIId = int.Parse(ddlPINumber.SelectedValue);
                    dyeingOrder.MerchandiserId = int.Parse(ddlMerchandiser.SelectedValue);
                    dyeingOrder.DyeingOrderNumber = tbxDyeingOrderNumber.Text;
                    dyeingOrder.UpdateDate = DateTime.Now;
                    dyeingOrder.UpdatedBy = CommonMethods.SessionInfo.UserName;

                    foreach (var item in oldItems)
                    {
                        unitOfWork.GenericRepositories<DyeingOrderItems>().Delete(item);
                    }

                    List<DyeingOrderItems> listItems = new List<DyeingOrderItems>();
                    for (int rowIndex = 0; rowIndex < rptItemRecEntryInfo.Items.Count; rowIndex++)
                    {
                        DropDownList ddlColors = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        DropDownList ddlItems = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlItems");

                        TextBox tbxLOTNumber = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxLOTNumber");
                        TextBox tbxLabDipRef = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxLabDipRef");
                        TextBox tbxShade = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxShade");
                        TextBox tbxQty = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxQty");
                        TextBox tbxRemarks = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxRemarks");

                        if (ddlColors.SelectedValue != "" && ddlItems.SelectedValue != "" && !string.IsNullOrEmpty(tbxQty.Text))
                        {
                            var styleId = int.Parse(ddlStyles.SelectedValue);
                            var item = new DyeingOrderItems()
                            {
                                DyeingOrderId = dyeingOrder.DyeingOrderId,
                                ColorId = int.Parse(ddlColors.SelectedValue),
                                ItemId = int.Parse(ddlItems.SelectedValue),
                                LOTNumber = tbxLOTNumber.Text,
                                Quantity = Convert.ToDecimal(tbxQty.Text),
                                LabDipRef = tbxLabDipRef.Text,
                                Shade = tbxShade.Text,
                                Remarks = tbxRemarks.Text,
                                CreateDate = DateTime.Now,
                                CreatedBy = CommonMethods.SessionInfo.UserName
                            };
                            totalQty += item.Quantity;
                            listItems.Add(item);
                        }
                    }

                    if (listItems.Count() > 0)
                    {
                        dyeingOrder.DyeingOrderQuantity = totalQty;
                        dyeingOrder.DyeingOrderItems = listItems;
                        unitOfWork.GenericRepositories<DyeingOrder>().Update(dyeingOrder);
                        unitOfWork.Save();

                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewDyeingOrders.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please add dyeing order items.')", true);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
        }

        protected void ddlBuyer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBuyers.SelectedValue == "")
            {
                ddlStyles.Items.Clear();
            }
            else
            {
                BindStylesByBuyer(ddlStyles, int.Parse(ddlBuyers.SelectedValue));
            }
        }


        private void BindStylesByBuyer(DropDownList ddlStyles, int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void ddlPINumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPINumber.SelectedValue == "")
            {
                divItemInfo.Visible = false;
                lnkbtnSaveEntries.Visible = false;
                lnkbtnUpdateEntries.Visible = false;
            }
            else
            {
                SetInitialRowCount();
                divItemInfo.Visible = true;
                if (doId == 0)
                {
                    lnkbtnSaveEntries.Visible = true;
                    lnkbtnUpdateEntries.Visible = false;
                }
                else
                {
                    lnkbtnSaveEntries.Visible = false;
                    lnkbtnUpdateEntries.Visible = true;
                }
            }
        }
    }
}