﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class ReceiveYarnAtStoreV2 : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        decimal totalGoods = 0;
        decimal grandTotalAmount = 0;
        //DataTable dtColors = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadYarnAndAccessoriesPurchaseType(ddlPurchaseType, 1, 0);                
                LoadStore();
                LoadMerchandiserList();
               

                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                if (Request.QueryString["receivedId"] != null)
                {
                    CommonMethods.LoadYarnAndAccessoriesSupplierDropdown(ddlYarnAndAccssSuppliers, 1, 0);
                    ReceivedId = int.Parse(Tools.UrlDecode(Request.QueryString["receivedId"]));
                    var recInfo = unitOfWork.GenericRepositories<YarnReceivedAtStore>().Get(x => x.YarnReceivedId == ReceivedId).FirstOrDefault();
                    tbxReceiveDate.Text = recInfo.ReceiveDate.ToString("yyyy-MM-dd");
                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, recInfo.BuyerId);

                    BindStylesByBuyer(ddlStyles, recInfo.BuyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, recInfo.StyleId);
                    //ddlPiNumber.SelectedIndex = CommonMethods.MatchDropDownItem(ddlPiNumber, recInfo.PiNumber);
                    //Test
                    //CommonMethods.LoadDropdownById(ddlPiNumber, Convert.ToInt32(ddlPiNumber.SelectedValue), "PIInfo", 1, 0);
                    LoadAllPIInfo();
                    ddlPiNumber.SelectedIndex = CommonMethods.MatchDropDownItem(ddlPiNumber, int.Parse(recInfo.PiNumber.ToString()));
                    // ddlPiNumber.SelectedIndex = CommonMethods.MatchDropDownItem(ddlPiNumber, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["OrderSeasonId"].ToString()) ? "0" : dtPIInfo.Rows[0]["OrderSeasonId"].ToString())));

                    //Test
                    ddlYarnAndAccssSuppliers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlYarnAndAccssSuppliers, recInfo.SupplierId);
                    ddlPurchaseType.SelectedIndex = CommonMethods.MatchDropDownItem(ddlPurchaseType, recInfo.PurchaseType);
                    ddlStore.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStore, recInfo.StoreId);
                    ddlMerchandiser.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMerchandiser, recInfo.MerchandiserId ?? 0);
                    tbxInvoiceNumber.Text = recInfo.InvoiceNumber;
                    CommonMethods.LoadLCBySupplier(ddlLCNumber, 1, 0, recInfo.SupplierId);
                    ddlLCNumber.SelectedIndex = CommonMethods.MatchDropDownItem(ddlLCNumber, recInfo.LCId ?? 0);
                    tbxChalanNo.Text = recInfo.ChalanNo;

                    tbxLCNumber.Text = recInfo.LCNumber;
                    tbxDyeingOrderNumber.Text = recInfo.DyeingOrderNumber;

                    LoadDyeingOrderByStyle(recInfo.StyleId);
                    ddlDyeingOrder.SelectedIndex = CommonMethods.MatchDropDownItem(ddlDyeingOrder, recInfo.DyeingOrderId ?? 0);

                    var itemDetails = unitOfWork.GetDataTableFromSql($"SELECT CAST(ColorId as nvarchar(max)) as ColorId,CAST(ItemId as nvarchar(max)) as ItemId,CAST(ItemCatId as nvarchar(max)) as ItemCategoryId,LOTNumber,CAST(ReceivedQty as nvarchar(max))  as Quantity FROM YarnReceivedAtStoreItems WHERE YarnReceivedId = {recInfo.YarnReceivedId}");
                    ViewState["CurrentTable"] = itemDetails;
                    rptItemRecEntryInfo.DataSource = itemDetails;
                    rptItemRecEntryInfo.DataBind();
                    SetPreviousData();
                    
                   
                    
                    //if (recInfo.LCId != null)
                    //{
                    //    ShowPIInformationByLCId(recInfo.LCId ?? 0);
                    //}

                    ShowItemEntryInfo();
                   
                    sizeActionTitle.Text = "Update Yarn Received Information";
                }
            }
        }

        private void LoadMerchandiserList()
        {
            var dt = unitOfWork.GetDataTableFromSql("EXEC usp_GetMerchandiserList");
            ddlMerchandiser.Items.Add(new ListItem("", ""));
            ddlMerchandiser.DataSource = dt;
            ddlMerchandiser.DataTextField = "Name";
            ddlMerchandiser.DataValueField = "Id";
            ddlMerchandiser.DataBind();
            ddlMerchandiser.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlMerchandiser.SelectedIndex = 0;
        }

        int ReceivedId
        {
            set { ViewState["receivedId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["receivedId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        private void LoadStore()
        {
            var dt = unitOfWork.GetDataTableFromSql("SELECT StoreId, StoreName FROM Stores WHERE StoreType = 2");
            ddlStore.Items.Add(new ListItem("", ""));
            ddlStore.DataSource = dt;
            ddlStore.DataTextField = "StoreName";
            ddlStore.DataValueField = "StoreId";
            ddlStore.DataBind();
            ddlStore.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlStore.SelectedIndex = 0;
           
        }

        private void LoadAllPIInfo()
        {

            var dt = unitOfWork.GetDataTableFromSql("SELECT Id, PINumber FROM PIInfo");
            ddlPiNumber.Items.Add(new ListItem("", ""));
            ddlPiNumber.DataSource = dt;
            ddlPiNumber.DataTextField = "PINumber";
            ddlPiNumber.DataValueField = "Id";
            ddlPiNumber.DataBind();
            ddlPiNumber.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlPiNumber.SelectedIndex = 0;
        }

       

        protected void ddlYarnAndAccssSuppliers_SelectedIndexChanged(object sender, EventArgs e)
        {
            var supplierIdTxt = ddlYarnAndAccssSuppliers.SelectedValue;
            if (supplierIdTxt != "")
            {
                var supplierId = int.Parse(supplierIdTxt);
                var styleId = int.Parse(ddlStyles.SelectedValue);
                CommonMethods.LoadYarnLCBySupplierAndStyle(ddlLCNumber, 1, 0, supplierId, styleId);
                ddlPiNumber.Items.Clear();
                LoadPiInfo();
            }
            else
            {
                ddlLCNumber.Items.Clear();
            }
        }


        protected void ddlPiNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            var PiNumberTxt = ddlPiNumber.SelectedValue;
            if (PiNumberTxt != "")
            {
                // var supplierId = int.Parse(supplierIdTxt);
                //var styleId = int.Parse(ddlStyles.SelectedValue);
                //CommonMethods.LoadYarnLCBySupplierAndStyle(ddlLCNumber, 1, 0, supplierId, styleId);
                //LoadPiInfo();
                if (Request.QueryString["receivedId"] != null)
                { }
                else { 
                SetInitialRowCount();
                ShowItemEntryInfo();
                LoadDyeingOrderByPIAndStyle();
                }
            }
            else
            {
                ddlLCNumber.Items.Clear();
            }
        }

        private void LoadDyeingOrderByPIAndStyle()
        {
            var dt = unitOfWork.GetDataTableFromSql($"Select DyeingOrderId,DyeingOrderNumber from DyeingOrder where  PIId = {int.Parse(ddlPiNumber.SelectedValue)}");
            ddlDyeingOrder.Items.Add(new ListItem("", ""));
            ddlDyeingOrder.DataSource = dt;
            ddlDyeingOrder.DataTextField = "DyeingOrderNumber";
            ddlDyeingOrder.DataValueField = "DyeingOrderId";
            ddlDyeingOrder.DataBind();
            //ddlDyeingOrder.Items.Insert(0, new ListItem("NA","0"));
            ddlDyeingOrder.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlDyeingOrder.SelectedIndex = 0;
        }

        protected void ddlLCNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetInitialRowCount();
            ShowItemEntryInfo();
            //lnkbtnSaveEntries.Visible = true;
            var lcIdTxt = ddlLCNumber.SelectedValue;
            //if (lcIdTxt != "")
            //{

            //    var LCId = int.Parse(lcIdTxt);
            //    ShowPIInformationByLCId(LCId);
            //    //ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);    
            //}
        }

        private void ShowItemEntryInfo()
        {
            //if(ddlLCNumber.SelectedValue != "" || ddlDyeingOrder.SelectedValue != "")
            if(ddlYarnAndAccssSuppliers.SelectedValue != "" )
            {                
                divItemInfo.Visible = true;
            }
            else
            {
                divItemInfo.Visible = false;
                lnkbtnSaveEntries.Visible = false;
                lnkbtnUpdateEntries.Visible = false;
            }

            //if ((ddlLCNumber.SelectedValue != "" || ddlDyeingOrder.SelectedValue != "") && ReceivedId > 0)
            if (ddlYarnAndAccssSuppliers.SelectedValue != "" && ReceivedId > 0)
            {
                lnkbtnUpdateEntries.Visible = true;
                lnkbtnSaveEntries.Visible = false;
            }
            else
            {
                lnkbtnUpdateEntries.Visible = false;
                lnkbtnSaveEntries.Visible = true;
            }
        }

        //private void ShowPIInformationByLCId(int LCId)
        //{
        //    pnlDetails.Visible = true;

        //    string sql = $"Exec usp_GetPIInfoByLCIds '{LCId}'";
        //    var dt = unitOfWork.GetDataTableFromSql(sql);

        //    if (dt.Rows.Count > 0)
        //    {
        //        rptPISummary.Visible = true;
        //        rptPISummary.DataSource = dt;
        //        rptPISummary.DataBind();
        //        //SetInitialRowCount();
        //        //divItemInfo.Visible = true;
        //        //lnkbtnSaveEntries.Visible = true;
        //    }
        //    else
        //    {
        //        divItemInfo.Visible = false;
        //        rptPISummary.Visible = false;
        //        lnkbtnSaveEntries.Visible = false;
        //    }
        //}

        protected void rptPISummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                totalGoods = 0;
                grandTotalAmount = 0;

                string PIId = DataBinder.Eval(e.Item.DataItem, "PIId").ToString();

                Repeater rptPIItemInfo = (Repeater)e.Item.FindControl("rptPIItemInfo");
                Label lblNoItemFound = (Label)e.Item.FindControl("lblNoItemFound");

                string sql2 = $"Exec usp_GetPIItemsByPIId '{PIId}'";
                var dt2 = unitOfWork.GetDataTableFromSql(sql2);

                if (dt2.Rows.Count > 0)
                {
                    lblNoItemFound.Visible = false;

                    rptPIItemInfo.DataSource = dt2;
                    rptPIItemInfo.DataBind();

                }
                else
                {
                    lblNoItemFound.Visible = true;

                }

            }
        }

        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            //dt.Columns.Add(new DataColumn("StyleId", typeof(string)));
            dt.Columns.Add(new DataColumn("ColorId", typeof(string)));
            //dt.Columns.Add(new DataColumn("ItemTypeId", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemCategoryId", typeof(string)));
            dt.Columns.Add(new DataColumn("LOTNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));

            dr = dt.NewRow();

            //dr["StyleId"] = string.Empty;
            dr["ColorId"] = string.Empty;
            //dr["ItemTypeId"] = string.Empty;
            dr["ItemId"] = string.Empty;
            dr["ItemCategoryId"] = string.Empty;
            dr["LOTNumber"] = string.Empty;
            dr["Quantity"] = string.Empty;


            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptItemRecEntryInfo.DataSource = dt;
            rptItemRecEntryInfo.DataBind();
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {
                        //DropDownList ddlStyles = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                        DropDownList ddlColors = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        // DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                       DropDownList ddlItems = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlItems");
                      // SetItemAllDropdown(ddlItems, int.Parse(ddlPiNumber.SelectedValue), int.Parse(ddlStyles.SelectedValue), int.Parse(ddlBuyers.SelectedValue));
                        DropDownList ddlItemCategory = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlItemCategory");

                        TextBox tbxLOTNumber = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxLOTNumber");
                        TextBox tbxQty = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxQty");

                        //dtCurrentTable.Rows[rowIndex]["StyleId"] = ddlStyles.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["ColorId"] = ddlColors.SelectedValue;
                        //dtCurrentTable.Rows[rowIndex]["ItemTypeId"] = ddlItemTypes.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["ItemId"] = ddlItems.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["ItemCategoryId"] = ddlItemCategory.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["LOTNumber"] = tbxLOTNumber.Text;
                        dtCurrentTable.Rows[rowIndex]["Quantity"] = tbxQty.Text;


                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            //drCurrentRow["StyleId"] = string.Empty;
                            drCurrentRow["ColorId"] = string.Empty;
                            //drCurrentRow["ItemTypeId"] = string.Empty;
                            drCurrentRow["ItemId"] = string.Empty;
                            drCurrentRow["ItemCategoryId"] = string.Empty;
                            drCurrentRow["LOTNumber"] = string.Empty;
                            drCurrentRow["Quantity"] = string.Empty;

                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptItemRecEntryInfo.DataSource = dtCurrentTable;
                    this.rptItemRecEntryInfo.DataBind();

                }
            }
            this.SetPreviousData();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //DropDownList ddlStyles = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                        DropDownList ddlColors = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        // DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                        DropDownList ddlItems = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlItems");
                        DropDownList ddlItemCategory = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlItemCategory");


                        TextBox tbxLOTNumber = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxLOTNumber");
                        TextBox tbxQty = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxQty");
                        

                        int selectedStyleId = 0;
                        if (ddlStyles.SelectedValue != "")
                        {
                            selectedStyleId = int.Parse(ddlStyles.SelectedValue);
                        }
                        //LoadColorsByStyleId(selectedStyleId);
                        SetColorDropdown(ddlColors, selectedStyleId);
                        ddlColors.SelectedIndex = CommonMethods.MatchDropDownItem(ddlColors, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ColorId"].ToString()) ? "0" : dt.Rows[i]["ColorId"].ToString())));


                 
                        if (String.IsNullOrEmpty(ddlPiNumber.SelectedValue))
                        {
                            SetItemDropdown(ddlItems, int.Parse(ddlStyles.SelectedValue));
                        }
                        else
                        {
                            SetItemAllDropdown(ddlItems, int.Parse(ddlPiNumber.SelectedValue.ToString()), int.Parse(ddlStyles.SelectedValue), int.Parse(ddlBuyers.SelectedValue));
                        }

                        
                        ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ItemId"].ToString()) ? "0" : dt.Rows[i]["ItemId"].ToString())));

                        SetItemCategoryDropdown(ddlItemCategory, selectedStyleId);
                        ddlItemCategory.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItemCategory, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ItemCategoryId"].ToString()) ? "0" : dt.Rows[i]["ItemCategoryId"].ToString())));


                        tbxLOTNumber.Text = dt.Rows[i]["LOTNumber"].ToString();
                        tbxQty.Text = dt.Rows[i]["Quantity"].ToString();

                        rowIndex++;
                    }
                }
            }
        }
        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlStyles.SelectedValue != "")
            {
               // LoadDyeingOrderByStyle(int.Parse(ddlStyles.SelectedValue));
                CommonMethods.LoadYarnAndAccessoriesSupplierDropdown(ddlYarnAndAccssSuppliers, 1, 0);
                //LoadColorsByStyleId(int.Parse(ddlStyles.SelectedValue));
            }
            else
            {
                divItemInfo.Visible = false;
                lnkbtnSaveEntries.Visible = false;
                lnkbtnUpdateEntries.Visible = false;
                //SetInitialRowCount();
                ddlYarnAndAccssSuppliers.Items.Clear();
                //dtColors = null;
            }
            //DropDownList ddlStyle = (DropDownList)sender;
            //DropDownList ddlColors = (DropDownList)((RepeaterItem)ddlStyle.NamingContainer).FindControl("ddlColors");
            //DropDownList ddlItems = (DropDownList)((RepeaterItem)ddlStyle.NamingContainer).FindControl("ddlItems");
            //if (ddlStyle.SelectedValue == "")
            //{
            //    ddlItems.Items.Clear();
            //    ddlColors.Items.Clear();

            //}
            //else
            //{
            //    SetColorDropdown(ddlColors, int.Parse(ddlStyle.SelectedValue));
            //    SetItemDropdown(ddlItems, int.Parse(ddlStyle.SelectedValue));
            //}
        }
        private void LoadDyeingOrderByStyle(int styleId)
        {
            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetDyeingOrderByStyle {styleId}");
            ddlDyeingOrder.Items.Add(new ListItem("", ""));
            ddlDyeingOrder.DataSource = dt;
            ddlDyeingOrder.DataTextField = "DyeingOrderNumber";
            ddlDyeingOrder.DataValueField = "DyeingOrderId";
            ddlDyeingOrder.DataBind();
            ddlDyeingOrder.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlDyeingOrder.SelectedIndex = 0;
        }
        protected void rptPIItemInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                var qty = DataBinder.Eval(e.Item.DataItem, "Quantity").ToString();
                var totalAmount = DataBinder.Eval(e.Item.DataItem, "TotalAmount").ToString();

                totalGoods += decimal.Parse(qty);
                grandTotalAmount += decimal.Parse(totalAmount);


            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {

                Label lblTotalQuantity = (Label)e.Item.FindControl("lblTotalQuantity");
                Label lblGrandTotalAmount = (Label)e.Item.FindControl("lblGrandTotalAmount");


                lblTotalQuantity.Text = totalGoods.ToString();
                lblGrandTotalAmount.Text = grandTotalAmount.ToString();

            }

        }

        protected void SetColorDropdown(DropDownList ddlColor, int styleId)
        {
            var dtColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetStyleColorsByStyleId {styleId}");
            //var dtColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetStyleColorsByStyleId {styleId}");
            ddlColor.Items.Add(new ListItem("", ""));
            ddlColor.DataSource = dtColors;
            ddlColor.DataTextField = "ColorDescription";
            ddlColor.DataValueField = "BuyerColorId";
            ddlColor.DataBind();
            ddlColor.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlColor.SelectedIndex = 0;
        }

        protected void SetItemDropdown(DropDownList ddlItem, int styleId)
        {
            //var lcId = int.Parse(ddlLCNumber.SelectedValue);
            var dtItems = unitOfWork.GetDataTableFromSql($"SELECT Id as ItemId,ItemName FROM YarnAccessoriesItems WHERE ItemTypeId = 2 AND IsActive = 1"); //item type id  2 = yarn,
            ddlItem.Items.Add(new ListItem("", ""));
            ddlItem.DataSource = dtItems;
            ddlItem.DataTextField = "ItemName";
            ddlItem.DataValueField = "ItemId";
            ddlItem.DataBind();
            ddlItem.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlItem.SelectedIndex = 0;


        }

        protected void SetItemAllDropdown(DropDownList ddlItem, int PiNumber, int styleId, int BuyerId )
        {
            var dtItems = unitOfWork.GetDataTableFromSql($"Select distinct YI.Id ItemId,YI.ItemName from YarnAccessoriesItems YI inner join PIItems PIIs on YI.Id = PIIs.ItemId inner join PIInfo PN on PIIs.PIId = PN.Id where YI.ItemTypeId = 2 AND YI.IsActive = 1 AND PN.Id =   { PiNumber } and PIIs.StyleId =  { styleId } and PN.BuyerId =  { BuyerId } "); //and PN.BuyerId = 28 and PIIs.StyleId = 11058 item type id  2 = yarn,
            ddlItem.Items.Add(new ListItem("", ""));
            ddlItem.DataSource = dtItems;
            ddlItem.DataTextField = "ItemName";
            ddlItem.DataValueField = "ItemId";
            ddlItem.DataBind();
            ddlItem.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlItem.SelectedIndex = 0;

        }

        protected void SetItemCategoryDropdown(DropDownList ddlItemCategory, int styleId)
        {
            var dtColors = unitOfWork.GetDataTableFromSql($"SELECT Id as ItemCategoryId, CategoryName FROM ItemCategory");
            //var dtColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetStyleColorsByStyleId {styleId}");
            ddlItemCategory.Items.Add(new ListItem("", ""));
            ddlItemCategory.DataSource = dtColors;
            ddlItemCategory.DataTextField = "CategoryName";
            ddlItemCategory.DataValueField = "ItemCategoryId";
            ddlItemCategory.DataBind();
            ddlItemCategory.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlItemCategory.SelectedIndex = 0;
        }

        protected void rptItemRecEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
              

                var ddlColors = (DropDownList)e.Item.FindControl("ddlColors");
                var ddlItems = (DropDownList)e.Item.FindControl("ddlItems");
                var ddlItemCategory = (DropDownList)e.Item.FindControl("ddlItemCategory");
                SetColorDropdown(ddlColors, int.Parse(ddlStyles.SelectedValue));
                // SetItemDropdown(ddlItems, int.Parse(ddlStyles.SelectedValue));

                if (String.IsNullOrEmpty(ddlPiNumber.SelectedValue))
                {
                    SetItemDropdown(ddlItems, int.Parse(ddlStyles.SelectedValue));
                }
                else
                {
                    SetItemAllDropdown(ddlItems, int.Parse(ddlPiNumber.SelectedValue.ToString()), int.Parse(ddlStyles.SelectedValue), int.Parse(ddlBuyers.SelectedValue));
                }

                
                SetItemCategoryDropdown(ddlItemCategory, int.Parse(ddlStyles.SelectedValue));
                //var lcId = ddlLCNumber.SelectedValue;
                //if (lcId != "")
                //{
                //    SetStyleDropdown(ddlStyles, int.Parse(lcId));
                //}
            }
        }

        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(tbxReceiveDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter receive date.')", true);
            }
            else if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a style.')", true);
            }
            else if (ddlStore.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a store.')", true);
            }
            else if (ddlMerchandiser.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a merchandiser.')", true);
            }
            else if (ddlPurchaseType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select purchase type.')", true);
            }
            else if (ddlYarnAndAccssSuppliers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a supplier.')", true);
            }
            else if (string.IsNullOrEmpty(tbxChalanNo.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter Chalan Number')", true);
            }
            //else if (int.Parse(ddlYarnAndAccssSuppliers.SelectedValue) == 26 && ddlDyeingOrder.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter Dyeing Order Number')", true);
            //}

            //else if (int.Parse(ddlYarnAndAccssSuppliers.SelectedValue) != 26 && string.IsNullOrEmpty(tbxInvoiceNumber.Text))
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter Invoice Number')", true);
            //}
            else if (ddlPiNumber.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter PINumber.')", true);
            }
           
            else
            {
                try
                {
                    var yarnReceivedAtStore = new YarnReceivedAtStore()
                    {
                        ReceiveDate = Convert.ToDateTime(tbxReceiveDate.Text),
                        BuyerId = int.Parse(ddlBuyers.SelectedValue),
                        StyleId = int.Parse(ddlStyles.SelectedValue),
                        SupplierId = int.Parse(ddlYarnAndAccssSuppliers.SelectedValue),
                        PurchaseType = int.Parse(ddlPurchaseType.SelectedValue),
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName,
                        StoreId = int.Parse(ddlStore.SelectedValue),
                        PiNumber = int.Parse(ddlPiNumber.SelectedValue.ToString()),
                        InvoiceNumber = tbxInvoiceNumber.Text,
                        MerchandiserId = int.Parse(ddlMerchandiser.SelectedValue),
                        ChalanNo = tbxChalanNo.Text,
                        LCNumber = tbxLCNumber.Text,
                        DyeingOrderId = int.Parse(ddlDyeingOrder.SelectedValue == "" ? "0" : ddlDyeingOrder.SelectedValue),
                        //DyeingOrderNumber = tbxDyeingOrderNumber.Text,
                    };
                   // Int64 YarnReceivedID = 0;

                    //DataTable YarnReceivedIdbyPI = unitOfWork.GetDataTableFromSql($"Select Top 1 YarnReceivedId from YarnReceivedAtStore where PiNumber =   { int.Parse(ddlPiNumber.SelectedValue.ToString()) } AND StyleId = {int.Parse(ddlStyles.SelectedValue)} order by YarnReceivedId desc ");
                    //if (YarnReceivedIdbyPI.Rows.Count > 0)
                    //{
                    //    if (YarnReceivedIdbyPI.Rows[0]["YarnReceivedId"] != null)
                    //    {
                    //        YarnReceivedID = Convert.ToInt64(YarnReceivedIdbyPI.Rows[0]["YarnReceivedId"].ToString());
                    //    }
                    //}

                    //if (ddlLCNumber.SelectedValue != "")
                    //{
                    //    yarnReceivedAtStore.LCId = int.Parse(ddlLCNumber.SelectedValue);
                    //}
                    if (ddlDyeingOrder.SelectedValue != "")
                    {
                        yarnReceivedAtStore.DyeingOrderId = int.Parse(ddlDyeingOrder.SelectedValue);
                    }


                    List<YarnReceivedAtStoreItems> listItems = new List<YarnReceivedAtStoreItems>();
                    

                    
                   

                    for (int rowIndex = 0; rowIndex < rptItemRecEntryInfo.Items.Count; rowIndex++)
                    {
                        DropDownList ddlColors = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        DropDownList ddlItems = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlItems");
                        DropDownList ddlItemCategory = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlItemCategory");



                        int selectedColorId = 0;
                        if (ddlColors.SelectedValue != "")
                        {
                            selectedColorId = int.Parse(ddlColors.SelectedValue);
                        }

                        int selectedItemCatId = 0;
                        if (ddlItemCategory.SelectedValue != "")
                        {
                            selectedItemCatId = int.Parse(ddlItemCategory.SelectedValue);
                        }

                        TextBox tbxLOTNumber = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxLOTNumber");
                        TextBox tbxQty = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxQty");

                        

                        if (ddlColors.SelectedValue != "" && ddlItems.SelectedValue != "" && !string.IsNullOrEmpty(tbxQty.Text))
                        {
                            var styleId = int.Parse(ddlStyles.SelectedValue);
                            var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().Get(x => x.Id == styleId).FirstOrDefault();
                            Decimal PIqntyFromPIItem = 0;

                            

                            //if (YarnReceivedIdbyPI.Rows.Count < 1 || YarnReceivedIdbyPI.Rows[0]["YarnReceivedId"] == null)
                            //{
                            //    DataTable GetPIQntyfromPiitems = unitOfWork.GetDataTableFromSql($"Select Quantity from PIItems where PIId = { int.Parse(ddlPiNumber.SelectedValue.ToString()) } AND StyleId = {int.Parse(ddlStyles.SelectedValue)} AND ItemId = {int.Parse(ddlItems.SelectedValue)} ");

                            //    if (GetPIQntyfromPiitems.Rows[0]["Quantity"] != null)
                            //    {
                            //        PIqntyFromPIItem = Convert.ToDecimal(GetPIQntyfromPiitems.Rows[0]["Quantity"].ToString());
                            //    }
                            //}
                            //else
                            //{
                            //    DataTable GetPIQntyfromPiitems = unitOfWork.GetDataTableFromSql($"Select top 1 (PIQnty - (select sum(ReceivedQty) from YarnReceivedAtStoreItems where YarnReceivedId = {YarnReceivedID} AND ItemId = {int.Parse(ddlItems.SelectedValue)})) as ReceivedQty from YarnReceivedAtStoreItems where YarnReceivedId = {YarnReceivedID} and ItemId = {int.Parse(ddlItems.SelectedValue)} order by Id desc");

                            //    if (GetPIQntyfromPiitems.Rows.Count > 0)
                            //    {
                            //        if (String.IsNullOrEmpty(GetPIQntyfromPiitems.Rows[0]["ReceivedQty"].ToString()))
                            //        {
                            //            PIqntyFromPIItem = Convert.ToDecimal(GetPIQntyfromPiitems.Rows[0]["ReceivedQty"].ToString());
                            //        }

                            //    }
                            //    else
                            //    {
                            //        DataTable GetPIQntyfromPiitemsnotinstore = unitOfWork.GetDataTableFromSql($"Select Quantity from PIItems where PIId = { int.Parse(ddlPiNumber.SelectedValue.ToString()) } AND StyleId = {int.Parse(ddlStyles.SelectedValue)} AND ItemId = {int.Parse(ddlItems.SelectedValue)} ");
                            //        if (GetPIQntyfromPiitemsnotinstore.Rows[0]["Quantity"] != null)
                            //        {
                            //            PIqntyFromPIItem = Convert.ToDecimal(GetPIQntyfromPiitemsnotinstore.Rows[0]["Quantity"].ToString());
                            //        }
                            //    }


                            //}

                            var item = new YarnReceivedAtStoreItems()
                                {
                                    YarnReceivedId = yarnReceivedAtStore.YarnReceivedId,
                                    ColorId = selectedColorId,
                                    ItemId = int.Parse(ddlItems.SelectedValue),
                                    ItemCatId = Convert.ToInt64(selectedItemCatId),
                                    LOTNumber = tbxLOTNumber.Text,
                                    ReceivedQty = Convert.ToDecimal(tbxQty.Text),
                                    ReceivedQtyInKg = (decimal)0.453592 * Convert.ToDecimal(tbxQty.Text),
                                    CreateDate = DateTime.Now,
                                    ChalanNo = tbxChalanNo.Text,
                                    CreatedBy = CommonMethods.SessionInfo.UserName,
                                    PIQnty = PIqntyFromPIItem
                                };
                               
                            listItems.Add(item);
                            }
                            
                        }
                    //Linq query 
                    //var a = listItems.GroupBy(u => u.ItemId).Select(p => new YarnReceivedAtStoreItems
                    //{
                    //    ItemId = p.FirstOrDefault().ItemId,
                    //    ReceivedQty = p.Sum(b => b.ReceivedQty)
                    //});
                    //Linq query
                    if (listItems.Count() > 0)
                    {
                        yarnReceivedAtStore.YarnReceivedAtStoreItems = listItems;
                        unitOfWork.GenericRepositories<YarnReceivedAtStore>().Insert(yarnReceivedAtStore);
                        unitOfWork.Save();

                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ReceiveYarnAtStoreV2.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter item(s) info correctly.')", true);
                    }
                    
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
        }

        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(tbxReceiveDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter receive date.')", true);
            }
            else if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a style.')", true);
            }
            else if (ddlStore.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a store.')", true);
            }
            else if (ddlMerchandiser.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a merchandiser.')", true);
            }
            else if (ddlPurchaseType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select purchase type.')", true);
            }
            else if (ddlYarnAndAccssSuppliers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a supplier.')", true);
            }
            //else if (int.Parse(ddlYarnAndAccssSuppliers.SelectedValue) == 26 && ddlDyeingOrder.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter Dyeing Order Number')", true);
            //}
            //else if (int.Parse(ddlYarnAndAccssSuppliers.SelectedValue) != 26 && string.IsNullOrEmpty(tbxInvoiceNumber.Text))
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter Invoice Number')", true);
            //}
            else if (ddlPiNumber.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter PINumber.')", true);
            }
            else
            {
                try
                {

                    var recInfo = unitOfWork.GenericRepositories<YarnReceivedAtStore>().GetByID(ReceivedId);
                    var recItems = unitOfWork.GenericRepositories<YarnReceivedAtStoreItems>().Get(x => x.YarnReceivedId == ReceivedId).ToList();
                    foreach (var item in recItems)
                    {
                        unitOfWork.GenericRepositories<YarnReceivedAtStoreItems>().Delete(item);
                    }

                    recInfo.ReceiveDate = Convert.ToDateTime(tbxReceiveDate.Text);
                    recInfo.SupplierId = int.Parse(ddlYarnAndAccssSuppliers.SelectedValue);
                    recInfo.PurchaseType = int.Parse(ddlPurchaseType.SelectedValue);
                    recInfo.UpdateDate = DateTime.Now;
                    recInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                    recInfo.StoreId = int.Parse(ddlStore.SelectedValue);
                    recInfo.PiNumber = int.Parse(ddlPiNumber.SelectedValue);
                    recInfo.InvoiceNumber = tbxInvoiceNumber.Text;
                    recInfo.MerchandiserId = int.Parse(ddlMerchandiser.SelectedValue);
                    recInfo.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                    recInfo.StyleId = int.Parse(ddlStyles.SelectedValue);
                    recInfo.ChalanNo = tbxChalanNo.Text;
                    recInfo.LCNumber = tbxLCNumber.Text;
                    recInfo.DyeingOrderNumber = tbxDyeingOrderNumber.Text;
                    recInfo.DyeingOrderId = int.Parse(ddlDyeingOrder.SelectedValue == "" ? "0" : ddlDyeingOrder.SelectedValue);

                    if (ddlLCNumber.SelectedValue != "")
                    {
                        recInfo.LCId = int.Parse(ddlLCNumber.SelectedValue);
                    }
                    //if (ddlDyeingOrder.SelectedValue != "")
                    //{
                    //    recInfo.DyeingOrderId = int.Parse(ddlDyeingOrder.SelectedValue);
                    //}                  


                    List<YarnReceivedAtStoreItems> listItems = new List<YarnReceivedAtStoreItems>();
                    for (int rowIndex = 0; rowIndex < rptItemRecEntryInfo.Items.Count; rowIndex++)
                    {
                        DropDownList ddlColors = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        DropDownList ddlItems = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlItems");
                        DropDownList ddlItemCategory = (DropDownList)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("ddlItemCategory");

                        int selectedColorId = 0;
                        if (ddlColors.SelectedValue != "")
                        {
                            selectedColorId = int.Parse(ddlColors.SelectedValue);
                        }

                        TextBox tbxLOTNumber = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxLOTNumber");
                        TextBox tbxQty = (TextBox)this.rptItemRecEntryInfo.Items[rowIndex].FindControl("tbxQty");

                        if (ddlColors.SelectedValue != "" && ddlItems.SelectedValue != "" && !string.IsNullOrEmpty(tbxQty.Text))
                        {
                            var styleId = int.Parse(ddlStyles.SelectedValue);
                            var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().Get(x => x.Id == styleId).FirstOrDefault();
                            var item = new YarnReceivedAtStoreItems()
                            {
                                YarnReceivedId = recInfo.YarnReceivedId,
                                //StyleId = styleId,
                                ColorId = selectedColorId,
                                //BuyerId = styleInfo.BuyerId,
                                ItemId = int.Parse(ddlItems.SelectedValue),
                                ItemCatId = int.Parse(ddlItemCategory.SelectedValue),
                                LOTNumber = tbxLOTNumber.Text,
                                ReceivedQty = Convert.ToDecimal(tbxQty.Text),
                                ReceivedQtyInKg = (decimal)0.453592 * Convert.ToDecimal(tbxQty.Text),
                                ChalanNo = tbxChalanNo.Text,
                                CreateDate = DateTime.Now,
                                CreatedBy = CommonMethods.SessionInfo.UserName
                            };

                            listItems.Add(item);
                        }
                    }

                    if (listItems.Count() > 0)
                    {
                        recInfo.YarnReceivedAtStoreItems = listItems;
                        unitOfWork.GenericRepositories<YarnReceivedAtStore>().Update(recInfo);
                        unitOfWork.Save();

                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewReceivedYarnAtStoreV2.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter item(s) info correctly.')", true);
                    }  
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
        }

        protected void ddlBuyer_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DropDownList ddlBuyers = (DropDownList)sender;
            //DropDownList ddlStyle = (DropDownList)((RepeaterItem)ddlBuyers.NamingContainer).FindControl("ddlStyles");
            //DropDownList ddlColors = (DropDownList)((RepeaterItem)ddlBuyers.NamingContainer).FindControl("ddlColors");
            //DropDownList ddlItems = (DropDownList)((RepeaterItem)ddlBuyers.NamingContainer).FindControl("ddlItems");
            if (ddlBuyers.SelectedValue == "")
            {
                ddlStyles.Items.Clear();
            }
            else
            {
                BindStylesByBuyer(ddlStyles, int.Parse(ddlBuyers.SelectedValue));
            }

           
        }

        private void LoadPiInfo()
        {
            try
            {
                UnitOfWork objUnitOfWork = new UnitOfWork();
                DataTable objdataTable = objUnitOfWork.GetPIinfoByStyleByuerSupplier(Convert.ToInt32("0" + ddlBuyers.SelectedValue), Convert.ToInt32("0" + ddlStyles.SelectedValue), Convert.ToInt32("0" + ddlYarnAndAccssSuppliers.SelectedValue));
                if (objdataTable.Rows.Count > 0)
                {
                    ddlPiNumber.DataSource = objdataTable;
                    ddlPiNumber.DataTextField = "PINumber";
                    ddlPiNumber.DataValueField = "PIId";
                    ddlPiNumber.DataBind();
                    ddlPiNumber.Items.Insert(0, new ListItem("---Select---", string.Empty));
                    ddlPiNumber.SelectedIndex = 0;
                }
                else
                {

                }

            }
            catch (Exception ex)
            {


            }
        }

        private void BindStylesByBuyer(DropDownList ddlStyles, int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void ddlDyeingOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetInitialRowCount();
            ShowItemEntryInfo();
        }
    }
}