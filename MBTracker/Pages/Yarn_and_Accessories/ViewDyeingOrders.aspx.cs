﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Yarn_and_Accessories
{
    public partial class ViewDyeingOrders : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //EnableDisableEditButton();
                //EnableDisableDeleteButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                if (Request.QueryString["styleId"] != null)
                {
                    var styleId = int.Parse(Request.QueryString["styleId"]);
                    pnlEntry.Visible = false;

                }
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteDyeingOrder(doId);
                }
            }
        }

        private void DeleteDyeingOrder(int doId)
        {
            var doInfo = unitOfWork.GenericRepositories<DyeingOrder>().GetByID(doId);
            var lstItems = unitOfWork.GenericRepositories<DyeingOrderItems>().Get(x => x.DyeingOrderId == doId).ToList();
            foreach (var item in lstItems)
            {
                unitOfWork.GenericRepositories<DyeingOrderItems>().Delete(item);
            }
            unitOfWork.GenericRepositories<DyeingOrder>().Delete(doInfo);
            unitOfWork.Save();
            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
            LoadData();
        }

        int doId
        {
            set { ViewState["id"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["id"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            else
            {
                ddlStyles.Items.Clear();
            }
            pnlSummary.Visible = false;
            lblSummryNotFound.Visible = false;
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        private void EnableDisableDeleteButton()
        {
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewDyeingOrders", 2, 1);
        }
        private void EnableDisableEditButton()
        {
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewDyeingOrders", 1, 1);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            try
            {
                var styleId = 0;
                var orderDate = "";
                if (ddlStyles.SelectedValue == "" && string.IsNullOrEmpty(tbxDODate.Text))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select buyer with style or dyeing order date.')", true);
                }
                else
                {
                    if (ddlStyles.SelectedValue != "")
                        styleId = int.Parse(ddlStyles.SelectedValue);
                    if (!string.IsNullOrEmpty(tbxDODate.Text))
                        orderDate = DateTime.Parse(tbxDODate.Text).ToString("yyyy-MM-dd");
                    var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetDyeingOrderList {styleId},'{orderDate}'");
                    if (dt.Rows.Count > 0)
                    {
                        dt = dt.CheckEditDeletePermission(CommonMethods.SessionInfo.RoleId, "ViewDyeingOrders");
                        rptSummary.DataSource = dt;
                        rptSummary.DataBind();
                        pnlSummary.Visible = true;
                        rptSummary.Visible = true;
                        lblSummryNotFound.Visible = false;
                        lblNoDetails.Visible = false;
                    }
                    else
                    {
                        pnlSummary.Visible = false;
                        rptSummary.Visible = false;
                        pnlDetails.Visible = false;
                        rptDetails.Visible = false;
                        lblSummryNotFound.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            var id = int.Parse(e.CommandArgument.ToString());
            Response.Redirect("EnterDyeingOrder.aspx?doId=" + Tools.UrlEncode(id + ""));
        }


        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            doId = int.Parse(e.CommandArgument.ToString());
            if (doId != 0)
            {
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
            }
            else
            {
                var title = "Error";
                var msg = "Sorry, you can not delete it.<br />Something went wrong.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            }
        }


        protected void lnkbtnDetails_Command(object sender, CommandEventArgs e)
        {
            var doId = int.Parse(e.CommandArgument.ToString());
            var dtDetails = unitOfWork.GetDataTableFromSql($"EXEC usp_GetDyeingOrderDetailsByDyeingOrderId {doId}");

            if (dtDetails.Rows.Count > 0)
            {
                rptDetails.DataSource = dtDetails;
                rptDetails.DataBind();
                pnlDetails.Visible = true;
                rptDetails.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(0)", true);
                lblNoDetails.Visible = false;
                var buyer = dtDetails.Rows[0]["BuyerName"].ToString();
                var styleName = dtDetails.Rows[0]["StyleName"].ToString();
                var dyeingOrderNumber = dtDetails.Rows[0]["DyeingOrderNumber"].ToString();
                lblDetails.Text = $"Dyeing Order Details Buyer: {buyer}, Style: {styleName}, Dyeing Order: {dyeingOrderNumber}";
            }
            else
            {
                pnlDetails.Visible = false;
                rptDetails.Visible = false;
                lblNoDetails.Visible = true;
            }            
        }
    }
}