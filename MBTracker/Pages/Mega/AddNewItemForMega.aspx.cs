﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Mega.BasicSetUp
{
    public partial class AddNewItemForMega : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableDeleteButton();
                BindData();
            }
        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");

            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("AddNewItem", 2, 1);

        }


        protected void BindData()
        {

            DataTable dt = unitOfWork.GetDataTableFromSql("SELECT Id, YarnName FROM YarnListForMega");

            if (dt.Rows.Count > 0)
            {
                rptYarnForMega.DataSource = dt;
                rptYarnForMega.DataBind();
            }
            else
            {
                rptYarnForMega.DataSource = null;
                rptYarnForMega.DataBind();
            }
        }

        int YarnId
        {
            set { ViewState["YarnId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["YarnId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void PopulateItemInfo(int YarnId)
        {
            var item = unitOfWork.GenericRepositories<YarnListForMega>().GetByID(YarnId);
            if (item != null)
            {
                tbxYarnName.Text = item.YarnName;
                roleActionTitle.Text = "Update Item";
                btnSubmit.Visible = false;
                btnUpdate.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "onEdit()", true);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Invalid item id.')", true);
            }
        }

        protected void rptYarnForMega_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {

                YarnId = Convert.ToInt32(e.CommandArgument.ToString());
                PopulateItemInfo(YarnId);
            }
            else if (e.CommandName == "Delete")
            {
                try
                {
                    YarnId = Convert.ToInt32(e.CommandArgument.ToString());
                    var result = unitOfWork.GenericRepositories<YarnListForMega>().GetByID(YarnId);
                    if (result != null)
                    {
                        unitOfWork.GenericRepositories<YarnListForMega>().Delete(result);
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Invalid item id.')", true);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Error - something went wrong.')", true);
                }

                BindData();

              
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            string actionTaker = CommonMethods.SessionInfo.UserName;
            var yarnName = tbxYarnName.Text.Trim();
            

            var isExist = unitOfWork.GenericRepositories<YarnListForMega>().IsExist(x => x.YarnName == yarnName);

            if (!isExist)
            {
                string user = CommonMethods.SessionInfo.UserName;
                var newitem = new YarnListForMega()
                {
                    YarnName = yarnName,
                    CreatedBy = user,
                    CreatedDate = DateTime.Now
                };
                unitOfWork.GenericRepositories<YarnListForMega>().Insert(newitem);
                unitOfWork.Save();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('item already exists.')", true);
            }

            ClearAll();
            BindData();

        }

        protected void ClearAll()
        {
            tbxYarnName.Text = "";
            
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string actionTaker = CommonMethods.SessionInfo.UserName;

            var item = unitOfWork.GenericRepositories<YarnListForMega>().GetByID(YarnId);
            if (item != null)
            {
                item.YarnName = tbxYarnName.Text.Trim();
                item.UpdatedBy = actionTaker;
                item.UpdatedDate = DateTime.Now;

                unitOfWork.GenericRepositories<YarnListForMega>().Update(item);
                unitOfWork.Save();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Item update was failed.')", true);
            }

           
            ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('AddNewItemForMega.aspx');", true);
        }

    }
}