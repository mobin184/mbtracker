﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.DAL;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Mega
{
    public partial class YarnBookingInfo : System.Web.UI.Page
    {
        DatabaseManager dm = new DatabaseManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // EnableDisableDeleteButton();
                CommonMethods.LoadYarnForMegaDropdown(ddlItems, 1, 0);
                CommonMethods.LoadDropdown(ddlCategory, "ItemCategory", 1, 0);
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteBooking(ItemId);
                }
            }
        }


        protected void ddlYarn_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlItems.SelectedValue != "")
            {
                BindData(Convert.ToInt32(ddlItems.SelectedValue));
            }
            else
            {
                pnlColors.Visible = false;
            }
        }

        private void BindData(int itemId)
        {
            var dt = LoadBookingByItem(itemId);
            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                pnlColors.Visible = true;
            }
            else
            {
                pnlColors.Visible = false;
            }

        }

        public DataTable LoadBookingByItem(int itemId)
        {
            dm.AddParameteres("@ItemId", itemId);
            return dm.ExecuteQuery("[usp_GetBookingByYarnForMega]");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlItems.SelectedValue != "")
                {
                    var itemId = int.Parse(ddlItems.SelectedValue);
                    var BookingBy = tbxBookingBy.Text.Trim();
                    var Remarks = tbxRemarks.Text.Trim();
                    if (tbxBookingQuantity.Text.Trim() != "" && ddlCategory.SelectedValue != "")
                    {
                        var bookingQuantity = decimal.Parse(tbxBookingQuantity.Text.Trim());
                        var categoryId = int.Parse(ddlCategory.SelectedValue);
                        var megayarnbooking = new MegaYarnBookingByUser()
                            {
                                YarnId = itemId,
                                CategoryId = categoryId,
                                BookingQuantity = bookingQuantity,
                                BookingBy = BookingBy,
                                Remarks = Remarks,
                                IsActive = 1,
                                CreatedBy = CommonMethods.SessionInfo.UserName,
                                CreatedDate = DateTime.Now
                            };
                            unitOfWork.GenericRepositories<MegaYarnBookingByUser>().Insert(megayarnbooking);
                            unitOfWork.Save();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                            BindData(Convert.ToInt32(ddlItems.SelectedValue));
                            ClearFormData();
                       
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter Quantity And Category.')", true);
                    }
                    
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a Yarn.')", true);
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private void ClearFormData()
        {
            tbxBookingQuantity.Text = string.Empty;
            tbxRemarks.Text = string.Empty;
        }
        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int ItemId = Convert.ToInt32(e.CommandArgument.ToString());
                //PopulateColor(buyerColorId);
            }
            else if (e.CommandName == "Delete")
            {
                ItemId = Convert.ToInt32(e.CommandArgument.ToString());

                //var colorCannotBeDeleted = unitOfWork.GenericRepositories<viewBuyerColorsCanNotBeDeleted>().GetByID(ColorId);
                if (ItemId >0)
                {
                    var title = "Warning";
                    var msg = "Are you sure you want to delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                }
                else
                {
                    var title = "Warning";
                    var msg = "Sorry, you can not delete it.<br />There was some dependencies of this Item.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                }
            }
        }

        int ItemId
        {
            set { ViewState["itemId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["itemId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        private void DeleteBooking(int ItemId)
        {
            try
            {
                var bookingyarn = unitOfWork.GenericRepositories<MegaYarnBookingByUser>().GetByID(ItemId);
                unitOfWork.GenericRepositories<MegaYarnBookingByUser>().Delete(bookingyarn);
                unitOfWork.Save();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
                BindData(Convert.ToInt32(ddlItems.SelectedValue));
                ClearFormData();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }


    }
}