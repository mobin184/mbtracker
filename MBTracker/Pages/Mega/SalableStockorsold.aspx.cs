﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Mega
{
    
    public partial class SalableStockorsold : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        int initialLoadForUpdate = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetInitialRowCount();
            }
        }

        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var ddlItems = (DropDownList)e.Item.FindControl("ddlItems");
                var ddlCategory = (DropDownList)e.Item.FindControl("ddlCategory");
                var lblAvailableBalance = (Label)e.Item.FindControl("lblAvailableBalance");
                var tbxRemarks = (TextBox)e.Item.FindControl("tbxRemarks");
                var tbxQuantity = (TextBox)e.Item.FindControl("tbxQuantity");

                CommonMethods.LoadYarnForMegaDropdown(ddlItems, 1, 0);
                CommonMethods.LoadDropdown(ddlCategory, "ItemCategory", 1, 0);
                //ddlItems.SelectedIndex = CommonMethods.LoadYarnAndAccessoriesItemsDropdown(ddlItems, 1, 0);
                tbxQuantity.Text = DataBinder.Eval(e.Item.DataItem, "Quantity").ToString();
                tbxRemarks.Text = DataBinder.Eval(e.Item.DataItem, "Remarks").ToString();

                if (initialLoadForUpdate > 0)
                {
                    var itemId = DataBinder.Eval(e.Item.DataItem, "ItemId").ToString();
                    var categoryId = DataBinder.Eval(e.Item.DataItem, "CategoryId").ToString();
                    ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse(string.IsNullOrEmpty(itemId) ? "0" : itemId));
                    ddlCategory.SelectedIndex = CommonMethods.MatchDropDownItem(ddlCategory, int.Parse(string.IsNullOrEmpty(categoryId) ? "0" : categoryId));
                }

            }
        }

        protected void btnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {

                List<YarnWisePhysicalStoreBalanceForMega> YarnWiseBalanceForMegaInsert = new List<YarnWisePhysicalStoreBalanceForMega>();
                // List<YarnWiseBalanceForMega> YarnWiseBalanceForMegaUpdate = new List<YarnWiseBalanceForMega>();

                var ddlItems = (DropDownList)rptEntryInfo.Items[0].FindControl("ddlItems");
                var ddlCategory = (DropDownList)rptEntryInfo.Items[0].FindControl("ddlCategory");
                var lblAvailableBalance = (Label)rptEntryInfo.Items[0].FindControl("lblAvailableBalance");
                var tbxRemarks = (TextBox)rptEntryInfo.Items[0].FindControl("tbxRemarks");
                var tbxQuantity = (TextBox)rptEntryInfo.Items[0].FindControl("tbxQuantity");

                if (ddlItems.SelectedValue != "" && ddlCategory.SelectedValue != "" && tbxQuantity.Text != "")
                {
                    var ItemId = int.Parse(ddlItems.SelectedValue);
                    var CategoryId = int.Parse(ddlCategory.SelectedValue);
                    //var yarnwiseStoreBalance = new YarnWiseBalanceForMega()
                    //{
                    //    YarnId = int.Parse(ddlItems.SelectedValue),
                    //    Quantity = decimal.Parse(tbxQuantity.Text.ToString()),
                    //    Remarks = tbxRemarks.Text
                    //};
                    //YarnWiseBalanceForMegaInsert.Add(yarnwiseStoreBalance);

                    var availableBalance = decimal.Parse(unitOfWork.GetSingleStringValue($"EXEC usp_GetAvailablePhysicalBalanceForMegaStore {ItemId},{CategoryId}"));

                        var existingEntity = unitOfWork.GenericRepositories<YarnWisePhysicalStoreBalanceForMega>().Get(x => x.YarnId == ItemId && x.CategoryId == CategoryId).FirstOrDefault();

                        if (existingEntity != null)
                        {
                            // Update the properties of the existing entity
                            existingEntity.Quantity = (availableBalance - decimal.Parse(tbxQuantity.Text.ToString()));
                            existingEntity.Remarks = tbxRemarks.Text;

                            // Save the changes to the database
                            unitOfWork.Save();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error(' Can not Saved.')", true);
                        }


                    unitOfWork.Save();
                }

                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);

                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('SalableStockorsold.aspx');", true);

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }

        }

        protected void SelectedIndexChanged(object sender, EventArgs e)
        {
            var ddl = (DropDownList)sender;

            var ddlItems = (DropDownList)ddl.NamingContainer.FindControl("ddlItems");
            var ddlCategory = (DropDownList)ddl.NamingContainer.FindControl("ddlCategory");
            var lblAvailableBalance = (Label)ddl.NamingContainer.FindControl("lblAvailableBalance");
            var lblBookingBalanceBalance = (Label)ddl.NamingContainer.FindControl("lblBookingBalanceBalance");
            
             ShowAvailableBalance(ddlItems, ddlCategory, lblAvailableBalance, lblBookingBalanceBalance);
        }

        private void ShowAvailableBalance(DropDownList ddlItems, DropDownList ddlCategory, Label lblAB, Label lblBB)
        {
            if (ddlItems.SelectedValue != "" && ddlCategory.SelectedValue != "")
            {

                var itemId = !string.IsNullOrEmpty(ddlItems.SelectedValue) ? int.Parse(ddlItems.SelectedValue) : 0;
                var categoryId = !string.IsNullOrEmpty(ddlCategory.SelectedValue) ? int.Parse(ddlCategory.SelectedValue) : 0;
                var availableBalance = decimal.Parse(unitOfWork.GetSingleStringValue($"EXEC usp_GetAvailablePhysicalBalanceForMegaStore {itemId},{categoryId}"));

                lblAB.Text = availableBalance + "";
                var availableBookingBalance = decimal.Parse(unitOfWork.GetSingleStringValue($"EXEC usp_GetBookingBalanceForMegaSale {itemId},{categoryId}"));

                lblBB.Text = availableBookingBalance + "";
            }
            else
            {
                lblAB.Text = "";
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please fillup all field')", true);
            }
        }


        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));
            dt.Columns.Add(new DataColumn("CategoryId", typeof(string)));
            dt.Columns.Add(new DataColumn("AvailableBalance", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("Remarks", typeof(string)));
            dr = dt.NewRow();

            dr["ItemId"] = string.Empty;
            dr["CategoryId"] = string.Empty;
            dr["AvailableBalance"] = string.Empty;
            dr["Quantity"] = string.Empty;
            dr["Remarks"] = string.Empty;
            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;

            rptEntryInfo.DataSource = dt;
            rptEntryInfo.DataBind();
        }

    }
}