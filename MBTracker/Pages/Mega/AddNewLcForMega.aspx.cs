﻿using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using MBTracker.EF;
using Repositories;

namespace MBTracker.Pages.Mega
{
    public partial class AddNewLcForMega : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            string actionTaker = CommonMethods.SessionInfo.UserName;
            var lcNumber = tbxLCNumber.Text.Trim();
            var description = tbxDescription.Text.Trim();
           
            var isExist = unitOfWork.GenericRepositories<LcInfoForMega>().IsExist(x => x.LcNumber == lcNumber);

            if (!isExist)
            {
                string user = CommonMethods.SessionInfo.UserName;
                var newlc = new LcInfoForMega()
                {
                    LcNumber = lcNumber,
                    Description = description,
                    CreatedBy = user,
                    CreateDate = DateTime.Now
                };
                unitOfWork.GenericRepositories<LcInfoForMega>().Insert(newlc);
                unitOfWork.Save();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Lc Number already exists.')", true);
            }

            ClearAll();
           // BindData();

        }

        protected void ClearAll()
        {
            tbxLCNumber.Text = "";
            tbxDescription.Text = "";
        }


    }
}