﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Mega
{
    public partial class ReceiveYarnatStore : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        DataTable dtIssuedYarnAtStoreDetails;
        int initialLoadForUpdate = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetInitialRowCount();
            }
             
        }


        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var ddlItems = (DropDownList)e.Item.FindControl("ddlItems");
                var ddlCategory = (DropDownList)e.Item.FindControl("ddlCategory");
                var ddlLcNumber = (DropDownList)e.Item.FindControl("ddlLcNumber");
                var lblAvailableBalance = (Label)e.Item.FindControl("lblAvailableBalance");
              
                var tbxRemarks = (TextBox)e.Item.FindControl("tbxRemarks");
                var tbxQuantity = (TextBox)e.Item.FindControl("tbxQuantity");

                CommonMethods.LoadYarnForMegaDropdown(ddlItems, 1, 0);
                CommonMethods.LoadDropdown(ddlCategory, "ItemCategory", 1, 0);
                CommonMethods.LoadLcNumberForMegaDropdown(ddlLcNumber, 1, 0);
                
                tbxQuantity.Text = DataBinder.Eval(e.Item.DataItem, "Quantity").ToString();
                tbxRemarks.Text = DataBinder.Eval(e.Item.DataItem, "Remarks").ToString();


                if (initialLoadForUpdate > 0)
                {

                    var itemId = DataBinder.Eval(e.Item.DataItem, "ItemId").ToString();
                    var categoryId = DataBinder.Eval(e.Item.DataItem, "CategoryId").ToString();
                    var lcNumber = DataBinder.Eval(e.Item.DataItem, "LcNumber").ToString();
                   
                    ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse(string.IsNullOrEmpty(itemId) ? "0" : itemId));
                    ddlCategory.SelectedIndex = CommonMethods.MatchDropDownItem(ddlCategory, int.Parse(string.IsNullOrEmpty(categoryId) ? "0" : categoryId));
                    ddlLcNumber.SelectedIndex = CommonMethods.MatchDropDownItem(ddlLcNumber, int.Parse(string.IsNullOrEmpty(lcNumber) ? "0" : lcNumber));

                }

            }
        }

        protected void btnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {
                List<YarnWiseBalanceForMega> YarnWiseBalanceForMegaInsert = new List<YarnWiseBalanceForMega>();
                List<YarnWisePhysicalStoreBalanceForMega> YarnWisePhysicalBalanceForMegaInsert = new List<YarnWisePhysicalStoreBalanceForMega>();

                var ddlItems = (DropDownList)rptEntryInfo.Items[0].FindControl("ddlItems");
                var ddlLcNumber = (DropDownList)rptEntryInfo.Items[0].FindControl("ddlLcNumber");
                var ddlCategory = (DropDownList)rptEntryInfo.Items[0].FindControl("ddlCategory");
                var lblAvailableBalance = (Label)rptEntryInfo.Items[0].FindControl("lblAvailableBalance");
                var tbxRemarks = (TextBox)rptEntryInfo.Items[0].FindControl("tbxRemarks");
                var tbxQuantity = (TextBox)rptEntryInfo.Items[0].FindControl("tbxQuantity");

                if (ddlItems.SelectedValue != "" && ddlCategory.SelectedValue != "" && tbxQuantity.Text != "")
                    {
                        var ItemId = int.Parse(ddlItems.SelectedValue);
                        var CategoryId = int.Parse(ddlCategory.SelectedValue);
                        var LcId = int.Parse(ddlLcNumber.SelectedValue);
                        var yarnwiseStoreBalance = new YarnWiseBalanceForMega()
                            {   
                                YarnId = int.Parse(ddlItems.SelectedValue),
                                CategoryId = int.Parse(ddlCategory.SelectedValue),
                                MegaLcId = int.Parse(ddlLcNumber.SelectedValue),
                                Quantity = decimal.Parse(tbxQuantity.Text.ToString()),
                                Remarks = tbxRemarks.Text     
                            };

                    var yarnwisePhysicalStoreBalance = new YarnWisePhysicalStoreBalanceForMega()
                    {
                        YarnId = int.Parse(ddlItems.SelectedValue),
                        CategoryId = int.Parse(ddlCategory.SelectedValue),
                        Quantity = decimal.Parse(tbxQuantity.Text.ToString()),
                        Remarks = tbxRemarks.Text
                    };

                    YarnWiseBalanceForMegaInsert.Add(yarnwiseStoreBalance);
                    YarnWisePhysicalBalanceForMegaInsert.Add(yarnwisePhysicalStoreBalance);

                    var availableBalance = decimal.Parse(unitOfWork.GetSingleStringValue($"EXEC usp_GetAvailableBalanceForMegaStore {ItemId},{LcId},{CategoryId}"));
                    var availablePhysicalStoreBalance = decimal.Parse(unitOfWork.GetSingleStringValue($"EXEC usp_GetAvailablePhysicalBalanceForMegaStore {ItemId},{CategoryId}"));

                    //Updating Shipment Status start
                    var LcWiseShipmentavailableBalance = (decimal.Parse(unitOfWork.GetSingleStringValue($"EXEC usp_GetAvailableBalanceForMegaShipment {ItemId},{LcId},{CategoryId}"))- (decimal.Parse(tbxQuantity.Text.ToString())));
                    var OCNumberTable = unitOfWork.GetSingleStringValue($"UPDATE LcWiseShipmentQntyForMega SET Quantity = " + LcWiseShipmentavailableBalance + " WHERE MegaYarnId = " + ItemId + " AND MegaLcId = " + LcId + " AND CategoryId = " + CategoryId +  " ");

                    //Updating Shipment Status End


                    var isExist = unitOfWork.GenericRepositories<YarnWiseBalanceForMega>().Get(x => x.YarnId == yarnwiseStoreBalance.YarnId && x.MegaLcId == yarnwiseStoreBalance.MegaLcId && x.CategoryId == yarnwiseStoreBalance.CategoryId).Any();
                    if (!isExist)
                    {
                    foreach (var item in YarnWiseBalanceForMegaInsert)
                    {
                        unitOfWork.GenericRepositories<YarnWiseBalanceForMega>().Insert(item);
                    }
                       
                    }
                    else
                    {

                        var existingEntity = unitOfWork.GenericRepositories<YarnWiseBalanceForMega>().Get(x => x.YarnId == ItemId && x.MegaLcId == yarnwiseStoreBalance.MegaLcId && x.CategoryId == yarnwiseStoreBalance.CategoryId).FirstOrDefault();

                        if (existingEntity != null)
                        {
                            // Update the properties of the existing entity
                            existingEntity.Quantity = decimal.Parse(tbxQuantity.Text.ToString()) + availableBalance;
                            existingEntity.Remarks = tbxRemarks.Text;

                            // Save the changes to the database
                            unitOfWork.Save();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error(' Can not Saved.')", true);
                        }

                    }

                    //Mega Physical Store
                    var isOnlyYarnExist = unitOfWork.GenericRepositories<YarnWisePhysicalStoreBalanceForMega>().Get(x => x.YarnId == yarnwisePhysicalStoreBalance.YarnId && x.CategoryId == yarnwisePhysicalStoreBalance.CategoryId).Any();
                    if (!isOnlyYarnExist)
                    {
                        foreach (var physicalStoreitem in YarnWisePhysicalBalanceForMegaInsert)
                        {
                            unitOfWork.GenericRepositories<YarnWisePhysicalStoreBalanceForMega>().Insert(physicalStoreitem);
                        }

                    }
                    else
                    {

                        var existingEntity = unitOfWork.GenericRepositories<YarnWisePhysicalStoreBalanceForMega>().Get(x => x.YarnId == ItemId && x.CategoryId == CategoryId ).FirstOrDefault();

                        if (existingEntity != null)
                        {
                            // Update the properties of the existing entity
                            existingEntity.Quantity = decimal.Parse(tbxQuantity.Text.ToString()) + availablePhysicalStoreBalance;
                            existingEntity.Remarks = tbxRemarks.Text;

                            // Save the changes to the database
                            unitOfWork.Save();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error(' Can not Saved.')", true);
                        }

                    }
                    //Mega Physical Store
                    unitOfWork.Save();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Please Fillup all field.')", true);
                }  
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                                    
                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ReceiveYarnatStore.aspx');", true);
                   
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
           

        }

        protected void SelectedIndexChanged(object sender, EventArgs e)
        {
            var ddl = (DropDownList)sender;
            
            var ddlItems = (DropDownList)ddl.NamingContainer.FindControl("ddlItems");
            var ddlCategory = (DropDownList)ddl.NamingContainer.FindControl("ddlCategory");
            var ddlLcNumber = (DropDownList)ddl.NamingContainer.FindControl("ddlLcNumber");
            var lblAvailableBalance = (Label)ddl.NamingContainer.FindControl("lblAvailableBalance");
            ShowAvailableBalance(ddlItems, ddlCategory, ddlLcNumber, lblAvailableBalance);
        }


        private void ShowAvailableBalance(DropDownList ddlItems, DropDownList ddlCategory, DropDownList ddlLcNumber, Label lblAB)
        {
            if (ddlItems.SelectedValue != "" && ddlCategory.SelectedValue != "" && ddlLcNumber.SelectedValue != "")
            {
                var itemId = !string.IsNullOrEmpty(ddlItems.SelectedValue) ? int.Parse(ddlItems.SelectedValue) : 0;
                var categoryId = !string.IsNullOrEmpty(ddlCategory.SelectedValue) ? int.Parse(ddlCategory.SelectedValue) : 0;
                var lcId = !string.IsNullOrEmpty(ddlLcNumber.SelectedValue) ? int.Parse(ddlLcNumber.SelectedValue) : 0;
                var availableBalance = decimal.Parse(unitOfWork.GetSingleStringValue($"EXEC usp_GetAvailableBalanceForMegaStore {itemId},{lcId},{categoryId}"));

                lblAB.Text = availableBalance + "";
            }
            else
            {
                lblAB.Text = "";
            }
        }
        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));
            dt.Columns.Add(new DataColumn("CategoryId", typeof(string)));
            dt.Columns.Add(new DataColumn("LcNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("AvailableBalance", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("Remarks", typeof(string)));
            dr = dt.NewRow();

            dr["ItemId"] = string.Empty;
            dr["CategoryId"] = string.Empty;
            dr["LcNumber"] = string.Empty;
            dr["AvailableBalance"] = string.Empty;
            dr["Quantity"] = string.Empty;
            dr["Remarks"] = string.Empty;
            dt.Rows.Add(dr);
           
            ViewState["CurrentTable"] = dt;
            rptEntryInfo.DataSource = dt;
            rptEntryInfo.DataBind();
        }


    }
}