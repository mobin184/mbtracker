﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Mega
{
    public partial class ItemWiseLcInfo : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();


        DataTable dtIssuedYarnAtStoreDetails;
        int initialLoadForUpdate = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetInitialRowCount();
            }
        }

        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var ddlItems = (DropDownList)e.Item.FindControl("ddlItems");
                var ddlCategory = (DropDownList)e.Item.FindControl("ddlCategory");
                var ddlLcNumber = (DropDownList)e.Item.FindControl("ddlLcNumber");
                var lblAvailableBalance = (Label)e.Item.FindControl("lblAvailableBalance");

                var tbxRemarks = (TextBox)e.Item.FindControl("tbxRemarks");
                var tbxQuantity = (TextBox)e.Item.FindControl("tbxQuantity");

                CommonMethods.LoadYarnForMegaDropdown(ddlItems, 1, 0);
                CommonMethods.LoadDropdown(ddlCategory, "ItemCategory", 1, 0);
                CommonMethods.LoadLcNumberForMegaDropdown(ddlLcNumber, 1, 0);
               
                tbxQuantity.Text = DataBinder.Eval(e.Item.DataItem, "Quantity").ToString();
                tbxRemarks.Text = DataBinder.Eval(e.Item.DataItem, "Remarks").ToString();


                if (initialLoadForUpdate > 0)
                {
                    var itemId = DataBinder.Eval(e.Item.DataItem, "ItemId").ToString();
                    var categoryId = DataBinder.Eval(e.Item.DataItem, "CategoryId").ToString();
                    var lcNumber = DataBinder.Eval(e.Item.DataItem, "LcNumber").ToString();
                   
                    ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse(string.IsNullOrEmpty(itemId) ? "0" : itemId));
                    ddlCategory.SelectedIndex = CommonMethods.MatchDropDownItem(ddlCategory, int.Parse(string.IsNullOrEmpty(categoryId) ? "0" : categoryId));
                    ddlLcNumber.SelectedIndex = CommonMethods.MatchDropDownItem(ddlLcNumber, int.Parse(string.IsNullOrEmpty(lcNumber) ? "0" : lcNumber));

                }

            }
        }

        protected void btnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {

                List<LcWiseShipmentQntyForMega> LcWiseShipmentQntyForMegaInsert = new List<LcWiseShipmentQntyForMega>();
                // List<YarnWiseBalanceForMega> YarnWiseBalanceForMegaUpdate = new List<YarnWiseBalanceForMega>();

                var ddlItems = (DropDownList)rptEntryInfo.Items[0].FindControl("ddlItems");
                var ddlCategory = (DropDownList)rptEntryInfo.Items[0].FindControl("ddlCategory");
                var ddlLcNumber = (DropDownList)rptEntryInfo.Items[0].FindControl("ddlLcNumber");
                var lblAvailableBalance = (Label)rptEntryInfo.Items[0].FindControl("lblAvailableBalance");
                var tbxRemarks = (TextBox)rptEntryInfo.Items[0].FindControl("tbxRemarks");
                var tbxQuantity = (TextBox)rptEntryInfo.Items[0].FindControl("tbxQuantity");

                if (ddlItems.SelectedValue != "" && tbxQuantity.Text != "" && ddlLcNumber.SelectedValue != "")
                {
                    var ItemId = int.Parse(ddlItems.SelectedValue);
                    var CategoryId = int.Parse(ddlCategory.SelectedValue);
                    var LcId = int.Parse(ddlLcNumber.SelectedValue);
                    var lcwiseShipmentBalance = new LcWiseShipmentQntyForMega()
                    {
                        MegaYarnId = int.Parse(ddlItems.SelectedValue),
                        CategoryId = int.Parse(ddlCategory.SelectedValue),
                        MegaLcId = int.Parse(ddlLcNumber.SelectedValue),
                        Quantity = decimal.Parse(tbxQuantity.Text.ToString()),
                        Remarks = tbxRemarks.Text
                    };
                    LcWiseShipmentQntyForMegaInsert.Add(lcwiseShipmentBalance);

                    //var availableBalance = decimal.Parse(unitOfWork.GetSingleStringValue($"EXEC usp_GetAvailableBalanceForMegaShipment {ItemId}, {MegaLcId}"));

                    string sql = $"Exec usp_GetAvailableBalanceForMegaShipment '{ItemId}','{LcId}',{CategoryId}";
                    var availableBalanceTable = unitOfWork.GetDataTableFromSql(sql);
                    var availableBalance = decimal.Parse(availableBalanceTable.Rows[0]["AvailableBalance"].ToString());

                    var isExist = unitOfWork.GenericRepositories<LcWiseShipmentQntyForMega>().Get(x => x.MegaYarnId == lcwiseShipmentBalance.MegaYarnId && x.CategoryId == lcwiseShipmentBalance.CategoryId && x.MegaLcId == lcwiseShipmentBalance.MegaLcId).Any();
                   
                    if (!isExist)
                    {
                        foreach (var item in LcWiseShipmentQntyForMegaInsert)
                        {
                            unitOfWork.GenericRepositories<LcWiseShipmentQntyForMega>().Insert(item);
                        }

                    }
                    else
                    {
                        var existingEntity = unitOfWork.GenericRepositories<LcWiseShipmentQntyForMega>().Get(x => x.MegaYarnId == lcwiseShipmentBalance.MegaYarnId && x.CategoryId == lcwiseShipmentBalance.CategoryId && x.MegaLcId == lcwiseShipmentBalance.MegaLcId).FirstOrDefault();

                        if (existingEntity != null)
                        {
                            // Update the properties of the existing entity
                            existingEntity.Quantity = decimal.Parse(tbxQuantity.Text.ToString()) + availableBalance;
                            existingEntity.Remarks = tbxRemarks.Text;

                            // Save the changes to the database
                            unitOfWork.Save();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error(' Can not Saved.')", true);
                        }



                    }
                    unitOfWork.Save();
                }


                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);

                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ItemWiseLcInfo.aspx');", true);


            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }

        }

        protected void SelectedIndexChanged(object sender, EventArgs e)
        {
            var ddl = (DropDownList)sender;

            var ddlItems = (DropDownList)ddl.NamingContainer.FindControl("ddlItems");
            var ddlCategory = (DropDownList)ddl.NamingContainer.FindControl("ddlCategory");
            var ddlLcNumber = (DropDownList)ddl.NamingContainer.FindControl("ddlLcNumber");

            var lblAvailableBalance = (Label)ddl.NamingContainer.FindControl("lblAvailableBalance");
            ShowAvailableBalance(ddlItems, ddlCategory, ddlLcNumber, lblAvailableBalance);
        }

        private void ShowAvailableBalance(DropDownList ddlItems, DropDownList ddlCategory, DropDownList ddlLcNumber, Label lblAB)
        {
            if (ddlItems.SelectedValue != "" && ddlLcNumber.SelectedValue != "")
            {
                var itemId = !string.IsNullOrEmpty(ddlItems.SelectedValue) ? int.Parse(ddlItems.SelectedValue) : 0;
                var categoryId = !string.IsNullOrEmpty(ddlCategory.SelectedValue) ? int.Parse(ddlCategory.SelectedValue) : 0;
                var lcId = !string.IsNullOrEmpty(ddlLcNumber.SelectedValue) ? int.Parse(ddlLcNumber.SelectedValue) : 0;

                var availableBalance = decimal.Parse(unitOfWork.GetSingleStringValue($"EXEC usp_GetAvailableBalanceForMegaShipment {itemId},{lcId},{categoryId}"));

                lblAB.Text = availableBalance + "";
            }
            else
            {
                lblAB.Text = "";
            }
        }

        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));
            dt.Columns.Add(new DataColumn("CategoryId", typeof(string)));
            dt.Columns.Add(new DataColumn("LcNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("AvailableBalance", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("Remarks", typeof(string)));

            dr = dt.NewRow();

            dr["ItemId"] = string.Empty;
            dr["CategoryId"] = string.Empty;
            dr["LcNumber"] = string.Empty;
            dr["AvailableBalance"] = string.Empty;
            dr["Quantity"] = string.Empty;
            dr["Remarks"] = string.Empty;
            dt.Rows.Add(dr);

            ViewState["CurrentTable"] = dt;
            rptEntryInfo.DataSource = dt;
            rptEntryInfo.DataBind();
        }

    }
}