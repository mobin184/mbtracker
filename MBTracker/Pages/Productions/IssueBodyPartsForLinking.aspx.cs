﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class IssueBodyPartsForLinking : Page
    {
        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                tbxIssueDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                CommonMethods.LoadFinishingFloorDropDown(ddlUserFinishingUnitFloor);
                if (Request.QueryString["issueId"] != null)
                {
                    IssueId = int.Parse(Tools.UrlDecode(Request.QueryString["issueId"]));
                    var issueInfo = unitOfWork.GenericRepositories<LinkingBodyIssue>().GetByID(IssueId);
                    PopulateInfo(issueInfo);
                    btnSaveEntries.Visible = false;
                    btnUpdateEntries.Visible = true;
                }
            }
        }

        private void PopulateInfo(LinkingBodyIssue issueInfo)
        {
            CommonMethods.LoadDropdownById(ddlStyles, issueInfo.BuyerId, "BuyerStyles", 1, 0);
            var dtSizes = buyerManager.GetSizes(issueInfo.BuyerId);
            DataTable dtCloned = dtSizes.Clone();
            dtCloned.Columns[0].DataType = typeof(string);
            foreach (DataRow row in dtSizes.Rows)
            {
                dtCloned.ImportRow(row);
            }
            var nrow = dtCloned.NewRow();
            nrow["Id"] = "";
            nrow["SizeName"] = "--Select--";
            dtCloned.Rows.InsertAt(nrow, 0);
            ddlSize.DataTextField = "SizeName";
            ddlSize.DataValueField = "Id";
            ddlSize.DataSource = dtCloned;
            ddlSize.DataBind();

            tbxIssueDate.Text = issueInfo.IssueDate.ToString("yyyy-MM-dd");
            ddlUserFinishingUnitFloor.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUserFinishingUnitFloor, issueInfo.FinishingUnitFloorId);
            ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, issueInfo.BuyerId);
            ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, issueInfo.StyleId);
            loadColorsDropdown(ddlColors, issueInfo.StyleId);
            ddlColors.SelectedIndex = CommonMethods.MatchDropDownItem(ddlColors, issueInfo.BuyerColorId);
            ddlSize.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSize, issueInfo.SizeId);
            tbxIssueQuantity.Text = issueInfo.IssueQty + "";
            tbxKnitIdOrTCNo.Text = issueInfo.KnitIdOrTCNo;
            tbxLinkOpCardNo.Text = issueInfo.OPCardNo;
            ShowAvailableQty();
        }

        int IssueId
        {
            get
            {
                try
                {
                    return int.Parse(ViewState["issueId"].ToString());
                }
                catch (Exception)
                {
                    return 0;
                }
            }
            set
            {
                ViewState["issueId"] = value;
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            ddlSize.Items.Clear();
            ddlColors.Items.Clear();
            lblAvailableQty.Visible = false;
            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
                var dtSizes = buyerManager.GetSizes(int.Parse(ddlBuyers.SelectedValue));
                DataTable dtCloned = dtSizes.Clone();
                dtCloned.Columns[0].DataType = typeof(string);
                foreach (DataRow row in dtSizes.Rows)
                {
                    dtCloned.ImportRow(row);
                }
                var nrow = dtCloned.NewRow();
                nrow["Id"] = "";
                nrow["SizeName"] = "--Select--";
                dtCloned.Rows.InsertAt(nrow, 0);
                ddlSize.DataTextField = "SizeName";
                ddlSize.DataValueField = "Id";
                ddlSize.DataSource = dtCloned;
                ddlSize.DataBind();

            }
        }
        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlColors.Items.Clear();
            lblAvailableQty.Visible = false;
            if (ddlStyles.SelectedValue != "")
            {
                loadColorsDropdown(ddlColors, int.Parse(ddlStyles.SelectedValue));
            }
        }

        protected void loadColorsDropdown(DropDownList ddlColor, int styleId)
        {           
            var dtColors = new CommonManager().GetColorsByStyle(styleId);
            DataTable dtCloned = dtColors.Clone();
            dtCloned.Columns[0].DataType = typeof(string);
            foreach (DataRow row in dtColors.Rows)
            {
                dtCloned.ImportRow(row);
            }
            var nrow = dtCloned.NewRow();
            nrow["BuyerColorId"] = "";
            nrow["ColorDescription"] = "--Select--";
            dtCloned.Rows.InsertAt(nrow, 0);
            ddlColor.DataTextField = "ColorDescription";
            ddlColor.DataValueField = "BuyerColorId";
            ddlColor.DataSource = dtCloned;
            ddlColor.DataBind();
        }

        protected void btnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {
                var issueDate = DateTime.Parse(tbxIssueDate.Text);
                if (!string.IsNullOrEmpty(tbxKnitIdOrTCNo.Text))
                {
                    if (!string.IsNullOrEmpty(tbxIssueQuantity.Text))
                    {
                        if (issueDate <= DateTime.Now)
                        {
                            var issueQty = int.Parse(tbxIssueQuantity.Text);
                            var floorId = int.Parse(ddlUserFinishingUnitFloor.SelectedValue);
                            var styleId = int.Parse(ddlStyles.SelectedValue);
                            var buyerColorId = int.Parse(ddlColors.SelectedValue);
                            var sizeId = int.Parse(ddlSize.SelectedValue);
                            var linkOpCartNo = tbxLinkOpCardNo.Text.Trim();

                            var stAvailableQty = unitOfWork.GetSingleValue($"Exec usp_GetAvailableQtyForBodyIssue {floorId},{styleId},{buyerColorId},{sizeId},0");
                            int availableQty = 0;
                            if (!string.IsNullOrEmpty(stAvailableQty))
                            {
                                availableQty = int.Parse(stAvailableQty);
                            }
                            if (availableQty >= issueQty && issueQty > 0)
                            {
                                var lbi = new LinkingBodyIssue()
                                {
                                    IssueDate = issueDate,
                                    BuyerId = int.Parse(ddlBuyers.SelectedValue),
                                    StyleId = styleId,
                                    FinishingUnitFloorId = floorId,
                                    BuyerColorId = buyerColorId,
                                    IsReceived = false,
                                    IssueQty = issueQty,
                                    OPCardNo = linkOpCartNo,
                                    KnitIdOrTCNo = tbxKnitIdOrTCNo.Text.Trim(),
                                    SizeId = sizeId,
                                    CreatedBy = CommonMethods.SessionInfo.UserName,
                                    CreateDate = DateTime.Now
                                };

                                unitOfWork.GenericRepositories<LinkingBodyIssue>().Insert(lbi);
                                unitOfWork.Save();
                                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                            }
                            else
                            {
                                if(issueQty == 0)
                                {
                                    ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Issue quantity must be grater than zero.')", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Size available quantity:" + availableQty + "')", true);
                                }
                                
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Sorry you cant entry issue in future date.')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please enter issue quantity.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please enter Knit id or TC no.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void btnUpdateEntries_Click(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "blockUI()", true);
                var issueDate = DateTime.Parse(tbxIssueDate.Text);
                if (!string.IsNullOrEmpty(tbxKnitIdOrTCNo.Text))
                {
                    if (!string.IsNullOrEmpty(tbxIssueQuantity.Text))
                    {
                        if (issueDate <= DateTime.Now)
                        {
                            var issueQty = int.Parse(tbxIssueQuantity.Text);
                            var floorId = int.Parse(ddlUserFinishingUnitFloor.SelectedValue);
                            var styleId = int.Parse(ddlStyles.SelectedValue);
                            var buyerColorId = int.Parse(ddlColors.SelectedValue);
                            var sizeId = int.Parse(ddlSize.SelectedValue);
                            var linkOpCartNo = tbxLinkOpCardNo.Text.Trim();

                            var stAvailableQty = unitOfWork.GetSingleValue($"Exec usp_GetAvailableQtyForBodyIssue {floorId},{styleId},{buyerColorId},{sizeId},{IssueId}");
                            int availableQty = 0;
                            if (!string.IsNullOrEmpty(stAvailableQty))
                            {
                                availableQty = int.Parse(stAvailableQty);
                            }
                            if (availableQty >= issueQty && issueQty > 0)
                            {
                                var lbi = unitOfWork.GenericRepositories<LinkingBodyIssue>().GetByID(IssueId);
                                lbi.IssueDate = issueDate;
                                lbi.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                                lbi.StyleId = styleId;
                                lbi.FinishingUnitFloorId = floorId;
                                lbi.BuyerColorId = buyerColorId;
                                lbi.IssueQty = issueQty;
                                lbi.OPCardNo = linkOpCartNo;
                                lbi.KnitIdOrTCNo = tbxKnitIdOrTCNo.Text.Trim();
                                lbi.SizeId = sizeId;
                                lbi.UpdatedBy = CommonMethods.SessionInfo.UserName;
                                lbi.UpdateDate = DateTime.Now;

                                unitOfWork.GenericRepositories<LinkingBodyIssue>().Update(lbi);
                                unitOfWork.Save();
                                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                                //Response.AddHeader("REFRESH", "2;URL=ViewLinkingBodyIssues.aspx");
                                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewLinkingBodyIssues.aspx');", true);
                            }
                            else
                            {
                                if (issueQty == 0)
                                {
                                    ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Issue quantity must be grater than zero.')", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Size available quantity:" + availableQty + "')", true);
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Sorry you cant entry issue in future date.')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please enter issue quantity.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please enter Knit id or TC no.')", true);
                }
            }
            catch (Exception ex)
            {
                //ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "unblockUI()", true);
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void tbxLinkOpCardNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(tbxLinkOpCardNo.Text))
                {
                    var floorId = int.Parse(string.IsNullOrEmpty(ddlUserFinishingUnitFloor.SelectedValue) ? "0" : ddlUserFinishingUnitFloor.SelectedValue);
                    var linkOpCartNo = tbxLinkOpCardNo.Text.Trim();
                    var dtExistIssues = new DataTable();
                    if (IssueId != 0)
                    {
                        dtExistIssues = unitOfWork.GetDataTableFromSql($"EXEC usp_GetNotReceivedLinkingBodyIssueByOPCardNo '{linkOpCartNo}'");
                    }
                    else
                    {
                        dtExistIssues = unitOfWork.GetDataTableFromSql($"EXEC usp_GetNotReceivedLinkingBodyIssueByOPCardNoExcludingIssueId '{linkOpCartNo}',{IssueId}");
                    }

                    if (dtExistIssues.Rows.Count > 0)
                    {
                        rptExistIssues.DataSource = dtExistIssues;
                        rptExistIssues.DataBind();
                        rptExistIssues.Visible = true;
                        divExistIssues.Visible = true;
                        ScriptManager.RegisterStartupScript(this,this.GetType(), "message", "scrollDown()", true);
                        //btnSaveEntries.Visible = false;
                        //btnUpdateEntries.Visible = false;
                    }
                    else
                    {
                        rptExistIssues.DataSource = null;
                        rptExistIssues.DataBind();
                        rptExistIssues.Visible = false;
                        divExistIssues.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void ddlSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddlSize.SelectedValue != "")
            {
                ShowAvailableQty();
            }
            else
            {
                lblAvailableQty.Visible = false;
            }
        }

        protected void ddlColors_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSize.SelectedIndex = 0;
            lblAvailableQty.Visible = false;
        }

        protected void ShowAvailableQty()
        {
            var floorId = int.Parse(ddlUserFinishingUnitFloor.SelectedValue);
            var styleId = int.Parse(ddlStyles.SelectedValue);
            var buyerColorId = int.Parse(ddlColors.SelectedValue);
            var sizeId = int.Parse(ddlSize.SelectedValue);
            var stAvailableQty = "";
            //var linkOpCartNo = tbxLinkOpCardNo.Text.Trim();
            if (IssueId != 0)
            {
                stAvailableQty = unitOfWork.GetSingleValue($"Exec usp_GetAvailableQtyForBodyIssue {floorId},{styleId},{buyerColorId},{sizeId},{IssueId}");
            }
            else
            {
                stAvailableQty = unitOfWork.GetSingleValue($"Exec usp_GetAvailableQtyForBodyIssue {floorId},{styleId},{buyerColorId},{sizeId},0");
            }
            
            lblAvailableQty.Text = $"Available Quantity: {stAvailableQty}";
            lblAvailableQty.Visible = true;
        }
    }
}