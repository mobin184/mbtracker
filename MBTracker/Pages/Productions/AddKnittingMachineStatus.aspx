﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddKnittingMachineStatus.aspx.cs" Inherits="MBTracker.Pages.Productions.AddKnittingMachineStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        select, textarea {
            display: inline-block;
            /* height: 20px; */
            padding: 0px;
            /* margin-bottom: 10px; */
            font-size: 14px;
            line-height: 20px;
            color: #525252;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border-radius: 2px;
            vertical-align: middle;
        }

        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 5px;
        }
    </style>
    <div class="row-fluid">
        <div class="span6">
            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblProductionEntry" Text="Enter Knitting Machine Status:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row-fluid" runat="server">
                        <div class="span12">
                            <div id="dt_example" class="example_alt_pagination">
                                <div class="control-group">
                                    <label for="inputOrder" class="control-label">
                                        <asp:Label ID="lblEntryInfo" runat="server" Text="" Font-Bold="true"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:Repeater ID="rptGeneralInfo" runat="server" OnItemDataBound="rptGeneralInfo_ItemDataBound" OnItemCreated="rptGeneralInfo_ItemCreated">
                                            <HeaderTemplate>
                                                <table id="data-table-1" class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                <asp:Label ID="lblProductionUnit" runat="server" Text="Production Unit"></asp:Label>
                                                            </th>
                                                            <th>
                                                                <asp:Label ID="lblShift" runat="server" Text="Shift"></asp:Label>
                                                            </th>
                                                            <th>
                                                                <asp:Label ID="lblProductionDate" runat="server" Text="Production Date"></asp:Label>

                                                            </th>
                                                            <th>
                                                                <asp:Label ID="lblUser" runat="server" Text="User Name"></asp:Label>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlUserUnits" runat="server" Width="80%" AutoPostBack="true"></asp:DropDownList></td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlShift" runat="server" Width="100"></asp:DropDownList></td>
                                                    <asp:Label runat="server" ID="lblProductionEntryDate" Visible="false" Text='<%#Eval("ProductionEntryDate")%>'></asp:Label>
                                                    <td>
                                                        <asp:TextBox ID="tbxProductionDate" TextMode="Date" Width="130px" Text="" runat="server"></asp:TextBox></td>
                                                    <td><%#Eval("UserName") %> </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                 </table>
                                    <div class="clearfix">
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <div class="control-group">
                            <label for="inputOrder" class="control-label">
                                <asp:Label ID="lblKnittingMachineStatusEntry" runat="server" Text="" Font-Bold="true"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:Repeater ID="rptEntryInfo" runat="server" OnItemDataBound="rptEntryInfo_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table-1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lblMachineBrand" runat="server" Text="Machine <br/> Brand"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblRunningMachine" runat="server" Text="Number Of<br/>Running MC"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblUsedForSample" runat="server" Text="MC Used <br/>For Sample"></asp:Label></th>
                                                      <th>
                                                        <asp:Label ID="Label3" runat="server" Text="Used For<br/> Size Set"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblLongTimeStoped" runat="server" Text="Long Time<br/> Out of Order"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblTemporaryStoped" runat="server" Text="Temporary<br/> Out of Order"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblIdleMachine" runat="server" Text="Idle<br/> Machine"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label2" runat="server" Text="Machine Used<br/> For Setup"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblUsedForOtherWorks" runat="server" Text="MC Used For<br/> Other Works"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblPurposeOfOtherWorks" runat="server" Text="Purpose of <br/> Other Works"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label1" runat="server" Text="Total No.<br/>of Machines"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("MachineBrand") %>
                                                <asp:Label ID="lblDailyKnittingMachineStatusId" Text="" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblMachineBrandId" runat="server" Text='<%#Eval("MachineBrandId") %>' Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbxNoOfRunningMachine" Width="80" ReadOnly="true" runat="server" TextMode="Number" min="0"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbxUsedForSample" Width="80" runat="server" TextMode="Number" min="0"></asp:TextBox>
                                            </td>
                                              <td>
                                                <asp:TextBox ID="tbxUsedForSizeSet" Width="80"  runat="server" TextMode="Number" min="0"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbxLongTimeStoped" Width="80" runat="server" TextMode="Number" min="0"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbxTemporaryStoped" Width="80" runat="server" TextMode="Number" min="0"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbxIdleMachine" Width="80" runat="server" TextMode="Number" min="0"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbxUsedForSetup" Width="80" ReadOnly="true" runat="server" TextMode="Number" min="0"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbxUsedForOtherWorks" Width="80" runat="server" TextMode="Number" min="0"></asp:TextBox>
                                            </td>
                                            <td style="width:20%">
                                                <asp:TextBox ID="tbxPurposeOfOtherWorks" runat="server" Width="100%" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblNumberOfMachines" runat="server" Text='<%#Eval("NumberOfMachines") %>'></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                 </table>
                                    <div class="clearfix">
                                    </FooterTemplate>
                                </asp:Repeater>
                                <br />
                                <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>
                                <asp:Label ID="lblNoDataFound" runat="server" Text="No machine brand found in this unit." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
