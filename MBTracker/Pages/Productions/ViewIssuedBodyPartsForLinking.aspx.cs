﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class ViewIssuedBodyPartsForLinking : Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableEditButton();
                //tbxIssueDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
        }

        private void EnableDisableEditButton()
        {
            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Finishing");

            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewIssuedBodyPartsForLinking", 1, 1);

        }
        int IssueId
        {
            get
            {
                try
                {
                    return int.Parse(ViewState["issueId"].ToString());
                }
                catch (Exception)
                {
                    return 0;
                }
            }
            set
            {
                ViewState["issueId"] = value;
            }
        }

        protected void btnViewEntries_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (!string.IsNullOrEmpty(tbxIssueDate.Text))
                {
                    LoadData();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please select an Issue date.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void LoadData()
        {
            var opCardNo = "";//string.IsNullOrEmpty(tbxOpCardNo.Text) ? "" : tbxOpCardNo.Text;
            var issueDate = DateTime.Parse(tbxIssueDate.Text);
            var dtIssues = unitOfWork.GetDataTableFromSql($"EXEC usp_GetLinkingBodyIssueByDate {CommonMethods.SessionInfo.UserId},'{issueDate}','{opCardNo}'");
            if(dtIssues.Rows.Count > 0)
            {
                lblEntryNotFound.Visible = false;
                rptExistIssues.DataSource = dtIssues;
                rptExistIssues.DataBind();
                rptExistIssues.Visible = true;
                divExistIssues.Visible = true;

                rptReceived.DataSource = null;
                rptReceived.DataBind();
                divReceived.Visible = false;
            }
            else
            {
                lblEntryNotFound.Visible = true;
                rptExistIssues.DataSource = null;
                rptExistIssues.DataBind();
                rptExistIssues.Visible = false;
                divExistIssues.Visible = false;

                rptReceived.DataSource = null;
                rptReceived.DataBind();
                divReceived.Visible = false;
            }
            
           
        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                var issueId = int.Parse(e.CommandArgument.ToString());
                Response.Redirect("IssueBodyPartsForLinking.aspx?issueId=" + Tools.UrlEncode(issueId+""));
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void lnkbtnLinkReceived_Command(object sender, CommandEventArgs e)
        {
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            IssueId = int.Parse(commandArgs[0]);
            var isReceived = int.Parse(commandArgs[1]);            

            var dtReceive = unitOfWork.GetDataTableFromSql($"EXEC usp_GetLinkingBodyIssueById {IssueId}");
            rptReceived.DataSource = dtReceive;
            rptReceived.DataBind();

            if (isReceived == 1)
            {
                var receivedInfo = unitOfWork.GenericRepositories<LinkingBodyReceive>().Get(x => x.IssueId == IssueId).FirstOrDefault();
                ((TextBox)rptReceived.Items[0].FindControl("tbxReceivedQty")).Text = receivedInfo.ReceivedQty + "";
                ((TextBox)rptReceived.Items[0].FindControl("tbxReturnQty")).Text = receivedInfo.ReturnQty + "";
                ((TextBox)rptReceived.Items[0].FindControl("tbxWastageQty")).Text = receivedInfo.WastageQty + "";
            }

            rptReceived.Visible = true;
            rptReceived.Visible = true;
            divReceived.Visible = true;
            ScriptManager.RegisterStartupScript(this,this.GetType(), "message", "scrollDown()", true);
        }

        protected void lnkbtnReceiveIt_Command(object sender, CommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                var issueId = int.Parse(commandArgs[0]);
                var isReceived = int.Parse(commandArgs[1]);
                var issuQty = int.Parse(commandArgs[2]);
                if (isReceived == 0)
                {
                    var txtLinkQty = ((TextBox)rptReceived.Items[0].FindControl("tbxReceivedQty")).Text;
                    var txtreturnQty = ((TextBox)rptReceived.Items[0].FindControl("tbxReturnQty")).Text;
                    var txtwastageQty = ((TextBox)rptReceived.Items[0].FindControl("tbxWastageQty")).Text;
                    var linkingQty = int.Parse(string.IsNullOrEmpty(txtLinkQty) ? "0" : txtLinkQty);
                    var returnQty = int.Parse(string.IsNullOrEmpty(txtreturnQty) ? "0" : txtreturnQty);
                    var wastageQty = int.Parse(string.IsNullOrEmpty(txtwastageQty) ? "0" : txtwastageQty);

                    if (issuQty == (linkingQty + returnQty + wastageQty))
                    {
                        var issuInfo = unitOfWork.GenericRepositories<LinkingBodyIssue>().GetByID(IssueId);
                        issuInfo.IsReceived = true;
                        var receive = new LinkingBodyReceive()
                        {
                            IssueId = IssueId,
                            ReceivedDate = DateTime.Now,
                            ReceivedQty = linkingQty,
                            ReturnQty = returnQty,
                            WastageQty = wastageQty,
                            CreateDate = DateTime.Now,
                            CreatedBy = CommonMethods.SessionInfo.UserName
                        };
                        unitOfWork.GenericRepositories<LinkingBodyReceive>().Insert(receive);
                        unitOfWork.GenericRepositories<LinkingBodyIssue>().Update(issuInfo);
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                        LoadData();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Issue quantity and received quantity should be same.');", true);
                        ScriptManager.RegisterStartupScript(this,this.GetType(), "message", "scrollDown();", true);
                    }
                }
                else
                {
                    var txtLinkQty = ((TextBox)rptReceived.Items[0].FindControl("tbxReceivedQty")).Text;
                    var txtreturnQty = ((TextBox)rptReceived.Items[0].FindControl("tbxReturnQty")).Text;
                    var txtwastageQty = ((TextBox)rptReceived.Items[0].FindControl("tbxWastageQty")).Text;
                    var linkingQty = int.Parse(string.IsNullOrEmpty(txtLinkQty) ? "0" : txtLinkQty);
                    var returnQty = int.Parse(string.IsNullOrEmpty(txtreturnQty) ? "0" : txtreturnQty);
                    var wastageQty = int.Parse(string.IsNullOrEmpty(txtwastageQty) ? "0" : txtwastageQty);

                    if (issuQty == (linkingQty + returnQty + wastageQty))
                    {
                        var receive = unitOfWork.GenericRepositories<LinkingBodyReceive>().Get(x => x.IssueId == IssueId).FirstOrDefault();

                        receive.ReceivedQty = linkingQty;
                        receive.ReturnQty = returnQty;
                        receive.WastageQty = wastageQty;
                        receive.UpdateDate = DateTime.Now;
                        receive.UpdatedBy = CommonMethods.SessionInfo.UserName;
                        unitOfWork.GenericRepositories<LinkingBodyReceive>().Update(receive);
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                        LoadData();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Issue quantity and received quantity should be same.');", true);
                        ScriptManager.RegisterStartupScript(this,this.GetType(), "message", "scrollDown();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }
    }
}