﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewIssuedBodyPartsForLinking.aspx.cs" Inherits="MBTracker.Pages.Productions.ViewIssuedBodyPartsForLinking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table th {
            text-align: center;
        }

        .disable {
            cursor: not-allowed;
            pointer-events: none;
        }
    </style>
    <div class="row-fluid">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="View Issued Body Parts for Linking:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row" style="padding-left: 15px; padding-right: 15px;">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                            <%--<span style="font-weight: 700; padding-right: 400px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field--%>
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="search" />
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label1" runat="server" Text="Select Issued Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxIssueDate" TextMode="Date" Width="100%" Text="" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="search"
                                            ControlToValidate="tbxIssueDate"><span style="font-weight: 700; color: #CC0000">Please select date issued.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <br />
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label11" runat="server" Text=""></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:Button runat="server" CssClass="btn btn-info" ID="btnViewEntries" Text="View Entries" ValidationGroup="search" OnClick="btnViewEntries_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <asp:Label ID="lblEntryNotFound" runat="server" Text="No data found." Visible="false" BackColor="#ffff00"></asp:Label>
        <div class="span12" runat="server" visible="false" id="divExistIssues">
            <label for="inputBuyer" class="control-label">
                <asp:Label ID="Label2" runat="server" Text="Issued Body Parts For Linking:" Font-Bold="true"></asp:Label></label>
            <div class="widget">
                <div class="widget-body">
                    <div class="control-group">

                        <div class="controls controls-row">
                            <div id="dt_example" class="example_alt_pagination">
                                <div class="control-group">
                                    <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                        <asp:Repeater ID="rptExistIssues" runat="server">
                                            <HeaderTemplate>
                                                <table id="data-table" class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <tr>
                                                                <th>
                                                                    <asp:Label ID="lbl1" runat="server" Text="Operator<br/>Card No"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="lbl2" runat="server" Text="Finishing<br/>Floor"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="lbl3" runat="server" Text="Issue<br/>Date"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="lbl4" runat="server" Text="Buyer<br/>Name"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label8" runat="server" Text="Style<br/>Name"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label9" runat="server" Text="Color<br/>Name"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label10" runat="server" Text="Issued<br/>Quantity"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label7" runat="server" Text="Received<br/>Status"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label3" runat="server" Text="Take<br/>Actions"></asp:Label></th>
                                                            </tr>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%#Eval("OPCardNo") %></td>
                                                    <td><%#Eval("FloorName") %></td>
                                                    <td><%#string.Format("{0:dd-MMM-yyyy}",Eval("IssueDate")) %></td>
                                                    <td><%#Eval("BuyerName") %></td>
                                                    <td><%#Eval("StyleName") %></td>
                                                    <td><%#Eval("ColorDescription") %></td>
                                                    <td><%#Eval("IssueQty") %></td>
                                                    <td><%#Eval("IsReceived").ToString() == "1" ? "<span class='label label-success'>&nbsp;&nbsp;&nbsp;Received&nbsp;&nbsp;&nbsp;</span>": "<span class='label label-danger'>Not Received</span>" %></td>
                                                    <td>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" Visible='<%#Eval("IsReceived").ToString() == "0" || Convert.ToBoolean(ViewState["editEnabled"]) == false ? false:true %>' disabled class="btn btn-success btn-small hidden-phone" Text="Edit Issue"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkbtnEdit" runat="server" Visible='<%#Eval("IsReceived").ToString() == "0" && Convert.ToBoolean(ViewState["editEnabled"]) == true ? true:false %>' CommandName="Edit" CommandArgument='<%#Eval("Id") %>' OnCommand="lnkbtnEdit_Command" class="btn btn-success btn-small hidden-phone" Text="Edit Issue"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkbtnLinkReceived" Visible='<%# ViewState["editEnabled"]%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id")+","+Eval("IsReceived") %>' OnCommand="lnkbtnLinkReceived_Command" class="btn btn-success btn-small hidden-phone" Text='<%#Eval("IsReceived").ToString() == "1" ? "Update Receive": "&nbsp&nbsp&nbsp&nbsp&nbsp&nbspReceive&nbsp&nbsp&nbsp&nbsp&nbsp" %>'></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                </table>
                            <div class="clearfix"></div>
                                            </FooterTemplate>
                                        </asp:Repeater>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid" runat="server" visible="false" id="divReceived">
        <div class="span12">
            <label for="inputBuyer" class="control-label">
                <asp:Label ID="Label5" runat="server" Text="Linked Body Parts Receive:" Font-Bold="true"></asp:Label></label>
            <div class="widget">
                <div class="widget-body">
                    <div class="control-group">

                        <div class="controls controls-row">
                            <div id="dt_example" class="example_alt_pagination">
                                <div class="control-group">
                                    <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                        <asp:Repeater ID="rptReceived" runat="server">
                                            <HeaderTemplate>
                                                <table id="data-table" class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <tr>
                                                                <th>
                                                                    <asp:Label ID="lbl1" runat="server" Text="Operator<br/>Card No"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="lbl2" runat="server" Text="Finishing<br/>Floor"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="lbl3" runat="server" Text="Issue<br/>Date"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="lbl4" runat="server" Text="Buyer<br/>Name"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label8" runat="server" Text="Style<br/>Name"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label9" runat="server" Text="Color<br/>Name"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label10" runat="server" Text="Issued<br/>Quantity"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label7" runat="server" Text="Linked<br/>Quantity"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label3" runat="server" Text="Return<br/>Quantity"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label12" runat="server" Text="Wastage<br/>Quantity"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label13" runat="server" Text="Take<br/>Actions"></asp:Label></th>
                                                            </tr>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%#Eval("OPCardNo") %></td>
                                                    <td><%#Eval("FloorName") %></td>
                                                    <td><%#string.Format("{0:dd-MMM-yyyy}",Eval("IssueDate")) %></td>
                                                    <td><%#Eval("BuyerName") %></td>
                                                    <td><%#Eval("StyleName") %></td>
                                                    <td><%#Eval("ColorDescription") %></td>
                                                    <td><%#Eval("IssueQty") %></td>
                                                    <td>
                                                        <asp:TextBox ID="tbxReceivedQty" TextMode="Number" Width="70" Text='<%#Eval("IssueQty") %>' runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="tbxReturnQty" TextMode="Number" Width="70" Text="" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="tbxWastageQty" TextMode="Number" Width="70" Text="" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkbtnReceiveIt" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id")+","+Eval("IsReceived")+","+Eval("IssueQty") %>' OnCommand="lnkbtnReceiveIt_Command" class="btn btn-success btn-small hidden-phone" Text='<%#Eval("IsReceived").ToString() == "1" ? "Update":"Receive" %>'></asp:LinkButton>

                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                </table>
                            <div class="clearfix"></div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <asp:Label ID="Label6" runat="server" Text="knitting delivery entries not found!" Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
    <script src="../../js/jquery.dataTables.js"></script>
</asp:Content>
