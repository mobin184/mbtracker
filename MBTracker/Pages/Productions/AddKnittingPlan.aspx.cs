﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using System.Globalization;
using Repositories;
using MBTracker.EF;

namespace MBTracker.Pages.Productions
{
    public partial class AddKnittingPlan : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        ProductionManager productionManager = new ProductionManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        DataTable dtMachineBrands;
        DataTable dtProductionUnits = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                if (Request.QueryString["machineAssignedId"] != null)
                {
                    MachineAssignedId = int.Parse(Tools.UrlDecode(Request.QueryString["machineAssignedId"]));
                    var planInfo = unitOfWork.GenericRepositories<MachinesAssignedToOrder>().GetByID(MachineAssignedId);
                    MachineBrandId = planInfo.MachineBrandId;
                    UnitId = planInfo.UnitId;
                    var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(planInfo.StyleId);
                    BindStylesByBuyer(styleInfo.BuyerId);
                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleInfo.Id);

                    ddlBuyers.Enabled = false;
                    ddlStyles.Enabled = false;


                    LoadPlanInfo();
                    CommonMethods.LoadProductionMonthDropDown(ddlMonthYears, 1, 0);
                    CalculateProductions();
                    lblTitle.Text = "Update Knitting Plan";
                }
            }
        }

        int MachineAssignedId
        {
            set { ViewState["machineAssignedId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["machineAssignedId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        int UnitId
        {
            set { ViewState["unitId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["unitId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        int MachineBrandId
        {
            set { ViewState["machineBrandId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["machineBrandId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        string MachineBrandName
        {
            set { ViewState["machineBrandName"] = value; }
            get
            {
                try
                {
                    return ViewState["machineBrandName"]?.ToString();
                }
                catch
                {
                    return "";
                }
            }
        }

        int KnittingTime
        {
            set { ViewState["knittingTime"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["knittingTime"]);
                }
                catch
                {
                    return 0;
                }
            }
        }



        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlStyles.Items.Clear();
            pnlPlanningInfo.Visible = false;
            pnlShipmentInfo.Visible = false;
            pnlAssignMachinesToOrder.Visible = false;
            pnlMachinesAvailable.Visible = false;
            // ddlOrders.Items.Clear();

            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            //else
            //{
            //    pnlPlanningInfo.Visible = false;
            //    pnlShipmentInfo.Visible = false;
            //    pnlAssignMachinesToOrder.Visible = false;
            //    //pnlMonthYearSelection.Visible = false;
            //    pnlMachinesAvailable.Visible = false;
            //}
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ddlOrders.Items.Clear();
            if (ddlStyles.SelectedValue != "")
            {
                //BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
                CommonMethods.LoadProductionMonthDropDown(ddlMonthYears, 1, 0);
                LoadPlanInfo();
            }
            else
            {
                pnlPlanningInfo.Visible = false;
                pnlShipmentInfo.Visible = false;
                pnlAssignMachinesToOrder.Visible = false;
                //pnlMonthYearSelection.Visible = false;
                pnlMachinesAvailable.Visible = false;
            }


        }

        //private void BindOrdersByStyle(int styleId)
        //{
        //    CommonMethods.LoadOrderDropdownByStyle(ddlOrders, styleId, 1, 0);
        //}

        //protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        //{


        //    if (ddlOrders.SelectedValue != "")
        //    {

        //        CommonMethods.LoadProductionMonthDropDown(ddlMonthYears, 1, 0);
        //        LoadPlanInfo();

        //    }
        //    else
        //    {
        //        pnlPlanningInfo.Visible = false;
        //        pnlShipmentInfo.Visible = false;
        //        pnlAssignMachinesToOrder.Visible = false;
        //        //pnlMonthYearSelection.Visible = false;
        //        pnlMachinesAvailable.Visible = false;

        //    }
        //}

        private void LoadPlanInfo()
        {
            DataTable dt = new DataTable();


            var styleId = Convert.ToInt32(ddlStyles.SelectedValue);
            dt = productionManager.GetProductionPlanningInfo(styleId);
            if (dt.Rows.Count < 1)
            {
                dt = orderManager.GetProductionPlanningInfo(styleId);
            }

            if (dt.Rows.Count > 0)
            {
                KnittingTime = Convert.ToInt32(dt.Rows[0][8].ToString());
                //MachineBrandId = Convert.ToInt32(dt.Rows[0][3].ToString());

                dtMachineBrands = orderManager.GetMachineBrands();

                rptPlanInfo.DataSource = dt;
                rptPlanInfo.DataBind();
                lblNoPlanningInfo.Visible = false;
                rptPlanInfo.Visible = true;
                pnlPlanningInfo.Visible = true;
                LoadShipmentInfo(styleId);
                if (MachineAssignedId != 0)
                {
                    btnSaveMachinesForOrder.Visible = false;
                    btnUpdateMachinesForOrder.Visible = true;
                    lblMachinePlanning.Text = "Update Knitting Plan:";
                }
                else
                {
                    btnSaveMachinesForOrder.Visible = true;
                    btnUpdateMachinesForOrder.Visible = false;
                    lblMachinePlanning.Text = "Add a Knitting Plan:";
                }
            }
            else
            {
                rptPlanInfo.DataSource = null;
                rptPlanInfo.DataBind();
                rptPlanInfo.Visible = false;
                lblNoPlanningInfo.Visible = true;
                pnlPlanningInfo.Visible = false;

                pnlShipmentInfo.Visible = false;
                pnlAssignMachinesToOrder.Visible = false;
                //pnlMonthYearSelection.Visible = false;
                pnlMachinesAvailable.Visible = false;

            }
            BindTotalOrderQuantity(styleId);
        }
        private void BindTotalOrderQuantity(int styleId)
        {
            divTotalOrderQuantity.Visible = true;
            var totalOrderQty = unitOfWork.GetSingleValue($"Exec usp_GetActiveOrderQuantityByStyleId {styleId}");
            divTotalOrderQuantity.InnerHtml = $"<div style='width:100%; padding-bottom:9px; margin-bottom:5px; font-size:13px; padding:10px; background-color:#ffff00aa; font-weight:bold'>Order Quantity: <span style='font-size:15px;'>{totalOrderQty}</span>    &nbsp;&nbsp;<asp:LinkButton ID='lnkbtnView' onClick='window.open(\"../Reports/SummaryOfOrdersV2.aspx?styleId={Tools.UrlEncode(styleId + "")}\",\"_blank\")' class='btn btn-success btn-mini hidden-phone pull-right' Text='View Details'>View Details</asp:LinkButton></div>";
        }
        private void LoadShipmentInfo(int styleId)
        {

            pnlShipmentInfo.Visible = true;

            DataTable dt = new DataTable();
            dt = orderManager.GetShipmentInfoByStyleId(styleId);

            if (dt.Rows.Count > 0)
            {
                rptShipmentSummary.DataSource = dt;
                rptShipmentSummary.DataBind();

                rptShipmentSummary.Visible = true;
                lblNoShippingInfo.Visible = false;

                pnlAssignMachinesToOrder.Visible = true;
                //pnlMonthYearSelection.Visible = true;

                LoadProductionUnitsByMachineBrand(styleId);
            }
            else
            {
                rptShipmentSummary.DataSource = null;
                rptShipmentSummary.DataBind();

                rptShipmentSummary.Visible = false;
                lblNoShippingInfo.Visible = true;

                pnlAssignMachinesToOrder.Visible = false;
                //pnlMonthYearSelection.Visible = false;
                pnlMachinesAvailable.Visible = false;

            }
        }




        private void LoadProductionUnitsByMachineBrand(int styleId)
        {
            //DataTable dt = new DataTable();
            dtProductionUnits = productionManager.GetProductionUnitsByMachineBrandOfAStyle(styleId);
            if (MachineAssignedId != 0)
            {
                DataRow[] result = dtProductionUnits.Select();
                foreach (DataRow row in result)
                {
                    if (int.Parse(row["ProductionUnitId"].ToString()) != UnitId)
                        dtProductionUnits.Rows.Remove(row);
                }
            }

            rptIssueQuantityAndMachinesPerUnit.DataSource = dtProductionUnits;
            rptIssueQuantityAndMachinesPerUnit.DataBind();
            pnlAssignMachinesToOrder.Visible = true;





        }

        protected void ddlMonthYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlMonthYears.SelectedValue != "")
            {

                LoadMonthlyDates(Convert.ToInt32(ddlMonthYears.SelectedValue));
                pnlMachinesAvailable.Visible = true;
            }
            else
            {
                pnlMachinesAvailable.Visible = false;
            }
        }

        private void LoadMonthlyDates(int monthYearId)
        {
            DataTable dtMonthYearInfo = new DataTable();
            dtMonthYearInfo = orderManager.GetMonthYearInfo(monthYearId);

            string monthName = dtMonthYearInfo.Rows[0][1].ToString();
            string yearName = dtMonthYearInfo.Rows[0][2].ToString();
            int numberOfDays = Convert.ToInt32(dtMonthYearInfo.Rows[0][3].ToString());

            DataTable monthlyDates = MonthlyDates();
            string leadingZero = "0";


            for (int i = 1; i <= numberOfDays; i++)
            {
                if (i.ToString().Length == 1)
                {
                    monthlyDates.Rows.Add((leadingZero + i.ToString()) + " - " + monthName + " - " + yearName);
                }
                else
                {
                    monthlyDates.Rows.Add(i.ToString() + " - " + monthName + " - " + yearName);
                }
            }

            rptMonthlyDates.DataSource = monthlyDates;
            rptMonthlyDates.DataBind();

            Label lblHeaderMessage = (Label)rptMonthlyDates.Controls[0].Controls[0].FindControl("lblAvailableMachineMessage");
            lblHeaderMessage.Text = "Availability of Machines - Brand: " + MachineBrandName;

        }


        public DataTable MonthlyDates()
        {
            DataTable dt = new DataTable("MonthlyDates");
            dt.Columns.Add("Date", typeof(string));
            return dt;
        }


        protected void rptPlanInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlMCBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                ddlMCBrands.DataTextField = "MachineBrand";
                ddlMCBrands.DataValueField = "Id";
                ddlMCBrands.DataSource = dtMachineBrands;
                ddlMCBrands.DataBind();

                ddlMCBrands.SelectedValue = DataBinder.Eval(e.Item.DataItem, "MachineBrandId").ToString();
            }
        }


        protected void rptPlanInfo_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //int knittingMachineBrandId = 0;

            //if (e.CommandName == "Edit")
            //{
            //    e.Item.FindControl("lblMCBrandName").Visible = false;
            //    e.Item.FindControl("ddlMachineBrands").Visible = true;

            //    e.Item.FindControl("lblMCGauge").Visible = false;
            //    e.Item.FindControl("tbxKnittingMCGauge").Visible = true;

            //    e.Item.FindControl("lblKnittingTime").Visible = false;
            //    e.Item.FindControl("tbxKnittingTime").Visible = true;

            //    e.Item.FindControl("lnkbtnEdit").Visible = false;
            //    e.Item.FindControl("lnkbtnUpdate").Visible = true;
            //    e.Item.FindControl("lnkbtnCancel").Visible = true;

            //}
            //else if (e.CommandName == "Update")
            //{

            //    DropDownList ddlKnittingMCBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");

            //    if (ddlKnittingMCBrands.SelectedValue != "")
            //    {
            //        knittingMachineBrandId = Convert.ToInt32(ddlKnittingMCBrands.SelectedValue);
            //    }
            //    TextBox tbxKnittingMCGauge = (TextBox)e.Item.FindControl("tbxKnittingMCGauge");
            //    TextBox tbxKnittingTime = (TextBox)e.Item.FindControl("tbxKnittingTime");


            //    UpdateMachineInfoForProduction(Convert.ToInt32(ddlOrders.SelectedValue), knittingMachineBrandId, tbxKnittingMCGauge.Text, tbxKnittingTime.Text);

            //    LoadPlanInfo();

            //}
            //else if (e.CommandName == "Cancel")
            //{
            //    LoadPlanInfo();
            //}

        }

        //private void UpdateMachineInfoForProduction(int orderId, int knittingMachineBrandId, string knittingMachineGauge, string knittingTime)
        //{

        //    int updateResult = productionManager.InsertOrderProductionMachineInfo(orderId, knittingMachineBrandId, knittingMachineGauge, knittingTime, CommonMethods.SessionInfo.UserName, DateTime.Now);
        //    if (updateResult > 0)
        //    {
        //        ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Planning info was updated successfully.')", true);
        //    }
        //}


        protected void rptShipmentSummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label machinesRequired = (Label)e.Item.FindControl("lblMCRequired");
                double numMachinesRequired = Math.Ceiling((Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "ShipmentQuantity").ToString()) * KnittingTime) / 1320);
                machinesRequired.Text = numMachinesRequired.ToString();

            }
        }


        protected void rptMonthlyDates_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptMachinesAvailable = (Repeater)e.Item.FindControl("rptMachinesAvailable");
                //DateTime productionDate = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "Date").ToString());

                if (dtProductionUnits.Rows.Count <= 0)
                {
                    dtProductionUnits = productionManager.GetProductionUnitsByMachineBrandOfAStyle(Convert.ToInt32(ddlStyles.SelectedValue));
                }
                //DataTable dt = productionManager.GetAvailableMachines(MachineBrandId, productionDate, Convert.ToInt32(ddlStyles.SelectedValue));

                rptMachinesAvailable.DataSource = dtProductionUnits;
                rptMachinesAvailable.DataBind();

            }
        }

        protected void btnSaveMachinesForOrder_Click(object sender, EventArgs e)
        {

            DataTable dtOrderAssignedToProdMachines = MachinesAssignedToOrder();
            int hasRow = 0;
            bool isValid = true;
            bool machineAvailable = true;
            int rowCount = 0;
            int machinesAssignedToOrderResult = 0;

            var styleId = Convert.ToInt32(ddlStyles.SelectedValue);

            for (int i = 0; i < rptIssueQuantityAndMachinesPerUnit.Items.Count; i++)
            {
                var rptUnitMachines = (Repeater)rptIssueQuantityAndMachinesPerUnit.Items[i].FindControl("rptUnitMachines");
                Label lblProdUnitId = (Label)rptIssueQuantityAndMachinesPerUnit.Items[i].FindControl("lblUnitId");
                Label lblUnitName = (Label)rptIssueQuantityAndMachinesPerUnit.Items[i].FindControl("lblUnitName");
                for (int j = 0; j < rptUnitMachines.Items.Count; j++)
                {
                    TextBox tbxIssueQty = (TextBox)rptUnitMachines.Items[j].FindControl("tbxIssueQty");
                    TextBox tbxNumMachines = (TextBox)rptUnitMachines.Items[j].FindControl("tbxNumMC");
                    TextBox tbxStartDate = (TextBox)rptUnitMachines.Items[j].FindControl("tbxFromDate");
                    TextBox tbxEndDate = (TextBox)rptUnitMachines.Items[j].FindControl("tbxToDate");
                    Label lblMCBrandId = (Label)rptUnitMachines.Items[j].FindControl("lblMCBrandId");
                    Label lblProductionQty = (Label)rptUnitMachines.Items[j].FindControl("lblProductionQty");

                    if (tbxIssueQty.Text != "" && tbxNumMachines.Text != "" && tbxStartDate.Text != "" && tbxEndDate.Text != "")
                    {
                        rowCount++;
                        //if (int.Parse(tbxIssueQty.Text) > int.Parse(lblProductionQty.Text))
                        //{
                        //    isValid = false;
                        //    break;
                        //}
                        var startDate = DateTime.Parse(tbxStartDate.Text);
                        var endDate = DateTime.Parse(tbxEndDate.Text);
                        var unitId = int.Parse(lblProdUnitId.Text);
                        var noOfMc = int.Parse(tbxNumMachines.Text);
                        var unitName = lblUnitName.Text;
                        var brandId = int.Parse(lblMCBrandId.Text);
                        while (startDate <= endDate)
                        {
                            string sql = $"Exec usp_GetAvailableMachines_PerProductionUnitByStyleDateAndMachineBrand '{startDate}',{styleId},{unitId},{brandId}";
                            var dt = unitOfWork.GetDataTableFromSql(sql);
                            var McAvailable = dt.Rows[0].Field<int>("MachinesAvailable");
                            var BrandName = dt.Rows[0].Field<string>("BrandName");
                            //if (noOfMc > McAvailable)
                            //{
                            //    var msg = $"Unit Name: {unitName}<br />Machine Brand: {BrandName} <br />Production Date: {startDate.ToString("dd-MMM-yyyy")} <br /> Available Machine: {McAvailable}";
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + "Information" + "','" + msg + "','','350','true');", true);
                            //    machineAvailable = false;
                            //    break;
                            //}
                            var st = @"<table class=""table table-bordered table-hover""><tr><th>Knitting Unit</th><th>Machine Brand</th><th>Knitting Date</th><th>Available Machine</th></tr>";
                            if (noOfMc > McAvailable)
                            {
                                // var msg = $"Unit Name: {unitName}<br />Machine Brand: {BrandName} <br />Production Date: {startDate.ToString("dd-MMM-yyyy")} <br /> Available Machine: {McAvailable}";
                                st += $"<tr><td>{unitName}</td><td>{BrandName}</td><td>{startDate.ToString("dd-MMM-yyyy")}</td><td>{McAvailable}</td></tr></table>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + "Information" + "','" + st + "','','350','true');", true);
                                machineAvailable = false;
                                break;
                            }
                            startDate = startDate.AddDays(1);
                        }
                    }
                }
            }


            if (rowCount > 0)
            {
                if (machineAvailable)
                {
                    if (isValid)
                    {
                        for (int i = 0; i < rptIssueQuantityAndMachinesPerUnit.Items.Count; i++)
                        {
                            var rptUnitMachines = (Repeater)rptIssueQuantityAndMachinesPerUnit.Items[i].FindControl("rptUnitMachines");
                            Label lblProdUnitId = (Label)rptIssueQuantityAndMachinesPerUnit.Items[i].FindControl("lblUnitId");
                            for (int j = 0; j < rptUnitMachines.Items.Count; j++)
                            {
                                TextBox tbxIssueQty = (TextBox)rptUnitMachines.Items[j].FindControl("tbxIssueQty");
                                TextBox tbxNumMachines = (TextBox)rptUnitMachines.Items[j].FindControl("tbxNumMC");
                                TextBox tbxStartDate = (TextBox)rptUnitMachines.Items[j].FindControl("tbxFromDate");
                                TextBox tbxEndDate = (TextBox)rptUnitMachines.Items[j].FindControl("tbxToDate");
                                Label lblMCBrandId = (Label)rptUnitMachines.Items[j].FindControl("lblMCBrandId");

                                if (tbxIssueQty.Text != "" && tbxNumMachines.Text != "" && tbxStartDate.Text != "" && tbxEndDate.Text != "")
                                {
                                    dtOrderAssignedToProdMachines.Rows.Add(styleId, Convert.ToInt32(lblProdUnitId.Text), int.Parse(lblMCBrandId.Text), Convert.ToInt32(tbxIssueQty.Text), Convert.ToInt32(tbxNumMachines.Text), Convert.ToDateTime(tbxStartDate.Text), Convert.ToDateTime(tbxEndDate.Text), CommonMethods.SessionInfo.UserName, DateTime.Now);
                                    hasRow++;
                                }
                            }
                        }

                        if (hasRow > 0)
                        {
                            machinesAssignedToOrderResult = productionManager.SaveMachinesAssignedToOrder(dtOrderAssignedToProdMachines);
                            if (machinesAssignedToOrderResult > 0)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "returnToPage('AddKnittingPlan.aspx');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter correct information.')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Issue quantity can not be greater than production quantity.')", true);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter correct information.')", true);
            }
        }

        public DataTable MachinesAssignedToOrder()
        {
            DataTable dt = new DataTable("MachinesAssignedToOrder");
            dt.Columns.Add("StyleId", typeof(int));
            dt.Columns.Add("UnitId", typeof(int));
            dt.Columns.Add("MachineBrandId", typeof(int));
            dt.Columns.Add("IssueQuantity", typeof(int));
            dt.Columns.Add("NumberOfMachines", typeof(int));
            dt.Columns.Add("ProdStartDate", typeof(string));
            dt.Columns.Add("ProdEndDate", typeof(string));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(string));
            return dt;
        }


        protected void tbxNumMC_TextChanged(object sender, EventArgs e)
        {
            CalculateProductions();
        }

        protected void tbxFromDate_TextChanged(object sender, EventArgs e)
        {
            CalculateProductions();
        }

        protected void tbxToDate_TextChanged(object sender, EventArgs e)
        {
            CalculateProductions();
        }



        private void CalculateProductions()
        {

            int totalProduction = 0;
            string st = "";
            var machineValidationFlag = true;
            for (int i = 0; i < rptIssueQuantityAndMachinesPerUnit.Items.Count; i++)
            {
                var rptUnitMachines = (Repeater)rptIssueQuantityAndMachinesPerUnit.Items[i].FindControl("rptUnitMachines");
                var lblUnitId = (Label)rptIssueQuantityAndMachinesPerUnit.Items[i].FindControl("lblUnitId");
                var unitId = int.Parse(lblUnitId.Text);
                for (int j = 0; j < rptUnitMachines.Items.Count; j++)
                {
                    TextBox tbxNumMachines = (TextBox)rptUnitMachines.Items[j].FindControl("tbxNumMC");
                    TextBox tbxStartDate = (TextBox)rptUnitMachines.Items[j].FindControl("tbxFromDate");
                    TextBox tbxEndDate = (TextBox)rptUnitMachines.Items[j].FindControl("tbxToDate");
                    Label lblProductionQty = (Label)rptUnitMachines.Items[j].FindControl("lblProductionQty");
                    Label lblMCBrandId = (Label)rptUnitMachines.Items[j].FindControl("lblMCBrandId");
                    Label lblKnittingTime = (Label)rptUnitMachines.Items[j].FindControl("lblKnittingTime");
                    TextBox tbxIssueQty = (TextBox)rptUnitMachines.Items[j].FindControl("tbxIssueQty");
                    var knittingTime = int.Parse(lblKnittingTime.Text);
                    var machineBrandId = int.Parse(lblMCBrandId.Text);
                    if (tbxNumMachines.Text != "" && tbxStartDate.Text != "" && tbxEndDate.Text != "")
                    {
                        DateTime startDate = Convert.ToDateTime(tbxStartDate.Text);
                        DateTime endDate = Convert.ToDateTime(tbxEndDate.Text);
                        int numOfProdDays = 0;
                        if (startDate > endDate)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('End date should be greater than start date.');", true);
                        }
                        else
                        {
                            numOfProdDays = (endDate - startDate).Days + 1;
                        }
                        var numOfMachine = Convert.ToInt32(tbxNumMachines.Text);
                        double numProduced = ((numOfProdDays * numOfMachine * 1320) / knittingTime);
                        var prodQty = Convert.ToInt32(Math.Floor(numProduced));
                        lblProductionQty.Text = prodQty + "";
                        tbxIssueQty.Text = prodQty + "";
                        totalProduction = totalProduction + prodQty;
                        if (MachineAssignedId == 0)
                        {
                            st = @"<table class=""table table-bordered table-hover""><tr><th>Knitting Unit</th><th>Machine Brand</th><th>Knitting Date</th><th>Available Machine</th></tr>";
                            while (startDate <= endDate)
                            {
                                var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetAvailableMachinesByProductionUnitStyleDateAndMachineBrand '{startDate.ToString("yyyy-MM-dd")}','{machineBrandId}','{unitId}'");
                                var availableMachine = int.Parse(dt.Rows[0]["MachinesAvailable"]?.ToString());
                                if (numOfMachine > availableMachine)
                                {
                                    var brandName = dt.Rows[0]["BrandName"]?.ToString();
                                    var kDate = dt.Rows[0]["ProductionDate"]?.ToString();
                                    var unitName = dt.Rows[0]["UnitName"]?.ToString();
                                    machineValidationFlag = false;
                                    st += $"<tr><td>{unitName}</td><td>{brandName}</td><td>{kDate}</td><td>{availableMachine}</td></tr>";
                                }
                                startDate = startDate.AddDays(1);
                            }
                            st += "</table>";
                            if (!machineValidationFlag)
                            {
                                string title = "Information:";
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + st + "','','350');", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + st + "','','350','true');", true);
                            }
                        }
                    }
                    else
                    {
                        lblProductionQty.Text = "";
                        tbxIssueQty.Text = "";
                    }
                }

            }

            lblTotalProductions.Text = "Total Production: " + totalProduction.ToString() + " Pcs.";
        }

        protected void rptIssueQuantityAndMachinesPerUnit_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var unitId = int.Parse(((Label)e.Item.FindControl("lblUnitId")).Text);
                var rptUnitMachines = (Repeater)e.Item.FindControl("rptUnitMachines");
                var styleId = int.Parse(ddlStyles.SelectedValue);
                var dt = unitOfWork.GetDataTableFromSql($"Exec usp_GetProductionUnitsMachineBrandByStyle {unitId},{styleId}");
                if (MachineAssignedId != 0)
                {
                    DataRow[] result = dt.Select();
                    foreach (DataRow row in result)
                    {
                        if (int.Parse(row["Id"].ToString()) != MachineBrandId)
                            dt.Rows.Remove(row);
                    }
                }
                rptUnitMachines.DataSource = dt;
                rptUnitMachines.DataBind();
            }
        }


        protected void rptMachinesAvailable_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var unitId = int.Parse(((Label)e.Item.FindControl("lblUnitId")).Text);
                var rptUnitMachinesAvailable = (Repeater)e.Item.FindControl("rptUnitMachinesAvailable");
                var productionDate = DateTime.Parse(((Label)((Repeater)sender).Parent.FindControl("lblProducitonDate")).Text).ToString("yyyy-MM-dd");
                var styleId = int.Parse(ddlStyles.SelectedValue);
                string sql = $"Exec usp_GetAvailableMachines_PerProductionUnitByStyleAndDate '{productionDate}',{styleId},{unitId}";
                var dt = unitOfWork.GetDataTableFromSql(sql);
                rptUnitMachinesAvailable.DataSource = dt;
                rptUnitMachinesAvailable.DataBind();
            }
        }

        protected void btnUpdateMachinesForOrder_Click(object sender, EventArgs e)
        {
            try
            {

                var rptUnitMachines = (Repeater)rptIssueQuantityAndMachinesPerUnit.Items[0].FindControl("rptUnitMachines");

                TextBox tbxIssueQty = (TextBox)rptUnitMachines.Items[0].FindControl("tbxIssueQty");
                TextBox tbxNumMachines = (TextBox)rptUnitMachines.Items[0].FindControl("tbxNumMC");
                TextBox tbxStartDate = (TextBox)rptUnitMachines.Items[0].FindControl("tbxFromDate");
                TextBox tbxEndDate = (TextBox)rptUnitMachines.Items[0].FindControl("tbxToDate");
                Label lblMCBrandId = (Label)rptUnitMachines.Items[0].FindControl("lblMCBrandId");
                Label lblProductionQty = (Label)rptUnitMachines.Items[0].FindControl("lblProductionQty");
                Label lblProdUnitId = (Label)rptIssueQuantityAndMachinesPerUnit.Items[0].FindControl("lblUnitId");
                Label lblUnitName = (Label)rptIssueQuantityAndMachinesPerUnit.Items[0].FindControl("lblUnitName");
                var styleId = int.Parse(ddlStyles.SelectedValue);
                if (tbxIssueQty.Text != "" && tbxNumMachines.Text != "" && tbxStartDate.Text != "" && tbxEndDate.Text != "")
                {
                    //if (int.Parse(tbxIssueQty.Text) > int.Parse(lblProductionQty.Text))
                    //{
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Issue quantity can not be greater than production quantity.')", true);
                    //}
                    //else
                    //{


                    var planInfo = unitOfWork.GenericRepositories<MachinesAssignedToOrder>().GetByID(MachineAssignedId);

                    var startDate = DateTime.Parse(tbxStartDate.Text);
                    var endDate = DateTime.Parse(tbxEndDate.Text);
                    var unitId = int.Parse(lblProdUnitId.Text);
                    var noOfMc = int.Parse(tbxNumMachines.Text);
                    var unitName = lblUnitName.Text;
                    var brandId = int.Parse(lblMCBrandId.Text);
                    var machineAvailable = true;
                    while (startDate <= endDate)
                    {

                        string sql = $"Exec usp_GetAvailableMachines_PerProductionUnitByStyleDateAndMachineBrand '{startDate}',{styleId},{unitId},{brandId}";
                        var dt = unitOfWork.GetDataTableFromSql(sql);
                        var McAvailable = dt.Rows[0].Field<int>("MachinesAvailable");
                        var BrandName = dt.Rows[0].Field<string>("BrandName");

                        if (startDate >= planInfo.ProdStartDate && startDate <= planInfo.ProdEndDate)
                        {
                            McAvailable += planInfo.NumberOfMachines;
                        }

                        var st = @"<table class=""table table-bordered table-hover""><tr><th>Knitting Unit</th><th>Machine Brand</th><th>Knitting Date</th><th>Available Machine</th></tr>";
                        if (noOfMc > McAvailable)
                        {
                            // var msg = $"Unit Name: {unitName}<br />Machine Brand: {BrandName} <br />Production Date: {startDate.ToString("dd-MMM-yyyy")} <br /> Available Machine: {McAvailable}";
                            st += $"<tr><td>{unitName}</td><td>{BrandName}</td><td>{startDate.ToString("dd-MMM-yyyy")}</td><td>{McAvailable}</td></tr></table>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + "Information" + "','" + st + "','','350','true');", true);
                            machineAvailable = false;
                            break;
                        }
                        startDate = startDate.AddDays(1);
                    }

                    if (machineAvailable)
                    {
                        planInfo.IssueQuantity = int.Parse(tbxIssueQty.Text);
                        planInfo.NumberOfMachines = int.Parse(tbxNumMachines.Text);
                        planInfo.ProdStartDate = DateTime.Parse(tbxStartDate.Text);
                        planInfo.ProdEndDate = DateTime.Parse(tbxEndDate.Text);
                        unitOfWork.GenericRepositories<MachinesAssignedToOrder>().Update(planInfo);
                        unitOfWork.Save();

                        CommonMethods.SendEventNotification(7, int.Parse(ddlBuyers.SelectedValue), int.Parse(ddlStyles.SelectedValue), null, MachineAssignedId);

                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewKnittingPlans.aspx');", true);
                    }
                    //}
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter correct information.')", true);
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        protected void rptUnitMachines_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (MachineAssignedId != 0)
                {
                    var tbxIssueQty = (TextBox)e.Item.FindControl("tbxIssueQty");
                    var tbxNumMC = (TextBox)e.Item.FindControl("tbxNumMC");
                    var tbxFromDate = (TextBox)e.Item.FindControl("tbxFromDate");
                    var tbxToDate = (TextBox)e.Item.FindControl("tbxToDate");

                    var planInfo = unitOfWork.GenericRepositories<MachinesAssignedToOrder>().GetByID(MachineAssignedId);
                    tbxIssueQty.Text = planInfo.IssueQuantity + "";
                    tbxNumMC.Text = planInfo.NumberOfMachines + "";
                    tbxFromDate.Text = planInfo.ProdStartDate.ToString("yyyy-MM-dd");
                    tbxToDate.Text = planInfo.ProdEndDate.ToString("yyyy-MM-dd");
                }
            }
        }
    }
}