﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class ViewDeliveredKnittedPartsToFinishingUnit : Page
    {
        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableEditButton();
                EnableDisableDeleteButton();
                //tbxDelioverdDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteKnittedPartsDeliveryInfo(knittedDeliverdId);
                }
            }
        }

        private void DeleteKnittedPartsDeliveryInfo(int knittedDeliverdId)
        {
            try
            {
                var lstColorSizes = unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnitColorSizes>().Get(x => x.KnittedDeliveredId == knittedDeliverdId).ToList();
                foreach (var item in lstColorSizes)
                {
                    unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnitColorSizes>().Delete(item);
                }
                unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnit>().Delete(knittedDeliverdId);
                unitOfWork.Save();
                LoadData();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        private void EnableDisableDeleteButton()
        {
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");
        }
        private void EnableDisableEditButton()
        {
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Knitting - Mgmt,Knitting - Floor");
        }
        protected void btnViewEntries_Click(object sender, EventArgs e)
        {
            try
            {
                LoadData();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void LoadData()
        {
            divSummary.Visible = false;
            divColors.Visible = false;
            rptEntryInfo.DataSource = null;
            rptEntryInfo.DataBind();
            if (!string.IsNullOrEmpty(tbxDelioverdDate.Text))
            {
                var delDate = DateTime.Parse(tbxDelioverdDate.Text);
                var dtEntries = unitOfWork.GetDataTableFromSql($"Exec usp_GetKnittedPartsDeliverdToFinishingUnitByUserAndDate {CommonMethods.SessionInfo.UserId},'{delDate}'");
                if (dtEntries.Rows.Count > 0)
                {
                    rptSummary.DataSource = dtEntries;
                    rptSummary.DataBind();
                    rptSummary.Visible = true;
                    lblEntryNotFound.Visible = false;
                    dataDiv.Visible = true;
                }
                else
                {
                    rptSummary.DataSource = null;
                    rptSummary.DataBind();
                    rptSummary.Visible = false;
                    lblEntryNotFound.Visible = true;
                    dataDiv.Visible = false;
                }
                divSummary.Visible = true;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select received date.')", true);
            }
        }
        protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        {
            knittedDeliverdId = int.Parse(e.CommandArgument.ToString());
            var dtRecColors = unitOfWork.GetDataTableFromSql($"Exec usp_GetKnittedDeliveredToFinishingUnitDetailsByDeliveredId {knittedDeliverdId}");
            divColors.Visible = true;
            if (dtRecColors.Rows.Count > 0)
            {
                rptEntryInfo.DataSource = dtRecColors;
                rptEntryInfo.DataBind();
                rptEntryInfo.Visible = true;
                lblNoDataFound.Visible = false;
            }
            else
            {
                rptEntryInfo.DataSource = null;
                rptEntryInfo.DataBind();
                rptEntryInfo.Visible = false;
                lblNoDataFound.Visible = true;
            }
            ScriptManager.RegisterStartupScript(this,this.GetType(), "message", "scrollDown()", true);
        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            var knittingDeliveredId = int.Parse(e.CommandArgument.ToString());
            Response.Redirect("DeliverKnittedPartsToFinishingUnitV2.aspx?knittingDeliveredId=" + Tools.UrlEncode(knittingDeliveredId+""));
        }

        int knittedDeliverdId
        {
            set { ViewState["knittingReceivedId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["knittingReceivedId"]);
                }
                catch
                {
                    return 0;
                }
            }

        }
        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var buyerId = int.Parse(((Label)e.Item.FindControl("lblBuyerId")).Text);
                var dtSizes = buyerManager.GetSizes(buyerId);
                GridView gvSizeQuantity1 = (GridView)e.Item.FindControl("gvSizeQuantity1");
                Label lblNoSizeFound = (Label)e.Item.FindControl("lblNoSizeFound");
                if (dtSizes.Rows.Count > 0)
                {
                    DataTable dtOneRow = new DataTable();
                    DataRow dr = null;
                    dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                    dr = dtOneRow.NewRow();
                    dr["RowNumber"] = 1;
                    dtOneRow.Rows.Add(dr);

                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantity1.Columns.Add(templateField);
                    }

                    gvSizeQuantity1.DataSource = dtOneRow;
                    gvSizeQuantity1.DataBind();
                    gvSizeQuantity1.Visible = true;
                    lblNoSizeFound.Visible = false;

                }
                else
                {
                    gvSizeQuantity1.Visible = false;
                    lblNoSizeFound.Visible = true;
                }
            }
        }

        protected void gvSizeQuantity1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var buyerId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId")).Text);
                var styleId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId")).Text);
                var dtSizes = buyerManager.GetSizes(buyerId);
                Label txtSizeQty = null;
                Label lblSizeId = null;

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new Label();
                    txtSizeQty.ID = "tbxReceivQuantity" + i.ToString();
                    txtSizeQty.Text = "";
                    txtSizeQty.Width = 70;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);


                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);

                    var entryId = new Label();
                    entryId.ID = "lblentryId" + i.ToString();
                    entryId.Text = "";
                    entryId.Visible = false;
                    e.Row.Cells[i].Controls.Add(entryId);


                    var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());
                    var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                    var buyerColorId = int.Parse(string.IsNullOrEmpty(lblBuyerColorId.Text) ? "0" : lblBuyerColorId.Text);
                    if (buyerColorId != 0)
                    {
                        var sizeInfo = unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnitColorSizes>().Get(x => x.KnittedDeliveredId == knittedDeliverdId && x.BuyerColorId == buyerColorId && x.SizeId == sizeId).FirstOrDefault();
                        if (sizeInfo != null)
                        {
                            txtSizeQty.Text = sizeInfo.DeliveredQty + "";
                            entryId.Text = sizeInfo.Id + "";
                        }
                    }
                }
            }
        }

        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            knittedDeliverdId = int.Parse(e.CommandArgument.ToString());


            if (knittedDeliverdId != 0)
            {
                //var title = "Warning";
                //var msg = "Are you sure you want to delete?";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);

                Page.ClientScript.RegisterStartupScript(this.GetType(), "toastr_message", "toastr.success('Are you working?')", true);
            }
            else
            {
                var title = "Error";
                var msg = "Sorry, you can not delete it.<br />Something went wrong.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            }
        }
    }
}