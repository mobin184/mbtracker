﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Microsoft.ReportingServices.Interfaces;
using Newtonsoft.Json;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace MBTracker.Pages.Productions
{
    public partial class EnterKnittingEntryV2 : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        ProductionManager productionManager = new ProductionManager();
        DataTable dtUserUnits;
        DataTable dtSizes;
        DataTable dtYarnIssuedForSizes;
        int buyerId;
        int styleId;
        int unitId;
        int buyerColorId;
        int entryRowNumber2 = 0;

        UnitOfWork unitOfWork = new UnitOfWork();
        JsonSerializerSettings setting = new JsonSerializerSettings
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Request.QueryString["unitId"] != null && Request.QueryString["knittingDate"] != null && Request.QueryString["shiftId"] != null)
                {
                    UnitId = int.Parse(Tools.UrlDecode(Request.QueryString["unitId"]));
                    KnittingDate = DateTime.Parse(Tools.UrlDecode(Request.QueryString["knittingDate"]));
                    ShiftId = int.Parse(Tools.UrlDecode(Request.QueryString["shiftId"]));

                    //info = unitOfWork.GenericRepositories<DailyKnittings>().Get(x => x.UnitId == UnitId && x.KnittingDate == KnittingDate).ToList();
                    lblProductionEntry.Text = "Update Daily Knitting Entry:";
                }

                PopulateGeneralInfo();
            }
        }

        int ShiftId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["shiftId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["shiftId"] = value; }
        }
        int UnitId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["unitId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["unitId"] = value; }
        }
        DateTime KnittingDate
        {
            get
            {
                try
                {
                    return Convert.ToDateTime(ViewState["knittingDate"]);
                }
                catch
                {
                    return DateTime.Today;
                }
            }
            set { ViewState["knittingDate"] = value; }
        }

        private void PopulateGeneralInfo()
        {

            dtUserUnits = new CommonManager().GetProductionUnitsByUser(Convert.ToInt32(CommonMethods.SessionInfo.UserId));

            if (dtUserUnits.Rows.Count > 0)
            {
                DataTable dtProdEntryGeneralInfo = ProdEntryGeneralInfo();
                if (UnitId == 0)
                {

                    dtProdEntryGeneralInfo.Rows.Add(String.Format("{0:yyyy-MM-dd}", DateTime.Now), CommonMethods.SessionInfo.UserFullName.ToString());


                    PopulateEntryInfo(Convert.ToInt32(dtUserUnits.Rows[0][0].ToString()), DateTime.Now);
                }
                else
                {
                    dtProdEntryGeneralInfo.Rows.Add(String.Format("{0:yyyy-MM-dd}", KnittingDate), CommonMethods.SessionInfo.UserFullName.ToString());
                    PopulateEntryInfo(UnitId, KnittingDate);
                }

                rptGeneralInfo.DataSource = dtProdEntryGeneralInfo;
                rptGeneralInfo.DataBind();
                lblNoUnitPermission.Visible = false;
            }
            else
            {
                lblNoUnitPermission.Visible = true;
                lnkbtnSaveEntries.Visible = false;
            }
        }


        public DataTable ProdEntryGeneralInfo()
        {
            DataTable dt = new DataTable("ProdEntryGeneralInfo");
            dt.Columns.Add("EntryDate", typeof(string));
            dt.Columns.Add("UserName", typeof(string));
            return dt;
        }


        private void PopulateEntryInfo(int unitId, DateTime productionDate)
        {
            DataTable dt = new DataTable();
            if (UnitId != 0)
            {
                int userId = CommonMethods.SessionInfo.UserId;

                dt = unitOfWork.GetDataTableFromSql($"Exec usp_Get_DailyKnittingEntryInfoByUnitAndDate {unitId},{ShiftId},'{productionDate}','{userId}' ");
            }
            else
            {
                //dt = this.productionManager.GetProductionEntryInfo(unitId, productionDate);
                dt = this.productionManager.GetProductionEntryInfo(0, productionDate);
                dt.Columns.Add("McUsed", typeof(string));
            }

            if (dt.Rows.Count <= 0)
            {
                DataRow newBlankRow = dt.NewRow();
                newBlankRow["BuyerColorId"] = 0;
                newBlankRow["StyleId"] = 0;
                newBlankRow["MachineBrandId"] = 0;
                newBlankRow["FinishingUnitId"] = 0;
                newBlankRow["BuyerId"] = 0;
                newBlankRow["UnitId"] = unitId;
                newBlankRow["ColorDescription"] = "";
                newBlankRow["BrandName"] = "";
                newBlankRow["KnittingMachineGauge"] = "";
                newBlankRow["KnittingTime"] = "";
                newBlankRow["NumberOfMachines"] = 0;
                dt.Rows.InsertAt(newBlankRow, dt.Rows.Count);
                this.lblNoKnittingGoingOn.Visible = false;
                this.pnlKnittingEntry.Visible = true;
                if (this.UnitId != 0)
                {
                    this.lnkbtnSaveEntries.Visible = false;
                    this.lnkbtnUpdateEntries.Visible = true;
                }
                else
                {
                    this.lnkbtnSaveEntries.Visible = true;
                    this.lnkbtnUpdateEntries.Visible = false;
                }
            }
            else
            {
                this.lblNoKnittingGoingOn.Visible = false;
                this.pnlKnittingEntry.Visible = true;
                if (this.UnitId != 0)
                {
                    this.lnkbtnSaveEntries.Visible = false;
                    this.lnkbtnUpdateEntries.Visible = true;
                }
                else
                {
                    this.lnkbtnSaveEntries.Visible = true;
                    this.lnkbtnUpdateEntries.Visible = false;
                }
            }
            this.ViewState["CurrentTable"] = dt;
            this.rptEntryInfo.DataSource = dt;
            this.rptEntryInfo.DataBind();
        }

        protected void rptGeneralInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlUserUnits = (DropDownList)e.Item.FindControl("ddlUserUnits");
                ddlUserUnits.DataTextField = "UnitName";
                ddlUserUnits.DataValueField = "ProductionUnitId";
                ddlUserUnits.DataSource = dtUserUnits;
                ddlUserUnits.DataBind();
                DropDownList ddlShift = (DropDownList)e.Item.FindControl("ddlShift");
                CommonMethods.LoadDropdown(ddlShift, "Shifts", 1, 0);

                var lblEntryDate = (Label)e.Item.FindControl("lblEntryDate");
                var tbxKnittingDate = (TextBox)e.Item.FindControl("tbxKnittingDate");
                if (tbxKnittingDate != null && !string.IsNullOrEmpty(lblEntryDate.Text))
                {
                    tbxKnittingDate.Text = Convert.ToDateTime(lblEntryDate.Text).ToString("yyyy-MM-dd");
                }

                if (UnitId == 0)
                {
                    ddlUserUnits.SelectedIndex = 0;
                }
                else
                {
                    ddlUserUnits.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUserUnits, UnitId);
                    ddlShift.SelectedIndex = CommonMethods.MatchDropDownItem(ddlShift, ShiftId);
                    ddlShift.Enabled = false;
                    ddlUserUnits.Enabled = false;
                    tbxKnittingDate.Enabled = false;
                }
            }
        }


        protected void rptGeneralInfo_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlUserUnits = (DropDownList)e.Item.FindControl("ddlUserUnits");
                ddlUserUnits.SelectedIndexChanged += ddlUserUnits_SelectedIndexChanged;
            }
        }

        protected void ddlUserUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlUserUnits = (DropDownList)sender;
            var stProductionDate = ((TextBox)rptGeneralInfo.Items[0].FindControl("tbxKnittingDate")).Text;
            DateTime productionDate;
            if (string.IsNullOrEmpty(stProductionDate))
            {
                productionDate = DateTime.Now;
                ((TextBox)rptGeneralInfo.Items[0].FindControl("tbxKnittingDate")).Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
            else
            {
                productionDate = DateTime.Parse(stProductionDate);
            }
            PopulateEntryInfo(Convert.ToInt32(ddlUserUnits.SelectedValue), productionDate);
        }

        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var stBuyerId = DataBinder.Eval(e.Item.DataItem, "BuyerId").ToString();
                var ddlBuyers = (DropDownList)e.Item.FindControl("ddlBuyers");
                var ddlFinishingUnit = (DropDownList)e.Item.FindControl("ddlFinishingUnit");
                //var unitId = int.Parse(((DropDownList)rptGeneralInfo.Items[0].FindControl("ddlUnitUnits")).SelectedValue);
                LoadBuyerDropdown(ddlBuyers);
                LoadFinishingUnitDropdown(ddlFinishingUnit);
                if (stBuyerId != "")
                {
                    var buyerId = int.Parse(stBuyerId);
                    var styleId = int.Parse(DataBinder.Eval(e.Item.DataItem, "StyleId").ToString());
                    var BuyerColorId = int.Parse(DataBinder.Eval(e.Item.DataItem, "BuyerColorId").ToString());
                    var MachineBrandId = int.Parse(DataBinder.Eval(e.Item.DataItem, "MachineBrandId").ToString());
                    var FinishingUnitId = int.Parse(DataBinder.Eval(e.Item.DataItem, "FinishingUnitId").ToString() ?? "0");
                    var ddlStyles = (DropDownList)e.Item.FindControl("ddlStyles");
                    var lblUnitId = (Label)e.Item.FindControl("lblUnitId");
                    if (UnitId != 0)
                    {
                        var tbxMCUsed = (TextBox)e.Item.FindControl("tbxMCUsed");
                        var lblMCUsed = (Label)e.Item.FindControl("lblMCUsed");
                        tbxMCUsed.Text = DataBinder.Eval(e.Item.DataItem, "NumberOfMachines").ToString();
                        lblMCUsed.Text = DataBinder.Eval(e.Item.DataItem, "NumberOfMachines").ToString();

                    }

                    var ddlColors = (DropDownList)e.Item.FindControl("ddlColors");
                    var ddlMachineBrand = (DropDownList)e.Item.FindControl("ddlMachineBrand");
                    var gvSizeQuantity2 = (GridView)e.Item.FindControl("gvSizeQuantity2");
                    LoadStyleDropdown(buyerId, ddlStyles);
                    LoadColorDropdown(styleId, ddlColors);
                    if(UnitId != 0)
                    {
                        LoadMachinBrandDropdownForKnittingUpdate(ddlMachineBrand, int.Parse(lblUnitId.Text), KnittingDate.ToString("yyyy-MM-dd"));
                    }
                    else
                    {
                        LoadMachinBrandDropdown(ddlMachineBrand, int.Parse(lblUnitId.Text));
                    }                    
                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, buyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleId);
                    ddlColors.SelectedIndex = CommonMethods.MatchDropDownItem(ddlColors, BuyerColorId);
                    ddlMachineBrand.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrand, MachineBrandId);
                    ddlFinishingUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlFinishingUnit, FinishingUnitId);

                    //LoadSizeGrid(buyerId, gvSizeQuantity2);
                    LoadSizeGrid(styleId, gvSizeQuantity2);

                }


                var ddlJacquardPortion = (DropDownList)e.Item.FindControl("ddlJacquardPortion");

                if (UnitId != 0)
                {

                    var tbxMCUsed = (TextBox)e.Item.FindControl("tbxMCUsed");
                    var lblMCUsed = (Label)e.Item.FindControl("lblMCUsed");

                    var machinInfo = unitOfWork.GenericRepositories<DailyKnittings>().Get(x => x.StyleId == styleId && x.BuyerColorId == buyerColorId && x.KnittingUnitId == unitId && x.KnittingDate == KnittingDate && x.ShiftId == ShiftId).FirstOrDefault();
                    if (machinInfo != null)
                    {
                        ddlJacquardPortion.SelectedValue = machinInfo.JacquardPortion;
                        ddlJacquardPortion.Items.FindByText(machinInfo.JacquardPortion).Selected = true;
                        tbxMCUsed.Text = machinInfo.NumOfMC + "";
                        lblMCUsed.Text = machinInfo.NumOfMC + "";
                    }
                }
                else
                {
                    ddlJacquardPortion.SelectedIndex = 0;
                }


            }
        }




        protected void gvSizeQuantity_RowCreated2(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlStyles = (DropDownList)((GridView)sender).DataItemContainer.FindControl("ddlStyles");

                DropDownList lblBuyerId = (DropDownList)((GridView)sender).DataItemContainer.FindControl("ddlBuyers");

                this.buyerId = int.Parse(lblBuyerId.Text);
                this.styleId = int.Parse(ddlStyles.Text);

                // DropDownList ddlStyles = (DropDownList)((GridView)sender).DataItemContainer.FindControl("ddlStyles");
                DropDownList ddlColors = (DropDownList)((GridView)sender).DataItemContainer.FindControl("ddlColors");
                Label lblUnitId = (Label)((GridView)sender).DataItemContainer.FindControl("lblUnitId");
                Label lblMachineBrandId = (Label)((GridView)sender).DataItemContainer.FindControl("lblMachineBrandId");
                TextBox tbxMCUsed = (TextBox)((GridView)sender).DataItemContainer.FindControl("tbxMCUsed");
                Label lblMCUsed = (Label)((GridView)sender).DataItemContainer.FindControl("lblMCUsed");



                this.entryRowNumber2++;
                TextBox txtSizeQty = null;
                Label lblSizeId = null;
                LoadSizeInfo();
                for (int i = 0; i < this.dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new TextBox()
                    {
                        ID = string.Concat("txtSizeQty2", i.ToString()),
                        Text = "",
                        Width = 60,
                        TextMode = TextBoxMode.Number,
                         
                    };
                    txtSizeQty.Attributes["min"] = "1";
                    txtSizeQty.CssClass = "sizeQty";
                    //txtSizeQty.ValidationGroup = "save";
                    e.Row.Cells[i].Controls.Add(txtSizeQty);
                    lblSizeId = new Label()
                    {
                        ID = string.Concat("lblSizeId2", i.ToString()),
                        Text = this.dtSizes.Rows[i][0].ToString(),
                        Visible = false
                    };
                    e.Row.Cells[i].Controls.Add(lblSizeId);
                    Label enitryId = new Label()
                    {
                        ID = string.Concat("lblenitryId2", i.ToString()),
                        Text = "",
                        Visible = false
                    };
                    e.Row.Cells[i].Controls.Add(enitryId);

                    int styleId = int.Parse(ddlStyles.SelectedValue == "" ? "0" : ddlStyles.SelectedValue);
                    int buyerColorId = int.Parse(ddlColors.SelectedValue == "" ? "0" : ddlColors.SelectedValue);
                    int unitId = int.Parse(lblUnitId.Text == "" ? "0" : lblUnitId.Text);
                    int sizeId = int.Parse(this.dtSizes.Rows[i][0].ToString());
                    int machineBrandId = int.Parse(lblMachineBrandId.Text == "" ? "0" : lblMachineBrandId.Text);
                    int numMCUsed = int.Parse(tbxMCUsed.Text == "" ? "0" : tbxMCUsed.Text);
                    if (numMCUsed == 0)
                    {
                        numMCUsed = int.Parse(lblMCUsed.Text == "" ? "0" : lblMCUsed.Text);
                    }
                    


                    // var sizeSt = unitOfWork.GetSingleValue($"SELECT MAX(ISNULL(ocs.Quantity,0)) FROM OrderColors oc INNER JOIN OrderColorSizes ocs ON oc.Id = ocs.OrderColorId INNER JOIN Orders o ON o.Id = oc.OrderId WHERE o.IsActive =1 AND oc.BuyerColorId = {buyerColorId} AND ocs.SizeId = {sizeId}");
                    //var sizeQty = CommonMethods.GetSizeQuantityByStyleAndColor(styleId, buyerColorId, sizeId);
                    //if (sizeQty > 0)
                    //{
                    var availableQty = int.Parse(unitOfWork.GetSingleStringValue($"Exec usp_GetAvailableQtyForKnittingByStyleColorSizeAndUnit '{styleId}','{buyerColorId}','{sizeId}','{unitId}'"));
                    if (this.UnitId != 0 && ddlStyles.SelectedValue != "")
                    {
                        DailyKnittings pInfo = this.unitOfWork.GenericRepositories<DailyKnittings>().Get((DailyKnittings x) => x.StyleId == styleId && x.BuyerColorId == buyerColorId && x.MachineBrandId == machineBrandId && x.KnittingUnitId == unitId && x.SizeId == sizeId && (x.KnittingDate == this.KnittingDate) && x.NumOfMC == numMCUsed && x.ShiftId == this.ShiftId, null, "").FirstOrDefault<DailyKnittings>();


                        if (pInfo != null)
                        {
                            availableQty += pInfo.ProductionQty ?? 0;
                            txtSizeQty.Text = pInfo.ProductionQty.ToString() ?? "";
                            enitryId.Text = pInfo.Id.ToString() ?? "";
                        }
                    }
                    var avalableBalance = new Label();
                    avalableBalance.ID = "lblavalableBalance" + i.ToString();
                    avalableBalance.Text = "Bal: " + availableQty;
                    avalableBalance.CssClass = "available-balance";
                    e.Row.Cells[i].Controls.Add(avalableBalance);

                    txtSizeQty.Attributes.Add("max", availableQty + "");
                    //}
                    //else
                    //{
                    //    txtSizeQty.Enabled = false;
                    //}

                }
            }
        }




        protected void LoadYarnIssueForSizes()
        {
            dtYarnIssuedForSizes = productionManager.GetYarnIssuedForSizesByStyle(styleId, unitId, buyerColorId);
        }



        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {

            try
            {
                int unit = int.Parse(((DropDownList)this.rptGeneralInfo.Items[0].FindControl("ddlUserUnits")).SelectedValue);
                string stShift = ((DropDownList)this.rptGeneralInfo.Items[0].FindControl("ddlShift")).SelectedValue;
                string stKnittingDate = ((TextBox)this.rptGeneralInfo.Items[0].FindControl("tbxKnittingDate")).Text;


                if (string.IsNullOrEmpty(stKnittingDate))
                {
                    ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.error('Please select knitting date!')", true);
                }
                else if (string.IsNullOrEmpty(stShift))
                {
                    ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.error('Please select shift!')", true);
                }
                else
                {
                    int shiftId = int.Parse(stShift);
                    DateTime knittingDate = DateTime.Parse(stKnittingDate);
                    var exist = unitOfWork.GenericRepositories<DailyKnittings>().IsExist(x => x.KnittingUnitId == unit && x.ShiftId == shiftId && x.KnittingDate == DbFunctions.TruncateTime(knittingDate));


                    if (!exist)
                    {
                        if (this.unitOfWork.GenericRepositories<DailyKnittings>().Get((DailyKnittings x) => x.KnittingUnitId == unitId && (x.KnittingDate == DateTime.Today) && x.ShiftId == shiftId, null, "").Any<DailyKnittings>())
                        {
                            ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.error('Entries already exist!')", true);
                        }
                        else
                        {
                            DataTable dtDailyKnittingInfo = this.DailyKnittingInfo();
                            int? machineUsed = new int?(0);
                            int? production = new int?(0);
                            int hasRow = 0;
                            int knittingEntriesSavingResult = 0;
                            for (int i = 0; i < this.rptEntryInfo.Items.Count; i++)
                            {
                                DropDownList ddlBuyers = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlBuyers");
                                DropDownList ddlStyles = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlStyles");
                                DropDownList ddlColors = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlColors");
                                DropDownList ddlMachineBrand = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlMachineBrand");
                                DropDownList ddlFinishingUnit = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlFinishingUnit");
                                TextBox tbxKnittingMCGauge = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxKnittingMCGauge");
                                DropDownList ddlJacquardPortion = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlJacquardPortion");
                                TextBox tbxMCUsed = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxMCUsed");
                                Label lblMCUsed = (Label)this.rptEntryInfo.Items[i].FindControl("lblMCUsed");
                                GridView gvSizeQuantity2 = (GridView)this.rptEntryInfo.Items[i].FindControl("gvSizeQuantity2");
                                for (int j = 0; j < gvSizeQuantity2.Columns.Count; j++)
                                {
                                    machineUsed = null;
                                    production = null;
                                    string sizeIdLabel2Id = string.Concat("lblSizeId2", j.ToString());
                                    string sizeQuantityTextBox2Id = string.Concat("txtSizeQty2", j.ToString());
                                    Label sizeId = (Label)gvSizeQuantity2.Rows[0].Cells[j].FindControl(sizeIdLabel2Id);
                                    TextBox tbxSizeQty = (TextBox)gvSizeQuantity2.Rows[0].Cells[j].FindControl(sizeQuantityTextBox2Id);

                                    //if ((!(ddlBuyers.SelectedValue != "") || !(ddlStyles.SelectedValue != "") || !(ddlColors.SelectedValue != "") || !(ddlMachineBrand.SelectedValue != "") || !(tbxKnittingMCGauge.Text != "") ? false : tbxMCUsed.Text != "") || !(tbxSizeQty.Text != ""))
                                    //{

                                    if ((ddlBuyers.SelectedValue != "") && (ddlStyles.SelectedValue != "") && (ddlColors.SelectedValue != "") && (ddlMachineBrand.SelectedValue != "") && (tbxKnittingMCGauge.Text != "") && (tbxMCUsed.Text != "") && (tbxSizeQty.Text != "") && ddlFinishingUnit.SelectedValue != "")
                                    {
                                        if ((tbxMCUsed.Text != "" ? true : !(tbxSizeQty.Text == "")))
                                        {
                                            if (tbxMCUsed.Text != "")
                                            {
                                                machineUsed = new int?(Convert.ToInt32(tbxMCUsed.Text));
                                            }
                                            if (tbxSizeQty.Text != "")
                                            {
                                                production = new int?(Convert.ToInt32(tbxSizeQty.Text));
                                            }
                                        }


                                        dtDailyKnittingInfo.Rows.Add(new object[] { knittingDate, ddlStyles.SelectedValue, int.Parse(ddlColors.SelectedValue), unit, shiftId, sizeId.Text, ddlMachineBrand.SelectedValue, ddlFinishingUnit.SelectedValue, tbxKnittingMCGauge.Text, ddlJacquardPortion.SelectedItem.Text, machineUsed, production, CommonMethods.SessionInfo.UserName, DateTime.Now });
                                        hasRow++;

                                    }
                                }
                            }


                            if (hasRow <= 0)
                            {
                                ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.error('Entries could not be saved. Please enter valid data.')", true);
                            }
                            else
                            {


                                DataView view = new DataView(dtDailyKnittingInfo);
                                DataTable distinctValues = view.ToTable(true, "UnitId", "StyleId", "BuyerColorId", "MachineBrandId", "NumOfMC");

                                var newDt = distinctValues.AsEnumerable()
                                  .GroupBy(r => r.Field<int>("MachineBrandId"))
                                  .Select(g =>
                                  {
                                      var row = distinctValues.NewRow();
                                      row["MachineBrandId"] = g.Key;
                                      row["NumOfMC"] = g.Sum(r => r.Field<int>("NumOfMC"));
                                      row["UnitId"] = g.Max(r => r.Field<int>("UnitId"));
                                      return row;
                                  }).CopyToDataTable();

                                var unitMcQty = true;

                                if (newDt.Rows.Count > 0)
                                {

                                    foreach (DataRow row in newDt.Rows)
                                    {
                                        var mcBrandId = int.Parse(row["MachineBrandId"].ToString());
                                        var numOfMC = int.Parse(row["NumOfMC"].ToString());
                                        var unitId = int.Parse(row["UnitId"].ToString());

                                        var unitMC = unitOfWork.GenericRepositories<ProductonUnitMachineBrand>().Get(x => x.ProductionUnitId == unitId && x.MachineBrandId == mcBrandId).Sum(x => x.NumberOfMachines);
                                        if (numOfMC > unitMC)
                                        {
                                            unitMcQty = false;
                                            break;
                                        }

                                    }
                                }

                                if (unitMcQty)
                                {
                                    knittingEntriesSavingResult = this.productionManager.SaveDalilyKnittingInfo(dtDailyKnittingInfo);
                                    string sql = string.Format("EXEC usp_Insert_DailyKnittingSummary '{0}',{1}", knittingDate, unit);
                                    this.unitOfWork.ExeccuteRawQyery(sql, null);
                                    if (knittingEntriesSavingResult > 0)
                                    {
                                        ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                                        ScriptManager.RegisterStartupScript(this, base.GetType(), "Script", "reloadPage();", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.error('Please enter valid MC qty. You can not enter more qty than unit MC qty.');", true);
                                }

                            }

                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.error('Entries already exist for this unit, shift and date');", true);
                    }
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message.Replace("\'", "") + "')", true);
            }
        }


        protected void lnkbtnCalculateSizeTotal_Click(object sender, EventArgs e)
        {


            int shiftTotal = 0;
            int sizeTotal = 0;
            int mcUsed = 0;

            for (int i = 0; i < this.rptEntryInfo.Items.Count; i++)
            {

                Label lblSizeTotal = (Label)this.rptEntryInfo.Items[i].FindControl("lblSizeTotal");
                TextBox tbxMCUsed = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxMCUsed");
                if (tbxMCUsed.Text != "")
                {
                    mcUsed += int.Parse(tbxMCUsed.Text);
                }

                GridView gvSizeQuantity2 = (GridView)this.rptEntryInfo.Items[i].FindControl("gvSizeQuantity2");
                sizeTotal = 0;
                for (int j = 0; j < gvSizeQuantity2.Columns.Count; j++)
                {
                    string sizeIdLabel2Id = string.Concat("lblSizeId2", j.ToString());
                    string sizeQuantityTextBox2Id = string.Concat("txtSizeQty2", j.ToString());
                    Label sizeId = (Label)gvSizeQuantity2.Rows[0].Cells[j].FindControl(sizeIdLabel2Id);

                    TextBox tbxSizeQty = (TextBox)gvSizeQuantity2.Rows[0].Cells[j].FindControl(sizeQuantityTextBox2Id);

                    if (tbxSizeQty.Text != "")
                    {
                        sizeTotal += int.Parse(tbxSizeQty.Text);
                    }

                }
                lblSizeTotal.Text = sizeTotal.ToString();
                shiftTotal += sizeTotal;
            }

            lblShiftTotal.Text = "MC Total : " + mcUsed + "<br/> Shift Total: " + shiftTotal.ToString();


        }


        private void LoadBuyerDropdown(DropDownList ddl)
        {
            CommonMethods.LoadBuyerDropdownForUser(ddl, 1, 0, 0);
        }

        private void LoadFinishingUnitDropdown(DropDownList ddl)
        {
            CommonMethods.LoadDropdown(ddl, "FinishingUnits", 1, 0, 0);
        }

        private void LoadColorDropdown(int styleId, DropDownList ddl)
        {
            var dt = this.orderManager.GetColorsByStyleId(styleId);
            var dr = dt.NewRow();
            dr["BuyerColorId"] = "";
            dr["ColorDescription"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);
            ddl.DataSource = dt;
            ddl.DataTextField = "ColorDescription";
            ddl.DataValueField = "BuyerColorId";
            ddl.DataBind();
        }

        private void LoadMachinBrandDropdown(DropDownList ddl, int unitId)
        {
            string sql = $"Exec usp_GetAllMachineBrandByUnitId {unitId}";
            var dt = unitOfWork.GetDataTableFromSql(sql);
            DataRow dr = dt.NewRow();
            dr["MachineBrandId"] = "";
            dr["MachineBrand"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);

            ddl.DataTextField = "MachineBrand";
            ddl.DataValueField = "MachineBrandId";
            ddl.DataSource = dt;
            ddl.DataBind();
        }

        private void LoadMachinBrandDropdownForKnittingUpdate(DropDownList ddl, int unitId, string knittingDate)
        {
            string sql = $"Exec usp_GetAllMachineBrandByUnitIdAndKnittingDate '{unitId}','{knittingDate}'";
            var dt = unitOfWork.GetDataTableFromSql(sql);
            DataRow dr = dt.NewRow();
            dr["MachineBrandId"] = "";
            dr["MachineBrand"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);

            ddl.DataTextField = "MachineBrand";
            ddl.DataValueField = "MachineBrandId";
            ddl.DataSource = dt;
            ddl.DataBind();
        }

        private void LoadSizeGrid(int styleId, GridView gridView)
        {
            gridView.Columns.Clear();
            // this.dtSizes = this.buyerManager.GetSizes(buyerId);
            this.dtSizes = this.buyerManager.GetStyleSizes(styleId);

            DataTable dtOneRow = new DataTable();
            DataRow dr = null;
            dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dr = dtOneRow.NewRow();
            dr["RowNumber"] = 1;
            dtOneRow.Rows.Add(dr);
            if (this.dtSizes.Rows.Count > 0)
            {
                TemplateField templateField = null;
                for (int i = 0; i < this.dtSizes.Rows.Count; i++)
                {
                    templateField = new TemplateField()
                    {
                        HeaderText = this.dtSizes.Rows[i][1].ToString()
                    };
                    gridView.Columns.Add(templateField);
                }
            }
            gridView.DataSource = dtOneRow;
            gridView.DataBind();
        }

        protected void LoadSizeInfo()
        {
            //this.dtSizes = this.buyerManager.GetSizes(this.buyerId);

            this.dtSizes = this.buyerManager.GetStyleSizes(this.styleId);

        }

        private void LoadStyleDropdown(int buyerId, DropDownList ddl)
        {
            CommonMethods.LoadOnlyRunningStyleDropdownById(ddl, buyerId, "BuyerStyles", 1, 0);
        }

        public DataTable DailyKnittingInfo()
        {
            DataTable dt = new DataTable("DailyKnittingInfo");
            dt.Columns.Add("KnittingDate", typeof(string));
            dt.Columns.Add("StyleId", typeof(int));
            dt.Columns.Add("BuyerColorId", typeof(int));
            dt.Columns.Add("UnitId", typeof(int));
            dt.Columns.Add("ShiftId", typeof(int));
            dt.Columns.Add("SizeId", typeof(int));
            dt.Columns.Add("MachineBrandId", typeof(int));
            dt.Columns.Add("FinishingUnitId", typeof(int));
            dt.Columns.Add("MachineGauge", typeof(string));
            dt.Columns.Add("JacquardPortion", typeof(string));
            dt.Columns.Add("NumOfMC", typeof(int));
            // dt.Columns.Add("NumMCAtNight", typeof(int));
            //dt.Columns.Add("Target", typeof(int));
            dt.Columns.Add("ProductionQty", typeof(int));
            //dt.Columns.Add("NightTarget", typeof(int));
            //dt.Columns.Add("NightProduction", typeof(int));
            //dt.Columns.Add("QuantityDelivered", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(string));
            return dt;
        }

        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {
            try
            {
                int num = int.Parse(((DropDownList)this.rptGeneralInfo.Items[0].FindControl("ddlUserUnits")).SelectedValue);
                string stShift = ((DropDownList)this.rptGeneralInfo.Items[0].FindControl("ddlShift")).SelectedValue;
                string stKnittingDate = ((TextBox)this.rptGeneralInfo.Items[0].FindControl("tbxKnittingDate")).Text;
                if (string.IsNullOrEmpty(stKnittingDate))
                {
                    ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.error('Please select knitting date!')", true);
                }
                else if (string.IsNullOrEmpty(stShift))
                {
                    ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.error('Please select shift!')", true);
                }
                else
                {
                    int num1 = int.Parse(stShift);
                    DateTime knittingDate = DateTime.Parse(stKnittingDate);
                    if (this.unitOfWork.GenericRepositories<DailyKnittings>().Get((DailyKnittings x) => x.KnittingUnitId == unitId && (x.KnittingDate == DateTime.Today) && x.ShiftId == num1, null, "").Any<DailyKnittings>())
                    {
                        ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.error('Entries already exist!')", true);
                    }
                    else
                    {
                        DataTable dtDailyKnittingInfo = this.DailyKnittingInfo();
                        int? machineUsed = new int?(0);
                        int? production = new int?(0);
                        int hasRow = 0;
                        int knittingEntriesSavingResult = 0;
                        for (int i = 0; i < this.rptEntryInfo.Items.Count; i++)
                        {
                            DropDownList ddlBuyers = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlBuyers");
                            DropDownList ddlStyles = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlStyles");
                            DropDownList ddlColors = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlColors");
                            DropDownList ddlMachineBrand = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlMachineBrand");
                            DropDownList ddlFinishingUnit = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlFinishingUnit");
                            TextBox tbxKnittingMCGauge = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxKnittingMCGauge");
                            DropDownList ddlJacquardPortion = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlJacquardPortion");
                            TextBox tbxMCUsed = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxMCUsed");
                            Label lblMCUsed = (Label)this.rptEntryInfo.FindControl("lblMCUsed");
                            GridView gvSizeQuantity2 = (GridView)this.rptEntryInfo.Items[i].FindControl("gvSizeQuantity2");
                            for (int j = 0; j < gvSizeQuantity2.Columns.Count && ddlBuyers.SelectedValue != ""; j++)
                            {
                                machineUsed = null;
                                production = null;
                                string sizeIdLabel2Id = string.Concat("lblSizeId2", j.ToString());
                                string sizeQuantityTextBox2Id = string.Concat("txtSizeQty2", j.ToString());
                                Label sizeId = (Label)gvSizeQuantity2.Rows[0].Cells[j].FindControl(sizeIdLabel2Id);
                                TextBox tbxSizeQty = (TextBox)gvSizeQuantity2.Rows[0].Cells[j].FindControl(sizeQuantityTextBox2Id);
                                if ((tbxMCUsed.Text != "" ? true : !(tbxSizeQty.Text == "")))
                                {
                                    if (tbxMCUsed.Text != "")
                                    {
                                        machineUsed = new int?(Convert.ToInt32(tbxMCUsed.Text));
                                    }
                                    if (tbxSizeQty.Text != "")
                                    {
                                        production = new int?(Convert.ToInt32(tbxSizeQty.Text));
                                    }
                                }
                                //if ((!(ddlBuyers.SelectedValue != "") || !(ddlStyles.SelectedValue != "") || !(ddlColors.SelectedValue != "") || !(ddlMachineBrand.SelectedValue != "") || !(tbxKnittingMCGauge.Text != "") ? false : tbxMCUsed.Text != ""))
                                //{ 
                                if ((ddlBuyers.SelectedValue != "") && (ddlStyles.SelectedValue != "") && (ddlColors.SelectedValue != "") && (ddlMachineBrand.SelectedValue != "") && (tbxKnittingMCGauge.Text != "") && (tbxMCUsed.Text != "") && (tbxSizeQty.Text != "") && ddlFinishingUnit.SelectedValue != "")
                                {
                                    dtDailyKnittingInfo.Rows.Add(new object[] { knittingDate, ddlStyles.SelectedValue, int.Parse(ddlColors.SelectedValue), num, num1, sizeId.Text, ddlMachineBrand.SelectedValue, ddlFinishingUnit.SelectedValue, tbxKnittingMCGauge.Text, ddlJacquardPortion.SelectedItem.Text, machineUsed, production, CommonMethods.SessionInfo.UserName, DateTime.Now });
                                    hasRow++;
                                }
                            }
                        }
                        if (hasRow <= 0)
                        {
                            ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.error('Entries could not be saved.')", true);
                        }
                        else
                        {

                            DataView view = new DataView(dtDailyKnittingInfo);
                            DataTable distinctValues = view.ToTable(true, "UnitId", "StyleId", "BuyerColorId", "MachineBrandId", "NumOfMC");

                            var newDt = distinctValues.AsEnumerable()
                              .GroupBy(r => r.Field<int>("MachineBrandId"))
                              .Select(g =>
                              {
                                  var row = distinctValues.NewRow();
                                  row["MachineBrandId"] = g.Key;
                                  row["NumOfMC"] = g.Sum(r => r.Field<int>("NumOfMC"));
                                  row["UnitId"] = g.Max(r => r.Field<int>("UnitId"));
                                  return row;
                              }).CopyToDataTable();

                            var unitMcQty = true;

                            if (newDt.Rows.Count > 0)
                            {

                                foreach (DataRow row in newDt.Rows)
                                {
                                    var mcBrandId = int.Parse(row["MachineBrandId"].ToString());
                                    var numOfMC = int.Parse(row["NumOfMC"].ToString());
                                    var unitId = int.Parse(row["UnitId"].ToString());

                                    var unitMC = unitOfWork.GenericRepositories<ProductonUnitMachineBrand>().Get(x => x.ProductionUnitId == unitId && x.MachineBrandId == mcBrandId).Sum(x => x.NumberOfMachines);
                                    if (numOfMC > unitMC)
                                    {
                                        unitMcQty = false;
                                        break;
                                    }

                                }
                            }

                            if (unitMcQty)
                            {
                                unitOfWork.ExeccuteRawQyery($"DELETE FROM DailyKnittings WHERE KnittingUnitId = {num} AND ShiftId ={num1} AND KnittingDate ='{knittingDate}'");
                                knittingEntriesSavingResult = this.productionManager.SaveDalilyKnittingInfo(dtDailyKnittingInfo);
                                string sql = string.Format("EXEC usp_Insert_DailyKnittingSummary '{0}',{1}", knittingDate, num);
                                this.unitOfWork.ExeccuteRawQyery(sql, null);

                               unitOfWork.SaveAuditLogWhenSql("Updated", num + "", "DailyKnittings", unitOfWork);

                                if (knittingEntriesSavingResult > 0)
                                {
                                    ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewKnittingEntriesV2.aspx');", true);
                                }

                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.error('Please enter valid MC qty. You can not enter more qty than unit MC qty.');", true);
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }



        protected void tbxKnittingDate_TextChanged(object sender, EventArgs e)
        {
            DataTable dtProdEntryGeneralInfo = ProdEntryGeneralInfo();
            var unitId = int.Parse(((DropDownList)rptGeneralInfo.Items[0].FindControl("ddlUserUnits")).SelectedValue);
            var stKnittingDate = ((TextBox)rptGeneralInfo.Items[0].FindControl("tbxKnittingDate")).Text;
            if (!string.IsNullOrEmpty(stKnittingDate))
            {
                var knittingDate = DateTime.Parse(stKnittingDate);
                PopulateEntryInfo(unitId, knittingDate);
            }
            else
            {
                ((TextBox)rptGeneralInfo.Items[0].FindControl("tbxKnittingDate")).Text = DateTime.Now.ToString("yyyy-MM-dd");
                PopulateEntryInfo(unitId, DateTime.Now);
            }
        }

        private void AddNewRowToGrid()
        {
            lblShiftTotal.Text = "";

            List<DataTable> sizeAndQtyTableList = new List<DataTable>();
            int unitId = int.Parse(((DropDownList)this.rptGeneralInfo.Items[0].FindControl("ddlUserUnits")).SelectedValue);
            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {
                        DropDownList ddlBuyers = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlBuyers");
                        DropDownList ddlStyles = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                        DropDownList ddlColors = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        DropDownList ddlMachineBrand = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlMachineBrand");
                        DropDownList ddlFinishingUnit = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlFinishingUnit");
                        TextBox tbxKnittingMCGauge = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxKnittingMCGauge");
                        TextBox tbxMCUsed = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxMCUsed");
                        GridView gvSizeQuantity2 = (GridView)this.rptEntryInfo.Items[rowIndex].FindControl("gvSizeQuantity2");
                        dtCurrentTable.Rows[rowIndex]["BuyerId"] = (ddlBuyers.SelectedValue == "" ? "0" : ddlBuyers.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["StyleId"] = (ddlStyles.SelectedValue == "" ? "0" : ddlStyles.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["MachineBrandId"] = (ddlMachineBrand.SelectedValue == "" ? "0" : ddlMachineBrand.SelectedValue);
                        dtCurrentTable.Columns["FinishingUnitId"].ReadOnly = false;
                        dtCurrentTable.Rows[rowIndex]["FinishingUnitId"] = int.Parse(ddlFinishingUnit.SelectedValue == "" ? "0" : ddlFinishingUnit.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["BuyerColorId"] = (ddlColors.SelectedValue == "" ? "0" : ddlColors.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["KnittingMachineGauge"] = tbxKnittingMCGauge.Text;
                        dtCurrentTable.Rows[rowIndex]["McUsed"] = tbxMCUsed.Text;
                        DataTable sizeAndQtyTable = new DataTable();
                        sizeAndQtyTable.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                        sizeAndQtyTable.Columns.Add(new DataColumn("Column0", typeof(string)));
                        for (int j = 0; j < gvSizeQuantity2.Columns.Count; j++)
                        {
                            DataRow sizeAndQtyRow = null;
                            sizeAndQtyRow = sizeAndQtyTable.NewRow();
                            sizeAndQtyRow["RowNumber"] = j + 1;
                            sizeAndQtyRow["Column0"] = string.Empty;
                            sizeAndQtyTable.Rows.Add(sizeAndQtyRow);
                            string controlId = string.Concat("txtSizeQty2", j.ToString());
                            TextBox tbxSizeQty = (TextBox)gvSizeQuantity2.Rows[0].Cells[j].FindControl(controlId);
                            sizeAndQtyTable.Rows[j]["Column0"] = tbxSizeQty.Text;
                        }
                        sizeAndQtyTableList.Add(sizeAndQtyTable);
                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["BuyerColorId"] = 0;
                            drCurrentRow["StyleId"] = 0;
                            drCurrentRow["MachineBrandId"] = 0;
                            drCurrentRow["FinishingUnitId"] = 0;
                            drCurrentRow["BuyerId"] = 0;
                            drCurrentRow["UnitId"] = unitId;
                            drCurrentRow["ColorDescription"] = "";
                            drCurrentRow["BrandName"] = "";
                            drCurrentRow["KnittingMachineGauge"] = "";
                            drCurrentRow["KnittingTime"] = "";
                            drCurrentRow["NumberOfMachines"] = 0;
                        }
                    }
                    dtCurrentTable.Rows.InsertAt(drCurrentRow, dtCurrentTable.Rows.Count);
                    this.ViewState["CurrentTable"] = dtCurrentTable;
                    this.ViewState["SizeAndQtyTableList"] = sizeAndQtyTableList;
                    this.rptEntryInfo.DataSource = dtCurrentTable;
                    this.rptEntryInfo.DataBind();
                }
            }
            this.SetPreviousData();
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            this.AddNewRowToGrid();
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            DropDownList ddlBuyer = (DropDownList)sender;
            DropDownList ddlStyle = (DropDownList)((RepeaterItem)ddlBuyer.NamingContainer).FindControl("ddlStyles");
            DropDownList ddlColors = (DropDownList)((RepeaterItem)ddlBuyer.NamingContainer).FindControl("ddlColors");
            DropDownList ddlMachineBrand = (DropDownList)((RepeaterItem)ddlBuyer.NamingContainer).FindControl("ddlMachineBrand");
            GridView gvSize = (GridView)((RepeaterItem)ddlBuyer.NamingContainer).FindControl("gvSizeQuantity2");

            gvSize.DataSource = null;
            gvSize.DataBind();

            if (ddlBuyer.SelectedValue == "")
            {
                ddlStyle.Items.Clear();
                ddlColors.Items.Clear();
                ddlMachineBrand.Items.Clear();
                gvSize.Visible = false;

            }
            else
            {
                gvSize.Visible = false;
                ddlColors.Items.Clear();
                int buyerId = int.Parse(ddlBuyer.SelectedValue);
                this.LoadStyleDropdown(buyerId, ddlStyle);
            }
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlStyle = (DropDownList)sender;
            DropDownList ddlColors = (DropDownList)((RepeaterItem)ddlStyle.NamingContainer).FindControl("ddlColors");
            DropDownList ddlMachineBrand = (DropDownList)((RepeaterItem)ddlStyle.NamingContainer).FindControl("ddlMachineBrand");
            DropDownList ddlFinishingUnit = (DropDownList)((RepeaterItem)ddlStyle.NamingContainer).FindControl("ddlFinishingUnit");
            Label lblUnitId = (Label)((RepeaterItem)ddlStyle.NamingContainer).FindControl("lblUnitId");
            GridView gvSize = (GridView)((RepeaterItem)ddlStyle.NamingContainer).FindControl("gvSizeQuantity2");

            gvSize.DataSource = null;
            gvSize.DataBind();

            if (ddlStyle.SelectedValue == "")
            {
                ddlColors.Items.Clear();
                gvSize.Visible = false;
            }
            else
            {
                gvSize.Visible = false;
                var styleId = int.Parse(ddlStyle.SelectedValue);
                this.LoadColorDropdown(styleId, ddlColors);
                //var unitId = ((DropDownList)rptGeneralInfo.Items[0].FindControl("ddlUnitUnits")).SelectedValue;
                var unitId = int.Parse(lblUnitId.Text);
                this.LoadMachinBrandDropdown(ddlMachineBrand, unitId);
                // this.LoadSizeGrid(buyerId, gvSize);

                SetFinishingUnit(styleId, unitId, ddlFinishingUnit,ddlMachineBrand);

            }
        }


        private void SetFinishingUnit(int styleId, int unitId, DropDownList ddlFinishingUnit, DropDownList ddlMCBrand)
        {
            ddlFinishingUnit.SelectedValue = "";
            //var info = unitOfWork.GetRecordSet<LastFinishingUnitAndMC>($"SELECT TOP(1) FinishingUnitId,MachineBrandId FROM DailyKnittings WHERE StyleId = '{styleId}' ORDER BY ID DESC").FirstOrDefault();
            var FinishingUnitId = unitOfWork.GetSingleStringValue($"SELECT TOP(1) FinishingUnitId FROM DailyKnittings WHERE StyleId = '{styleId}' ORDER BY ID DESC");
            var MachineBrandId = unitOfWork.GetSingleStringValue($"SELECT TOP(1) MachineBrandId FROM DailyKnittings WHERE StyleId = '{styleId}' AND KnittingUnitId = '{unitId}' ORDER BY ID DESC");

            //var lastEntry = unitOfWork.GenericRepositories<DailyKnittings>().Get(x => x.StyleId == styleId).OrderByDescending(x => x.Id).FirstOrDefault();
            if (!string.IsNullOrEmpty(FinishingUnitId))
            {
                ddlFinishingUnit.SelectedValue = FinishingUnitId;
            }

            if (!string.IsNullOrEmpty(MachineBrandId))
            {
                ddlMCBrand.SelectedValue = MachineBrandId + "";
            }
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if ((this.ViewState["CurrentTable"] == null ? false : this.ViewState["SizeAndQtyTableList"] != null))
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                List<DataTable> sizeAndQtyTableList = (List<DataTable>)this.ViewState["SizeAndQtyTableList"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DropDownList ddlBuyers = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlBuyers");
                        DropDownList ddlStyles = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                        DropDownList ddlColors = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        DropDownList ddlMachineBrand = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlMachineBrand");
                        DropDownList ddlFinishingUnit = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlFinishingUnit");
                        TextBox tbxKnittingMCGauge = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxKnittingMCGauge");
                        TextBox tbxMCUsed = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxMCUsed");
                        Label lblMCUsed = (Label)this.rptEntryInfo.Items[rowIndex].FindControl("lblMCUsed");
                        GridView gvSizeQuantity2 = (GridView)this.rptEntryInfo.Items[rowIndex].FindControl("gvSizeQuantity2");
                        ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["BuyerId"].ToString()) ? "0" : dt.Rows[i]["BuyerId"].ToString())));
                        ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["StyleId"].ToString()) ? "0" : dt.Rows[i]["StyleId"].ToString())));
                        ddlColors.SelectedIndex = CommonMethods.MatchDropDownItem(ddlColors, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["BuyerColorId"].ToString()) ? "0" : dt.Rows[i]["BuyerColorId"].ToString())));
                        ddlMachineBrand.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrand, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["MachineBrandId"].ToString()) ? "0" : dt.Rows[i]["MachineBrandId"].ToString())));
                        ddlFinishingUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlFinishingUnit, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["FinishingUnitId"].ToString()) ? "0" : dt.Rows[i]["FinishingUnitId"].ToString())));
                        tbxMCUsed.Text = dt.Rows[i]["McUsed"].ToString();
                        lblMCUsed.Text = dt.Rows[i]["McUsed"].ToString();
                        tbxKnittingMCGauge.Text = dt.Rows[i]["KnittingMachineGauge"].ToString();
                        if (i < sizeAndQtyTableList.Count)
                        {
                            DataTable tableFromList = sizeAndQtyTableList[i];
                            for (int j = 0; j < tableFromList.Rows.Count; j++)
                            {
                                TextBox tbxSizeQty = (TextBox)gvSizeQuantity2.Rows[0].Cells[j].FindControl(string.Concat("txtSizeQty2", j.ToString()));
                                tbxSizeQty.Text = tableFromList.Rows[j]["Column0"].ToString();
                            }
                        }
                        rowIndex++;
                    }
                }
            }
        }


        protected void ddlColors_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlColors = (DropDownList)sender;
            DropDownList ddlStyle = (DropDownList)((RepeaterItem)ddlColors.NamingContainer).FindControl("ddlStyles");
            DropDownList ddlBuyer = (DropDownList)((RepeaterItem)ddlColors.NamingContainer).FindControl("ddlBuyers");
            //DropDownList ddlMachineBrand = (DropDownList)((RepeaterItem)ddlBuyer.NamingContainer).FindControl("ddlMachineBrand");
            GridView gvSize = (GridView)((RepeaterItem)ddlBuyer.NamingContainer).FindControl("gvSizeQuantity2");

            gvSize.DataSource = null;
            gvSize.DataBind();

            if (ddlColors.SelectedValue == "")
            {
                gvSize.Visible = false;

            }
            else
            {
                //var buyerId = int.Parse(ddlBuyer.SelectedValue);
                //gvSize.Visible = true;
                //this.LoadSizeGrid(buyerId, gvSize);

                var styleId = int.Parse(ddlStyle.SelectedValue);
                gvSize.Visible = true;
                this.LoadSizeGrid(styleId, gvSize);
            }
        }


    }

    public class LastFinishingUnitAndMC
    {
        public int? MachineBrandId { get; set; }
        public int? FinishingUnitId { get; set; }
    }
}