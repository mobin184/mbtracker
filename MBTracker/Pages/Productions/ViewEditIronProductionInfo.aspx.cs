﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class ViewEditIronProductionInfo : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        OrderManager orderManager = new OrderManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableEditButton();
                EnableDisableDeleteButton();

                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteIronProductionInfo(Id);
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
            }
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlColors.Items.Clear();
            if (ddlStyles.SelectedValue != "")
            {
                LoadColorDropdown(int.Parse(ddlStyles.SelectedValue), ddlColors);
            }
        }

        private void DeleteIronProductionInfo(int Id)
        {
            var ironProductionInfo = unitOfWork.GenericRepositories<IronProductionInfo>().GetByID(Id);
            unitOfWork.GenericRepositories<IronProductionInfo>().Delete(ironProductionInfo);
            unitOfWork.Save();
            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
            LoadData();
        }

        private void EnableDisableDeleteButton()
        {
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewEditIronProductionInfo", 2, 1);
        }

        private void EnableDisableEditButton()
        {

            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewEditIronProductionInfo", 1, 1);
        }

        protected void btnViewEntries_Click(object sender, EventArgs e)
        {
            try
            {
                LoadData();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void LoadData()
        {
            divSummary.Visible = false;

            var ironProductionDate = "";
            if (!string.IsNullOrEmpty(tbxIronProductionDate.Text))
            {
                ironProductionDate = DateTime.Parse(tbxIronProductionDate.Text).ToString("yyyy-MM-dd");
            }

            var buyerId = 0;
            if (ddlBuyers.SelectedValue != "")
            {
                buyerId = int.Parse(ddlBuyers.SelectedValue);
            }

            var styleId = 0;

            if (ddlStyles.SelectedValue != "")
            {
                styleId = int.Parse(ddlStyles.SelectedValue);
            }

            var colorId = 0;

            if (ddlColors.SelectedValue != "")
            {
                colorId = int.Parse(ddlColors.SelectedValue);
            }

            var dtEntries = unitOfWork.GetDataTableFromSql($"EXEC usp_IronProductionInfoByUserAndDate  '{ironProductionDate}','{buyerId}','{styleId}','{colorId}' ");

            if (dtEntries.Rows.Count > 0)
            {
                dtEntries = dtEntries.CheckEditDeletePermission(CommonMethods.SessionInfo.RoleId, "ViewEditIronProductionInfo");
                rptSummary.DataSource = dtEntries;
                rptSummary.DataBind();
                rptSummary.Visible = true;
                lblEntryNotFound.Visible = false;
                dataDiv.Visible = true;

            }
            else
            {
                rptSummary.DataSource = null;
                rptSummary.DataBind();
                rptSummary.Visible = false;
                lblEntryNotFound.Visible = true;
                dataDiv.Visible = false;
            }
            divSummary.Visible = true;

        }

        protected void rptSummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var IronProductionQty = decimal.Parse(DataBinder.Eval(e.Item.DataItem, "Quantity").ToString());

            }
        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            var IronProductionId = int.Parse(e.CommandArgument.ToString());
            Id = IronProductionId;

            for (int i = 0; i < rptSummary.Items.Count; i++)
            {
                var tbxUpdatedQtyR = (TextBox)rptSummary.Items[i].FindControl("UpdatedIronedQty");
                if (tbxUpdatedQtyR.Visible)
                {
                    var lnkbtnEditR = (LinkButton)rptSummary.Items[i].FindControl("lnkbtnEdit");
                    var lblLinkingQtyR = (Label)rptSummary.Items[i].FindControl("LinkingQty");
                    var btnUpdateR = (LinkButton)rptSummary.Items[i].FindControl("lnkbtnUpdated");
                    var lnkbtnCancelR = (LinkButton)rptSummary.Items[i].FindControl("lnkbtnCancel");
                    var btnDeleteR = (LinkButton)rptSummary.Items[i].FindControl("lnkbtnDelete");

                    tbxUpdatedQtyR.Visible = false;
                    btnUpdateR.Visible = false;
                    lnkbtnCancelR.Visible = false;
                    btnDeleteR.Visible = true;
                    lblLinkingQtyR.Visible = true;
                    lnkbtnEditR.Visible = true;

                    var rptDeliveryR = (TextBox)rptSummary.Items[i].FindControl("rptIronProductionDate");
                    var rptStylesR = (DropDownList)rptSummary.Items[i].FindControl("rptStyles");
                    var rptColorsR = (DropDownList)rptSummary.Items[i].FindControl("rptColors");

                    var trAvailableBalanceR = (PlaceHolder)rptSummary.Items[i].FindControl("trAvailableBalance");

                    rptDeliveryR.Visible = false;
                    rptStylesR.Visible = false;
                    rptColorsR.Visible = false;

                    trAvailableBalanceR.Visible = false;

                    var lblIronProductionDateR = (Label)rptSummary.Items[i].FindControl("lblIronProductionDate");
                    var lblStylesR = (Label)rptSummary.Items[i].FindControl("lblStyles");
                    var lblColorsR = (Label)rptSummary.Items[i].FindControl("lblColors");


                    lblIronProductionDateR.Visible = true;
                    lblStylesR.Visible = true;
                    lblColorsR.Visible = true;
                }
            }


            var tbxUpdatedQty = (TextBox)((RepeaterItem)((LinkButton)sender).Parent).FindControl("UpdatedIronedQty");
            var tbxUpdatedRemarks = (TextBox)((RepeaterItem)((LinkButton)sender).Parent).FindControl("UpdatedRemarks");
            var lblIronedQty = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("IronedQty");
            var lblRemarks = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("Remarks");
            var btnUpdate = (LinkButton)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lnkbtnUpdated");
            var lnkbtnCancel = (LinkButton)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lnkbtnCancel");
            var btnDelete = (LinkButton)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lnkbtnDelete");

            var rptIronProductionDate = (TextBox)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptIronProductionDate");
            var rptStyles = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptStyles");
            var rptFinishingUnit = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptFinishingUnit");
            var rptLinkingFloor = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptLinkingFloor");
            var rptColors = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptColors");
            //var rptSizes = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptSizes");

            var lblIronProductionDate = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblIronProductionDate");
            var lblStyles = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblStyles");
            var lblFinishingUnit = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblFinishingUnit");
            var lblLinkingFloor = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblLinkingFloor");
            var lblColors = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblColors");
            //var lblSizes = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblSizes");

            var trAvailableBalance = (PlaceHolder)((RepeaterItem)((LinkButton)sender).Parent).FindControl("trAvailableBalance");
            // var rptAvailableBalance = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptAvailableBalance");

            var btnEdit = ((LinkButton)sender);

            tbxUpdatedQty.Visible = true;
            tbxUpdatedRemarks.Visible = true;
            lblIronedQty.Visible = false;
            lblRemarks.Visible = false;
            btnUpdate.Visible = true;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            lnkbtnCancel.Visible = true;
            trAvailableBalance.Visible = true;

            var ironProductionInfo = unitOfWork.GenericRepositories<IronProductionInfo>().GetByID(Id);

            LoadStyleDropdown(ironProductionInfo.BuyerId, rptStyles);

            CommonMethods.LoadDropdown(rptFinishingUnit, "FinishingUnits", 1, 0);
            CommonMethods.LoadDropdown(rptLinkingFloor, "FinishingUnitFloors", 1, 0);
            LoadColorDropdown(ironProductionInfo.StyleId, rptColors);
            //LoadSizesDropdown(trimingMendingInfo.StyleId, rptSizes);


            //set data
            rptStyles.SelectedValue = ironProductionInfo.StyleId + "";
            rptFinishingUnit.SelectedValue = ironProductionInfo.FinishingUnitId + "";
            rptLinkingFloor.SelectedValue = ironProductionInfo.LinkingFloorId + "";
            rptColors.SelectedValue = ironProductionInfo.ColorId + "";
            // rptSizes.SelectedValue = trimingMendingInfo.SizeId + "";
            rptIronProductionDate.Text = ironProductionInfo.IronProductionDate.ToString("yyyy-MM-dd");
            rptIronProductionDate.Visible = true;
            rptFinishingUnit.Visible = true;
            rptLinkingFloor.Visible = true;
            rptColors.Visible = true;
            // rptSizes.Visible = true;
            rptStyles.Visible = true;

            lblIronProductionDate.Visible = false;
            lblStyles.Visible = false;
            lblFinishingUnit.Visible = false;
            lblLinkingFloor.Visible = false;
            lblColors.Visible = false;
            //lblSizes.Visible = false;
            // lblAvailableBalance.Visible = true;

        }

        private void LoadStyleDropdown(int buyerId, DropDownList ddlStyles)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        private void LoadColorDropdown(int styleId, DropDownList ddl)
        {
            var dt = orderManager.GetColorsByStyleId(styleId);

            var dr = dt.NewRow();
            dr["BuyerColorId"] = "";
            dr["ColorDescription"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);
            ddl.DataSource = dt;
            ddl.DataTextField = "ColorDescription";
            ddl.DataValueField = "BuyerColorId";
            ddl.DataBind();
        }

        int Id
        {
            set { ViewState["Id"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["Id"]);
                }
                catch
                {
                    return 0;
                }
            }

        }

        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            Id = int.Parse(e.CommandArgument.ToString());
            if (Id != 0)
            {
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);


            }
            else
            {
                var title = "Error";
                var msg = "Sorry, you can not delete it.<br />Something went wrong.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            }
        }

        protected void lnkbtnUpdated_Command(object sender, CommandEventArgs e)
        {
            var Id = int.Parse(e.CommandArgument.ToString());
            var tbxUpdatedQty = (TextBox)((RepeaterItem)((LinkButton)sender).Parent).FindControl("UpdatedIronedQty");
            var tbxRemarks = (TextBox)((RepeaterItem)((LinkButton)sender).Parent).FindControl("UpdatedRemarks");
            var lblIronedQty = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("IronedQty");
            var btnUpdate = (LinkButton)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lnkbtnUpdated");
            var btnEdit = ((LinkButton)sender);
            var ddl = (LinkButton)sender;
            var rptIronProductionDate = (TextBox)ddl.NamingContainer.FindControl("rptIronProductionDate");
            var rptStyles = (DropDownList)ddl.NamingContainer.FindControl("rptStyles");
            var rptFinishingUnit = (DropDownList)ddl.NamingContainer.FindControl("rptFinishingUnit");
            var rptLinkingFloor = (DropDownList)ddl.NamingContainer.FindControl("rptLinkingFloor");
            var rptColors = (DropDownList)ddl.NamingContainer.FindControl("rptColors");
            //var rptSizes = (DropDownList)ddl.NamingContainer.FindControl("rptSizes");

            if (string.IsNullOrEmpty(rptIronProductionDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select Delivery Date.')", true);
                return;
            }
            else if (rptStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select style.')", true);
                return;
            }

            else if (rptFinishingUnit.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select Finishing Unit.')", true);
                return;
            }
            else if (rptLinkingFloor.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select Linking Floor.')", true);
                return;
            }
            else if (rptColors.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select color.')", true);
                return;
            }


            if (!string.IsNullOrEmpty(tbxUpdatedQty.Text))
            {
                var ironProductionInfo = unitOfWork.GenericRepositories<IronProductionInfo>().GetByID(Id);
                if (ironProductionInfo != null)
                {
                    var updatedIronedQty = decimal.Parse(tbxUpdatedQty.Text);
                    var styleId = 0;
                    if (rptStyles.SelectedValue != "")
                    {
                        styleId = int.Parse(rptStyles.SelectedValue);
                    }

                    var FinishingUnitId = 0;
                    if (rptFinishingUnit.SelectedValue != "")
                    {
                        FinishingUnitId = int.Parse(rptFinishingUnit.SelectedValue);
                    }
                    var LinkingFloorId = 0;
                    if (rptLinkingFloor.SelectedValue != "")
                    {
                        LinkingFloorId = int.Parse(rptLinkingFloor.SelectedValue);
                    }
                    var colorId = 0;
                    if (rptColors.SelectedValue != "")
                    {
                        colorId = int.Parse(rptColors.SelectedValue);
                    }


                    if (updatedIronedQty >= 0)
                    {
                        tbxUpdatedQty.Visible = true;
                        lblIronedQty.Visible = false;
                        btnUpdate.Visible = true;
                        btnEdit.Visible = false;

                        ironProductionInfo.Quantity = decimal.Parse(tbxUpdatedQty.Text);
                        ironProductionInfo.Remarks = tbxRemarks.Text;
                        ironProductionInfo.UpdatedDate = DateTime.Now;
                        ironProductionInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                        ironProductionInfo.IronProductionDate = DateTime.Parse(rptIronProductionDate.Text);
                        ironProductionInfo.StyleId = int.Parse(rptStyles.SelectedValue);
                        ironProductionInfo.FinishingUnitId = int.Parse(rptFinishingUnit.SelectedValue);
                        ironProductionInfo.LinkingFloorId = int.Parse(rptLinkingFloor.SelectedValue);
                        ironProductionInfo.ColorId = int.Parse(rptColors.SelectedValue);
                        //trimingMendingProductionInfo.SizeId = int.Parse(rptSizes.SelectedValue);
                        unitOfWork.GenericRepositories<IronProductionInfo>().Update(ironProductionInfo);
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                        LoadData();
                        //lblAvailableBalance.Visible = false;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('You can not issue more than available balance. Available balance is less than 0')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Unable to update.')", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter issue quantity.')", true);
            }

        }

        protected void SelectedIndexChanged(object sender, EventArgs e)
        {
            var ddl = (DropDownList)sender;
            var rptStyles = (DropDownList)ddl.NamingContainer.FindControl("rptStyles");
            var rptColors = (DropDownList)ddl.NamingContainer.FindControl("rptColors");

            var rptAvailableBalance = (Label)ddl.NamingContainer.FindControl("rptAvailableBalance");

        }

        protected void rptStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            var rptStyles = (DropDownList)sender;
            var rptColors = (DropDownList)((RepeaterItem)((DropDownList)sender).Parent).FindControl("rptColors");
            //var rptSizes = (DropDownList)((RepeaterItem)((DropDownList)sender).Parent).FindControl("rptSizes");
            var rptAvailableBalance = (Label)((RepeaterItem)((DropDownList)sender).Parent).FindControl("rptAvailableBalance");
            rptColors.Items.Clear();

            if (rptStyles.SelectedValue != "")
            {
                var styleId = int.Parse(rptStyles.SelectedValue);
                LoadColorDropdown(styleId, rptColors);


            }
        }

        protected void lnkbtnCancel_Command(object sender, CommandEventArgs e)
        {
            LoadData();

        }


    }
}