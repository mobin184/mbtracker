﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class AddBoothReceive : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var dtFinishigUnits = new CommonManager().GetFinishingUnitsByUser(CommonMethods.SessionInfo.UserId);
                ddlUserFinishingUnit.DataTextField = "UnitName";
                ddlUserFinishingUnit.DataValueField = "FinishingUnitId";
                ddlUserFinishingUnit.DataSource = dtFinishigUnits;
                ddlUserFinishingUnit.DataBind();
                tbxReceivedDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                CommonMethods.LoadDropdown(ddlKnitingFactory, "KnittingFactory", 1, 0);
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                CommonMethods.LoadDropdown(ddlReceivedTypeName, "receivedType", 1, 0);

                if (Request.QueryString["knittingReceivedId"] != null)
                {
                    KnittingReceivedId = int.Parse(Tools.UrlDecode(Request.QueryString["knittingReceivedId"]));
                    var recInfo = unitOfWork.GenericRepositories<DailyKnittingReceive>().GetByID(KnittingReceivedId);
                    BindDataToUI(recInfo);
                }
            }
        }

        private void BindDataToUI(DailyKnittingReceive recInfo)
        {
            CommonMethods.LoadDropdownById(ddlStyles, recInfo.BuyerId, "BuyerStyles", 1, 0);
            ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, recInfo.BuyerId);
            ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, recInfo.StyleId);
            ddlUserFinishingUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUserFinishingUnit, recInfo.FinishingUnitId);
            ddlReceivedTypeName.SelectedIndex = CommonMethods.MatchDropDownItem(ddlReceivedTypeName, recInfo.ReceivedTypeName);
            //ddlKnitingFactory.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnitingFactory, recInfo.KnittingFactoryId);
            ddlKnitingFactory.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnitingFactory, recInfo.KnittingFactoryId ?? 0);

            ddlBuyers.Enabled = false;
            ddlStyles.Enabled = false;
            ddlUserFinishingUnit.Enabled = false;
            ddlReceivedTypeName.Enabled = false;
            tbxReceivedDate.Text = recInfo.ReceivedDate.ToString("yyyy-MM-dd");
            tbxChalanNumber.Text = recInfo.ChalanNo;
            tbxGatePassNo.Text = recInfo.GatePassNo;
            tbxVehicleNo.Text = recInfo.VehicleNo;
            tbxNewStyle.Text = recInfo.NewStyle;
            tbxDescription.Text = recInfo.Description;
            tbxBearer.Text = recInfo.Bearer;
            LoadEntryInfo(recInfo.StyleId, recInfo.BuyerId);
            divColors.Visible = true;
        }

        int KnittingReceivedId
        {
            set { ViewState["knittingReceivedId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["knittingReceivedId"]);
                }
                catch
                {
                    return 0;
                }
            }

        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
            }
            divColors.Visible = false;
            rptEntryInfo.Visible = false;
            rptEntryInfo.DataSource = null;
            rptEntryInfo.DataBind();
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlStyles.SelectedValue != "")
            {
                LoadEntryInfo(int.Parse(ddlStyles.SelectedValue), int.Parse(ddlBuyers.SelectedValue));
            }
            else
            {
                divColors.Visible = false;
                rptEntryInfo.Visible = false;
                rptEntryInfo.DataSource = null;
                rptEntryInfo.DataBind();
            }
        }

        private void LoadEntryInfo(int styleId, int buyerId)
        { 

            var dtColors = new CommonManager().GetColorsByStyle(styleId);
            if (dtColors.Rows.Count > 0)
            {
                dtColors.Columns.Add("StyleId", typeof(int));
                dtColors.Columns.Add("BuyerId", typeof(int));
                dtColors.Columns.Add("StyleName", typeof(string));
                dtColors.Columns.Add("BuyerName", typeof(string));
                foreach (DataRow row in dtColors.Rows)
                {
                    row["StyleId"] = styleId;
                    row["BuyerId"] = buyerId;
                    row["StyleName"] = ddlStyles.SelectedItem.Text;
                    row["BuyerName"] = ddlBuyers.SelectedItem.Text;
                }

                rptEntryInfo.DataSource = dtColors;
                rptEntryInfo.DataBind();
                rptEntryInfo.Visible = true;
                lblNoDataFound.Visible = false;
                if (KnittingReceivedId != 0)
                {
                    btnSaveEntries.Visible = false;
                    btnUpdateEntries.Visible = true;
                }
                else
                {
                    btnSaveEntries.Visible = true;
                    btnUpdateEntries.Visible = false;
                }
                divColors.Visible = true;
            }
            else
            {
                btnSaveEntries.Visible = false;
                btnUpdateEntries.Visible = false;
                lblNoDataFound.Visible = true;
                rptEntryInfo.DataSource = null;
                rptEntryInfo.DataBind();
                rptEntryInfo.Visible = false;
                divColors.Visible = false;
            }
        }

        protected void gvSizeQuantity1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var buyerId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId")).Text);
                var styleId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId")).Text);
                //var dtSizes = buyerManager.GetSizes(buyerId);
                var dtSizes = buyerManager.GetStyleSizes(styleId);
                TextBox txtSizeQty = null;
                Label lblSizeId = null;

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new TextBox();
                    txtSizeQty.ID = "tbxReceivQuantity" + i.ToString();
                    txtSizeQty.Text = "";
                    txtSizeQty.Width = 60;
                    txtSizeQty.TextMode = TextBoxMode.Number;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);


                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);

                    var entryId = new Label();
                    entryId.ID = "lblentryId" + i.ToString();
                    entryId.Text = "";
                    entryId.Visible = false;
                    e.Row.Cells[i].Controls.Add(entryId);
                    var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());
                    var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                    var buyerColorId = int.Parse(string.IsNullOrEmpty(lblBuyerColorId.Text) ? "0" : lblBuyerColorId.Text);
                    if (buyerColorId != 0)
                    {
                        var sizeQty = CommonMethods.GetSizeQuantityByStyleAndColor(styleId, buyerColorId, sizeId);
                        if (sizeQty > 0)
                        {
                            if (KnittingReceivedId != 0)
                            {
                                var sizeInfo = unitOfWork.GenericRepositories<DailyKnittingReceiveColorSizes>().Get(x => x.KnittingReceiveId == KnittingReceivedId && x.BuyerColorId == buyerColorId && x.SizeId == sizeId).FirstOrDefault();
                                if (sizeInfo != null)
                                {
                                    txtSizeQty.Text = sizeInfo.ReceivedQty + "";
                                    entryId.Text = sizeInfo.Id + "";
                                }
                            } 
                        }
                        else
                        {
                            txtSizeQty.Enabled = false;
                        }
                    }
                }
            }
        }

        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //var buyerId = int.Parse(ddlBuyers.SelectedValue);
                var styleId = int.Parse(ddlStyles.SelectedValue);
                //var dtSizes = buyerManager.GetSizes(buyerId);
                var dtSizes = buyerManager.GetStyleSizes(styleId);
                GridView gvSizeQuantity1 = (GridView)e.Item.FindControl("gvSizeQuantity1");
                Label lblNoSizeFound = (Label)e.Item.FindControl("lblNoSizeFound");
                if (dtSizes.Rows.Count > 0)
                {
                    DataTable dtOneRow = new DataTable();
                    DataRow dr = null;
                    dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                    dr = dtOneRow.NewRow();
                    dr["RowNumber"] = 1;
                    dtOneRow.Rows.Add(dr);

                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantity1.Columns.Add(templateField);
                    }

                    gvSizeQuantity1.DataSource = dtOneRow;
                    gvSizeQuantity1.DataBind();
                    gvSizeQuantity1.Visible = true;
                    lblNoSizeFound.Visible = false;

                }
                else
                {
                    gvSizeQuantity1.Visible = false;
                    lblNoSizeFound.Visible = true;
                }
            }
        }

        protected void btnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {
                var receivedDate = DateTime.Parse(tbxReceivedDate.Text);
                if (receivedDate <= DateTime.Now)
                {

                    List<DailyKnittingReceiveColorSizes> lstColorSizes = new List<DailyKnittingReceiveColorSizes>();
                    int BuyerIdForQuery = int.Parse(ddlBuyers.SelectedValue);
                    var dkr = new DailyKnittingReceive()
                    {
                        FinishingUnitId = int.Parse(ddlUserFinishingUnit.SelectedValue),
                        BuyerId = int.Parse(ddlBuyers.SelectedValue),
                        StyleId = int.Parse(ddlStyles.SelectedValue),
                        ReceivedTypeName = int.Parse(ddlReceivedTypeName.SelectedValue),
                        ChalanNo = tbxChalanNumber.Text,
                        GatePassNo = tbxGatePassNo.Text,
                        VehicleNo = tbxVehicleNo.Text,
                        NewStyle = tbxNewStyle.Text,
                        Bearer = tbxBearer.Text,
                        Description = tbxDescription.Text,
                        ReceivedDate = receivedDate,
                        ReceivedQty = 0,
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName,
                        KnittingFactoryId = int.Parse(ddlKnitingFactory.SelectedValue)
                    };

                    int totalRecQty = 0;
                    for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                    {
                        var buyerColorId = int.Parse(((Label)rptEntryInfo.Items[i].FindControl("lblBuyerColorId")).Text);
                        var gvSizeQty = (GridView)rptEntryInfo.Items[i].FindControl("gvSizeQuantity1");
                        for (int j = 0; j < gvSizeQty.Columns.Count; j++)
                        {
                            var sizeId = int.Parse(((Label)gvSizeQty.Rows[0].Cells[j].FindControl("lblSizeId" + j)).Text);
                            var recQty = ((TextBox)gvSizeQty.Rows[0].Cells[j].FindControl("tbxReceivQuantity" + j)).Text;
                            if (!string.IsNullOrEmpty(recQty))
                            {
                                var SizeTypeId = unitOfWork.GenericRepositories<BuyerSizes>().Get(x => x.Id == sizeId && x.BuyerId == BuyerIdForQuery).Select(x => x.SizeTypeId).First();
                                var colorSize = new DailyKnittingReceiveColorSizes()
                                {
                                    FinishingUnitId = int.Parse(ddlUserFinishingUnit.SelectedValue),
                                    BuyerId = int.Parse(ddlBuyers.SelectedValue),
                                    StyleId = int.Parse(ddlStyles.SelectedValue),
                                    ReceivedTypeName = int.Parse(ddlReceivedTypeName.SelectedValue),
                                    SizeTypeId = SizeTypeId ?? 0,
                                    ReceivedDate = receivedDate,
                                    BuyerColorId = buyerColorId,
                                    KnittingReceiveId = dkr.Id,
                                    ReceivedQty = int.Parse(recQty),
                                    SizeId = sizeId,
                                    CreateDate = DateTime.Now,
                                    CreatedBy = CommonMethods.SessionInfo.UserName
                                };
                                lstColorSizes.Add(colorSize);
                                totalRecQty += int.Parse(recQty);
                            }
                        }
                    }

                    if (lstColorSizes.Count > 0)
                    {
                        dkr.ReceivedQty = totalRecQty;
                        unitOfWork.GenericRepositories<DailyKnittingReceive>().Insert(dkr);
                        foreach (var item in lstColorSizes)
                        {
                            unitOfWork.GenericRepositories<DailyKnittingReceiveColorSizes>().Insert(item);
                        }
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter size quantity.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cant receive in future date')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void btnUpdateEntries_Click(object sender, EventArgs e)
        {
            try
            {
                int BuyerIdForQuery = int.Parse(ddlBuyers.SelectedValue);
                var receivedDate = DateTime.Parse(tbxReceivedDate.Text);
                if (receivedDate <= DateTime.Now)
                {
                    var dkr = unitOfWork.GenericRepositories<DailyKnittingReceive>().GetByID(KnittingReceivedId);
                    List<DailyKnittingReceiveColorSizes> lstColorSizes = new List<DailyKnittingReceiveColorSizes>();

                    dkr.FinishingUnitId = int.Parse(ddlUserFinishingUnit.SelectedValue);
                    dkr.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                    dkr.StyleId = int.Parse(ddlStyles.SelectedValue);
                    dkr.ReceivedTypeName = int.Parse(ddlReceivedTypeName.SelectedValue);
                    dkr.ChalanNo = tbxChalanNumber.Text;
                    dkr.GatePassNo = tbxGatePassNo.Text;
                    dkr.VehicleNo = tbxVehicleNo.Text;
                    dkr.NewStyle = tbxNewStyle.Text;
                    dkr.Bearer = tbxBearer.Text;
                    dkr.Description = tbxDescription.Text;
                    dkr.ReceivedDate = receivedDate;
                    dkr.UpdateDate = DateTime.Now;
                    dkr.UpdatedBy = CommonMethods.SessionInfo.UserName;
                    dkr.KnittingFactoryId = int.Parse(ddlKnitingFactory.SelectedValue);


                    int totalRecQty = 0;
                    for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                    {
                        var buyerColorId = int.Parse(((Label)rptEntryInfo.Items[i].FindControl("lblBuyerColorId")).Text);
                        var gvSizeQty = (GridView)rptEntryInfo.Items[i].FindControl("gvSizeQuantity1");
                        for (int j = 0; j < gvSizeQty.Columns.Count; j++)
                        {
                            var sizeId = int.Parse(((Label)gvSizeQty.Rows[0].Cells[j].FindControl("lblSizeId" + j)).Text);
                            var recQty = ((TextBox)gvSizeQty.Rows[0].Cells[j].FindControl("tbxReceivQuantity" + j)).Text;
                            var SizeTypeId = unitOfWork.GenericRepositories<BuyerSizes>().Get(x => x.Id == sizeId && x.BuyerId == BuyerIdForQuery).Select(x => x.SizeTypeId).First();
                            if (!string.IsNullOrEmpty(recQty))
                            {
                                var colorSize = new DailyKnittingReceiveColorSizes()
                                {
                                    FinishingUnitId = int.Parse(ddlUserFinishingUnit.SelectedValue),
                                    BuyerId = int.Parse(ddlBuyers.SelectedValue),
                                    StyleId = int.Parse(ddlStyles.SelectedValue),
                                    ReceivedTypeName = int.Parse(ddlReceivedTypeName.SelectedValue),
                                    SizeTypeId = SizeTypeId ?? 0,
                                    ReceivedDate = receivedDate,

                                    BuyerColorId = buyerColorId,
                                    KnittingReceiveId = dkr.Id,
                                    ReceivedQty = int.Parse(recQty),
                                    SizeId = sizeId,
                                    CreateDate = DateTime.Now,
                                    CreatedBy = CommonMethods.SessionInfo.UserName
                                };
                                lstColorSizes.Add(colorSize);
                                totalRecQty += int.Parse(recQty);
                            }
                        }
                    }

                    if (lstColorSizes.Count > 0)
                    {
                        dkr.ReceivedQty = totalRecQty;
                        unitOfWork.GenericRepositories<DailyKnittingReceive>().Update(dkr);
                        var previousColors = unitOfWork.GenericRepositories<DailyKnittingReceiveColorSizes>().Get(x => x.KnittingReceiveId == KnittingReceivedId).ToList();
                        foreach (var item in previousColors)
                        {
                            unitOfWork.GenericRepositories<DailyKnittingReceiveColorSizes>().Delete(item);
                        }
                        foreach (var item in lstColorSizes)
                        {
                            unitOfWork.GenericRepositories<DailyKnittingReceiveColorSizes>().Insert(item);
                        }
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                        //Response.AddHeader("REFRESH", "2;URL=ViewKnittingReceiveEntries.aspx");
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewBoothReceives.aspx');", true);

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter size quantity!')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cant receive in future date')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void lnkbtnCalculateColorTotal_Click(object sender, EventArgs e)
        {
            ShowColorAndStyleTotal();
        }

        private void ShowColorAndStyleTotal()
        {
            decimal colorTotal = 0;
            decimal styleTotal = 0;
            decimal styleTotalpcs = 0;

            for (int i = 0; i < rptEntryInfo.Items.Count; i++)
            {

             
                GridView gvSizeQuantity1 = (GridView)rptEntryInfo.Items[i].FindControl("gvSizeQuantity1");
                var lblTotalInLBS = (Label)rptEntryInfo.Items[i].FindControl("lblColorTotalLBS");
              

                colorTotal = 0;

                for (int j = 0; j < gvSizeQuantity1.Columns.Count; j++)
                {
                    string sizeQuantityTextBox2Id = string.Concat("tbxReceivQuantity", j.ToString());
                    TextBox tbxSizeQty = (TextBox)gvSizeQuantity1.Rows[0].Cells[j].FindControl(sizeQuantityTextBox2Id);

                    if (tbxSizeQty.Text != "")
                    {
                        colorTotal += decimal.Parse(tbxSizeQty.Text);
                    }
                }
                lblTotalInLBS.Text = colorTotal.ToString();
               
                styleTotal += colorTotal;
               
            }

           
            lblStyleTotalpcs.Text = "Style Total: " + styleTotal.ToString() + "(pcs)";
        }


    }
}