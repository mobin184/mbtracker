﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class AddKnittingMachineStatus : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        ProductionManager productionManager = new ProductionManager();
        DataTable dtUserUnits;
        DataTable dtShift;
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["unitId"] != null && Request.QueryString["knittingDate"] != null)
                {
                    UnitId = int.Parse(Tools.UrlDecode(Request.QueryString["unitId"]));
                    KnittingDate = DateTime.Parse(Tools.UrlDecode(Request.QueryString["knittingDate"]));
                    ShiftId = int.Parse(Tools.UrlDecode(Request.QueryString["shiftId"]));
                    lblProductionEntry.Text = "Update Daily Knitting Machine Status:";
                }

                PopulateGeneralInfo();
            }
        }

        int UnitId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["unitId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["unitId"] = value; }
        }

        int ShiftId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["shiftId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["shiftId"] = value; }
        }


        DateTime KnittingDate
        {
            get
            {
                try
                {
                    return Convert.ToDateTime(ViewState["knittingDate"]);
                }
                catch
                {
                    return DateTime.Today;
                }
            }
            set { ViewState["knittingDate"] = value; }
        }

        private void PopulateGeneralInfo()
        {

            dtUserUnits = new CommonManager().GetProductionUnitsByUser(Convert.ToInt32(CommonMethods.SessionInfo.UserId));
            var lst = unitOfWork.GenericRepositories<Shifts>().Get().ToList();
            dtShift = Tools.ToDataTable(lst);
            if (dtUserUnits.Rows.Count > 0)
            {

                DataTable dtProdEntryGeneralInfo = StatusEntryGeneralInfo();
                if (UnitId == 0)
                {
                    dtProdEntryGeneralInfo.Rows.Add("", CommonMethods.SessionInfo.UserFullName.ToString());
                    PopulateEntryInfo(Convert.ToInt32(dtUserUnits.Rows[0][0].ToString()));

                }
                else
                {
                    dtProdEntryGeneralInfo.Rows.Add(String.Format("{0:yyyy-MM-dd}", KnittingDate), CommonMethods.SessionInfo.UserFullName.ToString());
                    PopulateEntryInfo(UnitId);
                }

                rptGeneralInfo.DataSource = dtProdEntryGeneralInfo;
                rptGeneralInfo.DataBind();

            }

        }

        public DataTable StatusEntryGeneralInfo()
        {
            DataTable dt = new DataTable("StatusEntryGeneralInfo");
            dt.Columns.Add("ProductionEntryDate", typeof(string));
            dt.Columns.Add("UserName", typeof(string));
            return dt;
        }

        protected void rptGeneralInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlUserUnits = (DropDownList)e.Item.FindControl("ddlUserUnits");
                ddlUserUnits.DataTextField = "UnitName";
                ddlUserUnits.DataValueField = "ProductionUnitId";
                ddlUserUnits.DataSource = dtUserUnits;
                ddlUserUnits.DataBind();

                DropDownList ddlShift = (DropDownList)e.Item.FindControl("ddlShift");
                CommonMethods.LoadDropdown(ddlShift, "Shifts", 1, 0);
                //ddlShift.DataTextField = "ShiftName";
                //ddlShift.DataValueField = "Id";
                //ddlShift.DataSource = dtShift;
                //ddlShift.DataBind();

                var lblProductionEntryDate = (Label)e.Item.FindControl("lblProductionEntryDate");
                var tbxProductionDate = (TextBox)e.Item.FindControl("tbxProductionDate");
                if (tbxProductionDate != null && !string.IsNullOrEmpty(lblProductionEntryDate.Text))
                {
                    tbxProductionDate.Text = string.IsNullOrEmpty(lblProductionEntryDate.Text) ? "" : Convert.ToDateTime(lblProductionEntryDate.Text).ToString("yyyy-MM-dd");
                }



                if (UnitId == 0)
                {
                    ddlUserUnits.SelectedIndex = 0;
                    ddlShift.SelectedIndex = 0;
                }
                else
                {
                    ddlUserUnits.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUserUnits, UnitId);
                    ddlShift.SelectedIndex = CommonMethods.MatchDropDownItem(ddlShift, ShiftId);
                    ddlUserUnits.Enabled = false;
                    tbxProductionDate.Enabled = false;
                    ddlShift.Enabled = false;
                }
            }
        }

        protected void rptGeneralInfo_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlUserUnits = (DropDownList)e.Item.FindControl("ddlUserUnits");
                ddlUserUnits.SelectedIndexChanged += ddlUserUnits_SelectedIndexChanged;
            }
        }

        protected void ddlUserUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlUserUnits = (DropDownList)sender;
            PopulateEntryInfo(Convert.ToInt32(ddlUserUnits.SelectedValue));
        }

        private void PopulateEntryInfo(int unitId)
        {
            string sql = $"Exec usp_GetAllMachineBrandByUnitId {unitId}";
            var dt = unitOfWork.GetDataTableFromSql(sql);

            if (dt.Rows.Count > 0)
            {
                lblNoDataFound.Visible = false;
                rptEntryInfo.Visible = true;
                if (UnitId == 0)
                {
                    lnkbtnSaveEntries.Visible = true;
                    lnkbtnUpdateEntries.Visible = false;
                }
                else
                {
                    lnkbtnSaveEntries.Visible = false;
                    lnkbtnUpdateEntries.Visible = true;
                }
            }
            else
            {
                lblNoDataFound.Visible = true;
                rptEntryInfo.Visible = false;
                lnkbtnSaveEntries.Visible = false;
                lnkbtnUpdateEntries.Visible = false;
            }
            rptEntryInfo.DataSource = dt;
            rptEntryInfo.DataBind();
        }

        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {
                var unitId = int.Parse(((DropDownList)rptGeneralInfo.Items[0].FindControl("ddlUserUnits")).SelectedValue);
                var stShift = ((DropDownList)rptGeneralInfo.Items[0].FindControl("ddlShift")).SelectedValue;
               
                var stproductionDate = ((TextBox)rptGeneralInfo.Items[0].FindControl("tbxProductionDate")).Text;
                if (!string.IsNullOrEmpty(stShift))
                {
                    var shiftId = int.Parse(stShift);
                    if (!string.IsNullOrEmpty(stproductionDate))
                    {
                        var prductionDate = DateTime.Parse(stproductionDate);
                        if (prductionDate <= DateTime.Today)
                        {

                            if (!CheckIsExist(unitId, shiftId, prductionDate))
                            {
                                var flag = true;
                                for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                                {

                                    var lblMachineBrandId = (Label)rptEntryInfo.Items[i].FindControl("lblMachineBrandId");
                                    var tbxNoOfRunningMachine = (TextBox)rptEntryInfo.Items[i].FindControl("tbxNoOfRunningMachine");
                                    var tbxUsedForSample = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForSample");
                                    var tbxUsedForSizeSet = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForSizeSet");
                                    var tbxLongTimeStoped = (TextBox)rptEntryInfo.Items[i].FindControl("tbxLongTimeStoped");
                                    var tbxTemporaryStoped = (TextBox)rptEntryInfo.Items[i].FindControl("tbxTemporaryStoped");
                                    var tbxIdleMachine = (TextBox)rptEntryInfo.Items[i].FindControl("tbxIdleMachine");
                                    var tbxUsedForSetup = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForSetup");
                                    var tbxUsedForOtherWorks = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForOtherWorks");

                                    var tbxPurposeOfOtherWorks = (TextBox)rptEntryInfo.Items[i].FindControl("tbxPurposeOfOtherWorks");

                                    var lblNumberOfMachines = (Label)rptEntryInfo.Items[i].FindControl("lblNumberOfMachines");
                                    var noOfMc = int.Parse(lblNumberOfMachines.Text);

                                    var machineBrandId = string.IsNullOrEmpty(lblMachineBrandId.Text) ? 0 : int.Parse(lblMachineBrandId.Text);
                                    var runningMachine = string.IsNullOrEmpty(tbxNoOfRunningMachine.Text) ? (int?)null : int.Parse(tbxNoOfRunningMachine.Text);
                                    var forSample = string.IsNullOrEmpty(tbxUsedForSample.Text) ? (int?)null : int.Parse(tbxUsedForSample.Text);
                                    var forSizeSet = string.IsNullOrEmpty(tbxUsedForSizeSet.Text) ? (int?)null : int.Parse(tbxUsedForSizeSet.Text);
                                    var longTimeStop = string.IsNullOrEmpty(tbxLongTimeStoped.Text) ? (int?)null : int.Parse(tbxLongTimeStoped.Text);
                                    var tempStop = string.IsNullOrEmpty(tbxTemporaryStoped.Text) ? (int?)null : int.Parse(tbxTemporaryStoped.Text);
                                    var idleMachine = string.IsNullOrEmpty(tbxIdleMachine.Text) ? (int?)null : int.Parse(tbxIdleMachine.Text);
                                    var usedForSetup = string.IsNullOrEmpty(tbxUsedForSetup.Text) ? (int?)null : int.Parse(tbxUsedForSetup.Text);
                                    var otherWorks = string.IsNullOrEmpty(tbxUsedForOtherWorks.Text) ? (int?)null : int.Parse(tbxUsedForOtherWorks.Text);
                                    //var otherWorksPurpose = string.IsNullOrEmpty(tbxPurposeOfOtherWorks.Text) ? (string?)null : tbxPurposeOfOtherWorks.Text.ToString());

                                    //int totalUsed = ((runningMachine ?? 0) + (forSample ?? 0) + (forSizeSet ?? 0) + (longTimeStop ?? 0) + (tempStop ?? 0) + (idleMachine ?? 0) + (usedForSetup ?? 0) + (otherWorks ?? 0));
                                    //if (noOfMc != totalUsed)
                                    //{
                                    //    flag = false;
                                    //}
                                    //else
                                    //{
                                        var dailyMachineStatus = new DailyKnittingMachineStatus()
                                        {
                                            ProductionDate = prductionDate,
                                            KnittingUnitId = unitId,
                                            ShiftId = shiftId,
                                            MachineBrandId = machineBrandId,
                                            NoOfRunningMachine = runningMachine,
                                            UsedForSample = forSample,
                                            UsedForSizeSet = forSizeSet,
                                            LongTimeStop = longTimeStop,
                                            TemporaryStop = tempStop,
                                            IdleMachine = idleMachine,
                                            UsedForSetup = usedForSetup,
                                            UsedForOtherWorks = otherWorks,
                                            PurposeOfOtherWorks = tbxPurposeOfOtherWorks.Text.ToString(),
                                            CreatedBy = CommonMethods.SessionInfo.UserName,
                                            CreateDate = DateTime.Now
                                        };
                                        unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().Insert(dailyMachineStatus);
                                    //}                                    
                                }

                                if (flag)
                                {
                                    unitOfWork.Save();
                                    unitOfWork.ExeccuteRawQyery($"EXEC usp_Insert_DailyMachineStatusSummary '{prductionDate.Date}',{unitId}");
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Total number of machines and number entered should be equal!')", true);
                                }   
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Entry already exist for this unit, shift and date!')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('You cant enter future date data!')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please enter production date!')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please select shift !')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            } 
        }

        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {
            try
            {
                var unitId = int.Parse(((DropDownList)rptGeneralInfo.Items[0].FindControl("ddlUserUnits")).SelectedValue);
                var stShift = ((DropDownList)rptGeneralInfo.Items[0].FindControl("ddlShift")).SelectedValue;

                var stproductionDate = ((TextBox)rptGeneralInfo.Items[0].FindControl("tbxProductionDate")).Text;
                if (!string.IsNullOrEmpty(stShift))
                {
                    var shiftId = int.Parse(stShift);
                    if (!string.IsNullOrEmpty(stproductionDate))
                    {
                        var prductionDate = DateTime.Parse(stproductionDate);
                        if (prductionDate <= DateTime.Today)
                        {
                            var flag = true;
                            for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                            {
                                var lblDailyKnittingMachineStatusId = (Label)rptEntryInfo.Items[i].FindControl("lblDailyKnittingMachineStatusId");
                                var lblMachineBrandId = (Label)rptEntryInfo.Items[i].FindControl("lblMachineBrandId");
                                var tbxNoOfRunningMachine = (TextBox)rptEntryInfo.Items[i].FindControl("tbxNoOfRunningMachine");
                                var tbxUsedForSample = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForSample");
                                var tbxUsedForSizeSet = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForSizeSet");
                                var tbxLongTimeStoped = (TextBox)rptEntryInfo.Items[i].FindControl("tbxLongTimeStoped");
                                var tbxTemporaryStoped = (TextBox)rptEntryInfo.Items[i].FindControl("tbxTemporaryStoped");
                                var tbxIdleMachine = (TextBox)rptEntryInfo.Items[i].FindControl("tbxIdleMachine");
                                var tbxUsedForSetup = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForSetup");

                                var tbxUsedForOtherWorks = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForOtherWorks");

                                var tbxPurposeOfOtherWorks = (TextBox)rptEntryInfo.Items[i].FindControl("tbxPurposeOfOtherWorks");

                                var lblNumberOfMachines = (Label)rptEntryInfo.Items[i].FindControl("lblNumberOfMachines");
                                var noOfMc = int.Parse(lblNumberOfMachines.Text);

                                var DailyKnittingMachineStatusId = string.IsNullOrEmpty(lblDailyKnittingMachineStatusId.Text) ? 0 : int.Parse(lblDailyKnittingMachineStatusId.Text);
                                var machineBrandId = string.IsNullOrEmpty(lblMachineBrandId.Text) ? 0 : int.Parse(lblMachineBrandId.Text);
                                var runningMachine = string.IsNullOrEmpty(tbxNoOfRunningMachine.Text) ? (int?)null : int.Parse(tbxNoOfRunningMachine.Text);
                                var forSample = string.IsNullOrEmpty(tbxUsedForSample.Text) ? (int?)null : int.Parse(tbxUsedForSample.Text);
                                var forSizeSet = string.IsNullOrEmpty(tbxUsedForSizeSet.Text) ? (int?)null : int.Parse(tbxUsedForSizeSet.Text);
                                var longTimeStop = string.IsNullOrEmpty(tbxLongTimeStoped.Text) ? (int?)null : int.Parse(tbxLongTimeStoped.Text);
                                var tempStop = string.IsNullOrEmpty(tbxTemporaryStoped.Text) ? (int?)null : int.Parse(tbxTemporaryStoped.Text);
                                var idleMachine = string.IsNullOrEmpty(tbxIdleMachine.Text) ? (int?)null : int.Parse(tbxIdleMachine.Text);
                                var usedForSetup = string.IsNullOrEmpty(tbxUsedForSetup.Text) ? (int?)null : int.Parse(tbxUsedForSetup.Text);
                                var otherWorks = string.IsNullOrEmpty(tbxUsedForOtherWorks.Text) ? (int?)null : int.Parse(tbxUsedForOtherWorks.Text);
                                //int totalUsed = ((runningMachine ?? 0 )+ (forSample ?? 0 )+ (forSizeSet ?? 0) + (longTimeStop ?? 0) + (tempStop ?? 0) + (idleMachine ?? 0) + (usedForSetup ?? 0) + (otherWorks ?? 0));
                                //if (noOfMc != totalUsed)
                                //{
                                //    flag = false;
                                //}
                                //else
                                //{

                                    var info = unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().GetByID(DailyKnittingMachineStatusId);
                                    if (info == null)
                                    {


                                        var dailyMachineStatus = new DailyKnittingMachineStatus()
                                        {
                                            ProductionDate = prductionDate,
                                            KnittingUnitId = unitId,
                                            ShiftId = shiftId,
                                            MachineBrandId = machineBrandId,
                                            NoOfRunningMachine = runningMachine,
                                            UsedForSample = forSample,
                                            UsedForSizeSet = forSizeSet,
                                            LongTimeStop = longTimeStop,
                                            TemporaryStop = tempStop,
                                            IdleMachine = idleMachine,
                                            UsedForSetup = usedForSetup,
                                            UsedForOtherWorks = otherWorks,
                                            PurposeOfOtherWorks = tbxPurposeOfOtherWorks.Text.ToString(),
                                            CreatedBy = CommonMethods.SessionInfo.UserName,
                                            CreateDate = DateTime.Now
                                        };
                                        unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().Insert(dailyMachineStatus);
                                    }
                                    else
                                    {
                                        info.MachineBrandId = machineBrandId;
                                        info.NoOfRunningMachine = runningMachine;
                                        info.UsedForSample = forSample;
                                        info.UsedForSizeSet = forSizeSet;
                                        info.LongTimeStop = longTimeStop;
                                        info.TemporaryStop = tempStop;
                                        info.IdleMachine = idleMachine;
                                        info.UsedForSetup = usedForSetup;
                                        info.UsedForOtherWorks = otherWorks;
                                        info.PurposeOfOtherWorks = tbxPurposeOfOtherWorks.Text.ToString();

                                        info.UpdatedBy = CommonMethods.SessionInfo.UserName;
                                        info.UpdateDate = DateTime.Now;
                                        unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().Update(info);
                                    }
                                //}                              
                            }

                            if (flag)
                            {
                                unitOfWork.Save();
                                unitOfWork.ExeccuteRawQyery($"EXEC usp_Insert_DailyMachineStatusSummary '{prductionDate.Date}',{unitId}");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
                                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewKnittingMachineStatus.aspx');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Total no of machine and entered machine should be equal!')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('You cant enter future date data!')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please enter production date!')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please select shift !')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private bool CheckIsExist(int unitId, int shiftId, DateTime productionDate) => unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().Get(x => x.KnittingUnitId == unitId && x.ShiftId == shiftId && x.ProductionDate == productionDate).Any();

        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (UnitId != 0 && ShiftId != 0)
                {
                    unitOfWork = new UnitOfWork();
                    var lblDailyKnittingMachineStatusId = (Label)e.Item.FindControl("lblDailyKnittingMachineStatusId");
                    var lblMachineBrandId = (Label)e.Item.FindControl("lblMachineBrandId");
                    var machinBrandId = int.Parse(lblMachineBrandId.Text);
                    var tbxNoOfRunningMachine = (TextBox)e.Item.FindControl("tbxNoOfRunningMachine");
                    var tbxUsedForSample = (TextBox)e.Item.FindControl("tbxUsedForSample");
                    var tbxUsedForSizeSet = (TextBox)e.Item.FindControl("tbxUsedForSizeSet");
                    var tbxLongTimeStoped = (TextBox)e.Item.FindControl("tbxLongTimeStoped");
                    var tbxTemporaryStoped = (TextBox)e.Item.FindControl("tbxTemporaryStoped");
                    var tbxIdleMachine = (TextBox)e.Item.FindControl("tbxIdleMachine");
                    var tbxUsedForOtherWorks = (TextBox)e.Item.FindControl("tbxUsedForOtherWorks");
                    var tbxPurposeOfOtherWorks = (TextBox)e.Item.FindControl("tbxPurposeOfOtherWorks");
                    var tbxUsedForSetup = (TextBox)e.Item.FindControl("tbxUsedForSetup");


                    var statusInfo = unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().Get().Where(x => x.KnittingUnitId == UnitId && x.ShiftId == ShiftId && x.MachineBrandId == machinBrandId && x.ProductionDate == KnittingDate).FirstOrDefault();
                    if (statusInfo != null)
                    {
                        lblDailyKnittingMachineStatusId.Text = statusInfo.Id + "";
                        tbxNoOfRunningMachine.Text = statusInfo.NoOfRunningMachine + "";
                        tbxUsedForSample.Text = statusInfo.UsedForSample + "";
                        tbxUsedForSizeSet.Text = statusInfo.UsedForSizeSet + "";
                        tbxLongTimeStoped.Text = statusInfo.LongTimeStop + "";
                        tbxTemporaryStoped.Text = statusInfo.TemporaryStop + "";
                        tbxIdleMachine.Text = statusInfo.IdleMachine + "";
                        tbxUsedForOtherWorks.Text = statusInfo.UsedForOtherWorks + "";
                        tbxPurposeOfOtherWorks.Text = statusInfo.PurposeOfOtherWorks + "";
                        tbxUsedForSetup.Text = statusInfo.UsedForSetup + "";
                    }
                }
            }
        }
    }
}