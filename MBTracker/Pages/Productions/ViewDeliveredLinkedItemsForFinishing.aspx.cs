﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace MBTracker.Pages.Productions
{
    public partial class ViewDeliveredLinkedItemsForFinishing : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableEditButton();
                //tbxDeliveryDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
        }

        private void EnableDisableEditButton()
        {
            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Finishing - Mgmt,Finishing - Linking");
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewDeliveredLinkedItemsForFinishing", 1, 1);
        }

        protected void btnViewEntries_Click(object sender, EventArgs e)
        {
            try
            {
                LoadData();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }


        protected void LoadData()
        {
            divSummary.Visible = false;
            divColors.Visible = false;
            rptEntryInfo.DataSource = null;
            rptEntryInfo.DataBind();


            if (!string.IsNullOrEmpty(tbxDeliveryDate.Text))
            {
                var delDate = DateTime.Parse(tbxDeliveryDate.Text);
                var dtEntries = unitOfWork.GetDataTableFromSql($"Exec usp_GetLinkedItemsDeliveredToFinishingSectionByUserAndDate {CommonMethods.SessionInfo.UserId},'{delDate}'");
                if (dtEntries.Rows.Count > 0)
                {
                    //var userRole = CommonMethods.SessionInfo.RoleName;
                    //var editEnabled = 0;
                    //if ((userRole.Contains("Finishing") && (userRole.Contains("Booth") || userRole.Contains("Mgmt"))) || userRole.Contains("Admin") || userRole.Contains("Top Mgmt"))
                    //{
                    //    editEnabled = 1;
                    //}
                    //dtEntries.Columns.Add("EditEnabled", typeof(int));
                    //foreach (DataRow row in dtEntries.Rows)
                    //{
                    //    row["EditEnabled"] = editEnabled;
                    //}
                    rptSummary.DataSource = dtEntries;
                    rptSummary.DataBind();
                    rptSummary.Visible = true;
                    lblEntryNotFound.Visible = false;
                    divSummary.Visible = true;
                }
                else
                {
                    rptSummary.DataSource = null;
                    rptSummary.DataBind();
                    rptSummary.Visible = false;
                    lblEntryNotFound.Visible = true;
                    divSummary.Visible = false;
                }
                
            }
            else
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please select date received.')", true);
            }
        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            var deliveryId = int.Parse(e.CommandArgument.ToString());
            Response.Redirect("DeliverLinkedItemsForFinishing.aspx?deliveryId=" + Tools.UrlEncode(deliveryId+""));
        }

        int DeliveryId
        {
            get
            {
                try
                {
                    return int.Parse(ViewState["deliveryId"].ToString());
                }
                catch (Exception)
                {
                    return 0;
                }
            }
            set { ViewState["deliveryId"] = value; }
        }


        protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        {
            string[] args = e.CommandArgument.ToString().Split(',');
            DeliveryId = int.Parse(args[0]);
            var IsReceived = int.Parse(args[1]);
            var dtRecColors = unitOfWork.GetDataTableFromSql($"Exec usp_GetLinkedItemsDeliveredToFinishingSectionByDeliveryId {DeliveryId}");
            divColors.Visible = true;
            if (dtRecColors.Rows.Count > 0)
            {
                rptEntryInfo.DataSource = dtRecColors;
                rptEntryInfo.DataBind();
                rptEntryInfo.Visible = true;
                lblNoDataFound.Visible = false;

                var userRole = CommonMethods.SessionInfo.RoleName;
                var confirmEnable = false;
                if (userRole.Contains("Finishing - Packing") || userRole.Contains("Finishing - Mgmt") || userRole.Contains("Admin") || userRole.Contains("Top Mgmt"))
                {
                    confirmEnable = true;
                }
                if(IsReceived != 1 && confirmEnable)
                {
                    btnConfirm.Visible = true;
                }
                else
                {
                    btnConfirm.Visible = false;
                }
            }
            else
            {
                rptEntryInfo.DataSource = null;
                rptEntryInfo.DataBind();
                rptEntryInfo.Visible = false;
                lblNoDataFound.Visible = true;
                btnConfirm.Visible = false;
            }
            ScriptManager.RegisterStartupScript(this,this.GetType(), "message", "scrollDown()", true);
        }

        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var buyerId = int.Parse(((Label)e.Item.FindControl("lblBuyerId")).Text);
                var dtSizes = buyerManager.GetSizes(buyerId);
                GridView gvSizeQuantity1 = (GridView)e.Item.FindControl("gvSizeQuantity1");
                Label lblNoSizeFound = (Label)e.Item.FindControl("lblNoSizeFound");
                if (dtSizes.Rows.Count > 0)
                {
                    DataTable dtOneRow = new DataTable();
                    DataRow dr = null;
                    dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                    dr = dtOneRow.NewRow();
                    dr["RowNumber"] = 1;
                    dtOneRow.Rows.Add(dr);

                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantity1.Columns.Add(templateField);
                    }

                    gvSizeQuantity1.DataSource = dtOneRow;
                    gvSizeQuantity1.DataBind();
                    gvSizeQuantity1.Visible = true;
                    lblNoSizeFound.Visible = false;

                }
                else
                {
                    gvSizeQuantity1.Visible = false;
                    lblNoSizeFound.Visible = true;
                }
            }
        }

        protected void gvSizeQuantity1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var buyerId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId")).Text);
                var styleId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId")).Text);
                var dtSizes = buyerManager.GetSizes(buyerId);
                Label txtSizeQty = null;
                Label lblSizeId = null;

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new Label();
                    txtSizeQty.ID = "tbxReceivQuantity" + i.ToString();
                    txtSizeQty.Text = "";
                    txtSizeQty.Width = 70;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);


                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);

                    var entryId = new Label();
                    entryId.ID = "lblentryId" + i.ToString();
                    entryId.Text = "";
                    entryId.Visible = false;
                    e.Row.Cells[i].Controls.Add(entryId);


                    var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());
                    var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                    var buyerColorId = int.Parse(string.IsNullOrEmpty(lblBuyerColorId.Text) ? "0" : lblBuyerColorId.Text);
                    if (buyerColorId != 0)
                    {
                        var sizeInfo = unitOfWork.GenericRepositories<LinkedItemsDeliveredToFinishingSectionColorSizes>().Get(x => x.LinkedItemsDeliveredId == DeliveryId && x.BuyerColorId == buyerColorId && x.SizeId == sizeId).FirstOrDefault();
                        if (sizeInfo != null)
                        {
                            txtSizeQty.Text = sizeInfo.DeliveryQty + "";
                            entryId.Text = sizeInfo.Id + "";
                        }
                    }
                }
            }
        }



        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            var delInfo = unitOfWork.GenericRepositories<LinkedItemsDeliveredToFinishingSection>().GetByID(DeliveryId);
            delInfo.IsReceived = true;
            delInfo.ReceivedBy = CommonMethods.SessionInfo.UserName;
            delInfo.ReceivedDate = DateTime.Now;
            unitOfWork.GenericRepositories<LinkedItemsDeliveredToFinishingSection>().Update(delInfo);
            unitOfWork.Save();
            ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.success('Confirmation successful.');", true);
            LoadData();
        }





    }
}