﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Productions
{
    public partial class FixColumnAndHeader : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                var dtKnittingUnit = new CommonManager().GetProductionUnitsByUser(CommonMethods.SessionInfo.UserId);
                var dtFinishingUnit = new CommonManager().LoadFinishingUnits();
                ddlUserProductionUnit.DataTextField = "UnitName";
                ddlUserProductionUnit.DataValueField = "ProductionUnitId";
                ddlUserProductionUnit.DataSource = dtKnittingUnit;
                ddlUserProductionUnit.DataBind();
                ddlFinishingUnit.DataTextField = "FinishingUnitName";
                ddlFinishingUnit.DataValueField = "FinishingUnitId";
                ddlFinishingUnit.DataSource = dtFinishingUnit;
                ddlFinishingUnit.DataBind();
                tbxDelivryDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                //CommonMethods.LoadDropdown(ddlBuyers, "Buyers WHERE IsActive = 1 ORDER BY BuyerName", 1, 0);

                if (Request.QueryString["knittingDeliveredId"] != null)
                {
                    KnittingDeliveredId = int.Parse(Tools.UrlDecode(Request.QueryString["knittingDeliveredId"]));
                    var recInfo = unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnit>().GetByID(KnittingDeliveredId);
                    BindDataToUI(recInfo);
                }
            }

        }

        private void BindDataToUI(KnittedItemDelvierdToFinishingUnit recInfo)
        {
            CommonMethods.LoadDropdownById(ddlStyles, recInfo.BuyerId, "BuyerStyles", 1, 0);
            ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, recInfo.BuyerId);
            ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, recInfo.StyleId);
            ddlUserProductionUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUserProductionUnit, recInfo.KnittingUnitId);
            ddlFinishingUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlFinishingUnit, recInfo.FinishingUnitId);
            ddlBuyers.Enabled = false;
            ddlStyles.Enabled = false;
            ddlUserProductionUnit.Enabled = false;
            tbxDelivryDate.Text = recInfo.DeliveredDate.ToString("yyyy-MM-dd");
            tbxChalanNumber.Text = recInfo.ChalanNo;
            tbxGatePassNo.Text = recInfo.GatePassNo;
            tbxVehicleNo.Text = recInfo.VehicleNo;
            LoadEntryInfo(recInfo.StyleId, recInfo.BuyerId);
            divColors.Visible = true;
        }

        int KnittingDeliveredId
        {
            set { ViewState["knittingReceivedId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["knittingReceivedId"]);
                }
                catch
                {
                    return 0;
                }
            }

        }



        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
            }
            divColors.Visible = false;
            rptEntryInfo.Visible = false;
            rptEntryInfo.DataSource = null;
            rptEntryInfo.DataBind();
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlStyles.SelectedValue != "")
            {
                LoadEntryInfo(int.Parse(ddlStyles.SelectedValue), int.Parse(ddlBuyers.SelectedValue));
            }
            else
            {
                divColors.Visible = false;
                rptEntryInfo.Visible = false;
                rptEntryInfo.DataSource = null;
                rptEntryInfo.DataBind();
            }
        }

        private void LoadEntryInfo(int styleId, int buyerId)
        {

            var dtColors = new CommonManager().GetColorsByStyle(styleId);
            if (dtColors.Rows.Count > 0)
            {
                dtColors.Columns.Add("StyleId", typeof(int));
                dtColors.Columns.Add("BuyerId", typeof(int));
                dtColors.Columns.Add("StyleName", typeof(string));
                dtColors.Columns.Add("BuyerName", typeof(string));
                foreach (DataRow row in dtColors.Rows)
                {
                    row["StyleId"] = styleId;
                    row["BuyerId"] = buyerId;
                    row["StyleName"] = ddlStyles.SelectedItem.Text;
                    row["BuyerName"] = ddlBuyers.SelectedItem.Text;
                }

                rptEntryInfo.DataSource = dtColors;
                rptEntryInfo.DataBind();
                rptEntryInfo.Visible = true;
                lblNoDataFound.Visible = false;
                if (KnittingDeliveredId != 0)
                {
                    btnSaveEntries.Visible = false;
                    btnUpdateEntries.Visible = true;
                }
                else
                {
                    btnSaveEntries.Visible = true;
                    btnUpdateEntries.Visible = false;
                }
                divColors.Visible = true;
            }
            else
            {
                btnSaveEntries.Visible = false;
                btnUpdateEntries.Visible = false;
                lblNoDataFound.Visible = true;
                rptEntryInfo.DataSource = null;
                rptEntryInfo.DataBind();
                rptEntryInfo.Visible = false;
                divColors.Visible = false;
            }
        }


        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var buyerId = int.Parse(ddlBuyers.SelectedValue);
                var dtSizes = buyerManager.GetSizes(buyerId);
                GridView gvSizeQuantity1 = (GridView)e.Item.FindControl("gvSizeQuantity1");
                Label lblNoSizeFound = (Label)e.Item.FindControl("lblNoSizeFound");
                if (dtSizes.Rows.Count > 0)
                {
                    DataTable dtOneRow = new DataTable();
                    DataRow dr = null;
                    dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                    dr = dtOneRow.NewRow();
                    dr["RowNumber"] = 1;
                    dtOneRow.Rows.Add(dr);

                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantity1.Columns.Add(templateField);
                    }

                    gvSizeQuantity1.DataSource = dtOneRow;
                    gvSizeQuantity1.DataBind();
                    gvSizeQuantity1.Visible = true;
                    lblNoSizeFound.Visible = false;

                }
                else
                {
                    gvSizeQuantity1.Visible = false;
                    lblNoSizeFound.Visible = true;
                }
            }
        }


        protected void gvSizeQuantity1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var buyerId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId")).Text);
                var styleId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId")).Text);
                var dtSizes = buyerManager.GetSizes(buyerId);
                TextBox txtSizeQty = null;
                Label lblSizeId = null;

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new TextBox();
                    txtSizeQty.ID = "tbxReceivQuantity" + i.ToString();
                    txtSizeQty.Text = "";
                    txtSizeQty.Width = 60;
                    txtSizeQty.TextMode = TextBoxMode.Number;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);


                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);

                    var entryId = new Label();
                    entryId.ID = "lblentryId" + i.ToString();
                    entryId.Text = "";
                    entryId.Visible = false;
                    e.Row.Cells[i].Controls.Add(entryId);

                    var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());
                    var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                    var buyerColorId = int.Parse(string.IsNullOrEmpty(lblBuyerColorId.Text) ? "0" : lblBuyerColorId.Text);
                    if (buyerColorId != 0)
                    {
                        //var sizeQty = CommonMethods.GetSizeQuantityByStyleAndColor(styleId, buyerColorId, sizeId);
                        //if (sizeQty > 0)
                        //{
                        if (KnittingDeliveredId != 0)
                        {
                            var sizeInfo = unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnitColorSizes>().Get(x => x.KnittedDeliveredId == KnittingDeliveredId && x.BuyerColorId == buyerColorId && x.SizeId == sizeId).FirstOrDefault();
                            if (sizeInfo != null)
                            {
                                txtSizeQty.Text = sizeInfo.DeliveredQty + "";
                                entryId.Text = sizeInfo.Id + "";
                            }
                        }
                        //}
                        //else
                        //{
                        //    txtSizeQty.Enabled = false;
                        //}
                    }
                }
            }
        }



        protected void btnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {
                var deliveredDate = DateTime.Parse(tbxDelivryDate.Text);
                if (deliveredDate <= DateTime.Now)
                {

                    List<KnittedItemDelvierdToFinishingUnitColorSizes> lstColorSizes = new List<KnittedItemDelvierdToFinishingUnitColorSizes>();
                    var dkr = new KnittedItemDelvierdToFinishingUnit()
                    {
                        KnittingUnitId = int.Parse(ddlUserProductionUnit.SelectedValue),
                        BuyerId = int.Parse(ddlBuyers.SelectedValue),
                        StyleId = int.Parse(ddlStyles.SelectedValue),
                        FinishingUnitId = int.Parse(ddlFinishingUnit.SelectedValue),
                        DeliveredDate = deliveredDate,
                        ChalanNo = tbxChalanNumber.Text,
                        GatePassNo = tbxGatePassNo.Text,
                        VehicleNo = tbxVehicleNo.Text,
                        DeliveredQty = 0,
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };

                    int totalRecQty = 0;
                    for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                    {
                        var buyerColorId = int.Parse(((Label)rptEntryInfo.Items[i].FindControl("lblBuyerColorId")).Text);
                        var gvSizeQty = (GridView)rptEntryInfo.Items[i].FindControl("gvSizeQuantity1");
                        for (int j = 0; j < gvSizeQty.Columns.Count; j++)
                        {
                            var sizeId = int.Parse(((Label)gvSizeQty.Rows[0].Cells[j].FindControl("lblSizeId" + j)).Text);
                            var recQty = ((TextBox)gvSizeQty.Rows[0].Cells[j].FindControl("tbxReceivQuantity" + j)).Text;
                            if (!string.IsNullOrEmpty(recQty))
                            {
                                var colorSize = new KnittedItemDelvierdToFinishingUnitColorSizes()
                                {
                                    BuyerColorId = buyerColorId,
                                    KnittedDeliveredId = dkr.Id,
                                    DeliveredQty = int.Parse(recQty),
                                    SizeId = sizeId,
                                    CreateDate = DateTime.Now,
                                    CreatedBy = CommonMethods.SessionInfo.UserName
                                };
                                lstColorSizes.Add(colorSize);
                                totalRecQty += int.Parse(recQty);
                            }
                        }
                    }

                    if (lstColorSizes.Count > 0)
                    {
                        dkr.DeliveredQty = totalRecQty;
                        unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnit>().Insert(dkr);
                        foreach (var item in lstColorSizes)
                        {
                            unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnitColorSizes>().Insert(item);
                        }
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter size quantity.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cant deliver in future date')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void btnUpdateEntries_Click(object sender, EventArgs e)
        {
            try
            {
                var deliveredDate = DateTime.Parse(tbxDelivryDate.Text);
                if (deliveredDate <= DateTime.Now)
                {
                    var dkr = unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnit>().GetByID(KnittingDeliveredId);
                    List<KnittedItemDelvierdToFinishingUnitColorSizes> lstColorSizes = new List<KnittedItemDelvierdToFinishingUnitColorSizes>();

                    dkr.KnittingUnitId = int.Parse(ddlUserProductionUnit.SelectedValue);
                    dkr.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                    dkr.StyleId = int.Parse(ddlStyles.SelectedValue);
                    dkr.FinishingUnitId = int.Parse(ddlFinishingUnit.SelectedValue);
                    dkr.ChalanNo = tbxChalanNumber.Text;
                    dkr.GatePassNo = tbxGatePassNo.Text;
                    dkr.VehicleNo = tbxVehicleNo.Text;
                    dkr.DeliveredDate = deliveredDate;
                    dkr.UpdateDate = DateTime.Now;
                    dkr.UpdatedBy = CommonMethods.SessionInfo.UserName;


                    int totalRecQty = 0;
                    for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                    {
                        var buyerColorId = int.Parse(((Label)rptEntryInfo.Items[i].FindControl("lblBuyerColorId")).Text);
                        var gvSizeQty = (GridView)rptEntryInfo.Items[i].FindControl("gvSizeQuantity1");
                        for (int j = 0; j < gvSizeQty.Columns.Count; j++)
                        {
                            var sizeId = int.Parse(((Label)gvSizeQty.Rows[0].Cells[j].FindControl("lblSizeId" + j)).Text);
                            var recQty = ((TextBox)gvSizeQty.Rows[0].Cells[j].FindControl("tbxReceivQuantity" + j)).Text;
                            if (!string.IsNullOrEmpty(recQty))
                            {
                                var colorSize = new KnittedItemDelvierdToFinishingUnitColorSizes()
                                {
                                    BuyerColorId = buyerColorId,
                                    KnittedDeliveredId = dkr.Id,
                                    DeliveredQty = int.Parse(recQty),
                                    SizeId = sizeId,
                                    CreateDate = DateTime.Now,
                                    CreatedBy = CommonMethods.SessionInfo.UserName
                                };
                                lstColorSizes.Add(colorSize);
                                totalRecQty += int.Parse(recQty);
                            }
                        }
                    }

                    if (lstColorSizes.Count > 0)
                    {
                        dkr.DeliveredQty = totalRecQty;
                        unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnit>().Update(dkr);
                        var previousColors = unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnitColorSizes>().Get(x => x.KnittedDeliveredId == KnittingDeliveredId).ToList();
                        foreach (var item in previousColors)
                        {
                            unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnitColorSizes>().Delete(item);
                        }
                        foreach (var item in lstColorSizes)
                        {
                            unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnitColorSizes>().Insert(item);
                        }
                        unitOfWork.Save();

                        //ClientScriptManager cs = Page.ClientScript;
                        ////cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
                        //cs.RegisterStartupScript(GetType(), "toastr_message", "toastr.success('Updated successfully.')");


                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);



                        //Response.AddHeader("REFRESH", "2;URL=ViewDeliveredKnittedPartsToFinishingUnit.aspx");
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewDeliveredKnittedPartsToFinishingUnit.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter size quantity!')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cant deliver in future date')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }




    }
}