﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class ReportNewProblem : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        ProductionManager productionManager = new ProductionManager();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               //CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
               //CommonMethods.LoadFinishingFloorDropDown(ddlUserFinishingUnitFloor);
                CommonMethods.LoadDropdown(ddlReportedStage, "PossibleProblemReportingStages", 1, 0);
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);


                //if (Request.QueryString["Id"] != null)
                //{
                //    ProblemId = int.Parse(Tools.UrlDecode(Request.QueryString["Id"]));
                //    var info = unitOfWork.GenericRepositories<ReportedProblems>().GetByID(ProblemId);
                //    PopulateInfo(info);
                //    btnSaveEntries.Visible = false;
                //    btnUpdateEntries.Visible = true;
                //}
            }
        }

        private void PopulateInfo(ReportedProblems info)
        {
            //div1.Visible = false;
            //div2.Visible = false;
            //buttonsDiv.Visible = false;

            //if(info.ReportedStageId == 1)
            //{
            //    div1.Visible = true;
            //    buttonsDiv.Visible = true;
            //}
            //else if(info.ReportedStageId == 2)
            //{
            //    div2.Visible = true;
            //    //buttonsDiv.Visible = true;
            //}


            //CommonMethods.LoadDropdownById(ddlStyles, info.BuyerId ?? 0, "BuyerStyles", 1, 0);
            //var dtSizes = buyerManager.GetSizes(info.BuyerId ?? 0);
            //DataTable dtCloned = dtSizes.Clone();
            //dtCloned.Columns[0].DataType = typeof(string);
            //foreach (DataRow row in dtSizes.Rows)
            //{
            //    dtCloned.ImportRow(row);
            //}
            //var nrow = dtCloned.NewRow();
            //nrow["Id"] = "";
            //nrow["SizeName"] = "--Select--";
            //dtCloned.Rows.InsertAt(nrow, 0);
            //ddlSize.DataTextField = "SizeName";
            //ddlSize.DataValueField = "Id";
            //ddlSize.DataSource = dtCloned;
            //ddlSize.DataBind();

            //ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, info.BuyerId ?? 0);
            //ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, info.StyleId ?? 0);
            //ddlUserFinishingUnitFloor.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUserFinishingUnitFloor, info.FinishingUnitFloorId ?? 0);
            //ddlReportedStage.SelectedIndex = CommonMethods.MatchDropDownItem(ddlReportedStage, info.ReportedStageId);
            //ddlSize.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSize, info.SizeId ?? 0);
            //tbxLOTNumber.Text = info.LOTNumber;
            //tbxTCNumber.Text = info.TCNumber;
            //tbxMCNumber.Text = info.MCNumber;
            //tbxKnitId.Text = info.KnitId;
            //tbxProblemDescription.Text = info.ProblemDescription;

        }

        int ProblemId
        {
            set { ViewState["problemId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["problemId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            //ddlSize.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
                //    var dtSizes = buyerManager.GetSizes(int.Parse(ddlBuyers.SelectedValue));
                //    DataTable dtCloned = dtSizes.Clone();
                //    dtCloned.Columns[0].DataType = typeof(string);
                //    foreach (DataRow row in dtSizes.Rows)
                //    {
                //        dtCloned.ImportRow(row);
                //    }
                //    var nrow = dtCloned.NewRow();
                //    nrow["Id"] = "";
                //    nrow["SizeName"] = "--Select--";
                //    dtCloned.Rows.InsertAt(nrow, 0);
                //    ddlSize.DataTextField = "SizeName";
                //    ddlSize.DataValueField = "Id";
                //    ddlSize.DataSource = dtCloned;
                //    ddlSize.DataBind();

            }
        }

        protected void btnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {
                //bool IsValid = true;
                //var reportingStage = ddlReportedStage.SelectedValue;
                //if (reportingStage != "")
                //{
                //    var stageId = int.Parse(reportingStage);
                //    if (stageId == 1)
                //    {
                //        Page.Validate("bodyLinking");
                //        if (!Page.IsValid)
                //        {
                //            IsValid = false;
                //        }
                //    }
                //    else if (stageId == 2)
                //    {

                //    }
                //}
                //if (IsValid)
                //{
                    ReportedProblems rp = new ReportedProblems()
                    {
                        ReportedStageId = int.Parse(ddlReportedStage.SelectedValue),
                      //  FinishingUnitFloorId = int.Parse(ddlUserFinishingUnitFloor.SelectedValue),
                        BuyerId = int.Parse(ddlBuyers.SelectedValue),
                        StyleId = int.Parse(ddlStyles.SelectedValue),
                        //SizeId = string.IsNullOrEmpty(ddlSize.SelectedValue) ? (int?)null : int.Parse(ddlSize.SelectedValue),
                        //LOTNumber = tbxLOTNumber.Text,
                        //TCNumber = tbxTCNumber.Text,
                        //MCNumber = tbxMCNumber.Text,
                        //KnitId = tbxKnitId.Text,
                        ProblemDescription = tbxProblemDescription.Text,
                        CreatedBy = CommonMethods.SessionInfo.UserName,
                        CreateDate = DateTime.Now
                    };
                    unitOfWork.GenericRepositories<ReportedProblems>().Insert(rp);
                    unitOfWork.Save();

                    UploadFiles(rp.Id);


                    CommonMethods.SendEventNotification(9, rp.BuyerId, rp.StyleId, null, rp.Id);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ReportNewProblem.aspx');", true);
                //}

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        protected void btnUpdateEntries_Click(object sender, EventArgs e)
        {
            //try
            //{

            //    bool IsValid = true;
            //    var reportingStage = ddlReportedStage.SelectedValue;
            //    if (reportingStage != "")
            //    {
            //        var stageId = int.Parse(reportingStage);
            //        if (stageId == 1)
            //        {
            //            Page.Validate("bodyLinking");
            //            if (!Page.IsValid)
            //            {
            //                IsValid = false;
            //            }
            //        }
            //        else if (stageId == 2)
            //        {

            //        }
            //    }
            //    if (IsValid)
            //    {
            //        var rp = unitOfWork.GenericRepositories<ReportedProblems>().GetByID(ProblemId);

            //        rp.ReportedStageId = int.Parse(ddlReportedStage.SelectedValue);
            //        rp.FinishingUnitFloorId = int.Parse(ddlUserFinishingUnitFloor.SelectedValue);
            //        rp.BuyerId = int.Parse(ddlBuyers.SelectedValue);
            //        rp.StyleId = int.Parse(ddlStyles.SelectedValue);
            //        rp.SizeId = string.IsNullOrEmpty(ddlSize.SelectedValue) ? (int?)null : int.Parse(ddlSize.SelectedValue);
            //        rp.LOTNumber = tbxLOTNumber.Text;
            //        rp.TCNumber = tbxTCNumber.Text;
            //        rp.MCNumber = tbxMCNumber.Text;
            //        rp.KnitId = tbxKnitId.Text;
            //        rp.ProblemDescription = tbxProblemDescription.Text;
            //        rp.UpdatedBy = CommonMethods.SessionInfo.UserName;
            //        rp.UpdateDate = DateTime.Now;

            //        unitOfWork.GenericRepositories<ReportedProblems>().Update(rp);
            //        unitOfWork.Save();
            //        CommonMethods.SendEventNotification(9, null, null, null, rp.Id);
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
            //        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewReportedProblems.aspx');", true);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            //}
        }

        protected void ddlReportedStage_SelectedIndexChanged(object sender, EventArgs e)
        {
            //var reportingStage = ddlReportedStage.SelectedValue;
            //div1.Visible = false;
            //div2.Visible = false;
            //buttonsDiv.Visible = false;
            //if (reportingStage != "")
            //{
            //    var reportingStageId = int.Parse(reportingStage);
            //    if (reportingStageId == 1)
            //    {
            //        div1.Visible = true;
            //        buttonsDiv.Visible = true;
            //    }
            //    else if (reportingStageId == 2)
            //    {
            //        div2.Visible = true;
            //        //buttonsDiv.Visible = true;
            //    }
            //}
        }

        public void UploadFiles(int reportedProblemId)
        {
            foreach (HttpPostedFile postedFile in FileUpload1.PostedFiles)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                string contentType = postedFile.ContentType;
                int fileSize = postedFile.ContentLength;
                if (fileSize > 0)
                {
                    using (Stream fs = postedFile.InputStream)
                    {
                        using (BinaryReader br = new BinaryReader(fs))
                        {
                            byte[] bytes = br.ReadBytes((Int32)fs.Length);

                            productionManager.SaveReportedProblemFiles(reportedProblemId, fileName, contentType, fileSize, bytes, CommonMethods.SessionInfo.UserName, DateTime.Now);
                        }
                    }
                }
            }
        }

    }
}