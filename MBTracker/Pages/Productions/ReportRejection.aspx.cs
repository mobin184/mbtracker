﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using Repositories;
using MBTracker.EF;
using MBTracker.Code_Folder.Productions;

namespace MBTracker.Pages.Productions
{
    public partial class ReportRejection : System.Web.UI.Page
    {



        ProductionManager productionManager = new ProductionManager();
        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        DataTable dtSizes;
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);

                if (Request.QueryString["rejectionId"] != null)
                {
                    ProductRejectionId = int.Parse(Tools.UrlDecode(Request.QueryString["rejectionId"]));
                    var recInfo = unitOfWork.GenericRepositories<ProductRejections>().GetByID(ProductRejectionId);
                    BindDataToUI(recInfo);
                }

            }
             
        }


        private void BindDataToUI(ProductRejections recInfo)
        {
            CommonMethods.LoadDropdownById(ddlStyles, recInfo.BuyerId, "BuyerStyles", 1, 0);
            ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, recInfo.BuyerId);
            ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, recInfo.StyleId);

            BindOrdersByStyle(recInfo.StyleId);
            ddlOrders.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrders, recInfo.OrderId);

            CommonMethods.LoadFinishingFloorDropDown(ddlFinishingFloor);
            ddlFinishingFloor.SelectedIndex = CommonMethods.MatchDropDownItem(ddlFinishingFloor, recInfo.FinishingFloorId);
            //ddlBuyers.Enabled = false;
            //ddlStyles.Enabled = false;
            //ddlOrders.Enabled = false;

            lblTitle.Text = "Update Rejection";
            lblMessage.Text = "Update Rejection Information";
            btnSaveRejection.Visible = false;
            btnUpdateRejection.Visible = true;


            LoadEntryInfo(recInfo.StyleId, recInfo.BuyerId, recInfo.OrderId);
            //divColors.Visible = true;
        }

        private void LoadEntryInfo(int styleId, int buyerId, int orderId)
        {

            LoadOrderColorsByOrderId(orderId);

        }



        int ProductRejectionId
        {
            set { ViewState["rejectionId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["rejectionId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
                rptColorAndSizes.DataSource = null;
                rptColorAndSizes.DataBind();
                pnlColorDeliveryCountryAndQuantity.Visible = false;
                ddlOrders.Items.Clear();
            }
            else
            {
                ddlStyles.Items.Clear();
                ddlOrders.Items.Clear();
                pnlColorDeliveryCountryAndQuantity.Visible = false;
            }

        }

        private void BindStylesByBuyer(int buyerId)
        {

            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);

        }


        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlOrders.Items.Clear();

            if (ddlStyles.SelectedValue != "")
            {

                BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
            }

        }


        private void BindOrdersByStyle(int styleId)
        {

            CommonMethods.LoadOrderDropdownByStyle(ddlOrders, styleId, 1, 0);

        }

        protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOrders.SelectedValue != "")
            {
                LoadSizeInfo();
                ddlBuyers.Enabled = false;
                resetButton.Visible = true;

                CommonMethods.LoadFinishingFloorDropDown(ddlFinishingFloor);
                LoadOrderColorsByOrderId(Convert.ToInt32(ddlOrders.SelectedValue));
            }
            else
            {
                pnlColorDeliveryCountryAndQuantity.Visible = false;
            }
        }


        private void LoadOrderColorsByOrderId(int orderId)
        {
            DataTable dt = new DataTable();
            dt = orderManager.GetOrderColorsByOrderId(orderId);

            if (dt.Rows.Count > 0)
            {
                rptColorAndSizes.DataSource = dt;
                rptColorAndSizes.DataBind();
                pnlColorDeliveryCountryAndQuantity.Visible = true;

            }
            else
            {
                pnlColorDeliveryCountryAndQuantity.Visible = false;

            }
        }


        protected void LoadSizeInfo()
        {
            dtSizes = buyerManager.GetSizes(Convert.ToInt32(ddlBuyers.SelectedValue));

        }


        protected void rptColorAndSizes_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                GridView gvSizeAndQty = (GridView)e.Item.FindControl("gvSizeAndQty");
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                if (dtSizes == null || dtSizes.Rows.Count == 0)
                {
                    LoadSizeInfo();
                }


                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeAndQty.Columns.Add(templateField);

                    }

                }

                gvSizeAndQty.DataSource = dtOneRow;
                gvSizeAndQty.DataBind();


                if (ProductRejectionId > 0)
                {
                    TextBox tbxNotes = (TextBox)e.Item.FindControl("tbxNotes");
                    Label lblRejectionColorId = (Label)e.Item.FindControl("lblColorId");

                    DataTable dt = productionManager.GetRejectionNotesByColor(ProductRejectionId, int.Parse(lblRejectionColorId.Text));
                    if (dt.Rows.Count > 0 && dt.Rows[0][0].ToString() != "")
                    {
                        tbxNotes.Text = dt.Rows[0][0].ToString();
                    }
                }

            }


        }

        protected void gvSizeAndQty_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

               


                TextBox txtSizeQty = null;
                Label lblSizeId = null;
                LoadSizeInfo();

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new TextBox();
                    txtSizeQty.TextMode = TextBoxMode.Number;
                    txtSizeQty.ID = "txtSizeQty1" + i.ToString();

                    if (ProductRejectionId > 0)
                    {

                        var lblRejectionColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblColorId");
                        int rejectionColorId = Convert.ToInt32(lblRejectionColorId.Text);
                        var sizeId = dtSizes.Rows[i][0].ToString();

                        DataTable dt = productionManager.GetRejectionSizeAndQuantity(ProductRejectionId, rejectionColorId, int.Parse(sizeId));

                        if (dt.Rows.Count > 0 && dt.Rows[0][1].ToString() != "0")
                        {
                            txtSizeQty.Text = dt.Rows[0][1].ToString();
                        }
                    }

                    txtSizeQty.Width = 70;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);

                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId1" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);
                }
            }
        }


        public DataTable RejectionColorAndSize()
        {
            DataTable dt = new DataTable("ProductRejectionColorAndSizes");
            dt.Columns.Add("ProductRejectionId", typeof(int));
            dt.Columns.Add("BuyerColorId", typeof(int));
            dt.Columns.Add("SizeId", typeof(int));
            dt.Columns.Add("SizeQuantity", typeof(int));
            dt.Columns.Add("Notes", typeof(string));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }


        protected void btnReset_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
      
            int buyerId = int.Parse(ddlBuyers.SelectedValue);
            int styleId = int.Parse(ddlStyles.SelectedValue);
            int orderId = int.Parse(ddlOrders.SelectedValue);
            int finishingFloorId = int.Parse(ddlFinishingFloor.SelectedValue);

            int productRejectionResultId = productionManager.InsertProductRejection(buyerId, styleId, orderId, finishingFloorId, CommonMethods.SessionInfo.UserName, DateTime.Now);

            if (productRejectionResultId > 0)
            {
                int saveRejectionColorsAndSizesReslult = saveRejectionColorsAndSizes(productRejectionResultId);

                if (saveRejectionColorsAndSizesReslult > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Color and sizes could not be saved.')", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Production rejection could not be saved!')", true);
            }

        }

        public int saveRejectionColorsAndSizes(int productRejectionId)
        {

            int sizeQuantity = 0;
            int rejectionSaveResult = 0;

     
            DataTable dtRejectionColorAndSizes = RejectionColorAndSize();

            for (int i = 0; i < rptColorAndSizes.Items.Count; i++)
            {
                Label lblBuyerColorId = (Label)rptColorAndSizes.Items[i].FindControl("lblColorId");
                GridView gridView = (GridView)rptColorAndSizes.Items[i].FindControl("gvSizeAndQty");

                TextBox tbxNotes = (TextBox)rptColorAndSizes.Items[i].FindControl("tbxNotes");


                for (int k = 0; k < gridView.Columns.Count; k++)
                {

                    string sizeIdLabel1Id = "lblSizeId1" + k.ToString();
                    string sizeQuantityTextBox1Id = "txtSizeQty1" + k.ToString();

                    Label tbxSizeId = (Label)gridView.Rows[0].Cells[k].FindControl(sizeIdLabel1Id);
                    TextBox sizeQty = (TextBox)gridView.Rows[0].Cells[k].FindControl(sizeQuantityTextBox1Id);

                    if (sizeQty.Text != "")
                    {
                        sizeQuantity = Convert.ToInt32(sizeQty.Text);
                    }
                    else
                    {
                        sizeQuantity = 0;
                    }

                    //colorSizeQty += sizeQuantity;

                    if (sizeQuantity > 0)
                    {
                        dtRejectionColorAndSizes.Rows.Add(productRejectionId, Convert.ToInt32(lblBuyerColorId.Text), Convert.ToInt32(tbxSizeId.Text), sizeQuantity, tbxNotes.Text, CommonMethods.SessionInfo.UserName, DateTime.Now);
                    }
                }

            }


            rejectionSaveResult = productionManager.SaveProductRejections(dtRejectionColorAndSizes);

            if (rejectionSaveResult > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Product rejection could not be saved.')", true);
            }

            return rejectionSaveResult;


        }

        protected void btnUpdateRejection_Click(object sender, EventArgs e)
        {

            try
            {
                    var pr = unitOfWork.GenericRepositories<ProductRejections>().GetByID(ProductRejectionId);
                    List<ProductRejectionColorSizes> lstColorSizes = new List<ProductRejectionColorSizes>();


                    pr.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                    pr.StyleId = int.Parse(ddlStyles.SelectedValue);
                    pr.OrderId = int.Parse(ddlOrders.SelectedValue);
                    pr.FinishingFloorId = int.Parse(ddlFinishingFloor.SelectedValue);

                    pr.UpdateDate = DateTime.Now;
                    pr.UpdatedBy = CommonMethods.SessionInfo.UserName;


                    for (int i = 0; i < rptColorAndSizes.Items.Count; i++)
                    {
                        var lblBuyerColorId = (Label)rptColorAndSizes.Items[i].FindControl("lblColorId");
                        var gridView = (GridView)rptColorAndSizes.Items[i].FindControl("gvSizeAndQty");
                        var tbxNotes = (TextBox)rptColorAndSizes.Items[i].FindControl("tbxNotes");


                    for (int j = 0; j < gridView.Columns.Count; j++)
                        {
                            
                        string sizeIdLabel1Id = "lblSizeId1" + j.ToString();
                        string sizeQuantityTextBox1Id = "txtSizeQty1" + j.ToString();
                        var sizeId = (Label)gridView.Rows[0].Cells[j].FindControl(sizeIdLabel1Id);
                        var rejQty = (TextBox)gridView.Rows[0].Cells[j].FindControl(sizeQuantityTextBox1Id);


                        if (!string.IsNullOrEmpty(rejQty.Text))
                            {
                            var colorSize = new ProductRejectionColorSizes()
                            {
                                ProductRejectionsId = ProductRejectionId,
                                BuyerColorId = int.Parse(lblBuyerColorId.Text),
                                SizeId = int.Parse(sizeId.Text),
                                SizeQuantity = int.Parse(rejQty.Text),
                                Notes = tbxNotes.Text,
                                UpdateDate = DateTime.Now,
                                UpdatedBy = CommonMethods.SessionInfo.UserName
                                };
                                lstColorSizes.Add(colorSize);
                            }
                        }
                    }

                    if (lstColorSizes.Count > 0)
                    {
                  
                        unitOfWork.GenericRepositories<ProductRejections>().Update(pr);
                        var previousColors = unitOfWork.GenericRepositories<ProductRejectionColorSizes>().Get(x => x.ProductRejectionsId == ProductRejectionId).ToList();
                        foreach (var item in previousColors)
                        {
                            unitOfWork.GenericRepositories<ProductRejectionColorSizes>().Delete(item);
                        }
                        foreach (var item in lstColorSizes)
                        {
                            unitOfWork.GenericRepositories<ProductRejectionColorSizes>().Insert(item);
                        }
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewEditRejections.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter size quantity!')", true);
                    }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }



        }



    }
}