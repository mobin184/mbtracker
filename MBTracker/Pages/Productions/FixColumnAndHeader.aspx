﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FixColumnAndHeader.aspx.cs" Inherits="MBTracker.Pages.Productions.FixColumnAndHeader" %>


<%@ Register Src="~/User_Controls/Header.ascx" TagPrefix="uc1" TagName="Header" %>
<%@ Register Src="~/User_Controls/MainMenu.ascx" TagPrefix="uc1" TagName="MainMenu" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>MB Tracker</title>
    <link href="../../css/web.css" rel="stylesheet" />
    <script type="text/javascript" src="../../js/gridviewscroll.js"></script>
    <script type="text/javascript">
        var gridViewScroll = null;
        window.onload = function () {
            gridViewScroll = new GridViewScroll({
                elementID: "entryTable",
                width: 1425,
                height: 500,
                freezeColumn: true,
                freezeFooter: false,
                freezeColumnCssClass: "GridViewScrollItemFreeze",
                freezeFooterCssClass: "GridViewScrollFooterFreeze",
                freezeHeaderRowCount: 1,
                freezeColumnCount: 1,
                onscroll: function (scrollTop, scrollLeft) {
                    console.log(scrollTop + " - " + scrollLeft);
                }
            });
            gridViewScroll.enhance();
        }
        function getScrollPosition() {
            var position = gridViewScroll.scrollPosition;
            alert("scrollTop: " + position.scrollTop + ", scrollLeft: " + position.scrollLeft);
        }
        function setScrollPosition() {
            var scrollPosition = { scrollTop: 50, scrollLeft: 50 };

            gridViewScroll.scrollPosition = scrollPosition;
        }
    </script>

    <script src="../../js/html5-trunk.js"></script>
    <link href="~/css/bootstrap.min.css" rel="stylesheet" />
    <link type="text/css" href="~/icomoon/style.css" rel="stylesheet" />
    <%--<link type="text/css" href="/css/nvd-charts.css" rel="stylesheet" />--%>
    <link type="text/css" href="~/css/main.css" rel="stylesheet" />
    <%--   <link type="text/css" href='/css/fullcalendar/fullcalendar.css' rel='stylesheet' />
    <link type="text/css" href='/css/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />--%>

    <link type="text/css" href="~/css/custom.css" rel="stylesheet" />
    <link type="text/css" href="~/content/toastr.css" rel="stylesheet" />
    <script src="../../js/jquery.min.js"></script>
    <script src="../../js/bootstrap.js"></script>
    <script src="../../Scripts/toastr.js"></script>
    <%--  <link href="../../FancyBox/jquery.fancybox.css" rel="stylesheet" />--%>
    <link href="../../css/font-awesome.min.css" rel="stylesheet" />

    <%--<link href="../../css/all.css" rel="stylesheet" />--%>
    <link href="../../css/sweetalert.css" rel="stylesheet" />
    <script src="../../Scripts/sweetalert.min.js"></script>
    <link href="../../css/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.min.js"></script>
    <script src="../../js/jquery.blockUI.js"></script>
    <script src="../../Scripts/jquery.signalR-2.2.2.js"></script>
    <script src="/SignalR/hubs"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#main-nav ul li ul li").hover(function () {
                $(this).children("ul").toggleClass('test-SubMenu-open', 1, "easeOutSine");
            }, function () {
                $(this).children("ul").toggleClass('test-SubMenu-open', 1, "easeOutSine");
            });
        });





    </script>

</head>

<body style="margin: 0px; width: 100%; height: 100%;">
    <form id="form1" runat="server">


        <header id="header">
            <uc1:Header runat="server" ID="Header" />
        </header>

        <div class="container" id="menu" style="margin: 0px;">
            <div id="main-nav" class="hidden-phone hidden-tablet">
                <uc1:MainMenu runat="server" ID="MainMenu" />
                <div class="clearfix"></div>
            </div>
        </div>




        <div class="container-fluid">
            <div class="dashboard-wrapper">
                <div class="main-container" id="body">
                    <div class="page-header">
                        <div class="clearfix"></div>
                    </div>
                    <div>

                        <div class="row-fluid">
                            <div class="span6">
                                <div class="widget">
                                    <div class="widget-header">
                                        <div class="title">
                                            <span class="fs1" aria-hidden="true" data-icon=""></span>
                                            <asp:Label runat="server" ID="lblTitle" Text="Deliver Knitted Parts:"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="widget-body">
                                        <div class="row" style="padding-left: 5px; padding-right: 5px;">
                                            <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                                                <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="control-group">
                                                <div class="col-md-12 col-sm-12">
                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-horizontal">
                                                    <div class="control-group">
                                                        <label for="inputStyle" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                                            <asp:Label ID="Label2" runat="server" Text="Delivery Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                                        <div class="controls controls-row" style="margin-left: 140px">
                                                            <asp:TextBox ID="tbxDelivryDate" AutoPostBack="true" TextMode="Date" Width="100%" Text="" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                                ControlToValidate="tbxDelivryDate"><span style="font-weight: 700; color: #CC0000">Please select receive date.</span></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                                            <asp:Label ID="Label1" runat="server" Text="Knitting Unit:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                                        <div class="controls controls-row" style="margin-left: 140px">
                                                            <asp:DropDownList ID="ddlUserProductionUnit" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                                                ControlToValidate="ddlUserProductionUnit"><span style="font-weight: 700; color: #CC0000">Please select a knitting unit.</span></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                                            <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                                        <div class="controls controls-row" style="margin-left: 140px">
                                                            <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                                ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label for="inputStyle" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                                            <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                                        <div class="controls controls-row" style="margin-left: 140px">
                                                            <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                                                ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select a style.</span></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-horizontal">
                                                    <div class="control-group">
                                                        <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                                            <asp:Label ID="Label3" runat="server" Text="Finishing Unit:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                                        <div class="controls controls-row" style="margin-left: 140px">
                                                            <asp:DropDownList ID="ddlFinishingUnit" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                                                ControlToValidate="ddlFinishingUnit"><span style="font-weight: 700; color: #CC0000">Please select a finishing unit.</span></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                                            <asp:Label ID="Label4" runat="server" Text="Chalan No:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                                        <div class="controls controls-row" style="margin-left: 140px">
                                                            <asp:TextBox ID="tbxChalanNumber" runat="server" placeholder="Enter chalan number" CssClass="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                                                ControlToValidate="tbxChalanNumber"><span style="font-weight: 700; color: #CC0000">Please enter chalan number.</span></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                                            <asp:Label ID="Label6" runat="server" Text="Gate Pass No:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                                        <div class="controls controls-row" style="margin-left: 140px">
                                                            <asp:TextBox ID="tbxGatePassNo" runat="server" placeholder="Enter gate pass number" CssClass="form-control"></asp:TextBox>
                                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxGatePassNo"><span style="font-weight: 700; color: #CC0000">Enter gate pass no.</span></asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                                            <asp:Label ID="Label5" runat="server" Text="Vehicle No:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                                        <div class="controls controls-row" style="margin-left: 140px">
                                                            <asp:TextBox ID="tbxVehicleNo" runat="server" placeholder="Enter vehicle number" CssClass="form-control"></asp:TextBox>
                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxVehicleNo"><span style="font-weight: 700; color: #CC0000">Enter vehicle no.</span></asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                         
    
    <div class="row-fluid">
        <asp:Label ID="lblNoDataFound" runat="server" Text="There is no color for this style."  Visible="false" BackColor="#ffff00"></asp:Label>
    </div>
    <div class="row-fluid" runat="server" id="divColors" visible="false">
        <div class="span12">
            <div class="widget">
                <div class="widget-body">
                    <div class="row-fluid">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblEntry" runat="server" Text="" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                    <asp:Repeater ID="rptEntryInfo" OnItemDataBound="rptEntryInfo_ItemDataBound" runat="server">
                                        <HeaderTemplate>
                                            <table id="entryTable" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr class="GridViewScrollHeader">
                                                        <%--  <th>
                                                            <asp:Label ID="lblBuyer" runat="server" Text="Buyer<br/> Names"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblStyle" runat="server" Text="Styles<br/> Names"></asp:Label></th>--%>
                                                        <th style="text-align:center">
                                                            <asp:Label ID="lblColor" runat="server" Text="Color Names"></asp:Label></th>
                                                        <th style="text-align:center">
                                                            <asp:Label ID="Label5" runat="server" Text="Size & Quantity"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr class="GridViewScrollItem">
                                                <td style="text-align:left"><%#Eval("ColorDescription") %>
                                                    <asp:Label ID="lblBuyerColorId" runat="server" Text='<%#Eval("BuyerColorId") %>' Visible="false"></asp:Label></td>
                                                <asp:Label ID="lblStyleId" runat="server" Text='<%#Eval("StyleId") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblBuyerId" runat="server" Text='<%#Eval("BuyerId") %>' Visible="false"></asp:Label>
                                                <td>
                                                    <asp:GridView ID="gvSizeQuantity1" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity1_RowCreated">
                                                    </asp:GridView>
                                                    <asp:Label ID="lblNoSizeFound" runat="server" Text="No size found." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                </table>
                            <div class="clearfix"></div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <asp:Button ID="btnSaveEntries" runat="server" ValidationGroup="save" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSaveEntries_Click"></asp:Button>
            <asp:Button ID="btnUpdateEntries" Visible="false" ValidationGroup="save" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdateEntries_Click"></asp:Button>
        </div>
    </div>


                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-12" id="footer">
            <div class="row" style="text-align: center; color: #f79825">
                <b>Copyright © 2020 <a style="text-decoration: none; color: #f79825;" href="http://www.masihata.com/">Masihata Group</a> </b>
            </div>
        </div>

    </form>
</body>
</html>
