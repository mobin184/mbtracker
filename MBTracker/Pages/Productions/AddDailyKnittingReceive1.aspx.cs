﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class AddDailyKnittingReceive : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        ProductionManager productionManager = new ProductionManager();
        DataTable dtUserUnits;
        DataTable dtSizes;
        DataTable dtYarnIssuedForSizes;
        int buyerId;
        int styleId;
        int unitId;
        string colorCode;
        int entryRowNumber2 = 0;
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //if (Request.QueryString["unitId"] != null && Request.QueryString["deliveryDate"] != null && Request.QueryString["shiftId"] != null && Request.QueryString["receiveDate"] != null && Request.QueryString["batchId"] != null)
                //{
                //    UnitId = int.Parse(Request.QueryString["unitId"]);
                //    DeliveryDate = DateTime.Parse(Request.QueryString["deliveryDate"]);
                //    ShiftId = int.Parse(Request.QueryString["shiftId"]);
                //    BatchId = int.Parse(Request.QueryString["batchId"]);
                //    ReceiveDate = DateTime.Parse(Request.QueryString["receiveDate"]);
                //    EditMode = true;
                //    PopulateGeneralInfo();
                //    pnlDetailsTitle.Visible = true;
                //    pnlSearch.Visible = false;
                //    pnlSummary.Visible = true;
                //    pnlDetails.Visible = true;
                //}
                //else
                //{
                tbxFilterDate.Text = DateTime.Today.ToString("yyyy-MM-dd");
                dtUserUnits = new CommonManager().GetProductionUnitsByUser(Convert.ToInt32(CommonMethods.SessionInfo.UserId));
                ddlUserUnit.DataSource = dtUserUnits;
                ddlUserUnit.DataTextField = "UnitName";
                ddlUserUnit.DataValueField = "ProductionUnitId";
                ddlUserUnit.DataBind();
                LoadData();
                pnlSummary.Visible = false;
                pnlDetails.Visible = false;
                //}
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
            pnlSummary.Visible = false;
            pnlDetails.Visible = false;
        }


        protected void LoadData()
        {
            var date = string.IsNullOrEmpty(tbxFilterDate.Text) ? "" : Convert.ToDateTime(tbxFilterDate.Text).ToString();
            var unitId = int.Parse(ddlUserUnit.SelectedValue);
            string sql = $"Exec usp_GetKnittingDeliveryEntryByDateAndUnit '{date}',{unitId}";
            var dt = unitOfWork.GetDataTableFromSql(sql);

            if (dt.Rows.Count > 0)
            {
                rpt.Visible = true;
                rpt.DataSource = dt;
                rpt.DataBind();
                lblNoDataFound.Visible = false;
            }
            else
            {
                rpt.Visible = false;
                lblNoDataFound.Visible = true;
            }
        }

        protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        {
            pnlSummary.Visible = true;
            pnlDetails.Visible = true;
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            UnitId = int.Parse(commandArgs[0]);
            DeliveryDate = DateTime.Parse(commandArgs[1]);
            ShiftId = int.Parse(commandArgs[2]);
            BatchId = int.Parse(commandArgs[3]);
            KnittingReceived = int.Parse(commandArgs[4]);
            ReceiveDate = string.IsNullOrEmpty(commandArgs[5].ToString()) ? (DateTime?)null : DateTime.Parse(commandArgs[5]);
            PopulateGeneralInfo();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "message", "scrollDown()", true);
        }


        int KnittingReceived
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["knittingReceived"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["knittingReceived"] = value; }
        }
        int UnitId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["unitId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["unitId"] = value; }
        }
        DateTime DeliveryDate
        {
            get
            {
                try
                {
                    return Convert.ToDateTime(ViewState["deliveryDate"]);
                }
                catch
                {
                    return DateTime.Today;
                }
            }
            set { ViewState["deliveryDate"] = value; }
        }
        int ShiftId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["shiftId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["shiftId"] = value; }
        }
        int BatchId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["batchId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["batchId"] = value; }
        }
        DateTime? ReceiveDate
        {
            get
            {
                try
                {
                    return string.IsNullOrEmpty(ViewState["receiveDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(ViewState["receiveDate"]);
                }
                catch
                {
                    return null;
                }
            }
            set { ViewState["receiveDate"] = value; }
        }

        //bool EditMode
        //{
        //    get
        //    {
        //        try
        //        {
        //            return Convert.ToBoolean(ViewState["editMode"]);
        //        }
        //        catch
        //        {
        //            return false;
        //        }
        //    }
        //    set { ViewState["editMode"] = value; }
        //}
        public DataTable ProdEntryGeneralInfo()
        {
            DataTable dt = new DataTable("ProdEntryGeneralInfo");
            dt.Columns.Add("Unit", typeof(string));
            dt.Columns.Add("Shift", typeof(string));
            dt.Columns.Add("DeliveryDate", typeof(string));
            dt.Columns.Add("UserName", typeof(string));
            return dt;
        }

        private void PopulateGeneralInfo()
        {
            dtUserUnits = new CommonManager().GetProductionUnitsByUser(Convert.ToInt32(CommonMethods.SessionInfo.UserId));
            var UnitInfo = unitOfWork.GenericRepositories<ProductionUnits>().GetByID(UnitId);
            if (dtUserUnits.Rows.Count > 0)
            {
                DataTable dtProdEntryGeneralInfo = ProdEntryGeneralInfo();
                if (UnitId == 0)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "toastr_message", "toastr.error('Invalid Unit Id.')", true);
                }
                else
                {
                    dtProdEntryGeneralInfo.Rows.Add(UnitInfo.UnitName, ShiftId == 1 ? "Day" : ShiftId == 2 ? "Night" : ShiftId == 3 ? "Both Shifts" : "", String.Format("{0:dd/MM/yyyy}", DeliveryDate), CommonMethods.SessionInfo.UserFullName.ToString());
                    PopulateEntryInfo(UnitId);
                }

                rptGeneralInfo.DataSource = dtProdEntryGeneralInfo;
                rptGeneralInfo.DataBind();
            }
        }

        private void PopulateEntryInfo(int unitId)
        {
            //DataTable dt = productionManager.GetProductionEntryInfo(unitId);
            var dt = unitOfWork.GetDataTableFromSql($"Exec usp_GetDeliveryInfoByBatchId {BatchId}");
            if (dt.Rows.Count > 0)
            {
                lblNoKnittingGoingOn.Visible = false;
                rptEntryInfo.Visible = true;
                lblDetails.Visible = true;
                lblSummary.Visible = true;
                //if (!EditMode)
                //{
                if (KnittingReceived == 0)
                {
                    btnReceive.Visible = true;
                }
                else
                {
                    btnReceive.Visible = false;
                }
                //}
                //else
                //{
                //    btnReceive.Visible = false;
                //    btnUpdate.Visible = true;
                //}


            }
            else
            {
                lblNoKnittingGoingOn.Visible = true;
                rptEntryInfo.Visible = false;
                lblDetails.Visible = false;
                lblSummary.Visible = false;
                btnReceive.Visible = false;

            }
            rptEntryInfo.DataSource = dt;
            rptEntryInfo.DataBind();
        }

        protected void rptGeneralInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var tbxReceiveDate = (TextBox)e.Item.FindControl("tbxReceiveDate");
                if (ReceiveDate != null)
                {
                    tbxReceiveDate.Text = ReceiveDate?.ToString("yyyy-MM-dd");
                    tbxReceiveDate.Enabled = false;
                }
                else
                {
                    tbxReceiveDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                }
            }
        }

        protected void ddlUserUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlUserUnits = (DropDownList)sender;
            PopulateEntryInfo(Convert.ToInt32(ddlUserUnits.SelectedValue));
        }

        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gvSizeQuantity2 = (GridView)e.Item.FindControl("gvSizeQuantity2");
                GridView gvSizeQuantity3 = (GridView)e.Item.FindControl("gvSizeQuantity3");

                buyerId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BuyerId").ToString());
                styleId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "StyleId").ToString());
                unitId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "UnitId").ToString());
                colorCode = DataBinder.Eval(e.Item.DataItem, "ColorCode").ToString().Trim();

                dtSizes = buyerManager.GetSizes(buyerId);
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantity2.Columns.Add(templateField);
                        gvSizeQuantity3.Columns.Add(templateField);

                    }
                }

                gvSizeQuantity2.DataSource = dtOneRow;
                gvSizeQuantity2.DataBind();
                gvSizeQuantity3.DataSource = dtOneRow;
                gvSizeQuantity3.DataBind();
            }
        }

        protected void gvSizeQuantity_RowCreated2(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblBuyerId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId");
                //var gvSizeQuantity3 = (GridView)((GridView)sender).DataItemContainer.FindControl("gvSizeQuantity3"); 
                buyerId = int.Parse(lblBuyerId.Text);

                entryRowNumber2++;

                Label txtSizeQty = null;
                Label lblSizeId = null;
                LoadSizeInfo();

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new Label();
                    txtSizeQty.ID = "txtSizeQty2" + i.ToString();
                    txtSizeQty.Text = "";
                    txtSizeQty.Width = 70;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);

                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId2" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);

                    var enitryId = new Label();
                    enitryId.ID = "lblenitryId2" + i.ToString();
                    enitryId.Text = "";
                    enitryId.Visible = false;
                    e.Row.Cells[i].Controls.Add(enitryId);

                    if (UnitId != 0)
                    {
                        unitOfWork = new UnitOfWork();
                        var lblStyleId = (Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId");
                        var lblColorCode = (Label)((GridView)sender).DataItemContainer.FindControl("lblColorCode");
                        var lblUnitId = (Label)((GridView)sender).DataItemContainer.FindControl("lblUnitId");
                        var styleId = int.Parse(lblStyleId.Text);
                        var colorCode = lblColorCode.Text.Trim();
                        var unitId = int.Parse(lblUnitId.Text);
                        var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());

                        var pInfo = unitOfWork.GenericRepositories<DailyKnittingDeliveries>().Get(x => x.StyleId == styleId && x.ColorCode == colorCode && x.SizeId == sizeId && x.BatchId == BatchId).FirstOrDefault();
                        if (pInfo != null)
                        {
                            txtSizeQty.Text = pInfo.DeliveryQty + "";
                            enitryId.Text = pInfo.Id + "";
                        }
                    }
                }
            }
        }

        protected void gvSizeQuantity_RowCreated3(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblBuyerId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId");
                buyerId = int.Parse(lblBuyerId.Text);
                entryRowNumber2++;

                TextBox txtSizeQty = null;
                Label lblSizeId = null;
                LoadSizeInfo();

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new TextBox();
                    txtSizeQty.ID = "txtSizeQty3" + i.ToString();
                    txtSizeQty.Text = "";
                    txtSizeQty.Width = 70;
                    txtSizeQty.Enabled = false;
                    txtSizeQty.TextMode = TextBoxMode.Number;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);

                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId3" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);

                    var enitryId = new Label();
                    enitryId.ID = "lblenitryId3" + i.ToString();
                    enitryId.Text = "";
                    enitryId.Visible = false;
                    e.Row.Cells[i].Controls.Add(enitryId);

                    if (UnitId != 0)
                    {
                        unitOfWork = new UnitOfWork();
                        var lblStyleId = (Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId");
                        var lblColorCode = (Label)((GridView)sender).DataItemContainer.FindControl("lblColorCode");
                        var lblUnitId = (Label)((GridView)sender).DataItemContainer.FindControl("lblUnitId");
                        var styleId = int.Parse(lblStyleId.Text);
                        var colorCode = lblColorCode.Text.Trim();
                        var unitId = int.Parse(lblUnitId.Text);
                        var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());
                        if (KnittingReceived == 0)
                        {
                            var pInfo = unitOfWork.GenericRepositories<DailyKnittingDeliveries>().Get(x => x.StyleId == styleId && x.ColorCode == colorCode && x.SizeId == sizeId && x.BatchId == BatchId).FirstOrDefault();
                            if (pInfo != null)
                            {
                                txtSizeQty.Text = pInfo.DeliveryQty + "";
                                enitryId.Text = pInfo.Id + "";
                                txtSizeQty.Enabled = true;
                            }
                        }
                        else
                        {
                            var pInfo = unitOfWork.GenericRepositories<DailyKnittingReceive>().Get(x => x.StyleId == styleId && x.ColorCode == colorCode && x.SizeId == sizeId && x.BatchId == BatchId).FirstOrDefault();
                            if (pInfo != null)
                            {
                                txtSizeQty.Text = pInfo.ReceivedQty + "";
                                enitryId.Text = pInfo.Id + "";
                            }
                        }

                    }
                }
            }
        }


        protected void LoadSizeInfo()
        {
            dtSizes = buyerManager.GetSizes(buyerId);
        }

        protected void LoadYarnIssueForSizes()
        {
            dtYarnIssuedForSizes = productionManager.GetYarnIssuedForSizesByStyle(styleId, unitId, colorCode);
        }

        protected void btnReceive_Click(object sender, EventArgs e)
        {
            try
            {
                //var unitId = int.Parse(((Label)rptEntryInfo.Items[0].FindControl("lblUnitId")).Text);
                //var stShift = ((DropDownList)rptGeneralInfo.Items[0].FindControl("ddlShift")).SelectedValue;
                var stReceiveDate = ((TextBox)rptGeneralInfo.Items[0].FindControl("tbxReceiveDate")).Text;

                if (!string.IsNullOrEmpty(stReceiveDate))
                {
                    var receiveDate = Convert.ToDateTime(stReceiveDate);
                    if (receiveDate < DateTime.Now)
                    {
                        int totalReceivedQty = 0;
                        for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                        {
                            Label lblOrderId = (Label)rptEntryInfo.Items[i].FindControl("lblStyleId");
                            Label lblColorCode = (Label)rptEntryInfo.Items[i].FindControl("lblColorCode");
                            Label lblUnitId = (Label)rptEntryInfo.Items[i].FindControl("lblUnitId");
                            GridView gvSizeQuantity3 = (GridView)rptEntryInfo.Items[i].FindControl("gvSizeQuantity3");
                            for (int j = 0; j < gvSizeQuantity3.Columns.Count; j++)
                            {
                                int receivedQty = 0;
                                string sizeIdLabel2Id = "lblSizeId3" + j.ToString();
                                string sizeQuantityTextBox2Id = "txtSizeQty3" + j.ToString();
                                Label sizeId = (Label)gvSizeQuantity3.Rows[0].Cells[j].FindControl(sizeIdLabel2Id);
                                TextBox tbxSizeQty = (TextBox)gvSizeQuantity3.Rows[0].Cells[j].FindControl(sizeQuantityTextBox2Id);

                                if (!(tbxSizeQty.Text == ""))
                                {
                                    receivedQty = int.Parse(tbxSizeQty.Text);
                                    totalReceivedQty += receivedQty;
                                    var dialyKnittingReceived = new DailyKnittingReceive()
                                    {
                                        BatchId = BatchId,
                                        StyleId = int.Parse(lblOrderId.Text),
                                        ColorCode = lblColorCode.Text.Trim(),
                                        SizeId = int.Parse(sizeId.Text),
                                        ReceivedQty = receivedQty,
                                        CreatedBy = CommonMethods.SessionInfo.UserName,
                                        CreateDate = DateTime.Now
                                    };
                                    unitOfWork.GenericRepositories<DailyKnittingReceive>().Insert(dialyKnittingReceived);
                                }
                            }
                        }
                        var batchInfo = unitOfWork.GenericRepositories<KnittingDeliveryBatches>().GetByID(BatchId);
                        batchInfo.KnittingReceiveDate = receiveDate;
                        batchInfo.KnittingReceiveQty = totalReceivedQty;
                        unitOfWork.GenericRepositories<KnittingDeliveryBatches>().Update(batchInfo);
                        unitOfWork.Save();
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "toastr_message", "toastr.success('Delivery receive entries were saved successfully.')", true);
                        //Response.AppendHeader("Refresh", "2");

                        ClearData(); 
                    }
                    else
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "toastr_message", "toastr.error('You cant receive at future date!')", true);
                    }
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "toastr_message", "toastr.error('Please select receive date!')", true);
                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "')", true);
            }
        }

        private void ClearData()
        {
            LoadData();
            pnlSummary.Visible = false;
            pnlDetails.Visible = false;
            rptEntryInfo.DataSource = null;
            rptEntryInfo.DataBind();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

        }
    }
}