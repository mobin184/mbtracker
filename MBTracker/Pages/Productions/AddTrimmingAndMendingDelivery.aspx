﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddTrimmingAndMendingDelivery.aspx.cs" Inherits="MBTracker.Pages.Productions.AddTrimmingAndMendingDelivery" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        input[type="radio"], input[type=checkbox] {
            margin: -2px 0 0;
        }

        .cblClass label {
           
            padding-left: 2px !important;
            padding-right: 14px !important;
        }
    </style>

    <asp:Panel ID="pnlNonEdit1" runat="server">

        <div class="row-fluid">

            <div class="span9">
                <div class="widget">
                    <div class="widget-header">
                        <div class="title">
                            <span class="fs1" aria-hidden="true" data-icon=""></span>
                            <asp:Label runat="server" ID="actionTitle" Text="Enter Trimming & Mending Delivery Info:"></asp:Label>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="form-horizontal">
                            <div class="pull-right" style="text-align: center; font-size: 12px; line-height: 20px; padding-bottom: 5px">
                                <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                            </div>
                <div class="control-group">
                    <div class="col-md-12 col-sm-12">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                    </div>
                            
                   <div class="col-md-6">
                      
                        <div class="control-group">
                                <label for="inputStyle" class="control-label" style="float: left;width:140px; padding-top:5px;text-align:right" >
                                    <asp:Label ID="Label2" runat="server" Text="T&M Delivery Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                <div class="controls controls-row" style="margin-left:140px">
                                    <asp:TextBox ID="tbxTrimingMendingDeliveryDate" AutoPostBack="true" TextMode="Date" Width="100%" Text="" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="tbxTrimingMendingDeliveryDate"><span style="font-weight: 700; color: #CC0000">Please select Delivery date.</span></asp:RequiredFieldValidator>
                                </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label" style="float: left;width:140px; padding-top:5px;text-align:right">
                                <asp:Label ID="Label3" runat="server" Text="Finishing Unit:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                            <div class="controls controls-row" style="margin-left:140px">
                                <asp:DropDownList ID="ddlFinishingUnit" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlFinishingUnit"><span style="font-weight: 700; color: #CC0000">Please select a finishing unit.</span></asp:RequiredFieldValidator>
                            </div>
                         </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label" style="float: left;width:140px; padding-top:5px;text-align:right">
                                <asp:Label ID="LinkingFloor" runat="server" Text="Linking Floor:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                            <div class="controls controls-row" style="margin-left:140px">
                                <asp:DropDownList ID="ddlLinkingFloor" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlLinkingFloor"><span style="font-weight: 700; color: #CC0000">Please select a Linking Floor.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-horizontal">
                                
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left;width:140px; padding-top:5px;text-align:right">
                                        <asp:Label ID="Label5" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left:140px">
                                        <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label" style="float: left;width:140px; padding-top:5px;text-align:right">
                                        <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left:140px">
                                        <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select a style.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left;width:140px; padding-top:5px;text-align:right">
                                        <asp:Label ID="Label7" runat="server" Text="Remarks:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left:140px">
                                        <asp:TextBox ID="tbxDescription" TextMode="multiline" Rows="3" runat="server" placeholder="Enter description" CssClass="form-control"></asp:TextBox>
                                        
                                    </div>
                                </div>

                            </div>

                    </div>

               </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <br />
        <br />

        <div class="row-fluid">
            <div id="divPIOtherInfo" runat="server" visible="false">

                <div class="span9">

                    <div class="widget">

                        <div class="widget-body">
                            <div class="form-horizontal">

                                
                                <div class="control-group">
                                    <div class="controls controls-row" style="text-align: right">
                                        <span style="font-weight: 700; color: #CC0000">*</span>Row will not be saved if all the fields don't have values.
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputOrder" class="control-label">
                                        <asp:Label ID="lblPIEntry" runat="server" Text="Color Wise Qnty:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls" style="overflow-x: auto; float:left; margin-left:0px;">
                                        <asp:Repeater ID="rptTrimmingAndMendingDeliveryEntryInfo" runat="server" OnItemDataBound="rptTrimmingAndMendingDeliveryEntryInfo_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table22" class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th><asp:Label ID="lblSL" runat="server" Text="SN"></asp:Label></th>
                                                            <th><asp:Label ID="lblColors" runat="server" Text="Colors<br/>"></asp:Label></th>
                                                           
                                                            <th>
                                                                <asp:Label ID="lblQty" runat="server" Text="Quantity"></asp:Label></th>
                                                           
                                                            <%--<th>
                                                                <asp:Label ID="lblUnitPrice" runat="server" Text="Used Mc"></asp:Label></th>--%>
                                                           
                                                             <th>
                                                                <asp:Label ID="Action" runat="server" Text="Action"></asp:Label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align:center;vertical-align:middle; font-weight:bold"><%# (Container.ItemIndex+1) %></td>
                                                    <td style="width: 30%; padding-top: 18px">
                                                        <asp:DropDownList ID="ddlColors" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </td>
                                                    <td style="width: 25%; padding-top: 18px">
                                                        <asp:TextBox ID="tbxQuantity" Width="100%" runat="server" TextMode="Number" AutoPostBack="true" OnTextChanged="CalculatePrices"></asp:TextBox>
                                                    </td>
                                                    <%--<td style="width: 20%; padding-top: 18px">
                                                        <asp:TextBox ID="tbxUsedNumberOfMC" Width="100%" runat="server" TextMode="Number" AutoPostBack="true"></asp:TextBox>
                                                    </td>--%>
                                                    <td style="min-width:100px; padding-top: 18px">
                                                       
                                                        <asp:LinkButton ID="btnRemove" runat="server" Display="Dynamic" style="font-size:13px;padding: 4px 5px; line-height: 13px; " CssClass="btn btn-sm btn-danger" CommandArgument="<%# (Container.ItemIndex) %>"  AutoPostBack="true" OnCommand="btnRemove_Command" Text="Del."  />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <td style="font-weight: 500; text-align: center; background-color: lavender"></td>
                                                <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                    <asp:Label ID="lblTotal" runat="server" Text="Total:" Font-Bold="true"></asp:Label></td>
                                                <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                    <asp:Label ID="lblTotalQuantity" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                                                <%--<td style="font-weight: 500; text-align: left; background-color: lavender"></td>--%>
                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                </tbody>
                                 </table>
                                            <div class="pull-right" style="margin-right: 0px">
                                                <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="81px" Text="Add Row" OnClick="btnAddRow_Click" />
                                            </div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>

                            </div>
                          
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </asp:Panel>


    <div class="row-fluid">

                <br />
                <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
             
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
