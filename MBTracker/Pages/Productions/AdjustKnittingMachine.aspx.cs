﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using Repositories;
using MBTracker.EF;
 
namespace MBTracker.Pages.Productions
{
    public partial class AdjustKnittingMachine : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        ProductionManager productionManager = new ProductionManager();

        DataTable dtMachineBrands;
        DataTable dt = new DataTable();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }

        }

        int MachineBrandId
        {
            set { ViewState["machineBrandId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["machineBrandId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        string MachineBrandName
        {
            set { ViewState["machineBrandName"] = value; }
            get
            {
                try
                {
                    return ViewState["machineBrandName"].ToString();
                }
                catch
                {
                    return "";
                }
            }
        }

        int KnittingTime
        {
            set { ViewState["knittingTime"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["knittingTime"]);
                }
                catch
                {
                    return 0;
                }
            }
        }



        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlStyles.Items.Clear();
            //ddlOrders.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            else
            {
                pnlPlanningInfo.Visible = false;
                //pnlShipmentInfo.Visible = false;
                //pnlAssignMachinesToOrder.Visible = false;
                //pnlMonthYearSelection.Visible = false;
                //pnlMachinesAvailable.Visible = false;

            }

        }

        private void BindStylesByBuyer(int buyerId)
        {
            pnlPlanningInfo.Visible = false;
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ddlOrders.Items.Clear();
            if (ddlStyles.SelectedValue != "")
            {
                //BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
                LoadPlanInfo();
            }
            else
            {
                pnlPlanningInfo.Visible = false;
                //pnlShipmentInfo.Visible = false;
                //pnlAssignMachinesToOrder.Visible = false;
                //pnlMonthYearSelection.Visible = false;
                //pnlMachinesAvailable.Visible = false;
            }


        }

        //private void BindOrdersByStyle(int styleId)
        //{

        //    CommonMethods.LoadOrderDropdownByStyle(ddlOrders, styleId, 1, 0);

        //}

        //protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        //{


        //    if (ddlOrders.SelectedValue != "")
        //    {

        //        //CommonMethods.LoadProductionMonthDropDown(ddlMonthYears, 1, 0);
        //        LoadPlanInfo();

        //    }
        //    else
        //    {
        //        pnlPlanningInfo.Visible = false;
        //        //pnlShipmentInfo.Visible = false;
        //        //pnlAssignMachinesToOrder.Visible = false;
        //        //pnlMonthYearSelection.Visible = false;
        //        //pnlMachinesAvailable.Visible = false;

        //    }
        //}

        private void LoadPlanInfo()
        {            

            var styleId = Convert.ToInt32(ddlStyles.SelectedValue);

            dt = productionManager.GetProductionPlanningInfo(styleId);
            if (dt.Rows.Count < 1)
            {
                dt = orderManager.GetProductionPlanningInfo(styleId);
            }

            if (dt.Rows.Count > 0)
            {
                //KnittingTime = Convert.ToInt32(dt.Rows[0][8].ToString());
                //MachineBrandId = Convert.ToInt32(dt.Rows[0][3].ToString());
                var dtMachineBrands = orderManager.GetMachineBrands();


                rptPlanInfo.DataSource = dtMachineBrands;
                rptPlanInfo.DataBind();
                lblNoPlanningInfo.Visible = false;
                rptPlanInfo.Visible = true;
                pnlPlanningInfo.Visible = true;

                var totalOrderQty = unitOfWork.GetSingleValue($"Exec usp_GetActiveOrderQuantityByStyleId {styleId}");
                lblOrderQuantity.Text = totalOrderQty;
            }
            else
            {
                rptPlanInfo.DataSource = null;
                rptPlanInfo.DataBind();
                rptPlanInfo.Visible = false;
                lblNoPlanningInfo.Visible = true;
                pnlPlanningInfo.Visible = false;

                //pnlShipmentInfo.Visible = false;
                //pnlAssignMachinesToOrder.Visible = false;
                //pnlMonthYearSelection.Visible = false;
                //pnlMachinesAvailable.Visible = false;

            }

        }



        protected void rptPlanInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var lblMCBrandId = (Label)e.Item.FindControl("lblMCBrandId");

                for (int i = 0; i < dt.Rows.Count; i++)
                {                   
                    var MCBrandId = Convert.ToInt32(dt.Rows[i][3].ToString()); 
                    if (Convert.ToInt32(lblMCBrandId.Text) == MCBrandId)
                    {
                        var knittingTime = Convert.ToInt32(dt.Rows[i][8].ToString());
                        var knittingMCGauge = dt.Rows[i][4].ToString();
                        var NeedlePerCM = dt.Rows[i][6].ToString();
                        var MCSpeed = dt.Rows[i][7].ToString();

                        var tbxKnittingTime = (TextBox)e.Item.FindControl("tbxKnittingTime");
                        var tbxKnittingMCGauge = (TextBox)e.Item.FindControl("tbxKnittingMCGauge");
                        var tbxNeedlePerCM = (TextBox)e.Item.FindControl("tbxNeedlePerCM");
                        var tbxMachineSpeed = (TextBox)e.Item.FindControl("tbxMachineSpeed");

                        tbxKnittingMCGauge.Enabled = true;
                        tbxNeedlePerCM.Enabled = true;
                        tbxMachineSpeed.Enabled = true;
                        tbxKnittingTime.Enabled = true;

                        var chkSelect = (CheckBox)e.Item.FindControl("chkSelect");
                        chkSelect.Checked = true;
                        tbxKnittingTime.Text = knittingTime + "";
                        tbxKnittingMCGauge.Text = knittingMCGauge + "";
                        tbxMachineSpeed.Text = MCSpeed;
                        tbxNeedlePerCM.Text = NeedlePerCM;
                    }
                }
                

                //DropDownList ddlMCBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                //ddlMCBrands.DataTextField = "MachineBrand";
                //ddlMCBrands.DataValueField = "Id";
                //ddlMCBrands.DataSource = dtMachineBrands;
                //ddlMCBrands.DataBind();

                //ddlMCBrands.SelectedValue = DataBinder.Eval(e.Item.DataItem, "MachineBrandId").ToString();
            }
        }


        protected void rptPlanInfo_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int knittingMachineBrandId = 0;

            if (e.CommandName == "Edit")
            {
                e.Item.FindControl("lblMCBrandName").Visible = false;
                e.Item.FindControl("ddlMachineBrands").Visible = true;

                e.Item.FindControl("lblMCGauge").Visible = false;
                e.Item.FindControl("tbxKnittingMCGauge").Visible = true;

                e.Item.FindControl("lblKnittingTime").Visible = false;
                e.Item.FindControl("tbxKnittingTime").Visible = true;

                e.Item.FindControl("lnkbtnEdit").Visible = false;
                e.Item.FindControl("lnkbtnUpdate").Visible = true;
                e.Item.FindControl("lnkbtnCancel").Visible = true;

                e.Item.FindControl("lblStage").Visible = false;
                e.Item.FindControl("ddlStage").Visible = true;

                e.Item.FindControl("lblNeedlePerCM").Visible = false;
                e.Item.FindControl("tbxNeedlePerCM").Visible = true;

                e.Item.FindControl("lblMachineSpeed").Visible = false;
                e.Item.FindControl("tbxMachineSpeed").Visible = true;





            }
            else if (e.CommandName == "Update")
            {

                DropDownList ddlKnittingMCBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");

                if (ddlKnittingMCBrands.SelectedValue != "")
                {
                    knittingMachineBrandId = Convert.ToInt32(ddlKnittingMCBrands.SelectedValue);
                }
                TextBox tbxKnittingMCGauge = (TextBox)e.Item.FindControl("tbxKnittingMCGauge");
                TextBox tbxNeedlePerCM = (TextBox)e.Item.FindControl("tbxNeedlePerCM");
                TextBox tbxMachineSpeed = (TextBox)e.Item.FindControl("tbxMachineSpeed");
                TextBox tbxKnittingTime = (TextBox)e.Item.FindControl("tbxKnittingTime");

                DropDownList ddlStage = (DropDownList)e.Item.FindControl("ddlStage");


                UpdateMachineInfoForProduction(Convert.ToInt32(ddlStyles.SelectedValue), knittingMachineBrandId, tbxKnittingMCGauge.Text, tbxNeedlePerCM.Text, tbxMachineSpeed.Text, tbxKnittingTime.Text, ddlStage.SelectedItem.Text);

                LoadPlanInfo();

            }
            else if (e.CommandName == "Cancel")
            {
                LoadPlanInfo();
            }

        }

        private void UpdateMachineInfoForProduction(int styleId, int knittingMachineBrandId, string knittingMachineGauge, string needlePerCM, string machineSpeed, string knittingTime, string stage)
        {
            int updateResult = productionManager.InsertOrderProductionMachineInfo(styleId, knittingMachineBrandId, knittingMachineGauge, needlePerCM, machineSpeed, knittingTime, stage, CommonMethods.SessionInfo.UserName, DateTime.Now);
            if (updateResult > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
        }

        protected void chkSelect_CheckedChanged(object sender, EventArgs e)
        {
            var checkBox = (CheckBox)sender;
            if (checkBox.Checked == true)
            {
                var tbxKnittingMCGauge = (TextBox)checkBox.Parent.FindControl("tbxKnittingMCGauge");
                var tbxNeedlePerCM = (TextBox)checkBox.Parent.FindControl("tbxNeedlePerCM");
                var tbxMachineSpeed = (TextBox)checkBox.Parent.FindControl("tbxMachineSpeed");
                var tbxKnittingTime = (TextBox)checkBox.Parent.FindControl("tbxKnittingTime");

                tbxKnittingMCGauge.Enabled = true;
                tbxNeedlePerCM.Enabled = true;
                tbxMachineSpeed.Enabled = true;
                tbxKnittingTime.Enabled = true;
            }
            else
            {
                var tbxKnittingMCGauge = (TextBox)checkBox.Parent.FindControl("tbxKnittingMCGauge");
                var tbxNeedlePerCM = (TextBox)checkBox.Parent.FindControl("tbxNeedlePerCM");
                var tbxMachineSpeed = (TextBox)checkBox.Parent.FindControl("tbxMachineSpeed");
                var tbxKnittingTime = (TextBox)checkBox.Parent.FindControl("tbxKnittingTime");

                tbxKnittingMCGauge.Text = "";
                tbxNeedlePerCM.Text = "";
                tbxMachineSpeed.Text = "";
                tbxKnittingTime.Text = "";


                tbxKnittingMCGauge.Enabled = false;
                tbxNeedlePerCM.Enabled = false;
                tbxMachineSpeed.Enabled = false;
                tbxKnittingTime.Enabled = false;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var styleId = Convert.ToInt32(ddlStyles.SelectedValue);
                var stage = ddlStage.SelectedValue;
                var user = CommonMethods.SessionInfo.UserName;
                var saveEnabled = true;
                List<OrderProductionMachineInfo> lstOPMC = new List<OrderProductionMachineInfo>();
                for (int i = 0; i < rptPlanInfo.Items.Count; i++)
                {
                   var chkSelect = (CheckBox)rptPlanInfo.Items[i].FindControl("chkSelect");
                    if (chkSelect.Checked)
                    {
                        var lblMCBrandId = (Label)rptPlanInfo.Items[i].FindControl("lblMCBrandId");
                        var tbxKnittingMCGauge = (TextBox)rptPlanInfo.Items[i].FindControl("tbxKnittingMCGauge");
                        var tbxNeedlePerCM = (TextBox)rptPlanInfo.Items[i].FindControl("tbxNeedlePerCM");
                        var tbxMachineSpeed = (TextBox)rptPlanInfo.Items[i].FindControl("tbxMachineSpeed");
                        var tbxKnittingTime = (TextBox)rptPlanInfo.Items[i].FindControl("tbxKnittingTime");

                        if (string.IsNullOrEmpty(tbxKnittingMCGauge.Text))
                        {
                            saveEnabled = false;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter knitting machine gauge.');", true);
                            break;
                        }
                        if (string.IsNullOrEmpty(tbxKnittingTime.Text))
                        {
                            saveEnabled = false;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter knitting time.');", true);
                            break;
                        }
                        var opmc = new OrderProductionMachineInfo()
                        {
                            StyleId = styleId,
                            KnittingMachineBrandId = int.Parse(lblMCBrandId.Text),
                            KnittingMachineGauge = tbxKnittingMCGauge.Text,
                            NeedlePerCM = tbxNeedlePerCM.Text,
                            KnittingTime = tbxKnittingTime.Text,
                            MachineSpeed = tbxMachineSpeed.Text,
                            Stage = stage,
                            IsNewRecord = 1,
                            CreatedBy = user,
                            CreateDate = DateTime.Now
                        };

                        lstOPMC.Add(opmc);
                    }                   
                }

                if(lstOPMC.Count() > 0)
                {
                    if (saveEnabled)
                    {
                        var lstOldOPMC = unitOfWork.GenericRepositories<OrderProductionMachineInfo>().Get(x => x.StyleId == styleId).ToList();
                        foreach (var item in lstOldOPMC)
                        {
                            item.IsNewRecord = 0;
                            item.UpdatedBy = user;
                            item.UpdateDate = DateTime.Now;
                            unitOfWork.GenericRepositories<OrderProductionMachineInfo>().Update(item);
                        }

                        foreach (var item in lstOPMC)
                        {
                            unitOfWork.GenericRepositories<OrderProductionMachineInfo>().Insert(item);
                        }

                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                    }                    
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select machine brand.');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

        }
    }
}