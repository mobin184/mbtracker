﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class AddLabelAttachment : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var dtFinishigUnits = new CommonManager().GetFinishingUnitsByUser(CommonMethods.SessionInfo.UserId);
                ddlFinishingUnit.DataTextField = "UnitName";
                ddlFinishingUnit.DataValueField = "FinishingUnitId";
                ddlFinishingUnit.DataSource = dtFinishigUnits;
                ddlFinishingUnit.DataBind();

                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                CommonMethods.LoadDropdown(ddlLinkingFloor, "FinishingUnitFloors", 1, 0);
            }
        }

        int Id
        {
            set { ViewState["Id"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["Id"]);
                }
                catch
                {
                    return 0;
                }
            }

        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
            }
            divColors.Visible = false;
            rptLabelAttachmentEntryInfo.Visible = false;
            rptLabelAttachmentEntryInfo.DataSource = null;
            rptLabelAttachmentEntryInfo.DataBind();
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlStyles.SelectedValue != "")
            {
                LoadEntryInfo(int.Parse(ddlStyles.SelectedValue), int.Parse(ddlBuyers.SelectedValue));
                
            }
            else
            {
                divColors.Visible = false;
                rptLabelAttachmentEntryInfo.Visible = false;
                rptLabelAttachmentEntryInfo.DataSource = null;
                rptLabelAttachmentEntryInfo.DataBind();
            }
        }


        private void LoadEntryInfo(int styleId, int buyerId)
        {

            var dtColors = new CommonManager().GetColorsByStyle(styleId);
            if (dtColors.Rows.Count > 0)
            {
                dtColors.Columns.Add("StyleId", typeof(int));
                dtColors.Columns.Add("BuyerId", typeof(int));
                dtColors.Columns.Add("StyleName", typeof(string));
                dtColors.Columns.Add("BuyerName", typeof(string));
                foreach (DataRow row in dtColors.Rows)
                {
                    row["StyleId"] = styleId;
                    row["BuyerId"] = buyerId;
                    row["StyleName"] = ddlStyles.SelectedItem.Text;
                    row["BuyerName"] = ddlBuyers.SelectedItem.Text;
                }

                rptLabelAttachmentEntryInfo.DataSource = dtColors;
                rptLabelAttachmentEntryInfo.DataBind();
                rptLabelAttachmentEntryInfo.Visible = true;
                lblNoDataFound.Visible = false;
                if (Id != 0)
                {
                    btnSaveEntries.Visible = false;
                }
                else
                {
                    btnSaveEntries.Visible = true;
                }
                divColors.Visible = true;
            }
            else
            {
                btnSaveEntries.Visible = false;
                lblNoDataFound.Visible = true;
                rptLabelAttachmentEntryInfo.DataSource = null;
                rptLabelAttachmentEntryInfo.DataBind();
                rptLabelAttachmentEntryInfo.Visible = false;
                divColors.Visible = false;
            }
        }

        protected void gvSizeQuantityLabelAttach_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var buyerId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId")).Text);
                var styleId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId")).Text);

                var dtSizes = buyerManager.GetStyleSizes(styleId);
                TextBox txtSizeQty = null;
                Label lblSizeId = null;

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new TextBox();
                    txtSizeQty.ID = "tbxReceivQuantity" + i.ToString();
                    txtSizeQty.Text = "";
                    txtSizeQty.Width = 60;
                    txtSizeQty.TextMode = TextBoxMode.Number;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);
                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);

                    var entryId = new Label();
                    entryId.ID = "lblentryId" + i.ToString();
                    entryId.Text = "";
                    entryId.Visible = false;
                    e.Row.Cells[i].Controls.Add(entryId);


                    //For Disable size qnty
                    var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());
                    var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                    var buyerColorId = int.Parse(string.IsNullOrEmpty(lblBuyerColorId.Text) ? "0" : lblBuyerColorId.Text);
                    if (buyerColorId != 0)
                    {
                        var sizeQty = CommonMethods.GetSizeQuantityByStyleAndColor(styleId, buyerColorId, sizeId);
                        if (sizeQty <= 0)
                        {
                            txtSizeQty.Enabled = false;
                        }

                    }

                }
            }
        }

        protected void rptLabelAttachmentEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var styleId = int.Parse(ddlStyles.SelectedValue);
                var dtSizes = buyerManager.GetStyleSizes(styleId);
                GridView gvSizeQuantity1 = (GridView)e.Item.FindControl("gvSizeQuantity1");
                Label lblNoSizeFound = (Label)e.Item.FindControl("lblNoSizeFound");
                if (dtSizes.Rows.Count > 0)
                {
                    DataTable dtOneRow = new DataTable();
                    DataRow dr = null;
                    dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                    dr = dtOneRow.NewRow();
                    dr["RowNumber"] = 1;
                    dtOneRow.Rows.Add(dr);

                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantity1.Columns.Add(templateField);
                    }

                    gvSizeQuantity1.DataSource = dtOneRow;
                    gvSizeQuantity1.DataBind();
                    gvSizeQuantity1.Visible = true;
                    lblNoSizeFound.Visible = false;

                }
                else
                {
                    gvSizeQuantity1.Visible = false;
                    lblNoSizeFound.Visible = true;
                }
            }
        }

        protected void btnSaveEntries_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(tbxLabelAttachment.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter Label Attachment date.')", true);
            }
            else if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a Buyer.')", true);
            }
            else if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a Style.')", true);
            }
            else if (ddlFinishingUnit.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a Finishing Unit.')", true);
            }
            else if (ddlLinkingFloor.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a Linking Floor.')", true);
            }
            else
            {
                try
                {
                    var labelAttachmentDate = DateTime.Parse(tbxLabelAttachment.Text);

                    decimal styleTotallbs = 5;
                    if (styleTotallbs > 0)
                    {

                        List<LabelAttachmentInfo> lstLabelAttachmentInfo = new List<LabelAttachmentInfo>();


                        var flag = true;

                        for (int i = 0; i < rptLabelAttachmentEntryInfo.Items.Count; i++)
                        {

                            var buyerColorId = int.Parse(((Label)rptLabelAttachmentEntryInfo.Items[i].FindControl("lblBuyerColorId")).Text);

                            var tbxRemarks = (TextBox)rptLabelAttachmentEntryInfo.Items[i].FindControl("tbxRemarks");
                            var lblColorTotalLBS = (Label)rptLabelAttachmentEntryInfo.Items[i].FindControl("lblColorTotalLBS");


                            if (buyerColorId != 0)
                            {
                                var gvSizeQty = (GridView)rptLabelAttachmentEntryInfo.Items[i].FindControl("gvSizeQuantity1");
                                for (int j = 0; j < gvSizeQty.Columns.Count; j++)
                                {
                                    var sizeId = int.Parse(((Label)gvSizeQty.Rows[0].Cells[j].FindControl("lblSizeId" + j)).Text);
                                    var recQty = ((TextBox)gvSizeQty.Rows[0].Cells[j].FindControl("tbxReceivQuantity" + j)).Text;

                                    if (!string.IsNullOrEmpty(recQty))
                                    {
                                        var labelAttachmentInfo = new LabelAttachmentInfo()
                                        {
                                            BuyerId = int.Parse(ddlBuyers.SelectedValue),
                                            StyleId = int.Parse(ddlStyles.SelectedValue),
                                            LabelAttachDate = labelAttachmentDate,
                                            FinishingUnitId = int.Parse(ddlFinishingUnit.SelectedValue),
                                            LinkingFloorId = int.Parse(ddlLinkingFloor.SelectedValue),
                                            ColorId = buyerColorId,
                                            SizeId = sizeId,
                                            Quantity = decimal.Parse(recQty),
                                            CreateDate = DateTime.Now,
                                            CreatedBy = CommonMethods.SessionInfo.UserName
                                        };
                                        lstLabelAttachmentInfo.Add(labelAttachmentInfo);
                                    }
                                }
                            }
                        }
                        if (flag)
                        {
                            if (lstLabelAttachmentInfo != null && lstLabelAttachmentInfo.Count > 0)
                            {
                                foreach (var item in lstLabelAttachmentInfo)
                                {
                                    unitOfWork.GenericRepositories<LabelAttachmentInfo>().Insert(item);
                                }
                                unitOfWork.Save();
                                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);

                                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('AddLabelAttachment.aspx');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter all valid information.')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You can not issue more that available balance.')", true);
                        }

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('No data was entered.')", true);
                    }

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
        }

        protected void lnkbtnCalculateColorTotal_Click(object sender, EventArgs e)
        {
            ShowColorAndStyleTotal();
        }

        private void ShowColorAndStyleTotal()
        {
            decimal colorTotal = 0;
            decimal styleTotal = 0;
            decimal styleTotalpcs = 0;

            for (int i = 0; i < rptLabelAttachmentEntryInfo.Items.Count; i++)
            {
                GridView gvSizeQuantity1 = (GridView)rptLabelAttachmentEntryInfo.Items[i].FindControl("gvSizeQuantity1");
                var lblTotalInLBS = (Label)rptLabelAttachmentEntryInfo.Items[i].FindControl("lblColorTotalLBS");

                colorTotal = 0;

                for (int j = 0; j < gvSizeQuantity1.Columns.Count; j++)
                {
                    string sizeQuantityTextBox2Id = string.Concat("tbxReceivQuantity", j.ToString());
                    TextBox tbxSizeQty = (TextBox)gvSizeQuantity1.Rows[0].Cells[j].FindControl(sizeQuantityTextBox2Id);

                    if (tbxSizeQty.Text != "")
                    {
                        colorTotal += decimal.Parse(tbxSizeQty.Text);
                    }
                }
                lblTotalInLBS.Text = colorTotal.ToString();

                styleTotal += colorTotal;

            }


            lblStyleTotalpcs.Text = "Style Total: " + styleTotal.ToString() + "(pcs)";
        }

    }
}