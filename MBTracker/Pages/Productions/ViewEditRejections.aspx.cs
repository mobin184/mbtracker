﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using Repositories;
using MBTracker.EF;
using MBTracker.Code_Folder.Productions;

namespace MBTracker.Pages.Productions
{
    public partial class ViewEditRejections : System.Web.UI.Page
    {
        ProductionManager productionManager = new ProductionManager();
        BuyerManager buyerManager = new BuyerManager();
        //OrderManager orderManager = new OrderManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        DataTable dtRejectionInfo;
        //int BuyerColorId;
        DataTable dtSizes;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                EnableDisableEditButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
        }


        private void EnableDisableEditButton()
        {
            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt");

            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewEditRejections", 1, 1);
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {


            ddlStyles.Items.Clear();
            ddlOrders.Items.Clear();
            //pnlColorDeliveryCountryAndQuantity.Visible = false;
            //pnlShipmentSummary.Visible = false;


            if (ddlBuyers.SelectedValue != "")
            {

                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            //else
            //{
            //    ddlStyles.Items.Clear();
            //    ddlOrders.Items.Clear();
            //    pnlColorDeliveryCountryAndQuantity.Visible = false;
            //    pnlShipmentSummary.Visible = false;
            //}
        }

        private void BindStylesByBuyer(int buyerId)
        {

            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);

        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlOrders.Items.Clear();
            //pnlColorDeliveryCountryAndQuantity.Visible = false;
            //pnlShipmentSummary.Visible = false;

            if (ddlStyles.SelectedValue != "")
            {

                BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
            }
            //else
            //{
            //    ddlOrders.Items.Clear();
            //    pnlColorDeliveryCountryAndQuantity.Visible = false;
            //    pnlShipmentSummary.Visible = false;
            //}


        }

        private void BindOrdersByStyle(int styleId)
        {

            CommonMethods.LoadOrderDropdownByStyle(ddlOrders, styleId, 1, 0);

        }


        protected void btnViewRejection_Click(object sender, EventArgs e)
        {

            if (ddlOrders.SelectedValue != "")
            {

                pnlColorAndRejectionQuantity.Visible = true;
                LoadProductRejections();
                //PopulateShipmentDates();
                //PopulateSummary();

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select an order number.')", true);
            }
        }

        private void LoadProductRejections()
        {
            int selectedOrderValue = 0;

            if (ddlOrders.SelectedValue != "")
            {
                selectedOrderValue = Convert.ToInt32(ddlOrders.SelectedValue);
            }

            dtRejectionInfo = productionManager.GetRejectionInfoByOrderId(selectedOrderValue);

            if (dtRejectionInfo.Rows.Count > 0)
            {
                rptRejections.DataSource = dtRejectionInfo;
                rptRejections.DataBind();

                lblNoRejectionDataFound.Visible = false;
                pnlColorAndRejectionQuantity.Visible = true;
            }
            else
            {
                rptRejections.DataSource = null;
                rptRejections.DataBind();
                lblNoRejectionDataFound.Visible = true;
                pnlColorAndRejectionQuantity.Visible = false;
            }


        }

        protected void LoadSizeInfo(int buyerId)
        {
            dtSizes = buyerManager.GetSizes(buyerId);
        }

        protected void rptRejections_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptRejectionColors = (Repeater)e.Item.FindControl("rptRejectionColors");
                Label lblRejectionId = (Label)e.Item.FindControl("lblRejectionId");
                int rejectionId = Convert.ToInt32(lblRejectionId.Text);

                DataTable dtRejectionColors = productionManager.GetRejectionColorsByRejectionId(rejectionId);

                if (dtRejectionColors.Rows.Count > 0)
                {
                    rptRejectionColors.DataSource = dtRejectionColors;
                    rptRejectionColors.DataBind();
                }
            }
        }


        protected void rptRejectionColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {


                LoadSizeInfo(int.Parse(ddlBuyers.SelectedValue));
            
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                GridView gvSizeAndQty = (GridView)e.Item.FindControl("gvSizeAndQty");


                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                 
                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeAndQty.Columns.Add(templateField);
                    }
                }

 
                gvSizeAndQty.DataSource = dtOneRow;
                gvSizeAndQty.DataBind();



            }
        }

        protected void gvSizeAndQty_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                var lblProductionRejectionId = (Label)((GridView)sender).DataItemContainer.FindControl("lblRejectionId2");
                var lblRejectionColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblRejectionColorId");

                int productRejectionId = Convert.ToInt32(lblProductionRejectionId.Text);
                int rejectionColorId = Convert.ToInt32(lblRejectionColorId.Text);

                if (dtSizes == null || dtSizes.Rows.Count == 0)
                {
                    LoadSizeInfo(int.Parse(ddlBuyers.SelectedValue));
                }

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    var sizeId = dtSizes.Rows[i][0].ToString();
                    DataTable dt = productionManager.GetRejectionSizeAndQuantity(productRejectionId, rejectionColorId, int.Parse(sizeId));

                    if (dt.Rows.Count > 0 && dt.Rows[0][1].ToString() != "0")
                    {
                        var lblQty = new Label();
                        lblQty.Text = dt.Rows[0][1].ToString();
                        e.Row.Cells[i].Controls.Add(lblQty);
                    }
                }

            }
        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            int rejectionId = Convert.ToInt32(e.CommandArgument.ToString());
            Response.Redirect("ReportRejection.aspx?rejectionId=" + Tools.UrlEncode(rejectionId + ""));
        }

    }
}