﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class ViewKnittingMachineStatus : System.Web.UI.Page
    {

        int nightTotalRunningMachines = 0;
        int nightTotalUsedForSample = 0;
        int nightTotalUsedForSizeSet = 0;
        int nightTotalLongTimeStop = 0;
        int nightTotalTempStop = 0;
        int nightTotalIdleMachines = 0;
        int nightTotalUsedForSetUp = 0;
        int nightTotalUsedForOtherWorks = 0;
        int nightTotalForUnits = 0;

        int dayTotalRunningMachines = 0;
        int dayTotalUsedForSample = 0;
        int dayTotalUsedForSizeSet = 0;
        int dayTotalLongTimeStop = 0;
        int dayTotalTempStop = 0;
        int dayTotalIdleMachines = 0;
        int dayTotalUsedForSetUp = 0;
        int dayTotalUsedForOtherWorks = 0;
        int dayTotalForUnits = 0;


        UnitOfWork unitOfWork = new UnitOfWork();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //EnableDisableEditButton();
                //EnableDisableDeleteButton();
                //tbxFilterDate.Text = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd");
                //LoadData();
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteMachineStatus(UnitId, KnittingDate, ShiftId);
                }
            }
        }

        private void DeleteMachineStatus(int unitId, DateTime knittingDate, int shiftId)
        {
            try
            {
                var lstMachineInfo = unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().Get(x => x.KnittingUnitId == unitId && x.ProductionDate == knittingDate && x.ShiftId == shiftId).ToList();
                foreach (var item in lstMachineInfo)
                {
                    unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().Delete(item);
                }
                unitOfWork.Save();
                LoadData();
                pnlDetails.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewKnittingMachineStatus", 2, 1);
        }
        private void EnableDisableEditButton()
        {
            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,QCE,Knitting - Floor");

            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewKnittingMachineStatus", 1, 1);

        }

        private void LoadData()
        {
            var date = string.IsNullOrEmpty(tbxFilterDate.Text) ? "" : Convert.ToDateTime(tbxFilterDate.Text).ToString();
            if (!string.IsNullOrEmpty(date))
            {
                var userId = CommonMethods.SessionInfo.UserId;
                string sql = $"Exec usp_GetKnittingMachineStatusSummaryByUserId '{date}', {userId}";
                var dt = unitOfWork.GetDataTableFromSql(sql);
                lblSummary.Visible = true;
                dataDisplayRepeater.Visible = true;

                if (dt.Rows.Count > 0)
                {
                    rpt.Visible = true;
                    dataDiv.Visible = true;
                    dt = dt.CheckEditDeletePermission(CommonMethods.SessionInfo.RoleId, "ViewKnittingMachineStatus");
                    rpt.DataSource = dt;
                    rpt.DataBind();
                    lblNoDataFound.Visible = false;
                }
                else
                {
                    rpt.Visible = false;
                    dataDiv.Visible = false;
                    lblNoDataFound.Visible = true;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select knitting date!')", true);
            }
        }


        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                int shiftId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "ShiftId").ToString());

                int totalNoOfRunningMachine = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "totalNoOfRunningMachine").ToString());
                int totalUsedForSample = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "totalUsedForSample").ToString());
                int totalUsedForSizeSet = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "totalUsedForSizeSet").ToString());
                int totalLongTimeStop = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "totalLongTimeStop").ToString());
                int totalTemporaryStop = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "totalTemporaryStop").ToString());
                int totalIdleMachine = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "totalIdleMachine").ToString());
                int totalUsedForSetup = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "totalUsedForSetup").ToString());
                int totalUsedForOtherWorks = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "totalUsedForOtherWorks").ToString());
                int unitTotal = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "unitTotal").ToString());



                if (shiftId == 1)
                {
                    dayTotalRunningMachines = dayTotalRunningMachines + totalNoOfRunningMachine;
                    dayTotalUsedForSample = dayTotalUsedForSample + totalUsedForSample;
                    dayTotalUsedForSizeSet = dayTotalUsedForSizeSet + totalUsedForSizeSet;
                    dayTotalLongTimeStop = dayTotalLongTimeStop + totalLongTimeStop;
                    dayTotalTempStop = dayTotalTempStop + totalTemporaryStop;
                    dayTotalIdleMachines = dayTotalIdleMachines + totalIdleMachine;
                    dayTotalUsedForSetUp = dayTotalUsedForSetUp + totalUsedForSetup;
                    dayTotalUsedForOtherWorks = dayTotalUsedForOtherWorks + totalUsedForOtherWorks;
                    dayTotalForUnits = dayTotalForUnits + unitTotal;

                }
                else if (shiftId == 2)
                {
                    nightTotalRunningMachines = nightTotalRunningMachines + totalNoOfRunningMachine;
                    nightTotalUsedForSample = nightTotalUsedForSample + totalUsedForSample;
                    nightTotalUsedForSizeSet = nightTotalUsedForSizeSet + totalUsedForSizeSet;
                    nightTotalLongTimeStop = nightTotalLongTimeStop + totalLongTimeStop;
                    nightTotalTempStop = nightTotalTempStop + totalTemporaryStop;
                    nightTotalIdleMachines = nightTotalIdleMachines + totalIdleMachine;
                    nightTotalUsedForSetUp = nightTotalUsedForSetUp + totalUsedForSetup;
                    nightTotalUsedForOtherWorks = nightTotalUsedForOtherWorks + totalUsedForOtherWorks;
                    nightTotalForUnits = nightTotalForUnits + unitTotal;
                }
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {

                Label lblTotalRunningMCs = (Label)e.Item.FindControl("lblTotalRunningMCs");
                Label lblTotalUsedForSample = (Label)e.Item.FindControl("lblTotalUsedForSample");
                Label lblTotalUsedForSizeSet = (Label)e.Item.FindControl("lblTotalUsedForSizeSet");
                Label lblLongTimeStop = (Label)e.Item.FindControl("lblLongTimeStop");
                Label lblTempStop = (Label)e.Item.FindControl("lblTempStop");
                Label lblIdleMachines = (Label)e.Item.FindControl("lblIdleMachines");
                Label lblUsedForSetUp = (Label)e.Item.FindControl("lblUsedForSetUp");
                Label lblUsedForOtherWorks = (Label)e.Item.FindControl("lblUsedForOtherWorks");
                Label lblTotalMachinesForUnits = (Label)e.Item.FindControl("lblTotalMachinesForUnits");

           

                lblTotalRunningMCs.Text = "Night: " + nightTotalRunningMachines + "<br/>&nbsp;&nbsp;Day: " + dayTotalRunningMachines;
                lblTotalUsedForSample.Text = "Night: " + nightTotalUsedForSample + "<br/>&nbsp;&nbsp;Day: " + dayTotalUsedForSample;
                lblTotalUsedForSizeSet.Text = "Night: " + nightTotalUsedForSizeSet + "<br/>&nbsp;&nbsp;Day: " + dayTotalUsedForSizeSet;
                lblLongTimeStop.Text = "Night: " + nightTotalLongTimeStop + "<br/>&nbsp;&nbsp;Day: " + dayTotalLongTimeStop;
                lblTempStop.Text = "Night: " + nightTotalTempStop + "<br/>&nbsp;&nbsp;Day: " + dayTotalTempStop;
                lblIdleMachines.Text = "Night: " + nightTotalIdleMachines + "<br/>&nbsp;&nbsp;Day: " + dayTotalIdleMachines;
                lblUsedForSetUp.Text = "Night: " + nightTotalUsedForSetUp + "<br/>&nbsp;&nbsp;Day: " + dayTotalUsedForSetUp;
                lblUsedForOtherWorks.Text = "Night: " + nightTotalUsedForOtherWorks + "<br/>&nbsp;&nbsp;Day: " + dayTotalUsedForOtherWorks;
                lblTotalMachinesForUnits.Text = "Night: " + nightTotalForUnits + "<br/>&nbsp;&nbsp;Day: " + dayTotalForUnits;

            }
        }




        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            int unitId = int.Parse(commandArgs[0]);
            string knittingDate = DateTime.Parse(commandArgs[1]).ToString("yyyy-MM-dd");
            int shiftId = int.Parse(commandArgs[2]);

            Response.Redirect("AddKnittingMachineStatus.aspx?unitId=" + Tools.UrlEncode(unitId + "") + "&knittingDate=" + Tools.UrlEncode(knittingDate + "") + "&shiftId=" + Tools.UrlEncode(shiftId + ""));
        }

        protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        {
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            UnitId = int.Parse(commandArgs[0]);
            KnittingDate = DateTime.Parse(commandArgs[1]);
            ShiftId = int.Parse(commandArgs[2]);
            var unitName = commandArgs[3];
            var shiftName = commandArgs[4];
            PopulateEntryInfo(UnitId);
            lblKnittingMachineStatusView.Text = $"Unit: {unitName} <br/>Shift: {shiftName}<br/> Date: {KnittingDate.ToString("dd-MM-yyyy")}";
            pnlDetails.Visible = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown()", true);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
            pnlDetails.Visible = false;
        }


        int UnitId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["unitId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["unitId"] = value; }
        }

        int ShiftId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["shiftId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["shiftId"] = value; }
        }


        DateTime KnittingDate
        {
            get
            {
                try
                {
                    return Convert.ToDateTime(ViewState["knittingDate"]);
                }
                catch
                {
                    return DateTime.Today;
                }
            }
            set { ViewState["knittingDate"] = value; }
        }

        private void PopulateEntryInfo(int unitId)
        {
            string sql = $"Exec usp_GetAllMachineBrandByUnitId {unitId}";
            var dt = unitOfWork.GetDataTableFromSql(sql);

            if (dt.Rows.Count > 0)
            {
                lblNoDataFound.Visible = false;
                rptEntryInfo.Visible = true;
                lblKnittingMachineStatusView.Visible = true;
            }
            else
            {
                lblNoDataFoundForView.Visible = true;
                rptEntryInfo.Visible = false;
                lblKnittingMachineStatusView.Visible = true;
            }
            rptEntryInfo.DataSource = dt;
            rptEntryInfo.DataBind();
        }

        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (UnitId != 0 && ShiftId != 0)
                {
                    unitOfWork = new UnitOfWork();
                    var lblDailyKnittingMachineStatusId = (Label)e.Item.FindControl("lblDailyKnittingMachineStatusId");
                    var lblMachineBrandId = (Label)e.Item.FindControl("lblMachineBrandId");
                    var machinBrandId = int.Parse(lblMachineBrandId.Text);
                    var lblNoOfRunningMachine = (Label)e.Item.FindControl("lblNoOfRunningMachine");
                    var lblUsedForSample = (Label)e.Item.FindControl("lblUsedForSample");
                    var lblUsedForSizeSet = (Label)e.Item.FindControl("lblUsedForSizeSet");
                    var lblLongTimeStoped = (Label)e.Item.FindControl("lblLongTimeStoped");
                    var lblTemporaryStoped = (Label)e.Item.FindControl("lblTemporaryStoped");
                    var lblIdleMachine = (Label)e.Item.FindControl("lblIdleMachine");
                    var lblUsedForOtherWorks = (Label)e.Item.FindControl("lblUsedForOtherWorks");

                    var lblPurposeOfOtherWorksValue = (Label)e.Item.FindControl("lblPurposeOfOtherWorksValue");
                    var lblTotalMachines = (Label)e.Item.FindControl("lblTotalMachines");

                    var lblMachineUsedForSetup = (Label)e.Item.FindControl("lblMachineUsedForSetup");

                    var statusInfo = unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().Get().Where(x => x.KnittingUnitId == UnitId && x.ShiftId == ShiftId && x.MachineBrandId == machinBrandId && x.ProductionDate == KnittingDate).ToList();
                    if (statusInfo != null)
                    {
                        lblDailyKnittingMachineStatusId.Text = statusInfo.FirstOrDefault().Id + "";
                        lblNoOfRunningMachine.Text = statusInfo.Sum(x=>x.NoOfRunningMachine) + "";
                        lblUsedForSample.Text = statusInfo.Sum(x => x.UsedForSample) + "";
                        lblUsedForSizeSet.Text = statusInfo.Sum(x => x.UsedForSizeSet) + "";
                        lblLongTimeStoped.Text = statusInfo.Sum(x => x.LongTimeStop) + "";
                        lblTemporaryStoped.Text = statusInfo.Sum(x => x.TemporaryStop) + "";
                        lblIdleMachine.Text = statusInfo.Sum(x => x.IdleMachine) + "";
                        lblUsedForOtherWorks.Text = statusInfo.Sum(x => x.UsedForOtherWorks) + "";
                        lblPurposeOfOtherWorksValue.Text = statusInfo.FirstOrDefault().PurposeOfOtherWorks + "";
                        lblMachineUsedForSetup.Text = statusInfo.Sum(x => x.UsedForSetup) + "";



                        var runningMachine = string.IsNullOrEmpty(lblNoOfRunningMachine.Text) ? (int?)0 : int.Parse(lblNoOfRunningMachine.Text);
                        var forSample = string.IsNullOrEmpty(lblUsedForSample.Text) ? (int?)0 : int.Parse(lblUsedForSample.Text);
                        var forSizeSet = string.IsNullOrEmpty(lblUsedForSizeSet.Text) ? (int?)0 : int.Parse(lblUsedForSizeSet.Text);
                        var longTimeStop = string.IsNullOrEmpty(lblLongTimeStoped.Text) ? (int?)0 : int.Parse(lblLongTimeStoped.Text);
                        var tempStop = string.IsNullOrEmpty(lblTemporaryStoped.Text) ? (int?)0 : int.Parse(lblTemporaryStoped.Text);
                        var idleMachine = string.IsNullOrEmpty(lblIdleMachine.Text) ? (int?)0 : int.Parse(lblIdleMachine.Text);
                        var usedForSetup = string.IsNullOrEmpty(lblUsedForOtherWorks.Text) ? (int?)0 : int.Parse(lblUsedForOtherWorks.Text);
                        var otherWorks = string.IsNullOrEmpty(lblMachineUsedForSetup.Text) ? (int?)0 : int.Parse(lblMachineUsedForSetup.Text);

                        lblTotalMachines.Text = (runningMachine + forSample +forSizeSet+ longTimeStop + tempStop + idleMachine + usedForSetup + otherWorks) + "";
                    }
                }
            }
        }

        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            UnitId = int.Parse(commandArgs[0]);
            KnittingDate = DateTime.Parse(commandArgs[1]);
            ShiftId = int.Parse(commandArgs[2]);

            if (UnitId != 0)
            {
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
            }
            else
            {
                var title = "Error";
                var msg = "Sorry, you can not delete it.<br />Something went wrong.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            }
        }
    }
}