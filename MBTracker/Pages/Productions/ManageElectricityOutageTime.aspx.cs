﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class ManageElectricityOutageTime : System.Web.UI.Page
    {
        ProductionManager productionManager = new ProductionManager();

        UnitOfWork unitOfWork = new UnitOfWork();



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableDeleteButton();

                CommonMethods.LoadKnittingUnitDropdown(ddlUserUnits, 1, 0);
                CommonMethods.LoadDropdown(ddlShifts, "Shifts", 1, 0);
                DisableFields();
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteOutageRecord(OutageId);
                }
            }
        }

        private void DeleteOutageRecord(int outageId)
        {
            try
            {
                var outage = unitOfWork.GenericRepositories<KnittingUnitElectricityOutages>().GetByID(outageId);

                unitOfWork.GenericRepositories<KnittingUnitElectricityOutages>().Delete(outage);
                unitOfWork.Save();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
                Response.AddHeader("REFRESH", "2;URL=ManageElectricityOutageTime.aspx");

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");

            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ManageElectricityOutageTime", 2, 1);

        }

        int OutageId
        {
            set { ViewState["outageId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["outageId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlUserUnits_SelectedIndexChanged(object sender, EventArgs e)
        {

            if ((ddlUserUnits.SelectedValue != "") && (ddlShifts.SelectedValue != "") && (tbxDate.Text != ""))
            {
                BindData(Convert.ToInt32(ddlUserUnits.SelectedValue), Convert.ToInt32(ddlShifts.SelectedValue), tbxDate.Text.ToString());
            }
            else
            {
                pnlOutages.Visible = false;
            }
        }

        protected void ddlShifts_SelectedIndexChanged(object sender, EventArgs e)
        {

            if ((ddlUserUnits.SelectedValue != "") && (ddlShifts.SelectedValue != "") && (tbxDate.Text != ""))
            {
                BindData(Convert.ToInt32(ddlUserUnits.SelectedValue), Convert.ToInt32(ddlShifts.SelectedValue), tbxDate.Text.ToString());
            }
            else
            {
                pnlOutages.Visible = false;
            }
        }


        protected void tbxDate_OnTextChanged(object sender, EventArgs e)
        {


            if ((ddlUserUnits.SelectedValue != "") && (ddlShifts.SelectedValue != "") && (tbxDate.Text != ""))
            {
                BindData(Convert.ToInt32(ddlUserUnits.SelectedValue), Convert.ToInt32(ddlShifts.SelectedValue), tbxDate.Text.ToString());
            }
            else
            {
                pnlOutages.Visible = false;
            }
        }

        private void BindData(int unitId, int shiftId, string selectedDate)
        {

            var dt = productionManager.LoadElectricityOutagesByUnitAndDate(unitId, shiftId, DateTime.Parse(selectedDate));

            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                pnlOutages.Visible = true;
                DisableFields();
            }
            else
            {
                pnlOutages.Visible = false;
                EnableFields();
            }

        }

        private void DisableFields()
        {
            tbxNumTimes.Enabled = false;
            tbxElectricityOffTime.Enabled = false;
            tbxProdLossTime.Enabled = false;

            tbxNumTimes.Text = "";
            tbxElectricityOffTime.Text = "";
            tbxProdLossTime.Text = "";

        }

        private void EnableFields()
        {

            tbxNumTimes.Enabled = true;
            tbxElectricityOffTime.Enabled = true;
            tbxProdLossTime.Enabled = true;

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if ((ddlUserUnits.SelectedValue != "") && (ddlShifts.SelectedValue != "") && !string.IsNullOrEmpty(tbxDate.Text) && !string.IsNullOrEmpty(tbxNumTimes.Text) && !string.IsNullOrEmpty(tbxElectricityOffTime.Text) && !string.IsNullOrEmpty(tbxProdLossTime.Text))
                {

                    var electricityOutage = new KnittingUnitElectricityOutages()
                    {
                        OutageDate = tbxDate.Text.ToString(),
                        KnittingUnitId = int.Parse(ddlUserUnits.SelectedValue),
                        ShiftId = int.Parse(ddlShifts.SelectedValue),
                        NumOutageTimes = int.Parse(tbxNumTimes.Text.ToString()),
                        TotalOutageTime = int.Parse(tbxElectricityOffTime.Text.ToString()),
                        ProductionLossTime = int.Parse(tbxProdLossTime.Text.ToString()),
                        Remarks = tbxRemarks.Text.ToString(),

                        CreatedBy = CommonMethods.SessionInfo.UserName,
                        CreateDate = DateTime.Now
                    };
                    unitOfWork.GenericRepositories<KnittingUnitElectricityOutages>().Insert(electricityOutage);
                    unitOfWork.Save();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);

                    BindData(Convert.ToInt32(ddlUserUnits.SelectedValue), Convert.ToInt32(ddlShifts.SelectedValue), tbxDate.Text.ToString());

                    DisableFields();

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select date and a unit.')", true);
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            try
            {

                var outageInfo = unitOfWork.GenericRepositories<KnittingUnitElectricityOutages>().GetByID(OutageId);
                if (outageInfo != null)
                {

                    outageInfo.NumOutageTimes = int.Parse(tbxNumTimes.Text.ToString());
                    outageInfo.TotalOutageTime = int.Parse(tbxElectricityOffTime.Text.ToString());
                    outageInfo.ProductionLossTime = int.Parse(tbxProdLossTime.Text.ToString());
                    outageInfo.Remarks = tbxRemarks.Text.ToString();

                    outageInfo.UpdateDate = DateTime.Now;
                    outageInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;

                    unitOfWork.GenericRepositories<KnittingUnitElectricityOutages>().Update(outageInfo);
                    unitOfWork.Save();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                    BindData(Convert.ToInt32(ddlUserUnits.SelectedValue), Convert.ToInt32(ddlShifts.SelectedValue), tbxDate.Text.ToString());
                    
                    ClearFormData();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Invalid color Id.')", true);
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }

        }


        private void ClearFormData()
        {
            tbxNumTimes.Text = string.Empty;
            tbxElectricityOffTime.Text = string.Empty;
            tbxProdLossTime.Text = string.Empty;
        }


        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                OutageId = Convert.ToInt32(e.CommandArgument.ToString());
                PopulateOutage(OutageId);
            }
            else if (e.CommandName == "Delete")
            {
                OutageId = Convert.ToInt32(e.CommandArgument.ToString());

                var outageInfo = unitOfWork.GenericRepositories<KnittingUnitElectricityOutages>().GetByID(OutageId);
                if (outageInfo != null)
                {
                    var title = "Warning";
                    var msg = "Are you sure you want to delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                }
                //else
                //{
                //    var title = "Warning";
                //    var msg = "Sorry, you can not delete it.<br />There was some dependencies of this color.";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                //}
            }
        }

        private void PopulateOutage(int outageId)
        {
            var outageInfo = unitOfWork.GenericRepositories<KnittingUnitElectricityOutages>().GetByID(outageId);

            if (outageInfo != null)
            {
                OutageId = outageInfo.Id;

                tbxDate.Text = outageInfo.OutageDate;
                ddlUserUnits.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUserUnits, outageInfo.KnittingUnitId);
                ddlShifts.SelectedIndex = CommonMethods.MatchDropDownItem(ddlShifts, outageInfo.ShiftId);


                tbxNumTimes.Text = outageInfo.NumOutageTimes.ToString();
                tbxElectricityOffTime.Text = outageInfo.TotalOutageTime.ToString();
                tbxProdLossTime.Text = outageInfo.ProductionLossTime.ToString();
                tbxRemarks.Text = outageInfo.Remarks;


                EnableFields();
                tbxDate.Enabled = false;
                ddlUserUnits.Enabled = false;
                ddlShifts.Enabled = false;

                btnSave.Visible = false;
                btnUpdate.Visible = true;
                styleActionTitle.Text = "Update Electricity Outage:";

            }
        }
    }
}