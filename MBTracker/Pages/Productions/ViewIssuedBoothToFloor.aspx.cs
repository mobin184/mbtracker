﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class ViewIssuedBoothToFloor : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        OrderManager orderManager = new OrderManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableEditButton();
                EnableDisableDeleteButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteBoothIssueInfo(IssueId);
                }
            }
        }

        //public static void LoadProductionUnitsForIssueByUser(DropDownList ddlUserProductionUnit)
        //{

        //    var dtKnittingUnit = new CommonManager().GetProductionUnitsByUser(CommonMethods.SessionInfo.UserId);

        //    var lstOtherUnits = new UnitOfWork().GenericRepositories<OtherKnittingUnits>().Get().ToList();
        //    foreach (var item in lstOtherUnits)
        //    {
        //        var drow = dtKnittingUnit.NewRow();
        //        drow["ProductionUnitId"] = item.ProductionUnitId;
        //        drow["UnitName"] = item.UnitName;
        //        dtKnittingUnit.Rows.Add(drow);
        //    }

        //    var dr1 = dtKnittingUnit.NewRow();
        //    dr1["ProductionUnitId"] = 0;
        //    dr1["UnitName"] = "---Select---";
          
        //    dtKnittingUnit.Rows.InsertAt(dr1, 0);
        //    ddlUserProductionUnit.DataTextField = "UnitName";
        //    ddlUserProductionUnit.DataValueField = "ProductionUnitId";
        //    ddlUserProductionUnit.DataSource = dtKnittingUnit;
        //    ddlUserProductionUnit.DataBind();
        //}

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
            }
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlColors.Items.Clear();
            if (ddlStyles.SelectedValue != "")
            {
                LoadColorDropdown(int.Parse(ddlStyles.SelectedValue), ddlColors);
            }
        }

        private void DeleteBoothIssueInfo(int IssueId)
        {
            var issueInfo = unitOfWork.GenericRepositories<BoothIssueToFloor>().GetByID(IssueId);
           
            unitOfWork.GenericRepositories<BoothIssueToFloor>().Delete(issueInfo);
            unitOfWork.Save();
            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
            LoadData();
        }

        private void EnableDisableDeleteButton()
        {
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewIssuedBoothToFloor", 2, 1);
        }

        private void EnableDisableEditButton()
        {
            
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewIssuedBoothToFloor", 1, 1);
        }

        protected void btnViewEntries_Click(object sender, EventArgs e)
        {
            try
            {
                LoadData();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }


        protected void LoadData()
        {
            divSummary.Visible = false;
            
           // var knittingUnit = 0;
            var issueDate = "";
            if (!string.IsNullOrEmpty(tbxIssuedDate.Text))
            {
                issueDate = DateTime.Parse(tbxIssuedDate.Text).ToString("yyyy-MM-dd");
            }

            
            var buyerId = 0;
            if (ddlBuyers.SelectedValue != "")
            {
                buyerId = int.Parse(ddlBuyers.SelectedValue);
            }

            var styleId = 0;

            if (ddlStyles.SelectedValue != "")
            {
                styleId = int.Parse(ddlStyles.SelectedValue);
            }

            var colorId = 0;

            if (ddlColors.SelectedValue != "")
            {
                colorId = int.Parse(ddlColors.SelectedValue);
            }

            var dtEntries = unitOfWork.GetDataTableFromSql($"EXEC usp_BoothIssuedAtFloorByUserAndDate  '{issueDate}','{buyerId}','{styleId}','{colorId}' ");

            if (dtEntries.Rows.Count > 0)
                    {
                        dtEntries = dtEntries.CheckEditDeletePermission(CommonMethods.SessionInfo.RoleId, "ViewIssuedBoothToFloor");
                        rptSummary.DataSource = dtEntries;
                        rptSummary.DataBind();
                        rptSummary.Visible = true;
                        lblEntryNotFound.Visible = false;
                        dataDiv.Visible = true;
                    }
                    else
                    {
                        rptSummary.DataSource = null;
                        rptSummary.DataBind();
                        rptSummary.Visible = false;
                        lblEntryNotFound.Visible = true;
                        dataDiv.Visible = false;
                    }
                    divSummary.Visible = true;
              
        }

        protected void rptSummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var issuedQty = decimal.Parse(DataBinder.Eval(e.Item.DataItem, "Quantity").ToString());
               
            }
        }

        
        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            var BoothIssueId = int.Parse(e.CommandArgument.ToString());
            IssueId = BoothIssueId;

            //for (int i = 0; i < rptSummary.Items.Count; i++)
            //{
            //    var tbxUpdatedQtyR = (TextBox)rptSummary.Items[i].FindControl("UpdatedIssuedQty");
            //    if (tbxUpdatedQtyR.Visible)
            //    {
            //        var lnkbtnEditR = (LinkButton)rptSummary.Items[i].FindControl("lnkbtnEdit");
            //        var lblIssuedQtyR = (Label)rptSummary.Items[i].FindControl("IssuedQty");
            //        var btnUpdateR = (LinkButton)rptSummary.Items[i].FindControl("lnkbtnUpdated");
            //        var lnkbtnCancelR = (LinkButton)rptSummary.Items[i].FindControl("lnkbtnCancel");
            //        var btnDeleteR = (LinkButton)rptSummary.Items[i].FindControl("lnkbtnDelete");

            //        tbxUpdatedQtyR.Visible = false;
            //        btnUpdateR.Visible = false;
            //        lnkbtnCancelR.Visible = false;
            //        btnDeleteR.Visible = true;
            //        lblIssuedQtyR.Visible = true;
            //        lnkbtnEditR.Visible = true;

            //        var rptIssueDateR = (TextBox)rptSummary.Items[i].FindControl("rptIssueDate");
            //        var rptStylesR = (DropDownList)rptSummary.Items[i].FindControl("rptStyles");
            //        var rptColorsR = (DropDownList)rptSummary.Items[i].FindControl("rptColors");
            //        var trAvailableBalanceR = (PlaceHolder)rptSummary.Items[i].FindControl("trAvailableBalance");

            //        rptIssueDateR.Visible = false;
            //        rptStylesR.Visible = false;
            //        rptColorsR.Visible = false;
            //        trAvailableBalanceR.Visible = false;

            //        var lblIssueDateR = (Label)rptSummary.Items[i].FindControl("lblIssueDate");
            //        var lblStylesR = (Label)rptSummary.Items[i].FindControl("lblStyles");
            //        var lblColorsR = (Label)rptSummary.Items[i].FindControl("lblColors");
                    
            //        lblIssueDateR.Visible = true;
            //        lblStylesR.Visible = true;
            //        lblColorsR.Visible = true;
                    
            //    }
            //}


            var tbxUpdatedQty = (TextBox)((RepeaterItem)((LinkButton)sender).Parent).FindControl("UpdatedIssuedQty");
            var lblIssuedQty = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("IssuedQty");
            var btnUpdate = (LinkButton)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lnkbtnUpdated");
            var lnkbtnCancel = (LinkButton)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lnkbtnCancel");
            var btnDelete = (LinkButton)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lnkbtnDelete");

            var rptIssueDate = (TextBox)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptIssueDate");
            var rptStyles = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptStyles");
            var rptKnittingFactory = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptKnittingFactory");
            var rptFinishingUnit = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptFinishingUnit");
            var rptLinkingFloor = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptLinkingFloor");
            var rptColors = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptColors");
            var rptSizes = (DropDownList)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptSizes");
            

            var lblIssueDate = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblIssueDate");
            var lblStyles = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblStyles");
            var lblKnittingFactory = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblKnittingFactory");
            var lblFinishingUnit = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblFinishingUnit");
            var lblLinkingFloor = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblLinkingFloor");
            var lblColors = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblColors");
            var lblSizes = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lblSizes");
            
            var trAvailableBalance = (PlaceHolder)((RepeaterItem)((LinkButton)sender).Parent).FindControl("trAvailableBalance");
            var rptAvailableBalance = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("rptAvailableBalance");


            var btnEdit = ((LinkButton)sender);

            tbxUpdatedQty.Visible = true;
            lblIssuedQty.Visible = false;
            btnUpdate.Visible = true;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            lnkbtnCancel.Visible = true;
            trAvailableBalance.Visible = true;
           
            var issueInfo = unitOfWork.GenericRepositories<BoothIssueToFloor>().GetByID(IssueId);

            LoadStyleDropdown(issueInfo.BuyerId, rptStyles);
            CommonMethods.LoadDropdown(rptKnittingFactory, "KnittingFactory", 1, 0);
            CommonMethods.LoadDropdown(rptFinishingUnit, "FinishingUnits", 1, 0);
            CommonMethods.LoadDropdown(rptLinkingFloor, "FinishingUnitFloors", 1, 0);
            LoadColorDropdown(issueInfo.StyleId, rptColors);
            LoadSizesDropdown(issueInfo.StyleId, rptSizes);
           // LoadStores(rptStores);

            //set data
            rptStyles.SelectedValue = issueInfo.StyleId + "";
            rptKnittingFactory.SelectedValue = issueInfo.KnittingFactoryId + "";
            rptFinishingUnit.SelectedValue = issueInfo.FinishingUnitId + "";
            rptLinkingFloor.SelectedValue = issueInfo.LinkingFloorId + "";
            rptColors.SelectedValue = issueInfo.ColorId + "";
            rptSizes.SelectedValue = issueInfo.SizeId + "";
            rptIssueDate.Text = issueInfo.IssueDate.ToString("yyyy-MM-dd");

            rptIssueDate.Visible = true;
            rptKnittingFactory.Visible = true;
            rptFinishingUnit.Visible = true;
            rptLinkingFloor.Visible = true;
            rptColors.Visible = true;
            rptSizes.Visible = true;
            rptStyles.Visible = true;

            lblIssueDate.Visible = false;
            lblStyles.Visible = false;
            lblKnittingFactory.Visible = false;
            lblFinishingUnit.Visible = false;
            lblLinkingFloor.Visible = false;
            lblColors.Visible = false;
            lblSizes.Visible = false;
            lblAvailableBalance.Visible = true;
           
        }

        private void LoadSizesDropdown(int styleId, DropDownList ddl)
        {
            var dt = unitOfWork.GetDataTableFromSql("SELECT  bs.Id,bs.[SizeName] FROM StyleSizes ss INNER JOIN BuyerSizes bs ON ss.SizeId = bs.Id WHERE ss.StyleId = styleId ORDER BY bs.OrderBy ASC ");
            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = "SizeName";
            ddl.DataValueField = "Id";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;
        }

        //private void LoadKnittingUnits(DropDownList ddl)
        //{
        //    var dtKnittingUnit = new CommonManager().GetProductionUnitsByUser(CommonMethods.SessionInfo.UserId);
        //    var lstOtherUnits = new UnitOfWork().GenericRepositories<OtherKnittingUnits>().Get().ToList();
        //    foreach (var item in lstOtherUnits)
        //    {
        //        var drow = dtKnittingUnit.NewRow();
        //        drow["ProductionUnitId"] = item.ProductionUnitId;
        //        drow["UnitName"] = item.UnitName;
        //        dtKnittingUnit.Rows.Add(drow);
        //    }


        //    ddl.DataSource = dtKnittingUnit;
        //    ddl.DataTextField = "UnitName";
        //    ddl.DataValueField = "ProductionUnitId";
        //    ddl.DataBind();
        //    ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
        //    ddl.SelectedIndex = 0;
        //}

        private void LoadStyleDropdown(int buyerId, DropDownList ddlStyles)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        private void LoadColorDropdown(int styleId, DropDownList ddl)
        {
            var dt = orderManager.GetColorsByStyleId(styleId);

            var dr = dt.NewRow();
            dr["BuyerColorId"] = "";
            dr["ColorDescription"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);
            ddl.DataSource = dt;
            ddl.DataTextField = "ColorDescription";
            ddl.DataValueField = "BuyerColorId";
            ddl.DataBind();
        }


        int IssueId
        {
            set { ViewState["IssueId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["IssueId"]);
                }
                catch
                {
                    return 0;
                }
            }

        }


        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            IssueId = int.Parse(e.CommandArgument.ToString());
            if (IssueId != 0)
            {
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);

               
            }
            else
            {
                var title = "Error";
                var msg = "Sorry, you can not delete it.<br />Something went wrong.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            }
        }

        protected void lnkbtnUpdated_Command(object sender, CommandEventArgs e)
        {
            var yarnIssueId = int.Parse(e.CommandArgument.ToString());
            var tbxUpdatedQty = (TextBox)((RepeaterItem)((LinkButton)sender).Parent).FindControl("UpdatedIssuedQty");
            var lblIssuedQty = (Label)((RepeaterItem)((LinkButton)sender).Parent).FindControl("IssuedQty");
            var btnUpdate = (LinkButton)((RepeaterItem)((LinkButton)sender).Parent).FindControl("lnkbtnUpdated");
            var btnEdit = ((LinkButton)sender);
            var ddl = (LinkButton)sender;
            var rptIssueDate = (TextBox)ddl.NamingContainer.FindControl("rptIssueDate");
            var rptStyles = (DropDownList)ddl.NamingContainer.FindControl("rptStyles");
            var rptKnittingFactory = (DropDownList)ddl.NamingContainer.FindControl("rptKnittingFactory");
            var rptFinishingUnit = (DropDownList)ddl.NamingContainer.FindControl("rptFinishingUnit");
            var rptLinkingFloor = (DropDownList)ddl.NamingContainer.FindControl("rptLinkingFloor");
            var rptColors = (DropDownList)ddl.NamingContainer.FindControl("rptColors");
            var rptSizes = (DropDownList)ddl.NamingContainer.FindControl("rptSizes");
            
            if (string.IsNullOrEmpty(rptIssueDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select Issue Date.')", true);
                return;
            }
            else if (rptStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select style.')", true);
                return;
            }
            else if (rptKnittingFactory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select Knitting Factory.')", true);
                return;
            }
            else if (rptFinishingUnit.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select Finishing Unit.')", true);
                return;
            }
            else if (rptLinkingFloor.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select Linking Floor.')", true);
                return;
            }
            else if (rptColors.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select color.')", true);
                return;
            }
            else if (rptSizes.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('Please select Sizes.')", true);
                return;
            }

            if (!string.IsNullOrEmpty(tbxUpdatedQty.Text))
            {
                var issueInfo = unitOfWork.GenericRepositories<BoothIssueToFloor>().GetByID(IssueId);
                if (issueInfo != null)
                {
                    var updatedIssuedQty = decimal.Parse(tbxUpdatedQty.Text);
                    var styleId = 0;
                    if (rptStyles.SelectedValue != "")
                    {
                        styleId = int.Parse(rptStyles.SelectedValue);
                    }
                    var knittingFactoryId = 0;
                    if (rptKnittingFactory.SelectedValue != "")
                    {
                        knittingFactoryId = int.Parse(rptKnittingFactory.SelectedValue);
                    }
                    var FinishingUnitId = 0;
                    if (rptFinishingUnit.SelectedValue != "")
                    {
                        FinishingUnitId = int.Parse(rptFinishingUnit.SelectedValue);
                    }
                    var LinkingFloorId = 0;
                    if (rptLinkingFloor.SelectedValue != "")
                    {
                        LinkingFloorId = int.Parse(rptLinkingFloor.SelectedValue);
                    }
                    var colorId = 0;
                    if (rptColors.SelectedValue != "")
                    {
                        colorId = int.Parse(rptColors.SelectedValue);
                    }

                    int SizeId = 0;
                    if (rptSizes.SelectedValue != "")
                    {
                        SizeId = int.Parse(rptSizes.SelectedValue);
                    }

                    if (updatedIssuedQty >= 0)
                    {
                        tbxUpdatedQty.Visible = true;
                        lblIssuedQty.Visible = false;
                        btnUpdate.Visible = true;
                        btnEdit.Visible = false;

                        issueInfo.Quantity = decimal.Parse(tbxUpdatedQty.Text);
                        issueInfo.UpdatedDate = DateTime.Now;
                        issueInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                        issueInfo.IssueDate = DateTime.Parse(rptIssueDate.Text);
                        issueInfo.StyleId = int.Parse(rptStyles.SelectedValue);
                        issueInfo.KnittingFactoryId = int.Parse(rptKnittingFactory.SelectedValue);
                        issueInfo.FinishingUnitId = int.Parse(rptFinishingUnit.SelectedValue);
                        issueInfo.LinkingFloorId = int.Parse(rptLinkingFloor.SelectedValue);
                        issueInfo.ColorId = int.Parse(rptColors.SelectedValue);
                        issueInfo.SizeId = int.Parse(rptSizes.SelectedValue);
                        unitOfWork.GenericRepositories<BoothIssueToFloor>().Update(issueInfo);
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                        LoadData();
                        lblAvailableBalance.Visible = false;
                    }
                    else
                    {
                        // ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('You can not issue more than available balance. Available balance is {availableBalance}')", true);
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", $"toastr.error('You can not issue more than available balance. Available balance is less than 0')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Unable to update.')", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter issue quantity.')", true);
            }

        }


        protected void SelectedIndexChanged(object sender, EventArgs e)
        {
            var ddl = (DropDownList)sender;
            var rptStyles = (DropDownList)ddl.NamingContainer.FindControl("rptStyles");
            var rptColors = (DropDownList)ddl.NamingContainer.FindControl("rptColors");
            var rptAvailableBalance = (Label)ddl.NamingContainer.FindControl("rptAvailableBalance");
        }

        

        protected void rptStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            var rptStyles = (DropDownList)sender;
            var rptColors = (DropDownList)((RepeaterItem)((DropDownList)sender).Parent).FindControl("rptColors");
            var rptSizes = (DropDownList)((RepeaterItem)((DropDownList)sender).Parent).FindControl("rptSizes");
            var rptAvailableBalance = (Label)((RepeaterItem)((DropDownList)sender).Parent).FindControl("rptAvailableBalance");
            rptColors.Items.Clear();
            rptSizes.Items.Clear();
           

            if (rptStyles.SelectedValue != "")
            {
                var styleId = int.Parse(rptStyles.SelectedValue);
                LoadColorDropdown(styleId, rptColors);
                LoadSizesDropdown(styleId, rptSizes);
               
            }
        }

        protected void lnkbtnCancel_Command(object sender, CommandEventArgs e)
        {
            LoadData();
            lblAvailableBalance.Visible = false;
        }


    }
}