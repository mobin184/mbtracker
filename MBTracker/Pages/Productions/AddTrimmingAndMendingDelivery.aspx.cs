﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Commercial_Tasks;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class AddTrimmingAndMendingDelivery : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        CommercialManager commercialManager = new CommercialManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        DataTable dtPIItemDetails;
        decimal totalGoods = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var dtFinishigUnits = new CommonManager().GetFinishingUnitsByUser(CommonMethods.SessionInfo.UserId);
                ddlFinishingUnit.DataTextField = "UnitName";
                ddlFinishingUnit.DataValueField = "FinishingUnitId";
                ddlFinishingUnit.DataSource = dtFinishigUnits;
                ddlFinishingUnit.DataBind();

                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                CommonMethods.LoadDropdown(ddlLinkingFloor, "FinishingUnitFloors", 1, 0);
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteOperation();
                }
            }
        }

        int Id
        {
            set { ViewState["Id"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["Id"]);
                }
                catch
                {
                    return 0;
                }
            }

        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
            }

        }

        protected void CalculatePrices(object sender, EventArgs e)
        {
            TextBox tbxSender = (TextBox)sender;

            TextBox tbxQuantity = (TextBox)((RepeaterItem)tbxSender.NamingContainer).FindControl("tbxQuantity");

            if (tbxQuantity.Text != "")
            {
                decimal quantity = Convert.ToDecimal(tbxQuantity.Text);
            }

            CalculateGrandTotal();

        }

        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("ColorId", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("UsedNumberOfMC", typeof(string)));
            dr = dt.NewRow();
            dr["ColorId"] = string.Empty;
            dr["Quantity"] = string.Empty;
            dr["UsedNumberOfMC"] = string.Empty;
            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptTrimmingAndMendingDeliveryEntryInfo.DataSource = dt;
            rptTrimmingAndMendingDeliveryEntryInfo.DataBind();

            CalculateGrandTotal();
        }

        protected void rptTrimmingAndMendingDeliveryEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var qty = DataBinder.Eval(e.Item.DataItem, "Quantity").ToString();
                var UsedNumberOfMC = DataBinder.Eval(e.Item.DataItem, "UsedNumberOfMC").ToString();
                var ddlColors = (DropDownList)e.Item.FindControl("ddlColors");
                var tbxQuantity = (TextBox)e.Item.FindControl("tbxQuantity");
                //var tbxUsedNumberOfMC = (TextBox)e.Item.FindControl("tbxUsedNumberOfMC");
                if (ddlStyles.SelectedValue != "")
                {
                    SetColorDropdown(ddlColors, int.Parse(ddlStyles.SelectedValue));
                }

                tbxQuantity.Text = qty.ToString();
                //tbxUsedNumberOfMC.Text = UsedNumberOfMC.ToString();

                if (qty != "")
                {
                    totalGoods += decimal.Parse(qty);
                }

            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {

            }
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {
                        DropDownList ddlColors = (DropDownList)this.rptTrimmingAndMendingDeliveryEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        TextBox tbxQuantity = (TextBox)this.rptTrimmingAndMendingDeliveryEntryInfo.Items[rowIndex].FindControl("tbxQuantity");
                        //TextBox tbxUsedNumberOfMC = (TextBox)this.rptTrimmingAndMendingDeliveryEntryInfo.Items[rowIndex].FindControl("tbxUsedNumberOfMC");
                        dtCurrentTable.Rows[rowIndex]["ColorId"] = ddlColors.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["Quantity"] = tbxQuantity.Text;
                        //dtCurrentTable.Rows[rowIndex]["UsedNumberOfMC"] = tbxUsedNumberOfMC.Text;

                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["ColorId"] = string.Empty;
                            drCurrentRow["Quantity"] = string.Empty;
                            //drCurrentRow["UsedNumberOfMC"] = string.Empty;
                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;
                    this.rptTrimmingAndMendingDeliveryEntryInfo.DataSource = dtCurrentTable;
                    this.rptTrimmingAndMendingDeliveryEntryInfo.DataBind();
                }
            }
            this.SetPreviousData();
            CalculateGrandTotal();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DropDownList ddlColors = (DropDownList)this.rptTrimmingAndMendingDeliveryEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        TextBox tbxQuantity = (TextBox)this.rptTrimmingAndMendingDeliveryEntryInfo.Items[rowIndex].FindControl("tbxQuantity");
                        //TextBox tbxUsedNumberOfMC = (TextBox)this.rptTrimmingAndMendingDeliveryEntryInfo.Items[rowIndex].FindControl("tbxUsedNumberOfMC");
                        SetColorDropdown(ddlColors, int.Parse(ddlStyles.SelectedValue));
                        ddlColors.SelectedIndex = CommonMethods.MatchDropDownItem(ddlColors, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ColorId"].ToString()) ? "0" : dt.Rows[i]["ColorId"].ToString())));
                        tbxQuantity.Text = dt.Rows[i]["Quantity"].ToString();
                        //tbxUsedNumberOfMC.Text = dt.Rows[i]["UsedNumberOfMC"].ToString();
                        rowIndex++;
                    }
                }
            }
        }

        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a Style.')", true);
            }
            else if (tbxTrimingMendingDeliveryDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter a Trimming & Mending Date.')", true);
            }
            else if (ddlFinishingUnit.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a Finishing Unit.')", true);
            }
            else if (ddlLinkingFloor.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a Linking Floor.')", true);
            }
            else if (!CheckItems())
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter item(s) info correctly.')", true);
            }
            else
            {
                try
                {
                    List<TrimmingMendingDeliveryInfo> listTrimmingMendingDeliveryItems = new List<TrimmingMendingDeliveryInfo>();

                    for (int rowIndex = 0; rowIndex < rptTrimmingAndMendingDeliveryEntryInfo.Items.Count; rowIndex++)
                    {
                        DropDownList ddlColors = (DropDownList)this.rptTrimmingAndMendingDeliveryEntryInfo.Items[rowIndex].FindControl("ddlColors");
                        TextBox tbxQuantity = (TextBox)this.rptTrimmingAndMendingDeliveryEntryInfo.Items[rowIndex].FindControl("tbxQuantity");
                        //TextBox tbxUsedNumberOfMC = (TextBox)this.rptTrimmingAndMendingDeliveryEntryInfo.Items[rowIndex].FindControl("tbxUsedNumberOfMC");
                        // TextBox tbxRemarks = (TextBox)this.rptLinkingProductionEntryInfo.Items[rowIndex].FindControl("tbxRemarks");

                        // Check if all controls are found and not null
                        if (ddlColors != null && tbxQuantity != null)
                        {
                            if (ddlColors.SelectedValue != "" && !string.IsNullOrEmpty(tbxQuantity.Text))
                            {
                                var BuyerId = int.Parse(ddlBuyers.SelectedValue);
                                var StyleId = int.Parse(ddlStyles.SelectedValue);
                                var FinishingUnitId = int.Parse(ddlFinishingUnit.SelectedValue);
                                var LinkingFloorId = int.Parse(ddlLinkingFloor.SelectedValue);
                                var TrimmingMendingDeliveryDate = DateTime.Parse(tbxTrimingMendingDeliveryDate.Text);
                                //var Remarks = tbxDescription.Text;
                                var item = new TrimmingMendingDeliveryInfo()
                                {
                                    TrimmingMendingDeliveryDate = TrimmingMendingDeliveryDate,
                                    BuyerId = BuyerId,
                                    StyleId = StyleId,
                                    FinishingUnitId = FinishingUnitId,
                                    LinkingFloorId = LinkingFloorId,
                                    ColorId = int.Parse(ddlColors.SelectedValue),
                                    Quantity = Convert.ToDecimal(tbxQuantity.Text),
                                    //UsedNumberOfMC = Convert.ToInt32(Convert.ToDecimal(tbxUsedNumberOfMC.Text)),
                                    Remarks = tbxDescription.Text,
                                    CreateDate = DateTime.Now,
                                    CreatedBy = CommonMethods.SessionInfo.UserName
                                };

                                listTrimmingMendingDeliveryItems.Add(item);  // Add the item to the list
                            }
                        }
                        else
                        {
                            // Handle the case where a control is not found (optional)
                            // Log the error or display a message
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('One or more controls not found.')", true);
                            return;
                        }
                    }

                    if (listTrimmingMendingDeliveryItems.Count > 0)
                    {
                        foreach (var item in listTrimmingMendingDeliveryItems)
                        {
                            unitOfWork.GenericRepositories<TrimmingMendingDeliveryInfo>().Insert(item);
                        }

                        unitOfWork.Save();

                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('AddTrimmingAndMendingDelivery.aspx');", true);
                    }

                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please add Color Wise Quantity.')", true);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }

        }

        private bool CheckItems()
        {
            bool hasItem = false;

            for (int rowIndex = 0; rowIndex < rptTrimmingAndMendingDeliveryEntryInfo.Items.Count; rowIndex++)
            {
                DropDownList ddlColors = (DropDownList)this.rptTrimmingAndMendingDeliveryEntryInfo.Items[rowIndex].FindControl("ddlColors");
                TextBox tbxQuantity = (TextBox)this.rptTrimmingAndMendingDeliveryEntryInfo.Items[rowIndex].FindControl("tbxQuantity");
                //TextBox tbxUsedNumberOfMC = (TextBox)this.rptTrimmingAndMendingDeliveryEntryInfo.Items[rowIndex].FindControl("tbxUsedNumberOfMC");

                try
                {
                    decimal quantity = Convert.ToDecimal(tbxQuantity.Text);
                    //decimal UsedNumberOfMC = Convert.ToInt32(tbxUsedNumberOfMC.Text);

                    if (quantity > decimal.Parse("0.00") && ddlColors.SelectedValue != "")
                    {
                        hasItem = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    hasItem = false;
                    break;
                }
            }

            return hasItem;
        }

        private void CreateDataTableForViewState()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("StyleId", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            //dt.Columns.Add(new DataColumn("UsedNumberOfMC", typeof(string)));
            for (int rowIndex = 0; rowIndex < dtPIItemDetails.Rows.Count; rowIndex++)
            {
                dr = dt.NewRow();
                dr["StyleId"] = dtPIItemDetails.Rows[rowIndex]["StyleId"];
                dr["ItemId"] = dtPIItemDetails.Rows[rowIndex]["ItemId"];
                dr["Quantity"] = dtPIItemDetails.Rows[rowIndex]["Quantity"];
                //dr["UsedNumberOfMC"] = dtPIItemDetails.Rows[rowIndex]["UsedNumberOfMC"];
                dr["TotalAmount"] = dtPIItemDetails.Rows[rowIndex]["TotalAmount"];
                dt.Rows.Add(dr);
            }

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {

            DropDownList ddlStyles = (DropDownList)sender;
            if (ddlStyles.SelectedValue != "")
            {
                divPIOtherInfo.Visible = false;

                if (ddlBuyers.SelectedValue != "" && ddlStyles.SelectedValue != "")
                {
                    SetInitialRowCount();
                    divPIOtherInfo.Visible = true;
                }

            }
            else
            {
                //ddlOrders.Items.Clear();
            }

        }

        private void CalculateGrandTotal()
        {
            decimal grandQtyTotal = 0;
            // decimal grandDollarTotal = 0;

            for (int i = 0; i < this.rptTrimmingAndMendingDeliveryEntryInfo.Items.Count; i++)
            {
                TextBox tbxQuantity = (TextBox)this.rptTrimmingAndMendingDeliveryEntryInfo.Items[i].FindControl("tbxQuantity");

                if (tbxQuantity.Text != "")
                {
                    decimal quantity = Convert.ToDecimal(tbxQuantity.Text);
                    grandQtyTotal = grandQtyTotal + quantity;
                }

            }

            Label lblTotalQuantity = (Label)rptTrimmingAndMendingDeliveryEntryInfo.Controls[rptTrimmingAndMendingDeliveryEntryInfo.Controls.Count - 1].Controls[0].FindControl("lblTotalQuantity");

            lblTotalQuantity.Text = Math.Round(grandQtyTotal, 2).ToString();
        }

        int RowId
        {
            set { ViewState["rowId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["rowId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void btnRemove_Command(object sender, CommandEventArgs e)
        {
            try
            {
                RowId = Convert.ToInt32(e.CommandArgument.ToString());
                var IsDeletable = true;
                if (IsDeletable)
                {
                    var title = "Warning";
                    var msg = "Are you sure you want to Delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                }
                else
                {
                    var title = "Warning";
                    var msg = "Sorry, you can not delete it.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }

        }

        private void DeleteOperation()
        {
            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                //DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    dtCurrentTable.Rows.RemoveAt(RowId);
                    this.ViewState["CurrentTable"] = dtCurrentTable;
                    this.rptTrimmingAndMendingDeliveryEntryInfo.DataSource = dtCurrentTable;
                    this.rptTrimmingAndMendingDeliveryEntryInfo.DataBind();
                }
            }
            this.SetPreviousData();
            CalculateGrandTotal();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Row deleted successfully.');", true);
        }

        protected void SetColorDropdown(DropDownList ddlColor, int styleId)
        {
            var dtColors = unitOfWork.GetDataTableFromSql($"EXEC usp_GetStyleColorsByStyleId {styleId}");
            ddlColor.Items.Add(new ListItem("", ""));
            ddlColor.DataSource = dtColors;
            ddlColor.DataTextField = "ColorDescription";
            ddlColor.DataValueField = "BuyerColorId";
            ddlColor.DataBind();
            ddlColor.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlColor.SelectedIndex = 0;
        }


    }
}