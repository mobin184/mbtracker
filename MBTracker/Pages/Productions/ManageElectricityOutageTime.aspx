﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ManageElectricityOutageTime.aspx.cs" Inherits="MBTracker.Pages.Productions.ManageElectricityOutageTime" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid">
        <div class="span5">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="styleActionTitle" Text="Manage Electricity Outage Time:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 20px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="lbl1" runat="server" Text="Select Date"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxDate" runat="server" placeholder="Enter Outage date" CssClass="form-control" Width="250" TextMode="Date" AutoPostBack="true" OnTextChanged="tbxDate_OnTextChanged"></asp:TextBox>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxDate"><span style="font-weight: 700; color: #CC0000">Please select a date.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Unit:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlUserUnits" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlUserUnits_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlUserUnits"><span style="font-weight: 700; color: #CC0000">Please select a unit.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label8" runat="server" Text="Select Shift:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlShifts" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control" OnSelectedIndexChanged="ddlShifts_SelectedIndexChanged"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlShifts"><span style="font-weight: 700; color: #CC0000">Please select a shift.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>



                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label1" runat="server" Text="How many times?"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxNumTimes" runat="server" placeholder="Enter Number of Outage" CssClass="form-control" Width="250"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxNumTimes"><span style="font-weight: 700; color: #CC0000">Enter # of outages</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label6" runat="server" Text="Electricity Off Time(min)"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxElectricityOffTime" runat="server" placeholder="Enter outage time in minutes" CssClass="form-control" Width="250"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxElectricityOffTime"><span style="font-weight: 700; color: #CC0000">Enter off time</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label7" runat="server" Text="Production Loss Time(min)"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxProdLossTime" runat="server" placeholder="Enter loss time in minutes" CssClass="form-control" Width="250"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxProdLossTime"><span style="font-weight: 700; color: #CC0000">Enter loss time</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="Remarks"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxRemarks" runat="server" placeholder="Enter remarks (Optional)" CssClass="form-control" Width="250" TextMode="MultiLine"></asp:TextBox>
                                
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="control-group">
                            <br />
                            <div class="controls-row">
                                <asp:Button ID="btnSave" runat="server" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnSave_Click" />
                                <asp:Button ID="btnUpdate" runat="server" Visible="False" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdate_Click" CssClass="btn btn-info pull-right" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="span7">
            <asp:Panel ID="pnlOutages" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-header">
                        <div class="title">
                            <span class="fs1" aria-hidden="true" data-icon=""></span>Reported Outage:
                        </div>
                    </div>
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">
                            <asp:Repeater ID="rpt" runat="server" OnItemCommand="rpt_ItemCommand">
                                <HeaderTemplate>
                                    <table id="data-table" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>

                                                <th>
                                                    <asp:Label ID="Label12" runat="server" Text="Date"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label5" runat="server" Text="Unit"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label4" runat="server" Text="Shift"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label9" runat="server" Text="How Many Times"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label10" runat="server" Text="Off Time(min)"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label11" runat="server" Text="Production Loss Time(min)"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label3" runat="server" Text="Actions"></asp:Label></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%#Eval("OutageDate") %> </td>
                                        <td><%#Eval("KnittingUnitName") %></td>
                                        <td><%#Eval("ShiftName") %></td>

                                        <td><%#Eval("NumOutageTimes") %> </td>
                                        <td><%#Eval("TotalOutageTime") %></td>
                                        <td><%#Eval("ProductionLossTime") %></td>

                                        <td>
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-small hidden-phone" Text="Delete"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                         </table>
                            <div class="clearfix">
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </asp:Panel>
        </div>
    </div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
