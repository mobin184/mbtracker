﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class DeliverKnittedPartsToFinishingUnitV2 : System.Web.UI.Page
    {


        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        DataTable dtMachineBrand = new DataTable();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // var ctrl = Tools.GetPostBackControl(this);

                var user = CommonMethods.SessionInfo;
                if (user == null)
                {
                    HttpContext.Current.Session["returnUrl"] = HttpContext.Current.Request.Url.PathAndQuery;
                    Response.Redirect("~/Login.aspx");
                }


                var dtKnittingUnit = new CommonManager().GetProductionUnitsByUser(CommonMethods.SessionInfo.UserId);
                var dtFinishingUnit = new CommonManager().LoadFinishingUnits();
                ddlUserProductionUnit.DataTextField = "UnitName";
                ddlUserProductionUnit.DataValueField = "ProductionUnitId";
                ddlUserProductionUnit.DataSource = dtKnittingUnit;
                ddlUserProductionUnit.DataBind();
                ddlFinishingUnit.DataTextField = "FinishingUnitName";
                ddlFinishingUnit.DataValueField = "FinishingUnitId";
                ddlFinishingUnit.DataSource = dtFinishingUnit;
                ddlFinishingUnit.DataBind();
                ddlFinishingUnit.Items.Insert(0, new ListItem("---Select---", string.Empty));
                ddlFinishingUnit.SelectedIndex = 0;
                tbxDelivryDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                //CommonMethods.LoadDropdown(ddlBuyers, "Buyers WHERE IsActive = 1 ORDER BY BuyerName", 1, 0);

                if (Request.QueryString["knittingDeliveredId"] != null)
                {
                    KnittingDeliveredId = int.Parse(Tools.UrlDecode(Request.QueryString["knittingDeliveredId"]));
                    var recInfo = unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnit>().GetByID(KnittingDeliveredId);
                    BindDataToUI(recInfo);
                }
            }
            else
            {
                var ctrl = Tools.GetPostBackControl(this);
                CTRLId = ctrl.ClientID;
            }

            // btnSaveEntries.Attributes.Add("onclick", "javascript:Disable()");


        }

        private void BindDataToUI(KnittedItemDelvierdToFinishingUnit recInfo)
        {
            CommonMethods.LoadDropdownById(ddlStyles, recInfo.BuyerId, "BuyerStyles", 1, 0);
            ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, recInfo.BuyerId);
            ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, recInfo.StyleId);
            ddlUserProductionUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUserProductionUnit, recInfo.KnittingUnitId);
            ddlFinishingUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlFinishingUnit, recInfo.FinishingUnitId);
            ddlBuyers.Enabled = false;
            ddlStyles.Enabled = false;
            ddlUserProductionUnit.Enabled = false;
            tbxDelivryDate.Text = recInfo.DeliveredDate.ToString("yyyy-MM-dd");
            tbxChalanNumber.Text = recInfo.ChalanNo;
            tbxGatePassNo.Text = recInfo.GatePassNo;
            tbxVehicleNo.Text = recInfo.VehicleNo;
            LoadEntryInfo(recInfo.StyleId, recInfo.BuyerId);

        }

        int KnittingDeliveredId
        {
            set { ViewState["knittingReceivedId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["knittingReceivedId"]);
                }
                catch
                {
                    return 0;
                }
            }

        }


        string CTRLId
        {
            set { ViewState["CTRLId"] = value; }
            get
            {
                try
                {
                    return ViewState["CTRLId"]?.ToString();
                }
                catch
                {
                    return "";
                }
            }

        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblStyleTotal.Text = "";

            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                //CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
                CommonMethods.LoadOnlyRunningStyleDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
            }
            divColors.Visible = false;
            rptEntryInfo.Visible = false;
            rptEntryInfo.DataSource = null;
            rptEntryInfo.DataBind();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "CacheItems()", true);
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblStyleTotal.Text = "";
            ddlFinishingUnit.SelectedIndex = 0;
            ddlFinishingUnit.Enabled = true;
            if (ddlStyles.SelectedValue != "")
            {
                LoadEntryInfo(int.Parse(ddlStyles.SelectedValue), int.Parse(ddlBuyers.SelectedValue));
                SetFinishingUnitFromKnittingEntry(int.Parse(ddlStyles.SelectedValue));
            }
            else
            {
                divColors.Visible = false;
                rptEntryInfo.Visible = false;
                rptEntryInfo.DataSource = null;
                rptEntryInfo.DataBind();
            }
            // ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "CacheItems()", true);
        }


        private void SetFinishingUnitFromKnittingEntry(int styleId)
        {
            var finishingUnitIdInKnittingEntry = unitOfWork.GenericRepositories<DailyKnittings>().Get(x => x.StyleId == styleId).OrderByDescending(x => x.KnittingDate).FirstOrDefault();
            if(finishingUnitIdInKnittingEntry != null && finishingUnitIdInKnittingEntry.FinishingUnitId > 0)
            {
                ddlFinishingUnit.SelectedValue = finishingUnitIdInKnittingEntry.FinishingUnitId + "";
                ddlFinishingUnit.Enabled = false;
            }
        }

        private void LoadEntryInfo(int styleId, int buyerId)
        {


            var unitId = int.Parse(ddlUserProductionUnit.SelectedValue);


            //ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Are you coming here?')", true);

            lblSaveMessage.Visible = false;

            var dtColors = new CommonManager().GetColorsByStyle(styleId);
            if (dtColors.Rows.Count > 0)
            {
                dtColors.Columns.Add("StyleId", typeof(int));
                dtColors.Columns.Add("BuyerId", typeof(int));
                dtColors.Columns.Add("StyleName", typeof(string));
                dtColors.Columns.Add("BuyerName", typeof(string));
                foreach (DataRow row in dtColors.Rows)
                {
                    row["StyleId"] = styleId;
                    row["BuyerId"] = buyerId;
                    row["StyleName"] = ddlStyles.SelectedItem.Text;
                    row["BuyerName"] = ddlBuyers.SelectedItem.Text;
                }

                rptEntryInfo.DataSource = dtColors;
                rptEntryInfo.DataBind();
                rptEntryInfo.Visible = true;
                lblNoDataFound.Visible = false;
                if (KnittingDeliveredId != 0)
                {
                    btnSaveEntries.Visible = false;
                    btnUpdateEntries.Visible = true;
                }
                else
                {
                    btnSaveEntries.Visible = true;
                    btnUpdateEntries.Visible = false;
                }
                divColors.Visible = true;
            }
            else
            {
                btnSaveEntries.Visible = false;
                btnUpdateEntries.Visible = false;
                lblNoDataFound.Visible = true;
                rptEntryInfo.DataSource = null;
                rptEntryInfo.DataBind();
                rptEntryInfo.Visible = false;
                divColors.Visible = false;
            }
        }


        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //var buyerId = int.Parse(ddlBuyers.SelectedValue);
                var styleId = int.Parse(ddlStyles.SelectedValue);
                var unitId = int.Parse(ddlUserProductionUnit.SelectedValue);

                var lblBuyerColorId = (Label)e.Item.FindControl("lblBuyerColorId");
                var buyerColorId = int.Parse(lblBuyerColorId.Text);

                dtMachineBrand = unitOfWork.GetDataTableFromSql($"Exec usp_GetAllMachineBrandByUnitIdStyleAndColor '{unitId}','{styleId}','{buyerColorId}'");

                //var dtSizes = buyerManager.GetSizes(buyerId);
                var dtSizes = buyerManager.GetStyleSizes(styleId);

                var ddlMachineBrand = (DropDownList)e.Item.FindControl("ddlMachineBrand");
                ddlMachineBrand.DataSource = dtMachineBrand;
                ddlMachineBrand.DataTextField = "MachineBrand";
                ddlMachineBrand.DataValueField = "MachineBrandId";
                ddlMachineBrand.DataBind();
                ddlMachineBrand.Items.Insert(0, new ListItem("---Select---", string.Empty));
                ddlMachineBrand.SelectedIndex = 0;

                GridView gvSizeQuantity1 = (GridView)e.Item.FindControl("gvSizeQuantity1");
                Label lblNoSizeFound = (Label)e.Item.FindControl("lblNoSizeFound");
                if (dtSizes.Rows.Count > 0)
                {
                    DataTable dtOneRow = new DataTable();
                    DataRow dr = null;
                    dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                    dr = dtOneRow.NewRow();
                    dr["RowNumber"] = 1;
                    dtOneRow.Rows.Add(dr);

                    //DataRow dr1 = null;
                    ////dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                    //dr1 = dtOneRow.NewRow();
                    //dr1["RowNumber"] = 2;
                    //dtOneRow.Rows.Add(dr1);

                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantity1.Columns.Add(templateField);
                    }

                    gvSizeQuantity1.DataSource = dtOneRow;
                    gvSizeQuantity1.DataBind();
                    gvSizeQuantity1.Visible = true;
                    lblNoSizeFound.Visible = false;

                }
                else
                {
                    gvSizeQuantity1.Visible = false;
                    lblNoSizeFound.Visible = true;
                }
            }
        }


        protected void gvSizeQuantity1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && CTRLId != "cpMain_btnSaveEntries" && CTRLId != "cpMain_btnUpdateEntries")
            {

                ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(btnSaveEntries);
                ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(btnUpdateEntries);

                var buyerId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId")).Text);
                var styleId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId")).Text);
                var ddlMachineBrand = (DropDownList)((GridView)sender).DataItemContainer.FindControl("ddlMachineBrand");
                var ddlGG = (DropDownList)((GridView)sender).DataItemContainer.FindControl("ddlGG");
                var lblColorAvailableBalance = (Label)((GridView)sender).DataItemContainer.FindControl("lblColorAvailableBalance");
                var unitId = 0;
                if (!string.IsNullOrEmpty(ddlUserProductionUnit.SelectedValue))
                {
                    unitId = int.Parse(ddlUserProductionUnit.SelectedValue);
                }
                //var dtSizes = buyerManager.GetSizes(buyerId);

                var dtSizes = buyerManager.GetStyleSizes(styleId);

                TextBox txtSizeQty = null;
                Label lblSizeId = null;
                var ttlAvilableBlance = 0;

                var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                var buyerColorId = int.Parse(string.IsNullOrEmpty(lblBuyerColorId.Text) ? "0" : lblBuyerColorId.Text);

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    bool ggset = false;

                    txtSizeQty = new TextBox();
                    txtSizeQty.ID = "tbxReceivQuantity" + i.ToString();
                    txtSizeQty.Text = "";
                    txtSizeQty.Width = 60;
                    txtSizeQty.TextMode = TextBoxMode.Number;
                    txtSizeQty.Attributes.Add("min", "0");
                    txtSizeQty.CssClass = "sizeQty";
                    e.Row.Cells[i].Controls.Add(txtSizeQty);

                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);

                    var entryId = new Label();
                    entryId.ID = "lblentryId" + i.ToString();
                    entryId.Text = "";
                    entryId.Visible = false;
                    e.Row.Cells[i].Controls.Add(entryId);



                    var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());
                    
                    if (buyerColorId != 0)
                    {
                        //var sizeQty = CommonMethods.GetSizeQuantityByStyleAndColor(styleId, buyerColorId, sizeId);
                        //if (sizeQty > 0)
                        //{
                        var availableQty = int.Parse(unitOfWork.GetSingleStringValue($"Exec usp_GetAvailableKnittingsQtyForDeliverToFinishingUnitByStyleColorSize '{styleId}','{buyerColorId}','{sizeId}','{unitId}'"));
                        if (KnittingDeliveredId != 0)
                        {
                            var sizeInfo = unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnitColorSizes>().Get(x => x.KnittedDeliveredId == KnittingDeliveredId && x.BuyerColorId == buyerColorId && x.SizeId == sizeId).FirstOrDefault();
                            if (sizeInfo != null)
                            {
                                availableQty += sizeInfo.DeliveredQty ?? 0;
                                entryId.Text = sizeInfo.Id + "";
                                txtSizeQty.Text = sizeInfo.DeliveredQty + "";

                                if (!ggset)
                                {
                                    ddlMachineBrand.SelectedValue = sizeInfo.MachineBrandId + "";
                                    if (!string.IsNullOrEmpty(sizeInfo.MachineBrandId+""))
                                    {
                                        LoadGaugeDropdown(ddlGG, unitId, styleId, buyerColorId, sizeInfo.MachineBrandId ?? 0);
                                        if (!string.IsNullOrEmpty(sizeInfo.GG + ""))
                                        {
                                            ddlGG.SelectedValue = sizeInfo.GG + "";
                                        }
                                    }
                                    

                                    ggset = true;
                                }
                            }
                        }

                        var avalableBalance = new Label();
                        avalableBalance.ID = "lblavalableBalance" + i.ToString();
                        avalableBalance.Text = "Bal: " + availableQty;
                        avalableBalance.CssClass = "available-balance";
                        e.Row.Cells[i].Controls.Add(avalableBalance);
                        ttlAvilableBlance += availableQty;

                        var isExist = false;

                        isExist = unitOfWork.GenericRepositories<DailyKnittings>().IsExist(x => x.StyleId == styleId && x.BuyerColorId == buyerColorId && x.SizeId == sizeId && x.ProductionQty > 0 && x.KnittingUnitId == unitId);
                        if (!isExist)
                        {
                            txtSizeQty.Enabled = false;

                        }
                        txtSizeQty.Attributes.Add("max", availableQty + "");


                        //}
                        //else
                        //{
                        //    txtSizeQty.Enabled = false;
                        //}
                    }
                }

                lblColorAvailableBalance.Text = "(Total bal: " + ttlAvilableBlance + ")";
            }
        }



        protected void btnSaveEntries_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(tbxDelivryDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter delivery date.')", true);
            }
            else if (ddlUserProductionUnit.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select the knitting unit.')", true);
            }
            else if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a style.')", true);
            }
            else if (ddlFinishingUnit.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a finishing unit.')", true);
            }
            else if (String.IsNullOrEmpty(tbxChalanNumber.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter chalan Number.')", true);
            }
            else
            {
                try
                {
                    var deliveredDate = DateTime.Parse(tbxDelivryDate.Text);

                    if (deliveredDate <= DateTime.Now && ddlBuyers.SelectedValue != "" && ddlStyles.SelectedValue != "")
                    {

                        List<KnittedItemDelvierdToFinishingUnitColorSizes> lstColorSizes = new List<KnittedItemDelvierdToFinishingUnitColorSizes>();
                        var dkr = new KnittedItemDelvierdToFinishingUnit()
                        {
                            KnittingUnitId = int.Parse(ddlUserProductionUnit.SelectedValue),
                            BuyerId = int.Parse(ddlBuyers.SelectedValue),
                            StyleId = int.Parse(ddlStyles.SelectedValue),
                            FinishingUnitId = int.Parse(ddlFinishingUnit.SelectedValue),
                            DeliveredDate = deliveredDate,
                            ChalanNo = tbxChalanNumber.Text,
                            GatePassNo = tbxGatePassNo.Text,
                            VehicleNo = tbxVehicleNo.Text,
                            DeliveredQty = 0,
                            CreateDate = DateTime.Now,
                            CreatedBy = CommonMethods.SessionInfo.UserName
                        };
                        bool machineSelected = true;
                        bool machineGGSelected = true;
                        int totalRecQty = 0;
                        for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                        {
                            var buyerColorId = int.Parse(((Label)rptEntryInfo.Items[i].FindControl("lblBuyerColorId")).Text);
                            var ddlMachineBrand = (DropDownList)rptEntryInfo.Items[i].FindControl("ddlMachineBrand");
                            var ddlGG = (DropDownList)rptEntryInfo.Items[i].FindControl("ddlGG");
                            var gvSizeQty = (GridView)rptEntryInfo.Items[i].FindControl("gvSizeQuantity1");
                            for (int j = 0; j < gvSizeQty.Columns.Count; j++)
                            {
                                var sizeId = int.Parse(((Label)gvSizeQty.Rows[0].Cells[j].FindControl("lblSizeId" + j)).Text);
                                var recQty = ((TextBox)gvSizeQty.Rows[0].Cells[j].FindControl("tbxReceivQuantity" + j)).Text;
                                if (!string.IsNullOrEmpty(recQty))
                                {
                                    if (string.IsNullOrEmpty(ddlMachineBrand.SelectedValue))
                                    {
                                        machineSelected = false;
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(ddlGG.SelectedValue))
                                        {
                                            machineGGSelected = false;
                                        }
                                        else
                                        {
                                            var colorSize = new KnittedItemDelvierdToFinishingUnitColorSizes()
                                            {
                                                BuyerColorId = buyerColorId,
                                                KnittedDeliveredId = dkr.Id,
                                                DeliveredQty = int.Parse(recQty),
                                                SizeId = sizeId,
                                                MachineBrandId = int.Parse(ddlMachineBrand.SelectedValue),
                                                GG = int.Parse(ddlGG.SelectedValue),
                                                CreateDate = DateTime.Now,
                                                CreatedBy = CommonMethods.SessionInfo.UserName
                                            };
                                            lstColorSizes.Add(colorSize);
                                            totalRecQty += int.Parse(recQty);
                                        }
                                    }
                                }
                            }
                        }

                        if (machineSelected)
                        {
                            if (machineGGSelected)
                            {
                                if (lstColorSizes.Count > 0)
                                {

                                    //btnSaveEntries.Enabled = false;

                                    dkr.DeliveredQty = totalRecQty;
                                    unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnit>().Insert(dkr);
                                    foreach (var item in lstColorSizes)
                                    {
                                        unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnitColorSizes>().Insert(item);
                                    }

                                    unitOfWork.Save();

                                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                                    //Response.AddHeader("REFRESH", "2;URL=DeliverKnittedPartsToFinishingUnitV2.aspx");
                                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('DeliverKnittedPartsToFinishingUnitV2.aspx');", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter size quantity.')", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select garments gauge.')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select machine brand.')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cannot deliver in future date')", true);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }

            }
        }

        protected void btnUpdateEntries_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(tbxDelivryDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter delivery date.')", true);
            }
            else if (ddlUserProductionUnit.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select the knitting unit.')", true);
            }
            else if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a style.')", true);
            }
            else if (ddlFinishingUnit.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a finishing unit.')", true);
            }
            else if (String.IsNullOrEmpty(tbxChalanNumber.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter chalan Number.')", true);
            }
            else
            {

                try
                {
                    var deliveredDate = DateTime.Parse(tbxDelivryDate.Text);
                    if (deliveredDate <= DateTime.Now)
                    {
                        var dkr = unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnit>().GetByID(KnittingDeliveredId);
                        List<KnittedItemDelvierdToFinishingUnitColorSizes> lstColorSizes = new List<KnittedItemDelvierdToFinishingUnitColorSizes>();

                        dkr.KnittingUnitId = int.Parse(ddlUserProductionUnit.SelectedValue);
                        dkr.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                        dkr.StyleId = int.Parse(ddlStyles.SelectedValue);
                        dkr.FinishingUnitId = int.Parse(ddlFinishingUnit.SelectedValue);
                        dkr.ChalanNo = tbxChalanNumber.Text;
                        dkr.GatePassNo = tbxGatePassNo.Text;
                        dkr.VehicleNo = tbxVehicleNo.Text;
                        dkr.DeliveredDate = deliveredDate;
                        dkr.UpdateDate = DateTime.Now;
                        dkr.UpdatedBy = CommonMethods.SessionInfo.UserName;

                        bool machineSelected = true;
                        bool machineGGSelected = true;
                        int totalRecQty = 0;
                        for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                        {
                            var buyerColorId = int.Parse(((Label)rptEntryInfo.Items[i].FindControl("lblBuyerColorId")).Text);
                            var ddlMachineBrand = (DropDownList)rptEntryInfo.Items[i].FindControl("ddlMachineBrand");
                            var ddlGG = (DropDownList)rptEntryInfo.Items[i].FindControl("ddlGG");
                            var gvSizeQty = (GridView)rptEntryInfo.Items[i].FindControl("gvSizeQuantity1");
                            for (int j = 0; j < gvSizeQty.Columns.Count; j++)
                            {
                                var sizeId = int.Parse(((Label)gvSizeQty.Rows[0].Cells[j].FindControl("lblSizeId" + j)).Text);
                                var recQty = ((TextBox)gvSizeQty.Rows[0].Cells[j].FindControl("tbxReceivQuantity" + j)).Text;
                                if (!string.IsNullOrEmpty(recQty))
                                {
                                    if (string.IsNullOrEmpty(ddlMachineBrand.SelectedValue))
                                    {
                                        machineSelected = false;
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(ddlGG.SelectedValue))
                                        {
                                            machineGGSelected = false;
                                        }
                                        else
                                        {
                                            var colorSize = new KnittedItemDelvierdToFinishingUnitColorSizes()
                                            {
                                                BuyerColorId = buyerColorId,
                                                KnittedDeliveredId = dkr.Id,
                                                DeliveredQty = int.Parse(recQty),
                                                MachineBrandId = int.Parse(ddlMachineBrand.SelectedValue),
                                                GG = int.Parse(ddlGG.SelectedValue),

                                                SizeId = sizeId,
                                                CreateDate = DateTime.Now,
                                                CreatedBy = CommonMethods.SessionInfo.UserName
                                            };
                                            lstColorSizes.Add(colorSize);
                                            totalRecQty += int.Parse(recQty);
                                        }

                                    }
                                }
                            }
                        }
                        if (machineSelected)
                        {
                            if (machineGGSelected)
                            {
                                if (lstColorSizes.Count > 0)
                                {

                                    btnUpdateEntries.Enabled = false;

                                    dkr.DeliveredQty = totalRecQty;
                                    unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnit>().Update(dkr);
                                    var previousColors = unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnitColorSizes>().Get(x => x.KnittedDeliveredId == KnittingDeliveredId).ToList();
                                    foreach (var item in previousColors)
                                    {
                                        unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnitColorSizes>().Delete(item);
                                    }
                                    foreach (var item in lstColorSizes)
                                    {
                                        unitOfWork.GenericRepositories<KnittedItemDelvierdToFinishingUnitColorSizes>().Insert(item);
                                    }
                                    unitOfWork.Save();

                                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                                    //Response.AddHeader("REFRESH", "2;URL=ViewDeliveredKnittedPartsToFinishingUnitV2.aspx");
                                    //Response.Redirect("ViewDeliveredKnittedPartsToFinishingUnitV2.aspx");
                                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewDeliveredKnittedPartsToFinishingUnitV2.aspx');", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter size quantity!')", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select garments gauge!')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select machine brand!')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cant deliver in future date')", true);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
        }

        protected void lnkbtnCalculateColorTotal_Click(object sender, EventArgs e)
        {


            int colorTotal = 0;
            int styleTotal = 0;

            for (int i = 0; i < this.rptEntryInfo.Items.Count; i++)
            {

                Label lblColorTotalValue = (Label)this.rptEntryInfo.Items[i].FindControl("lblColorTotalValue");

                GridView gvSizeQuantity1 = (GridView)this.rptEntryInfo.Items[i].FindControl("gvSizeQuantity1");
                colorTotal = 0;

                for (int j = 0; j < gvSizeQuantity1.Columns.Count; j++)
                {
                    //string sizeIdLabel2Id = string.Concat("lblSizeId", j.ToString());
                    string sizeQuantityTextBox2Id = string.Concat("tbxReceivQuantity", j.ToString());
                    //Label sizeId = (Label)gvSizeQuantity1.Rows[0].Cells[j].FindControl(sizeIdLabel2Id);

                    TextBox tbxSizeQty = (TextBox)gvSizeQuantity1.Rows[0].Cells[j].FindControl(sizeQuantityTextBox2Id);

                    if (tbxSizeQty.Text != "")
                    {
                        colorTotal += int.Parse(tbxSizeQty.Text);
                    }

                }
                lblColorTotalValue.Text = colorTotal.ToString();
                styleTotal += colorTotal;
            }

            lblStyleTotal.Text = "Style Total: " + styleTotal.ToString();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "CacheItems()", true);

        }

        protected void ddlUserProductionUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlStyles.SelectedValue != "" && ddlBuyers.SelectedValue != "")
            {

                LoadEntryInfo(int.Parse(ddlStyles.SelectedValue), int.Parse(ddlBuyers.SelectedValue));
            }
        }

        protected void ddlMachineBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CTRLId != "cpMain_btnSaveEntries" && CTRLId != "cpMain_btnUpdateEntries")
            {


                var ddlMachineBrand = (DropDownList)sender;
                var ddlGG = (DropDownList)((DropDownList)sender).DataItemContainer.FindControl("ddlGG");
                var buyerColorId = int.Parse(((Label)((DropDownList)sender).DataItemContainer.FindControl("lblBuyerColorId")).Text);
                var styleId = int.Parse(((Label)((DropDownList)sender).DataItemContainer.FindControl("lblStyleId")).Text);
                var unitId = int.Parse(ddlUserProductionUnit.SelectedValue);
                ddlGG.Items.Clear();
                if (!string.IsNullOrEmpty(ddlMachineBrand.SelectedValue))
                {
                    var mcBrandId = int.Parse(ddlMachineBrand.SelectedValue);
                    LoadGaugeDropdown(ddlGG, unitId, styleId, buyerColorId, mcBrandId);
                }
            }
        }

        protected void LoadGaugeDropdown(DropDownList ddlGG, int unitId, int styleId, int buyerColorId, int machineBrandId)
        {
            //ddlGG.Items.Clear();
            var lstGG = unitOfWork.GenericRepositories<DailyKnittings>().Get(x => x.KnittingUnitId == unitId && x.StyleId == styleId && x.BuyerColorId == buyerColorId && x.MachineBrandId == machineBrandId).Select(x => new
            {
                MachineGauge = x.MachineGauge
            }).Distinct().ToList();
            ddlGG.DataSource = lstGG;
            ddlGG.DataTextField = "MachineGauge";
            ddlGG.DataValueField = "MachineGauge";
            ddlGG.SelectedIndex = -1;
            ddlGG.DataBind();
            ddlGG.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlGG.SelectedIndex = 0;
        }
    }
}

