﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ReportRejection.aspx.cs" Inherits="MBTracker.Pages.Productions.ReportRejection" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table th {
            text-align: center;
        }

        .form-horizontal .control-label {
            width: 140px;
        }

        .form-horizontal .controls {
            margin-left: 140px;
            padding-left: 10px;
        }
    </style>

    <div class="row-fluid" id="divBuyerAndStyle" runat="server">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="Report Rejection:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row" style="padding-left: 5px;">
                        <div class="form-horizontal">
                            <div style="text-align: right; font-size: 12px; line-height: 10px; padding-right: 18px; padding-bottom: 10px">
                                <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                            </div>
                            <div class="control-group">
                                <div class="col-md-12 col-sm-12">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="Label2" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>

                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="Label1" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="lblOrder" runat="server" Text="Select Order:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" CssClass="form-control" OnSelectedIndexChanged="ddlOrders_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="inputFinishingFloor" class="control-label">
                                            <asp:Label ID="lblFinishingFloor" runat="server" Text="Finishing Floor:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlFinishingFloor" runat="server" CssClass="form-control" Display="Dynamic" Width="100%"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="ddlFinishingFloor"><span style="font-weight: 700; color: #CC0000">Select finishing floor.</span></asp:RequiredFieldValidator>
                                            <%--<asp:Button ID="resetButton" runat="server" Visible="false" class="btn btn-success btn-samll pull-left btnStyle" Text="&nbsp;&nbsp;&nbsp;Reset&nbsp;&nbsp;&nbsp;" Style="margin-left: 375px" OnClick="btnReset_Click" />--%>
                                        </div>
                                    </div>

                                    <div class="pull-right" style="text-align: right; font-size: 2px; line-height: 5px; padding-bottom: 2px">
                                        <span style="font-weight: 100; padding-right: 400px"></span><span style="font-weight: 100; color: #CC0000">&nbsp;</span>
                                    </div>
                                    <div class="control-group">
                                        <label for="inputFinishingFloor" class="control-label">
                                            <span style="font-weight: 100; color: #CC0000">&nbsp;</span></label>
                                        <div class="controls controls-row">
                                            <asp:Button ID="resetButton" runat="server" Visible="false" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Reset&nbsp;&nbsp;&nbsp;" OnClick="btnReset_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">

        <div class="span9" style="margin-left: 0px" runat="server" id="pnlColorDeliveryCountryAndQuantity" visible="false">
            <div class="widget">
                <div class="widget-body">
                    <asp:Panel ID="pnl" runat="server">
                        <%--<div class="form-horizontal">--%>


                        <div class="control-group">
                            <label for="inputDeliveryCountry" class="control-label">
                                <asp:Label ID="lblMessage" runat="server" Text="Enter Rejection Information:" Font-Bold="true" BackColor="#ffff00"></asp:Label></label>
                        </div>
                        <div class="control-group">
                            <label for="inputDeliveryCountry" class="control-label"></label>
                            <div class="controls controls-row">
                                <div id="dt_example2" class="example_alt_pagination" style="overflow-x: auto">
                                    <asp:Repeater ID="rptColorAndSizes" runat="server" OnItemDataBound="rptColorAndSizes_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 200px; text-align: center">
                                                            <asp:Label ID="lblColorDesc" runat="server" Text="Color"></asp:Label></th>
                                                        <th style="width: 350px">
                                                            <asp:Label ID="lblColorDescription" runat="server" Text="Size & Quantity"></asp:Label></th>
                                                        <th style="width: 350px">
                                                            <asp:Label ID="lblNotes" runat="server" Text="Notes"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="vertical-align: middle">
                                                    <asp:Label ID="lblColorDesc" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ColorDescription")%>'></asp:Label>
                                                    <asp:Label ID="lblColorId" Visible="false" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>'></asp:Label>
                                                </td>

                                                <td>
                                                    <asp:GridView ID="gvSizeAndQty" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeAndQty_RowCreated"></asp:GridView>
                                                </td>
                                                <td style="vertical-align: middle">
                                                    <asp:TextBox ID="tbxNotes" runat="server" TextMode="MultiLine" Height="70px" Width="300px"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label12" runat="server" Text="&nbsp;" CssClass="text-left"></asp:Label></label>
                            <div class="controls controls-row">
                                <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 0px">
                                    <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                </div>
                                <asp:Button ID="btnSaveRejection" runat="server" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSave_Click" />
                                <asp:Button ID="btnUpdateRejection" runat="server" Visible="false" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdateRejection_Click" />
                            </div>
                        </div>
                        <%--</div>--%>
                    </asp:Panel>
                </div>
            </div>
        </div>

    </div>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
