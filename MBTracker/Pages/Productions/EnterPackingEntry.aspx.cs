﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class EnterPackingEntry : Page
    {
        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        DataTable dtSizes;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                tbxPackingDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                // CommonMethods.LoadDropdown(ddlBuyers, "Buyers WHERE IsActive = 1 ORDER BY BuyerName", 1, 0);
                CommonMethods.LoadFinishingFloorDropDown(ddlFinishingUnitFloor);
                if (Request.QueryString["packingId"] != null)
                {
                    PackingId = int.Parse(Tools.UrlDecode(Request.QueryString["packingId"]));
                    PopulateExFactoryInfoForUpdate(PackingId);
                    actionTitle.Text = "Update Packing Entry:";
                }
            }
        }

        private void PopulateExFactoryInfoForUpdate(int packingId)
        {
            try
            {
                var packedInfo = unitOfWork.GenericRepositories<PackedItems>().GetByID(PackingId);
                if (packedInfo != null)
                {
                    tbxPackingDate.Text = packedInfo.PackedDate.ToString("yyyy-MM-dd");
                    BindStylesByBuyer(packedInfo.BuyerId);
                    BindOrdersByStyle(packedInfo.StyleId);
                    BindShipmentByOrder(packedInfo.OrderId);
                    ddlFinishingUnitFloor.SelectedIndex = CommonMethods.MatchDropDownItem(ddlFinishingUnitFloor, packedInfo.FinishingUnitFloorId);

                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, packedInfo.BuyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, packedInfo.StyleId);
                    ddlOrders.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrders, packedInfo.OrderId);
                    ddlShipment.SelectedIndex = CommonMethods.MatchDropDownItem(ddlShipment, packedInfo.ShipmentDateId);
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                    LoadColorByShipmnetId(packedInfo.ShipmentDateId);
                    pnlColorDeliveryCountryAndQuantity.Visible = true;
                    LoadSizeInfo();

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Invalid packing id.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        int PackingId
        {
            get
            {
                try
                {
                    return int.Parse(ViewState["packingId"].ToString());
                }
                catch (Exception)
                {
                    return 0;
                }
            }
            set
            {
                ViewState["packingId"] = value;
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            ddlOrders.Items.Clear();
            ddlShipment.Items.Clear();
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
                rptColorAndDeliveryCountries.DataSource = null;
                rptColorAndDeliveryCountries.DataBind();
            }
        }
        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlOrders.Items.Clear();
            ddlShipment.Items.Clear();
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            if (ddlStyles.SelectedValue != "")
            {
                BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
            }
        }

        private void BindOrdersByStyle(int styleId)
        {
            CommonMethods.LoadOrderDropdownByStyle(ddlOrders, styleId, 1, 0);
        }

        protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            ddlShipment.Items.Clear();
            if (ddlOrders.SelectedValue != "")
            {
                BindShipmentByOrder(int.Parse(ddlOrders.SelectedValue));
            }
        }

        private void BindShipmentByOrder(int orderId)
        {
            var lstShipment = unitOfWork.GenericRepositories<OrderShipmentDates>().Get(x => x.OrderId == orderId).Select(a => new
            {
                Id = a.Id.ToString(),
                ShipmentDate = a.ShipmentDate.ToString("dd-MMM-yyyy") + " (" + a.ShipmentMode + ")"
            }).ToList();
            lstShipment.Insert(0, new { Id = "", ShipmentDate = "--Select--" });
            ddlShipment.DataSource = lstShipment;
            ddlShipment.DataTextField = "ShipmentDate";
            ddlShipment.DataValueField = "Id";
            ddlShipment.DataBind();
        }

        protected void ddlShipment_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            if (ddlShipment.SelectedValue != "")
            {
                LoadSizeInfo();
                LoadColorByShipmnetId(int.Parse(ddlShipment.SelectedValue));
            }
        }

        private void LoadColorByShipmnetId(int ShipmentDateId)
        {
            try
            {
                var dtColors = unitOfWork.GetDataTableFromSql($"Exec usp_GetShipmentColorsByShipmentDateId {ShipmentDateId}");
                if (dtColors.Rows.Count > 0)
                {
                    rptColorAndDeliveryCountries.DataSource = dtColors;
                    rptColorAndDeliveryCountries.DataBind();
                    rptShipmentInformation.DataSource = dtColors;
                    rptShipmentInformation.DataBind();
                    pnlColorDeliveryCountryAndQuantity.Visible = true;
                    lblColorOrDeliveryCountryNotFound.Visible = false;
                }
                else
                {
                    rptColorAndDeliveryCountries.DataSource = null;
                    rptColorAndDeliveryCountries.DataBind();
                    rptShipmentInformation.DataSource = null;
                    rptShipmentInformation.DataBind();
                    pnlColorDeliveryCountryAndQuantity.Visible = false;
                    lblColorOrDeliveryCountryNotFound.Text = "No color found for shipment.";
                    lblColorOrDeliveryCountryNotFound.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void LoadSizeInfo()
        {
            dtSizes = buyerManager.GetSizes(Convert.ToInt32(ddlBuyers.SelectedValue));
        }
        protected void rptColorAndDeliveryCountries_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //var shipmnetId = int.Parse(ddlShipment.SelectedValue);
                var rptDeliveryCountryAndQty = (Repeater)e.Item.FindControl("rptDeliveryCountryAndQty");
                //var gvAvailableSizeAndQty = (GridView)e.Item.FindControl("gvAvailableSizeAndQty");
                ((Label)e.Item.FindControl("lblStyleId")).Text = ddlStyles.SelectedValue;
                var buyerColorId = int.Parse(DataBinder.Eval(e.Item.DataItem, "BuyerColorId").ToString());
                var shipmentDateId = int.Parse(DataBinder.Eval(e.Item.DataItem, "ShipmentDateId").ToString());
                var dtDeliveryCountries = unitOfWork.GetDataTableFromSql($"EXEC usp_GetDeliveryCountryByShipmentDateIdAndColorId {shipmentDateId},{buyerColorId}");
                rptDeliveryCountryAndQty.DataSource = dtDeliveryCountries;
                rptDeliveryCountryAndQty.DataBind();

                //DataTable dtOneRow = new DataTable();
                //DataRow dr = null;
                //dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                //dr = dtOneRow.NewRow();
                //dr["RowNumber"] = 1;
                //dtOneRow.Rows.Add(dr);

                //if (dtSizes.Rows.Count > 0)
                //{
                //    TemplateField templateField = null;
                //    for (int i = 0; i < dtSizes.Rows.Count; i++)
                //    {
                //        templateField = new TemplateField();
                //        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                //        //gvAvailableSizeAndQty.Columns.Add(templateField);
                //    }
                //}
                //gvAvailableSizeAndQty.DataSource = dtOneRow;
                //gvAvailableSizeAndQty.DataBind();
            }
        }

        protected void gvSizeAndQty_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label txtSizeQty = null;
                Label lblSizeId = null;
                LoadSizeInfo();

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new Label();
                    txtSizeQty.ID = "txtSizeQty1" + i.ToString();

                    var sizeId = Convert.ToInt32(dtSizes.Rows[i][0].ToString());
                    var lblDeliveryCountryId = (Label)((GridView)sender).DataItemContainer.FindControl("lblDeliveryCountryId");
                    var deleveryCountryId = Convert.ToInt32(lblDeliveryCountryId.Text);

                    var lblColorId = (Label)((GridView)sender).DataItemContainer.NamingContainer.NamingContainer.FindControl("lblColorId");
                    var lblShipmentId = (Label)((GridView)sender).DataItemContainer.NamingContainer.NamingContainer.FindControl("lblShipmentDateId");
                    var shipmentId = int.Parse(lblShipmentId.Text);
                    var colorId = Convert.ToInt32(lblColorId.Text);
                    var shipmentColorCountrySize = unitOfWork.GenericRepositories<ShipmentColorCountrySizes>().Get(x => x.BuyerColorId == colorId && x.OrderDeliveryCountryId == deleveryCountryId && x.SizeId == sizeId && x.OrderShipmentDateId == shipmentId).FirstOrDefault();

                    if (shipmentColorCountrySize != null)
                    {
                        txtSizeQty.Text = shipmentColorCountrySize.SizeQuantity > 0 ? shipmentColorCountrySize.SizeQuantity + "" : "";
                        var lblShipmentColorCountrySize = new Label();
                        lblShipmentColorCountrySize.ID = "lblShipmentColorCountrySize1" + i.ToString();
                        lblShipmentColorCountrySize.Text = shipmentColorCountrySize.Id + "";
                        lblShipmentColorCountrySize.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblShipmentColorCountrySize);
                    }
                    else
                    {
                        txtSizeQty.Text = "";
                        var lblShipmentColorCountrySize = new Label();
                        lblShipmentColorCountrySize.ID = "lblShipmentColorCountrySize1" + i.ToString();
                        lblShipmentColorCountrySize.Text = "";
                        lblShipmentColorCountrySize.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblShipmentColorCountrySize);
                    }


                    txtSizeQty.Width = 50;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);

                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId1" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);
                }
            }
        }

        protected void gvSizeAndQty1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtSizeQty = null;
                Label lblSizeId = null;
                LoadSizeInfo();

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new TextBox();
                    txtSizeQty.TextMode = TextBoxMode.Number;
                    txtSizeQty.Enabled = false;
                    txtSizeQty.ID = "txtSizeQty1" + i.ToString();

                    var sizeId = Convert.ToInt32(dtSizes.Rows[i][0].ToString());
                    var lblDeliveryCountryId = (Label)((GridView)sender).DataItemContainer.FindControl("lblDeliveryCountryId");
                    var deleveryCountryId = Convert.ToInt32(lblDeliveryCountryId.Text);

                    var lblColorId = (Label)((GridView)sender).DataItemContainer.NamingContainer.NamingContainer.FindControl("lblColorId");
                    var lblShipmentId = (Label)((GridView)sender).DataItemContainer.NamingContainer.NamingContainer.FindControl("lblShipmentDateId");
                    var shipmentId = int.Parse(lblShipmentId.Text);
                    var colorId = Convert.ToInt32(lblColorId.Text);
                    var shipmentColorCountrySize = unitOfWork.GenericRepositories<ShipmentColorCountrySizes>().Get(x => x.BuyerColorId == colorId && x.OrderDeliveryCountryId == deleveryCountryId && x.SizeId == sizeId && x.OrderShipmentDateId == shipmentId).FirstOrDefault();

                    if (shipmentColorCountrySize != null)
                    {
                        if (shipmentColorCountrySize.SizeQuantity > 0)
                        {
                            if (PackingId > 0)
                            {
                                var packedColorSize = unitOfWork.GenericRepositories<PackedItemsColorSizes>().Get(x => x.PackedId == PackingId && x.DeliveryCountryId == deleveryCountryId && x.BuyerColorId == colorId && x.SizeId == sizeId).FirstOrDefault();
                                if (packedColorSize != null)
                                {
                                    txtSizeQty.Text = packedColorSize.PackedQty + "";
                                }
                            }
                            //txtSizeQty.Text = shipmentColorCountrySize.SizeQuantity + "";
                            txtSizeQty.Enabled = true;
                        }

                        var lblShipmentColorCountrySize = new Label();
                        lblShipmentColorCountrySize.ID = "lblShipmentColorCountrySize1" + i.ToString();
                        lblShipmentColorCountrySize.Text = shipmentColorCountrySize.Id + "";
                        lblShipmentColorCountrySize.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblShipmentColorCountrySize);
                    }
                    else
                    {
                        txtSizeQty.Text = "";
                        var lblShipmentColorCountrySize = new Label();
                        lblShipmentColorCountrySize.ID = "lblShipmentColorCountrySize1" + i.ToString();
                        lblShipmentColorCountrySize.Text = "";
                        lblShipmentColorCountrySize.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblShipmentColorCountrySize);
                    }


                    txtSizeQty.Width = 50;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);

                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId1" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);
                }
            }
        }

        protected void rptDeliveryCountryAndQty_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //GridView gvSizeAndQty = (GridView)e.Item.FindControl("gvSizeAndQty");
                GridView gvSizeAndQty1 = (GridView)e.Item.FindControl("gvSizeAndQty1");
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);
                LoadSizeInfo();
                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        //gvSizeAndQty.Columns.Add(templateField);
                        gvSizeAndQty1.Columns.Add(templateField);

                    }

                }

                //gvSizeAndQty.DataSource = dtOneRow;
                //gvSizeAndQty.DataBind();
                gvSizeAndQty1.DataSource = dtOneRow;
                gvSizeAndQty1.DataBind();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(tbxPackingDate.Text))
                {
                    if (ddlFinishingUnitFloor.SelectedValue != "")
                    {
                        var packingDate = DateTime.Parse(tbxPackingDate.Text);
                        if (packingDate <= DateTime.Now)
                        {
                            var packedItems = new PackedItems()
                            {
                                BuyerId = int.Parse(ddlBuyers.SelectedValue),
                                StyleId = int.Parse(ddlStyles.SelectedValue),
                                ShipmentDateId = int.Parse(ddlShipment.SelectedValue),
                                FinishingUnitFloorId = int.Parse(ddlFinishingUnitFloor.SelectedValue),
                                PackedDate = packingDate,
                                OrderId = int.Parse(ddlOrders.SelectedValue),
                                TotalPackedQty = 0,
                                CreatedBy = CommonMethods.SessionInfo.UserName,
                                CreateDate = DateTime.Now
                            };

                            int totalPackedQty = 0;
                            List<PackedItemsColorSizes> lstPackedItemsColorSizes = new List<PackedItemsColorSizes>();
                            for (int i = 0; i < rptColorAndDeliveryCountries.Items.Count; i++)
                            {
                                var buyerColorId = int.Parse(((Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblColorId")).Text);
                                var shipmentDateId = int.Parse(((Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblShipmentDateId")).Text);
                                var styleId = int.Parse(((Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblStyleId")).Text);

                                var rptDeliveryCountryAndQty = (Repeater)rptColorAndDeliveryCountries.Items[i].FindControl("rptDeliveryCountryAndQty");
                                for (int j = 0; j < rptDeliveryCountryAndQty.Items.Count; j++)
                                {
                                    var gvSizeAndQty1 = (GridView)rptDeliveryCountryAndQty.Items[j].FindControl("gvSizeAndQty1");
                                    var deliveryCountryId = int.Parse(((Label)rptDeliveryCountryAndQty.Items[j].FindControl("lblDeliveryCountryId")).Text);
                                    for (int k = 0; k < gvSizeAndQty1.Columns.Count; k++)
                                    {
                                        var sizeId = int.Parse(((Label)gvSizeAndQty1.Rows[0].Cells[k].FindControl("lblSizeId1" + k)).Text);
                                        var stPackedQty = ((TextBox)gvSizeAndQty1.Rows[0].Cells[k].FindControl("txtSizeQty1" + k)).Text;

                                        if (!string.IsNullOrEmpty(stPackedQty))
                                        {
                                            var packedQty = int.Parse(stPackedQty);
                                            totalPackedQty += packedQty;
                                            var packColorSize = new PackedItemsColorSizes()
                                            {
                                                BuyerColorId = buyerColorId,
                                                SizeId = sizeId,
                                                PackedQty = packedQty,
                                                DeliveryCountryId = deliveryCountryId,
                                                CreateDate = DateTime.Now,
                                                CreatedBy = CommonMethods.SessionInfo.UserName,
                                                PackedId = packedItems.Id
                                            };
                                            lstPackedItemsColorSizes.Add(packColorSize);
                                        }
                                    }
                                }
                            }

                            packedItems.TotalPackedQty = totalPackedQty;

                            if (lstPackedItemsColorSizes.Count > 0)
                            {
                                unitOfWork.GenericRepositories<PackedItems>().Insert(packedItems);
                                foreach (var item in lstPackedItemsColorSizes)
                                {
                                    unitOfWork.GenericRepositories<PackedItemsColorSizes>().Insert(item);
                                }
                                unitOfWork.Save();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter packing quantity');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Packing data should not be future date.')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a finishing floor.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter packing date.')", true);
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(tbxPackingDate.Text))
                {

                    if (ddlFinishingUnitFloor.SelectedValue != "")
                    {
                        var packingDate = DateTime.Parse(tbxPackingDate.Text);
                        if (packingDate <= DateTime.Now)
                        {
                            var packedInfo = unitOfWork.GenericRepositories<PackedItems>().GetByID(PackingId);
                            packedInfo.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                            packedInfo.FinishingUnitFloorId = int.Parse(ddlFinishingUnitFloor.SelectedValue);
                            packedInfo.StyleId = int.Parse(ddlStyles.SelectedValue);
                            packedInfo.ShipmentDateId = int.Parse(ddlShipment.SelectedValue);
                            packedInfo.PackedDate = packingDate;
                            packedInfo.OrderId = int.Parse(ddlOrders.SelectedValue);
                            packedInfo.TotalPackedQty = 0;
                            packedInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                            packedInfo.UpdateDate = DateTime.Now;

                            int totalPackedQty = 0;
                            List<PackedItemsColorSizes> lstPackedItemsColorSizes = new List<PackedItemsColorSizes>();
                            for (int i = 0; i < rptColorAndDeliveryCountries.Items.Count; i++)
                            {
                                var buyerColorId = int.Parse(((Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblColorId")).Text);
                                var shipmentDateId = int.Parse(((Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblShipmentDateId")).Text);
                                var styleId = int.Parse(((Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblStyleId")).Text);

                                var rptDeliveryCountryAndQty = (Repeater)rptColorAndDeliveryCountries.Items[i].FindControl("rptDeliveryCountryAndQty");
                                for (int j = 0; j < rptDeliveryCountryAndQty.Items.Count; j++)
                                {
                                    var gvSizeAndQty1 = (GridView)rptDeliveryCountryAndQty.Items[j].FindControl("gvSizeAndQty1");
                                    var deliveryCountryId = int.Parse(((Label)rptDeliveryCountryAndQty.Items[j].FindControl("lblDeliveryCountryId")).Text);
                                    for (int k = 0; k < gvSizeAndQty1.Columns.Count; k++)
                                    {
                                        var sizeId = int.Parse(((Label)gvSizeAndQty1.Rows[0].Cells[k].FindControl("lblSizeId1" + k)).Text);
                                        var stPackedQty = ((TextBox)gvSizeAndQty1.Rows[0].Cells[k].FindControl("txtSizeQty1" + k)).Text;

                                        if (!string.IsNullOrEmpty(stPackedQty))
                                        {
                                            var packedQty = int.Parse(stPackedQty);
                                            totalPackedQty += packedQty;
                                            var packColorSize = new PackedItemsColorSizes()
                                            {
                                                BuyerColorId = buyerColorId,
                                                SizeId = sizeId,
                                                PackedQty = packedQty,
                                                DeliveryCountryId = deliveryCountryId,
                                                CreateDate = DateTime.Now,
                                                CreatedBy = CommonMethods.SessionInfo.UserName,
                                                PackedId = packedInfo.Id
                                            };
                                            lstPackedItemsColorSizes.Add(packColorSize);
                                        }
                                    }
                                }
                            }

                            packedInfo.TotalPackedQty = totalPackedQty;

                            if (lstPackedItemsColorSizes.Count > 0)
                            {
                                unitOfWork.GenericRepositories<PackedItems>().Update(packedInfo);
                                var oldColorSize = unitOfWork.GenericRepositories<PackedItemsColorSizes>().Get(x => x.PackedId == PackingId).ToList();
                                foreach (var item in oldColorSize)
                                {
                                    unitOfWork.GenericRepositories<PackedItemsColorSizes>().Delete(item);
                                }
                                foreach (var item in lstPackedItemsColorSizes)
                                {
                                    unitOfWork.GenericRepositories<PackedItemsColorSizes>().Insert(item);
                                }
                                unitOfWork.Save();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                                //Response.AddHeader("REFRESH", "2;URL=ViewExFactoryEntries.aspx");
                                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewPackingEntries.aspx');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter packing quantity')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Packing data should not be future date.')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a finishing floor.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter packing date.')", true);
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void gvAvailableSizeAndQty_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label txtSizeQty = null;
                Label lblSizeId = null;
                LoadSizeInfo();

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new Label();
                    txtSizeQty.ID = "txtSizeQty1" + i.ToString();

                    var sizeId = Convert.ToInt32(dtSizes.Rows[i][0].ToString());

                    var lblColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblColorId");
                    var lblStyleId = (Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId");
                    var buyerColorId = Convert.ToInt32(lblColorId.Text);
                    var styleId = Convert.ToInt32(lblStyleId.Text);
                    var availableQty = unitOfWork.GetSingleValue($"EXEC usp_GetAvailableFinishindItemQtyByStyleAndSize {styleId},{buyerColorId},{sizeId}");
                    txtSizeQty.Text = availableQty;
                    txtSizeQty.Width = 50;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);

                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId1" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);
                }
            }
        }

        protected void rptAvailableQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var gvAvailableSizeAndQty = (GridView)e.Item.FindControl("gvAvailableSizeAndQty");
                ((Label)e.Item.FindControl("lblStyleId")).Text = ddlStyles.SelectedValue;
              

                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);
                LoadSizeInfo();
                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;
                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvAvailableSizeAndQty.Columns.Add(templateField);
                    }
                }
                gvAvailableSizeAndQty.DataSource = dtOneRow;
                gvAvailableSizeAndQty.DataBind();
            }
        }

        protected void rptShipmentInformation_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //var shipmnetId = int.Parse(ddlShipment.SelectedValue);
                var rptDeliveryCountryAndQtyAvailableQty = (Repeater)e.Item.FindControl("rptDeliveryCountryAndQtyAvailableQty");
                //var gvAvailableSizeAndQty = (GridView)e.Item.FindControl("gvAvailableSizeAndQty");
                ((Label)e.Item.FindControl("lblStyleId")).Text = ddlStyles.SelectedValue;
                var buyerColorId = int.Parse(DataBinder.Eval(e.Item.DataItem, "BuyerColorId").ToString());
                var shipmentDateId = int.Parse(DataBinder.Eval(e.Item.DataItem, "ShipmentDateId").ToString());
                var dtDeliveryCountries = unitOfWork.GetDataTableFromSql($"EXEC usp_GetDeliveryCountryByShipmentDateIdAndColorId {shipmentDateId},{buyerColorId}");
                rptDeliveryCountryAndQtyAvailableQty.DataSource = dtDeliveryCountries;
                rptDeliveryCountryAndQtyAvailableQty.DataBind();                
            }
        }

        protected void rptDeliveryCountryAndQtyAvailableQty_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gvSizeAndQty = (GridView)e.Item.FindControl("gvSizeAndQty");
                //GridView gvSizeAndQty1 = (GridView)e.Item.FindControl("gvSizeAndQty1");
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);
                LoadSizeInfo();
                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeAndQty.Columns.Add(templateField);
                        //gvSizeAndQty1.Columns.Add(templateField);

                    }

                }

                gvSizeAndQty.DataSource = dtOneRow;
                gvSizeAndQty.DataBind();
                //gvSizeAndQty1.DataSource = dtOneRow;
                //gvSizeAndQty1.DataBind();
            }
        }
    }
}