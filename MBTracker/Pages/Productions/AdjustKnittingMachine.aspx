﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AdjustKnittingMachine.aspx.cs" MaintainScrollPositionOnPostback="true" Inherits="MBTracker.Pages.Productions.AdjustKnittingMachine" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 5px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        input[type="radio"], input[type="checkbox"] {
            width:16px;
            height:16px;
        }
    </style>

    <div class="row-fluid">
        <div class="span6">
            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblMachinePlanning" Text="Manage Knitting Machines:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="form-horizontal">

                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>

                        <div style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 5px">
                           <%-- <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field--%>
                        </div>


                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputStyle" class="control-label">
                                <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div style="text-align: left; font-size: 12px; line-height: 30px; padding-bottom: 5px">
                            <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span9">
            <asp:Label ID="lblNoPlanningInfo" runat="server" Visible="false" Text="<br/><br/>No planning info found." BackColor="#ffff00"></asp:Label>

            <asp:Panel ID="pnlPlanningInfo" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-body">
                        <div class="form-horizontal">
                            <div class="control-group">
                                <label for="inputEmail3" class="control-label" style="width: 110px; text-align: left">
                                    <asp:Label ID="Label1" runat="server" Text="Order Quantity:" Font-Bold="false"></asp:Label></label>
                                <div class="controls controls-row" style="margin-left: 100px;">
                                    <label for="inputEmail3" class="control-label" style="text-align: left">
                                        <asp:Label ID="lblOrderQuantity" runat="server" Style="text-align: left" Text="" Font-Bold="true"></asp:Label></label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputEmail3" class="control-label" style="width: 110px; text-align: left">
                                    <asp:Label ID="Label2" runat="server" Text="Production Stage:" Font-Bold="false"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row" style="margin-left: 100px;">
                                    <asp:DropDownList ID="ddlStage" CssClass="form-control" runat="server" Width="150px" Style="margin-top: 5px;">
                                        <asp:ListItem Text="Sample Stage" Value="SampleStage"></asp:ListItem>
                                        <asp:ListItem Text="Pre-Production" Value="Preproduction" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Size Setting" Value="SizeSetting"></asp:ListItem>
                                        <asp:ListItem Text="Planning Stage" Value="PlanningStage"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <%--  <div class="form-horizontal">--%>

                        <div class="control-group" style="padding-top: 20px;">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label5" runat="server" Text="Machine Information:" Font-Bold="true"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:Repeater ID="rptPlanInfo" OnItemDataBound="rptPlanInfo_ItemDataBound" runat="server">
                                    <HeaderTemplate>
                                        <table id="data-table1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lblSelect" runat="server" Text="Select Item"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblMachineBrand" runat="server" Text="Machine Brand"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblMachineGauge" runat="server" Text="Machine Gauge"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblNeedlePerCM" runat="server" Text="Needle Per CM"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblMachineSpeed" runat="server" Text="Machine Speed"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblKnittingTime" runat="server" Text="Knitting Time"></asp:Label></th>
                                                    <%--<th>
                                                        <asp:Label ID="lblProductionStage" runat="server" Text="Production<br/>Stage"></asp:Label></th>--%>
                                                    <%--<th>
                                                        <asp:Label ID="lblAction" runat="server" Text="Take <br/>Action"></asp:Label></th>--%>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="text-align: center; vertical-align: middle" width="10%">
                                                <asp:CheckBox runat="server" AutoPostBack="true" ID="chkSelect" OnCheckedChanged="chkSelect_CheckedChanged" />
                                            </td>
                                            <td style="text-align: center; vertical-align: middle" width="30%">
                                                <asp:Label ID="lblMCBrandId" runat="server" Visible="false" Text='<%# Eval("Id")%>'></asp:Label>
                                                <asp:Label ID="lblMCBrandName" runat="server" Width="150px" Text='<%# Eval("MachineBrand")%>'></asp:Label>
                                            </td>
                                            <td style="text-align: center" width="15%">
                                                <asp:TextBox ID="tbxKnittingMCGauge" runat="server" Enabled="false" Width="60px" Text=''></asp:TextBox>
                                            </td>
                                            <td style="text-align: center" width="15%">
                                                <asp:TextBox ID="tbxNeedlePerCM" runat="server" Enabled="false" Width="60px" Text=''></asp:TextBox>
                                            </td>
                                            <td style="text-align: center" width="15%">
                                                <asp:TextBox ID="tbxMachineSpeed" runat="server" Enabled="false" Width="60px" Text=''></asp:TextBox>
                                            </td>
                                            <td style="text-align: center" width="15%">
                                                <asp:TextBox ID="tbxKnittingTime" runat="server" Enabled="false" Width="60px" Text=''></asp:TextBox>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                            </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="control-row" style="padding-top: 0px;">
                    <asp:Button ID="btnSave" runat="server" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSave_Click" />
                    <asp:Button ID="btnUpdate" runat="server" Visible="false" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdate_Click" />
                </div>
            </asp:Panel>
        </div>
    </div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
