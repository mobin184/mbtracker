﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewKnittingEntries.aspx.cs" Inherits="MBTracker.Pages.Productions.ViewKnittingEntries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table th, .table td {
            padding: 4px;
            line-height: 10px;
            text-align: center;
            vertical-align: top;
            border-top: 1px solid #e0e0e0;
        }
    </style>

    <div class="row-fluid">
        <div class="span6">
            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="View Daily Knittings:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">

                    <div class="row-fluid" runat="server" visible="true" id="FilterDiv">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                           <%-- <span style="font-weight: 700; padding-right: 400px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field--%>
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="col-md-10" style="padding-left: 0px">

                            <div class="col-md-9" style="padding-left: 0px">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">
                                            <asp:Label ID="lblFilterDate" runat="server" Text="Select Knitting Date:"></asp:Label>
                                            <span style="font-weight: 700; color: #CC0000">*</span>
                                        </label>
                                        <div class="controls controls-row" style="margin-left: 150px">
                                            <asp:TextBox ID="tbxFilterDate" runat="server" placeholder="Enter date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxFilterDate"><span style="font-weight: 700; color: #CC0000">Please select a knitting date.</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                     <div class="control-group">
                                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">
                                            <asp:Label ID="Label8" runat="server" Text=""></asp:Label></label>
                                        <div class="controls controls-row" style="margin-left: 150px">
                                            <br />
                                             <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="View Knittings" CssClass="btn btn-info pull-left" OnClick="btnSearch_Click" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3">
                               
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row-fluid" runat="server">
        <div class="span6">
            <div class="widget" id="dataDiv" runat="server" visible="false">
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <div class="control-group">
                            <label for="inputOrder" class="control-label">
                                <asp:Label runat="server" ID="lblLabelMessage" Text="Knitting Entires:" Font-Bold="true" Visible="false"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:Repeater ID="rpt" runat="server" OnItemDataBound="rpt_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="hidden">
                                                        <asp:Label ID="Label3" runat="server" Text="UnitId"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label1" runat="server" Text="Knitting Date"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label2" runat="server" Text="Knitting Unit"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label7" runat="server" Text="Shift"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label6" runat="server" Text="Production Qty"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label16" runat="server" Text="Actions"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td class="hidden"><%#Eval("KnittingUnitId") %>></td>
                                            <td><%#String.Format("{0:dd-MMM-yyyy}",Eval("KnittingDate")) %> </td>
                                            <td><%#Eval("UnitName") %></td>
                                            <td><%#Eval("ShiftName") %></td>
                                            <td><%#Eval("Production") %></td>

                                            <td>
                                                <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete"  Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("KnittingUnitId")+","+ Eval("KnittingDate")+","+ Eval("ShiftId")%>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkbtnEdit" Visible='<%# ViewState["editEnabled"]%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("KnittingUnitId")+","+ Eval("KnittingDate")+","+ Eval("ShiftId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="View" CommandArgument='<%#Eval("KnittingUnitId")+","+ Eval("KnittingDate")+","+ Eval("ShiftId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnView_Command" Text="View Details"></asp:LinkButton>
                                                
                                            </td>

                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                         </table>
                            <div class="clearfix">
                                    </FooterTemplate>
                                </asp:Repeater>

                                
                                <asp:Label ID="lblNightTotal" runat="server" Text="" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblDayTotal" runat="server" Text="" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblGrandTotal" runat="server" Text="" Font-Bold="true"></asp:Label>

                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>

    <div class="row-fluid" runat="server" id="pnlSummary" visible="false">
        <div class="span12" style="margin-top: 10px">

            <div class="col-md-6" style="padding-left: 0px">
                <div id="dt_example" class="example_alt_pagination">

                    <div class="widget">
                        <div class="widget-body">

                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblSummary" Visible="false" runat="server" Text="" Font-Bold="true"> Unit & Production Date Info:</asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rptGeneralInfo" runat="server" OnItemDataBound="rptGeneralInfo_ItemDataBound" OnItemCreated="rptGeneralInfo_ItemCreated">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblProductionUnit" Enabled="false" runat="server" Text="Production Unit"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblProductionDate" runat="server" Text="Production Date"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Shift"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblUser" runat="server" Text="User Name"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="ddlUserUnits" runat="server" Width="100" AutoPostBack="true"></asp:DropDownList></td>

                                                <td><%#Eval("ProductionEntryDate") %><asp:TextBox ID="tbxDeliveryDate" Visible="false" Text='<%#Eval("ProductionEntryDate") %>' runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlShift" runat="server" Width="100"></asp:DropDownList></td>
                                                <td><%#Eval("UserName") %> </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                 </table>
                                    <div class="clearfix">
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>


        </div>
    </div>

    <div class="row-fluid" runat="server" id="pnlDetails" visible="false">
        <div class="span12">

            <div class="widget">
                <div class="widget-body">

                    <div id="dt_example" class="example_alt_pagination">
                        <div class="control-group">
                            <label for="inputOrder" class="control-label">
                                <asp:Label ID="lblDetails" runat="server" Visible="false" Text="Detailed Information:" Font-Bold="true"></asp:Label></label>
                            <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                <asp:Repeater ID="rptEntryInfo" runat="server" OnItemDataBound="rptEntryInfo_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table4" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lblBuyer" runat="server" Text="Buyer<br/> Names"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblStyle" runat="server" Text="Styles<br/> Names"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblColor" runat="server" Text="Color<br/> Names"></asp:Label></th>
                                                    <th class="hidden">
                                                        <asp:Label ID="lblTotalIssue" runat="server" Text="Yarn<br/> Issued"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblMCBrand" runat="server" Text="Machine<br/> Brand"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblMCGauge" runat="server" Text="Machine<br/> Gauge"></asp:Label></th>
                                                    <th class="hidden">
                                                        <asp:Label ID="Label1" runat="server" Text="Jacquard<br/> Portion"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label2" runat="server" Text="Number of <br/>MC Used"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label4" runat="server" Text="Actual Knittings by Size<br/> &nbsp;"></asp:Label></th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" Width="75" Text='<%#Eval("BuyerName") %>'></asp:Label>
                                                <asp:Label ID="lblStyleId" runat="server" Text='<%#Eval("StyleId") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblBuyerId" runat="server" Text='<%#Eval("BuyerId") %>' Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" Width="75" Text='<%#Eval("StyleName") %>'></asp:Label></td>
                                            <td><%#Eval("ColorDescription") %>
                                                <asp:Label ID="lblBuyerColorId" runat="server" Text='<%#Eval("BuyerColorId") %>' Visible="false"></asp:Label></td>

                                            <td class="hidden">
                                               <%-- <asp:GridView ID="gvSizeQuantity1" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity_RowCreated1">
                                                </asp:GridView>--%>
                                                <asp:Label ID="lblUnitId" runat="server" Text='<%#Eval("UnitId") %>' Visible="false"></asp:Label>
                                            </td>



                                            <td><%#Eval("BrandName") %>
                                                <asp:Label ID="lblMachineBrandId" runat="server" Text='<%#Eval("MachineBrandId") %>' Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblKnittingMCGauge" runat="server" Text='<%#Eval("KnittingMachineGauge") %>'></asp:Label>
                                                <asp:Label ID="lblTarget" runat="server" Text='<%#Eval("Target") %>' Visible="false"></asp:Label></td>

                                            <td class="hidden">
                                                <asp:DropDownList ID="ddlJacquardPortion" Enabled="false" runat="server" Width="100px">
                                                    <asp:ListItem Text="Complete" Value="Complete"></asp:ListItem>
                                                    <asp:ListItem Text="No Work" Value="Nowork"></asp:ListItem>
                                                    <asp:ListItem Text="Technical" Value="Technical"></asp:ListItem>
                                                    <asp:ListItem Text="Sample" Value="Sample"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>

                                            <td>
                                                <asp:TextBox ID="tbxMCUsed" Enabled="false" runat="server" Width="70px" Text='<%#Eval("NumberOfMachines") %>'></asp:TextBox></td>

                                            <td>
                                                <asp:GridView ID="gvSizeQuantity2" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity_RowCreated2">
                                                </asp:GridView>

                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                 </table>
                                    <div class="clearfix"></div>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Label ID="lblNoKnittingGoingOn" runat="server" Text="Production was not planned in this unit today." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    </div>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
    <%--  <script src="../../js/jquery.dataTables.js"></script>--%>
</asp:Content>
