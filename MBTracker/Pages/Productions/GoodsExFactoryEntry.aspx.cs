﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class GoodsExFactoryEntry : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        DataTable dtSizes;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                tbxExFactoryDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
               // CommonMethods.LoadDropdown(ddlBuyers, "Buyers WHERE IsActive = 1 ORDER BY BuyerName", 1, 0);
                CommonMethods.LoadFinishingFloorDropDown(ddlFinishingUnitFloor);
                if (Request.QueryString["exFactoryId"] != null)
                {
                    ExFactoryId = int.Parse(Request.QueryString["exFactoryId"]);
                    PopulateExFactoryInfoForUpdate(ExFactoryId);
                    actionTitle.Text = "Update Goods Ex-Factory Entry:";
                }
            }
        }

        private void PopulateExFactoryInfoForUpdate(int exFactoryId)
        {
            try
            {
                var exFactoryInfo = unitOfWork.GenericRepositories<GoodsExFactory>().GetByID(ExFactoryId);
                if (exFactoryInfo != null)
                {
                    tbxExFactoryDate.Text = exFactoryInfo.ExFactoryDate.ToString("yyyy-MM-dd");
                    BindStylesByBuyer(exFactoryInfo.BuyerId);
                    BindOrdersByStyle(exFactoryInfo.StyleId);
                    BindShipmentByOrder(exFactoryInfo.OrderId);
                    ddlFinishingUnitFloor.SelectedIndex = CommonMethods.MatchDropDownItem(ddlFinishingUnitFloor, exFactoryInfo.FinishingUnitFloorId);

                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, exFactoryInfo.BuyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, exFactoryInfo.StyleId);
                    ddlOrders.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrders, exFactoryInfo.OrderId);
                    ddlShipment.SelectedIndex = CommonMethods.MatchDropDownItem(ddlShipment, exFactoryInfo.ShipmentDateId);
                    btnSaveGoodsExFactory.Visible = false;
                    btnUpdateFoodsExFactory.Visible = true;
                    LoadColorByShipmnetId(exFactoryInfo.ShipmentDateId);
                    pnlColorDeliveryCountryAndQuantity.Visible = true;
                    LoadSizeInfo();

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Invalid ex-factory id.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        int ExFactoryId
        {
            get
            {
                try
                {
                    return int.Parse(ViewState["exFactoryId"].ToString());
                }
                catch (Exception)
                {
                    return 0;
                }
            }
            set
            {
                ViewState["exFactoryId"] = value;
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            ddlOrders.Items.Clear();
            ddlShipment.Items.Clear();
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
                rptColorAndDeliveryCountries.DataSource = null;
                rptColorAndDeliveryCountries.DataBind();
            }
        }
        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlOrders.Items.Clear();
            ddlShipment.Items.Clear();
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            if (ddlStyles.SelectedValue != "")
            {
                BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
            }
        }

        private void BindOrdersByStyle(int styleId)
        {
            CommonMethods.LoadOrderDropdownByStyle(ddlOrders, styleId, 1, 0);
        }

        protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            ddlShipment.Items.Clear();
            if (ddlOrders.SelectedValue != "")
            {
                BindShipmentByOrder(int.Parse(ddlOrders.SelectedValue));
            }
        }

        private void BindShipmentByOrder(int orderId)
        {
            var lstShipment = unitOfWork.GenericRepositories<OrderShipmentDates>().Get(x => x.OrderId == orderId).Select(a => new
            {
                Id = a.Id.ToString(),
                ShipmentDate = a.ShipmentDate.ToString("dd/MM/yyyy") + " (" + a.ShipmentMode + ")"
            }).ToList();
            lstShipment.Insert(0, new { Id = "", ShipmentDate = "--Select--" });
            ddlShipment.DataSource = lstShipment;
            ddlShipment.DataTextField = "ShipmentDate";
            ddlShipment.DataValueField = "Id";
            ddlShipment.DataBind();
        }

        protected void ddlShipment_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            if (ddlShipment.SelectedValue != "")
            {
                LoadSizeInfo();
                LoadColorByShipmnetId(int.Parse(ddlShipment.SelectedValue));
            }
        }

        private void LoadColorByShipmnetId(int ShipmentDateId)
        {
            try
            {
                var dtColors = unitOfWork.GetDataTableFromSql($"Exec usp_GetShipmentColorsByShipmentDateId {ShipmentDateId}");
                if (dtColors.Rows.Count > 0)
                {
                    rptColorAndDeliveryCountries.DataSource = dtColors;
                    rptColorAndDeliveryCountries.DataBind();
                    pnlColorDeliveryCountryAndQuantity.Visible = true;
                    lblColorOrDeliveryCountryNotFound.Visible = false;
                }
                else
                {
                    rptColorAndDeliveryCountries.DataSource = null;
                    rptColorAndDeliveryCountries.DataBind();
                    pnlColorDeliveryCountryAndQuantity.Visible = false;
                    lblColorOrDeliveryCountryNotFound.Text = "No Color found for shipment.";
                    lblColorOrDeliveryCountryNotFound.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void LoadSizeInfo()
        {
            dtSizes = buyerManager.GetSizes(Convert.ToInt32(ddlBuyers.SelectedValue));
        }
        protected void rptColorAndDeliveryCountries_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var shipmnetId = int.Parse(ddlShipment.SelectedValue);
                var rptDeliveryCountryAndQty = (Repeater)e.Item.FindControl("rptDeliveryCountryAndQty");
                var gvAvailableSizeAndQty = (GridView)e.Item.FindControl("gvAvailableSizeAndQty");
                ((Label)e.Item.FindControl("lblStyleId")).Text = ddlStyles.SelectedValue;
                var buyerColorId = int.Parse(DataBinder.Eval(e.Item.DataItem, "BuyerColorId").ToString());
                var shipmentDateId = int.Parse(DataBinder.Eval(e.Item.DataItem, "ShipmentDateId").ToString());
                var dtDeliveryCountries = unitOfWork.GetDataTableFromSql($"EXEC usp_GetDeliveryCountryByShipmentDateIdAndColorId {shipmentDateId},{buyerColorId}");
                rptDeliveryCountryAndQty.DataSource = dtDeliveryCountries;
                rptDeliveryCountryAndQty.DataBind();

                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;
                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvAvailableSizeAndQty.Columns.Add(templateField);
                    }
                }
                gvAvailableSizeAndQty.DataSource = dtOneRow;
                gvAvailableSizeAndQty.DataBind();
            }
        }

        protected void gvSizeAndQty_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label txtSizeQty = null;
                Label lblSizeId = null;
                LoadSizeInfo();

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new Label();
                    txtSizeQty.ID = "txtSizeQty1" + i.ToString();

                    var sizeId = Convert.ToInt32(dtSizes.Rows[i][0].ToString());
                    var lblDeliveryCountryId = (Label)((GridView)sender).DataItemContainer.FindControl("lblDeliveryCountryId");
                    var deleveryCountryId = Convert.ToInt32(lblDeliveryCountryId.Text);

                    var lblColorId = (Label)((GridView)sender).DataItemContainer.NamingContainer.NamingContainer.FindControl("lblColorId");
                    var lblShipmentId = (Label)((GridView)sender).DataItemContainer.NamingContainer.NamingContainer.FindControl("lblShipmentDateId");
                    var shipmentId = int.Parse(lblShipmentId.Text);
                    var colorId = Convert.ToInt32(lblColorId.Text);
                    var shipmentColorCountrySize = unitOfWork.GenericRepositories<ShipmentColorCountrySizes>().Get(x => x.BuyerColorId == colorId && x.OrderDeliveryCountryId == deleveryCountryId && x.SizeId == sizeId && x.OrderShipmentDateId == shipmentId).FirstOrDefault();

                    if (shipmentColorCountrySize != null)
                    {
                        txtSizeQty.Text = shipmentColorCountrySize.SizeQuantity + "";
                        var lblShipmentColorCountrySize = new Label();
                        lblShipmentColorCountrySize.ID = "lblShipmentColorCountrySize1" + i.ToString();
                        lblShipmentColorCountrySize.Text = shipmentColorCountrySize.Id + "";
                        lblShipmentColorCountrySize.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblShipmentColorCountrySize);
                    }
                    else
                    {
                        txtSizeQty.Text = "";
                        var lblShipmentColorCountrySize = new Label();
                        lblShipmentColorCountrySize.ID = "lblShipmentColorCountrySize1" + i.ToString();
                        lblShipmentColorCountrySize.Text = "";
                        lblShipmentColorCountrySize.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblShipmentColorCountrySize);
                    }


                    txtSizeQty.Width = 30;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);

                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId1" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);
                }
            }
        }

        protected void gvSizeAndQty1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtSizeQty = null;
                Label lblSizeId = null;
                LoadSizeInfo();

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new TextBox();
                    txtSizeQty.TextMode = TextBoxMode.Number;
                    txtSizeQty.Enabled = false;
                    txtSizeQty.ID = "txtSizeQty1" + i.ToString();

                    var sizeId = Convert.ToInt32(dtSizes.Rows[i][0].ToString());
                    var lblDeliveryCountryId = (Label)((GridView)sender).DataItemContainer.FindControl("lblDeliveryCountryId");
                    var deleveryCountryId = Convert.ToInt32(lblDeliveryCountryId.Text);

                    var lblColorId = (Label)((GridView)sender).DataItemContainer.NamingContainer.NamingContainer.FindControl("lblColorId");
                    var lblShipmentId = (Label)((GridView)sender).DataItemContainer.NamingContainer.NamingContainer.FindControl("lblShipmentDateId");
                    var shipmentId = int.Parse(lblShipmentId.Text);
                    var colorId = Convert.ToInt32(lblColorId.Text);
                    var shipmentColorCountrySize = unitOfWork.GenericRepositories<ShipmentColorCountrySizes>().Get(x => x.BuyerColorId == colorId && x.OrderDeliveryCountryId == deleveryCountryId && x.SizeId == sizeId && x.OrderShipmentDateId == shipmentId).FirstOrDefault();

                    if (shipmentColorCountrySize != null)
                    {
                        if (shipmentColorCountrySize.SizeQuantity > 0)
                        {
                            if(ExFactoryId > 0)
                            {
                                var exFactoryEntryQty = unitOfWork.GenericRepositories<GoodsExFactoryColorSizes>().Get(x => x.ExFactoryId == ExFactoryId && x.DeliveryCountryId == deleveryCountryId && x.BuyerColorId == colorId && x.SizeId == sizeId).FirstOrDefault();
                                if(exFactoryEntryQty != null)
                                {
                                    txtSizeQty.Text = exFactoryEntryQty.ExFactoryQty + "";
                                }
                            }
                            //txtSizeQty.Text = shipmentColorCountrySize.SizeQuantity + "";
                            txtSizeQty.Enabled = true;
                        }

                        var lblShipmentColorCountrySize = new Label();
                        lblShipmentColorCountrySize.ID = "lblShipmentColorCountrySize1" + i.ToString();
                        lblShipmentColorCountrySize.Text = shipmentColorCountrySize.Id + "";
                        lblShipmentColorCountrySize.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblShipmentColorCountrySize);
                    }
                    else
                    {
                        txtSizeQty.Text = "";
                        var lblShipmentColorCountrySize = new Label();
                        lblShipmentColorCountrySize.ID = "lblShipmentColorCountrySize1" + i.ToString();
                        lblShipmentColorCountrySize.Text = "";
                        lblShipmentColorCountrySize.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblShipmentColorCountrySize);
                    }


                    txtSizeQty.Width = 60;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);

                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId1" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);
                }
            }
        }

        protected void rptDeliveryCountryAndQty_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gvSizeAndQty = (GridView)e.Item.FindControl("gvSizeAndQty");
                GridView gvSizeAndQty1 = (GridView)e.Item.FindControl("gvSizeAndQty1");
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);
                LoadSizeInfo();
                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeAndQty.Columns.Add(templateField);
                        gvSizeAndQty1.Columns.Add(templateField);

                    }

                }

                gvSizeAndQty.DataSource = dtOneRow;
                gvSizeAndQty.DataBind();
                gvSizeAndQty1.DataSource = dtOneRow;
                gvSizeAndQty1.DataBind();
            }
        }

        protected void btnSaveGoodsExFactory_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(tbxExFactoryDate.Text))
                {
                    if (ddlFinishingUnitFloor.SelectedValue != "")
                    {
                        var exFactoryDate = DateTime.Parse(tbxExFactoryDate.Text);
                        if (exFactoryDate <= DateTime.Now)
                        {
                            var goodsExFactory = new GoodsExFactory()
                            {
                                BuyerId = int.Parse(ddlBuyers.SelectedValue),
                                StyleId = int.Parse(ddlStyles.SelectedValue),
                                ShipmentDateId = int.Parse(ddlShipment.SelectedValue),
                                FinishingUnitFloorId = int.Parse(ddlFinishingUnitFloor.SelectedValue),
                                ExFactoryDate = exFactoryDate,
                                OrderId = int.Parse(ddlOrders.SelectedValue),
                                TotalExFactoryQty = 0,
                                CreatedBy = CommonMethods.SessionInfo.UserName,
                                CreateDate = DateTime.Now
                            };

                            int totalExFactoryQty = 0;
                            List<GoodsExFactoryColorSizes> lstGoodsExFactory = new List<GoodsExFactoryColorSizes>();
                            for (int i = 0; i < rptColorAndDeliveryCountries.Items.Count; i++)
                            {
                                var buyerColorId = int.Parse(((Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblColorId")).Text);
                                var shipmentDateId = int.Parse(((Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblShipmentDateId")).Text);
                                var styleId = int.Parse(((Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblStyleId")).Text);

                                var rptDeliveryCountryAndQty = (Repeater)rptColorAndDeliveryCountries.Items[i].FindControl("rptDeliveryCountryAndQty");
                                for (int j = 0; j < rptDeliveryCountryAndQty.Items.Count; j++)
                                {
                                    var gvSizeAndQty1 = (GridView)rptDeliveryCountryAndQty.Items[j].FindControl("gvSizeAndQty1");
                                    var deliveryCountryId = int.Parse(((Label)rptDeliveryCountryAndQty.Items[j].FindControl("lblDeliveryCountryId")).Text);
                                    for (int k = 0; k < gvSizeAndQty1.Columns.Count; k++)
                                    {
                                        var sizeId = int.Parse(((Label)gvSizeAndQty1.Rows[0].Cells[k].FindControl("lblSizeId1" + k)).Text);
                                        var stExFactoryQty = ((TextBox)gvSizeAndQty1.Rows[0].Cells[k].FindControl("txtSizeQty1" + k)).Text;

                                        if (!string.IsNullOrEmpty(stExFactoryQty))
                                        {
                                            var exFactoryQty = int.Parse(stExFactoryQty);
                                            totalExFactoryQty += exFactoryQty;
                                            var exColorSize = new GoodsExFactoryColorSizes()
                                            {
                                                BuyerColorId = buyerColorId,
                                                SizeId = sizeId,
                                                ExFactoryQty = exFactoryQty,
                                                DeliveryCountryId = deliveryCountryId,
                                                CreateDate = DateTime.Now,
                                                CreatedBy = CommonMethods.SessionInfo.UserName,
                                                ExFactoryId = goodsExFactory.Id
                                            };
                                            lstGoodsExFactory.Add(exColorSize);
                                        }
                                    }
                                }
                            }

                            goodsExFactory.TotalExFactoryQty = totalExFactoryQty;

                            if (lstGoodsExFactory.Count > 0)
                            {
                                unitOfWork.GenericRepositories<GoodsExFactory>().Insert(goodsExFactory);
                                foreach (var item in lstGoodsExFactory)
                                {
                                    unitOfWork.GenericRepositories<GoodsExFactoryColorSizes>().Insert(item);
                                }
                                unitOfWork.Save();
                                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please enter Ex-Factory quantity')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Ex-Factory data should not be future date.')", true);
                        } 
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please select a finishing unit floor.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please enter ex-factory date.')", true);
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void btnUpdateFoodsExFactory_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(tbxExFactoryDate.Text))
                {

                    if (ddlFinishingUnitFloor.SelectedValue !="")
                    {
                        var exFactoryDate = DateTime.Parse(tbxExFactoryDate.Text);
                        if (exFactoryDate <= DateTime.Now)
                        {
                            var goodsExFactory = unitOfWork.GenericRepositories<GoodsExFactory>().GetByID(ExFactoryId);
                            goodsExFactory.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                            goodsExFactory.FinishingUnitFloorId = int.Parse(ddlFinishingUnitFloor.SelectedValue);
                            goodsExFactory.StyleId = int.Parse(ddlStyles.SelectedValue);
                            goodsExFactory.ShipmentDateId = int.Parse(ddlShipment.SelectedValue);
                            goodsExFactory.ExFactoryDate = exFactoryDate;
                            goodsExFactory.OrderId = int.Parse(ddlOrders.SelectedValue);
                            goodsExFactory.TotalExFactoryQty = 0;
                            goodsExFactory.UpdatedBy = CommonMethods.SessionInfo.UserName;
                            goodsExFactory.UpdateDate = DateTime.Now;

                            int totalExFactoryQty = 0;
                            List<GoodsExFactoryColorSizes> lstGoodsExFactory = new List<GoodsExFactoryColorSizes>();
                            for (int i = 0; i < rptColorAndDeliveryCountries.Items.Count; i++)
                            {
                                var buyerColorId = int.Parse(((Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblColorId")).Text);
                                var shipmentDateId = int.Parse(((Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblShipmentDateId")).Text);
                                var styleId = int.Parse(((Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblStyleId")).Text);

                                var rptDeliveryCountryAndQty = (Repeater)rptColorAndDeliveryCountries.Items[i].FindControl("rptDeliveryCountryAndQty");
                                for (int j = 0; j < rptDeliveryCountryAndQty.Items.Count; j++)
                                {
                                    var gvSizeAndQty1 = (GridView)rptDeliveryCountryAndQty.Items[j].FindControl("gvSizeAndQty1");
                                    var deliveryCountryId = int.Parse(((Label)rptDeliveryCountryAndQty.Items[j].FindControl("lblDeliveryCountryId")).Text);
                                    for (int k = 0; k < gvSizeAndQty1.Columns.Count; k++)
                                    {
                                        var sizeId = int.Parse(((Label)gvSizeAndQty1.Rows[0].Cells[k].FindControl("lblSizeId1" + k)).Text);
                                        var stExFactoryQty = ((TextBox)gvSizeAndQty1.Rows[0].Cells[k].FindControl("txtSizeQty1" + k)).Text;

                                        if (!string.IsNullOrEmpty(stExFactoryQty))
                                        {
                                            var exFactoryQty = int.Parse(stExFactoryQty);
                                            totalExFactoryQty += exFactoryQty;
                                            var exColorSize = new GoodsExFactoryColorSizes()
                                            {
                                                BuyerColorId = buyerColorId,
                                                SizeId = sizeId,
                                                ExFactoryQty = exFactoryQty,
                                                DeliveryCountryId = deliveryCountryId,
                                                CreateDate = DateTime.Now,
                                                CreatedBy = CommonMethods.SessionInfo.UserName,
                                                ExFactoryId = goodsExFactory.Id
                                            };
                                            lstGoodsExFactory.Add(exColorSize);
                                        }
                                    }
                                }
                            }

                            goodsExFactory.TotalExFactoryQty = totalExFactoryQty;

                            if (lstGoodsExFactory.Count > 0)
                            {
                                unitOfWork.GenericRepositories<GoodsExFactory>().Update(goodsExFactory);
                                var oldColorSize = unitOfWork.GenericRepositories<GoodsExFactoryColorSizes>().Get(x => x.ExFactoryId == ExFactoryId).ToList();
                                foreach (var item in oldColorSize)
                                {
                                    unitOfWork.GenericRepositories<GoodsExFactoryColorSizes>().Delete(item);
                                }
                                foreach (var item in lstGoodsExFactory)
                                {
                                    unitOfWork.GenericRepositories<GoodsExFactoryColorSizes>().Insert(item);
                                }
                                unitOfWork.Save();
                                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                                //Response.AddHeader("REFRESH", "2;URL=ViewExFactoryEntries.aspx");
                                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewExFactoryEntries.aspx');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please enter Ex-Factory quantity')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Ex-Factory data should not be future date.')", true);
                        } 
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please select a finishing unit floor.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please enter ex-factory date.')", true);
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void gvAvailableSizeAndQty_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label txtSizeQty = null;
                Label lblSizeId = null;
                LoadSizeInfo();

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new Label();
                    txtSizeQty.ID = "txtSizeQty1" + i.ToString();

                    var sizeId = Convert.ToInt32(dtSizes.Rows[i][0].ToString());

                    var lblColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblColorId");
                    var lblStyleId = (Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId");
                    var buyerColorId = Convert.ToInt32(lblColorId.Text);
                    var styleId = Convert.ToInt32(lblStyleId.Text);
                    var availableQty = unitOfWork.GetSingleValue($"EXEC usp_GetAvailableFinishindItemQtyByStyleAndSize {styleId},{buyerColorId},{sizeId}");
                    txtSizeQty.Text = availableQty;
                    txtSizeQty.Width = 40;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);

                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId1" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);
                }
            }
        }
    }
}