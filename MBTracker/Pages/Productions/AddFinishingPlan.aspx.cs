﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using System.Globalization;
using Repositories;
using MBTracker.EF;

namespace MBTracker.Pages.Productions
{
    public partial class AddFinishingPlan : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        ProductionManager productionManager = new ProductionManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        DataTable dtMachineBrands;
        DataTable dtProductionUnits = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                if (Request.QueryString["machineAssignedId"] != null)
                {
                    MachineAssignedId = int.Parse(Tools.UrlDecode(Request.QueryString["machineAssignedId"]));
                    var planInfo = unitOfWork.GenericRepositories<LinkingMachinesAssignedToOrder>().GetByID(MachineAssignedId);
                    LinkingMcGaugeId = planInfo.LinkingMachineGaugeId;
                    UnitId = planInfo.FinishingUnitId;
                    var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(planInfo.StyleId);
                    BindStylesByBuyer(styleInfo.BuyerId);
                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleInfo.Id);

                    ddlBuyers.Enabled = false;
                    ddlStyles.Enabled = false;

                    //LoadPlanInfo();
                    LoadShipmentInfo(styleInfo.Id);
                    CommonMethods.LoadProductionMonthDropDown(ddlMonthYears, 1, 0);
                    CalculateProductions();
                }
            }
        }

        int MachineAssignedId
        {
            set { ViewState["machineAssignedId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["machineAssignedId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        int UnitId
        {
            set { ViewState["unitId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["unitId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        int LinkingMcGaugeId
        {
            set { ViewState["linkingMcGaugeId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["linkingMcGaugeId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        string MachineBrandName
        {
            set { ViewState["machineBrandName"] = value; }
            get
            {
                try
                {
                    return ViewState["machineBrandName"].ToString();
                }
                catch
                {
                    return "";
                }
            }
        }

        int KnittingTime
        {
            set { ViewState["knittingTime"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["knittingTime"]);
                }
                catch
                {
                    return 0;
                }
            }
        }



        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlStyles.Items.Clear();
            pnlPlanningInfo.Visible = false;
            pnlShipmentInfo.Visible = false;
            pnlAssignMachinesToOrder.Visible = false;
            pnlMachinesAvailable.Visible = false;
            // ddlOrders.Items.Clear();

            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            //else
            //{
            //    pnlPlanningInfo.Visible = false;
            //    pnlShipmentInfo.Visible = false;
            //    pnlAssignMachinesToOrder.Visible = false;
            //    pnlMachinesAvailable.Visible = false;
            //}
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ddlOrders.Items.Clear();
            if (ddlStyles.SelectedValue != "")
            {
                //BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
                CommonMethods.LoadProductionMonthDropDown(ddlMonthYears, 1, 0);
                //LoadPlanInfo();
                LoadShipmentInfo(int.Parse(ddlStyles.SelectedValue));
                BindTotalOrderQuantity(int.Parse(ddlStyles.SelectedValue));
            }
            else
            {
                pnlPlanningInfo.Visible = false;
                pnlShipmentInfo.Visible = false;
                pnlAssignMachinesToOrder.Visible = false;
                //pnlMonthYearSelection.Visible = false;
                pnlMachinesAvailable.Visible = false;
            }


        }

        //private void BindOrdersByStyle(int styleId)
        //{
        //    CommonMethods.LoadOrderDropdownByStyle(ddlOrders, styleId, 1, 0);
        //}

        //protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        //{


        //    if (ddlOrders.SelectedValue != "")
        //    {

        //        CommonMethods.LoadProductionMonthDropDown(ddlMonthYears, 1, 0);
        //        LoadPlanInfo();

        //    }
        //    else
        //    {
        //        pnlPlanningInfo.Visible = false;
        //        pnlShipmentInfo.Visible = false;
        //        pnlAssignMachinesToOrder.Visible = false;
        //        //pnlMonthYearSelection.Visible = false;
        //        pnlMachinesAvailable.Visible = false;

        //    }
        //}

        //private void LoadPlanInfo()
        //{
        //    DataTable dt = new DataTable();


        //    var styleId = Convert.ToInt32(ddlStyles.SelectedValue);
        //    dt = productionManager.GetProductionPlanningInfo(styleId);
        //    if (dt.Rows.Count < 1)
        //    {
        //        dt = orderManager.GetProductionPlanningInfo(styleId);
        //    }

        //    if (dt.Rows.Count > 0)
        //    {
        //        KnittingTime = Convert.ToInt32(dt.Rows[0][8].ToString());
        //        MachineBrandId = Convert.ToInt32(dt.Rows[0][3].ToString());

        //        dtMachineBrands = orderManager.GetMachineBrands();

        //        rptPlanInfo.DataSource = dt;
        //        rptPlanInfo.DataBind();
        //        lblNoPlanningInfo.Visible = false;
        //        rptPlanInfo.Visible = true;
        //        pnlPlanningInfo.Visible = true;
        //        LoadShipmentInfo(styleId);
        //    }
        //    else
        //    {
        //        rptPlanInfo.DataSource = null;
        //        rptPlanInfo.DataBind();
        //        rptPlanInfo.Visible = false;
        //        lblNoPlanningInfo.Visible = true;
        //        pnlPlanningInfo.Visible = false;

        //        pnlShipmentInfo.Visible = false;
        //        pnlAssignMachinesToOrder.Visible = false;
        //        //pnlMonthYearSelection.Visible = false;
        //        pnlMachinesAvailable.Visible = false;

        //    }
        //    BindTotalOrderQuantity(styleId);
        //}
        private void BindTotalOrderQuantity(int styleId)
        {
            divTotalOrderQuantity.Visible = true;
            var totalOrderQty = unitOfWork.GetSingleValue($"Exec usp_GetActiveOrderQuantityByStyleId {styleId}");
            divTotalOrderQuantity.InnerHtml = $"<div style='width:100%; padding-bottom:9px; margin-bottom:5px; font-size:13px; padding:10px; background-color:#ffff00aa; font-weight:bold'>Order Quantity: <span style='font-size:15px;'>{totalOrderQty}</span>    &nbsp;&nbsp;<asp:LinkButton ID='lnkbtnView' onClick='window.open(\"../Reports/SummaryOfOrders.aspx?styleId={Tools.UrlEncode(styleId + "")}\",\"_blank\")' class='btn btn-success btn-mini hidden-phone pull-right' Text='View Details'>View Details</asp:LinkButton></div>";
        }
        private void LoadShipmentInfo(int styleId)
        {


            DataTable dt = new DataTable();
            dt = orderManager.GetShipmentInfoByStyleId(styleId);

            if (dt.Rows.Count > 0)
            {
                rptShipmentSummary.DataSource = dt;
                rptShipmentSummary.DataBind();

                rptShipmentSummary.Visible = true;
                lblNoShippingInfo.Visible = false;

                pnlAssignMachinesToOrder.Visible = true;
                //pnlMonthYearSelection.Visible = true;
                pnlPlanningInfo.Visible = true;
                pnlShipmentInfo.Visible = true;
                LoadFinishingUnits();
                lblNoPlanningInfo.Visible = false;

                if (MachineAssignedId != 0)
                {
                    btnSaveMachinesForOrder.Visible = false;
                    btnUpdateMachinesForOrder.Visible = true;
                    lblMachinePlanning.Text = "Update Finishing Plan:";

                }
                else
                {
                    btnSaveMachinesForOrder.Visible = true;
                    btnUpdateMachinesForOrder.Visible = false;
                    lblMachinePlanning.Text = "Add a Finishing Plan:";
                }
            }
            else
            {
                pnlPlanningInfo.Visible = false;
                rptShipmentSummary.DataSource = null;
                rptShipmentSummary.DataBind();
                lblNoPlanningInfo.Visible = true;
                rptShipmentSummary.Visible = false;
                pnlShipmentInfo.Visible = false;
                lblNoShippingInfo.Visible = true;

                pnlAssignMachinesToOrder.Visible = false;
                //pnlMonthYearSelection.Visible = false;
                pnlMachinesAvailable.Visible = false;

            }
        }




        private void LoadFinishingUnits()
        {
            //DataTable dt = new DataTable();
            

            if (MachineAssignedId != 0)
            {
                dtProductionUnits = unitOfWork.GetDataTableFromSql($"SELECT Id,UnitName FROM FinishingUnits WHERE Id = {UnitId}");
            }
            else
            {
                dtProductionUnits = unitOfWork.GetDataTableFromSql($"SELECT Id,UnitName FROM FinishingUnits");
            }

            rptIssueQuantityAndMachinesPerUnit.DataSource = dtProductionUnits;
            rptIssueQuantityAndMachinesPerUnit.DataBind();
            pnlAssignMachinesToOrder.Visible = true;
        }

        protected void ddlMonthYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlMonthYears.SelectedValue != "")
            {

                LoadMonthlyDates(Convert.ToInt32(ddlMonthYears.SelectedValue));
                pnlMachinesAvailable.Visible = true;
            }
            else
            {
                pnlMachinesAvailable.Visible = false;
            }
        }

        private void LoadMonthlyDates(int monthYearId)
        {
            DataTable dtMonthYearInfo = new DataTable();
            dtMonthYearInfo = orderManager.GetMonthYearInfo(monthYearId);

            string monthName = dtMonthYearInfo.Rows[0][1].ToString();
            string yearName = dtMonthYearInfo.Rows[0][2].ToString();
            int numberOfDays = Convert.ToInt32(dtMonthYearInfo.Rows[0][3].ToString());

            DataTable monthlyDates = MonthlyDates();
            string leadingZero = "0";


            for (int i = 1; i <= numberOfDays; i++)
            {
                if (i.ToString().Length == 1)
                {
                    monthlyDates.Rows.Add((leadingZero + i.ToString()) + " - " + monthName + " - " + yearName);
                }
                else
                {
                    monthlyDates.Rows.Add(i.ToString() + " - " + monthName + " - " + yearName);
                }
            }

            rptMonthlyDates.DataSource = monthlyDates;
            rptMonthlyDates.DataBind();

            Label lblHeaderMessage = (Label)rptMonthlyDates.Controls[0].Controls[0].FindControl("lblAvailableMachineMessage");
            lblHeaderMessage.Text = "Availability of Machines - Brand: " + MachineBrandName;

        }


        public DataTable MonthlyDates()
        {
            DataTable dt = new DataTable("MonthlyDates");
            dt.Columns.Add("Date", typeof(string));
            return dt;
        }


        protected void rptPlanInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlMCBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                ddlMCBrands.DataTextField = "MachineBrand";
                ddlMCBrands.DataValueField = "Id";
                ddlMCBrands.DataSource = dtMachineBrands;
                ddlMCBrands.DataBind();

                ddlMCBrands.SelectedValue = DataBinder.Eval(e.Item.DataItem, "MachineBrandId").ToString();
            }
        }



        protected void rptShipmentSummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label machinesRequired = (Label)e.Item.FindControl("lblMCRequired");
                double numMachinesRequired = Math.Ceiling((Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "ShipmentQuantity").ToString()) * KnittingTime) / 1320);
                machinesRequired.Text = numMachinesRequired.ToString();

            }
        }


        protected void rptMonthlyDates_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptMachinesAvailable = (Repeater)e.Item.FindControl("rptMachinesAvailable");
                //DateTime productionDate = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "Date").ToString());

                if (dtProductionUnits.Rows.Count <= 0)
                {
                    dtProductionUnits = unitOfWork.GetDataTableFromSql($"SELECT Id,UnitName FROM FinishingUnits");
                }
                //DataTable dt = productionManager.GetAvailableMachines(MachineBrandId, productionDate, Convert.ToInt32(ddlStyles.SelectedValue));

                rptMachinesAvailable.DataSource = dtProductionUnits;
                rptMachinesAvailable.DataBind();

            }
        }

        protected void btnSaveMachinesForOrder_Click(object sender, EventArgs e)
        {
            List<LinkingMachinesAssignedToOrder> lstMATO = new List<LinkingMachinesAssignedToOrder>();

            var styleId = Convert.ToInt32(ddlStyles.SelectedValue);
            var isValid = true;
            var machineAvailable = true;
            for (int i = 0; i < rptIssueQuantityAndMachinesPerUnit.Items.Count; i++)
            {
                var rptUnitMachines = (Repeater)rptIssueQuantityAndMachinesPerUnit.Items[i].FindControl("rptUnitMachines");
                Label lblProdUnitId = (Label)rptIssueQuantityAndMachinesPerUnit.Items[i].FindControl("lblUnitId");
                Label lblUnitName = (Label)rptIssueQuantityAndMachinesPerUnit.Items[i].FindControl("lblUnitName");
                for (int j = 0; j < rptUnitMachines.Items.Count; j++)
                {
                    TextBox tbxIssueQty = (TextBox)rptUnitMachines.Items[j].FindControl("tbxIssueQty");
                    TextBox tbxNumMachines = (TextBox)rptUnitMachines.Items[j].FindControl("tbxNumMC");
                    TextBox tbxLinkPerDay = (TextBox)rptUnitMachines.Items[j].FindControl("tbxLinkPerDay");
                    TextBox tbxStartDate = (TextBox)rptUnitMachines.Items[j].FindControl("tbxFromDate");
                    TextBox tbxEndDate = (TextBox)rptUnitMachines.Items[j].FindControl("tbxToDate");
                    Label lblMCGaugeId = (Label)rptUnitMachines.Items[j].FindControl("lblMCGaugeId");
                    Label lblProductionQty = (Label)rptUnitMachines.Items[j].FindControl("lblProductionQty");

                    if (tbxIssueQty.Text != "" && tbxNumMachines.Text != "" && tbxStartDate.Text != "" && tbxEndDate.Text != "" && tbxLinkPerDay.Text != "")
                    {
                        
                        var startDate = DateTime.Parse(tbxStartDate.Text);
                        var endDate = DateTime.Parse(tbxEndDate.Text);
                        var unitName = lblUnitName.Text;
                        var unitId = int.Parse(lblProdUnitId.Text);
                        var gaugeId = int.Parse(lblMCGaugeId.Text);
                        var noOfMc = int.Parse(tbxNumMachines.Text);
                        while (startDate<=endDate)
                        {
                            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetAvailableLinkingMachines_PerFinishingUnitByStyleDateAndGauge '{startDate}',{styleId},{unitId},{gaugeId}");
                            var McAvailable = dt.Rows[0].Field<int>("MachinesAvailable");
                            var gaugeNumber = dt.Rows[0].Field<int>("GaugeNumber");
                            if (noOfMc > McAvailable)
                            {
                                var msg = $"Unit Name: {unitName}<br />Machine Gauge: {gaugeNumber} <br />Production Date: {startDate.ToString("dd-MMM-yyyy")} <br /> Available Machine: {McAvailable}";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + "Information" + "','" + msg + "','','350','true');", true);
                                machineAvailable = false;
                                break;
                            }
                            startDate = startDate.AddDays(1);
                        }

                        var nMATO = new LinkingMachinesAssignedToOrder()
                        {
                            FinishingUnitId = int.Parse(lblProdUnitId.Text),
                            LinkingMachineGaugeId = int.Parse(lblMCGaugeId.Text),
                            LinkingPcsPerDay = int.Parse(tbxLinkPerDay.Text),
                            NumberOfMachines = int.Parse(tbxNumMachines.Text),
                            IssueQuantity = int.Parse(tbxIssueQty.Text),
                            ProdStartDate = DateTime.Parse(tbxStartDate.Text),
                            ProdEndDate = DateTime.Parse(tbxEndDate.Text),
                            StyleId = styleId,
                            CreatedBy = CommonMethods.SessionInfo.UserName,
                            CreateDate = DateTime.Now
                        };
                        lstMATO.Add(nMATO);
                        if (int.Parse(tbxIssueQty.Text) > int.Parse(lblProductionQty.Text))
                        {
                            isValid = false;
                            break;
                        }
                    }
                }
            }

            if (lstMATO.Count > 0)
            {
                if (machineAvailable)
                {
                    if (isValid)
                    {
                        foreach (var item in lstMATO)
                        {
                            unitOfWork.GenericRepositories<LinkingMachinesAssignedToOrder>().Insert(item);
                        }
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Issue quantity can not be greater than production quantity.')", true);
                    } 
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter correct information.')", true);
            }
        }

        protected void tbxNumMC_TextChanged(object sender, EventArgs e)
        {
            CalculateProductions();
        }

        protected void tbxFromDate_TextChanged(object sender, EventArgs e)
        {
            CalculateProductions();
        }

        protected void tbxToDate_TextChanged(object sender, EventArgs e)
        {
            CalculateProductions();
        }



        private void CalculateProductions()
        {

            int totalProduction = 0;

            for (int i = 0; i < rptIssueQuantityAndMachinesPerUnit.Items.Count; i++)
            {
                var rptUnitMachines = (Repeater)rptIssueQuantityAndMachinesPerUnit.Items[i].FindControl("rptUnitMachines");
                for (int j = 0; j < rptUnitMachines.Items.Count; j++)
                {
                    TextBox tbxNumMachines = (TextBox)rptUnitMachines.Items[j].FindControl("tbxNumMC");
                    TextBox tbxStartDate = (TextBox)rptUnitMachines.Items[j].FindControl("tbxFromDate");
                    TextBox tbxEndDate = (TextBox)rptUnitMachines.Items[j].FindControl("tbxToDate");
                    Label lblProductionQty = (Label)rptUnitMachines.Items[j].FindControl("lblProductionQty");
                    TextBox tbxLinkPerDay = (TextBox)rptUnitMachines.Items[j].FindControl("tbxLinkPerDay");


                    if (tbxNumMachines.Text != "" && tbxStartDate.Text != "" && tbxEndDate.Text != "" && tbxLinkPerDay.Text != "")
                    {
                        var LinkPerDay = int.Parse(tbxLinkPerDay.Text);
                        DateTime startDate = Convert.ToDateTime(tbxStartDate.Text);
                        DateTime endDate = Convert.ToDateTime(tbxEndDate.Text);
                        int numOfProdDays = 0;
                        if (startDate > endDate)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('End date should be greater than start date.');", true);
                        }
                        else
                        {
                            numOfProdDays = (endDate - startDate).Days + 1;
                        }
                        double numProduced = (numOfProdDays * Convert.ToInt32(tbxNumMachines.Text) * LinkPerDay);
                        var prodQty = Convert.ToInt32(Math.Floor(numProduced));
                        lblProductionQty.Text = prodQty + "";
                        totalProduction = totalProduction + prodQty;
                    }
                }

            }

            lblTotalProductions.Text = "Total Production: " + totalProduction.ToString() + " Pcs.";
        }

        protected void rptIssueQuantityAndMachinesPerUnit_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var unitId = int.Parse(((Label)e.Item.FindControl("lblUnitId")).Text);
                var rptUnitMachines = (Repeater)e.Item.FindControl("rptUnitMachines");
                //var styleId = int.Parse(ddlStyles.SelectedValue);
                var dt = unitOfWork.GetDataTableFromSql($"Exec usp_GetLinkingMachineGaugesByFinishingUnitId {unitId}");

                if (MachineAssignedId != 0)
                {
                    DataRow[] result = dt.Select();
                    foreach (DataRow row in result)
                    {
                        if (int.Parse(row["Id"].ToString()) != LinkingMcGaugeId)
                            dt.Rows.Remove(row);
                    }
                }

                rptUnitMachines.DataSource = dt;
                rptUnitMachines.DataBind();
            }
        }

        protected void rptMachinesAvailable_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var unitId = int.Parse(((Label)e.Item.FindControl("lblUnitId")).Text);
                var rptUnitMachinesAvailable = (Repeater)e.Item.FindControl("rptUnitMachinesAvailable");
                var productionDate = DateTime.Parse(((Label)((Repeater)sender).Parent.FindControl("lblProducitonDate")).Text).ToString("yyyy-MM-dd");
                var styleId = int.Parse(ddlStyles.SelectedValue);
                string sql = $"Exec usp_GetAvailableLinkingMachines_PerFinishingUnitByStyleAndDate '{productionDate}',{styleId},{unitId}";
                var dt = unitOfWork.GetDataTableFromSql(sql);
                rptUnitMachinesAvailable.DataSource = dt;
                rptUnitMachinesAvailable.DataBind();
            }
        }

        protected void btnUpdateMachinesForOrder_Click(object sender, EventArgs e)
        {
            try
            {

                var rptUnitMachines = (Repeater)rptIssueQuantityAndMachinesPerUnit.Items[0].FindControl("rptUnitMachines");

                TextBox tbxIssueQty = (TextBox)rptUnitMachines.Items[0].FindControl("tbxIssueQty");
                TextBox tbxNumMachines = (TextBox)rptUnitMachines.Items[0].FindControl("tbxNumMC");
                TextBox tbxStartDate = (TextBox)rptUnitMachines.Items[0].FindControl("tbxFromDate");
                TextBox tbxEndDate = (TextBox)rptUnitMachines.Items[0].FindControl("tbxToDate");
                TextBox tbxLinkPerDay = (TextBox)rptUnitMachines.Items[0].FindControl("tbxLinkPerDay");
                Label lblMCGaugeId = (Label)rptUnitMachines.Items[0].FindControl("lblMCGaugeId");
                Label lblProductionQty = (Label)rptUnitMachines.Items[0].FindControl("lblProductionQty");

                Label lblProdUnitId = (Label)rptIssueQuantityAndMachinesPerUnit.Items[0].FindControl("lblUnitId");
                Label lblUnitName = (Label)rptIssueQuantityAndMachinesPerUnit.Items[0].FindControl("lblUnitName");

                if (tbxIssueQty.Text != "" && tbxNumMachines.Text != "" && tbxStartDate.Text != "" && tbxEndDate.Text != "" && tbxLinkPerDay.Text != "")
                {
                    if (int.Parse(tbxIssueQty.Text) > int.Parse(lblProductionQty.Text))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Issue quantity can not be greater than production quantity.')", true);
                    }
                    else
                    {
                        var planInfo = unitOfWork.GenericRepositories<LinkingMachinesAssignedToOrder>().GetByID(MachineAssignedId);

                        var machineAvailable = true;

                        var startDate = DateTime.Parse(tbxStartDate.Text);
                        var endDate = DateTime.Parse(tbxEndDate.Text);
                        var unitName = lblUnitName.Text;
                        var unitId = int.Parse(lblProdUnitId.Text);
                        var gaugeId = int.Parse(lblMCGaugeId.Text);
                        var noOfMc = int.Parse(tbxNumMachines.Text);
                        var styleId = int.Parse(ddlStyles.SelectedValue);
                        while (startDate <= endDate)
                        {
                            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetAvailableLinkingMachines_PerFinishingUnitByStyleDateAndGauge '{startDate}',{styleId},{unitId},{gaugeId}");
                            var McAvailable = dt.Rows[0].Field<int>("MachinesAvailable");
                            var gaugeNumber = dt.Rows[0].Field<int>("GaugeNumber");
                            if (startDate >= planInfo.ProdStartDate && startDate <= planInfo.ProdEndDate)
                            {
                                McAvailable += planInfo.NumberOfMachines;
                            }

                            if (noOfMc > McAvailable)
                            {
                                var msg = $"Unit Name: {unitName}<br />Machine Gauge: {gaugeNumber} <br />Production Date: {startDate.ToString("dd-MMM-yyyy")} <br /> Available Machine: {McAvailable}";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + "Information" + "','" + msg + "','','350','true');", true);
                                machineAvailable = false;
                                break;
                            }
                            startDate = startDate.AddDays(1);
                        }


                        if (machineAvailable)
                        {
                            planInfo.IssueQuantity = int.Parse(tbxIssueQty.Text);
                            planInfo.NumberOfMachines = int.Parse(tbxNumMachines.Text);
                            planInfo.LinkingPcsPerDay = int.Parse(tbxLinkPerDay.Text);
                            planInfo.ProdStartDate = DateTime.Parse(tbxStartDate.Text);
                            planInfo.ProdEndDate = DateTime.Parse(tbxEndDate.Text);
                            unitOfWork.GenericRepositories<LinkingMachinesAssignedToOrder>().Update(planInfo);
                            unitOfWork.Save();
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
                            ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewFinishingPlans.aspx');", true); 
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter correct information.')", true);
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }
        protected void rptUnitMachines_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (MachineAssignedId != 0)
                {
                    var tbxIssueQty = (TextBox)e.Item.FindControl("tbxIssueQty");
                    var tbxNumMC = (TextBox)e.Item.FindControl("tbxNumMC");
                    var tbxFromDate = (TextBox)e.Item.FindControl("tbxFromDate");
                    var tbxToDate = (TextBox)e.Item.FindControl("tbxToDate");
                    var tbxLinkPerDay = (TextBox)e.Item.FindControl("tbxLinkPerDay");
                    

                    var planInfo = unitOfWork.GenericRepositories<LinkingMachinesAssignedToOrder>().GetByID(MachineAssignedId);
                    tbxIssueQty.Text = planInfo.IssueQuantity + "";
                    tbxNumMC.Text = planInfo.NumberOfMachines + "";
                    tbxLinkPerDay.Text = planInfo.LinkingPcsPerDay + "";
                    tbxFromDate.Text = planInfo.ProdStartDate.ToString("yyyy-MM-dd");
                    tbxToDate.Text = planInfo.ProdEndDate.ToString("yyyy-MM-dd");
                }
            }
        }
    }
}