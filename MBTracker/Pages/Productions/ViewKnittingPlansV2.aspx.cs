﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using Repositories;
using MBTracker.EF;
using System.Web.UI.HtmlControls;

namespace MBTracker.Pages.Productions
{
    public partial class ViewKnittingPlansV2 : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        ProductionManager productionManager = new ProductionManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //EnableDisableDeleteButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);

                if (Request.QueryString["styleId"] != null)
                {
                    var styleId = int.Parse(Tools.UrlDecode(Request.QueryString["styleId"]));
                    var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(styleId);
                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
                    BindStylesByBuyer(styleInfo.BuyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleId);
                    LoadPlanInfo(styleId);
                }
                else if (Request.QueryString["showInfo"] != null)
                {
                    var styleId = int.Parse(Tools.UrlDecode(Request.QueryString["showInfo"]));
                    var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(styleId);

                    if (CommonMethods.HasBuyerAssignedToUser(styleInfo.BuyerId, CommonMethods.SessionInfo.UserId))
                    {
                        ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
                        BindStylesByBuyer(styleInfo.BuyerId);
                        ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleId);
                        LoadPlanInfo(styleId);
                        ddlBuyers.Enabled = false;
                        ddlStyles.Enabled = false;
                    }
                    else
                    {
                        lblNotAuthorizedToView.Visible = true;
                        divBuyerAndStyle.Visible = false;
                    }
                }

            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteAssignedMachine(KnittingPlanId);
                }
            }
        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Knitting,Planning");
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewKnittingPlans", 2, 1);

        }

        int KnittingTime
        {
            set { ViewState["knittingTime"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["knittingTime"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlStyles.Items.Clear();
            //ddlOrders.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            pnlPlanningInfo.Visible = false;
            pnlShipmentInfo.Visible = false;
            pnlAssignMachinesToOrder.Visible = false;
            pnlPlanDetails.Visible = false;
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }
        private void LoadPlanInfo(int styleId)
        {
            DataTable dt = new DataTable();


            dt = productionManager.GetProductionPlanningInfo(styleId);
            if (dt.Rows.Count < 1)
            {
                dt = orderManager.GetProductionPlanningInfo(styleId);
            }

            if (dt.Rows.Count > 0)
            {

                pnlPlanningInfo.Visible = true;
                KnittingTime = Convert.ToInt32(dt.Rows[0][8].ToString());

                rptPlanInfo.DataSource = dt;
                rptPlanInfo.DataBind();
                lblNoPlanningInfo.Visible = false;
                rptPlanInfo.Visible = true;
                lblNoMCAssignedMessage.Visible = false;
                LoadShipmentInfo(Convert.ToInt32(ddlStyles.SelectedValue));
            }
            else
            {
                rptPlanInfo.DataSource = null;
                rptPlanInfo.DataBind();
                rptPlanInfo.Visible = false;
                lblNoPlanningInfo.Visible = true;

                pnlShipmentInfo.Visible = false;
                pnlAssignMachinesToOrder.Visible = false;
                pnlPlanningInfo.Visible = false;
                lblNoMCAssignedMessage.Visible = true;
            }

            BindTotalOrderQuantity(Convert.ToInt32(ddlStyles.SelectedValue));
            pnlPlanDetails.Visible = false;
        }

        private void BindTotalOrderQuantity(int styleId)
        {
            divTotalOrderQuantity.Visible = true;
            var totalOrderQty = unitOfWork.GetSingleValue($"Exec usp_GetActiveOrderQuantityByStyleId {styleId}");
            divTotalOrderQuantity.InnerHtml = $"<div style='width:100%; padding-bottom:9px; margin-bottom:5px; font-size:13px; padding:10px; background-color:#ffff00aa; font-weight:bold'>Order Quantity: <span style='font-size:15px;'>{totalOrderQty}</span>    &nbsp;&nbsp;<asp:LinkButton ID='lnkbtnView' onClick='window.open(\"../Reports/SummaryOfOrderColors.aspx?styleId={Tools.UrlEncode(styleId + "")}\",\"_blank\")' class='btn btn-success btn-mini hidden-phone pull-right' Text='View Details'>View Details</asp:LinkButton></div>";
        }

        private void LoadShipmentInfo(int styleId)
        {
            pnlShipmentInfo.Visible = true;

            DataTable dt = new DataTable();
            dt = orderManager.GetMonthWiseShipmentInfoByStyleId(styleId);

            if (dt.Rows.Count > 0)
            {
                rptShipmentSummary.DataSource = dt;
                rptShipmentSummary.DataBind();

                rptShipmentSummary.Visible = true;
                lblNoShippingInfo.Visible = false;


                LoadExistingMCAssignedToOrder();
            }
            else
            {
                rptShipmentSummary.DataSource = null;
                rptShipmentSummary.DataBind();

                rptShipmentSummary.Visible = false;
                lblNoShippingInfo.Visible = true;

                pnlAssignMachinesToOrder.Visible = false;

            }
        }

        int KnittingPlanId
        {
            set { ViewState["KnittingPlanId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["KnittingPlanId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        private void LoadExistingMCAssignedToOrder()
        {

            //DataTable dt = new DataTable();
            //dt = productionManager.GetMachinesAssignedToStyle(Convert.ToInt32(ddlStyles.SelectedValue));

            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetKnittingPlansByStyle '{Convert.ToInt32(ddlStyles.SelectedValue)}'");
            if (dt.Rows.Count > 0)
            {
                dt = dt.CheckEditDeletePermission(CommonMethods.SessionInfo.RoleId, "ViewKnittingPlans");
                decimal totalIssueQty = dt.Select().Sum(p => Convert.ToDecimal(p["IssueQty"]));
                decimal totalKnittingQty = dt.Select().Sum(p => Convert.ToDecimal(p["KnittingQty"]));
             
                rpt.DataSource = dt;
                rpt.DataBind();
                rpt.Visible = true;
                pnlAssignMachinesToOrder.Visible = true;
                lblNoMCAssignedMessage.Visible = false;

                Control footerTemplate = rpt.Controls[rpt.Controls.Count - 1].Controls[0];
                (footerTemplate.FindControl("lblTotalKnittingQty") as Label).Text = totalKnittingQty + "";
                (footerTemplate.FindControl("lblTotalIssueQty") as Label).Text = totalIssueQty + "";
            }
            else
            {
                rpt.DataSource = null;
                rpt.DataBind();
                rpt.Visible = false;
                lblNoMCAssignedMessage.Visible = true;
                pnlAssignMachinesToOrder.Visible = false;
                pnlPlanningInfo.Visible = false;

            }
            pnlPlanDetails.Visible = false;
        }

        protected void rptShipmentSummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label machinesRequired = (Label)e.Item.FindControl("lblMCRequired");
                double numMachinesRequired = Math.Ceiling((Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "ShipmentQuantity").ToString()) * KnittingTime) / 1320);
                machinesRequired.Text = numMachinesRequired.ToString();
            }
        }

        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                KnittingPlanId = Convert.ToInt32(e.CommandArgument.ToString());

                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
            }
            else if (e.CommandName == "Edit")
            {
                int machineAssignedId = Convert.ToInt32(e.CommandArgument.ToString());
                Response.Redirect("AddKnittingPlanV2.aspx?planId=" + Tools.UrlEncode(machineAssignedId + ""));
            }
        }

        private void DeleteAssignedMachine(int planId)
        {
            try
            {
                var planDetails = unitOfWork.GenericRepositories<KnittingPlanDetails>().Get(x => x.PlanId == planId).ToList();
                var planInfo = unitOfWork.GenericRepositories<KnittingPlan>().GetByID(planId);
                foreach (var item in planDetails)
                {
                    unitOfWork.GenericRepositories<KnittingPlanDetails>().Delete(item);
                }
                unitOfWork.GenericRepositories<KnittingPlan>().Delete(planInfo);
                unitOfWork.Save();
                LoadExistingMCAssignedToOrder();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Plan deletion failed.')", true);
            }
            
        }

        protected void btnViewKnittingPlan_Click(object sender, EventArgs e)
        {
            if (ddlBuyers.SelectedValue != "")
            {
                if (ddlStyles.SelectedValue != "")
                {
                    LoadPlanInfo(int.Parse(ddlStyles.SelectedValue));
                }
                else
                {
                    pnlPlanningInfo.Visible = false;
                    pnlShipmentInfo.Visible = false;
                    pnlAssignMachinesToOrder.Visible = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a style.');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a buyer');", true);
            }
        }

        protected void lbkbtnViewDetails_Command(object sender, CommandEventArgs e)
        {
            
            var args = e.CommandArgument.ToString();
            var param = args.Split(',');
            KnittingPlanId = Convert.ToInt32(param[0]);
            var unitName = param[1];
            var knittingQty = param[2];
            var IssueQty = param[3];
            var MachineBrandName = param[3];
            var planDetails = unitOfWork.GetDataTableFromSql($"SELECT *,'{unitName}' as Unit,'{knittingQty}' as KnittingQty,'{IssueQty}' as IssueQty,'{MachineBrandName}' as MachineBrand  FROM KnittingPlanDetails WHERE PlanId = '{KnittingPlanId}'");
            ViewState["RowSpan"] = planDetails.Rows.Count;
            rptPlanDetails.DataSource = planDetails;
            rptPlanDetails.DataBind();
            pnlPlanDetails.Visible = true;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown()", true);
        }

    }
}