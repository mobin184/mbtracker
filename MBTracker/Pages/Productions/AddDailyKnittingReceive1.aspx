﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddDailyKnittingReceive1.aspx.cs" Inherits="MBTracker.Pages.Productions.AddDailyKnittingReceive" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table th, .table td {
            padding: 4px;
            line-height: 10px;
            text-align: center;
            vertical-align: middle !important;
            border-top: 1px solid #e0e0e0;
        }

        .form-horizontal .control-label {
            padding-top: 0px;
        }

        .btn-info {
            margin: 0px
        }
    </style>
    <div class="row-fluid">
        <div class="span7" runat="server" id="pnlSearch">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="Daily Knitting Receive:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row-fluid" runat="server" visible="true" id="FilterDiv">
                        <div class="col-md-5">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputBuyerName" class="control-label" style="width: 100px; text-align: left">
                                        <asp:Label ID="lblFilterDate" runat="server" Text="Delivery Date:"></asp:Label>
                                        <span style="font-weight: 700; color: #CC0000"></span>
                                    </label>
                                    <div class="controls controls-row" style="margin-left: 110px;">
                                        <asp:TextBox ID="tbxFilterDate" runat="server" placeholder="Enter date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputBuyerName" class="control-label" style="width: 100px; text-align: left">
                                        <asp:Label ID="Label10" runat="server" Text="Production Unit:"></asp:Label>
                                        <span style="font-weight: 700; color: #CC0000"></span>
                                    </label>
                                    <div class="controls controls-row" style="margin-left: 110px;">
                                        <asp:DropDownList ID="ddlUserUnit" Width="100%" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2" style="padding-left: 0px;">
                            <asp:Button ID="btnSearch" Style="width: 100%" runat="server" Text="View Entries" CssClass="btn btn-info" OnClick="btnSearch_Click" />
                        </div>
                        <div class="col-md-12" style="padding-top: 15px;">
                            <div class="col-md-12" style="padding-left: 0px; padding-right: 0px">
                                <div id="dt_example" class="example_alt_pagination">
                                    <asp:Label runat="server" CssClass="" Font-Bold="true" Text="Delivery entires:"></asp:Label>
                                    <asp:Repeater ID="rpt" runat="server">
                                        <HeaderTemplate>
                                            <table id="data-table1" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="hidden">
                                                            <asp:Label ID="Label3" runat="server" Text="UnitId"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label1" runat="server" Text="Delivery Date"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label2" runat="server" Text="Knitting Unit"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label7" runat="server" Text="Shift"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label6" runat="server" Text="Delivery Qty"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label8" runat="server" Text="Entry Date & Time"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label12" runat="server" Text="Status"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label16" runat="server" Text="Action"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr style=''>
                                                <td class="hidden"><%#Eval("UnitId") %>></td>
                                                <td><%#String.Format("{0:dd/MM/yyyy}",Eval("DeliveryDate")) %> </td>
                                                <td><%#Eval("UnitName") %></td>
                                                <td><%#Eval("ShiftName") %></td>
                                                <td><%#Eval("DeliveryQty") %></td>
                                                <td><%#String.Format("{0:dd/MM/yyyy h:mm tt}",Eval("CreateDate")) %> </td>
                                                <td><%#Eval("IsKnittingReceived").ToString() == "1" ? "<span class='label label-success'>Received</span>": "<span class='label label-danger'>Not Received</span>" %></td>
                                                <td>
                                                    <%-- <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("UnitId")+","+ Eval("DeliveryDate")+","+ Eval("ShiftId")+","+ Eval("BatchId")%>' class="btn btn-success btn-small hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>--%>
                                                    <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="View" CommandArgument='<%#Eval("UnitId")+","+ Eval("DeliveryDate")+","+ Eval("ShiftId")+","+ Eval("BatchId")+","+Eval("IsKnittingReceived")+","+Eval("KnittingReceiveDate")%>' class="btn btn-success btn-small hidden-phone" OnCommand="lnkbtnView_Command" Text="View Details"></asp:LinkButton>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                            </table>
                                            <div class="clearfix"></div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid" runat="server" id="pnlDetails" visible="false">
        <div class="span12">
            <div class="widget">
                 <div class="widget-header" runat="server" id="pnlDetailsTitle" visible="false">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="Label13" Text="Update Daily Knitting Receive:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row-fluid" runat="server" id="pnlSummary" visible="false">

                        <div class="col-md-6">
                            <div id="dt_example3" class="example_alt_pagination">
                                <div class="control-group">
                                    <label for="inputOrder" class="control-label">
                                        <asp:Label ID="lblSummary" Visible="false" runat="server" Text="" Font-Bold="true"> Unit & Production Date Info</asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:Repeater ID="rptGeneralInfo" runat="server" OnItemDataBound="rptGeneralInfo_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table" class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                <asp:Label ID="lblProductionUnit" Enabled="false" runat="server" Text="Production Unit"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="Label5" runat="server" Text="Shift"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblProductionDate" runat="server" Text="Delivery Date"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="Label11" runat="server" Text="Receive Date"></asp:Label></th>
                                                            <th class="hidden">
                                                                <asp:Label ID="lblUser" runat="server" Text="User Name"></asp:Label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                       <%-- <asp:DropDownList ID="ddlUserUnits" runat="server" Width="100" AutoPostBack="true"></asp:DropDownList>--%>
                                                        <%#Eval("Unit") %>
                                                    </td>
                                                    <td>
                                                      <%--  <asp:DropDownList ID="ddlShift" runat="server" Width="130"></asp:DropDownList>--%>
                                                        <%#Eval("Shift") %>
                                                    </td>
                                                    <td><%#Eval("DeliveryDate") %> </td>
                                                    <td>
                                                        <asp:TextBox ID="tbxReceiveDate" runat="server" placeholder="Enter date" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                    </td>
                                                    <td class="hidden"><%#Eval("UserName") %> </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                                    </table>
                                                    <div class="clearfix"></div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="col-md-12">
                            <div id="dt_example2" class="example_alt_pagination">
                                <div class="control-group">
                                    <label for="inputOrder" class="control-label">
                                        <asp:Label ID="lblDetails" runat="server" Visible="false" Text="Details Information" Font-Bold="true"></asp:Label></label>
                                    <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                        <asp:Repeater ID="rptEntryInfo" runat="server" OnItemDataBound="rptEntryInfo_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table4" class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                <asp:Label ID="lblBuyer" runat="server" Text="Buyer<br/> Names"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblStyle" runat="server" Text="Styles<br/> Names"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblColor" runat="server" Text="Color<br/> Names"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="Label4" runat="server" Text="Size & Delivery<br/> Quantity"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="Label9" runat="server" Text="Size & Received<br/> Quantity"></asp:Label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" Width="75" Text='<%#Eval("BuyerName") %>'></asp:Label>
                                                        <asp:Label ID="lblStyleId" runat="server" Text='<%#Eval("StyleId") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblBuyerId" runat="server" Text='<%#Eval("BuyerId") %>' Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" Width="75" Text='<%#Eval("StyleName") %>'></asp:Label></td>
                                                    <td><%#Eval("ColorDescription") %>
                                                        <asp:Label ID="lblColorCode" runat="server" Text='<%#Eval("ColorCode") %>' Visible="false"></asp:Label></td>
                                                    <asp:Label ID="lblUnitId" runat="server" Text='<%#Eval("UnitId") %>' Visible="false"></asp:Label>
                                                    <td>
                                                        <asp:GridView ID="gvSizeQuantity2" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity_RowCreated2">
                                                        </asp:GridView>
                                                    </td>
                                                    <td>
                                                        <asp:GridView ID="gvSizeQuantity3" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity_RowCreated3">
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                                </table>
                                                <div class="clearfix"></div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <asp:Label ID="lblNoKnittingGoingOn" runat="server" Text="Production was not planned in this unit today." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                                        <asp:Button ID="btnReceive" runat="server" Text="Receive Deliveries" Visible="false" CssClass="btn btn-info pull-right" OnClick="btnReceive_Click" />
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update Deliveries" Visible="false" CssClass="btn btn-info pull-right" OnClick="btnUpdate_Click" />
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
