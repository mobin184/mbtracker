﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class ViewShipmentEntries : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        BuyerManager buyerManager = new BuyerManager();
        DataTable dtSizes;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //EnableDisableEditButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteShippedItem(ShippedItemId);
                }
            }
        }


        private void DeleteShippedItem(int shippedItemId)
        {
            try
            {
                var shippedItem = unitOfWork.GenericRepositories<ShippedItems>().GetByID(shippedItemId);
                var shippedItemColorSizes = unitOfWork.GenericRepositories<ShippedItemsColorSizes>().Get(x=>x.ShippedId==shippedItemId);
                foreach (var item in shippedItemColorSizes)
                {
                    unitOfWork.GenericRepositories<ShippedItemsColorSizes>().Delete(item);
                }
                unitOfWork.GenericRepositories<ShippedItems>().Delete(shippedItem);
                unitOfWork.Save();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
                LoadShipppedInfo();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Shipment deletion failed.')", true);
            }
        }


        protected void LoadSizeInfo()
        {
            dtSizes = buyerManager.GetOrderSizes(int.Parse(ddlOrders.SelectedValue));
        }
        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            OrderId = 0;
            ddlStyles.Items.Clear();
            ddlOrders.Items.Clear();
            divDetailsInfo.Visible = false;
            rptInfo.DataSource = null;
            rptInfo.DataBind();
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            rptColorAndDeliveryCountries.DataSource = null;
            rptColorAndDeliveryCountries.DataBind();
        }
        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            OrderId = 0;
            ddlOrders.Items.Clear();
            divDetailsInfo.Visible = false;
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            rptColorAndDeliveryCountries.DataSource = null;
            rptColorAndDeliveryCountries.DataBind();
            rptInfo.DataSource = null;
            rptInfo.DataBind();
            if (ddlStyles.SelectedValue != "")
            {
                BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
            }
        }

        private void BindOrdersByStyle(int styleId)
        {
            CommonMethods.LoadOrderDropdownByStyle(ddlOrders, styleId, 1, 0);
        }

        protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        {
            OrderId = 0;
            if (ddlOrders.SelectedValue != "")
            {
                OrderId = int.Parse(ddlOrders.SelectedValue);
                LoadSizeInfo();
                //LoadShipppedInfo();
            }
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            rptColorAndDeliveryCountries.DataSource = null;
            rptColorAndDeliveryCountries.DataBind();
        }

        private void LoadShipppedInfo()
        {
            var buyerId = int.Parse(ddlBuyers.SelectedValue);
            var styleId = int.Parse(ddlStyles.SelectedValue);
            var orderId = int.Parse(ddlOrders.SelectedValue);

            var dtShippedInfo = unitOfWork.GetDataTableFromSql($"Exec usp_GetShppedInfoByBuyerStyleAndOrder {buyerId},{styleId},{orderId},{CommonMethods.SessionInfo.UserId}");
            
            if (dtShippedInfo.Rows.Count > 0)
            {
                dtShippedInfo.CheckEditDeletePermission(CommonMethods.SessionInfo.RoleId, "ShippedItems");
                rptInfo.DataSource = dtShippedInfo;
                rptInfo.DataBind();
                lblNoDataFound.Visible = false;
                divDetailsInfo.Visible = true;
            }
            else
            {
                rptInfo.DataSource = null;
                rptInfo.DataBind();
                lblNoDataFound.Visible = true;
                divDetailsInfo.Visible = false;
            }
        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            var shippedItemId = int.Parse(e.CommandArgument.ToString());
            Response.Redirect("EnterShipmentEntry.aspx?shippedId=" + Tools.UrlEncode(shippedItemId + ""));
        }

        protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        {
            ShippedItemId = int.Parse(e.CommandArgument.ToString());
            LoadColorByShipmentId();
            pnlColorDeliveryCountryAndQuantity.Visible = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown()", true);
        }

        int ShippedItemId
        {
            get
            {
                try
                {
                    return int.Parse(ViewState["ShippedItemId"].ToString());
                }
                catch (Exception)
                {
                    return 0;
                }
            }
            set
            {
                ViewState["ShippedItemId"] = value;
            }
        }

        int OrderId
        {
            get
            {
                try
                {
                    return int.Parse(ViewState["OrderId"].ToString());
                }
                catch (Exception)
                {
                    return 0;
                }
            }
            set
            {
                ViewState["OrderId"] = value;
            }
        }

        private void LoadColorByShipmentId()
        {
            try
            {
                var dtColors = unitOfWork.GetDataTableFromSql($"Exec [usp_GetShipmentColorsByShippedId] {ShippedItemId}");
                if (dtColors.Rows.Count > 0)
                {
                    rptColorAndDeliveryCountries.DataSource = dtColors;
                    rptColorAndDeliveryCountries.DataBind();
                    pnlColorDeliveryCountryAndQuantity.Visible = true;
                    lblColorOrDeliveryCountryNotFound.Visible = false;
                }
                else
                {
                    rptColorAndDeliveryCountries.DataSource = null;
                    rptColorAndDeliveryCountries.DataBind();
                    pnlColorDeliveryCountryAndQuantity.Visible = false;
                    lblColorOrDeliveryCountryNotFound.Text = "No color found for shipment.";
                    lblColorOrDeliveryCountryNotFound.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        protected void rptColorAndDeliveryCountries_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //var shipmnetId = int.Parse(ddlShipment.SelectedValue);
                var rptDeliveryCountryAndQty = (Repeater)e.Item.FindControl("rptDeliveryCountryAndQty");
                var lblBuyerColorId = (Label)e.Item.FindControl("lblBuyerColorId");
                var buyerColorId = int.Parse(lblBuyerColorId.Text);
                var dtDeliveryCountries = unitOfWork.GetDataTableFromSql($"EXEC usp_GetDeliveryCountryByShippedId {ShippedItemId},{buyerColorId}");
                rptDeliveryCountryAndQty.DataSource = dtDeliveryCountries;
                rptDeliveryCountryAndQty.DataBind();
            }
        }

        protected void gvSizeAndQty_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label txtSizeQty = null;
                Label lblSizeId = null;
                if(dtSizes == null)
                {
                    LoadSizeInfo();
                }


                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new Label();
                    txtSizeQty.ID = "txtSizeQty1" + i.ToString();

                    var sizeId = Convert.ToInt32(dtSizes.Rows[i][0].ToString());
                    var lblDeliveryCountryId = (Label)((GridView)sender).DataItemContainer.FindControl("lblDeliveryCountryId");
                    var deleveryCountryId = Convert.ToInt32(lblDeliveryCountryId.Text);

                    var lblColorId = (Label)((GridView)sender).DataItemContainer.NamingContainer.NamingContainer.FindControl("lblColorId");
                    var colorId = Convert.ToInt32(lblColorId.Text);
                    var shipmentColorCountrySize = unitOfWork.GenericRepositories<ShippedItemsColorSizes>().Get(x => x.ShippedId == ShippedItemId && x.DeliveryCountryId == deleveryCountryId &&  x.BuyerColorId == colorId && x.SizeId == sizeId).FirstOrDefault();

                    if (shipmentColorCountrySize != null)
                    {
                        txtSizeQty.Text = shipmentColorCountrySize.ShippedQty + "";
                        var lblShipmentColorCountrySize = new Label();
                        lblShipmentColorCountrySize.ID = "lblShipmentColorCountrySize1" + i.ToString();
                        lblShipmentColorCountrySize.Text = shipmentColorCountrySize.Id + "";
                        lblShipmentColorCountrySize.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblShipmentColorCountrySize);
                    }
                    else
                    {
                        txtSizeQty.Text = "";
                        var lblShipmentColorCountrySize = new Label();
                        lblShipmentColorCountrySize.ID = "lblShipmentColorCountrySize1" + i.ToString();
                        lblShipmentColorCountrySize.Text = "";
                        lblShipmentColorCountrySize.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblShipmentColorCountrySize);
                    }


                    txtSizeQty.Width = 30;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);

                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId1" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);
                }
            }
        }

        protected void rptDeliveryCountryAndQty_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gvSizeAndQty = (GridView)e.Item.FindControl("gvSizeAndQty");
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);
                if(dtSizes == null)
                {
                   LoadSizeInfo();
                }

                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeAndQty.Columns.Add(templateField);

                    }

                }

                gvSizeAndQty.DataSource = dtOneRow;
                gvSizeAndQty.DataBind();
            }
        }

        protected void btnViewEntries_Click(object sender, EventArgs e)
        {
            try
            {
                LoadShipppedInfo();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            ShippedItemId = Convert.ToInt32(e.CommandArgument.ToString());

            var title = "Warning";
            var msg = "Are you sure you want to delete?";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
        }
    }
}