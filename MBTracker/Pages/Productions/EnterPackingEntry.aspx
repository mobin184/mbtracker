﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="EnterPackingEntry.aspx.cs" Inherits="MBTracker.Pages.Productions.EnterPackingEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table th {
            text-align: center;
        }

        .table th, .table td {
            padding: 5px;
            text-align: center;
            vertical-align: top;
            border-top: 1px solid #e0e0e0;
        }
    </style>
    <div class="row-fluid">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="actionTitle" Text="Enter Packing Items:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row">
                        <div class="form-horizontal">
                            <div class="col-md-5 pull-right" style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 20px">
                                <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                            </div>
                            <div class="control-group">
                                <div class="col-md-12 col-sm-12">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                </div>
                            </div>
                            <div class="col-md-6">                                
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left;width:140px; padding-top:5px;text-align:right">
                                        <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row" style="margin-left:150px">
                                        <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left;width:140px; padding-top:5px;text-align:right">
                                        <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row" style="margin-left:150px">
                                        <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left;width:140px; padding-top:5px;text-align:right">
                                        <asp:Label ID="lblOrder" runat="server" Text="Select Order:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row" style="margin-left:150px">
                                        <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlOrders_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlOrders"><span style="font-weight: 700; color: #CC0000">Please select an order.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">                                
                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label" style="float: left;width:140px; padding-top:5px;text-align:right">
                                        <asp:Label ID="lblShippingDate" runat="server" Text="Packing Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row" style="margin-left:150px">
                                        <asp:TextBox ID="tbxPackingDate" runat="server" placeholder="Enter packing date" CssClass="form-control" Width="100%" TextMode="Date"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxPackingDate"><span style="font-weight: 700; color: #CC0000">Enter packing date</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left;width:140px; padding-top:5px;text-align:right">
                                        <asp:Label ID="Label1" runat="server" Text="Shipment Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row" style="margin-left:150px">
                                        <asp:DropDownList ID="ddlShipment" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlShipment_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlShipment"><span style="font-weight: 700; color: #CC0000">Select a shipment date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left;width:140px; padding-top:5px;text-align:right">
                                        <asp:Label ID="Label5" runat="server" Text="Finishing Floor:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row" style="margin-left:150px">
                                        <asp:DropDownList ID="ddlFinishingUnitFloor" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlShipment_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlFinishingUnitFloor"><span style="font-weight: 700; color: #CC0000">Please select floor.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="span9" id="pnlColorDeliveryCountryAndQuantity" runat="server" visible="false" style="margin-left: 0px;">
            <div class="widget">
                <div class="widget-body">
                    <div class="controls controls-row">
                        <asp:Label ID="Label6" runat="server" Text="" Visible="false" BackColor="#ffff00"></asp:Label>
                    </div>
                    <asp:Panel runat="server">
                        <div class="control-group">
                            <label for="inputDeliveryCountry" class="control-label">
                                <asp:Label ID="Label7" runat="server" Text="Shipment Information:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                        </div>
                        <div class="controls controls-row" style="overflow-x: auto">
                            <div id="dt_example2" class="example_alt_pagination">
                                <asp:Repeater ID="rptShipmentInformation" runat="server" OnItemDataBound="rptShipmentInformation_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="width: 200px">
                                                        <asp:Label ID="lblColorDesc" runat="server" Text="Color"></asp:Label></th>
                                                  <%--  <th style="width: 350px" class="hidden">
                                                        <asp:Label ID="Label2" runat="server" Text="Available Size & Quantity"></asp:Label></th>--%>
                                                    <th style="width: 350px">
                                                        <asp:Label ID="lblColorDescription" runat="server" Text="Country, Size & Quantity"></asp:Label></th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="vertical-align: middle">
                                                <asp:Label ID="lblColorDesc" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ColorDescription")%>'></asp:Label><asp:Label ID="lblColorId" Visible="false" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>'></asp:Label>
                                                <asp:Label ID="lblShipmentDateId" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShipmentDateId")%>'></asp:Label>
                                                <asp:Label ID="lblStyleId" Visible="false" runat="server" Text=''></asp:Label>
                                            </td>
                                           <%-- <td style="vertical-align: middle" class="hidden">
                                                <asp:GridView ID="gvAvailableSizeAndQty" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvAvailableSizeAndQty_RowCreated"></asp:GridView>
                                            </td>--%>
                                            <td>
                                                <asp:Repeater ID="rptDeliveryCountryAndQtyAvailableQty" runat="server" OnItemDataBound="rptDeliveryCountryAndQtyAvailableQty_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="data-table" class="table table-bordered table-hover">
                                                            <tr>
                                                                <th>
                                                                    <asp:Label ID="Label3" runat="server" Text="Country"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label2" runat="server" Text="Color Quantity"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label4" runat="server" Text="Size Quantity"></asp:Label></th>
                                                               <%-- <th>
                                                                    <asp:Label ID="lblColorDesc" runat="server" Text="Packing Quantity"></asp:Label></th>
                                                                </th>--%>
                                                            </tr>
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="vertical-align: middle">
                                                                <asp:Label ID="lblDeliveryCountryId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryCountryId")%>'></asp:Label><asp:Label ID="lblDeliveryCountryName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryCountryCode")%>'></asp:Label></th></td>
                                                            <td style="text-align:center;vertical-align:middle"><%#Eval("ColorShipmentQty") %></td>
                                                            <td>
                                                                <asp:GridView ID="gvSizeAndQty" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeAndQty_RowCreated"></asp:GridView>
                                                            </td>
                                                            <%--<td>
                                                                <asp:GridView ID="gvSizeAndQty1" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeAndQty1_RowCreated"></asp:GridView>
                                                            </td>--%>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                                                </table>
                                                                <div class="clearfix">
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                            </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div class="widget">
                <div class="widget-body">
                    <div class="controls controls-row">
                        <asp:Label ID="lblColorOrDeliveryCountryNotFound" runat="server" Text="" Visible="false" BackColor="#ffff00"></asp:Label>
                    </div>
                    <asp:Panel runat="server">
                        <div class="control-group">
                            <label for="inputDeliveryCountry" class="control-label">
                                <asp:Label ID="lblColorAndSize" runat="server" Text="Packing Information:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                        </div>
                        <div class="controls controls-row" style="overflow-x: auto">
                            <div id="dt_example2" class="example_alt_pagination">
                                <asp:Repeater ID="rptColorAndDeliveryCountries" runat="server" OnItemDataBound="rptColorAndDeliveryCountries_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="width: 200px">
                                                        <asp:Label ID="lblColorDesc" runat="server" Text="Color"></asp:Label></th>
                                                   <%-- <th style="width: 350px" class="hidden">
                                                        <asp:Label ID="Label2" runat="server" Text="Available Size & Quantity"></asp:Label></th>--%>
                                                    <th style="width: 350px">
                                                        <asp:Label ID="lblColorDescription" runat="server" Text="Country, Size & Quantity"></asp:Label></th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="vertical-align: middle">
                                                <asp:Label ID="lblColorDesc" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ColorDescription")%>'></asp:Label><asp:Label ID="lblColorId" Visible="false" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>'></asp:Label>
                                                <asp:Label ID="lblShipmentDateId" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShipmentDateId")%>'></asp:Label>
                                                <asp:Label ID="lblStyleId" Visible="false" runat="server" Text=''></asp:Label>
                                            </td>
                                           <%-- <td style="vertical-align: middle" class="hidden">
                                                <asp:GridView ID="gvAvailableSizeAndQty" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvAvailableSizeAndQty_RowCreated"></asp:GridView>
                                            </td>--%>
                                            <td>
                                                <asp:Repeater ID="rptDeliveryCountryAndQty" runat="server" OnItemDataBound="rptDeliveryCountryAndQty_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="data-table" class="table table-bordered table-hover">
                                                            <tr>
                                                                <th>
                                                                    <asp:Label ID="Label3" runat="server" Text="Country"></asp:Label></th>
                                                              <%--  <th>
                                                                    <asp:Label ID="Label4" runat="server" Text="Shipment Requirement"></asp:Label></th>--%>
                                                                <th>
                                                                    <asp:Label ID="lblColorDesc" runat="server" Text="Packing Quantity"></asp:Label></th>
                                                                </th>
                                                            </tr>
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="vertical-align: middle">
                                                                <asp:Label ID="lblDeliveryCountryId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryCountryId")%>'></asp:Label><asp:Label ID="lblDeliveryCountryName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryCountryCode")%>'></asp:Label></th></td>
                                                            <%--<td>
                                                                <asp:GridView ID="gvSizeAndQty" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeAndQty_RowCreated"></asp:GridView>
                                                            </td>--%>
                                                            <td>
                                                                <asp:GridView ID="gvSizeAndQty1" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeAndQty1_RowCreated"></asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                                                </table>
                                                                <div class="clearfix">
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                            </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label12" runat="server" Text="&nbsp;" CssClass="text-left"></asp:Label></label>
                            <div class="controls controls-row">
                                <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 0px">
                                    <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                </div>
                                  </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            
            <asp:Button ID="btnSave" runat="server" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSave_Click" />
            <asp:Button ID="btnUpdate" runat="server" Visible="false" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdate_Click" />

        
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
