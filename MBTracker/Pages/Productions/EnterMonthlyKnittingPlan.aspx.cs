﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Productions
{
    public partial class EnterMonthlyKnittingPlan : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        ProductionManager productionManager = new ProductionManager();
        DataTable dtUserUnits;
        DataTable dtSizes;
        DataTable dtYarnIssuedForSizes;
        int buyerId;
        int styleId;
        int unitId;
        int buyerColorId;
        int entryRowNumber2 = 0;

        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Request.QueryString["planId"] != null)
                {
                    KnittingPlanId = int.Parse(Tools.UrlDecode(Request.QueryString["planId"]));
                    //Year = int.Parse(Tools.UrlDecode(Request.QueryString["year"]));
                    // KnittingDate = DateTime.Parse(Tools.UrlDecode(Request.QueryString["knittingDate"]));
                    // ShiftId = int.Parse(Tools.UrlDecode(Request.QueryString["shiftId"]));

                    //info = unitOfWork.GenericRepositories<DailyKnittings>().Get(x => x.UnitId == UnitId && x.KnittingDate == KnittingDate).ToList();
                    lblProductionEntry.Text = "Update Monthly Knitting Plan:";
                }

                tbxMonth.Text = DateTime.Now.ToString("yyyy-MM");

                PopulateEntryInfo();
            }
        }

        int Month
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["Month"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["Month"] = value; }
        }

        int Year
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["Year"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["Year"] = value; }
        }

        int KnittingPlanId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["KnittingPlanId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["KnittingPlanId"] = value; }
        }
        private void PopulateEntryInfo()
        {
            DataTable dt = new DataTable();
            if (KnittingPlanId != 0)
            {
                var planInfo = unitOfWork.GenericRepositories<MonthlyKnittingPlan>().GetByID(KnittingPlanId);
                if(planInfo != null)
                {
                    tbxMonth.Text = new DateTime(planInfo.Year,planInfo.Month,1).ToString("yyyy-MM");
                }
                dt = unitOfWork.GetDataTableFromSql($@"SELECT BuyerId,StyleId,MachineBrandId,FinishingUnitId,KnittingMCGauge as KnittingMachineGauge,CAST(KnittingTime as nvarchar(max)) as KnittingTime, FORMAT(YarnETA,'yyyy-MM-dd') as ETA, FORMAT(PPApprDate,'yyyy-MM-dd') as PPApprDate, FORMAT(TOD,'yyyy-MM-dd') as TOD,CAST(Target as nvarchar(max)) as Target FROM MonthlyKnittingPlan WHERE [KnittingPlanId]='{KnittingPlanId}'");
            }
            else
            {
                dt = unitOfWork.GetDataTableFromSql(@"SELECT 0 as BuyerId,0 as StyleId, 0 as MachineBrandId,0 as FinishingUnitId,'' as KnittingMachineGauge,'' as KnittingTime, '' as ETA, '' as PPApprDate, '' as TOD, '' as Target");
            }

            this.ViewState["CurrentTable"] = dt;
            this.rptEntryInfo.DataSource = dt;
            this.rptEntryInfo.DataBind();

            if (dt.Rows.Count <= 0)
            {
                this.lblNoKnittingGoingOn.Visible = false;
                this.pnlKnittingEntry.Visible = true;
                if (this.Month != 0)
                {
                    this.lnkbtnSaveEntries.Visible = false;
                    this.lnkbtnUpdateEntries.Visible = true;
                }
                else
                {
                    this.lnkbtnSaveEntries.Visible = true;
                    this.lnkbtnUpdateEntries.Visible = false;
                }
            }
            else
            {
                this.lblNoKnittingGoingOn.Visible = false;
                this.pnlKnittingEntry.Visible = true;
                if (this.KnittingPlanId != 0)
                {
                    this.lnkbtnSaveEntries.Visible = false;
                    this.lnkbtnUpdateEntries.Visible = true;
                    //var btnAddRow = (Button)rptEntryInfo.Controls[rptEntryInfo.Controls.Count - 1].Controls[0].FindControl("btnAddRow");
                 
                    var btnAddRow = (Button)Tools.FindControl(rptEntryInfo, "btnAddRow");
                    btnAddRow.Visible = false;
                }
                else
                {
                    this.lnkbtnSaveEntries.Visible = true;
                    this.lnkbtnUpdateEntries.Visible = false;
                }
            }
           

            
        }

        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var stBuyerId = DataBinder.Eval(e.Item.DataItem, "BuyerId").ToString();
                var ddlBuyers = (DropDownList)e.Item.FindControl("ddlBuyers");
                var ddlFinishingUnit = (DropDownList)e.Item.FindControl("ddlFinishingUnit");
                LoadBuyerDropdown(ddlBuyers);
                LoadFinishingUnitDropdown(ddlFinishingUnit);
                if (stBuyerId != "")
                {
                    var buyerId = int.Parse(stBuyerId);
                    var styleId = int.Parse(DataBinder.Eval(e.Item.DataItem, "StyleId").ToString());
                    var MachineBrandId = int.Parse(DataBinder.Eval(e.Item.DataItem, "MachineBrandId").ToString());
                    var FinishingUnitId = int.Parse(DataBinder.Eval(e.Item.DataItem, "FinishingUnitId").ToString() ?? "0");
                    var ddlStyles = (DropDownList)e.Item.FindControl("ddlStyles");
                    var ddlMachineBrand = (DropDownList)e.Item.FindControl("ddlMachineBrand");

                    LoadStyleDropdown(buyerId, ddlStyles);
                    LoadMachinBrandDropdown(ddlMachineBrand);
                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, buyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleId);
                    ddlMachineBrand.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrand, MachineBrandId);
                    ddlFinishingUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlFinishingUnit, FinishingUnitId);

                }



            }
        }


        protected void LoadYarnIssueForSizes()
        {
            dtYarnIssuedForSizes = productionManager.GetYarnIssuedForSizesByStyle(styleId, unitId, buyerColorId);
        }



        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {

            try
            {

                string stKnittingDate = tbxMonth.Text;
                if (string.IsNullOrEmpty(stKnittingDate))
                {
                    ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.error('Please select a month!')", true);
                }
                else
                {

                    DateTime knittingMonthYear = DateTime.Parse(stKnittingDate);
                    var month = knittingMonthYear.Month;
                    var year = knittingMonthYear.Year;


                    //var exist = unitOfWork.GenericRepositories<MonthlyKnittingPlan>().IsExist(x => x.Month == month && x.Year == year);
                    var exist = false;
                    if (!exist)
                    {

                        DataTable dtDailyKnittingInfo = this.DailyKnittingInfo();
                        int? machineUsed = new int?(0);
                        int? production = new int?(0);
                        int hasRow = 0;
                        var userId = CommonMethods.SessionInfo.UserName;
                        for (int i = 0; i < this.rptEntryInfo.Items.Count; i++)
                        {
                            DropDownList ddlBuyers = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlBuyers");
                            DropDownList ddlStyles = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlStyles");
                            DropDownList ddlMachineBrand = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlMachineBrand");
                            DropDownList ddlFinishingUnit = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlFinishingUnit");
                            TextBox tbxKnittingMCGauge = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxKnittingMCGauge");
                            TextBox tbxKnittingTime = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxKnittingTime");
                            TextBox tbxYarnETA = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxYarnETA");
                            TextBox tbxPPApprDate = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxPPApprDate");
                            TextBox tbxTOD = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxTOD");
                            TextBox tbxTarget = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxTarget");

                            if ((ddlBuyers.SelectedValue != "") && (ddlStyles.SelectedValue != "") && (ddlMachineBrand.SelectedValue != "") && (ddlFinishingUnit.SelectedValue != "") && (tbxKnittingMCGauge.Text != "") && (tbxKnittingTime.Text != "") /*&& (tbxYarnETA.Text != "") && (tbxPPApprDate.Text != "") && (tbxTOD.Text != "") */&& (tbxTarget.Text != ""))
                            {

                                var mkp = new MonthlyKnittingPlan()
                                {
                                    BuyerId = int.Parse(ddlBuyers.SelectedValue),
                                    StyleId = int.Parse(ddlStyles.SelectedValue),
                                    MachineBrandId = int.Parse(ddlMachineBrand.SelectedValue),
                                    FinishingUnitId = int.Parse(ddlFinishingUnit.SelectedValue),
                                    KnittingMCGauge = tbxKnittingMCGauge.Text,
                                    KnittingTime = int.Parse(tbxKnittingTime.Text),
                                    Month = month,
                                    Year = year,
                                    Target = int.Parse(tbxTarget.Text),
                                    CreatedBy = userId,
                                    CreateDate = DateTime.Now
                                };

                                if (!string.IsNullOrEmpty(tbxYarnETA.Text))
                                {
                                    mkp.YarnETA = DateTime.Parse(tbxYarnETA.Text);
                                }
                                if (!string.IsNullOrEmpty(tbxPPApprDate.Text))
                                {
                                    mkp.PPApprDate = DateTime.Parse(tbxPPApprDate.Text);
                                }
                                if (!string.IsNullOrEmpty(tbxTOD.Text))
                                {
                                    mkp.TOD = DateTime.Parse(tbxTOD.Text);
                                }

                                unitOfWork.GenericRepositories<MonthlyKnittingPlan>().Insert(mkp);
                                hasRow++;
                            }

                            if (hasRow <= 0)
                            {
                                ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.error('Entries could not be saved. Please enter valid data.')", true);
                            }
                            else
                            {
                                unitOfWork.Save();
                                ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                                ScriptManager.RegisterStartupScript(this, base.GetType(), "Script", "reloadPage();", true);

                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.error('Entries already exist for this month');", true);
                    }
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message.Replace("\'", "") + "')", true);
            }
        }



        private void LoadBuyerDropdown(DropDownList ddl)
        {
            CommonMethods.LoadBuyerDropdownForUser(ddl, 1, 0, 0);
        }

        private void LoadFinishingUnitDropdown(DropDownList ddl)
        {
            CommonMethods.LoadDropdown(ddl, "FinishingUnits", 1, 0, 0);
        }

        private void LoadColorDropdown(int styleId, DropDownList ddl)
        {
            var dt = this.orderManager.GetColorsByStyleId(styleId);
            var dr = dt.NewRow();
            dr["BuyerColorId"] = "";
            dr["ColorDescription"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);
            ddl.DataSource = dt;
            ddl.DataTextField = "ColorDescription";
            ddl.DataValueField = "BuyerColorId";
            ddl.DataBind();
        }

        private void LoadMachinBrandDropdown(DropDownList ddl)
        {

            string sql = $@"SELECT CAST([Id] as nvarchar(max)) as MachineBrandId, MachineBrand =CASE 
			    WHEN [GaugeLowRange] = [GaugeHighRange]
				    THEN [BrandName] + ' (' + CAST([GaugeLowRange] AS varchar(12)) + ')' 
			    ELSE [BrandName] + ' (' + CAST([GaugeLowRange] AS varchar(12)) + ' - ' + CAST([GaugeHighRange] AS varchar(12)) + ')' 
		    END FROM [dbo].[MachineBrands] ORDER BY BrandName";
            var dt = unitOfWork.GetDataTableFromSql(sql);
            DataRow dr = dt.NewRow();
            dr["MachineBrandId"] = "";
            dr["MachineBrand"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);

            ddl.DataTextField = "MachineBrand";
            ddl.DataValueField = "MachineBrandId";
            ddl.DataSource = dt;
            ddl.DataBind();
        }

        private void LoadSizeGrid(int styleId, GridView gridView)
        {
            gridView.Columns.Clear();
            // this.dtSizes = this.buyerManager.GetSizes(buyerId);
            this.dtSizes = this.buyerManager.GetStyleSizes(styleId);

            DataTable dtOneRow = new DataTable();
            DataRow dr = null;
            dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dr = dtOneRow.NewRow();
            dr["RowNumber"] = 1;
            dtOneRow.Rows.Add(dr);
            if (this.dtSizes.Rows.Count > 0)
            {
                TemplateField templateField = null;
                for (int i = 0; i < this.dtSizes.Rows.Count; i++)
                {
                    templateField = new TemplateField()
                    {
                        HeaderText = this.dtSizes.Rows[i][1].ToString()
                    };
                    gridView.Columns.Add(templateField);
                }
            }
            gridView.DataSource = dtOneRow;
            gridView.DataBind();
        }

        protected void LoadSizeInfo()
        {
            //this.dtSizes = this.buyerManager.GetSizes(this.buyerId);

            this.dtSizes = this.buyerManager.GetStyleSizes(this.styleId);

        }

        private void LoadStyleDropdown(int buyerId, DropDownList ddl)
        {
            CommonMethods.LoadDropdownById(ddl, buyerId, "BuyerStyles", 1, 0);
        }

        public DataTable DailyKnittingInfo()
        {
            DataTable dt = new DataTable("DailyKnittingInfo");
            dt.Columns.Add("KnittingDate", typeof(string));
            dt.Columns.Add("StyleId", typeof(int));
            dt.Columns.Add("BuyerColorId", typeof(int));
            dt.Columns.Add("UnitId", typeof(int));
            dt.Columns.Add("ShiftId", typeof(int));
            dt.Columns.Add("SizeId", typeof(int));
            dt.Columns.Add("MachineBrandId", typeof(int));
            dt.Columns.Add("FinishingUnitId", typeof(int));
            dt.Columns.Add("MachineGauge", typeof(string));
            dt.Columns.Add("JacquardPortion", typeof(string));
            dt.Columns.Add("NumOfMC", typeof(int));
            // dt.Columns.Add("NumMCAtNight", typeof(int));
            //dt.Columns.Add("Target", typeof(int));
            dt.Columns.Add("ProductionQty", typeof(int));
            //dt.Columns.Add("NightTarget", typeof(int));
            //dt.Columns.Add("NightProduction", typeof(int));
            //dt.Columns.Add("QuantityDelivered", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(string));
            return dt;
        }

        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {
            try
            {
                string stKnittingDate = tbxMonth.Text;
                if (string.IsNullOrEmpty(stKnittingDate))
                {
                    ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.error('Please select a month!')", true);
                }
                else
                {
                    DateTime knittingMonthYear = DateTime.Parse(stKnittingDate);
                    var month = knittingMonthYear.Month;
                    var year = knittingMonthYear.Year;

                    //var oldEntries = unitOfWork.GenericRepositories<MonthlyKnittingPlan>().Get(x => x.Month == month && x.Year == year).ToList();
                    //foreach (var item in oldEntries)
                    //{
                    //    unitOfWork.GenericRepositories<MonthlyKnittingPlan>().Delete(item);
                    //}

                    var mkp = unitOfWork.GenericRepositories<MonthlyKnittingPlan>().GetByID(KnittingPlanId);
                    if (mkp != null)
                    {



                        DataTable dtDailyKnittingInfo = this.DailyKnittingInfo();
                        int? machineUsed = new int?(0);
                        int? production = new int?(0);
                        int hasRow = 0;
                        var userId = CommonMethods.SessionInfo.UserName;
                        for (int i = 0; i < this.rptEntryInfo.Items.Count; i++)
                        {
                            DropDownList ddlBuyers = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlBuyers");
                            DropDownList ddlStyles = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlStyles");
                            DropDownList ddlMachineBrand = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlMachineBrand");
                            DropDownList ddlFinishingUnit = (DropDownList)this.rptEntryInfo.Items[i].FindControl("ddlFinishingUnit");
                            TextBox tbxKnittingMCGauge = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxKnittingMCGauge");
                            TextBox tbxKnittingTime = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxKnittingTime");
                            TextBox tbxYarnETA = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxYarnETA");
                            TextBox tbxPPApprDate = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxPPApprDate");
                            TextBox tbxTOD = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxTOD");
                            TextBox tbxTarget = (TextBox)this.rptEntryInfo.Items[i].FindControl("tbxTarget");

                            if ((ddlBuyers.SelectedValue != "") && (ddlStyles.SelectedValue != "") && (ddlMachineBrand.SelectedValue != "") && (ddlFinishingUnit.SelectedValue != "") && (tbxKnittingMCGauge.Text != "") && (tbxKnittingTime.Text != "") /*&& (tbxYarnETA.Text != "") && (tbxPPApprDate.Text != "") && (tbxTOD.Text != "") */ && (tbxTarget.Text != ""))
                            {



                                mkp.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                                mkp.StyleId = int.Parse(ddlStyles.SelectedValue);
                                mkp.MachineBrandId = int.Parse(ddlMachineBrand.SelectedValue);
                                mkp.FinishingUnitId = int.Parse(ddlFinishingUnit.SelectedValue);
                                mkp.KnittingMCGauge = tbxKnittingMCGauge.Text;
                                mkp.KnittingTime = int.Parse(tbxKnittingTime.Text);
                                // mkp.YarnETA = DateTime.Parse(tbxYarnETA.Text),
                                //mkp.PPApprDate = DateTime.Parse(tbxPPApprDate.Text),
                                //mkp.TOD = DateTime.Parse(tbxTOD.Text),
                                mkp.Month = month;
                                mkp.Year = year;
                                mkp.Target = int.Parse(tbxTarget.Text);
                                mkp.UpdatedBy = userId;
                                mkp.UpdateDate = DateTime.Now;

                                if (!string.IsNullOrEmpty(tbxYarnETA.Text))
                                {
                                    mkp.YarnETA = DateTime.Parse(tbxYarnETA.Text);
                                }
                                if (!string.IsNullOrEmpty(tbxPPApprDate.Text))
                                {
                                    mkp.PPApprDate = DateTime.Parse(tbxPPApprDate.Text);
                                }
                                if (!string.IsNullOrEmpty(tbxTOD.Text))
                                {
                                    mkp.TOD = DateTime.Parse(tbxTOD.Text);
                                }

                                unitOfWork.GenericRepositories<MonthlyKnittingPlan>().Update(mkp);
                                hasRow++;
                            }

                            if (hasRow <= 0)
                            {
                                ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.error('Entries could not be saved. Please enter valid data.')", true);
                            }
                            else
                            {
                                unitOfWork.Save();
                                ScriptManager.RegisterStartupScript(this, base.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewMonthlyKnittingPlans.aspx');", true);

                            }
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message.Replace("\'", "") + "')", true);
            }

        }


        private void AddNewRowToGrid()
        {
            lblShiftTotal.Text = "";

            //List<DataTable> sizeAndQtyTableList = new List<DataTable>();
            //int unitId = int.Parse(((DropDownList)this.rptGeneralInfo.Items[0].FindControl("ddlUserUnits")).SelectedValue);
            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {
                        DropDownList ddlBuyers = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlBuyers");
                        DropDownList ddlStyles = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                        DropDownList ddlMachineBrand = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlMachineBrand");
                        DropDownList ddlFinishingUnit = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlFinishingUnit");
                        TextBox tbxKnittingMCGauge = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxKnittingMCGauge");
                        TextBox tbxKnittingTime = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxKnittingTime");
                        TextBox tbxYarnETA = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxYarnETA");
                        TextBox tbxPPApprDate = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxPPApprDate");
                        TextBox tbxTOD = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxTOD");
                        TextBox tbxTarget = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxTarget");

                        dtCurrentTable.Rows[rowIndex]["BuyerId"] = (ddlBuyers.SelectedValue == "" ? "0" : ddlBuyers.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["StyleId"] = (ddlStyles.SelectedValue == "" ? "0" : ddlStyles.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["MachineBrandId"] = (ddlMachineBrand.SelectedValue == "" ? "0" : ddlMachineBrand.SelectedValue);
                        dtCurrentTable.Columns["FinishingUnitId"].ReadOnly = false;
                        dtCurrentTable.Rows[rowIndex]["FinishingUnitId"] = int.Parse(ddlFinishingUnit.SelectedValue == "" ? "0" : ddlFinishingUnit.SelectedValue);
                        //dtCurrentTable.Rows[rowIndex]["BuyerColorId"] = (ddlColors.SelectedValue == "" ? "0" : ddlColors.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["KnittingMachineGauge"] = tbxKnittingMCGauge.Text;
                        dtCurrentTable.Rows[rowIndex]["KnittingTime"] = tbxKnittingTime.Text;
                        dtCurrentTable.Rows[rowIndex]["ETA"] = tbxYarnETA.Text;
                        dtCurrentTable.Rows[rowIndex]["PPApprDate"] = tbxPPApprDate.Text;
                        dtCurrentTable.Rows[rowIndex]["TOD"] = tbxTOD.Text;
                        dtCurrentTable.Rows[rowIndex]["Target"] = tbxTarget.Text;
                        DataTable sizeAndQtyTable = new DataTable();
                        sizeAndQtyTable.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                        sizeAndQtyTable.Columns.Add(new DataColumn("Column0", typeof(string)));

                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["BuyerId"] = 0;
                            drCurrentRow["StyleId"] = 0;
                            drCurrentRow["MachineBrandId"] = 0;
                            drCurrentRow["FinishingUnitId"] = 0;
                            drCurrentRow["KnittingMachineGauge"] = "";
                            drCurrentRow["KnittingTime"] = "";
                            drCurrentRow["ETA"] = "";
                            drCurrentRow["PPApprDate"] = "";
                            drCurrentRow["TOD"] = "";
                            drCurrentRow["Target"] = "";
                        }
                    }
                    dtCurrentTable.Rows.InsertAt(drCurrentRow, dtCurrentTable.Rows.Count);
                    this.ViewState["CurrentTable"] = dtCurrentTable;
                    this.rptEntryInfo.DataSource = dtCurrentTable;
                    this.rptEntryInfo.DataBind();
                }
            }
            this.SetPreviousData();
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            this.AddNewRowToGrid();
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlBuyer = (DropDownList)sender;
            DropDownList ddlStyle = (DropDownList)((RepeaterItem)ddlBuyer.NamingContainer).FindControl("ddlStyles");
            DropDownList ddlMachineBrand = (DropDownList)((RepeaterItem)ddlBuyer.NamingContainer).FindControl("ddlMachineBrand");

            if (ddlBuyer.SelectedValue == "")
            {
                ddlStyle.Items.Clear();
                ddlMachineBrand.Items.Clear();
            }
            else
            {
                int buyerId = int.Parse(ddlBuyer.SelectedValue);
                this.LoadStyleDropdown(buyerId, ddlStyle);
                this.LoadMachinBrandDropdown(ddlMachineBrand);
            }
        }


        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                //List<DataTable> sizeAndQtyTableList = (List<DataTable>)this.ViewState["SizeAndQtyTableList"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DropDownList ddlBuyers = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlBuyers");
                        DropDownList ddlStyles = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                        DropDownList ddlMachineBrand = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlMachineBrand");
                        DropDownList ddlFinishingUnit = (DropDownList)this.rptEntryInfo.Items[rowIndex].FindControl("ddlFinishingUnit");
                        TextBox tbxKnittingMCGauge = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxKnittingMCGauge");
                        TextBox tbxKnittingTime = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxKnittingTime");
                        TextBox tbxYarnETA = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxYarnETA");
                        TextBox tbxPPApprDate = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxPPApprDate");
                        TextBox tbxTOD = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxTOD");
                        TextBox tbxTarget = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxTarget");

                        ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["BuyerId"].ToString()) ? "0" : dt.Rows[i]["BuyerId"].ToString())));
                        ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["StyleId"].ToString()) ? "0" : dt.Rows[i]["StyleId"].ToString())));
                        ddlMachineBrand.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrand, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["MachineBrandId"].ToString()) ? "0" : dt.Rows[i]["MachineBrandId"].ToString())));
                        ddlFinishingUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlFinishingUnit, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["FinishingUnitId"].ToString()) ? "0" : dt.Rows[i]["FinishingUnitId"].ToString())));

                        tbxKnittingMCGauge.Text = dt.Rows[i]["KnittingMachineGauge"].ToString();
                        tbxKnittingTime.Text = dt.Rows[i]["KnittingTime"].ToString();
                        tbxYarnETA.Text = dt.Rows[i]["ETA"].ToString();
                        tbxPPApprDate.Text = dt.Rows[i]["PPApprDate"].ToString();
                        tbxTOD.Text = dt.Rows[i]["TOD"].ToString();
                        tbxTarget.Text = dt.Rows[i]["Target"].ToString();
                        rowIndex++;
                    }
                }
            }
        }
    }
}