﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="AddKnittingPlanV2.aspx.cs" Inherits="MBTracker.Pages.Productions.AddKnittingPlanV2" Async="true" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .width-100 {
            width: 100% !important;
            /*transition: ease 0.3s;*/
            /*-webkit-transition-duration: 0.3s;*/
        }

        .display-none {
            /*width:0px !important;*/
            display: none !important;
            transition: ease 0.3s;
        }

        #entryForm {
            -webkit-transition-duration: 0.3s;
        }

        #otherInfo {
            -webkit-transition-duration: 0.3s;
            transition: visibility 0s, opacity 0.5s linear;
        }
    </style>
    <script>
        //$('#btnViewMachineAvailability').click(function () {
        //    alert('working');
        //    //var w = window.open();
        //    //var html = $("#DivMachineAvailable").html();
        //    //$(w.document.body).html(html);
        //});

        function showAvailability() {
            var res = $('#cpMain_ddlMonthYears').val();
            if (res != "") {
                let params = `scrollbars=yes,resizable=yes,status=yes,`;
                var win = window.open(params);
                var headhtml = `<html><head><title>Machine Availability</title><link href="/css/bootstrap.min.css" rel="stylesheet" />
                <link type = "text/css" href = "/icomoon/style.css" rel = "stylesheet" />
                <link type="text/css" href="/css/main.css" rel="stylesheet" />
                <link type="text/css" href="/css/custom.css" rel="stylesheet" /><style>tr{background-color: white;} tr,td{font-size:13px}.table { width: 100%; margin-bottom: 0px; } .controls  > #data-table{ padding: 20px;} .widget{background-color: #fff;}</style></head><body>`;
                win.document.write(headhtml);
                var html = $("#DivMachineAvailable").html();
                win.document.write(html);
                win.document.write('</body></html>');
            }
            else {
                toastr.error('Please select a month.');
            }

        };

        function toggle() {
            $('#entryForm').toggleClass('width-100');
            $('#otherInfo').toggleClass('display-none');
            if ($('#toggleBtn').attr("value") == '>') {
                $('#toggleBtn').attr("value", '<');
            }
            else {
                $('#toggleBtn').attr("value", '>');
            }
            //console.log($('#toggleBtn').attr("value"));
        }
    </script>
    <%--<div class="row-fluid">
     <div class="span12">
               <div class="widget">

               
                <div class="clearfix"></div>
            </div>
      </div>
</div> --%>
    <style>
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 3px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        .ui-dialog {
            height: auto;
            width: 40% !important;
            top: 200px;
            left: 30% !important;
            display: block;
            z-index: 101;
        }
    </style>



    <div class="row-fluid">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblMachinePlanning" Text="Add a Knitting Plan:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 15px">
                            <%-- <span style="font-weight: 700; padding-right: 400px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field--%>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <asp:Label ID="lblStyle" style="padding:0px !important" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <%--<div class="control-group">
                            <label for="inputOrder" class="control-label">
                                <asp:Label ID="lblOrder" runat="server" Text="Select Order:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlOrders_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlOrders"><span style="font-weight: 700; color: #CC0000">Please select an order.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span9" id="entryForm">
            <asp:Label ID="lblNoPlanningInfo" runat="server" Visible="false" Text="<br/><br/>No planning info found." BackColor="#ffff00"></asp:Label>
            <asp:Panel ID="pnlAssignMachinesToOrder" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-body">
                        <%--<input type="button" id="toggleBtn" class="btn btn-success" onclick="toggle()" style="margin-right: -35px; float: right; padding: 7px;" value=">" />--%>

                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group" style="min-height: 75px;">
                                <%-- <label for="inputOrder" class="control-label">
                                    <asp:Label ID="Label3" runat="server" Text="View Machine Availability :" BackColor="#ffff00" Font-Bold="true"></asp:Label></label>--%>
                                <div class="col-md-7" style="padding-left: 0px;">
                                    <div class="col-md-4" style="padding-left: 0px; padding-right: 5px">
                                        <div class="controls controls-row">
                                            <label for="inputOrder" class="control-label">
                                                <asp:Label ID="Label2" runat="server" Text="Start Date :" Font-Bold="true" Style="display: inline !important"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <asp:TextBox ID="tbxFromDate" runat="server" placeholder="Start date" TextMode="Date" Style="min-height: 34px" Width="100%"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding-left: 5px; padding-right: 0px">
                                        <div class="controls controls-row">
                                            <label for="inputOrder" class="control-label">
                                                <asp:Label ID="Label3" runat="server" Text="End Date :" Font-Bold="true" Style="display: inline !important"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <asp:TextBox ID="tbxToDate" runat="server" placeholder="End date" TextMode="Date" Style="min-height: 34px" Width="100%"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="inputOrder" class="control-label">
                                            <asp:Label ID="Label12" runat="server" Text="" Font-Bold="true" Style="display: inline !important">&nbsp;</asp:Label></label>
                                        <asp:Button type="button" ID="generateEntryForm" runat="server" AutoPostBack="true" OnClick="generateEntryForm_Click" class="btn btn-success" Style="margin-left: 10px; float: left; height: 34px; width: 85%" Text="Generate Form" />
                                    </div>
                                </div>
                                <div class="col-md-5" style="padding-right: 0px;">
                                    <div class="controls controls-row">
                                        <label for="inputOrder" class="control-label">
                                            <asp:Label ID="Label11" runat="server" Text="View Machine Availability :" Font-Bold="true" Style="display: inline !important"></asp:Label></label>
                                        <asp:DropDownList ID="ddlMonthYears" runat="server" AutoPostBack="true" Display="Dynamic" Width="67%" CssClass="form-control" OnSelectedIndexChanged="ddlMonthYears_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlMonthYears"><span style="font-weight: 700; color: #CC0000">Please select production month.</span></asp:RequiredFieldValidator>
                                        <input type="button" id="btnViewMachineAvailability" onclick="showAvailability()" class="btn btn-success" style="margin-left: 10px; float: right; height: 34px; width: 30%" value="View" />
                                    </div>
                                </div>
                            </div>
                            <div runat="server" visible="false" id="divEntryForm">
                                <div class="control-group">
                                    <label for="inputOrder" class="control-label">
                                        <asp:Label ID="lblTitle" runat="server" Text="Add a New Plan:" Font-Bold="true"></asp:Label></label>

                                    <div class="controls controls-row">
                                        <asp:Repeater ID="rptIssueQuantityAndMachinesPerUnit" OnItemDataBound="rptIssueQuantityAndMachinesPerUnit_ItemDataBound" runat="server">
                                            <HeaderTemplate>
                                                <table id="data-table-9" class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                <asp:Label ID="lblProductionUnitLabel" runat="server" Text="Units"></asp:Label></th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center; vertical-align: middle">
                                                        <asp:Label runat="server" ID="lblUnitName" Text='<%#Eval("UnitName") %>'></asp:Label><asp:Label ID="lblUnitId" runat="server" Visible="false" Text='<%#Eval("ProductionUnitId") %>'></asp:Label></td>
                                                    <td>
                                                        <asp:Repeater ID="rptUnitMachines" runat="server" OnItemDataBound="rptUnitMachines_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="data-table-1" class="table table-bordered table-hover" style="table-layout: fixed">
                                                                    <thead>
                                                                        <tr>
                                                                            <th style="width: 15%">
                                                                                <asp:Label ID="lblProductionUnitLabel" runat="server" Text="Machine<br/> Brand"></asp:Label></th>

                                                                            <th style="width: 10%">
                                                                                <asp:Label ID="lblTotalMachinesLabel" runat="server" Text="Issue<br/> Quantity"></asp:Label></th>
                                                                            <th>&nbsp;</th>
                                                                            <%--  <th>
                                                                                <asp:Label ID="Label6" runat="server" Text="Number of<br/> Machines"></asp:Label></th>
                                                                            <th>
                                                                                <asp:Label ID="Label7" runat="server" Text="Start<br/> Date"></asp:Label></th>
                                                                            <th>
                                                                                <asp:Label ID="Label8" runat="server" Text="End<br/> Date"></asp:Label></th>--%>
                                                                            <th style="width: 10%">
                                                                                <asp:Label ID="Label9" runat="server" Text="Knitting<br/> Quantity"></asp:Label></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td style="text-align: center; vertical-align: middle;">
                                                                        <%#Eval("BrandName") %>
                                                                        <asp:Label ID="lblMCBrandId" runat="server" Visible="false" Text='<%#Eval("Id") %>'></asp:Label>
                                                                        <asp:Label ID="lblKnittingTime" runat="server" Visible="false" Text='<%#Eval("KnittingTime") %>'></asp:Label>
                                                                    </td>
                                                                    <td style="text-align: center; vertical-align: middle;">
                                                                        <asp:TextBox ID="tbxIssueQty" runat="server" placeholder="" min="0" TextMode="Number" Width="60px"></asp:TextBox></td>
                                                                    <td style="overflow-x: auto;">
                                                                        <div style="width: 100%;">
                                                                            <asp:GridView ID="gvDates" RowStyle-Height="30" Style="margin-left: 5px" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px"  OnRowCreated="gvDates_RowCreated">
                                                                            </asp:GridView>
                                                                        </div>

                                                                    </td>
                                                                    <%-- <td style="text-align: center; vertical-align: middle">
                                                                        <asp:TextBox ID="tbxNumMC" runat="server" placeholder="" min="0" TextMode="Number" Width="60px" AutoPostBack="true" OnTextChanged="tbxNumMC_TextChanged"></asp:TextBox></td>
                                                                    <td style="text-align: center; vertical-align: middle">
                                                                        <asp:TextBox ID="tbxFromDate" runat="server" placeholder="Start date" TextMode="Date" Width="150px" AutoPostBack="true" OnTextChanged="tbxFromDate_TextChanged"></asp:TextBox></td>
                                                                    <td style="text-align: center; vertical-align: middle">
                                                                        <asp:TextBox ID="tbxToDate" runat="server" placeholder="End date" TextMode="Date" Width="150px" AutoPostBack="true" OnTextChanged="tbxToDate_TextChanged"></asp:TextBox></td>--%>
                                                                    <td style="text-align: center; vertical-align: middle;">
                                                                        <asp:Label ID="lblProductionQty" runat="server" Text="" Width="80px"></asp:Label></td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tbody>
                                        </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </td>

                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                        </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrder" class="control-label">
                                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:Label ID="lblTotalProductions" runat="server" Text="Total Production: 0 Pcs" Font-Bold="true"></asp:Label>
                                        
                                        <asp:Button ID="btnUpdateMachinesForOrder" runat="server" Visible="false" class="btn btn-success btn-small pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdateMachinesForOrder_Click" />
                                        <asp:Button ID="btnSaveMachinesForOrder" runat="server" class="btn btn-success btn-small pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSaveMachinesForOrder_Click" />
                                        <asp:Button ID="btnCalculate" style="margin-right:25px" runat="server" class="btn btn-success btn-small pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Calculate&nbsp;&nbsp;&nbsp;" OnClick="CalculateProductionQty" />
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputOrder" class="control-label">
                                        <asp:Label ID="Label4" runat="server" Text=""></asp:Label></label>
                                    <div class="controls controls-row">
                                    </div>
                                </div>
                                <div style="text-align: left; font-size: 12px; line-height: 30px; padding-bottom: 5px">
                                    <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div class="span3" id="otherInfo">
            <asp:Panel ID="pnlPlanningInfo" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-body">
                        <div class="control-group">
                            <div style="width: 100%" id="divTotalOrderQuantity" runat="server" visible="false">
                            </div>
                            <br />
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label5" runat="server" Text="Planning Information:" Font-Bold="true"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:Repeater ID="rptPlanInfo" runat="server" OnItemDataBound="rptPlanInfo_ItemDataBound" OnItemCommand="rptPlanInfo_ItemCommand">
                                    <HeaderTemplate>
                                        <table id="data-table-2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th hidden>
                                                        <asp:Label ID="lblOrderQty" runat="server" Text="Order<br/>Quantity"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblMachineBrand" runat="server" Text="Machine<br/>Brand"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblMachineGauge" runat="server" Text="Machine<br/>Gauge"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblKnittingTime" runat="server" Text="Knitting<br/>Time"></asp:Label></th>
                                                    <%-- <th>
                                                            <asp:Label ID="lblCol9" runat="server" Text="Actions<br/>&nbsp;" ></asp:Label></th>--%>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td hidden style="text-align: center" width="12%"><%#Eval("OrderQuantity") %> </td>
                                            <td style="text-align: center" width="32%">
                                                <asp:Label ID="lblMCBrandName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BrandName")%>'></asp:Label><asp:Label ID="lblMCBrandId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "MachineBrandId")%>'></asp:Label>
                                                <asp:DropDownList ID="ddlMachineBrands" runat="server" Display="Dynamic" CssClass="form-control" Visible="false"></asp:DropDownList>
                                            </td>
                                            <td style="text-align: center" width="12%">
                                                <asp:Label ID="lblMCGauge" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingMachineGauge")%>'></asp:Label>
                                                <asp:TextBox ID="tbxKnittingMCGauge" runat="server" CssClass="form-control" placeholder="MC Gauge" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingMachineGauge")%>'></asp:TextBox>
                                            </td>
                                            <td style="text-align: center" width="12%">
                                                <asp:Label ID="lblKnittingTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingTime")%>'></asp:Label>
                                                <asp:TextBox ID="tbxKnittingTime" runat="server" placeholder="Knitting time" CssClass="form-control" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingTime")%>'></asp:TextBox>
                                            </td>

                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                            </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>

                        <div style="text-align: left; font-size: 12px; line-height: 20px; padding-bottom: 5px">
                            <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                        </div>
                        <asp:Panel ID="pnlShipmentInfo" runat="server" Visible="false">

                            <div class="control-group" style="line-height: 10px">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblShipmentDates" runat="server" Text="Shipment Summary:" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rptShipmentSummary" runat="server" OnItemDataBound="rptShipmentSummary_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table-3" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblCol1" runat="server" Text="Shipment<br/>Month"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblCol3" runat="server" Text="Shipment<br/> Quantity"></asp:Label></th>
                                                        <th class="hidden">
                                                            <asp:Label ID="lblCol4" runat="server" Text="Machines<br/> Required"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center; vertical-align: middle">
                                                    <asp:Label ID="lblShipmentDates" runat="server" Text=' <%#DataBinder.Eval(Container.DataItem, "ShipmentMonth")%>'> </asp:Label></td>
                                                <td style="text-align: center; vertical-align: middle">
                                                    <asp:Label ID="lblShipmentQuantity" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShipmentQuantity")%>'></asp:Label>
                                                </td>
                                                <td class="hidden" style="text-align: center; vertical-align: middle">
                                                    <asp:Label ID="lblMCRequired" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                            </table>
                                            <div class="clearfix">
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <asp:Label ID="lblNoShippingInfo" runat="server" Visible="false" Text="<br/><br/>No shipping info found." BackColor="#ffff00"></asp:Label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>

    <div class="row-fluid" style="display: none">
        <div class="span9" id="DivMachineAvailable">
            <asp:Panel ID="pnlMachinesAvailable" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="Label10" runat="server" Text=""></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rptMonthlyDates" runat="server" OnItemDataBound="rptMonthlyDates_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table-4" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblCol1" runat="server" Text="Dates"></asp:Label></th>
                                                        <th style="text-align: center">
                                                            <asp:Label ID="lblAvailableMachineMessage" runat="server" Text="Availability of Machines"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center; vertical-align: central">

                                                    <asp:Label ID="lblProducitonDate" runat="server" Text='<%#Eval("Date") %>'></asp:Label></th>
                                                </td>
                                                <td>
                                                    <div class="row-fluid">
                                                        <div class="widget-body" style="border: none">
                                                            <div class="form-horizontal no-margin">
                                                                <asp:Repeater ID="rptMachinesAvailable" OnItemDataBound="rptMachinesAvailable_ItemDataBound" runat="server">
                                                                    <HeaderTemplate>
                                                                        <table id="data-table" class="table table-bordered table-hover">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>
                                                                                        <asp:Label ID="lblProductionUnitLabel" runat="server" Text="Units"></asp:Label></th>
                                                                                    <th></th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td style="text-align: center" width="13%">
                                                                                <%#Eval("UnitName") %><asp:Label ID="lblUnitId" runat="server" Visible="false" Text='<%#Eval("ProductionUnitId") %>'></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Repeater ID="rptUnitMachinesAvailable" runat="server">
                                                                                    <HeaderTemplate>
                                                                                        <table id="data-table" class="table table-bordered table-hover">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>
                                                                                                        <asp:Label ID="lbl111" runat="server" Text="Machine Brand"></asp:Label></th>
                                                                                                    <th>
                                                                                                        <asp:Label ID="lblTotalMachinesLabel" runat="server" Text="Total Machines"></asp:Label></th>
                                                                                                    <th>
                                                                                                        <asp:Label ID="lblAlreadyBookedMachinesLabel" runat="server" Text="Booked By All Styles"></asp:Label></th>
                                                                                                    <th>
                                                                                                        <asp:Label ID="lblOrderBookedMachineLabel" runat="server" Text="Booked By This Style"></asp:Label></th>
                                                                                                    <th>
                                                                                                        <asp:Label ID="lblAvailableMachinesLabel" runat="server" Text="Machines Available"></asp:Label></th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td style="text-align: center" width="13%"><%#Eval("BrandName") %><asp:Label ID="lblMCBrandId" runat="server" Visible="false" Text='<%#Eval("Id") %>'></asp:Label></td>

                                                                                            <td style="text-align: center" width="12%"><%#Eval("TotalMachines") %></td>
                                                                                            <td style="text-align: center" width="12%">
                                                                                                <asp:Label ID="lblAlreadyBookedMachines" runat="server" Text='<%#Eval("AlreadyBooked") %>'></asp:Label></td>
                                                                                            <td style="text-align: center" width="12%">
                                                                                                <asp:Label ID="lblBookedMachinesForOrder" runat="server" Text='<%#Eval("BookedMachinesForOrder") %>'></asp:Label></td>
                                                                                            <td style="text-align: center" width="12%">
                                                                                                <asp:Label ID="lblAvailableMachines" runat="server" Text='<%#Eval("MachinesAvailable") %>'></asp:Label></td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </tbody>
                                                                </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </tbody>
                                                                </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>

                                            </tr>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                         </table>
                            <div class="clearfix">
                                        </FooterTemplate>
                                    </asp:Repeater>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
    <div class="clearfix"></div>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
