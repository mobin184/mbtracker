﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="IssueNeckForLinking.aspx.cs" Inherits="MBTracker.Pages.Productions.IssueNeckForLinking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table th {
            text-align: center;
        }
    </style>
    <div class="row-fluid">
        <div class="span9">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="Issue For Neck Linking:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row" style="padding-left: 15px; padding-right: 15px;">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                            <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label">
                                        <asp:Label ID="Label2" runat="server" Text="Received Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxIssueDate" AutoPostBack="true" TextMode="Date" Width="100%" Text="" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxIssueDate"><span style="font-weight: 700; color: #CC0000">Please select issue date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label1" runat="server" Text="Finishing Unit:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlUserFinishingUnitFloor" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlUserFinishingUnitFloor"><span style="font-weight: 700; color: #CC0000">Please select a finishing unit.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label">
                                        <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select a style.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label">
                                        <asp:Label ID="Label3" runat="server" Text="Select Color:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlColors" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" CssClass="form-control" OnSelectedIndexChanged="ddlColors_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlColors"><span style="font-weight: 700; color: #CC0000">Please select a color.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label">
                                        <asp:Label ID="Label8" runat="server" Text="Select Size:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlSize" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" CssClass="form-control" OnSelectedIndexChanged="ddlSize_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlSize"><span style="font-weight: 700; color: #CC0000">Please select a size.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="" class="control-label">
                                        <asp:Label ID="Label5" runat="server" Text="Link OP Card No:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxLinkOpCardNo" AutoPostBack="true" OnTextChanged="tbxLinkOpCardNo_TextChanged" runat="server" placeholder="Enter link op card no" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxLinkOpCardNo"><span style="font-weight: 700; color: #CC0000">Enter link op card no.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="" class="control-label">
                                        <asp:Label ID="Label6" runat="server" Text="Knit ID/TC No:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxKnitIdOrTCNo" AutoPostBack="false" ClientIDMode="Static" runat="server" placeholder="Enter knit Id/TC No" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxKnitIdOrTCNo"><span style="font-weight: 700; color: #CC0000">Enter knit Id/TC No.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="" class="control-label">
                                        <asp:Label ID="Label4" runat="server" Text="Issue Quantity:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxIssueQuantity" runat="server" TextMode="Number" placeholder="Enter issue quantity" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxIssueQuantity"><span style="font-weight: 700; color: #CC0000">Enter knit Id/TC No.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group" >
                                    <label for="" class="control-label">
                                        <asp:Label ID="Label11" runat="server" Text=""></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row" style=" margin-top:10px;">
                                       <asp:Label runat="server" ID="lblAvailableQty" Visible="false" style="font-size:16px; color:forestgreen;text-align:right" Text="Available Quantity: 100" ></asp:Label>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnSaveEntries" runat="server" ValidationGroup="save" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSaveEntries_Click"></asp:Button>
            <asp:Button ID="btnUpdateEntries" Visible="false" ValidationGroup="save" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdateEntries_Click"></asp:Button>
            <br />
        </div>
    </div>
    <div class="row-fluid" runat="server" id="divExistIssues" visible="false" style="padding-top:15px">
        <div class="span9">
            <div class="widget">
                <div class="widget-body">
                    <div class="row-fluid">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblEntry" runat="server" Text="" Font-Bold="true">Not Received Issues</asp:Label></label>
                                <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                    <asp:Repeater ID="rptExistIssues" runat="server">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lbl1" runat="server" Text="Operator Card No"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lbl2" runat="server" Text="Finishing Floor"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lbl3" runat="server" Text="Issue Date"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lbl4" runat="server" Text="Buyer "></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label8" runat="server" Text="Style "></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label9" runat="server" Text="Color"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label10" runat="server" Text="Issue Quantity"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label7" runat="server" Text="Received Status"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%#Eval("OPCardNo") %></td>
                                                <td><%#Eval("FloorName") %></td>
                                                <td><%#string.Format("{0:dd-MMM-yyyy}",Eval("IssueDate")) %></td>
                                                <td><%#Eval("BuyerName") %></td>
                                                <td><%#Eval("StyleName") %></td>
                                                <td><%#Eval("ColorDescription") %></td>
                                                <td><%#Eval("IssueQty") %></td>
                                                <td><%#Eval("IsReceived").ToString() == "1" ? "<span class='label label-success'>Received</span>": "<span class='label label-danger'>Not Received</span>" %></td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                </table>
                            <div class="clearfix"></div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
