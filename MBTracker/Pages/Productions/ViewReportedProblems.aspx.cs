﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class ViewReportedProblems : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        ProductionManager productionManager = new ProductionManager();

        int reportedProblemId;
        DataTable dtReportedProblemFiles;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);

                if (Request.QueryString["showInfo"] != null)
                {
                    var reportedProblemId = int.Parse(Tools.UrlDecode(Request.QueryString["showInfo"]));
                    var info = unitOfWork.GenericRepositories<ReportedProblems>().GetByID(reportedProblemId);
                    divBuyerAndStyle.Visible = false;

                    if (CommonMethods.HasBuyerAssignedToUser(info.BuyerId, CommonMethods.SessionInfo.UserId))
                    {
                        ShowReportedProblems(info.StyleId);
                    }
                    else
                    {
                        lblNotAuthorizedToView.Visible = true;
                    }
                }


                // EnableDisableEditButton();
                         
            }
        }

        private void EnableDisableEditButton()
        {
            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Finishing - Mgmt");

            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewReportedProblems", 1, 1);

        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlStyles.Items.Clear();
            pnlDetails.Visible = false;
            lblNoDataFound.Visible = false;
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void btnViewEntries_Click(object sender, EventArgs e)
        {
            ShowReportedProblems(int.Parse(ddlStyles.SelectedValue));

        }


        private void ShowReportedProblems(int? styleId)
        {
            try
            {
                var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetReportedProblems {styleId}");
                if (dt.Rows.Count > 0)
                {
                    rptReportedProblems.DataSource = dt;
                    rptReportedProblems.DataBind();
                    rptReportedProblems.Visible = true;
                    lblNoDataFound.Visible = false;
                    pnlDetails.Visible = true;
                }
                else
                {
                    rptReportedProblems.DataSource = null;
                    rptReportedProblems.DataBind();
                    rptReportedProblems.Visible = false;
                    lblNoDataFound.Visible = true;
                    pnlDetails.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }


        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            var id = int.Parse(e.CommandArgument.ToString());
            Response.Redirect("ReportNewProblem.aspx?Id=" + Tools.UrlEncode(id + ""));
        }

        protected void rptReportedProblems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptReportedProblemFiles = (Repeater)e.Item.FindControl("rptFiles");
                Label lblReportedProblemId = (Label)e.Item.FindControl("lblReportedProblemId");
                reportedProblemId = Convert.ToInt32(lblReportedProblemId.Text);

                dtReportedProblemFiles = productionManager.GetReportedProblemFileInfo(reportedProblemId);

                if (dtReportedProblemFiles.Rows.Count > 0)
                {
                    rptReportedProblemFiles.DataSource = dtReportedProblemFiles;
                    rptReportedProblemFiles.DataBind();
                }
            }
        }


    }
}