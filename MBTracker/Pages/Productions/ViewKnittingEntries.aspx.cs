﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class ViewKnittingEntries : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        ProductionManager productionManager = new ProductionManager();
        DataTable dtUserUnits;
        DataTable dtSizes;
        DataTable dtYarnIssuedForSizes;
        int buyerId;
        int styleId;
        int unitId;
        int buyerColorId;
        int entryRowNumber2 = 0;
        int nightTotal = 0;
        int dayTotal = 0;
        int grandTotal = 0;



        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableEditButton();
                EnableDisableDeleteButton();
                //tbxFilterDate.Text = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd");
                //LoadData();
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteKnittingEntries(UnitId, KnittingDate, ShiftId);
                }
            }
        }

        private void DeleteKnittingEntries(int unitId, DateTime knittingDate, int shiftId)
        {
            try
            {
                var lstEntries = unitOfWork.GenericRepositories<DailyKnittings>().Get(x => x.KnittingUnitId == unitId && x.KnittingDate == knittingDate && x.ShiftId == shiftId).ToList();
                foreach (var item in lstEntries)
                {
                    unitOfWork.GenericRepositories<DailyKnittings>().Delete(item);
                }
                unitOfWork.Save();
                LoadData();
                pnlSummary.Visible = false;
                pnlDetails.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        private void EnableDisableDeleteButton()
        {
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");
        }
        private void EnableDisableEditButton()
        {
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Knitting - Mgmt,Knitting - Floor");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
            pnlSummary.Visible = false;
            pnlDetails.Visible = false;

        }


        protected void LoadData()
        {
            var date = string.IsNullOrEmpty(tbxFilterDate.Text) ? "" : Convert.ToDateTime(tbxFilterDate.Text).ToString();

            if (!string.IsNullOrEmpty(date))
            {
                var userId = CommonMethods.SessionInfo.UserId;
                string sql = $"Exec usp_GetKnittingEntryByDateAndUser '{date}', 0, {userId}";
                var dt = unitOfWork.GetDataTableFromSql(sql);

                if (dt.Rows.Count > 0)
                {
                    rpt.Visible = true;
                    dataDiv.Visible = true;

                    rpt.DataSource = dt;
                    rpt.DataBind();
                    lblNoDataFound.Visible = false;
                    lblLabelMessage.Visible = true;

                    lblNightTotal.Text = "Night Total: " + nightTotal.ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    lblDayTotal.Text = "Day Total: " + dayTotal.ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    lblGrandTotal.Text = "Grand Total: " + grandTotal.ToString();

                }
                else
                {
                    rpt.Visible = false;
                    dataDiv.Visible = false;

                    lblNoDataFound.Visible = true;
                    lblLabelMessage.Visible = false;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select knitting date!')", true);
            }


        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            int unitId = int.Parse(commandArgs[0]);
            string knittingDate = DateTime.Parse(commandArgs[1]).ToString("yyyy-MM-dd");
            int shiftId = int.Parse(commandArgs[2]);

            Response.Redirect("EnterKnittingEntry.aspx?unitId=" + Tools.UrlEncode(unitId + "") + "&knittingDate=" + Tools.UrlEncode(knittingDate) + "&shiftId=" + Tools.UrlEncode(shiftId + ""));
        }

        protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        {
            pnlSummary.Visible = true;
            pnlDetails.Visible = true;
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            UnitId = int.Parse(commandArgs[0]);
            KnittingDate = DateTime.Parse(commandArgs[1]);
            ShiftId = int.Parse(commandArgs[2]);
            PopulateGeneralInfo();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(900)", true);
        }


        int UnitId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["unitId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["unitId"] = value; }
        }

        DateTime KnittingDate
        {
            get
            {
                try
                {
                    return Convert.ToDateTime(ViewState["knittingDate"]);
                }
                catch
                {
                    return DateTime.Today;
                }
            }
            set { ViewState["knittingDate"] = value; }
        }
        int ShiftId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["shiftId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["shiftId"] = value; }
        }


        public DataTable ProdEntryGeneralInfo()
        {
            DataTable dt = new DataTable("ProdEntryGeneralInfo");
            dt.Columns.Add("ProductionEntryDate", typeof(string));
            dt.Columns.Add("UserName", typeof(string));
            return dt;
        }

        private void PopulateGeneralInfo()
        {
            dtUserUnits = new CommonManager().GetProductionUnitsByUser(Convert.ToInt32(CommonMethods.SessionInfo.UserId));

            if (dtUserUnits.Rows.Count > 0)
            {
                DataTable dtProdEntryGeneralInfo = ProdEntryGeneralInfo();
                if (UnitId == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Invalid Unit Id.');", true);
                }
                else
                {
                    DataTable dtEntryUser = new CommonManager().GetKnittingEntryUser(UnitId, ShiftId, KnittingDate);

                    dtProdEntryGeneralInfo.Rows.Add(String.Format("{0:dd-MMM-yyyy}", KnittingDate), dtEntryUser.Rows[0][0].ToString());
                    PopulateEntryInfo(UnitId, KnittingDate);
                }

                rptGeneralInfo.DataSource = dtProdEntryGeneralInfo;
                rptGeneralInfo.DataBind();
            }

        }

        private void PopulateEntryInfo(int unitId, DateTime productionDate)
        {

            int userId = CommonMethods.SessionInfo.UserId;
            DataTable dt = unitOfWork.GetDataTableFromSql($"Exec usp_Get_DailyKnittingEntryInfoByUnitAndDate {unitId},{ShiftId},'{productionDate}','{userId}' ");
          

            if (dt.Rows.Count > 0)
            {
                lblNoKnittingGoingOn.Visible = false;
                rptEntryInfo.Visible = true;
                lblDetails.Visible = true;
                lblSummary.Visible = true;
            }
            else
            {
                lblNoKnittingGoingOn.Visible = true;
                rptEntryInfo.Visible = false;
                lblDetails.Visible = false;
                lblSummary.Visible = false;

            }
            rptEntryInfo.DataSource = dt;
            rptEntryInfo.DataBind();


        }

        protected void rptGeneralInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlUserUnits = (DropDownList)e.Item.FindControl("ddlUserUnits");
                ddlUserUnits.DataTextField = "UnitName";
                ddlUserUnits.DataValueField = "ProductionUnitId";
                ddlUserUnits.DataSource = dtUserUnits;
                ddlUserUnits.DataBind();
                DropDownList ddlShift = (DropDownList)e.Item.FindControl("ddlShift");
                CommonMethods.LoadDropdown(ddlShift, "Shifts", 1, 0);
                if (UnitId == 0)
                {
                    ddlUserUnits.SelectedIndex = 0;
                    ddlShift.SelectedIndex = 0;
                }
                else
                {
                    ddlUserUnits.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUserUnits, UnitId);
                    ddlShift.SelectedIndex = CommonMethods.MatchDropDownItem(ddlShift, ShiftId);
                    ddlUserUnits.Enabled = false;
                    ddlShift.Enabled = false;
                }
            }
        }


        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                int shiftId  = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "ShiftId").ToString());
                int prodQty = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Production").ToString());

                if (shiftId == 1)
                {
                    dayTotal = dayTotal + prodQty;
                    grandTotal = grandTotal + prodQty;

                }
                else if (shiftId == 2)
                {
                    nightTotal = nightTotal + prodQty;
                    grandTotal = grandTotal + prodQty;
                }
            }
        }


        protected void rptGeneralInfo_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlUserUnits = (DropDownList)e.Item.FindControl("ddlUserUnits");
                ddlUserUnits.SelectedIndexChanged += ddlUserUnits_SelectedIndexChanged;
            }
        }

        protected void ddlUserUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlUserUnits = (DropDownList)sender;
            var stProductionDate = ((TextBox)rptGeneralInfo.Items[0].FindControl("tbxDeliveryDate")).Text;
            DateTime productionDate;
            if (string.IsNullOrEmpty(stProductionDate))
            {
                productionDate = DateTime.Now;
                ((TextBox)rptGeneralInfo.Items[0].FindControl("tbxDeliveryDate")).Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
            else
            {
                productionDate = DateTime.Parse(stProductionDate);
            }
            PopulateEntryInfo(Convert.ToInt32(ddlUserUnits.SelectedValue), productionDate);
        }

        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                //GridView gvSizeQuantity1 = (GridView)e.Item.FindControl("gvSizeQuantity1");
                GridView gvSizeQuantity2 = (GridView)e.Item.FindControl("gvSizeQuantity2");
                buyerId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BuyerId").ToString());
                styleId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "StyleId").ToString());
                unitId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "UnitId").ToString());
                buyerColorId = int.Parse(DataBinder.Eval(e.Item.DataItem, "BuyerColorId").ToString());

                dtSizes = buyerManager.GetSizes(buyerId);
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        //gvSizeQuantity1.Columns.Add(templateField);
                        gvSizeQuantity2.Columns.Add(templateField);
                    }
                }

                //gvSizeQuantity1.DataSource = dtOneRow;
                //gvSizeQuantity1.DataBind();

                gvSizeQuantity2.DataSource = dtOneRow;
                gvSizeQuantity2.DataBind();
                var ddlJacquardPortion = (DropDownList)e.Item.FindControl("ddlJacquardPortion");

                if (UnitId != 0)
                {
                    var tbxMCUsed = (TextBox)e.Item.FindControl("tbxMCUsed");
                    var machinInfo = unitOfWork.GenericRepositories<DailyKnittings>().Get(x => x.StyleId == styleId && x.BuyerColorId == buyerColorId && x.KnittingUnitId == unitId && x.KnittingDate == KnittingDate && ShiftId == ShiftId).FirstOrDefault();

                    if (machinInfo != null)
                    {
                        ddlJacquardPortion.SelectedValue = machinInfo.JacquardPortion;
                        ddlJacquardPortion.Items.FindByText(machinInfo.JacquardPortion).Selected = true;
                        tbxMCUsed.Text = machinInfo.NumOfMC + "";
                    }
                }
                else
                {
                    ddlJacquardPortion.SelectedIndex = 0;
                }
            }
        }

        protected void gvSizeQuantity_RowCreated1(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblYarnIssuedBySize = null;
                LoadYarnIssueForSizes();

                for (int i = 0; i < dtYarnIssuedForSizes.Rows.Count; i++)
                {
                    lblYarnIssuedBySize = new Label();
                    lblYarnIssuedBySize.ID = "lblYarnIssuedBySize" + i.ToString();
                    lblYarnIssuedBySize.Text = dtYarnIssuedForSizes.Rows[i][0].ToString();
                    e.Row.Cells[i].Controls.Add(lblYarnIssuedBySize);
                }
            }
        }



        protected void gvSizeQuantity_RowCreated2(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblBuyerId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId");
                buyerId = int.Parse(lblBuyerId.Text);

                entryRowNumber2++;

                Label txtSizeQty = null;
                Label lblSizeId = null;
                LoadSizeInfo();

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new Label();
                    txtSizeQty.ID = "txtSizeQty2" + i.ToString();
                    txtSizeQty.Text = "";
                    txtSizeQty.Width = 60;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);

                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId2" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);

                    var enitryId = new Label();
                    enitryId.ID = "lblenitryId2" + i.ToString();
                    enitryId.Text = "";
                    enitryId.Visible = false;
                    e.Row.Cells[i].Controls.Add(enitryId);

                    if (UnitId != 0)
                    {
                        var lblStyleId = (Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId");
                        var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                        var lblUnitId = (Label)((GridView)sender).DataItemContainer.FindControl("lblUnitId");
                        
                        var lblMachineBrandId = (Label)((GridView)sender).DataItemContainer.FindControl("lblMachineBrandId");

                        var styleId = int.Parse(lblStyleId.Text);
                        var buyerColorId = int.Parse(lblBuyerColorId.Text);
                        var unitId = int.Parse(lblUnitId.Text);
                        var machineBrandId = int.Parse(lblMachineBrandId.Text);
                        var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());

                        

                        var pInfo = unitOfWork.GenericRepositories<DailyKnittings>().Get(x => x.StyleId == styleId && x.BuyerColorId == buyerColorId && x.KnittingUnitId == unitId && x.SizeId == sizeId && x.KnittingDate == KnittingDate && x.ShiftId == ShiftId && x.MachineBrandId == machineBrandId).FirstOrDefault();
                        if (pInfo != null)
                        {
                            txtSizeQty.Text = pInfo.ProductionQty + "";
                            enitryId.Text = pInfo.Id + "";
                        }
                    }
                }
            }
        }


        protected void LoadSizeInfo()
        {
            dtSizes = buyerManager.GetSizes(buyerId);
        }

        protected void LoadYarnIssueForSizes()
        {
            dtYarnIssuedForSizes = productionManager.GetYarnIssuedForSizesByStyle(styleId, unitId, buyerColorId);
        }

        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            UnitId = int.Parse(commandArgs[0]);
            KnittingDate = DateTime.Parse(commandArgs[1]);
            ShiftId = int.Parse(commandArgs[2]);

            if (UnitId != 0)
            {
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
            }
            else
            {
                var title = "Error";
                var msg = "Sorry, you can not delete it.<br />Something went wrong.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            }
        }
    }
}