﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class ViewExFactoryEntries : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        BuyerManager buyerManager = new BuyerManager();
        DataTable dtSizes;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
        }


        protected void LoadSizeInfo()
        {
            dtSizes = buyerManager.GetSizes(Convert.ToInt32(ddlBuyers.SelectedValue));
        }
        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            ddlOrders.Items.Clear();
            divExFactoryInfo.Visible = false;
            rptInfo.DataSource = null;
            rptInfo.DataBind();
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            rptColorAndDeliveryCountries.DataSource = null;
            rptColorAndDeliveryCountries.DataBind();
        }
        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlOrders.Items.Clear();
            divExFactoryInfo.Visible = false;
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            rptColorAndDeliveryCountries.DataSource = null;
            rptColorAndDeliveryCountries.DataBind();
            rptInfo.DataSource = null;
            rptInfo.DataBind();
            if (ddlStyles.SelectedValue != "")
            {
                BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
            }
        }

        private void BindOrdersByStyle(int styleId)
        {
            CommonMethods.LoadOrderDropdownByStyle(ddlOrders, styleId, 1, 0);
        }

        protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOrders.SelectedValue != "")
            {
                LoadExFactoryInfo();
            }
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            rptColorAndDeliveryCountries.DataSource = null;
            rptColorAndDeliveryCountries.DataBind();
        }

        private void LoadExFactoryInfo()
        {
            var buyerId = int.Parse(ddlBuyers.SelectedValue);
            var styleId = int.Parse(ddlStyles.SelectedValue);
            var orderId = int.Parse(ddlOrders.SelectedValue);

            var dtExInfo = unitOfWork.GetDataTableFromSql($"Exec usp_GetExFactoryInfoByBuyerStyleAndOrder {buyerId},{styleId},{orderId},{CommonMethods.SessionInfo.UserId}");
            divExFactoryInfo.Visible = true;
            if (dtExInfo.Rows.Count > 0)
            {
                rptInfo.DataSource = dtExInfo;
                rptInfo.DataBind();
                lblNoDataFound.Visible = false;
            }
            else
            {
                rptInfo.DataSource = null;
                rptInfo.DataBind();
                lblNoDataFound.Visible = true;
            }            
        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            var exFactoryId = int.Parse(e.CommandArgument.ToString());
            Response.Redirect("GoodsExFactoryEntry.aspx?exFactoryId=" + exFactoryId);
        }

        protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        {
            ExFactoryId = int.Parse(e.CommandArgument.ToString());
            LoadColorByExFactoryId();
            pnlColorDeliveryCountryAndQuantity.Visible = true;
            ScriptManager.RegisterStartupScript(this,this.GetType(), "message", "scrollDown()", true);
        }

        int ExFactoryId
        {
            get
            {
                try
                {
                    return int.Parse(ViewState["exFactoryId"].ToString());
                }
                catch (Exception)
                {
                    return 0;
                }
            }
            set
            {
                ViewState["exFactoryId"] = value;
            }
        }

        private void LoadColorByExFactoryId()
        {
            try
            {
                var dtColors = unitOfWork.GetDataTableFromSql($"Exec usp_GetShipmentColorsByExFcatoryId {ExFactoryId}");
                if (dtColors.Rows.Count > 0)
                {
                    rptColorAndDeliveryCountries.DataSource = dtColors;
                    rptColorAndDeliveryCountries.DataBind();
                    pnlColorDeliveryCountryAndQuantity.Visible = true;
                    lblColorOrDeliveryCountryNotFound.Visible = false;
                }
                else
                {
                    rptColorAndDeliveryCountries.DataSource = null;
                    rptColorAndDeliveryCountries.DataBind();
                    pnlColorDeliveryCountryAndQuantity.Visible = false;
                    lblColorOrDeliveryCountryNotFound.Text = "No Color found for shipment.";
                    lblColorOrDeliveryCountryNotFound.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void rptColorAndDeliveryCountries_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //var shipmnetId = int.Parse(ddlShipment.SelectedValue);
                var rptDeliveryCountryAndQty = (Repeater)e.Item.FindControl("rptDeliveryCountryAndQty");

                var dtDeliveryCountries = unitOfWork.GetDataTableFromSql($"EXEC usp_GetDeliveryCountryByExFactoryId {ExFactoryId}");
                rptDeliveryCountryAndQty.DataSource = dtDeliveryCountries;
                rptDeliveryCountryAndQty.DataBind();
            }
        }

        protected void gvSizeAndQty_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label txtSizeQty = null;
                Label lblSizeId = null;
                LoadSizeInfo();

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new Label();
                    txtSizeQty.ID = "txtSizeQty1" + i.ToString();

                    var sizeId = Convert.ToInt32(dtSizes.Rows[i][0].ToString());
                    var lblDeliveryCountryId = (Label)((GridView)sender).DataItemContainer.FindControl("lblDeliveryCountryId");
                    var deleveryCountryId = Convert.ToInt32(lblDeliveryCountryId.Text);

                    var lblColorId = (Label)((GridView)sender).DataItemContainer.NamingContainer.NamingContainer.FindControl("lblColorId");                
                    var colorId = Convert.ToInt32(lblColorId.Text);
                    var shipmentColorCountrySize = unitOfWork.GenericRepositories<GoodsExFactoryColorSizes>().Get(x => x.ExFactoryId == ExFactoryId && x.BuyerColorId == colorId && x.SizeId == sizeId).FirstOrDefault();

                    if (shipmentColorCountrySize != null)
                    {
                        txtSizeQty.Text = shipmentColorCountrySize.ExFactoryQty + "";
                        var lblShipmentColorCountrySize = new Label();
                        lblShipmentColorCountrySize.ID = "lblShipmentColorCountrySize1" + i.ToString();
                        lblShipmentColorCountrySize.Text = shipmentColorCountrySize.Id + "";
                        lblShipmentColorCountrySize.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblShipmentColorCountrySize);
                    }
                    else
                    {
                        txtSizeQty.Text = "";
                        var lblShipmentColorCountrySize = new Label();
                        lblShipmentColorCountrySize.ID = "lblShipmentColorCountrySize1" + i.ToString();
                        lblShipmentColorCountrySize.Text = "";
                        lblShipmentColorCountrySize.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblShipmentColorCountrySize);
                    }


                    txtSizeQty.Width = 30;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);

                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId1" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);
                }
            }
        }

        protected void rptDeliveryCountryAndQty_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gvSizeAndQty = (GridView)e.Item.FindControl("gvSizeAndQty");
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);
                LoadSizeInfo();
                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeAndQty.Columns.Add(templateField);

                    }

                }

                gvSizeAndQty.DataSource = dtOneRow;
                gvSizeAndQty.DataBind();
            }
        }
    }
}