﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class DeliverKnittedItemsForLinking : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                tbxDeliveryDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                CommonMethods.LoadFinishingFloorDropDown(ddlUserFinishingUnitFloor);


                //var dtUsersFinishingFloor = new CommonManager().GetFinishingUnitFloorsByUser(CommonMethods.SessionInfo.UserId);
                //ddlUserFinishingUnitFloor.DataSource = dtUsersFinishingFloor;
                //ddlUserFinishingUnitFloor.DataTextField = "FloorName";
                //ddlUserFinishingUnitFloor.DataValueField = "FinishingUnitFloorId";
                //ddlUserFinishingUnitFloor.DataBind();

                if (Request.QueryString["deliveryId"] != null)
                {
                    DeliveryId = int.Parse(Tools.UrlDecode(Request.QueryString["deliveryId"]));
                    var delInfo = unitOfWork.GenericRepositories<KnittedItemsDeliveredToLinkingSection>().GetByID(DeliveryId);
                    BindDataToUI(delInfo);
                }
            }
        }

        private void BindDataToUI(KnittedItemsDeliveredToLinkingSection delInfo)
        {
            CommonMethods.LoadDropdownById(ddlStyles, delInfo.BuyerId, "BuyerStyles", 1, 0);
            ddlUserFinishingUnitFloor.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUserFinishingUnitFloor, delInfo.FinishingUnitFloorId);
            ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, delInfo.BuyerId);
            ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, delInfo.StyleId);
            ddlBuyers.Enabled = false;
            ddlStyles.Enabled = false;
            LoadEntryInfo(delInfo.StyleId, delInfo.BuyerId);
        }

        int DeliveryId
        {
            get
            {
                try
                {
                    return int.Parse(ViewState["deliveryId"].ToString());
                }
                catch (Exception)
                {
                    return 0;
                }
            }
            set { ViewState["deliveryId"] = value; }
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
            }
            divColors.Visible = false;
            rptEntryInfo.Visible = false;
            rptEntryInfo.DataSource = null;
            rptEntryInfo.DataBind();
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlStyles.SelectedValue != "")
            {
                LoadEntryInfo(int.Parse(ddlStyles.SelectedValue), int.Parse(ddlBuyers.SelectedValue));
            }
            else
            {
                divColors.Visible = false;
                rptEntryInfo.Visible = false;
                rptEntryInfo.DataSource = null;
                rptEntryInfo.DataBind();
            }
        }

        private void LoadEntryInfo(int styleId, int buyerId)
        {
            var dtRecColors = unitOfWork.GetDataTableFromSql($"Exec usp_GetKnittingReceivedDetailsByStyleId {styleId}");
            
            if (dtRecColors.Rows.Count > 0)
            {
                divColors.Visible = true;
                rptEntryInfo.DataSource = dtRecColors;
                rptEntryInfo.DataBind();
                rptEntryInfo.Visible = true;

                rptColorAvailableQty.DataSource = dtRecColors;
                rptColorAvailableQty.DataBind();
                rptColorAvailableQty.Visible = true;

                lblNoDataFound.Visible = false;

                if (DeliveryId != 0)
                {
                    btnSaveEntries.Visible = false;
                    btnUpdateEntries.Visible = true;
                }
                else
                {
                    btnSaveEntries.Visible = true;
                    btnUpdateEntries.Visible = false;
                }
            }
            else
            {
                divColors.Visible = false;
                rptEntryInfo.DataSource = null;
                rptEntryInfo.DataBind();
                rptEntryInfo.Visible = false;

                rptColorAvailableQty.DataSource = null;
                rptColorAvailableQty.DataBind();
                rptColorAvailableQty.Visible = false;

                lblNoDataFound.Visible = true;
                btnSaveEntries.Visible = false;
                btnUpdateEntries.Visible = false;
            }
            
        }

        protected void rptEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var buyerId = int.Parse(((Label)e.Item.FindControl("lblBuyerId")).Text);
                var dtSizes = buyerManager.GetSizes(buyerId);
                //GridView gvSizeQuantity1 = (GridView)e.Item.FindControl("gvSizeQuantity1");
                GridView gvSizeQuantity2 = (GridView)e.Item.FindControl("gvSizeQuantity2");
                Label lblNoSizeFound = (Label)e.Item.FindControl("lblNoSizeFound");
                if (dtSizes.Rows.Count > 0)
                {
                    DataTable dtOneRow = new DataTable();
                    DataRow dr = null;
                    dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                    dr = dtOneRow.NewRow();
                    dr["RowNumber"] = 1;
                    dtOneRow.Rows.Add(dr);

                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        //gvSizeQuantity1.Columns.Add(templateField);
                        gvSizeQuantity2.Columns.Add(templateField);
                    }

                    //gvSizeQuantity1.DataSource = dtOneRow;
                    //gvSizeQuantity1.DataBind();
                    //gvSizeQuantity1.Visible = true;
                    gvSizeQuantity2.DataSource = dtOneRow;
                    gvSizeQuantity2.DataBind();
                    gvSizeQuantity2.Visible = true;
                    lblNoSizeFound.Visible = false;

                }
                else
                {
                   // gvSizeQuantity1.Visible = false;
                    gvSizeQuantity2.Visible = false;
                    lblNoSizeFound.Visible = true;
                }
            }
        }

        protected void gvSizeQuantity1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var buyerId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId")).Text);
                var styleId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId")).Text);
                var dtSizes = buyerManager.GetSizes(buyerId);
                Label txtSizeQty = null;
                Label lblSizeId = null;

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new Label();
                    txtSizeQty.ID = "tbxReceivQuantity" + i.ToString();
                    txtSizeQty.Text = "";
                    txtSizeQty.Width = 50;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);


                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);


                    var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());
                    var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                    var buyerColorId = int.Parse(string.IsNullOrEmpty(lblBuyerColorId.Text) ? "0" : lblBuyerColorId.Text);
                    if (buyerColorId != 0)
                    {
                        var availableQty = int.Parse(unitOfWork.GetSingleValue($"Exec usp_GetAvailableKnittingsQtyByStyleColorSize {styleId},{buyerColorId},{sizeId}"));
                        if(DeliveryId != 0)
                        {
                            var sizeInfo = unitOfWork.GenericRepositories<KnittedItemsDeliveredToLinkingSectionColorSizes>().Get(x => x.KnittingDeliveryId == DeliveryId && x.BuyerColorId == buyerColorId && x.SizeId == sizeId).FirstOrDefault();
                            if (sizeInfo != null)
                            {
                                availableQty += sizeInfo.DeliveryQty ?? 0;
                            }
                        }
                        txtSizeQty.Text = availableQty+"";
                    }
                }
            }
        }

        protected void gvSizeQuantity2_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var buyerId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId")).Text);
                var styleId = int.Parse(((Label)((GridView)sender).DataItemContainer.FindControl("lblStyleId")).Text);
                var dtSizes = buyerManager.GetSizes(buyerId);
                TextBox txtSizeQty = null;
                Label lblSizeId = null;

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new TextBox();
                    txtSizeQty.ID = "tbxDeliveryQuantity" + i.ToString();
                    txtSizeQty.Text = "";
                    txtSizeQty.Width = 50;
                    txtSizeQty.TextMode = TextBoxMode.Number;

                    //txtSizeQty.TextChanged += new EventHandler(textbox_TextChanged);
                    //txtSizeQty.AutoPostBack = true;
                    txtSizeQty.EnableViewState = true;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);


                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);

                    //var entryId = new Label();
                    //entryId.ID = "lblentryId" + i.ToString();
                    //entryId.Text = "";
                    //entryId.Visible = false;
                    //e.Row.Cells[i].Controls.Add(entryId);
                    var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());
                    var lblBuyerColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerColorId");
                    var buyerColorId = int.Parse(string.IsNullOrEmpty(lblBuyerColorId.Text) ? "0" : lblBuyerColorId.Text);
                    var availableQty = int.Parse(unitOfWork.GetSingleValue($"Exec usp_GetAvailableKnittingsQtyByStyleColorSize {styleId},{buyerColorId},{sizeId}"));
                    if (DeliveryId != 0)
                    {                       
                        if (buyerColorId != 0)
                        {
                            var sizeInfo = unitOfWork.GenericRepositories<KnittedItemsDeliveredToLinkingSectionColorSizes>().Get(x => x.KnittingDeliveryId == DeliveryId && x.BuyerColorId == buyerColorId && x.SizeId == sizeId).FirstOrDefault();
                            if (sizeInfo != null)
                            {
                                availableQty += sizeInfo.DeliveryQty ?? 0;
                                txtSizeQty.Text = sizeInfo.DeliveryQty + "";                                
                            }
                            if (availableQty == 0)
                            {
                                txtSizeQty.Enabled = false;
                            }
                            else
                            {
                                txtSizeQty.Attributes["max"] = availableQty + "";
                            }
                        }
                    }
                    else
                    {                        
                        if (availableQty == 0)
                        {
                            txtSizeQty.Enabled = false;
                        }
                        else
                        {
                            txtSizeQty.Attributes["max"] = availableQty + "";
                        }
                    }
                }
            }
        }

        protected void btnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {
                var deliveryDate = DateTime.Parse(tbxDeliveryDate.Text);
                if (deliveryDate <= DateTime.Now)
                {
                    List<KnittedItemsDeliveredToLinkingSectionColorSizes> lstColorSizes = new List<KnittedItemsDeliveredToLinkingSectionColorSizes>();
                    var finishingUnitFloorId = int.Parse(ddlUserFinishingUnitFloor.SelectedValue);
                    var finishingUnitId = unitOfWork.GenericRepositories<FinishingUnitFloors>().Get(x => x.Id == finishingUnitFloorId).Select(x => x.FinishingUnitId).FirstOrDefault();
                    var dkdc = new KnittedItemsDeliveredToLinkingSection()
                    {
                        DeliveryDate = deliveryDate,
                        FinishingUnitId = finishingUnitId,
                        FinishingUnitFloorId = finishingUnitFloorId,
                        BuyerId = int.Parse(ddlBuyers.SelectedValue),
                        StyleId = int.Parse(ddlStyles.SelectedValue),
                        DeliveryQty = 0,
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };

                    int totalDelQty = 0;
                    for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                    {
                        var buyerColorId = int.Parse(((Label)rptEntryInfo.Items[i].FindControl("lblBuyerColorId")).Text);
                        var gvSizeQty = (GridView)rptEntryInfo.Items[i].FindControl("gvSizeQuantity2");
                        for (int j = 0; j < gvSizeQty.Columns.Count; j++)
                        {
                            var sizeId = int.Parse(((Label)gvSizeQty.Rows[0].Cells[j].FindControl("lblSizeId" + j)).Text);
                            var delQty = ((TextBox)gvSizeQty.Rows[0].Cells[j].FindControl("tbxDeliveryQuantity" + j)).Text;
                            if (!string.IsNullOrEmpty(delQty))
                            {
                                var colorSize = new KnittedItemsDeliveredToLinkingSectionColorSizes()
                                {
                                    BuyerColorId = buyerColorId,
                                    KnittingDeliveryId = dkdc.Id,
                                    DeliveryQty = int.Parse(delQty),
                                    SizeId = sizeId,
                                    CreateDate = DateTime.Now,
                                    CreatedBy = CommonMethods.SessionInfo.UserName
                                };
                                lstColorSizes.Add(colorSize);
                                totalDelQty += int.Parse(delQty);
                            }
                        }
                    }

                    if (lstColorSizes.Count > 0)
                    {
                        dkdc.DeliveryQty = totalDelQty;
                        unitOfWork.GenericRepositories<KnittedItemsDeliveredToLinkingSection>().Insert(dkdc);
                        foreach (var item in lstColorSizes)
                        {
                            unitOfWork.GenericRepositories<KnittedItemsDeliveredToLinkingSectionColorSizes>().Insert(item);
                        }
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please enter delivery size quantity.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('You cannot deliver in future date')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void btnUpdateEntries_Click(object sender, EventArgs e)
        {
            try
            {
                var deliveryDate = DateTime.Parse(tbxDeliveryDate.Text);
                if (deliveryDate <= DateTime.Now)
                {
                    List<KnittedItemsDeliveredToLinkingSectionColorSizes> lstColorSizes = new List<KnittedItemsDeliveredToLinkingSectionColorSizes>();
                    var finishingUnitFloorId = int.Parse(ddlUserFinishingUnitFloor.SelectedValue);
                    var finishingUnitId = unitOfWork.GenericRepositories<FinishingUnitFloors>().Get(x => x.Id == finishingUnitFloorId).Select(x => x.FinishingUnitId).FirstOrDefault();
                    var dkdc = unitOfWork.GenericRepositories<KnittedItemsDeliveredToLinkingSection>().GetByID(DeliveryId);

                    dkdc.DeliveryDate = deliveryDate;
                    dkdc.FinishingUnitId = finishingUnitId;
                    dkdc.FinishingUnitFloorId = finishingUnitFloorId;
                    dkdc.UpdateDate = DateTime.Now;
                    dkdc.UpdatedBy = CommonMethods.SessionInfo.UserName;

                    int totalDelQty = 0;
                    for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                    {
                        var buyerColorId = int.Parse(((Label)rptEntryInfo.Items[i].FindControl("lblBuyerColorId")).Text);
                        var gvSizeQty = (GridView)rptEntryInfo.Items[i].FindControl("gvSizeQuantity2");
                        for (int j = 0; j < gvSizeQty.Columns.Count; j++)
                        {
                            var sizeId = int.Parse(((Label)gvSizeQty.Rows[0].Cells[j].FindControl("lblSizeId" + j)).Text);
                            var delQty = ((TextBox)gvSizeQty.Rows[0].Cells[j].FindControl("tbxDeliveryQuantity" + j)).Text;
                            if (!string.IsNullOrEmpty(delQty))
                            {
                                var colorSize = new KnittedItemsDeliveredToLinkingSectionColorSizes()
                                {
                                    BuyerColorId = buyerColorId,
                                    KnittingDeliveryId = dkdc.Id,
                                    DeliveryQty = int.Parse(delQty),
                                    SizeId = sizeId,
                                    CreateDate = DateTime.Now,
                                    CreatedBy = CommonMethods.SessionInfo.UserName
                                };
                                lstColorSizes.Add(colorSize);
                                totalDelQty += int.Parse(delQty);
                            }
                        }
                    }

                    var lstOldColors = unitOfWork.GenericRepositories<KnittedItemsDeliveredToLinkingSectionColorSizes>().Get(x => x.KnittingDeliveryId == DeliveryId).ToList();
                    foreach (var item in lstOldColors)
                    {
                        unitOfWork.GenericRepositories<KnittedItemsDeliveredToLinkingSectionColorSizes>().Delete(item);
                    }
                    if (lstColorSizes.Count > 0)
                    {
                        dkdc.DeliveryQty = totalDelQty;
                        unitOfWork.GenericRepositories<KnittedItemsDeliveredToLinkingSection>().Update(dkdc);
                        foreach (var item in lstColorSizes)
                        {
                            unitOfWork.GenericRepositories<KnittedItemsDeliveredToLinkingSectionColorSizes>().Insert(item);
                        }
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
                        //Response.AddHeader("REFRESH", "2;URL=ViewDeliveredKnittedItems.aspx");
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewDeliveredKnittedItems.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please enter delivery size quantity.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('You cant deliver in future date');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        void textbox_TextChanged(object sender, EventArgs e)
        {
            var textBox = (TextBox)sender;
            var maxVlaue = int.Parse(textBox.Attributes["max"]);
            var inputValue = int.Parse(string.IsNullOrEmpty(textBox.Text) ? "0" : textBox.Text);
            if (inputValue > maxVlaue)
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Available quantity is " + maxVlaue + ", <br />You cant deliver more than available quantity');",true);
                ScriptManager.RegisterStartupScript(this,GetType(), "SetFocus", "document.getElementById('" + textBox.ClientID + "').focus();",true);
                textBox.Text = "";                
            }
            ScriptManager.RegisterStartupScript(this,GetType(), "message", "scrollDown(300);", true);
        }

        protected void rptColorAvailableQty_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var buyerId = int.Parse(((Label)e.Item.FindControl("lblBuyerId")).Text);
                var dtSizes = buyerManager.GetSizes(buyerId);
                GridView gvSizeQuantity1 = (GridView)e.Item.FindControl("gvSizeQuantity1");
                if (dtSizes.Rows.Count > 0)
                {
                    DataTable dtOneRow = new DataTable();
                    DataRow dr = null;
                    dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                    dr = dtOneRow.NewRow();
                    dr["RowNumber"] = 1;
                    dtOneRow.Rows.Add(dr);

                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeQuantity1.Columns.Add(templateField);
                    }

                    gvSizeQuantity1.DataSource = dtOneRow;
                    gvSizeQuantity1.DataBind();
                    gvSizeQuantity1.Visible = true;

                }
                else
                {
                    gvSizeQuantity1.Visible = false;
                 
                }
            }
        }
    }
}