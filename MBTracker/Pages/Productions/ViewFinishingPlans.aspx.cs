﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using Repositories;
using MBTracker.EF;

namespace MBTracker.Pages.Productions
{
    public partial class ViewFinishingPlans : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        ProductionManager productionManager = new ProductionManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableDeleteButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);

                if (Request.QueryString["styleId"] != null)
                {
                    var styleId = int.Parse(Tools.UrlDecode(Request.QueryString["styleId"]));
                    var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(styleId);
                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
                    BindStylesByBuyer(styleInfo.BuyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleId);
                    LoadShipmentInfo(styleId);
                    BindTotalOrderQuantity(styleId);
                }
            }
        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Finishing");
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewFinishingPlans", 2, 1);
        }

        int KnittingTime
        {
            set { ViewState["knittingTime"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["knittingTime"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlStyles.Items.Clear();
            //ddlOrders.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            //else
            //{
            //    pnlPlanningInfo.Visible = false;
            //    pnlShipmentInfo.Visible = false;
            //    pnlAssignMachinesToOrder.Visible = false;
            //}
            pnlPlanningInfo.Visible = false;
            pnlShipmentInfo.Visible = false;
            pnlAssignMachinesToOrder.Visible = false;
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        //protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    //ddlOrders.Items.Clear();
        //    if (ddlStyles.SelectedValue != "")
        //    {
        //        LoadPlanInfo();
        //        //BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
        //    }
        //    else
        //    {
        //        pnlPlanningInfo.Visible = false;
        //        pnlShipmentInfo.Visible = false;
        //        pnlAssignMachinesToOrder.Visible = false;
        //    }
        //}

        //private void BindOrdersByStyle(int styleId)
        //{

        //    CommonMethods.LoadOrderDropdownByStyle(ddlOrders, styleId, 1, 0);

        //}

        //protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        //{


        //    if (ddlOrders.SelectedValue != "")
        //    {
        //        LoadPlanInfo();

        //    }
        //    else
        //    {
        //        pnlPlanningInfo.Visible = false;
        //        pnlShipmentInfo.Visible = false;
        //        pnlAssignMachinesToOrder.Visible = false;
        //    }
        //}


        //private void LoadPlanInfo()
        //{
        //    DataTable dt = new DataTable();           


        //    dt = productionManager.GetProductionPlanningInfo(Convert.ToInt32(ddlStyles.SelectedValue));
        //    if (dt.Rows.Count < 1)
        //    {
        //        dt = orderManager.GetProductionPlanningInfo(Convert.ToInt32(ddlStyles.SelectedValue));
        //    }

        //    if (dt.Rows.Count > 0)
        //    {
        //        pnlPlanningInfo.Visible = true;
        //        KnittingTime = Convert.ToInt32(dt.Rows[0][8].ToString());

        //        rptPlanInfo.DataSource = dt;
        //        rptPlanInfo.DataBind();
        //        lblNoPlanningInfo.Visible = false;
        //        rptPlanInfo.Visible = true;
        //        lblNoMCAssignedMessage.Visible = false;
        //        LoadShipmentInfo(Convert.ToInt32(ddlStyles.SelectedValue));
        //    }
        //    else
        //    {
        //        rptPlanInfo.DataSource = null;
        //        rptPlanInfo.DataBind();
        //        rptPlanInfo.Visible = false;
        //        lblNoPlanningInfo.Visible = true;

        //        pnlShipmentInfo.Visible = false;
        //        pnlAssignMachinesToOrder.Visible = false;
        //        pnlPlanningInfo.Visible = false;
        //        lblNoMCAssignedMessage.Visible = true;
        //    }

        //    BindTotalOrderQuantity(Convert.ToInt32(ddlStyles.SelectedValue));
        //}

        private void BindTotalOrderQuantity(int styleId)
        {
            divTotalOrderQuantity.Visible = true;
            var totalOrderQty = unitOfWork.GetSingleValue($"Exec usp_GetActiveOrderQuantityByStyleId {styleId}");
            divTotalOrderQuantity.InnerHtml = $"<div style='width:100%; padding-bottom:9px; margin-bottom:5px; font-size:13px; padding:10px; background-color:#ffff00aa; font-weight:bold'>Order Quantity: <span style='font-size:15px;'>{totalOrderQty}</span>    &nbsp;&nbsp;<asp:LinkButton ID='lnkbtnView' onClick='window.open(\"../Reports/SummaryOfOrders.aspx?styleId={Tools.UrlEncode(styleId + "")}\",\"_blank\")' class='btn btn-success btn-mini hidden-phone pull-right' Text='View Details'>View Details</asp:LinkButton></div>";
        }

        private void LoadShipmentInfo(int styleId)
        {
           

            DataTable dt = new DataTable();
            dt = orderManager.GetShipmentInfoByStyleId(styleId);

            if (dt.Rows.Count > 0)
            {
                rptShipmentSummary.DataSource = dt;
                rptShipmentSummary.DataBind();

                rptShipmentSummary.Visible = true;
                lblNoShippingInfo.Visible = false;
                pnlShipmentInfo.Visible = true;
                pnlPlanningInfo.Visible = true;
                
            }
            else
            {
                rptShipmentSummary.DataSource = null;
                rptShipmentSummary.DataBind();

                rptShipmentSummary.Visible = false;
                lblNoShippingInfo.Visible = true;
                pnlPlanningInfo.Visible = false;
            }

            LoadExistingMCAssignedToOrder();
        }


        private void LoadExistingMCAssignedToOrder()
        {

            DataTable dt = new DataTable();

            var styleId = int.Parse(ddlStyles.SelectedValue);
            dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetLinkingMachinesAssingedToOrder_ByStyleId {styleId}");

            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                rpt.Visible = true;
                pnlAssignMachinesToOrder.Visible = true;
                lblNoMCAssignedMessage.Visible = false;
                lblNoMCAssignedMessage.Visible = false;
            }
            else
            {
                rpt.DataSource = null;
                rpt.DataBind();
                rpt.Visible = false;
                lblNoMCAssignedMessage.Visible = true;
                pnlAssignMachinesToOrder.Visible = false;
                pnlPlanningInfo.Visible = false;
                lblNoMCAssignedMessage.Visible = true;

            }

        }

        protected void rptShipmentSummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label machinesRequired = (Label)e.Item.FindControl("lblMCRequired");
                double numMachinesRequired = Math.Ceiling((Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "ShipmentQuantity").ToString()) * KnittingTime) / 1320);
                machinesRequired.Text = numMachinesRequired.ToString();
            }
        }

        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
           if (e.CommandName == "Delete")
            {
                int machineAssignedId = Convert.ToInt32(e.CommandArgument.ToString());
                DeleteAssignedMachine(machineAssignedId);
            }
            else if (e.CommandName == "Edit")
            {
                int machineAssignedId = Convert.ToInt32(e.CommandArgument.ToString());
                Response.Redirect("AddFinishingPlan.aspx?machineAssignedId=" + Tools.UrlEncode(machineAssignedId + ""));
            }
        }

        private void DeleteAssignedMachine(int machineAssignedId)
        {
            try
            {
                unitOfWork.GenericRepositories<LinkingMachinesAssignedToOrder>().Delete(machineAssignedId);
                unitOfWork.Save();
                LoadExistingMCAssignedToOrder();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Plan deletion failed.')", true);
            }            
        }

        protected void btnViewPlan_Click(object sender, EventArgs e)
        {
            if (ddlBuyers.SelectedValue != "")
            {
                if (ddlStyles.SelectedValue != "")
                {
                    LoadShipmentInfo(Convert.ToInt32(ddlStyles.SelectedValue));
                    BindTotalOrderQuantity(Convert.ToInt32(ddlStyles.SelectedValue));
                    //LoadPlanInfo();
                    //BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
                }
                else
                {
                    pnlPlanningInfo.Visible = false;
                    pnlShipmentInfo.Visible = false;
                    pnlAssignMachinesToOrder.Visible = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a style.');", true);
                }

                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a style.');", true);
                //}
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a buyer');", true);
            }
        }
    }
}