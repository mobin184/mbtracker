﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="DeliverLinkedItemsForFinishing.aspx.cs" Inherits="MBTracker.Pages.Productions.DeliverLinkedItemsForFinishing" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
     <style>
        .table th {
            text-align: center;
        }
    </style>

     <div class="row-fluid">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="Deliver Linked Items For Finishing:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row" style="padding-left: 15px; padding-right: 15px;">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                            <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label" style="float: left;width:140px; padding-top:5px;text-align:right">
                                        <asp:Label ID="Label2" runat="server" Text="Delivery Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row" style="margin-left:150px">
                                        <asp:TextBox ID="tbxDeliveryDate" AutoPostBack="true" TextMode="Date" Width="100%" Text="" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxDeliveryDate"><span style="font-weight: 700; color: #CC0000">Please select receive date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left;width:140px; padding-top:5px;text-align:right">
                                        <asp:Label ID="Label1" runat="server" Text="Finishing Unit Floor:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row" style="margin-left:150px">
                                        <asp:DropDownList ID="ddlUserFinishingUnitFloor" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUserFinishingUnitFloor_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlUserFinishingUnitFloor"><span style="font-weight: 700; color: #CC0000">Please select a finishing unit.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left;width:140px; padding-top:5px;text-align:right">
                                        <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row" style="margin-left:150px">
                                        <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label" style="float: left;width:140px; padding-top:5px;text-align:right">
                                        <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row" style="margin-left:150px">
                                        <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select a style.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <asp:Label ID="lblNoDataFound" runat="server" Text="There is no color for this style."  Visible="false" BackColor="#ffff00"></asp:Label>
    </div>
     <div class="row-fluid" runat="server" id="divColors" visible="false">
        <div class="span9">
            <div class="widget">
                <div class="widget-body">
                    <div class="row-fluid">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="Label4" runat="server" Text="Available Linked Item In Linking Section:" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                    <asp:Repeater ID="rptAvailableQuantity" OnItemDataBound="rptAvailableQuantity_ItemDataBound" runat="server">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblColor" runat="server" Text="Color Names"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Available Quantity By Sizes"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%#Eval("ColorDescription") %>
                                                    <asp:Label ID="lblBuyerColorId" runat="server" Text='<%#Eval("BuyerColorId") %>' Visible="false"></asp:Label></td>
                                                <asp:Label ID="lblStyleId" runat="server" Text='<%#Eval("StyleId") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblFloorId" runat="server" Text='<%#Eval("FloorId") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblBuyerId" runat="server" Text='<%#Eval("BuyerId") %>' Visible="false"></asp:Label>
                                                <td>
                                                    <asp:GridView ID="gvSizeQuantity1" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity1_RowCreated">
                                                    </asp:GridView>
                                                    <asp:Label ID="lblNoSizeFound" runat="server" Text="No size found." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                                                </td>                                                
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                </table>
                            <div class="clearfix"></div>
                                        </FooterTemplate>
                                    </asp:Repeater>                                    
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <div class="widget">
                <div class="widget-body">
                    <div class="row-fluid">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblEntry" runat="server" Text="Deliver Linked Items For Finishing:" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                    <asp:Repeater ID="rptEntryInfo" OnItemDataBound="rptEntryInfo_ItemDataBound" runat="server">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <%--  <th>
                                                            <asp:Label ID="lblBuyer" runat="server" Text="Buyer<br/> Names"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblStyle" runat="server" Text="Styles<br/> Names"></asp:Label></th>--%>
                                                        <th>
                                                            <asp:Label ID="lblColor" runat="server" Text="Color Names"></asp:Label></th>
                                                       <%-- <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Available Quantity By Sizes"></asp:Label></th>--%>
                                                        <th>
                                                            <asp:Label ID="Label3" runat="server" Text="Quantity To Deliver By Sizes"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <%--  <td><%#Eval("BuyerName") %></td>
                                                <td><%#Eval("StyleName") %></td>--%>
                                                <td><%#Eval("ColorDescription") %>
                                                    <asp:Label ID="lblBuyerColorId" runat="server" Text='<%#Eval("BuyerColorId") %>' Visible="false"></asp:Label></td>
                                                <asp:Label ID="lblStyleId" runat="server" Text='<%#Eval("StyleId") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblFloorId" runat="server" Text='<%#Eval("FloorId") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblBuyerId" runat="server" Text='<%#Eval("BuyerId") %>' Visible="false"></asp:Label>
                                                <%--<td>
                                                    <asp:GridView ID="gvSizeQuantity1" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity1_RowCreated">
                                                    </asp:GridView>
                                                    <asp:Label ID="lblNoSizeFound" runat="server" Text="No size found." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                                                </td>--%>
                                                <td>
                                                    <asp:GridView ID="gvSizeQuantity2" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity2_RowCreated">
                                                    </asp:GridView>
                                                    <asp:Label ID="lblNoSizeFound" runat="server" Text="No size found." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                </table>
                            <div class="clearfix"></div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <asp:Button ID="btnSaveEntries" runat="server" ValidationGroup="save" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSaveEntries_Click"></asp:Button>
            <asp:Button ID="btnUpdateEntries" Visible="false" ValidationGroup="save" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdateEntries_Click"></asp:Button>
        </div>
    </div>


</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
