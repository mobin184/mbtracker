﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Productions
{
    public partial class ViewMonthlyKnittingPlans : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        ProductionManager productionManager = new ProductionManager();
        DataTable dtUserUnits;
        DataTable dtSizes;
        DataTable dtYarnIssuedForSizes;
        int buyerId;
        int styleId;
        int unitId;
        int buyerColorId;
        int entryRowNumber2 = 0;
        int nightTotal = 0;
        int dayTotal = 0;
        int grandTotal = 0;

        int dailyUnitTotal = 0;



        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                var user = CommonMethods.SessionInfo;
                if (user == null)
                {
                    HttpContext.Current.Session["returnUrl"] = HttpContext.Current.Request.Url.PathAndQuery;
                    Response.Redirect("~/Login.aspx");
                }

                CommonMethods.LoadDropdown(ddlYear, "Years WHERE IsActive = 1", 1, 1);
                CommonMethods.LoadDropdown(ddlMonth, "Months", 1, 0);
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                //EnableDisableEditButton();
                //EnableDisableDeleteButton();
                //tbxFilterDate.Text = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd");
                //LoadData();
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteKnittingPlanEntries();
                }
            }
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlDetails.Visible = false;
            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
        }

        private void DeleteKnittingPlanEntries()
        {
            try
            {
                var knittingPlan = unitOfWork.GenericRepositories<MonthlyKnittingPlan>().GetByID(KnittingPlanId);
                unitOfWork.GenericRepositories<MonthlyKnittingPlan>().Delete(knittingPlan);
                unitOfWork.Save();
                //pnlDetails.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
                LoadDetailData();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Year = int.Parse(ddlYear.SelectedValue);
            Month = int.Parse(ddlMonth.SelectedValue);
            LoadDetailData();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(400)", true);
        }


        //protected void LoadData()
        //{
        //    if (!string.IsNullOrEmpty(ddlYear.SelectedValue))
        //    {
        //        var roleId = CommonMethods.SessionInfo.RoleId;
        //        string sql = $"Exec usp_GetMonthlyKnittingPlans '{ddlYear.SelectedValue}'";
        //        var dt = unitOfWork.GetDataTableFromSql(sql);

        //        if (dt.Rows.Count > 0)
        //        {
        //            dt = dt.CheckEditDeletePermission(roleId, "");

        //            rptUnits.Visible = true;
        //            dataDiv.Visible = true;

        //            rptUnits.DataSource = dt;
        //            rptUnits.DataBind();
        //            lblNoDataFound.Visible = false;
        //            //lblLabelMessage.Text = "Knitting entries on: " + String.Format("{0:dd-MMM-yyyy}", tbxFilterDate.Text); // String.Format("{0:MM/dd/yyyy}", date);
        //            lblLabelMessage.Visible = true;
        //            rptDetails.Visible = false;
        //        }
        //        else
        //        {
        //            rptUnits.Visible = false;
        //            dataDiv.Visible = false;

        //            lblNoDataFound.Visible = true;
        //            lblLabelMessage.Visible = false;
        //        }
        //    }
        //    else
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a year!')", true);
        //    }
        //}


        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            KnittingPlanId = int.Parse(commandArgs[0]);
            //Year = int.Parse(commandArgs[1]);
            Response.Redirect("EnterMonthlyKnittingPlan.aspx?planId=" + Tools.UrlEncode(KnittingPlanId + ""));
        }

        //protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        //{
        //    pnlDetails.Visible = true;
        //    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        //    Month = int.Parse(commandArgs[0]);
        //    Year = int.Parse(commandArgs[1]);

        //    var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetMonthlyKnittingPlanDetails '{Year}','{Month}'");
        //    if (dt.Rows.Count > 0)
        //    {
        //        rptDetails.DataSource = dt;
        //        rptDetails.DataBind();
        //        lblNoDetails.Visible = false;
        //        pnlDetails.Visible = true;
        //        rptDetails.Visible = true;
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(900)", true);
        //    }
        //    else
        //    {
        //        lblNoDetails.Visible = true;
        //        pnlDetails.Visible = false;
        //    }
        //}


        protected void LoadDetailData()
        {
            //pnlDetails.Visible = true;
            //string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            //Month = int.Parse(commandArgs[0]);
            //Year = int.Parse(commandArgs[1]);

            var styleId = 0;
            var buyerId = 0;

            if (!string.IsNullOrEmpty(ddlBuyers.SelectedValue))
            {
                buyerId = int.Parse(ddlBuyers.SelectedValue);
            }
            if (!string.IsNullOrEmpty(ddlStyles.SelectedValue))
            {
                styleId = int.Parse(ddlStyles.SelectedValue);
            }

            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetMonthlyKnittingPlanDetails '{Year}','{Month}','{buyerId}','{styleId}'");
            if (dt.Rows.Count > 0)
            {
                var roleId = CommonMethods.SessionInfo.RoleId;
                dt = dt.CheckEditDeletePermission(roleId, "");
                rptDetails.DataSource = dt;
                rptDetails.DataBind();
                lblNoDetails.Visible = false;
                pnlDetails.Visible = true;
                rptDetails.Visible = true;
                // dataDiv.Visible = true;

                lblGrandTotal.Text = "Total Target: " + grandTotal.ToString();

            }
            else
            {
                lblNoDetails.Visible = true;
                pnlDetails.Visible = false;
                //dataDiv.Visible = true;
            }
        }

        int Month
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["Month"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["Month"] = value; }
        }
        int Year
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["Year"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["Year"] = value; }
        }

        int KnittingPlanId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["KnittingPlanId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["KnittingPlanId"] = value; }
        }


        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            KnittingPlanId = int.Parse(commandArgs[0]);
            //Year = int.Parse(commandArgs[1]);
            if (KnittingPlanId != 0)
            {
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
            }
            else
            {
                var title = "Error";
                var msg = "Sorry, you can not delete it.<br />Something went wrong.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            }
        }


        protected void rptDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var issuedQty = int.Parse(DataBinder.Eval(e.Item.DataItem, "Target").ToString());
                grandTotal = grandTotal + issuedQty;
            }
        }
    }
}