﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Knitting_Parts;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Knitting_Parts
{
    public partial class IssueKnittingSpares : System.Web.UI.Page
    {

        PartsManager partManager = new PartsManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
            }
        }


        protected void ddlKnittingUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlKnittingUnits.SelectedValue != "")
            {
                LoadMachineBrandsByUnit(ddlMachineBrands, int.Parse(ddlKnittingUnits.SelectedValue));
            }
            else
            {
                ddlMachineBrands.Items.Clear();
            }
        }

        private void LoadMachineBrandsByUnit(DropDownList ddl, int unitId)
        {
            string sql = $"Exec usp_GetAllMachineBrandByUnitId {unitId}";
            var dt = unitOfWork.GetDataTableFromSql(sql);
            DataRow dr = dt.NewRow();
            dr["MachineBrandId"] = "";
            dr["MachineBrand"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);

            ddl.DataTextField = "MachineBrand";
            ddl.DataValueField = "MachineBrandId";
            ddl.DataSource = dt;
            ddl.DataBind();
        }


        protected void btnViewVerifiedRequests_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();


            int knittingUnitId = 0;
            int machineBrandId = 0;
            DateTime fromDate = DateTime.Now.AddDays(-30);

            if (ddlKnittingUnits.SelectedValue != "")
            {

                if (ddlMachineBrands.SelectedValue != "")
                {
                    machineBrandId = int.Parse(ddlMachineBrands.SelectedValue);
                }

                if (tbxFromDate.Text != "")
                {
                    fromDate = DateTime.Parse(tbxFromDate.Text);
                }

                knittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue);

                dt = partManager.GetVerifiedSpareRequests(knittingUnitId, machineBrandId, fromDate);

                if (dt.Rows.Count > 0)
                {
                    rptKnittingSpareRequests.DataSource = dt;
                    rptKnittingSpareRequests.DataBind();
                    lblNoRequestFound.Visible = false;
                    pnlViewKnittingSpareRequests.Visible = true;
                }
                else
                {
                    lblNoRequestFound.Visible = true;
                    pnlViewKnittingSpareRequests.Visible = false;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Select a knitting unit.')", true);
            }
        }


        protected void rptKnittingSpareRequests_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {

                DropDownList ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                Label lblMachineBrandId = (Label)e.Item.FindControl("lblMachineBrandId");
                LoadMachineBrandsByUnit(ddlMachineBrands, int.Parse(ddlKnittingUnits.SelectedValue));
                ddlMachineBrands.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrands, string.IsNullOrEmpty(lblMachineBrandId.Text.ToString()) ? 0 : int.Parse(lblMachineBrandId.Text.ToString()));


                DropDownList ddlKnittingMachines = (DropDownList)e.Item.FindControl("ddlKnittingMachines");
                Label lblMachineId = (Label)e.Item.FindControl("lblMachineId");
                // LoadMachinesByBrandAndUnit(ddlKnittingMachines, int.Parse(ddlMachineBrands.SelectedValue), int.Parse(ddlKnittingUnits.SelectedValue));
                ddlKnittingMachines.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnittingMachines, string.IsNullOrEmpty(lblMachineId.Text.ToString()) ? 0 : int.Parse(lblMachineId.Text.ToString()));

                DropDownList ddlKnittingPartTypes = (DropDownList)e.Item.FindControl("ddlKnittingPartTypes");
                Label lblSpareTypeId = (Label)e.Item.FindControl("lblSpareTypeId");
                // LoadSpareTypesDropdown(ddlKnittingPartTypes);
                ddlKnittingPartTypes.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnittingPartTypes, string.IsNullOrEmpty(lblSpareTypeId.Text.ToString()) ? 0 : int.Parse(lblSpareTypeId.Text.ToString()));



                DropDownList ddlSpares = (DropDownList)e.Item.FindControl("ddlSpares");
                Label lblSpareId = (Label)e.Item.FindControl("lblSpareId");
                // LoadSpareNames(ddlSpares, int.Parse(ddlKnittingPartTypes.SelectedValue), int.Parse(ddlMachineBrands.SelectedValue));
                ddlSpares.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSpares, string.IsNullOrEmpty(lblSpareId.Text.ToString()) ? 0 : int.Parse(lblSpareId.Text.ToString()));

                TextBox tbxQuantity = (TextBox)e.Item.FindControl("tbxQuantity");


                DropDownList ddlRequestReasons = (DropDownList)e.Item.FindControl("ddlRequestReasons");
                Label lblRequestReasonId = (Label)e.Item.FindControl("lblRequestReasonId");
                // LoadSpareChangeReasons(ddlRequestReasons);
                ddlRequestReasons.SelectedIndex = CommonMethods.MatchDropDownItem(ddlRequestReasons, string.IsNullOrEmpty(lblRequestReasonId.Text.ToString()) ? 0 : int.Parse(lblRequestReasonId.Text.ToString()));


                TextBox tbxRemarks = (TextBox)e.Item.FindControl("tbxRemarks");


                ddlMachineBrands.Visible = true;
                ddlKnittingMachines.Visible = true;
                ddlKnittingPartTypes.Visible = true;
                tbxQuantity.Visible = true;
                ddlSpares.Visible = true;
                ddlRequestReasons.Visible = true;
                tbxRemarks.Visible = true;




                Label lblMachineBrandValue = (Label)e.Item.FindControl("lblMachineBrandValue");
                Label lblMachineNameValue = (Label)e.Item.FindControl("lblMachineNameValue");
                Label lblSpareTypeValue = (Label)e.Item.FindControl("lblSpareTypeValue");
                Label lblSpareNameValue = (Label)e.Item.FindControl("lblSpareNameValue");
                Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
                Label lblRequestReason = (Label)e.Item.FindControl("lblRequestReason");
                Label lblRemarksValue = (Label)e.Item.FindControl("lblRemarksValue");



                lblMachineBrandValue.Visible = false;
                lblMachineNameValue.Visible = false;
                lblSpareTypeValue.Visible = false;
                lblSpareNameValue.Visible = false;
                lblQuantity.Visible = false;
                lblRequestReason.Visible = false;
                lblRemarksValue.Visible = false;


                e.Item.FindControl("lnkbtnEdit").Visible = false;
                e.Item.FindControl("lnkbtnUpdate").Visible = true;
                e.Item.FindControl("lnkbtnCancel").Visible = true;

            }
            else if (e.CommandName == "Update")
            {
                try
                {
                    DropDownList ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                    DropDownList ddlKnittingMachines = (DropDownList)e.Item.FindControl("ddlKnittingMachines");
                    DropDownList ddlKnittingPartTypes = (DropDownList)e.Item.FindControl("ddlKnittingPartTypes");
                    DropDownList ddlSpares = (DropDownList)e.Item.FindControl("ddlSpares");
                    TextBox tbxQuantity = (TextBox)e.Item.FindControl("tbxQuantity");
                    DropDownList ddlRequestReasons = (DropDownList)e.Item.FindControl("ddlRequestReasons");
                    TextBox tbxRemarks = (TextBox)e.Item.FindControl("tbxRemarks");


                    if (ddlMachineBrands.SelectedIndex == 0 || ddlKnittingMachines.SelectedIndex == 0 || ddlKnittingPartTypes.SelectedIndex == 0 || ddlSpares.SelectedIndex == 0 || tbxQuantity.Text.ToString() == "" || ddlRequestReasons.SelectedIndex == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Selection or data is invalid.')", true);
                    }
                    else
                    {
                        int spareRequestDetailId = Convert.ToInt32(e.CommandArgument.ToString());

                        var spareRequestDetail = unitOfWork.GenericRepositories<KnittingSpareRequestDetails>().GetByID(spareRequestDetailId);

                        spareRequestDetail.Id = spareRequestDetailId;
                        spareRequestDetail.KnittingMachineBrandId = int.Parse(ddlMachineBrands.SelectedValue);
                        spareRequestDetail.KnittingMachineId = int.Parse(ddlKnittingMachines.SelectedValue);
                        spareRequestDetail.KnittingSpareTypeId = int.Parse(ddlKnittingPartTypes.SelectedValue);
                        spareRequestDetail.KnittingSpareId = int.Parse(ddlSpares.SelectedValue);
                        spareRequestDetail.QuantityRequested = int.Parse(tbxQuantity.Text);
                        spareRequestDetail.ReasonForRequestId = int.Parse(ddlRequestReasons.SelectedValue);
                        spareRequestDetail.Remarks = tbxRemarks.Text.ToString();

                        spareRequestDetail.UpdatedBy = CommonMethods.SessionInfo.UserName;
                        spareRequestDetail.UpdateDate = DateTime.Now;



                        unitOfWork.GenericRepositories<KnittingSpareRequestDetails>().Update(spareRequestDetail);
                        unitOfWork.Save();

                        ddlMachineBrands.Visible = false;
                        ddlKnittingMachines.Visible = false;
                        ddlKnittingPartTypes.Visible = false;
                        tbxQuantity.Visible = false;
                        ddlSpares.Visible = false;
                        ddlRequestReasons.Visible = false;
                        tbxRemarks.Visible = false;



                        Label lblMachineBrandValue = (Label)e.Item.FindControl("lblMachineBrandValue");
                        Label lblMachineNameValue = (Label)e.Item.FindControl("lblMachineNameValue");
                        Label lblSpareTypeValue = (Label)e.Item.FindControl("lblSpareTypeValue");
                        Label lblSpareNameValue = (Label)e.Item.FindControl("lblSpareNameValue");
                        Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
                        Label lblRequestReason = (Label)e.Item.FindControl("lblRequestReason");
                        Label lblRemarksValue = (Label)e.Item.FindControl("lblRemarksValue");


                        lblMachineBrandValue.Visible = true;
                        lblMachineNameValue.Visible = true;
                        lblSpareTypeValue.Visible = true;
                        lblSpareNameValue.Visible = true;
                        lblQuantity.Visible = true;
                        lblRequestReason.Visible = true;
                        lblRemarksValue.Visible = true;


                        lblMachineBrandValue.Text = ddlMachineBrands.SelectedItem.Text;
                        lblMachineNameValue.Text = ddlKnittingMachines.SelectedItem.Text;
                        lblSpareTypeValue.Text = ddlKnittingPartTypes.SelectedItem.Text;
                        lblSpareNameValue.Text = ddlSpares.SelectedItem.Text;
                        lblQuantity.Text = tbxQuantity.Text;
                        lblRequestReason.Text = ddlRequestReasons.SelectedItem.Text;
                        lblRemarksValue.Text = tbxRemarks.Text;



                        Label lblMachineBrandId = (Label)e.Item.FindControl("lblMachineBrandId");
                        Label lblMachineId = (Label)e.Item.FindControl("lblMachineId");
                        Label lblSpareTypeId = (Label)e.Item.FindControl("lblSpareTypeId");
                        Label lblSpareId = (Label)e.Item.FindControl("lblSpareId");
                        Label lblRequestReasonId = (Label)e.Item.FindControl("lblRequestReasonId");


                        lblMachineBrandId.Text = ddlMachineBrands.SelectedValue;
                        lblMachineId.Text = ddlKnittingMachines.SelectedValue;
                        lblSpareTypeId.Text = ddlKnittingPartTypes.SelectedValue;
                        lblSpareId.Text = ddlSpares.SelectedValue;
                        lblRequestReasonId.Text = ddlRequestReasons.SelectedValue;


                        e.Item.FindControl("lnkbtnEdit").Visible = true;
                        e.Item.FindControl("lnkbtnUpdate").Visible = false;
                        e.Item.FindControl("lnkbtnCancel").Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
            else if (e.CommandName == "Cancel")
            {

                DropDownList ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                DropDownList ddlKnittingMachines = (DropDownList)e.Item.FindControl("ddlKnittingMachines");
                DropDownList ddlKnittingPartTypes = (DropDownList)e.Item.FindControl("ddlKnittingPartTypes");
                DropDownList ddlSpares = (DropDownList)e.Item.FindControl("ddlSpares");
                TextBox tbxQuantity = (TextBox)e.Item.FindControl("tbxQuantity");
                DropDownList ddlRequestReasons = (DropDownList)e.Item.FindControl("ddlRequestReasons");
                TextBox tbxRemarks = (TextBox)e.Item.FindControl("tbxRemarks");


                ddlMachineBrands.Visible = false;
                ddlKnittingMachines.Visible = false;
                ddlKnittingPartTypes.Visible = false;
                tbxQuantity.Visible = false;
                ddlSpares.Visible = false;
                ddlRequestReasons.Visible = false;
                tbxRemarks.Visible = false;


                Label lblMachineBrandValue = (Label)e.Item.FindControl("lblMachineBrandValue");
                Label lblMachineNameValue = (Label)e.Item.FindControl("lblMachineNameValue");
                Label lblSpareTypeValue = (Label)e.Item.FindControl("lblSpareTypeValue");
                Label lblSpareNameValue = (Label)e.Item.FindControl("lblSpareNameValue");
                Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
                Label lblRequestReason = (Label)e.Item.FindControl("lblRequestReason");
                Label lblRemarksValue = (Label)e.Item.FindControl("lblRemarksValue");


                lblMachineBrandValue.Visible = true;
                lblMachineNameValue.Visible = true;
                lblSpareTypeValue.Visible = true;
                lblSpareNameValue.Visible = true;
                lblQuantity.Visible = true;
                lblRequestReason.Visible = true;
                lblRemarksValue.Visible = true;


                e.Item.FindControl("lnkbtnEdit").Visible = true;
                e.Item.FindControl("lnkbtnUpdate").Visible = false;
                e.Item.FindControl("lnkbtnCancel").Visible = false;
            }
        }


        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {

            int issueFound = 0;

            try
            {
                List<KnittingSpareIssued> listKnittingSpareRequestIssued = new List<KnittingSpareIssued>();

                //List<KnittingSpareRequestDetails> listKnittingSpareRequestDetails = new List<KnittingSpareRequestDetails>();




                for (int rowIndex = 0; rowIndex < rptKnittingSpareRequests.Items.Count; rowIndex++)
                {

                    Label lblSpareRequestDetailId = (Label)this.rptKnittingSpareRequests.Items[rowIndex].FindControl("lblSpareRequestDetailId");
                    Label lblTotalIssued = (Label)this.rptKnittingSpareRequests.Items[rowIndex].FindControl("lblTotalIssued");
                    Label lblVerifiedQuantity = (Label)this.rptKnittingSpareRequests.Items[rowIndex].FindControl("lblVerifiedQuantity");
                    Label lblMachineNameValue = (Label)this.rptKnittingSpareRequests.Items[rowIndex].FindControl("lblMachineNameValue");
                    Label lblQtyAvailableValue = (Label)this.rptKnittingSpareRequests.Items[rowIndex].FindControl("lblQtyAvailableValue");



                    TextBox tbxIssueQuantity = (TextBox)this.rptKnittingSpareRequests.Items[rowIndex].FindControl("tbxIssueQuantity");
                    TextBox tbxIssueNotes = (TextBox)this.rptKnittingSpareRequests.Items[rowIndex].FindControl("tbxIssueNotes");


                    if (!string.IsNullOrEmpty(tbxIssueQuantity.Text))
                    {
                        if (int.Parse(lblVerifiedQuantity.Text) < (int.Parse(lblTotalIssued.Text) + int.Parse(tbxIssueQuantity.Text)))
                        {
                            issueFound++;
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cannot issue more than verified quantity for machine: " + lblMachineNameValue.Text.ToString() + "')", true);

                            break;
                        }
                        else if (int.Parse(lblQtyAvailableValue.Text) <  int.Parse(tbxIssueQuantity.Text))
                        {
                            issueFound++;
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cannot issue more than the quantity available: " + lblMachineNameValue.Text.ToString() + "')", true);
                            break;
                        }
                        else
                        {
                            var spareRequestIssued = new KnittingSpareIssued()
                            {

                                KnittingSpareRequestDetailsId = int.Parse(lblSpareRequestDetailId.Text),
                                IssueQuantity = int.Parse(tbxIssueQuantity.Text),
                                Notes = tbxIssueNotes.Text.ToString(),
                                CreateDate = DateTime.Now,
                                CreatedBy = CommonMethods.SessionInfo.UserName
                            };
                            listKnittingSpareRequestIssued.Add(spareRequestIssued);
                        }

                    }
                }

                if (issueFound == 0)
                {
                    foreach (var item in listKnittingSpareRequestIssued)
                    {
                        unitOfWork.GenericRepositories<KnittingSpareIssued>().Insert(item);
                    }

                    unitOfWork.Save();

                    foreach (var item in listKnittingSpareRequestIssued)
                    {
                        partManager.UpdateSpareSummaryForSpareIssue(item.KnittingSpareRequestDetailsId, item.IssueQuantity, CommonMethods.SessionInfo.UserName, DateTime.Now);
                    }


                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                }


            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);

            }

        }


    }
}