﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="RequestKnittingMachineSparesV2.aspx.cs" Inherits="MBTracker.Pages.Knitting_Spares.RequestKnittingMachineSparesV2" Async="true" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">


    <style>
        td > label {
            line-height: 13px
        }

        /*.table-bordered th, .table-bordered td {
            border-left: 1px solid #e0e0e0 !important;
            border-right: 1px solid #e0e0e0 !important;
            border-top: 1px solid #e0e0e0 !important;
            border-bottom: 1px solid #e0e0e0 !important;
        }*/


        .cblClass label {
            /*display: block;*/
            padding-left: 2px !important;
            padding-right: 14px !important;
        }
    </style>

    <div class="row-fluid">
        <div class="span9">

            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblSpareReceiveEntry" Text="Request Knitting Spares:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row-fluid" runat="server">
                        <div class="form-horizontal">

                            <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px;">
                                <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field
                            </div>

                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="padding-top: 20px;">
                                        <span style="font-weight: 700; color: #CC0000">*</span>
                                        <asp:Label ID="Label1" runat="server" Text="Select Knitting Unit:"></asp:Label></label>
                                    <div class="controls controls-row" style="padding-top: 20px;">
                                        <asp:DropDownList ID="ddlKnittingUnits" runat="server" AutoPostBack="true" Width="60%" CssClass="form-control" OnSelectedIndexChanged="ddlKnittingUnits_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <span style="font-weight: 700; color: #CC0000">*</span>
                                        <asp:Label ID="Label2" runat="server" Text="Select Machine Brand:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlMachineBrands" runat="server" AutoPostBack="true" CssClass="form-control" Width="60%" Style="min-width: 125px" OnSelectedIndexChanged="ddlMachineBrands_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <span style="font-weight: 700; color: #CC0000">*</span>
                                        <asp:Label ID="lblKnittingMachineMessage" runat="server" Text="Select Knitting Machine:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlMachineNames" runat="server" CssClass="form-control" Width="60%" Style="min-width: 125px"></asp:DropDownList>
                                    </div>
                                </div>


                                <div class="controls controls-row" style="padding-top: 20px; padding-bottom: 10px">
                                    <asp:Button ID="btnAddRow" runat="server" Visible="false" class="btn btn-add btn-small btnStyle pull-left" Width="81px" Text="Add Row(s)" OnClick="btnAddRow_Click" />
                                </div>



                            </div>

                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="padding-top: 20px; text-align: left">
                                        <asp:Label ID="lblListOfSpareMessage" runat="server" Text="List of Spares:" Font-Bold="true" Visible="false"></asp:Label></label>
                                </div>

                                <%--      <div class="controls controls-row">--%>
                                <div class="form-inline cblClass">
                                    <asp:CheckBoxList ID="cblSpares" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="2" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                </div>
                                <%-- </div>--%>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>



                </div>

            </div>

        </div>
    </div>



    <div class="row-fluid" id="divKnittingSpareSelection" runat="server" visible="false">
        <div class="span12">
            <div class="widget">

                <div class="widget-body">
                    <div class="form-horizontal">

                        <label for="inputOrder" class="control-label" style="text-align: left; height: 50px; width: 100%">
                            <span style="font-weight: 700; color: #CC0000">*</span>
                            <asp:Label ID="lblMessage" runat="server" Text="Select Spare Information:" Font-Bold="true"></asp:Label></label>

                        <%--  <div class="controls controls-row">--%>
                        <asp:Repeater ID="rptKnittingSpareRequestEntryInfo" runat="server" OnItemDataBound="rptKnittingSpareRequestEntryInfo_ItemDataBound">
                            <HeaderTemplate>
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="lblMachineBrand" runat="server" Text="Machine Brand"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblMachineName" runat="server" Text="Machine Name"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblSparesType" runat="server" Text="Spares Type"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblSpareName" runat="server" Text="Spare Name"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblRequestedQty" runat="server" Text="Quantity Requested"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblReasonForRequest" runat="server" Text="Request Reason"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="width: 12%">
                                        <asp:DropDownList ID="ddlMachineBrands" runat="server" AutoPostBack="true" CssClass="form-control" Style="min-width: 125px" OnSelectedIndexChanged="ddlMachineBrands_SelectedIndexChanged"></asp:DropDownList>
                                    </td>
                                    <td style="width: 18%">
                                        <asp:DropDownList ID="ddlMachineNames" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </td>

                                    <td style="width: 10%">
                                        <asp:DropDownList ID="ddlKnittingSpareTypes" runat="server" AutoPostBack="true" CssClass="form-control" ></asp:DropDownList>
                                    </td>
                                    <td style="width: 20%">
                                        <asp:DropDownList ID="ddlSpareNames" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </td>
                                    <td style="width: 10%">
                                        <asp:TextBox ID="tbxQuantityRequested" Width="100%" runat="server"></asp:TextBox></td>

                                    <td style="width: 15%">
                                        <asp:DropDownList ID="ddlRequestReasons" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </td>
                                    <td style="width: 15%">
                                        <asp:TextBox ID="tbxRemarks" Width="100%" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                 </table>
                                            <%--<div class="pull-right" style="margin-right: 0px; padding-top: 10px">
                                                <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="71px" Text="Add Row" OnClick="btnAddRow_Click" />
                                            </div>--%>
                            </FooterTemplate>
                        </asp:Repeater>
                        <%--     </div>--%>


                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>

            <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
            <%--<asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>--%>
        </div>
    </div>


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
