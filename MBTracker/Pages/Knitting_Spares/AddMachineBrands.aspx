﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddMachineBrands.aspx.cs" Inherits="MBTracker.Pages.Knitting_Spares.AddMachineBrands" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .dataTables_filter input{
            min-width:300px;
        }
        .active{
            color:green;
        }
        .inactive{
            color:red;
        }
        .yarn{
            color:blue;
        }
        .accessories{
            color:orangered;
        }
    </style>

    <div class="row-fluid">
       
        <div class="span5">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="roleActionTitle" Text="Add a New Machine Brands"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal no-margin">
                        <div style="text-align: right; font-size: 12px; line-height: 50px; padding-bottom: 7px">
                            <div class="col-md-12 col-sm-12">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                </div>
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Brand Name <span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxBrandName" required="true" CssClass="span12 form-control" type="text" ValidationGroup="save" placeholder="Brand Name"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxBrandName"><span style="font-weight: 700; color: #CC0000">Please enter brand name.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                       
                        <div class="control-group">
                            <label class="control-label">
                                Brand Description <span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxBrandDescription" required="true" CssClass="span12 form-control" type="text" ValidationGroup="save" placeholder="Brand Description"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxBrandDescription"><span style="font-weight: 700; color: #CC0000">Please enter Brand Description.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        
                        <div class="control-group">
                               <label class="control-label">
                                   Machine System <span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxMachineSystem" required="true" runat="server" placeholder="Machine System" TextMode="Number" CssClass="span12 form-control" ValidationGroup="save"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxMachineSystem"><span style="font-weight: 700; color: #CC0000">Please enter Machine System</span></asp:RequiredFieldValidator>
                                </div>
                            </div>

                        <div class="control-group">
                            <label class="control-label">
                                Gauge Low Range <span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxGaugeLowRange" required="true" CssClass="span12 form-control" type="text" ValidationGroup="save" placeholder="Gauge Low Range"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxGaugeLowRange"><span style="font-weight: 700; color: #CC0000">Please enter Gauge Low Range.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">
                                Gauge High Range <span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxGaugeHighRange" required="true" CssClass="span12 form-control" type="text" ValidationGroup="save" placeholder="Gauge High Range"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxGaugeHighRange"><span style="font-weight: 700; color: #CC0000">Please enter Gauge High Range.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">
                                Gauge Type <span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxGaugeType" required="true" CssClass="span12 form-control" type="text" ValidationGroup="save" placeholder="Gauge Type"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxGaugeType"><span style="font-weight: 700; color: #CC0000">Please enter Gauge Type.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">
                                Machine Brand Name <span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxMachineBrandName" required="true" CssClass="span12 form-control" type="text" ValidationGroup="save" placeholder="Machine Brand Name"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxMachineBrandName"><span style="font-weight: 700; color: #CC0000">Please enter Gauge Type.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                       

                        <div class="control-group">
                            <br />
                            <div class="controls-row">
                                <asp:Button ID="btnSubmit" runat="server" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnSave_Click" />
                                <%--<asp:Button ID="btnUpdate" runat="server" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" Visible="false" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnUpdate_Click" />--%>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>


        <div class="span7">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>Existing Machine Brands
                    </div>
                </div>
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
                            <asp:Repeater ID="rptMachineBrands" runat="server" OnItemCommand="rptBuyer_ItemCommand">
                                <HeaderTemplate>
                                    <thead>
                                        <tr>
                                            <th style="width: 40%">Brand Name</th>
                                            <th style="width: 20%">MachineBrand</th>
                                            <th style="width: 20%">MachineBrandName</th>
                                            <th style="width: 20%">Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="gradeX">
                                        <td style="text-align: left"><%#Eval("BrandName") %> </td>
                                        <td style="text-align: center"><%#Eval("MachineBrand") %>  </td>
                                        <td style="text-align: center"><%#Eval("MachineBrandName") %> </td>
                                        <td>
                                            <%--<asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>&nbsp;&nbsp;--%>
                                           <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-small hidden-phone" Text="Delete"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
