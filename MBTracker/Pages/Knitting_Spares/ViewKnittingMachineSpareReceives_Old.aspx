﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewKnittingMachineSpareReceives_Old.aspx.cs" Inherits="MBTracker.Pages.Knitting_Spares.ViewKnittingMachineSpareReceives_Old" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid" id="divBuyerAndStyle" runat="server">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="View Spare Receives:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="form-horizontal">

                        <div style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 25px">
                            <div class="pull-right" style="padding-bottom: 10px">
                                <span style="font-weight: 700;"></span><span style="font-weight: 700; color: #CC0000">*</span>Required Field
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <span style="font-weight: 700; color: #CC0000">*</span>
                                <asp:Label ID="lblFromDate" runat="server" Text="From Date:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxFromDateReceived" runat="server" TextMode="Date" Width="50%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <span style="font-weight: 700; color: #CC0000">*</span>
                                <asp:Label ID="lblToDate" runat="server" Text="To Date:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxToDateReceived" runat="server" TextMode="Date" Width="50%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="&nbsp;"></asp:Label></label>
                            <div class="controls controls-row">
                                <br />
                                <asp:Button ID="btnViewKnittingParts" runat="server" class="btn btn-success btn-midium pull-left btnStyle" Text="View Spare Receives" OnClick="btnViewKnittingParts_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <asp:Label ID="lblNoReceivesFound" Style="width: 100%" runat="server" Visible="false" Text="No spare was received during this period." BackColor="#ffff00"></asp:Label>
            <asp:Panel ID="pnlViewSpareReceives" runat="server" Visible="false">

                <div class="widget">
                    <div class="widget-body">
                        <div class="form-horizontal">
                            <div class="control-group" style="overflow-x: auto">
                                <label for="inputStyle" class="control-label" style="font-weight: bold; text-align: left">
                                    <asp:Label ID="Label1" runat="server" Style="font-weight: bold; text-align: left" Font-Bold="false" Text=""></asp:Label></label>
                                <div class="controls-row">
                                    <asp:Repeater ID="rptKnittingSpareReceives" runat="server" OnItemCommand="rptKnittingSpareReceives_ItemCommand">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblDateReceived" runat="server" Text="Date Received"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblSpareName" runat="server" Text="Spare Name"></asp:Label></th>
                                                        <%-- <th>
                                                            <asp:Label ID="lblSpareCode" runat="server" Text="Spare Code"></asp:Label></th>--%>
                                                        <th>
                                                            <asp:Label ID="lblSpareType" runat="server" Text="Spare Types"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblRequisitionNumber" runat="server" Text="Req. Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblBillNumber" runat="server" Text="Bill Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblChalanNumber" runat="server" Text="Chalan Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblOtherRefNumber" runat="server" Text="Other Ref."></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblSupplier" runat="server" Text="Supplier"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblSupplierCode" runat="server" Text="Supplier Code"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblDescription" runat="server" Text="Description"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblQuantity" runat="server" Text="Quantity"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="LabelAction" runat="server" Text="Action"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="vertical-align: middle; text-align: left; width: 15%">
                                                    <asp:Label ID="lblKnittingSpareReceiveId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingSpareReceiveId")%>'></asp:Label>
                                                    <asp:Label ID="lblKnittingSpareReceiveDetailsId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingSpareReceiveDetailsId")%>'></asp:Label>
                                                    <asp:Label ID="lblDateReceivedValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ReceiveDate")%>' Width="100%"></asp:Label>
                                                    <asp:TextBox ID="tbxDateReceive" Width="100%" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ReceiveDate")%>' Visible="false"></asp:TextBox>
                                                </td>

                                                <td style="vertical-align: middle; text-align: left; width: 15%">
                                                    <asp:Label ID="lblKnittingSpareId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingSpareId")%>'></asp:Label>
                                                    <asp:Label ID="lblSpareNameValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SpareName")%>' Width="100%"></asp:Label>
                                                    <asp:DropDownList ID="ddlSpares" runat="server" CssClass="form-control" Visible="false"></asp:DropDownList>
                                                </td>

                                                <td style="vertical-align: middle; text-align: left; width: 15%">
                                                    <asp:Label ID="Label3" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingSpareTypeId")%>'></asp:Label>
                                                    <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SpareName")%>' Width="100%"></asp:Label>
                                                    <asp:DropDownList ID="ddlSpareType" runat="server" CssClass="form-control" Visible="false"></asp:DropDownList>
                                                </td>

                                                <td style="vertical-align: middle; text-align: left; width: 15%">
                                                    <asp:Label ID="lblRequisitionNumberValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RequisitionNumber")%>' Width="100%"></asp:Label>
                                                    <asp:TextBox ID="tbxRequisitionNumber" Width="100%" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RequisitionNumber")%>' Visible="false"></asp:TextBox>
                                                </td>

                                                <td style="vertical-align: middle; text-align: left; width: 15%">
                                                    <asp:Label ID="lblInvoiceNumberValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "InvoiceNumber")%>' Width="100%"></asp:Label>
                                                    <asp:TextBox ID="tbxInvoiceNumber" Width="100%" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "InvoiceNumber")%>' Visible="false"></asp:TextBox>
                                                </td>

                                                <td style="vertical-align: middle; text-align: left; width: 15%">
                                                    <asp:Label ID="lblChalanNumberValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ChalanNumber")%>' Width="100%"></asp:Label>
                                                    <asp:TextBox ID="tbxChalanNumber" Width="100%" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ChalanNumber")%>' Visible="false"></asp:TextBox>
                                                </td>

                                                <td style="vertical-align: middle; text-align: left; width: 15%">
                                                    <asp:Label ID="lblOtherRefNumberValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OtherRefNumber")%>' Width="100%"></asp:Label>
                                                    <asp:TextBox ID="tbxOtherRefNumber" Width="100%" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OtherRefNumber")%>' Visible="false"></asp:TextBox>
                                                </td>

                                                <td style="vertical-align: middle; text-align: left; width: 15%">
                                                    <asp:Label ID="lblSupplierId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "SupplierId")%>'></asp:Label>
                                                    <asp:Label ID="lblSupplierNameValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SupplierName")%>' Width="100%"></asp:Label>
                                                    <asp:DropDownList ID="ddlSuppliers" runat="server" CssClass="form-control" Visible="false"></asp:DropDownList>
                                                </td>


                                                <td style="vertical-align: middle; text-align: left; width: 15%">
                                                    <asp:Label ID="lblSpareDescriptionValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SpareDescription")%>' Width="100%"></asp:Label>
                                                    <asp:TextBox ID="tbxSpareDescription" Width="100%" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SpareDescription")%>' Visible="false"></asp:TextBox>
                                                </td>

                                                <td style="vertical-align: middle; text-align: left; width: 15%">
                                                    <asp:Label ID="lblSpareQuantityValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SpareQuantity")%>' Width="100%"></asp:Label>
                                                    <asp:TextBox ID="tbxSpareQuantity" Width="100%" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SpareQuantity")%>' Visible="false"></asp:TextBox>
                                                </td>

                                                <td style="vertical-align: middle; text-align: left; width: 15%">
                                                    <asp:Label ID="lblSpareUnitPriceValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SpareUnitPrice")%>' Width="100%"></asp:Label>
                                                    <asp:TextBox ID="tbxSpareUnitPrice" Width="100%" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SpareUnitPrice")%>' Visible="false"></asp:TextBox>
                                                </td>

                                                <td style="vertical-align: middle; text-align: center; width: 25%">
                                                    <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="&nbsp;&nbsp;Edit&nbsp;&nbsp;"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnUpdate" runat="server" Visible="false" CommandName="Update" CommandArgument='<%#Eval("Id") %>' class="btn btn-warning2 btn-mini" Text="Update"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnCancel" runat="server" Visible="false" CommandName="Cancel" CommandArgument='<%#Eval("Id") %>' class="btn btn-warning2 btn-mini" Text="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;&nbsp;"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </asp:Panel>


        </div>
    </div>


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
