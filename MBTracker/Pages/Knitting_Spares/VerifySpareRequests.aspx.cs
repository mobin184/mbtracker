﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Knitting_Parts;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Knitting_Spares
{
    public partial class VerifySpareRequests : System.Web.UI.Page
    {

        PartsManager partManager = new PartsManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
            }
        }

        protected void ddlKnittingUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlKnittingUnits.SelectedValue != "")
            {
                LoadMachineBrandsByUnit(ddlMachineBrands, int.Parse(ddlKnittingUnits.SelectedValue));
            }
            else
            {
                ddlMachineBrands.Items.Clear();
                pnlViewKnittingSpareRequests.Visible = false;
            }
        }

        private void LoadMachineBrandsByUnit(DropDownList ddl, int unitId)
        {
            string sql = $"Exec usp_GetAllMachineBrandByUnitId {unitId}";
            var dt = unitOfWork.GetDataTableFromSql(sql);
            DataRow dr = dt.NewRow();
            dr["MachineBrandId"] = "";
            dr["MachineBrand"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);

            ddl.DataTextField = "MachineBrand";
            ddl.DataValueField = "MachineBrandId";
            ddl.DataSource = dt;
            ddl.DataBind();
        }


        protected void btnViewKnittingParts_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();


            int knittingUnitId = 0;
            int machineBrandId = 0;
            DateTime fromDate = DateTime.Now.AddDays(-30);

            if (ddlKnittingUnits.SelectedValue != "")
            {

                if (ddlMachineBrands.SelectedValue != "")
                {
                    machineBrandId = int.Parse(ddlMachineBrands.SelectedValue);
                }

                if (tbxFromDate.Text != "")
                {
                    fromDate = DateTime.Parse(tbxFromDate.Text);
                }

                knittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue);

                dt = partManager.GetKnittingSpareRequests(knittingUnitId, machineBrandId, fromDate);

                if (dt.Rows.Count > 0)
                {
                    rptKnittingSpareRequests.DataSource = dt;
                    rptKnittingSpareRequests.DataBind();
                    lblNoRequestFound.Visible = false;
                    pnlViewKnittingSpareRequests.Visible = true;
                }
                else
                {
                    lblNoRequestFound.Visible = true;
                    pnlViewKnittingSpareRequests.Visible = false;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Select a knitting unit.')", true);
            }
        }



        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {
                List<KnittingSpareRequestsVerified> listKnittingSpareRequestVerified = new List<KnittingSpareRequestsVerified>();

                List<KnittingSpareRequestDetails> listKnittingSpareRequestDetails = new List<KnittingSpareRequestDetails>();

                


                for (int rowIndex = 0; rowIndex < rptKnittingSpareRequests.Items.Count; rowIndex++)
                {

                    Label lblSpareRequestDetailId = (Label)this.rptKnittingSpareRequests.Items[rowIndex].FindControl("lblSpareRequestDetailId");
                    TextBox tbxVerifiedQty = (TextBox)this.rptKnittingSpareRequests.Items[rowIndex].FindControl("tbxVerifiedQty");
                    TextBox tbxVerificationNote = (TextBox)this.rptKnittingSpareRequests.Items[rowIndex].FindControl("tbxVerificationNote");


                    if (!string.IsNullOrEmpty(tbxVerifiedQty.Text))
                    {
                        var spareRequestVerified = new KnittingSpareRequestsVerified()
                        {

                            KnittingSpareRequestDetailsId = int.Parse(lblSpareRequestDetailId.Text),
                            VerifiedQuantity = int.Parse(tbxVerifiedQty.Text),
                            Notes = tbxVerificationNote.Text.ToString(),
                            CreateDate = DateTime.Now,
                            CreatedBy = CommonMethods.SessionInfo.UserName
                        };
                        listKnittingSpareRequestVerified.Add(spareRequestVerified);


                        var spareRequestDetail = unitOfWork.GenericRepositories<KnittingSpareRequestDetails>().GetByID(int.Parse(lblSpareRequestDetailId.Text));
                        spareRequestDetail.Id = int.Parse(lblSpareRequestDetailId.Text);
                        spareRequestDetail.Status = 1;
                        spareRequestDetail.UpdatedBy = CommonMethods.SessionInfo.UserName;
                        spareRequestDetail.UpdateDate = DateTime.Now;
                        listKnittingSpareRequestDetails.Add(spareRequestDetail);

                    }
                }

                foreach (var item in listKnittingSpareRequestVerified)
                {
                    unitOfWork.GenericRepositories<KnittingSpareRequestsVerified>().Insert(item);
                }

                foreach (var item in listKnittingSpareRequestDetails)
                {
                    unitOfWork.GenericRepositories<KnittingSpareRequestDetails>().Update(item);
                }



                unitOfWork.Save();
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);

            }

        }
    }
}