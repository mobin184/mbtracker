﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Knitting_Parts;
using MBTracker.EF;
using Repositories;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Knitting_Spares
{
    public partial class ViewKnittingMachineSpareRequests : System.Web.UI.Page
    {

        PartsManager partManager = new PartsManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);

                if (Request.QueryString["showInfo"] != null)
                {
                    var spareRequestId = int.Parse(Tools.UrlDecode(Request.QueryString["showInfo"]));
                    divUnitSelection.Visible = false;
                    ShowSpareRequests(spareRequestId);


                    //var info = unitOfWork.GenericRepositories<ReportedProblems>().GetByID(spareRequestId);
                    //divBuyerAndStyle.Visible = false;

                    //if (CommonMethods.HasBuyerAssignedToUser(info.BuyerId, CommonMethods.SessionInfo.UserId))
                    //{
                    //    ShowReportedProblems(info.StyleId);
                    //}
                    //else
                    //{
                    //    lblNotAuthorizedToView.Visible = true;
                    //}
                }
            }
        }

        protected void ddlKnittingUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlKnittingUnits.SelectedValue != "")
            {
                LoadMachineBrandsByUnit(ddlMachineBrands, int.Parse(ddlKnittingUnits.SelectedValue));
            }
            else
            {
                ddlMachineBrands.Items.Clear();
            }
        }

        private void LoadMachineBrandsByUnit(DropDownList ddl, int unitId)
        {
            string sql = $"Exec usp_GetAllMachineBrandByUnitId {unitId}";
            var dt = unitOfWork.GetDataTableFromSql(sql);
            DataRow dr = dt.NewRow();
            dr["MachineBrandId"] = "";
            dr["MachineBrand"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);

            ddl.DataTextField = "MachineBrand";
            ddl.DataValueField = "MachineBrandId";
            ddl.DataSource = dt;
            ddl.DataBind();
        }

        private void ShowSpareRequests(int spareRequestId)
        {
            DataTable dt = new DataTable();
            dt = partManager.GetKnittingSpareRequestsById(spareRequestId);

            if (dt.Rows.Count > 0)
            {
                rptKnittingSpareRequests.DataSource = dt;
                rptKnittingSpareRequests.DataBind();
                lblNoRequestFound.Visible = false;
                pnlViewKnittingSpareRequests.Visible = true;
            }
            else
            {
                lblNoRequestFound.Visible = true;
                pnlViewKnittingSpareRequests.Visible = false;
            }


        }


        protected void btnViewKnittingParts_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();


            int knittingUnitId = 0;
            int machineBrandId = 0;
            DateTime fromDate = DateTime.Now.AddDays(-30);

            if (ddlKnittingUnits.SelectedValue != "")
            {

                if (ddlMachineBrands.SelectedValue != "")
                {
                    machineBrandId = int.Parse(ddlMachineBrands.SelectedValue);
                }

                if (tbxFromDate.Text != "")
                {
                    fromDate = DateTime.Parse(tbxFromDate.Text);
                }

                knittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue);

                dt = partManager.GetKnittingSpareRequests(knittingUnitId, machineBrandId, fromDate);

                if (dt.Rows.Count > 0)
                {
                    rptKnittingSpareRequests.DataSource = dt;
                    rptKnittingSpareRequests.DataBind();
                    lblNoRequestFound.Visible = false;
                    pnlViewKnittingSpareRequests.Visible = true;
                }
                else
                {
                    lblNoRequestFound.Visible = true;
                    pnlViewKnittingSpareRequests.Visible = false;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Select a knitting unit.')", true);
            }
        }


        protected void rptKnittingSpareRequests_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {

                DropDownList ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                Label lblMachineBrandId = (Label)e.Item.FindControl("lblMachineBrandId");
                LoadMachineBrandsByUnit(ddlMachineBrands, int.Parse(ddlKnittingUnits.SelectedValue));
                ddlMachineBrands.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrands, string.IsNullOrEmpty(lblMachineBrandId.Text.ToString()) ? 0 : int.Parse(lblMachineBrandId.Text.ToString()));


                DropDownList ddlKnittingMachines = (DropDownList)e.Item.FindControl("ddlKnittingMachines");
                Label lblMachineId = (Label)e.Item.FindControl("lblMachineId");
                LoadMachinesByBrandAndUnit(ddlKnittingMachines, int.Parse(ddlMachineBrands.SelectedValue), int.Parse(ddlKnittingUnits.SelectedValue));
                ddlKnittingMachines.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnittingMachines, string.IsNullOrEmpty(lblMachineId.Text.ToString()) ? 0 : int.Parse(lblMachineId.Text.ToString()));

                DropDownList ddlKnittingPartTypes = (DropDownList)e.Item.FindControl("ddlKnittingPartTypes");
                Label lblSpareTypeId = (Label)e.Item.FindControl("lblSpareTypeId");
                LoadSpareTypesDropdown(ddlKnittingPartTypes);
                ddlKnittingPartTypes.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnittingPartTypes, string.IsNullOrEmpty(lblSpareTypeId.Text.ToString()) ? 0 : int.Parse(lblSpareTypeId.Text.ToString()));



                DropDownList ddlSpares = (DropDownList)e.Item.FindControl("ddlSpares");
                Label lblSpareId = (Label)e.Item.FindControl("lblSpareId");
                LoadSpareNames(ddlSpares, int.Parse(ddlKnittingPartTypes.SelectedValue), int.Parse(ddlMachineBrands.SelectedValue));
                ddlSpares.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSpares, string.IsNullOrEmpty(lblSpareId.Text.ToString()) ? 0 : int.Parse(lblSpareId.Text.ToString()));

                TextBox tbxQuantity = (TextBox)e.Item.FindControl("tbxQuantity");


                DropDownList ddlRequestReasons = (DropDownList)e.Item.FindControl("ddlRequestReasons");
                Label lblRequestReasonId = (Label)e.Item.FindControl("lblRequestReasonId");
                LoadSpareChangeReasons(ddlRequestReasons);
                ddlRequestReasons.SelectedIndex = CommonMethods.MatchDropDownItem(ddlRequestReasons, string.IsNullOrEmpty(lblRequestReasonId.Text.ToString()) ? 0 : int.Parse(lblRequestReasonId.Text.ToString()));


                TextBox tbxRemarks = (TextBox)e.Item.FindControl("tbxRemarks");


                ddlMachineBrands.Visible = true;
                ddlKnittingMachines.Visible = true;
                ddlKnittingPartTypes.Visible = true;
                tbxQuantity.Visible = true;
                ddlSpares.Visible = true;
                ddlRequestReasons.Visible = true;
                tbxRemarks.Visible = true;




                Label lblMachineBrandValue = (Label)e.Item.FindControl("lblMachineBrandValue");
                Label lblMachineNameValue = (Label)e.Item.FindControl("lblMachineNameValue");
                Label lblSpareTypeValue = (Label)e.Item.FindControl("lblSpareTypeValue");
                Label lblSpareNameValue = (Label)e.Item.FindControl("lblSpareNameValue");
                Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
                Label lblRequestReason = (Label)e.Item.FindControl("lblRequestReason");
                Label lblRemarksValue = (Label)e.Item.FindControl("lblRemarksValue");



                lblMachineBrandValue.Visible = false;
                lblMachineNameValue.Visible = false;
                lblSpareTypeValue.Visible = false;
                lblSpareNameValue.Visible = false;
                lblQuantity.Visible = false;
                lblRequestReason.Visible = false;
                lblRemarksValue.Visible = false;


                e.Item.FindControl("lnkbtnEdit").Visible = false;
                e.Item.FindControl("lnkbtnUpdate").Visible = true;
                e.Item.FindControl("lnkbtnCancel").Visible = true;

            }
            else if (e.CommandName == "Update")
            {
                try
                {
                    DropDownList ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                    DropDownList ddlKnittingMachines = (DropDownList)e.Item.FindControl("ddlKnittingMachines");
                    DropDownList ddlKnittingPartTypes = (DropDownList)e.Item.FindControl("ddlKnittingPartTypes");
                    DropDownList ddlSpares = (DropDownList)e.Item.FindControl("ddlSpares");
                    TextBox tbxQuantity = (TextBox)e.Item.FindControl("tbxQuantity");
                    DropDownList ddlRequestReasons = (DropDownList)e.Item.FindControl("ddlRequestReasons");
                    TextBox tbxRemarks = (TextBox)e.Item.FindControl("tbxRemarks");


                    if (ddlMachineBrands.SelectedIndex == 0 || ddlKnittingMachines.SelectedIndex == 0 || ddlKnittingPartTypes.SelectedIndex == 0 || ddlSpares.SelectedIndex == 0 || tbxQuantity.Text.ToString() == "" || ddlRequestReasons.SelectedIndex == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Selection or data is invalid.')", true);
                    }
                    else
                    {
                        int spareRequestDetailId = Convert.ToInt32(e.CommandArgument.ToString());

                        var spareRequestDetail = unitOfWork.GenericRepositories<KnittingSpareRequestDetails>().GetByID(spareRequestDetailId);

                        spareRequestDetail.Id = spareRequestDetailId;
                        spareRequestDetail.KnittingMachineBrandId = int.Parse(ddlMachineBrands.SelectedValue);
                        spareRequestDetail.KnittingMachineId = int.Parse(ddlKnittingMachines.SelectedValue);
                        spareRequestDetail.KnittingSpareTypeId = int.Parse(ddlKnittingPartTypes.SelectedValue);
                        spareRequestDetail.KnittingSpareId = int.Parse(ddlSpares.SelectedValue);
                        spareRequestDetail.QuantityRequested = int.Parse(tbxQuantity.Text);
                        spareRequestDetail.ReasonForRequestId = int.Parse(ddlRequestReasons.SelectedValue);
                        spareRequestDetail.Remarks = tbxRemarks.Text.ToString();

                        spareRequestDetail.UpdatedBy = CommonMethods.SessionInfo.UserName;
                        spareRequestDetail.UpdateDate = DateTime.Now;



                        unitOfWork.GenericRepositories<KnittingSpareRequestDetails>().Update(spareRequestDetail);
                        unitOfWork.Save();

                        ddlMachineBrands.Visible = false;
                        ddlKnittingMachines.Visible = false;
                        ddlKnittingPartTypes.Visible = false;
                        tbxQuantity.Visible = false;
                        ddlSpares.Visible = false;
                        ddlRequestReasons.Visible = false;
                        tbxRemarks.Visible = false;



                        Label lblMachineBrandValue = (Label)e.Item.FindControl("lblMachineBrandValue");
                        Label lblMachineNameValue = (Label)e.Item.FindControl("lblMachineNameValue");
                        Label lblSpareTypeValue = (Label)e.Item.FindControl("lblSpareTypeValue");
                        Label lblSpareNameValue = (Label)e.Item.FindControl("lblSpareNameValue");
                        Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
                        Label lblRequestReason = (Label)e.Item.FindControl("lblRequestReason");
                        Label lblRemarksValue = (Label)e.Item.FindControl("lblRemarksValue");


                        lblMachineBrandValue.Visible = true;
                        lblMachineNameValue.Visible = true;
                        lblSpareTypeValue.Visible = true;
                        lblSpareNameValue.Visible = true;
                        lblQuantity.Visible = true;
                        lblRequestReason.Visible = true;
                        lblRemarksValue.Visible = true;


                        lblMachineBrandValue.Text = ddlMachineBrands.SelectedItem.Text;
                        lblMachineNameValue.Text = ddlKnittingMachines.SelectedItem.Text;
                        lblSpareTypeValue.Text = ddlKnittingPartTypes.SelectedItem.Text;
                        lblSpareNameValue.Text = ddlSpares.SelectedItem.Text;
                        lblQuantity.Text = tbxQuantity.Text;
                        lblRequestReason.Text = ddlRequestReasons.SelectedItem.Text;
                        lblRemarksValue.Text = tbxRemarks.Text;



                        Label lblMachineBrandId = (Label)e.Item.FindControl("lblMachineBrandId");
                        Label lblMachineId = (Label)e.Item.FindControl("lblMachineId");
                        Label lblSpareTypeId = (Label)e.Item.FindControl("lblSpareTypeId");
                        Label lblSpareId = (Label)e.Item.FindControl("lblSpareId");
                        Label lblRequestReasonId = (Label)e.Item.FindControl("lblRequestReasonId");


                        lblMachineBrandId.Text = ddlMachineBrands.SelectedValue;
                        lblMachineId.Text = ddlKnittingMachines.SelectedValue;
                        lblSpareTypeId.Text = ddlKnittingPartTypes.SelectedValue;
                        lblSpareId.Text = ddlSpares.SelectedValue;
                        lblRequestReasonId.Text = ddlRequestReasons.SelectedValue;


                        e.Item.FindControl("lnkbtnEdit").Visible = true;
                        e.Item.FindControl("lnkbtnUpdate").Visible = false;
                        e.Item.FindControl("lnkbtnCancel").Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
            else if (e.CommandName == "Cancel")
            {

                DropDownList ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                DropDownList ddlKnittingMachines = (DropDownList)e.Item.FindControl("ddlKnittingMachines");
                DropDownList ddlKnittingPartTypes = (DropDownList)e.Item.FindControl("ddlKnittingPartTypes");
                DropDownList ddlSpares = (DropDownList)e.Item.FindControl("ddlSpares");
                TextBox tbxQuantity = (TextBox)e.Item.FindControl("tbxQuantity");
                DropDownList ddlRequestReasons = (DropDownList)e.Item.FindControl("ddlRequestReasons");
                TextBox tbxRemarks = (TextBox)e.Item.FindControl("tbxRemarks");


                ddlMachineBrands.Visible = false;
                ddlKnittingMachines.Visible = false;
                ddlKnittingPartTypes.Visible = false;
                tbxQuantity.Visible = false;
                ddlSpares.Visible = false;
                ddlRequestReasons.Visible = false;
                tbxRemarks.Visible = false;


                Label lblMachineBrandValue = (Label)e.Item.FindControl("lblMachineBrandValue");
                Label lblMachineNameValue = (Label)e.Item.FindControl("lblMachineNameValue");
                Label lblSpareTypeValue = (Label)e.Item.FindControl("lblSpareTypeValue");
                Label lblSpareNameValue = (Label)e.Item.FindControl("lblSpareNameValue");
                Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
                Label lblRequestReason = (Label)e.Item.FindControl("lblRequestReason");
                Label lblRemarksValue = (Label)e.Item.FindControl("lblRemarksValue");


                lblMachineBrandValue.Visible = true;
                lblMachineNameValue.Visible = true;
                lblSpareTypeValue.Visible = true;
                lblSpareNameValue.Visible = true;
                lblQuantity.Visible = true;
                lblRequestReason.Visible = true;
                lblRemarksValue.Visible = true;


                e.Item.FindControl("lnkbtnEdit").Visible = true;
                e.Item.FindControl("lnkbtnUpdate").Visible = false;
                e.Item.FindControl("lnkbtnCancel").Visible = false;
            }
        }


        private void LoadMachinesByBrandAndUnit(DropDownList ddl, int machineBrandId, int knittingUnitId)
        {
            CommonMethods.LoadKnittingMachinesDropdown(ddl, 3, 0, machineBrandId, knittingUnitId);
        }


        private void LoadSpareTypesDropdown(DropDownList ddl)
        {
            CommonMethods.LoadPartTypeDropdown(ddl, 1, 0);
        }

        private void LoadSpareNames(DropDownList ddl, int spareTypeId, int machineBrandId)
        {
            CommonMethods.LoadSpareNamesDropdown(ddl, 5, 0, spareTypeId, machineBrandId);
        }


        private void LoadSpareChangeReasons(DropDownList ddl)
        {
            CommonMethods.LoadWhyNeedleChangeDropdown(ddl, 1, 0);
        }


        protected void ddlMachineBrands_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlMachineBrands = (DropDownList)sender;
            DropDownList ddlKnittingMachines = (DropDownList)((RepeaterItem)ddlMachineBrands.NamingContainer).FindControl("ddlKnittingMachines");

            DropDownList ddlKnittingSpareTypes = (DropDownList)((RepeaterItem)ddlMachineBrands.NamingContainer).FindControl("ddlKnittingPartTypes");
            DropDownList ddlSpares = (DropDownList)((RepeaterItem)ddlMachineBrands.NamingContainer).FindControl("ddlSpares");


            if (ddlMachineBrands.SelectedValue != "" && ddlKnittingUnits.SelectedValue != "")
            {
                ddlKnittingMachines.Items.Clear();
                LoadMachinesByBrandAndUnit(ddlKnittingMachines, int.Parse(ddlMachineBrands.SelectedValue), int.Parse(ddlKnittingUnits.SelectedValue));

                ddlSpares.Items.Clear();
                LoadSpareNames(ddlSpares, int.Parse(ddlKnittingSpareTypes.SelectedValue), int.Parse(ddlMachineBrands.SelectedValue));
            }
            else
            {
                ddlKnittingMachines.Items.Clear();
                ddlSpares.Items.Clear();
            }
        }


        protected void ddlKnittingPartTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlKnittingSpareTypes = (DropDownList)sender;
            DropDownList ddlMachineBrands = (DropDownList)((RepeaterItem)ddlKnittingSpareTypes.NamingContainer).FindControl("ddlMachineBrands");
            DropDownList ddlSpares = (DropDownList)((RepeaterItem)ddlKnittingSpareTypes.NamingContainer).FindControl("ddlSpares");

            if (ddlKnittingSpareTypes.SelectedValue != "" && ddlMachineBrands.SelectedValue != "")
            {
                ddlSpares.Items.Clear();
                LoadSpareNames(ddlSpares, int.Parse(ddlKnittingSpareTypes.SelectedValue), int.Parse(ddlMachineBrands.SelectedValue));

            }
            else
            {
                ddlSpares.Items.Clear();
            }
        }



    }
}