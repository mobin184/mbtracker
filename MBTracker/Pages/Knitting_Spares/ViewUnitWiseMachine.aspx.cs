﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Knitting_Parts;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Knitting_Spares
{
    public partial class ViewUnitWiseMachine : System.Web.UI.Page
    {
        PartsManager partManager = new PartsManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadMachineBrandDropdown(ddlMachineBrands, 1, 0);
                CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
            }
        }

        protected void btnViewKnittingMachines_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();

            int machineBrandId = 0;
            int knittingUnitId = 0;

            if (ddlMachineBrands.SelectedValue != "")
            {
                machineBrandId = int.Parse(ddlMachineBrands.SelectedValue);
            }
            if (ddlKnittingUnits.SelectedValue != "")
            {
                knittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue);
            }


            dt = partManager.GetExistingProductionMachines(machineBrandId, knittingUnitId);

            if (dt.Rows.Count > 0)
            {
                rptKnittingMachines.DataSource = dt;
                rptKnittingMachines.DataBind();
                lblNoKnittingMachineFound.Visible = false;
                pnlViewKnittingMachines.Visible = true;
            }
            else
            {
                lblNoKnittingMachineFound.Visible = true;
                pnlViewKnittingMachines.Visible = false;
            }

        }

        protected void rptKnittingMachines_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label lblNeedleCapacity = (Label)e.Item.FindControl("lblNeedleCapacity");
            TextBox tbxNumberOfMachines = (TextBox)e.Item.FindControl("tbxNumberOfMachines");
            if (e.CommandName == "Edit")
            {

                DropDownList ddlKnittingUnits = (DropDownList)e.Item.FindControl("ddlKnittingUnits");
                Label lblKnittingUnitId = (Label)e.Item.FindControl("lblKnittingUnitId");
               
                CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
                

                ddlKnittingUnits.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnittingUnits, string.IsNullOrEmpty(lblKnittingUnitId.Text.ToString()) ? 0 : int.Parse(lblKnittingUnitId.Text.ToString()));
                ddlKnittingUnits.Visible = true;
                

                Label lblKnittingUnitValue = (Label)e.Item.FindControl("lblKnittingUnitValue");
                lblKnittingUnitValue.Visible = false;
                tbxNumberOfMachines.Text = lblNeedleCapacity.Text.Trim();
                tbxNumberOfMachines.Visible = true;
                lblNeedleCapacity.Visible = false;

                e.Item.FindControl("lnkbtnEdit").Visible = false;
                e.Item.FindControl("lnkbtnUpdate").Visible = true;
                e.Item.FindControl("lnkbtnCancel").Visible = true;

            }
            else if (e.CommandName == "Update")
            {
                try
                {
                    DropDownList ddlKnittingUnits = (DropDownList)e.Item.FindControl("ddlKnittingUnits");

                    if (ddlKnittingUnits.SelectedIndex == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Select a knitting unit.')", true);
                    }
                    else
                    {
                        ddlKnittingUnits.Visible = false;

                        Label lblKnittingUnitValue = (Label)e.Item.FindControl("lblKnittingUnitValue");
                        lblKnittingUnitValue.Visible = true;

                        
                       // lblKnittingUnitValue.Visible = true;

                        int knittingMachineId = Convert.ToInt32(e.CommandArgument.ToString());
                        var ProductonUnitMachineBrands = unitOfWork.GenericRepositories<ProductonUnitMachineBrand>().GetByID(knittingMachineId);

                        ProductonUnitMachineBrands.ProductionUnitId = int.Parse(ddlKnittingUnits.SelectedValue);
                        ProductonUnitMachineBrands.NumberOfMachines = int.Parse(tbxNumberOfMachines.Text);
                        ProductonUnitMachineBrands.UpdatedBy = CommonMethods.SessionInfo.UserName;
                        ProductonUnitMachineBrands.UpdateDate = DateTime.Now;

                        unitOfWork.GenericRepositories<ProductonUnitMachineBrand>().Update(ProductonUnitMachineBrands);
                        unitOfWork.Save();

                        lblKnittingUnitValue.Text = ddlKnittingUnits.SelectedItem.Text;
                        Label lblKnittingUnitId = (Label)e.Item.FindControl("lblKnittingUnitId");
                        lblKnittingUnitId.Text = ddlKnittingUnits.SelectedValue;

                        lblNeedleCapacity.Text = tbxNumberOfMachines.Text; 
                        lblNeedleCapacity.Visible = true;


                        tbxNumberOfMachines.Visible = false;
                        //lblNeedleCapacity.Visible = true;

                        e.Item.FindControl("lnkbtnEdit").Visible = true;
                        e.Item.FindControl("lnkbtnUpdate").Visible = false;
                        e.Item.FindControl("lnkbtnCancel").Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
            else if (e.CommandName == "Cancel")
            {

                DropDownList ddlKnittingUnits = (DropDownList)e.Item.FindControl("ddlKnittingUnits");
                ddlKnittingUnits.Visible = false;

                Label lblKnittingUnitValue = (Label)e.Item.FindControl("lblKnittingUnitValue");
                lblKnittingUnitValue.Visible = true;
                tbxNumberOfMachines.Visible = false;
                lblNeedleCapacity.Visible = true;

                e.Item.FindControl("lnkbtnEdit").Visible = true;
                e.Item.FindControl("lnkbtnUpdate").Visible = false;
                e.Item.FindControl("lnkbtnCancel").Visible = false;
            }
        }
    }
}