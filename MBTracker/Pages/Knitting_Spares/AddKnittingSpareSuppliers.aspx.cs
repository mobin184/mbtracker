﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Knitting_Parts
{
    public partial class AddKnittingSpareSuppliers : System.Web.UI.Page
    {

        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //if (Request.QueryString["unitId"] != null && Request.QueryString["knittingDate"] != null)
                //{
                //    UnitId = int.Parse(Tools.UrlDecode(Request.QueryString["unitId"]));
                //    KnittingDate = DateTime.Parse(Tools.UrlDecode(Request.QueryString["knittingDate"]));
                //    ShiftId = int.Parse(Tools.UrlDecode(Request.QueryString["shiftId"]));
                //    lblProductionEntry.Text = "Update Daily Knitting Machine Status:";
                //}

                PopulateEntryInfo();
            }

        }

        private void PopulateEntryInfo()
        {

            SetInitialRowCount();

        }


        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("SupplierName", typeof(string)));
            dt.Columns.Add(new DataColumn("SupplierAddress", typeof(string)));
            dt.Columns.Add(new DataColumn("SupplierPhone", typeof(string)));
            dt.Columns.Add(new DataColumn("SupplierEmail", typeof(string)));
            dt.Columns.Add(new DataColumn("SupplierAdditionalInfo", typeof(string)));


            dr = dt.NewRow();
            dr["SupplierName"] = string.Empty;
            dr["SupplierAddress"] = string.Empty;
            dr["SupplierPhone"] = string.Empty;
            dr["SupplierEmail"] = string.Empty;
            dr["SupplierAdditionalInfo"] = string.Empty;
            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptKnittingMachineSuppliersEntryInfo.DataSource = dt;
            rptKnittingMachineSuppliersEntryInfo.DataBind();
        }



        protected void rptKnittingMachineSuppliersEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{

            //    var ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
            //    LoadMachineBrands(ddlMachineBrands);

            //    var ddlKnittingUnits = (DropDownList)e.Item.FindControl("ddlKnittingUnits");
            //    LoadKnittigUnitDropdown(ddlKnittingUnits);

            //}

        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            this.AddNewRowToGrid();
        }

        private void AddNewRowToGrid()
        {

            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {

                        TextBox tbxKnittingSpareSupplierName = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[rowIndex].FindControl("tbxKnittingSpareSupplierName");
                        TextBox tbxKnittingSapreSupplierAddress = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[rowIndex].FindControl("tbxKnittingSapreSupplierAddress");
                        TextBox tbxSupplierPhone = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[rowIndex].FindControl("tbxSupplierPhone");
                        TextBox tbxSupplierEmail = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[rowIndex].FindControl("tbxSupplierEmail");
                        TextBox tbxKnittingSpareSupplierAdditionalInfo = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[rowIndex].FindControl("tbxKnittingSpareSupplierAdditionalInfo");



                        dtCurrentTable.Rows[rowIndex]["SupplierName"] = tbxKnittingSpareSupplierName.Text;
                        dtCurrentTable.Rows[rowIndex]["SupplierAddress"] = tbxKnittingSapreSupplierAddress.Text;
                        dtCurrentTable.Rows[rowIndex]["SupplierPhone"] = tbxSupplierPhone.Text;
                        dtCurrentTable.Rows[rowIndex]["SupplierEmail"] = tbxSupplierEmail.Text;
                        dtCurrentTable.Rows[rowIndex]["SupplierAdditionalInfo"] = tbxKnittingSpareSupplierAdditionalInfo.Text;


                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["SupplierName"] = string.Empty;
                            drCurrentRow["SupplierAddress"] = string.Empty;
                            drCurrentRow["SupplierPhone"] = string.Empty;
                            drCurrentRow["SupplierEmail"] = string.Empty;
                            drCurrentRow["SupplierAdditionalInfo"] = string.Empty;
                        }
                    }




                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptKnittingMachineSuppliersEntryInfo.DataSource = dtCurrentTable;
                    this.rptKnittingMachineSuppliersEntryInfo.DataBind();
                }
            }
            this.SetPreviousData();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        //TextBox tbxKnittingSpareSupplierName = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[rowIndex].FindControl("tbxKnittingSpareSupplierName");
                        //TextBox tbxKnittingSapreSupplierAddress = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[rowIndex].FindControl("tbxKnittingSapreSupplierAddress");
                        //TextBox tbxKnittingSpareSupplierAdditionalInfo = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[rowIndex].FindControl("tbxKnittingSpareSupplierAdditionalInfo");


                        TextBox tbxKnittingSpareSupplierName = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[rowIndex].FindControl("tbxKnittingSpareSupplierName");
                        TextBox tbxKnittingSapreSupplierAddress = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[rowIndex].FindControl("tbxKnittingSapreSupplierAddress");
                        TextBox tbxSupplierPhone = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[rowIndex].FindControl("tbxSupplierPhone");
                        TextBox tbxSupplierEmail = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[rowIndex].FindControl("tbxSupplierEmail");
                        TextBox tbxKnittingSpareSupplierAdditionalInfo = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[rowIndex].FindControl("tbxKnittingSpareSupplierAdditionalInfo");




                        tbxKnittingSpareSupplierName.Text = dt.Rows[i]["SupplierName"].ToString();
                        tbxKnittingSapreSupplierAddress.Text = dt.Rows[i]["SupplierAddress"].ToString();
                        tbxSupplierPhone.Text = dt.Rows[i]["SupplierPhone"].ToString();
                        tbxSupplierEmail.Text = dt.Rows[i]["SupplierEmail"].ToString();
                        tbxKnittingSpareSupplierAdditionalInfo.Text = dt.Rows[i]["SupplierAdditionalInfo"].ToString();

                        rowIndex++;
                    }
                }
            }
        }



        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {

                for (int i = 0; i < rptKnittingMachineSuppliersEntryInfo.Items.Count; i++)
                {

                    //TextBox tbxKnittingSpareSupplierName = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[i].FindControl("tbxKnittingSpareSupplierName");
                    //TextBox tbxKnittingSapreSupplierAddress = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[i].FindControl("tbxKnittingSapreSupplierAddress");
                    //TextBox tbxKnittingSpareSupplierAdditionalInfo = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[i].FindControl("tbxKnittingSpareSupplierAdditionalInfo");

                    TextBox tbxKnittingSpareSupplierName = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[i].FindControl("tbxKnittingSpareSupplierName");
                    TextBox tbxKnittingSapreSupplierAddress = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[i].FindControl("tbxKnittingSapreSupplierAddress");
                    TextBox tbxSupplierPhone = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[i].FindControl("tbxSupplierPhone");
                    TextBox tbxSupplierEmail = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[i].FindControl("tbxSupplierEmail");
                    TextBox tbxKnittingSpareSupplierAdditionalInfo = (TextBox)this.rptKnittingMachineSuppliersEntryInfo.Items[i].FindControl("tbxKnittingSpareSupplierAdditionalInfo");





                    if (tbxKnittingSpareSupplierName.Text != "")
                    {
                        var knittingSpareSupplier = new KnittingSpareSuppliers()
                        {
                            SupplierName = tbxKnittingSpareSupplierName.Text.ToString(),
                            SupplierAddress = tbxKnittingSapreSupplierAddress.Text.ToString(),
                            SupplierPhone = tbxSupplierPhone.Text.ToString(),
                            SupplierEmail = tbxSupplierEmail.Text.ToString(),
                            SupplierAdditionalInfo = tbxKnittingSpareSupplierAdditionalInfo.Text.ToString(),
                            IsActive = 1,
                            CreatedBy = CommonMethods.SessionInfo.UserName,
                            CreateDate = DateTime.Now
                        };
                        unitOfWork.GenericRepositories<KnittingSpareSuppliers>().Insert(knittingSpareSupplier);
                    }
                    //else
                    //{
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Data was not saved for empty supplie')", true);
                    //    break;
                    //}
                }

                unitOfWork.Save();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "')", true);
            }
        }


        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    var unitId = int.Parse(((DropDownList)rptGeneralInfo.Items[0].FindControl("ddlUserUnits")).SelectedValue);
            //    var stShift = ((DropDownList)rptGeneralInfo.Items[0].FindControl("ddlShift")).SelectedValue;

            //    var stproductionDate = ((TextBox)rptGeneralInfo.Items[0].FindControl("tbxProductionDate")).Text;
            //    if (!string.IsNullOrEmpty(stShift))
            //    {
            //        var shiftId = int.Parse(stShift);
            //        if (!string.IsNullOrEmpty(stproductionDate))
            //        {
            //            var prductionDate = DateTime.Parse(stproductionDate);
            //            if (prductionDate <= DateTime.Today)
            //            {
            //                var flag = true;
            //                for (int i = 0; i < rptEntryInfo.Items.Count; i++)
            //                {
            //                    var lblDailyKnittingMachineStatusId = (Label)rptEntryInfo.Items[i].FindControl("lblDailyKnittingMachineStatusId");
            //                    var lblMachineBrandId = (Label)rptEntryInfo.Items[i].FindControl("lblMachineBrandId");
            //                    var tbxNoOfRunningMachine = (TextBox)rptEntryInfo.Items[i].FindControl("tbxNoOfRunningMachine");
            //                    var tbxUsedForSample = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForSample");
            //                    var tbxLongTimeStoped = (TextBox)rptEntryInfo.Items[i].FindControl("tbxLongTimeStoped");
            //                    var tbxTemporaryStoped = (TextBox)rptEntryInfo.Items[i].FindControl("tbxTemporaryStoped");
            //                    var tbxIdleMachine = (TextBox)rptEntryInfo.Items[i].FindControl("tbxIdleMachine");
            //                    var tbxUsedForSetup = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForSetup");

            //                    var tbxUsedForOtherWorks = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForOtherWorks");
            //                    var lblNumberOfMachines = (Label)rptEntryInfo.Items[i].FindControl("lblNumberOfMachines");
            //                    var noOfMc = int.Parse(lblNumberOfMachines.Text);

            //                    var DailyKnittingMachineStatusId = string.IsNullOrEmpty(lblDailyKnittingMachineStatusId.Text) ? 0 : int.Parse(lblDailyKnittingMachineStatusId.Text);
            //                    var machineBrandId = string.IsNullOrEmpty(lblMachineBrandId.Text) ? 0 : int.Parse(lblMachineBrandId.Text);
            //                    var runningMachine = string.IsNullOrEmpty(tbxNoOfRunningMachine.Text) ? (int?)null : int.Parse(tbxNoOfRunningMachine.Text);
            //                    var forSample = string.IsNullOrEmpty(tbxUsedForSample.Text) ? (int?)null : int.Parse(tbxUsedForSample.Text);
            //                    var longTimeStop = string.IsNullOrEmpty(tbxLongTimeStoped.Text) ? (int?)null : int.Parse(tbxLongTimeStoped.Text);
            //                    var tempStop = string.IsNullOrEmpty(tbxTemporaryStoped.Text) ? (int?)null : int.Parse(tbxTemporaryStoped.Text);
            //                    var idleMachine = string.IsNullOrEmpty(tbxIdleMachine.Text) ? (int?)null : int.Parse(tbxIdleMachine.Text);
            //                    var usedForSetup = string.IsNullOrEmpty(tbxUsedForSetup.Text) ? (int?)null : int.Parse(tbxUsedForSetup.Text);
            //                    var otherWorks = string.IsNullOrEmpty(tbxUsedForOtherWorks.Text) ? (int?)null : int.Parse(tbxUsedForOtherWorks.Text);
            //                    int totalUsed = ((runningMachine ?? 0) + (forSample ?? 0) + (longTimeStop ?? 0) + (tempStop ?? 0) + (idleMachine ?? 0) + (usedForSetup ?? 0) + (otherWorks ?? 0));
            //                    if (noOfMc != totalUsed)
            //                    {
            //                        flag = false;
            //                    }
            //                    else
            //                    {

            //                        var info = unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().GetByID(DailyKnittingMachineStatusId);
            //                        if (info == null)
            //                        {


            //                            var dailyMachineStatus = new DailyKnittingMachineStatus()
            //                            {
            //                                ProductionDate = prductionDate,
            //                                KnittingUnitId = unitId,
            //                                ShiftId = shiftId,
            //                                MachineBrandId = machineBrandId,
            //                                NoOfRunningMachine = runningMachine,
            //                                UsedForSample = forSample,
            //                                LongTimeStop = longTimeStop,
            //                                TemporaryStop = tempStop,
            //                                IdleMachine = idleMachine,
            //                                //UsedForSetup = usedForSetup,
            //                                //UsedForOtherWorks = otherWorks,
            //                                CreatedBy = CommonMethods.SessionInfo.UserName,
            //                                CreateDate = DateTime.Now
            //                            };
            //                            unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().Insert(dailyMachineStatus);
            //                        }
            //                        else
            //                        {
            //                            info.MachineBrandId = machineBrandId;
            //                            info.NoOfRunningMachine = runningMachine;
            //                            info.UsedForSample = forSample;
            //                            info.LongTimeStop = longTimeStop;
            //                            info.TemporaryStop = tempStop;
            //                            info.IdleMachine = idleMachine;
            //                            //info.UsedForSetup = usedForSetup;
            //                            //info.UsedForOtherWorks = otherWorks;
            //                            info.UpdatedBy = CommonMethods.SessionInfo.UserName;
            //                            info.UpdateDate = DateTime.Now;
            //                            unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().Update(info);
            //                        }
            //                    }
            //                }

            //                if (flag)
            //                {
            //                    unitOfWork.Save();
            //                    unitOfWork.ExeccuteRawQyery($"EXEC usp_Insert_DailyMachineStatusSummary '{prductionDate.Date}',{unitId}");
            //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
            //                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewKnittingMachineStatus.aspx');", true);
            //                }
            //                else
            //                {
            //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Total no of machine and entered machine should be equal!')", true);
            //                }
            //            }
            //            else
            //            {
            //                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('You cant enter future date data!')", true);
            //            }
            //        }
            //        else
            //        {
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter production date!')", true);
            //        }
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select shift !')", true);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "')", true);
            //}

        }


    }
}