﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Knitting_Parts;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Knitting_Spares
{
    public partial class ViewVerifiedInfo : System.Web.UI.Page
    {
        PartsManager partManager = new PartsManager();
        UnitOfWork unitOfWork = new UnitOfWork();


        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
            }

        }

        protected void ddlKnittingUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlKnittingUnits.SelectedValue != "")
            {
                LoadMachineBrandsByUnit(ddlMachineBrands, int.Parse(ddlKnittingUnits.SelectedValue));
            }
            else
            {
                ddlMachineBrands.Items.Clear();
                pnlViewKnittingSpareRequests.Visible = false;
            }
        }


        private void LoadMachineBrandsByUnit(DropDownList ddl, int unitId)
        {
            string sql = $"Exec usp_GetAllMachineBrandByUnitId {unitId}";
            var dt = unitOfWork.GetDataTableFromSql(sql);
            DataRow dr = dt.NewRow();
            dr["MachineBrandId"] = "";
            dr["MachineBrand"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);

            ddl.DataTextField = "MachineBrand";
            ddl.DataValueField = "MachineBrandId";
            ddl.DataSource = dt;
            ddl.DataBind();
        }

        protected void btnViewVerifiedSpareRequests_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();


            int knittingUnitId = 0;
            int machineBrandId = 0;
            DateTime fromDate = DateTime.Now.AddDays(-30);

            if (ddlKnittingUnits.SelectedValue != "")
            {

                if (ddlMachineBrands.SelectedValue != "")
                {
                    machineBrandId = int.Parse(ddlMachineBrands.SelectedValue);
                }

                if (tbxFromDate.Text != "")
                {
                    fromDate = DateTime.Parse(tbxFromDate.Text);
                }

                knittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue);

                dt = partManager.GetVerifiedSpareRequests(knittingUnitId, machineBrandId, fromDate);

                if (dt.Rows.Count > 0)
                {
                    rptKnittingSpareRequests.DataSource = dt;
                    rptKnittingSpareRequests.DataBind();
                    lblNoRequestFound.Visible = false;
                    pnlViewKnittingSpareRequests.Visible = true;
                }
                else
                {
                    lblNoRequestFound.Visible = true;
                    pnlViewKnittingSpareRequests.Visible = false;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Select a knitting unit.')", true);
            }
        }


        protected void rptKnittingSpareRequests_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {


                TextBox tbxVerifiedQuantity = (TextBox)e.Item.FindControl("tbxVerifiedQuantity");
                TextBox tbxVerificationNote = (TextBox)e.Item.FindControl("tbxVerificationNote");


                tbxVerifiedQuantity.Visible = true;
                tbxVerificationNote.Visible = true;

                Label lblVerifiedQty = (Label)e.Item.FindControl("lblVerifiedQty");
                Label lblVerificationNote = (Label)e.Item.FindControl("lblVerificationNote");

                lblVerifiedQty.Visible = false;
                lblVerificationNote.Visible = false;


                e.Item.FindControl("lnkbtnEdit").Visible = false;
                e.Item.FindControl("lnkbtnUpdate").Visible = true;
                e.Item.FindControl("lnkbtnCancel").Visible = true;

            }
            else if (e.CommandName == "Update")
            {
                try
                {

                    TextBox tbxVerifiedQuantity = (TextBox)e.Item.FindControl("tbxVerifiedQuantity");
                    TextBox tbxVerificationNote = (TextBox)e.Item.FindControl("tbxVerificationNote");




                    if (tbxVerifiedQuantity.Text.ToString() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Enter verified quantity.')", true);
                    }
                    else
                    {
                        int spareRequestVerifiedId = Convert.ToInt32(e.CommandArgument.ToString());
                        var spareRequestDetail = unitOfWork.GenericRepositories<KnittingSpareRequestsVerified>().GetByID(spareRequestVerifiedId);
                        spareRequestDetail.Id = spareRequestVerifiedId;
                        spareRequestDetail.VerifiedQuantity = int.Parse(tbxVerifiedQuantity.Text);
                        spareRequestDetail.Notes = tbxVerificationNote.Text.ToString();
                        spareRequestDetail.UpdatedBy = CommonMethods.SessionInfo.UserName;
                        spareRequestDetail.UpdateDate = DateTime.Now;

                        unitOfWork.GenericRepositories<KnittingSpareRequestsVerified>().Update(spareRequestDetail);
                        unitOfWork.Save();

                        tbxVerifiedQuantity.Visible = false;
                        tbxVerificationNote.Visible = false;

                        Label lblVerifiedQty = (Label)e.Item.FindControl("lblVerifiedQty");
                        Label lblVerificationNote = (Label)e.Item.FindControl("lblVerificationNote");


                        lblVerifiedQty.Text = tbxVerifiedQuantity.Text;
                        lblVerificationNote.Text = tbxVerificationNote.Text;


                        lblVerifiedQty.Visible = true;
                        lblVerificationNote.Visible = true;

                        e.Item.FindControl("lnkbtnEdit").Visible = true;
                        e.Item.FindControl("lnkbtnUpdate").Visible = false;
                        e.Item.FindControl("lnkbtnCancel").Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
            else if (e.CommandName == "Cancel")
            {
                TextBox tbxVerifiedQuantity = (TextBox)e.Item.FindControl("tbxVerifiedQuantity");
                TextBox tbxVerificationNote = (TextBox)e.Item.FindControl("tbxVerificationNote");


                tbxVerifiedQuantity.Visible = false;
                tbxVerificationNote.Visible = false;

                Label lblVerifiedQty = (Label)e.Item.FindControl("lblVerifiedQty");
                Label lblVerificationNote = (Label)e.Item.FindControl("lblVerificationNote");

                lblVerifiedQty.Visible = true;
                lblVerificationNote.Visible = true;


                e.Item.FindControl("lnkbtnEdit").Visible = true;
                e.Item.FindControl("lnkbtnUpdate").Visible = false;
                e.Item.FindControl("lnkbtnCancel").Visible = false;
            }
        }

    }
}