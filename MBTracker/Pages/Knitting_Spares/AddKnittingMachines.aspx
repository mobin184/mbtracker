﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddKnittingMachines.aspx.cs" Inherits="MBTracker.Pages.Knitting_Parts.AddKnittingMachines" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid">
        <div class="span9">

            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblProductionEntry" Text="Add Knitting Machines:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="row-fluid" runat="server">

                        <div style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 15px">
                            <div class="pull-right" style="padding-bottom: 10px">
                                <span style="font-weight: 700;"></span><span style="font-weight: 700; color: #CC0000">*</span>Row will not be saved if all the fields don't have values.
                            </div>
                        </div>

                        <div id="dt_example" class="example_alt_pagination">

                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblKnittingEntry" runat="server" Text="Enter the information below." Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                    <asp:Repeater ID="rptKnittingMachinesEntryInfo" runat="server" OnItemDataBound="rptKnittingMachinesEntryInfo_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>

                                                        <th>
                                                            <asp:Label ID="lblMachineBrand" runat="server" Text="Machine Brand"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblKnittingUnit" runat="server" Text="Knitting Unit"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblMachineCode" runat="server" Text="Machine Code"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblMachineName" runat="server" Text="Machine Name"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblNeedleCapacity" runat="server" Text="Needle Capacity"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="ddlMachineBrands" runat="server" CssClass="form-control" Style="min-width: 125px"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlKnittingUnits" runat="server" CssClass="form-control" Style="min-width: 125px"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="tbxMachineCode" Width="75%" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="tbxMachineName" Width="100%" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="tbxNeedleCapacity" Width="100%" runat="server" TextMode="Number"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                 </table>
                                            <div class="pull-right" style="margin-right: 0px">
                                                <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="81px" Text="Add Row" OnClick="btnAddRow_Click" />
                                            </div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>

            </div>

            <br />
            <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
            <asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>
        </div>
    </div>

</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
