﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewKnittingSpares.aspx.cs" Inherits="MBTracker.Pages.Knitting_Parts.ViewKnittingSpares" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid" id="divBuyerAndStyle" runat="server">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="View Knitting Spares:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="form-horizontal">

                        <div style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 15px">
                            <div class="pull-right" style="padding-bottom: 10px">
                                <span style="font-weight: 700;"></span><span style="font-weight: 700; color: #CC0000">*</span>Selections are optional.
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblSpareTypes" runat="server" Text="Select Spare Types:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlSpareTypes" runat="server" Display="Dynamic" Width="60%" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                          <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblMachineBrand1" runat="server" Text="Select Machine Brand:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlMachineBrands" runat="server" Display="Dynamic" Width="60%" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="&nbsp;"></asp:Label></label>
                            <div class="controls controls-row">
                                <br />
                                <asp:Button ID="btnViewKnittingParts" runat="server" class="btn btn-success btn-midium pull-left btnStyle" Text="View Knitting Spares" OnClick="btnViewKnittingParts_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row-fluid">
        <div class="span9">
            <asp:Label ID="lblNoPartFound" Style="width: 100%" runat="server" Visible="false" Text="Knitting spare was not found." BackColor="#ffff00"></asp:Label>
            <asp:Panel ID="pnlViewKnittingParts" runat="server" Visible="false">

                <div class="widget">
                    <div class="widget-body">
                        <div class="form-horizontal">
                            <div class="control-group" style="overflow-x: auto">
                                <label for="inputStyle" class="control-label" style="font-weight: bold; text-align: left">
                                    <asp:Label ID="Label1" runat="server" Style="font-weight: bold; text-align: left" Font-Bold="false" Text=""></asp:Label></label>
                                <div class="controls-row">
                                    <asp:Repeater ID="rptKnittingParts" runat="server" OnItemCommand="rptKnittingParts_ItemCommand">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        
                                                        <th>
                                                            <asp:Label ID="lblPartsType" runat="server" Text="Spare Types"></asp:Label></th>
                                                         <th>
                                                            <asp:Label ID="lblMachineBrand" runat="server" Text="Machine Brands"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblPartsName" runat="server" Text="Spare Names"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblItemCode" runat="server" Text="Spare Codes"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblVariability" runat="server" Text="Fixed or Multi"></asp:Label></th>
                                                        
                                                        <%-- <th>
                                                            <asp:Label ID="lblPartCode" runat="server" Text="Internal Codes"></asp:Label></th>--%>
                                                        <th>
                                                            <asp:Label ID="LabelAction" runat="server" Text="Action"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                
                                                 <td style="vertical-align: middle; text-align: left; width: 15%">
                                                    <asp:Label ID="lblSpareTypeId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "SpareTypeId")%>'></asp:Label>
                                                    <asp:Label ID="lblPartTypeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PartTypeName")%>' Width="100%" ></asp:Label>
                                                    <asp:DropDownList ID="ddlKnittingPartTypes" runat="server" CssClass="form-control" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlKnittingPartTypes_SelectedIndexChanged" Visible="false"></asp:DropDownList>
                                                </td>
                                                
                                                <td style="vertical-align: middle; text-align: center; width:15%">
                                                     <asp:Label ID="lblMachineBrandId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "MachineBrandId")%>'></asp:Label>
                                                    <asp:Label ID="lblMachineBrandValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MachineBrandName")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                    <asp:DropDownList ID="ddlMachineBrands" runat="server" CssClass="form-control" Visible="false"></asp:DropDownList>
                                                </td>
                                                
                                                <td style="vertical-align: middle; text-align: center; width: 20%">
                                                    <asp:Label ID="lblPartsNameValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SpareName")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                    <asp:TextBox ID="tbxPartName" Width="100%" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SpareName")%>' Visible="false"></asp:TextBox>
                                                </td>
                                                <td style="vertical-align: middle; text-align: center; width: 15%">
                                                    <asp:Label ID="lblIntCode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SpareCode")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                    <asp:TextBox ID="tbxIntItemCode" Width="100%" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "SpareCode")%>'></asp:TextBox>
                                                </td>

                                                <td style="vertical-align: middle; text-align: center; width: 15%">
                                                    <asp:Label ID="lblFixedOrMultiId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "FixedOrMultiId")%>'></asp:Label>

                                                    <asp:Label ID="lblFixedOrMulti" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "VariabilityType")%>' Width="100%" ></asp:Label>
                                                    <asp:DropDownList ID="ddlFixedOrMulti" runat="server" CssClass="form-control" Visible="false">
                                                        <asp:ListItem Text="--Select--" Value="0" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="Fixed" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Multi" Value="2"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>

                                               
                                                
                                                <%-- <td>
                                                    <asp:Label ID="lblPartCodeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PartCode")%>' Width="100%" CssClass="text-left"></asp:Label></td>--%>


                                                <td style="vertical-align: middle; text-align: center; width: 25%">
                                                    <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="&nbsp;&nbsp;Edit&nbsp;&nbsp;"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnUpdate" runat="server" Visible="false" CommandName="Update" CommandArgument='<%#Eval("Id") %>' class="btn btn-warning2 btn-mini" Text="Update"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnCancel" runat="server" Visible="false" CommandName="Cancel" CommandArgument='<%#Eval("Id") %>' class="btn btn-warning2 btn-mini" Text="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;&nbsp;"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </asp:Panel>


        </div>
    </div>



</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
