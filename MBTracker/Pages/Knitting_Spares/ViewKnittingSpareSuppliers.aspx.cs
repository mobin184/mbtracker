﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Knitting_Parts;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Knitting_Parts
{
    public partial class ViewKnittingSpareSuppliers : System.Web.UI.Page
    {

        PartsManager partManager = new PartsManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                BindData();
            }

        }


        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            int knittingMachineId = int.Parse(e.CommandArgument.ToString());
            Response.Redirect("AddKnittingMachines.aspx?knittingMachineId=" + Tools.UrlEncode(knittingMachineId + ""));
        }

        private void BindData()
        {

            DataTable dt = new DataTable();

            dt = partManager.GetKnittingMachineSpareSuppliers();

            if (dt.Rows.Count > 0)
            {
                rptKnittingSpareSuppliers.DataSource = dt;
                rptKnittingSpareSuppliers.DataBind();
                lblNoSpareSupplierFound.Visible = false;
                pnlViewSpareSuppliers.Visible = true;
            }
            else
            {
                lblNoSpareSupplierFound.Visible = true;
                pnlViewSpareSuppliers.Visible = false;
            }

        }

        protected void rptKnittingSpareSuppliers_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {

                TextBox tbxKnittingSpareSupplierName = (TextBox)e.Item.FindControl("tbxKnittingSpareSupplierName");
                TextBox tbxKnittingSapreSupplierAddress = (TextBox)e.Item.FindControl("tbxKnittingSapreSupplierAddress");
                TextBox tbxPhoneValue = (TextBox)e.Item.FindControl("tbxPhoneValue");
                TextBox tbxEmailValue = (TextBox)e.Item.FindControl("tbxEmailValue");
                TextBox tbxKnittingSpareSupplierAdditionalInfo = (TextBox)e.Item.FindControl("tbxKnittingSpareSupplierAdditionalInfo");

                tbxKnittingSpareSupplierName.Visible = true;
                tbxKnittingSapreSupplierAddress.Visible = true;
                tbxPhoneValue.Visible = true;
                tbxEmailValue.Visible = true;
                tbxKnittingSpareSupplierAdditionalInfo.Visible = true;


                Label lblKnittingSpareSupplierNameValue = (Label)e.Item.FindControl("lblKnittingSpareSupplierNameValue");
                Label lblKnittingSapreSupplierAddressValue = (Label)e.Item.FindControl("lblKnittingSapreSupplierAddressValue");

                Label lblPhoneValue = (Label)e.Item.FindControl("lblPhoneValue");
                Label lblEmailValue = (Label)e.Item.FindControl("lblEmailValue");

                Label lblKnittingSpareSupplierAdditionalInfoValue = (Label)e.Item.FindControl("lblKnittingSpareSupplierAdditionalInfoValue");

                lblKnittingSpareSupplierNameValue.Visible = false;
                lblKnittingSapreSupplierAddressValue.Visible = false;
                lblPhoneValue.Visible = false;
                lblEmailValue.Visible = false;
                lblKnittingSpareSupplierAdditionalInfoValue.Visible = false;


                e.Item.FindControl("lnkbtnEdit").Visible = false;
                e.Item.FindControl("lnkbtnUpdate").Visible = true;
                e.Item.FindControl("lnkbtnCancel").Visible = true;

            }
            else if (e.CommandName == "Update")
            {

                try
                {
                    TextBox tbxKnittingSpareSupplierName = (TextBox)e.Item.FindControl("tbxKnittingSpareSupplierName");
                    TextBox tbxKnittingSapreSupplierAddress = (TextBox)e.Item.FindControl("tbxKnittingSapreSupplierAddress");
                    TextBox tbxPhoneValue = (TextBox)e.Item.FindControl("tbxPhoneValue");
                    TextBox tbxEmailValue = (TextBox)e.Item.FindControl("tbxEmailValue");
                    TextBox tbxKnittingSpareSupplierAdditionalInfo = (TextBox)e.Item.FindControl("tbxKnittingSpareSupplierAdditionalInfo");

                    if (tbxKnittingSpareSupplierName.Text == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Supplier name cannot be empty.')", true);
                    }
                    else
                    {
                        int spareSupplierId = Convert.ToInt32(e.CommandArgument.ToString());
                        var spareSupplier = unitOfWork.GenericRepositories<KnittingSpareSuppliers>().GetByID(spareSupplierId);

                        spareSupplier.SupplierName  = string.IsNullOrEmpty(tbxKnittingSpareSupplierName.Text) ? "" : tbxKnittingSpareSupplierName.Text.ToString();
                        spareSupplier.SupplierAddress = string.IsNullOrEmpty(tbxKnittingSapreSupplierAddress.Text) ? "" : tbxKnittingSapreSupplierAddress.Text.ToString();
                        spareSupplier.SupplierPhone = string.IsNullOrEmpty(tbxPhoneValue.Text) ? "" : tbxPhoneValue.Text.ToString();
                        spareSupplier.SupplierEmail = string.IsNullOrEmpty(tbxEmailValue.Text) ? "" : tbxEmailValue.Text.ToString();
                        spareSupplier.SupplierAdditionalInfo = string.IsNullOrEmpty(tbxKnittingSpareSupplierAdditionalInfo.Text) ? "" : tbxKnittingSpareSupplierAdditionalInfo.Text.ToString();


                        spareSupplier.UpdatedBy = CommonMethods.SessionInfo.UserName;
                        spareSupplier.UpdateDate = DateTime.Now;

                        unitOfWork.GenericRepositories<KnittingSpareSuppliers>().Update(spareSupplier);
                        unitOfWork.Save();

                        tbxKnittingSpareSupplierName.Visible = false;
                        tbxKnittingSapreSupplierAddress.Visible = false;
                        tbxPhoneValue.Visible = false;
                        tbxEmailValue.Visible = false;
                        tbxKnittingSpareSupplierAdditionalInfo.Visible = false;


                        Label lblKnittingSpareSupplierNameValue = (Label)e.Item.FindControl("lblKnittingSpareSupplierNameValue");
                        Label lblKnittingSapreSupplierAddressValue = (Label)e.Item.FindControl("lblKnittingSapreSupplierAddressValue");
                        Label lblPhoneValue = (Label)e.Item.FindControl("lblPhoneValue");
                        Label lblEmailValue = (Label)e.Item.FindControl("lblEmailValue");
                        Label lblKnittingSpareSupplierAdditionalInfoValue = (Label)e.Item.FindControl("lblKnittingSpareSupplierAdditionalInfoValue");

                        lblKnittingSpareSupplierNameValue.Visible = true;
                        lblKnittingSapreSupplierAddressValue.Visible = true;
                        lblPhoneValue.Visible = true;
                        lblEmailValue.Visible = true;
                        lblKnittingSpareSupplierAdditionalInfoValue.Visible = true;

                        lblKnittingSpareSupplierNameValue.Text = tbxKnittingSpareSupplierName.Text;
                        lblKnittingSapreSupplierAddressValue.Text = tbxKnittingSapreSupplierAddress.Text;
                        lblPhoneValue.Text = tbxPhoneValue.Text;
                        lblEmailValue.Text = tbxEmailValue.Text;
                        lblKnittingSpareSupplierAdditionalInfoValue.Text = tbxKnittingSpareSupplierAdditionalInfo.Text;


                        e.Item.FindControl("lnkbtnEdit").Visible = true;
                        e.Item.FindControl("lnkbtnUpdate").Visible = false;
                        e.Item.FindControl("lnkbtnCancel").Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
            else if (e.CommandName == "Cancel")
            {

                TextBox tbxKnittingSpareSupplierName = (TextBox)e.Item.FindControl("tbxKnittingSpareSupplierName");
                TextBox tbxKnittingSapreSupplierAddress = (TextBox)e.Item.FindControl("tbxKnittingSapreSupplierAddress");
                TextBox tbxPhoneValue = (TextBox)e.Item.FindControl("tbxPhoneValue");
                TextBox tbxEmailValue = (TextBox)e.Item.FindControl("tbxEmailValue");
                TextBox tbxKnittingSpareSupplierAdditionalInfo = (TextBox)e.Item.FindControl("tbxKnittingSpareSupplierAdditionalInfo");



                tbxKnittingSpareSupplierName.Visible = false;
                tbxKnittingSapreSupplierAddress.Visible = false;
                tbxPhoneValue.Visible = false;
                tbxEmailValue.Visible = false;
                tbxKnittingSpareSupplierAdditionalInfo.Visible = false;


                Label lblKnittingSpareSupplierNameValue = (Label)e.Item.FindControl("lblKnittingSpareSupplierNameValue");
                Label lblKnittingSapreSupplierAddressValue = (Label)e.Item.FindControl("lblKnittingSapreSupplierAddressValue");
                Label lblPhoneValue = (Label)e.Item.FindControl("lblPhoneValue");
                Label lblEmailValue = (Label)e.Item.FindControl("lblEmailValue");
                Label lblKnittingSpareSupplierAdditionalInfoValue = (Label)e.Item.FindControl("lblKnittingSpareSupplierAdditionalInfoValue");

                lblKnittingSpareSupplierNameValue.Visible = true;
                lblKnittingSapreSupplierAddressValue.Visible = true;
                lblPhoneValue.Visible = true;
                lblEmailValue.Visible = true;
                lblKnittingSpareSupplierAdditionalInfoValue.Visible = true;


                e.Item.FindControl("lnkbtnEdit").Visible = true;
                e.Item.FindControl("lnkbtnUpdate").Visible = false;
                e.Item.FindControl("lnkbtnCancel").Visible = false;
            }
        }


    }
}