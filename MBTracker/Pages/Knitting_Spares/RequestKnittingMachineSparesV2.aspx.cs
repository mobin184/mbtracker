﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Knitting_Parts;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Knitting_Spares
{
    public partial class RequestKnittingMachineSparesV2 : System.Web.UI.Page
    {

        PartsManager partManager = new PartsManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        //DataTable dtCurrentTable = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
                //SetInitialRowCount();
            }
        }

        protected void ddlKnittingUnits_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlMachineBrands.Items.Clear();
            ddlMachineNames.Items.Clear();
            cblSpares.Items.Clear();
            lblListOfSpareMessage.Visible = false;
            btnAddRow.Visible = false;

            if (ddlKnittingUnits.SelectedValue != "")
            {
                LoadMachineBrandsByUnit(ddlMachineBrands, int.Parse(ddlKnittingUnits.SelectedValue));
            }
            else
            {
                //ddlMachineBrands.Items.Clear();
                //ddlMachineNames.Items.Clear();
                divKnittingSpareSelection.Visible = false;
            }
        }

        private void LoadMachineBrandsByUnit(DropDownList ddl, int unitId)
        {
            string sql = $"Exec usp_GetAllMachineBrandByUnitId {unitId}";
            var dt = unitOfWork.GetDataTableFromSql(sql);
            DataRow dr = dt.NewRow();
            dr["MachineBrandId"] = "";
            dr["MachineBrand"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);

            ddl.DataTextField = "MachineBrand";
            ddl.DataValueField = "MachineBrandId";
            ddl.DataSource = dt;
            ddl.DataBind();
        }

        protected void ddlMachineBrands_SelectedIndexChanged(object sender, EventArgs e)
        {

            cblSpares.Items.Clear();
            ddlMachineNames.Items.Clear();
            btnAddRow.Visible = false;

            if (ddlMachineBrands.SelectedValue != "" && ddlKnittingUnits.SelectedValue != "")
            {
                //ddlMachineNames.Items.Clear();
                LoadMachinesByBrandAndUnit(ddlMachineNames, int.Parse(ddlMachineBrands.SelectedValue), int.Parse(ddlKnittingUnits.SelectedValue));

                //cblSpares.Items.Clear();
                lblListOfSpareMessage.Visible = true;

                LoadSpares(cblSpares, int.Parse(ddlMachineBrands.SelectedValue));
            }
            else
            {
                //cblSpares.Items.Clear();
                //ddlMachineNames.Items.Clear();
                lblListOfSpareMessage.Visible = false;
                divKnittingSpareSelection.Visible = false;
            }
        }


        private void LoadSpares(CheckBoxList cbl, int machineBrandId)
        {
            CommonMethods.LoadSpareCheckBoxList(cbl, machineBrandId, lblListOfSpareMessage, btnAddRow);
        }


        private void LoadMachinesByBrandAndUnit(DropDownList ddl, int machineBrandId, int knittingUnitId)
        {
            CommonMethods.LoadKnittingMachinesDropdown(ddl, 3, 0, machineBrandId, knittingUnitId);
        }


        private void SetRowsToRepeater()
        {
            DataTable dt = new DataTable();


            dt.Columns.Add(new DataColumn("MachineBrandId", typeof(string)));
            dt.Columns.Add(new DataColumn("MachineId", typeof(string)));
            dt.Columns.Add(new DataColumn("SpareTypeId", typeof(string)));
            dt.Columns.Add(new DataColumn("SpareId", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("RequestReasonId", typeof(string)));
            dt.Columns.Add(new DataColumn("Remarks", typeof(string)));



            for (int i = 0; i < cblSpares.Items.Count; i++)
            {
                if (cblSpares.Items[i].Selected)
                {

                    //int spareTypeId = cblSpares.Items[i];

                    DataRow dr = null;
                    dr = dt.NewRow();

                    dr["MachineBrandId"] = ddlMachineBrands.SelectedValue.ToString();
                    dr["MachineId"] = ddlMachineNames.SelectedValue.ToString();
                    dr["SpareTypeId"] = "0";
                    dr["SpareId"] = cblSpares.Items[i].Value.ToString();
                    dr["Quantity"] = string.Empty;
                    dr["RequestReasonId"] = "0";
                    dr["Remarks"] = string.Empty;

                    dt.Rows.Add(dr);
                }
            }

            if (dt.Rows.Count > 0)
            {
                divKnittingSpareSelection.Visible = true;
            }
            else
            {
                divKnittingSpareSelection.Visible = false;
            }

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptKnittingSpareRequestEntryInfo.DataSource = dt;
            rptKnittingSpareRequestEntryInfo.DataBind();
        }





        protected void rptKnittingSpareRequestEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                var ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                LoadMachineBrandsByUnit(ddlMachineBrands, int.Parse(ddlKnittingUnits.SelectedValue));
                int machineBrandId = int.Parse(DataBinder.Eval(e.Item.DataItem, "MachineBrandId").ToString());
                ddlMachineBrands.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrands, machineBrandId);
                ddlMachineBrands.Enabled = false;

                var ddlMachineNames = (DropDownList)e.Item.FindControl("ddlMachineNames");
                LoadMachinesByBrandAndUnit(ddlMachineNames, int.Parse(ddlMachineBrands.SelectedValue), int.Parse(ddlKnittingUnits.SelectedValue));
                int machineId = int.Parse(DataBinder.Eval(e.Item.DataItem, "MachineId").ToString());
                ddlMachineNames.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineNames, machineId);
                ddlMachineNames.Enabled = false;

                var ddlSpareNames = (DropDownList)e.Item.FindControl("ddlSpareNames");
                LoadSpareNames(ddlSpareNames, int.Parse(ddlMachineBrands.SelectedValue));
                int SpareId = int.Parse(DataBinder.Eval(e.Item.DataItem, "SpareId").ToString());
                ddlSpareNames.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSpareNames, SpareId);
                ddlSpareNames.Enabled = false;

                var ddlKnittingSpareTypes = (DropDownList)e.Item.FindControl("ddlKnittingSpareTypes");
                LoadSpareTypesDropdown(ddlKnittingSpareTypes);
                int spareTypeId = GetSpareTypeBySpareId(SpareId);
                ddlKnittingSpareTypes.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnittingSpareTypes, spareTypeId);
                ddlKnittingSpareTypes.Enabled = false;

                TextBox tbxQuantityRequested = (TextBox)e.Item.FindControl("tbxQuantityRequested");
                string qty = DataBinder.Eval(e.Item.DataItem, "Quantity").ToString();
                tbxQuantityRequested.Text = qty;



                var ddlRequestReasons = (DropDownList)e.Item.FindControl("ddlRequestReasons");
                LoadSpareChangeReasons(ddlRequestReasons);

                int RequestReasonId = int.Parse(DataBinder.Eval(e.Item.DataItem, "RequestReasonId").ToString());
                ddlRequestReasons.SelectedIndex = CommonMethods.MatchDropDownItem(ddlRequestReasons, RequestReasonId);

                TextBox tbxRemarks = (TextBox)e.Item.FindControl("tbxRemarks");
                string remarks = DataBinder.Eval(e.Item.DataItem, "Remarks").ToString();
                tbxRemarks.Text = remarks;
            }
        }

        private int GetSpareTypeBySpareId(int spareId)
        {
            DataTable dt = partManager.GetSpareTypeBySpareId(spareId);
            if (dt.Rows.Count > 0)
            {
                return int.Parse(dt.Rows[0][0].ToString());
            }
            else
                return 0;
        }

        private void LoadSpareTypesDropdown(DropDownList ddl)
        {
            CommonMethods.LoadPartTypeDropdown(ddl, 1, 0);
        }

        private void LoadSpareNames(DropDownList ddl, int machineBrandId)
        {
            CommonMethods.LoadAllTypesSpareNamesDropdown(ddl, 1, 0, machineBrandId);
        }


        private void LoadSpareChangeReasons(DropDownList ddl)
        {
            CommonMethods.LoadWhyNeedleChangeDropdown(ddl, 1, 0);
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {

            SetRowsToRepeater();

            //if (ddlKnittingUnits.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a knitting unit.')", true);
            //}
            //else if (ddlMachineBrands.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a knitting machine brand.')", true);
            //}
            //else if (ddlMachineNames.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a knitting machine.')", true);
            //}
            //else 
            //{

            //    if (this.ViewState["CurrentTable"] == null)
            //    {

            //        SetRowsToRepeater();

            //    }
            //    else
            //    {

            //        DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];

            //        if (dtCurrentTable.Rows.Count > 0)
            //        {
            //            for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
            //            {

            //                DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
            //                DropDownList ddlMachineNames = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlMachineNames");

            //                DropDownList ddlKnittingSpareTypes = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlKnittingSpareTypes");

            //                DropDownList ddlSpareNames = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlSpareNames");

            //                TextBox tbxQuantityRequested = (TextBox)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("tbxQuantityRequested");
            //                DropDownList ddlRequestReasons = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlRequestReasons");
            //                TextBox tbxRemarks = (TextBox)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("tbxRemarks");



            //                dtCurrentTable.Rows[rowIndex]["MachineBrandId"] = (ddlMachineBrands.SelectedValue == "" ? "0" : ddlMachineBrands.SelectedValue);
            //                dtCurrentTable.Rows[rowIndex]["MachineId"] = (ddlMachineNames.SelectedValue == "" ? "0" : ddlMachineNames.SelectedValue);

            //                dtCurrentTable.Rows[rowIndex]["SpareTypeId"] = (ddlKnittingSpareTypes.SelectedValue == "" ? "0" : ddlKnittingSpareTypes.SelectedValue);

            //                dtCurrentTable.Rows[rowIndex]["SpareId"] = (ddlSpareNames.SelectedValue == "" ? "0" : ddlSpareNames.SelectedValue);
            //                dtCurrentTable.Rows[rowIndex]["Quantity"] = tbxQuantityRequested.Text;

            //                dtCurrentTable.Rows[rowIndex]["RequestReasonId"] = (ddlRequestReasons.SelectedValue == "" ? "0" : ddlRequestReasons.SelectedValue);
            //                dtCurrentTable.Rows[rowIndex]["Remarks"] = tbxRemarks.Text;

            //            }


            //            for (int i = 0; i < cblSpares.Items.Count; i++)
            //            {
            //                if (cblSpares.Items[i].Selected)
            //                {

            //                    if (!CheckAlreadyMachineAndSpare(dtCurrentTable, int.Parse(ddlMachineNames.SelectedValue.ToString()), int.Parse(cblSpares.Items[i].Value.ToString())))
            //                    {
            //                        DataRow dr = null;
            //                        dr = dtCurrentTable.NewRow();

            //                        dr["MachineBrandId"] = ddlMachineBrands.SelectedValue.ToString();
            //                        dr["MachineId"] = ddlMachineNames.SelectedValue.ToString();
            //                        dr["SpareTypeId"] = "0";
            //                        dr["SpareId"] = cblSpares.Items[i].Value.ToString();
            //                        dr["Quantity"] = string.Empty;
            //                        dr["RequestReasonId"] = "0";
            //                        dr["Remarks"] = string.Empty;

            //                        dtCurrentTable.Rows.Add(dr);
            //                    }
            //                }
            //            }

            //            this.ViewState["CurrentTable"] = dtCurrentTable;

            //            this.rptKnittingSpareRequestEntryInfo.DataSource = dtCurrentTable;
            //            this.rptKnittingSpareRequestEntryInfo.DataBind();
            //        }
            //    }
            //    // this.SetPreviousData();
            //}


        }



        private bool CheckAlreadyMachineAndSpare(DataTable dtCurrentTable, int machineId, int spareId)
        {
            bool found = false;

            for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
            {
                if (int.Parse(dtCurrentTable.Rows[rowIndex]["MachineId"].ToString()) == machineId && int.Parse(dtCurrentTable.Rows[rowIndex]["SpareId"].ToString()) == spareId)
                {
                    found = true;
                    break;
                }
            }

            return found;

        }

        //private void SetPreviousData()
        //{
        //    int rowIndex = 0;
        //    if (this.ViewState["CurrentTable"] != null)
        //    {
        //        DataTable dt = (DataTable)this.ViewState["CurrentTable"];
        //        if (dt.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < dt.Rows.Count; i++)
        //            {

        //                DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
        //                DropDownList ddlMachineNames = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlMachineNames");
        //                DropDownList ddlSpareNames = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlSpareNames");

        //                TextBox tbxQuantityRequested = (TextBox)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("tbxQuantityRequested");
        //                DropDownList ddlRequestReasons = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlRequestReasons");
        //                TextBox tbxRemarks = (TextBox)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("tbxRemarks");


        //                LoadMachineBrandsByUnit(ddlMachineBrands, int.Parse(ddlKnittingUnits.SelectedValue));
        //                ddlMachineBrands.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrands, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["MachineBrandId"].ToString()) ? "0" : dt.Rows[i]["MachineBrandId"].ToString())));


        //                if (ddlMachineBrands.SelectedValue != "" && ddlKnittingUnits.SelectedValue != "")
        //                {
        //                    ddlMachineNames.Items.Clear();
        //                    LoadMachinesByBrandAndUnit(ddlMachineNames, int.Parse(ddlMachineBrands.SelectedValue), int.Parse(ddlKnittingUnits.SelectedValue));
        //                    ddlMachineNames.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineNames, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["MachineId"].ToString()) ? "0" : dt.Rows[i]["MachineId"].ToString())));
        //                }
        //                else
        //                {
        //                    ddlMachineNames.Items.Clear();
        //                }

        //                //LoadSpareTypesDropdown(ddlKnittingSpareTypes);
        //                //ddlKnittingSpareTypes.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnittingSpareTypes, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["SpareTypeId"].ToString()) ? "0" : dt.Rows[i]["SpareTypeId"].ToString())));


        //                //if (ddlKnittingSpareTypes.SelectedValue != "")
        //                //{
        //                //    this.LoadSpareNames(ddlSpareNames, int.Parse(ddlKnittingSpareTypes.SelectedValue), int.Parse(ddlMachineBrands.SelectedValue));
        //                //    ddlSpareNames.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSpareNames, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["SpareId"].ToString()) ? "0" : dt.Rows[i]["SpareId"].ToString())));

        //                //}

        //                LoadSpareChangeReasons(ddlRequestReasons);
        //                ddlRequestReasons.SelectedIndex = CommonMethods.MatchDropDownItem(ddlRequestReasons, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["RequestReasonId"].ToString()) ? "0" : dt.Rows[i]["RequestReasonId"].ToString())));

        //                tbxQuantityRequested.Text = dt.Rows[i]["Quantity"].ToString();
        //                tbxRemarks.Text = dt.Rows[i]["Remarks"].ToString();


        //                rowIndex++;
        //            }
        //        }
        //    }
        //}

        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {

            if (ddlKnittingUnits.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a knitting unit.')", true);
            }
            else
            {
                try
                {

                    List<KnittingSpareRequestDetails> listKnittingSpareRequestDetails = new List<KnittingSpareRequestDetails>();

                    var ksr = new KnittingSpareRequests()
                    {
                        KnittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue),
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };

                    unitOfWork.GenericRepositories<KnittingSpareRequests>().Insert(ksr);
                    unitOfWork.Save();

                    for (int rowIndex = 0; rowIndex < rptKnittingSpareRequestEntryInfo.Items.Count; rowIndex++)
                    {

                        DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
                        DropDownList ddlMachineNames = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlMachineNames");
                        DropDownList ddlKnittingSpareTypes = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlKnittingSpareTypes");
                        DropDownList ddlSpareNames = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlSpareNames");

                        TextBox tbxQuantityRequested = (TextBox)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("tbxQuantityRequested");
                        DropDownList ddlRequestReasons = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlRequestReasons");
                        TextBox tbxRemarks = (TextBox)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("tbxRemarks");



                        if (ddlMachineBrands.SelectedIndex != 0 && ddlMachineNames.SelectedIndex != 0 && ddlKnittingSpareTypes.SelectedIndex != 0 && ddlSpareNames.SelectedIndex != 0 && !string.IsNullOrEmpty(tbxQuantityRequested.Text) && ddlRequestReasons.SelectedIndex != 0)
                        {
                            var spareRequestDetails = new KnittingSpareRequestDetails()
                            {
                                KnittingSpareRequestId = ksr.Id,
                                KnittingMachineBrandId = int.Parse(ddlMachineBrands.SelectedValue),
                                KnittingMachineId = int.Parse(ddlMachineNames.SelectedValue),
                                KnittingSpareTypeId = int.Parse(ddlKnittingSpareTypes.SelectedValue),
                                KnittingSpareId = int.Parse(ddlSpareNames.SelectedValue),
                                QuantityRequested = int.Parse(tbxQuantityRequested.Text),
                                ReasonForRequestId = int.Parse(ddlRequestReasons.SelectedValue),
                                Remarks = tbxRemarks.Text.ToString(),
                                Status = 0,

                                CreateDate = DateTime.Now,
                                CreatedBy = CommonMethods.SessionInfo.UserName
                            };
                            listKnittingSpareRequestDetails.Add(spareRequestDetails);
                        }
                    }


                    foreach (var item in listKnittingSpareRequestDetails)
                    {
                        unitOfWork.GenericRepositories<KnittingSpareRequestDetails>().Insert(item);
                    }
                    unitOfWork.Save();

                    CommonMethods.SendSpareRequestNotification(12, ddlKnittingUnits.SelectedItem.Text.ToString(), ddlMachineBrands.SelectedItem.Text.ToString(), ksr.Id);

                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
        }

    }
}