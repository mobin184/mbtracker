﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Knitting_Parts;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Knitting_Parts
{
    public partial class ViewKnittingMachines : System.Web.UI.Page
    {

        PartsManager partManager = new PartsManager();
        UnitOfWork unitOfWork = new UnitOfWork();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadMachineBrandDropdown(ddlMachineBrands, 1, 0);
                CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
            }
        }

        protected void btnViewKnittingMachines_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();

            int machineBrandId = 0;
            int knittingUnitId = 0;


            if (ddlMachineBrands.SelectedValue != "")
            {
                machineBrandId = int.Parse(ddlMachineBrands.SelectedValue);
            }
            if (ddlKnittingUnits.SelectedValue != "")
            {
                knittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue);
            }


            dt = partManager.GetExistingKnittingMachines(machineBrandId, knittingUnitId);

            if (dt.Rows.Count > 0)
            {
                rptKnittingMachines.DataSource = dt;
                rptKnittingMachines.DataBind();
                lblNoKnittingMachineFound.Visible = false;
                pnlViewKnittingMachines.Visible = true;
            }
            else
            {
                lblNoKnittingMachineFound.Visible = true;
                pnlViewKnittingMachines.Visible = false;
            }

        }


        //protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        //{
        //    int knittingMachineId = int.Parse(e.CommandArgument.ToString());
        //    Response.Redirect("AddKnittingMachines.aspx?knittingMachineId=" + Tools.UrlEncode(knittingMachineId + ""));
        //}


        protected void rptKnittingMachines_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {

                DropDownList ddlKnittingUnits = (DropDownList)e.Item.FindControl("ddlKnittingUnits");
                Label lblKnittingUnitId = (Label)e.Item.FindControl("lblKnittingUnitId");
                CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);


                ddlKnittingUnits.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnittingUnits, string.IsNullOrEmpty(lblKnittingUnitId.Text.ToString()) ? 0 : int.Parse(lblKnittingUnitId.Text.ToString()));
                ddlKnittingUnits.Visible = true;

                Label lblKnittingUnitValue = (Label)e.Item.FindControl("lblKnittingUnitValue");
                lblKnittingUnitValue.Visible = false;

                e.Item.FindControl("lnkbtnEdit").Visible = false;
                e.Item.FindControl("lnkbtnUpdate").Visible = true;
                e.Item.FindControl("lnkbtnCancel").Visible = true;

            }
            else if (e.CommandName == "Update")
            {
                try
                {
                    DropDownList ddlKnittingUnits = (DropDownList)e.Item.FindControl("ddlKnittingUnits");

                    if (ddlKnittingUnits.SelectedIndex == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Select a knitting unit.')", true);
                    }
                    else
                    {
                        ddlKnittingUnits.Visible = false;

                        Label lblKnittingUnitValue = (Label)e.Item.FindControl("lblKnittingUnitValue");
                        lblKnittingUnitValue.Visible = true;

                        int knittingMachineId = Convert.ToInt32(e.CommandArgument.ToString());
                        var knittingMachine = unitOfWork.GenericRepositories<KnittingMachines>().GetByID(knittingMachineId);

                        knittingMachine.KnittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue);
                        knittingMachine.UpdatedBy = CommonMethods.SessionInfo.UserName;
                        knittingMachine.UpdateDate = DateTime.Now;

                        unitOfWork.GenericRepositories<KnittingMachines>().Update(knittingMachine);
                        unitOfWork.Save();

                        lblKnittingUnitValue.Text = ddlKnittingUnits.SelectedItem.Text;
                        Label lblKnittingUnitId = (Label)e.Item.FindControl("lblKnittingUnitId");
                        lblKnittingUnitId.Text = ddlKnittingUnits.SelectedValue;

                        e.Item.FindControl("lnkbtnEdit").Visible = true;
                        e.Item.FindControl("lnkbtnUpdate").Visible = false;
                        e.Item.FindControl("lnkbtnCancel").Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
            else if (e.CommandName == "Cancel")
            {

                DropDownList ddlKnittingUnits = (DropDownList)e.Item.FindControl("ddlKnittingUnits");
                ddlKnittingUnits.Visible = false;

                Label lblKnittingUnitValue = (Label)e.Item.FindControl("lblKnittingUnitValue");
                lblKnittingUnitValue.Visible = true;


                e.Item.FindControl("lnkbtnEdit").Visible = true;
                e.Item.FindControl("lnkbtnUpdate").Visible = false;
                e.Item.FindControl("lnkbtnCancel").Visible = false;
            }
        }



    }
}