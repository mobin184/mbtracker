﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Knitting_Spares
{
    public partial class AddMachineBrands : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

               // EnableDisableDeleteButton();
                BindData();
               
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteBrand(BrandId);
                }
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            string actionTaker = CommonMethods.SessionInfo.UserName;
            var brandName = tbxBrandName.Text.Trim();
            var brandDescription = tbxBrandDescription.Text.Trim();
            var machineSystem = tbxMachineSystem.Text.Trim();
            var gaugeLowRange = tbxGaugeLowRange.Text.Trim();
            var gaugeHighRange = tbxGaugeHighRange.Text.Trim();
            var gaugeType = tbxGaugeType.Text.Trim();
            var machineBrandName = tbxMachineBrandName.Text.Trim();

           

            var isExist = unitOfWork.GenericRepositories<MachineBrands>().IsExist(x => x.MachineBrandName == machineBrandName);

            if (!isExist)
            {
                string user = CommonMethods.SessionInfo.UserName;
                var newitem = new MachineBrands()
                {
                    BrandName = brandName,
                    BrandDescription = brandDescription,
                    MachineSystem = int.Parse(machineSystem),
                    GaugeLowRange = gaugeLowRange,
                    GaugeHighRange = gaugeHighRange,
                    GaugeType = gaugeType,
                    MachineBrandName = machineBrandName,
                    CreatedBy = user,
                    CreateDate = DateTime.Now
                };
                unitOfWork.GenericRepositories<MachineBrands>().Insert(newitem);
                unitOfWork.Save();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Machine Brand already exists.')", true);
            }

            ClearAll();
            BindData();

        }

        protected void ClearAll()
        {
            tbxBrandName.Text = "";
            tbxBrandDescription.Text = "";
            tbxMachineSystem.Text = "";
            tbxGaugeLowRange.Text = "";
            tbxGaugeHighRange.Text = "";
            tbxGaugeType.Text = "";
            tbxMachineBrandName.Text = "";
            //ddlIsActive.SelectedValue = "1";
            //tbxDescription.Text = "";

        }

        protected void BindData()
        {

            DataTable dt = unitOfWork.GetDataTableFromSql("SELECT Id, BrandName, MachineBrandName,  MachineBrand = CASE WHEN [GaugeLowRange] = [GaugeHighRange] THEN [BrandName] + ' (' + CAST([GaugeLowRange] AS varchar(12)) + ')' ELSE [BrandName] + ' (' + CAST([GaugeLowRange] AS varchar(12)) + ' - ' + CAST([GaugeHighRange] AS varchar(12)) + ')' END FROM MachineBrands");

            if (dt.Rows.Count > 0)
            {
                rptMachineBrands.DataSource = dt;
                rptMachineBrands.DataBind();
            }
            else
            {
                rptMachineBrands.DataSource = null;
                rptMachineBrands.DataBind();
            }
        }

        int BrandId
        {
            set { ViewState["brandId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["brandId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void rptBuyer_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {

                BrandId = Convert.ToInt32(e.CommandArgument.ToString());
              
            }
            else if (e.CommandName == "Delete")
            {
                BrandId = Convert.ToInt32(e.CommandArgument.ToString());
                
                    var title = "Warning";
                    var msg = "Are you sure you want to delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
               
            }
        }

        private void DeleteBrand(int BrandId)
        {
            try
            {
                
                var result = unitOfWork.GenericRepositories<MachineBrands>().GetByID(BrandId);
                if (result != null)
                {
                    unitOfWork.GenericRepositories<MachineBrands>().Delete(BrandId);
                    unitOfWork.Save();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
                   

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Invalid Machine Brand id.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
            BindData();
        }


    }
}