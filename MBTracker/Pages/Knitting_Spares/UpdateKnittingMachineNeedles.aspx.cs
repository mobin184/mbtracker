﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Knitting_Parts;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Knitting_Spares
{
    public partial class UpdateKnittingMachineNeedles : System.Web.UI.Page
    {

        PartsManager partManager = new PartsManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
               
                CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
               
            }
        }


        protected void ddlKnittingUnits_SelectedIndexChanged(object sender, EventArgs e)
        {


            if (ddlKnittingUnits.SelectedValue != "")
            {
                ddlMachineBrands.Items.Clear();
                LoadMachinBrandDropdown(ddlMachineBrands, int.Parse(ddlKnittingUnits.SelectedValue));
            }
            else
            {
                ddlMachineBrands.Items.Clear();
            }

        }

        protected void btnViewKnittingMachines_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();

            if (ddlKnittingUnits.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a knitting unit.')", true);
            }else if (ddlMachineBrands.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a machine brand.')", true);
            }
            else
            {
                dt = partManager.GetExistingKnittingMachines(int.Parse(ddlMachineBrands.SelectedValue), int.Parse(ddlKnittingUnits.SelectedValue));

                if (dt.Rows.Count > 0)
                {
                    rptKnittingMachines.DataSource = dt;
                    rptKnittingMachines.DataBind();
                    lblNoKnittingMachineFound.Visible = false;
                    pnlViewKnittingMachines.Visible = true;
                }
                else
                {
                    lblNoKnittingMachineFound.Visible = true;
                    pnlViewKnittingMachines.Visible = false;
                }
            }
        }


        private void LoadMachinBrandDropdown(DropDownList ddl, int unitId)
        {
            string sql = $"Exec usp_GetAllMachineBrandByUnitId {unitId}";
            var dt = unitOfWork.GetDataTableFromSql(sql);
            DataRow dr = dt.NewRow();
            dr["MachineBrandId"] = "";
            dr["MachineBrand"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);

            ddl.DataTextField = "MachineBrand";
            ddl.DataValueField = "MachineBrandId";
            ddl.DataSource = dt;
            ddl.DataBind();
        }


        protected void rptKnittingMachines_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                var ddlWhyChange = (DropDownList)e.Item.FindControl("ddlWhyChange");
                LoadNeedleChangeReasons(ddlWhyChange);

                var ddlNeedleBrands = (DropDownList)e.Item.FindControl("ddlNeedleBrands");
                LoadNeedleBrands(ddlNeedleBrands);

            }

        }

        private void LoadNeedleChangeReasons(DropDownList ddl)
        {
            CommonMethods.LoadWhyNeedleChangeDropdown(ddl, 1, 0);
        }

        private void LoadNeedleBrands(DropDownList ddl)
        {
            CommonMethods.LoadSpareBrandsDropdown(ddl, 1, 0, 1);
        }


        protected void ddlWhyChange_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlWhyChange = (DropDownList)sender;
            DropDownList ddlOtherMachines = (DropDownList)((RepeaterItem)ddlWhyChange.NamingContainer).FindControl("ddlOtherMachines");
           
            Label lblNumNeedlesBeforeValue = (Label)((RepeaterItem)ddlWhyChange.NamingContainer).FindControl("lblNumNeedlesBeforeValue");
            TextBox tbxNumNeedlesChangeValue = (TextBox)((RepeaterItem)ddlWhyChange.NamingContainer).FindControl("tbxNumNeedlesChangeValue");
            Label lblNumNeedleAfterValue = (Label)((RepeaterItem)ddlWhyChange.NamingContainer).FindControl("lblNumNeedleAfterValue");

            if (ddlWhyChange.SelectedValue == "3" || ddlWhyChange.SelectedValue == "4")
            {
                ddlOtherMachines.Enabled = true;
                ddlOtherMachines.Items.Clear();
                LoadOtherMachines(ddlOtherMachines, int.Parse(ddlMachineBrands.SelectedValue), int.Parse(ddlKnittingUnits.SelectedValue));
            }
            else
            {
                ddlOtherMachines.Enabled = false;
                ddlOtherMachines.Items.Clear();
            }

            lblNumNeedleAfterValue.Text = "";
            int needleAfter = 0;

            if (tbxNumNeedlesChangeValue.Text != "")
            {
                if (int.Parse(ddlWhyChange.SelectedValue) == 1 || int.Parse(ddlWhyChange.SelectedValue) == 2 || int.Parse(ddlWhyChange.SelectedValue) == 3)
                {
                    needleAfter = (int.Parse(lblNumNeedlesBeforeValue.Text) - int.Parse(tbxNumNeedlesChangeValue.Text));
                }
                else if (int.Parse(ddlWhyChange.SelectedValue) == 4 || int.Parse(ddlWhyChange.SelectedValue) == 5 || int.Parse(ddlWhyChange.SelectedValue) == 6)
                {
                    needleAfter = (int.Parse(lblNumNeedlesBeforeValue.Text) + int.Parse(tbxNumNeedlesChangeValue.Text));
                }

                lblNumNeedleAfterValue.Text = needleAfter.ToString();
            }
        }


        protected void tbxNumNeedlesChangeValue_OnTextChanged(object sender, EventArgs e)
        {
            TextBox tbxNumNeedlesChangeValue = (TextBox)sender;
            Label lblNumNeedleAfterValue = (Label)((RepeaterItem)tbxNumNeedlesChangeValue.NamingContainer).FindControl("lblNumNeedleAfterValue");
            Label lblNumNeedlesBeforeValue = (Label)((RepeaterItem)tbxNumNeedlesChangeValue.NamingContainer).FindControl("lblNumNeedlesBeforeValue");
            DropDownList ddlWhyChange = (DropDownList)((RepeaterItem)tbxNumNeedlesChangeValue.NamingContainer).FindControl("ddlWhyChange");

            lblNumNeedleAfterValue.Text = "";
            int needleAfter = 0;

            if (tbxNumNeedlesChangeValue.Text != "" && ddlWhyChange.SelectedIndex != 0)
            {
                if (int.Parse(ddlWhyChange.SelectedValue) == 1 || int.Parse(ddlWhyChange.SelectedValue) == 2 || int.Parse(ddlWhyChange.SelectedValue) == 3)
                {
                    needleAfter = (int.Parse(lblNumNeedlesBeforeValue.Text) - int.Parse(tbxNumNeedlesChangeValue.Text));
                }
                else if (int.Parse(ddlWhyChange.SelectedValue) == 4 || int.Parse(ddlWhyChange.SelectedValue) == 5)
                {
                    needleAfter = (int.Parse(lblNumNeedlesBeforeValue.Text) + int.Parse(tbxNumNeedlesChangeValue.Text));
                }

                lblNumNeedleAfterValue.Text = needleAfter.ToString();
            }

        }

        


        private void LoadOtherMachines(DropDownList ddl, int machineBrandId, int knittingUnitId)
        {
            CommonMethods.LoadKnittingMachinesDropdown(ddl, 3, 0, machineBrandId, knittingUnitId);
        }


        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {
                int numItems = 0;

                for (int i = 0; i < rptKnittingMachines.Items.Count; i++)
                {

                    Label lblMachineId = (Label)this.rptKnittingMachines.Items[i].FindControl("lblMachineId");
                    TextBox tbxNumNeedlesChangeValue = (TextBox)this.rptKnittingMachines.Items[i].FindControl("tbxNumNeedlesChangeValue");
                    Label lblNumNeedlesBeforeValue = (Label)this.rptKnittingMachines.Items[i].FindControl("lblNumNeedlesBeforeValue");
                    Label lblNumNeedleAfterValue = (Label)this.rptKnittingMachines.Items[i].FindControl("lblNumNeedleAfterValue");
                    TextBox tbxRemarks = (TextBox)this.rptKnittingMachines.Items[i].FindControl("tbxRemarks");

                    DropDownList ddlNeedleBrands = (DropDownList)this.rptKnittingMachines.Items[i].FindControl("ddlNeedleBrands");


                    DropDownList ddlWhyChange = (DropDownList)this.rptKnittingMachines.Items[i].FindControl("ddlWhyChange");
                    DropDownList ddlOtherMachines = (DropDownList)this.rptKnittingMachines.Items[i].FindControl("ddlOtherMachines");


                    int otherMachineId = 0;

                    if (ddlWhyChange.SelectedIndex != 0 && (int.Parse(ddlWhyChange.SelectedValue) == 3 || int.Parse(ddlWhyChange.SelectedValue) == 4))
                    {
                        if (ddlOtherMachines.SelectedIndex != 0)
                        {
                            otherMachineId = int.Parse(ddlOtherMachines.SelectedValue);
                        }
                        else
                        {
                            Label lblMachineNameValue = (Label)this.rptKnittingMachines.Items[i].FindControl("lblMachineNameValue");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Select other machine for: " + lblMachineNameValue.Text + "')", true);
                            break;
                        }

                    }


                    if (tbxNumNeedlesChangeValue.Text != "" && ddlWhyChange.SelectedIndex != 0)
                    {
                        var knittingMachine = new KnittingMachineCurrentNeedles()
                        {
                            //DateAndTime = DateTime.Now,
                            MachineId = int.Parse(lblMachineId.Text),
                            NumNeedlesBefore = int.Parse(lblNumNeedlesBeforeValue.Text),
                            NumNeedlesChanged = int.Parse(tbxNumNeedlesChangeValue.Text),
                            NeedleBrandId = int.Parse(ddlNeedleBrands.SelectedValue),
                            NumNeedlesAfter = int.Parse(lblNumNeedleAfterValue.Text),
                            WhyChanged = int.Parse(ddlWhyChange.SelectedValue),
                            Remarks = tbxRemarks.Text.ToString(),

                            OtherMachineId = (otherMachineId == 0 ? (int?)null : otherMachineId),
                            CreatedBy = CommonMethods.SessionInfo.UserName,
                            CreateDate = DateTime.Now
                        };
                        unitOfWork.GenericRepositories<KnittingMachineCurrentNeedles>().Insert(knittingMachine);
                        numItems++;
                    }
                }

                if (numItems > 0)
                {
                    unitOfWork.Save();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Data was not input correctly, nothing saved.');", true);
                }
                
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "')", true);
            }
        }

    }
}