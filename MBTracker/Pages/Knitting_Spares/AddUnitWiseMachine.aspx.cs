﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Knitting_Spares
{
    public partial class AddUnitWiseMachine : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                

                PopulateEntryInfo();
            }
            

        }
        //protected void btnAddRow_Click(object sender, EventArgs e)
        //{
        //    this.AddNewRowToGrid();
        //}
        private void PopulateEntryInfo()
        {

            SetInitialRowCount();

        }

        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("MachineBrandId", typeof(string)));
            dt.Columns.Add(new DataColumn("ProductionUnitId", typeof(string)));
            dt.Columns.Add(new DataColumn("NumberOfMachines", typeof(string)));

            dr = dt.NewRow();
            
            dr["MachineBrandId"] = string.Empty;
            dr["ProductionUnitId"] = string.Empty;
            dr["NumberOfMachines"] = string.Empty;
            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptKnittingMachinesEntryInfo.DataSource = dt;
            rptKnittingMachinesEntryInfo.DataBind();
        }

        protected void rptKnittingMachinesEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                var ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                LoadMachineBrands(ddlMachineBrands);

                var ddlProductionUnitId = (DropDownList)e.Item.FindControl("ddlProductionUnitId");
                LoadKnittigUnitDropdown(ddlProductionUnitId);

            }

        }

        //private void AddNewRowToGrid()
        //{

        //    if (this.ViewState["CurrentTable"] == null)
        //    {
        //        base.Response.Write("ViewState is null");
        //    }
        //    else
        //    {
        //        DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
        //        DataRow drCurrentRow = null;
        //        if (dtCurrentTable.Rows.Count > 0)
        //        {
        //            for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
        //            {


        //                DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
        //                DropDownList ddlKnittingUnits = (DropDownList)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("ddlKnittingUnits");
        //                TextBox tbxMachineCode = (TextBox)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("tbxMachineCode");
        //                TextBox tbxMachineName = (TextBox)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("tbxMachineName");
        //                TextBox tbxNeedleCapacity = (TextBox)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("tbxNeedleCapacity");


        //                dtCurrentTable.Rows[rowIndex]["MachineBrandId"] = (ddlMachineBrands.SelectedValue == "" ? "0" : ddlMachineBrands.SelectedValue);
        //                dtCurrentTable.Rows[rowIndex]["KnittingUnitId"] = (ddlKnittingUnits.SelectedValue == "" ? "0" : ddlKnittingUnits.SelectedValue);
        //                dtCurrentTable.Rows[rowIndex]["MachineCode"] = tbxMachineCode.Text;
        //                dtCurrentTable.Rows[rowIndex]["MachineName"] = tbxMachineName.Text;
        //                dtCurrentTable.Rows[rowIndex]["NeedleCapacity"] = tbxNeedleCapacity.Text;


        //                if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
        //                {
        //                    drCurrentRow = dtCurrentTable.NewRow();
        //                    drCurrentRow["MachineCode"] = string.Empty;
        //                    drCurrentRow["MachineName"] = string.Empty;
        //                    drCurrentRow["MachineBrandId"] = string.Empty;
        //                    drCurrentRow["KnittingUnitId"] = string.Empty;
        //                    drCurrentRow["NeedleCapacity"] = string.Empty;

        //                }
        //            }

        //            dtCurrentTable.Rows.Add(drCurrentRow);
        //            this.ViewState["CurrentTable"] = dtCurrentTable;

        //            this.rptKnittingMachinesEntryInfo.DataSource = dtCurrentTable;
        //            this.rptKnittingMachinesEntryInfo.DataBind();
        //        }
        //    }
        //    this.SetPreviousData();
        //}

        //private void SetPreviousData()
        //{
        //    int rowIndex = 0;
        //    if (this.ViewState["CurrentTable"] != null)
        //    {
        //        DataTable dt = (DataTable)this.ViewState["CurrentTable"];
        //        if (dt.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < dt.Rows.Count; i++)
        //            {

        //                DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
        //                DropDownList ddlKnittingUnits = (DropDownList)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("ddlKnittingUnits");
        //                TextBox tbxMachineCode = (TextBox)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("tbxMachineCode");
        //                TextBox tbxMachineName = (TextBox)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("tbxMachineName");
        //                TextBox tbxNeedleCapacity = (TextBox)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("tbxNeedleCapacity");


        //                this.LoadMachineBrands(ddlMachineBrands);
        //                this.LoadKnittigUnitDropdown(ddlKnittingUnits);



        //                ddlMachineBrands.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrands, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["MachineBrandId"].ToString()) ? "0" : dt.Rows[i]["MachineBrandId"].ToString())));
        //                ddlKnittingUnits.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnittingUnits, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["KnittingUnitId"].ToString()) ? "0" : dt.Rows[i]["KnittingUnitId"].ToString())));

        //                tbxMachineCode.Text = dt.Rows[i]["MachineCode"].ToString();
        //                tbxMachineName.Text = dt.Rows[i]["MachineName"].ToString();
        //                tbxNeedleCapacity.Text = dt.Rows[i]["NeedleCapacity"].ToString();

        //                rowIndex++;
        //            }
        //        }
        //    }
        //}

        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {

                for (int i = 0; i < rptKnittingMachinesEntryInfo.Items.Count; i++)
                {


                    DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingMachinesEntryInfo.Items[i].FindControl("ddlMachineBrands");
                    DropDownList ddlProductionUnitId = (DropDownList)this.rptKnittingMachinesEntryInfo.Items[i].FindControl("ddlProductionUnitId");
                    
                    TextBox tbxNumberOfMachines = (TextBox)this.rptKnittingMachinesEntryInfo.Items[i].FindControl("tbxNumberOfMachines");


                    if (ddlMachineBrands.SelectedIndex != 0 && ddlProductionUnitId.SelectedIndex != 0 && !string.IsNullOrEmpty(tbxNumberOfMachines.Text))
                    {
                        var ProductonUnitMachineBrand = new ProductonUnitMachineBrand()
                        {
                            
                            MachineBrandId = int.Parse(ddlMachineBrands.SelectedValue),
                            ProductionUnitId = int.Parse(ddlProductionUnitId.SelectedValue),
                            NumberOfMachines = int.Parse(tbxNumberOfMachines.Text),
                            CreatedBy = CommonMethods.SessionInfo.UserName,
                            CreateDate = DateTime.Now
                        };
                        unitOfWork.GenericRepositories<ProductonUnitMachineBrand>().Insert(ProductonUnitMachineBrand);
                    }
                }

                unitOfWork.Save();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "')", true);
            }
        }

        private void LoadMachineBrands(DropDownList ddl)
        {
            CommonMethods.LoadMachineBrandDropdown(ddl, 1, 0);
        }
        private void LoadKnittigUnitDropdown(DropDownList ddl)
        {
            CommonMethods.LoadKnittingUnitDropdown(ddl, 1, 0);
        }

    }
}