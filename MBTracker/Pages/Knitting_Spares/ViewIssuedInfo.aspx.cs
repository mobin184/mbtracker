﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Knitting_Parts;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Knitting_Spares
{
    public partial class ViewIssuedInfo : System.Web.UI.Page
    {

        PartsManager partManager = new PartsManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
            }

        }

        protected void ddlKnittingUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlKnittingUnits.SelectedValue != "")
            {
                LoadMachineBrandsByUnit(ddlMachineBrands, int.Parse(ddlKnittingUnits.SelectedValue));
            }
            else
            {
                ddlMachineBrands.Items.Clear();
                pnlViewKnittingSpareRequests.Visible = false;
            }
        }

        private void LoadMachineBrandsByUnit(DropDownList ddl, int unitId)
        {
            string sql = $"Exec usp_GetAllMachineBrandByUnitId {unitId}";
            var dt = unitOfWork.GetDataTableFromSql(sql);
            DataRow dr = dt.NewRow();
            dr["MachineBrandId"] = "";
            dr["MachineBrand"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);

            ddl.DataTextField = "MachineBrand";
            ddl.DataValueField = "MachineBrandId";
            ddl.DataSource = dt;
            ddl.DataBind();
        }


        protected void btnViewIssuedSpares_Click(object sender, EventArgs e)
        {
            ShowIssuedSpares();
        }


        private void ShowIssuedSpares()
        {
            DataTable dt = new DataTable();

            int knittingUnitId = 0;
            int machineBrandId = 0;
            DateTime fromDate = DateTime.Now.AddDays(-30);

            if (ddlKnittingUnits.SelectedValue != "")
            {

                if (ddlMachineBrands.SelectedValue != "")
                {
                    machineBrandId = int.Parse(ddlMachineBrands.SelectedValue);
                }

                if (tbxFromDate.Text != "")
                {
                    fromDate = DateTime.Parse(tbxFromDate.Text);
                }

                knittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue);

                dt = partManager.GetIssuedSpares(knittingUnitId, machineBrandId, fromDate);

                if (dt.Rows.Count > 0)
                {
                    rptIssuedSpares.DataSource = dt;
                    rptIssuedSpares.DataBind();
                    lblNoIssuedSparesFound.Visible = false;
                    pnlViewKnittingSpareRequests.Visible = true;
                }
                else
                {
                    lblNoIssuedSparesFound.Visible = true;
                    pnlViewKnittingSpareRequests.Visible = false;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Select a knitting unit.')", true);
            }
        }


            protected void rptIssuedSpares_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {


                TextBox tbxIssueQty = (TextBox)e.Item.FindControl("tbxIssueQty");
                TextBox tbxIssuedNotes = (TextBox)e.Item.FindControl("tbxIssuedNotes");


                tbxIssueQty.Visible = true;
                tbxIssuedNotes.Visible = true;

                Label lblIssedQty = (Label)e.Item.FindControl("lblIssedQty");
                Label lblIssedNotes = (Label)e.Item.FindControl("lblIssedNotes");

                lblIssedQty.Visible = false;
                lblIssedNotes.Visible = false;


                e.Item.FindControl("lnkbtnEdit").Visible = false;
                e.Item.FindControl("lnkbtnUpdate").Visible = true;
                e.Item.FindControl("lnkbtnCancel").Visible = true;

            }
            else if (e.CommandName == "Update")
            {
                try
                {

                    TextBox tbxIssueQty = (TextBox)e.Item.FindControl("tbxIssueQty");
                    TextBox tbxIssuedNotes = (TextBox)e.Item.FindControl("tbxIssuedNotes");


                    Label lblTotalIssued = (Label)e.Item.FindControl("lblTotalIssued");
                    Label lblIssedQty = (Label)e.Item.FindControl("lblIssedQty");
                    Label lblVerifiedQty = (Label)e.Item.FindControl("lblVerifiedQty");




                    if (tbxIssueQty.Text.ToString() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Enter issue quantity.')", true);
                    }
                    else
                    {

                        int newTotal = (int.Parse(lblTotalIssued.Text) - int.Parse(lblIssedQty.Text)) + int.Parse(tbxIssueQty.Text);

                        if (newTotal > int.Parse(lblVerifiedQty.Text))
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('You cannot issue more than verified quantity.')", true);
                        }
                        else
                        {

                            //lblTotalIssued.Text = newTotal.ToString();

                            int spareIssuedId = Convert.ToInt32(e.CommandArgument.ToString());

                          


                            var spareIssued = unitOfWork.GenericRepositories<KnittingSpareIssued>().GetByID(spareIssuedId);
                            spareIssued.Id = spareIssuedId;
                            spareIssued.IssueQuantity = int.Parse(tbxIssueQty.Text);
                            spareIssued.Notes = tbxIssuedNotes.Text.ToString();
                            spareIssued.UpdatedBy = CommonMethods.SessionInfo.UserName;
                            spareIssued.UpdateDate = DateTime.Now;


                            unitOfWork.GenericRepositories<KnittingSpareIssued>().Update(spareIssued);
                            unitOfWork.Save();


                            partManager.UpdateSpareSummaryForSpareIssueUpdate(spareIssuedId, int.Parse(lblIssedQty.Text.ToString()), int.Parse(tbxIssueQty.Text.ToString()), CommonMethods.SessionInfo.UserName, DateTime.Now);


                            tbxIssueQty.Visible = false;
                            tbxIssuedNotes.Visible = false;

                            //Label lblIssedQty = (Label)e.Item.FindControl("lblIssedQty");
                            Label lblIssedNotes = (Label)e.Item.FindControl("lblIssedNotes");

                            lblIssedQty.Text = tbxIssueQty.Text;
                            lblIssedNotes.Text = tbxIssuedNotes.Text;


                            lblIssedQty.Visible = true;
                            lblIssedNotes.Visible = true;

                            e.Item.FindControl("lnkbtnEdit").Visible = true;
                            e.Item.FindControl("lnkbtnUpdate").Visible = false;
                            e.Item.FindControl("lnkbtnCancel").Visible = false;

                            ShowIssuedSpares();

                        }
                            

                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
            else if (e.CommandName == "Cancel")
            {
                TextBox tbxIssueQty = (TextBox)e.Item.FindControl("tbxIssueQty");
                TextBox tbxIssuedNotes = (TextBox)e.Item.FindControl("tbxIssuedNotes");


                tbxIssueQty.Visible = false;
                tbxIssuedNotes.Visible = false;

                Label lblIssedQty = (Label)e.Item.FindControl("lblIssedQty");
                Label lblIssedNotes = (Label)e.Item.FindControl("lblIssedNotes");

                lblIssedQty.Visible = true;
                lblIssedNotes.Visible = true;


                e.Item.FindControl("lnkbtnEdit").Visible = true;
                e.Item.FindControl("lnkbtnUpdate").Visible = false;
                e.Item.FindControl("lnkbtnCancel").Visible = false;
            }
        }

    }
}