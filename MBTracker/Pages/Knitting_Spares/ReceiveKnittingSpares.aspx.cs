﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Knitting_Parts;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Knitting_Parts
{
    public partial class ReceiveKnittingSpares : System.Web.UI.Page
    {

        UnitOfWork unitOfWork = new UnitOfWork();
        PartsManager partManager = new PartsManager();
        DataTable receivedSapreDetails;
        int initialLoadForUpdate = 0;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Request.QueryString["spareReceiveId"] != null)
                {
                    SpareReceiveId = int.Parse(Tools.UrlDecode(Request.QueryString["spareReceiveId"]));
                    initialLoadForUpdate = SpareReceiveId;
                    lblSpareReceiveEntry.Text = "Update Knitting Spare Receive:";
                    lblMessage.Text = "Received Spare Information:";
                    lnkbtnSaveEntries.Visible = false;
                    lnkbtnUpdateEntries.Visible = true;

                }

                PopulateEntryInfo();
            }

        }


        int SpareReceiveId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["spareReceiveId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["spareReceiveId"] = value; }
        }


        private void PopulateEntryInfo()
        {
            if (SpareReceiveId > 0)
            {
                LoadReceivedSpares();
            }
            else
            {
                tbxDateReceived.Text = DateTime.Now.ToString("yyyy-MM-dd");
                LoadPartSuppliers(ddlSuppliers);

                SetInitialRowCount();
            }
        }

        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;


            dt.Columns.Add(new DataColumn("KnittingSpareTypeId", typeof(string)));
            dt.Columns.Add(new DataColumn("MachineBrandId", typeof(string)));
            dt.Columns.Add(new DataColumn("SpareId", typeof(string)));
            dt.Columns.Add(new DataColumn("SpareBrandId", typeof(string)));

            dt.Columns.Add(new DataColumn("SupplierCode", typeof(string)));
            dt.Columns.Add(new DataColumn("SpareDescription", typeof(string)));

            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("UnitPrice", typeof(string)));


            dr = dt.NewRow();


            dr["KnittingSpareTypeId"] = string.Empty;
            dr["MachineBrandId"] = string.Empty;
            dr["SpareId"] = string.Empty;
            dr["SpareBrandId"] = string.Empty;
            dr["SupplierCode"] = string.Empty;
            dr["SpareDescription"] = string.Empty;
            dr["Quantity"] = string.Empty;
            dr["UnitPrice"] = string.Empty;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptKnittingPartsReceiveEntryInfo.DataSource = dt;
            rptKnittingPartsReceiveEntryInfo.DataBind();
        }


        protected void rptKnittingPartsReceiveEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                if (initialLoadForUpdate > 0)
                {

                    var lblKnittingSpareReceiveDetailsId = (Label)e.Item.FindControl("lblKnittingSpareReceiveDetailsId");

                    var ddlKnittingSpareTypes = (DropDownList)e.Item.FindControl("ddlKnittingSpareTypes");
                    var ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                    var ddlSpareNames = (DropDownList)e.Item.FindControl("ddlSpareNames");
                    var ddlSpareBrands = (DropDownList)e.Item.FindControl("ddlSpareBrands");


                    var tbxSupplierCode = (TextBox)e.Item.FindControl("tbxSupplierCode");
                    var tbxSpareDescription = (TextBox)e.Item.FindControl("tbxSpareDescription");
                    var tbxQuantity = (TextBox)e.Item.FindControl("tbxQuantity");
                    var tbxUnitPrice = (TextBox)e.Item.FindControl("tbxUnitPrice");


                    var knittingSpareReceiveDetailsId = DataBinder.Eval(e.Item.DataItem, "KnittingSpareReceiveDetailsId").ToString();

                    var knittingSpareTypeId = int.Parse(DataBinder.Eval(e.Item.DataItem, "KnittingSpareTypeId").ToString());
                    var machineBrandId = int.Parse(DataBinder.Eval(e.Item.DataItem, "MachineBrandId").ToString());
                    var knittingSpareId = int.Parse(DataBinder.Eval(e.Item.DataItem, "SpareId").ToString());
                    var supplierCode = DataBinder.Eval(e.Item.DataItem, "SupplierCode").ToString();
                    var spareDescription = DataBinder.Eval(e.Item.DataItem, "SpareDescription").ToString();
                    var SpareQuantity = int.Parse(DataBinder.Eval(e.Item.DataItem, "Quantity").ToString());
                    var spareUnitPrice = DataBinder.Eval(e.Item.DataItem, "UnitPrice").ToString();
                    var SpareBrandId = DataBinder.Eval(e.Item.DataItem, "SpareBrandId").ToString();

                    

                    this.LoadMachineBrands(ddlMachineBrands);
                    this.LoadSpareTypesDropdown(ddlKnittingSpareTypes);
                    this.LoadSpareBrands(ddlSpareBrands, knittingSpareTypeId);

                    ddlKnittingSpareTypes.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnittingSpareTypes, knittingSpareTypeId);
                    ddlMachineBrands.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrands, machineBrandId);
                    ddlSpareBrands.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSpareBrands, int.Parse(SpareBrandId));



                    if (ddlKnittingSpareTypes.SelectedValue != "" && ddlMachineBrands.SelectedValue != "")
                    {
                        this.LoadSpareNames(ddlSpareNames, int.Parse(ddlKnittingSpareTypes.SelectedValue), int.Parse(ddlMachineBrands.SelectedValue));
                        ddlSpareNames.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSpareNames, knittingSpareId);

                    }

                    lblKnittingSpareReceiveDetailsId.Text = knittingSpareReceiveDetailsId.ToString();

                    tbxSupplierCode.Text = supplierCode.ToString();
                    tbxSpareDescription.Text = spareDescription.ToString();
                    tbxQuantity.Text = SpareQuantity.ToString();
                    tbxUnitPrice.Text = spareUnitPrice.ToString();

                }
                else
                {
                    var ddlKnittingSpareTypes = (DropDownList)e.Item.FindControl("ddlKnittingSpareTypes");
                    LoadSpareTypesDropdown(ddlKnittingSpareTypes);

                    var ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                    LoadMachineBrands(ddlMachineBrands);

                }

                //var ddlPartNames = (DropDownList)e.Item.FindControl("ddlPartNames");
                //LoadPartNames(ddlPartNames, int.Parse(ddlMachineBrands.SelectedValue), int.Parse(ddlKnittingSpareTypes.SelectedValue));

            }
        }


        private void LoadSpareTypesDropdown(DropDownList ddl)
        {
            CommonMethods.LoadPartTypeDropdown(ddl, 1, 0);
        }


        private void LoadMachineBrands(DropDownList ddl)
        {
            CommonMethods.LoadMachineBrandDropdown(ddl, 1, 0);
        }


        private void LoadSpareNames(DropDownList ddl, int spareTypeId, int machineBrandId)
        {
            CommonMethods.LoadSpareNamesDropdown(ddl, 5, 0, spareTypeId, machineBrandId);
        }


        private void LoadSpareBrands(DropDownList ddl, int knittingSpareTypeId)
        {
            CommonMethods.LoadSpareBrandsDropdown(ddl, 1, 0, knittingSpareTypeId);
        }


        private void LoadPartSuppliers(DropDownList ddl)
        {
            CommonMethods.LoadPartSuppliersDropdown(ddl, 1, 0);
        }

        private void LoadPartManufacturers(DropDownList ddl)
        {
            CommonMethods.LoadPartManufacturersDropdown(ddl, 1, 0);
        }


        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            this.AddNewRowToGrid();
        }

        private void AddNewRowToGrid()
        {

            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                initialLoadForUpdate = 0;
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {

                        DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
                        DropDownList ddlKnittingSpareTypes = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlKnittingSpareTypes");
                        DropDownList ddlSpareNames = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlSpareNames");
                        DropDownList ddlSpareBrands = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlSpareBrands");

                        TextBox tbxSupplierCode = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxSupplierCode");
                        TextBox tbxSpareDescription = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxSpareDescription");

                        TextBox tbxQuantity = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxQuantity");
                        TextBox tbxUnitPrice = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");


                        dtCurrentTable.Rows[rowIndex]["MachineBrandId"] = (ddlMachineBrands.SelectedValue == "" ? "0" : ddlMachineBrands.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["KnittingSpareTypeId"] = (ddlKnittingSpareTypes.SelectedValue == "" ? "0" : ddlKnittingSpareTypes.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["SpareId"] = (ddlSpareNames.SelectedValue == "" ? "0" : ddlSpareNames.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["SpareBrandId"] = (ddlSpareBrands.SelectedValue == "" ? "0" : ddlSpareBrands.SelectedValue);


                        dtCurrentTable.Rows[rowIndex]["SupplierCode"] = tbxSupplierCode.Text;
                        dtCurrentTable.Rows[rowIndex]["SpareDescription"] = tbxSpareDescription.Text;

                        dtCurrentTable.Rows[rowIndex]["Quantity"] = tbxQuantity.Text;
                        dtCurrentTable.Rows[rowIndex]["UnitPrice"] = tbxUnitPrice.Text;



                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["MachineBrandId"] = string.Empty;
                            drCurrentRow["KnittingSpareTypeId"] = string.Empty;
                            drCurrentRow["SpareId"] = string.Empty;
                            drCurrentRow["SpareBrandId"] = string.Empty;
                            drCurrentRow["SupplierCode"] = string.Empty;
                            drCurrentRow["SpareDescription"] = string.Empty;
                            drCurrentRow["Quantity"] = string.Empty;
                            drCurrentRow["UnitPrice"] = string.Empty;

                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptKnittingPartsReceiveEntryInfo.DataSource = dtCurrentTable;
                    this.rptKnittingPartsReceiveEntryInfo.DataBind();
                }
            }
            this.SetPreviousData();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
                        DropDownList ddlKnittingSpareTypes = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlKnittingSpareTypes");
                        DropDownList ddlSpareNames = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlSpareNames");
                        DropDownList ddlSpareBrands = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlSpareBrands");


                        TextBox tbxSupplierCode = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxSupplierCode");
                        TextBox tbxSpareDescription = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxSpareDescription");

                        TextBox tbxQuantity = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxQuantity");
                        TextBox tbxUnitPrice = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");


                        this.LoadMachineBrands(ddlMachineBrands);
                        this.LoadSpareTypesDropdown(ddlKnittingSpareTypes);


                        ddlMachineBrands.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrands, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["MachineBrandId"].ToString()) ? "0" : dt.Rows[i]["MachineBrandId"].ToString())));
                        ddlKnittingSpareTypes.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnittingSpareTypes, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["KnittingSpareTypeId"].ToString()) ? "0" : dt.Rows[i]["KnittingSpareTypeId"].ToString())));

                        if (ddlKnittingSpareTypes.SelectedValue != "" && ddlMachineBrands.SelectedValue != "")
                        {
                            this.LoadSpareNames(ddlSpareNames, int.Parse(ddlKnittingSpareTypes.SelectedValue), int.Parse(ddlMachineBrands.SelectedValue));
                            ddlSpareNames.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSpareNames, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["SpareId"].ToString()) ? "0" : dt.Rows[i]["SpareId"].ToString())));

                        }

                        if (ddlKnittingSpareTypes.SelectedValue != "")
                        {
                            ddlSpareBrands.Items.Clear();
                            LoadSpareBrands(ddlSpareBrands, int.Parse(ddlKnittingSpareTypes.SelectedValue));
                            ddlSpareBrands.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSpareBrands, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["SpareBrandId"].ToString()) ? "0" : dt.Rows[i]["SpareBrandId"].ToString())));
                        }



                        tbxSupplierCode.Text = dt.Rows[i]["SupplierCode"].ToString();
                        tbxSpareDescription.Text = dt.Rows[i]["SpareDescription"].ToString();

                        tbxQuantity.Text = dt.Rows[i]["Quantity"].ToString();
                        tbxUnitPrice.Text = dt.Rows[i]["UnitPrice"].ToString();


                        rowIndex++;
                    }
                }
            }
        }


        private void LoadReceivedSpares()
        {
            string sql = $"Exec usp_GetKnittingSpareReceivesById '{SpareReceiveId}'";
            var dtSpareReceive = unitOfWork.GetDataTableFromSql(sql);

            tbxDateReceived.Text = DateTime.Parse(dtSpareReceive.Rows[0]["ReceiveDate"].ToString()).ToString("yyyy-MM-dd");

            LoadPartSuppliers(ddlSuppliers);
            ddlSuppliers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSuppliers, int.Parse((string.IsNullOrEmpty(dtSpareReceive.Rows[0]["SupplierId"].ToString()) ? "0" : dtSpareReceive.Rows[0]["SupplierId"].ToString())));

            tbxRequistionNumber.Text = dtSpareReceive.Rows[0]["RequisitionNumber"].ToString();
            tbxInvoiceNumber.Text = dtSpareReceive.Rows[0]["InvoiceNumber"].ToString();
            tbxChalanNumber.Text = dtSpareReceive.Rows[0]["ChalanNumber"].ToString();
            tbxOtherRefNumber.Text = dtSpareReceive.Rows[0]["OtherRefNumber"].ToString();


            string sql2 = $"Exec usp_GetKnittingSpareReceiveDetailsByReceiveId '{SpareReceiveId}'";
            receivedSapreDetails = unitOfWork.GetDataTableFromSql(sql2);


            if (receivedSapreDetails.Rows.Count > 0)
            {
                rptKnittingPartsReceiveEntryInfo.DataSource = receivedSapreDetails;
                rptKnittingPartsReceiveEntryInfo.DataBind();

            }

            CreateDataTableForViewState();

        }

        private void CreateDataTableForViewState()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("KnittingSpareTypeId", typeof(string)));
            dt.Columns.Add(new DataColumn("MachineBrandId", typeof(string)));
            dt.Columns.Add(new DataColumn("SpareId", typeof(string)));
            dt.Columns.Add(new DataColumn("SupplierCode", typeof(string)));
            dt.Columns.Add(new DataColumn("SpareDescription", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("UnitPrice", typeof(string)));


            for (int rowIndex = 0; rowIndex < receivedSapreDetails.Rows.Count; rowIndex++)
            {
                dr = dt.NewRow();

                dr["KnittingSpareTypeId"] = receivedSapreDetails.Rows[rowIndex]["KnittingSpareTypeId"];
                dr["MachineBrandId"] = receivedSapreDetails.Rows[rowIndex]["MachineBrandId"];
                dr["SpareId"] = receivedSapreDetails.Rows[rowIndex]["SpareId"];
                dr["SupplierCode"] = receivedSapreDetails.Rows[rowIndex]["SupplierCode"];
                dr["SpareDescription"] = receivedSapreDetails.Rows[rowIndex]["SpareDescription"];
                dr["Quantity"] = receivedSapreDetails.Rows[rowIndex]["Quantity"];
                dr["UnitPrice"] = receivedSapreDetails.Rows[rowIndex]["UnitPrice"];

                dt.Rows.Add(dr);
            }


            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

        }


        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {

            if (ddlSuppliers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a supplier.')", true);
            }
            else if (String.IsNullOrEmpty(tbxRequistionNumber.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter requisition number.')", true);
            }
            //else if (String.IsNullOrEmpty(tbxInvoiceNumber.Text))
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter invoice/bill number.')", true);
            //}
            //else if (String.IsNullOrEmpty(tbxChalanNumber.Text))
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter chalan number.')", true);
            //}
            else
            {
                try
                {

                    List<KnittingSpareReceiveDetails> listKnittingSpareReceiveDetails = new List<KnittingSpareReceiveDetails>();

                    var ksr = new KnittingSpareReceives()
                    {
                        ReceiveDate = DateTime.Parse(tbxDateReceived.Text),
                        SupplierId = int.Parse(ddlSuppliers.SelectedValue),
                        RequisitionNumber = tbxRequistionNumber.Text,
                        InvoiceNumber = tbxInvoiceNumber.Text,
                        ChalanNumber = tbxChalanNumber.Text,
                        OtherRefNumber = tbxOtherRefNumber.Text,
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };

                    unitOfWork.GenericRepositories<KnittingSpareReceives>().Insert(ksr);
                    unitOfWork.Save();

                    for (int rowIndex = 0; rowIndex < rptKnittingPartsReceiveEntryInfo.Items.Count; rowIndex++)
                    {


                        DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
                        DropDownList ddlKnittingSpareTypes = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlKnittingSpareTypes");
                        DropDownList ddlSpareNames = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlSpareNames");
                        DropDownList ddlSpareBrands = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlSpareBrands");


                        TextBox tbxSupplierCode = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxSupplierCode");
                        TextBox tbxSpareDescription = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxSpareDescription");

                        TextBox tbxQuantity = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxQuantity");
                        TextBox tbxUnitPrice = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");


                        if (ddlKnittingSpareTypes.SelectedIndex != 0 && ddlSpareNames.SelectedIndex != 0 && ddlSpareBrands.SelectedIndex != 0 && !string.IsNullOrEmpty(tbxQuantity.Text) && !string.IsNullOrEmpty(tbxUnitPrice.Text))
                        {
                            var spareReceiveDetails = new KnittingSpareReceiveDetails()
                            {
                                KnittingSpareReceiveId = ksr.Id,
                                KnittingSpareId = int.Parse(ddlSpareNames.SelectedValue),
                                SpareBrandId = int.Parse(ddlSpareBrands.SelectedValue),
                                SupplierCode = tbxSupplierCode.Text,
                                SpareDescription = tbxSpareDescription.Text,
                                SpareQuantity = int.Parse(tbxQuantity.Text),
                                SpareUnitPrice = decimal.Parse(tbxUnitPrice.Text),
                                CreateDate = DateTime.Now,
                                CreatedBy = CommonMethods.SessionInfo.UserName
                            };
                            listKnittingSpareReceiveDetails.Add(spareReceiveDetails);
                        }

                    }


                    foreach (var item in listKnittingSpareReceiveDetails)
                    {
                        unitOfWork.GenericRepositories<KnittingSpareReceiveDetails>().Insert(item);
                    }

                    unitOfWork.Save();

                    foreach (var item in listKnittingSpareReceiveDetails)
                    {
                        partManager.UpdateSpareSummaryForSpareReceive(item.KnittingSpareId, item.SpareBrandId, ksr.SupplierId, item.SupplierCode, item.SpareQuantity, item.SpareUnitPrice.ToString(), CommonMethods.SessionInfo.UserName, DateTime.Now);
                    }


                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                   
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
        }

        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {

            if (ddlSuppliers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a supplier.')", true);
            }
            else if (String.IsNullOrEmpty(tbxRequistionNumber.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter requisition number.')", true);
            }
            //else if (String.IsNullOrEmpty(tbxInvoiceNumber.Text))
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter invoice/bill number.')", true);
            //}
            //else if (String.IsNullOrEmpty(tbxChalanNumber.Text))
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter chalan number.')", true);
            //}
            else
            {
                try
                {

                    var receiveSpare = unitOfWork.GenericRepositories<KnittingSpareReceives>().GetByID(SpareReceiveId);

                    receiveSpare.ReceiveDate = DateTime.Parse(tbxDateReceived.Text);
                    receiveSpare.SupplierId = int.Parse(ddlSuppliers.SelectedValue);
                    receiveSpare.RequisitionNumber = tbxRequistionNumber.Text;
                    receiveSpare.InvoiceNumber = tbxInvoiceNumber.Text;
                    receiveSpare.ChalanNumber = tbxChalanNumber.Text;
                    receiveSpare.OtherRefNumber = tbxOtherRefNumber.Text;
                    receiveSpare.UpdateDate = DateTime.Now;
                    receiveSpare.UpdatedBy = CommonMethods.SessionInfo.UserName;

                    unitOfWork.GenericRepositories<KnittingSpareReceives>().Update(receiveSpare);
                    unitOfWork.Save();


                    List<KnittingSpareReceiveDetails> listKnittingSpareReceiveDetails = new List<KnittingSpareReceiveDetails>();

                    for (int rowIndex = 0; rowIndex < rptKnittingPartsReceiveEntryInfo.Items.Count; rowIndex++)
                    {


                        DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
                        DropDownList ddlKnittingSpareTypes = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlKnittingSpareTypes");
                        DropDownList ddlSpareNames = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlSpareNames");
                        DropDownList ddlSpareBrands = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlSpareBrands");

                        TextBox tbxSupplierCode = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxSupplierCode");
                        TextBox tbxSpareDescription = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxSpareDescription");

                        TextBox tbxQuantity = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxQuantity");
                        TextBox tbxUnitPrice = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");

                        Label lblKnittingSpareReceiveDetailsId = (Label)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("lblKnittingSpareReceiveDetailsId");

                        if (!string.IsNullOrEmpty(lblKnittingSpareReceiveDetailsId.Text.ToString()))
                        {
                            partManager.DeleteSpareReceiveDetails(int.Parse(lblKnittingSpareReceiveDetailsId.Text), CommonMethods.SessionInfo.UserName, DateTime.Now);
                        }


                        if (ddlKnittingSpareTypes.SelectedIndex != 0 && ddlSpareNames.SelectedIndex != 0 && !string.IsNullOrEmpty(tbxQuantity.Text) && !string.IsNullOrEmpty(tbxUnitPrice.Text))
                        {
                            var spareReceiveDetails = new KnittingSpareReceiveDetails()
                            {
                                KnittingSpareReceiveId = SpareReceiveId,
                                KnittingSpareId = int.Parse(ddlSpareNames.SelectedValue),
                                SpareBrandId = int.Parse(ddlSpareBrands.SelectedValue),
                                SupplierCode = tbxSupplierCode.Text,
                                SpareDescription = tbxSpareDescription.Text,
                                SpareQuantity = int.Parse(tbxQuantity.Text),
                                SpareUnitPrice = decimal.Parse(tbxUnitPrice.Text),
                                CreateDate = DateTime.Now,
                                CreatedBy = CommonMethods.SessionInfo.UserName
                            };
                            listKnittingSpareReceiveDetails.Add(spareReceiveDetails);
                        }
                    }

                    foreach (var item in listKnittingSpareReceiveDetails)
                    {
                        unitOfWork.GenericRepositories<KnittingSpareReceiveDetails>().Insert(item);
                    }
                    unitOfWork.Save();



                    foreach (var item in listKnittingSpareReceiveDetails)
                    {
                        partManager.UpdateSpareSummaryForSpareReceive(item.KnittingSpareId, item.SpareBrandId, int.Parse(ddlSuppliers.SelectedValue), item.SupplierCode, item.SpareQuantity, item.SpareUnitPrice.ToString(), CommonMethods.SessionInfo.UserName, DateTime.Now);
                    }


                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                    //Response.Redirect("ViewKnittingMachineSpareReceives.aspx");
                    SetInitialRowCount();
                    //Server.Transfer("ViewKnittingMachineSpareReceives.aspx");
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }

        }


        protected void ddlKnittingSpareTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlKnittingSpareTypes = (DropDownList)sender;
            DropDownList ddlMachineBrands = (DropDownList)((RepeaterItem)ddlKnittingSpareTypes.NamingContainer).FindControl("ddlMachineBrands");
            DropDownList ddlPartNames = (DropDownList)((RepeaterItem)ddlKnittingSpareTypes.NamingContainer).FindControl("ddlSpareNames");

            DropDownList ddlSpareBrands = (DropDownList)((RepeaterItem)ddlKnittingSpareTypes.NamingContainer).FindControl("ddlSpareBrands");


            if (ddlKnittingSpareTypes.SelectedValue != "" && ddlMachineBrands.SelectedValue != "")
            {
                ddlPartNames.Items.Clear();
                LoadSpareNames(ddlPartNames, int.Parse(ddlKnittingSpareTypes.SelectedValue), int.Parse(ddlMachineBrands.SelectedValue));
            }
            else
            {
                ddlPartNames.Items.Clear();
            }


            if (ddlKnittingSpareTypes.SelectedValue != "")
            {
                ddlSpareBrands.Items.Clear();
                LoadSpareBrands(ddlSpareBrands, int.Parse(ddlKnittingSpareTypes.SelectedValue));
            }
            else
            {
                ddlSpareBrands.Items.Clear();
            }


        }

        protected void ddlMachineBrands_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlMachineBrands = (DropDownList)sender;
            DropDownList ddlKnittingSpareTypes = (DropDownList)((RepeaterItem)ddlMachineBrands.NamingContainer).FindControl("ddlKnittingSpareTypes");
            DropDownList ddlPartNames = (DropDownList)((RepeaterItem)ddlMachineBrands.NamingContainer).FindControl("ddlSpareNames");

            if (ddlMachineBrands.SelectedValue != "" && ddlKnittingSpareTypes.SelectedValue != "")
            {
                ddlPartNames.Items.Clear();
                LoadSpareNames(ddlPartNames, int.Parse(ddlKnittingSpareTypes.SelectedValue), int.Parse(ddlMachineBrands.SelectedValue));
            }
            else
            {
                ddlPartNames.Items.Clear();
            }
        }




    }
}


