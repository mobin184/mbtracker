﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Knitting_Parts;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Knitting_Spares
{
    public partial class RequestKnittingMachineSpares : System.Web.UI.Page
    {

        PartsManager partManager = new PartsManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                CommonMethods.LoadKnittingUnitDropdown(ddlKnittingUnits, 1, 0);
                //SetInitialRowCount();
            }
        }


        protected void ddlKnittingUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            rptKnittingSpareRequestEntryInfo.DataSource = null;
            rptKnittingSpareRequestEntryInfo.DataBind();
            divKnittingSpareSelection.Visible = false;

            if (ddlKnittingUnits.SelectedValue != "")
            {
                divKnittingSpareSelection.Visible = true;
                SetInitialRowCount();
            }
            else
            {
                divKnittingSpareSelection.Visible = false;
            }
        }

        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;


            dt.Columns.Add(new DataColumn("MachineBrandId", typeof(string)));
            dt.Columns.Add(new DataColumn("MachineId", typeof(string)));
            dt.Columns.Add(new DataColumn("SpareTypeId", typeof(string)));
            dt.Columns.Add(new DataColumn("SpareId", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("RequestReasonId", typeof(string)));
            dt.Columns.Add(new DataColumn("Remarks", typeof(string)));

            dr = dt.NewRow();


            dr["MachineBrandId"] = string.Empty;
            dr["MachineId"] = string.Empty;
            dr["SpareTypeId"] = string.Empty;
            dr["SpareId"] = string.Empty;
            dr["Quantity"] = string.Empty;
            dr["RequestReasonId"] = string.Empty;
            dr["Remarks"] = string.Empty;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptKnittingSpareRequestEntryInfo.DataSource = dt;
            rptKnittingSpareRequestEntryInfo.DataBind();
        }


        protected void rptKnittingSpareRequestEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                var ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                LoadMachineBrandsByUnit(ddlMachineBrands, int.Parse(ddlKnittingUnits.SelectedValue));


                var ddlKnittingSpareTypes = (DropDownList)e.Item.FindControl("ddlKnittingSpareTypes");
                LoadSpareTypesDropdown(ddlKnittingSpareTypes);

                var ddlRequestReasons = (DropDownList)e.Item.FindControl("ddlRequestReasons");
                LoadSpareChangeReasons(ddlRequestReasons);

                //var ddlPartNames = (DropDownList)e.Item.FindControl("ddlPartNames");
                //LoadPartNames(ddlPartNames, int.Parse(ddlMachineBrands.SelectedValue), int.Parse(ddlKnittingSpareTypes.SelectedValue));

            }
        }


        private void LoadMachineBrandsByUnit(DropDownList ddl, int unitId)
        {
            string sql = $"Exec usp_GetAllMachineBrandByUnitId {unitId}";
            var dt = unitOfWork.GetDataTableFromSql(sql);
            DataRow dr = dt.NewRow();
            dr["MachineBrandId"] = "";
            dr["MachineBrand"] = "--Select--";
            dt.Rows.InsertAt(dr, 0);

            ddl.DataTextField = "MachineBrand";
            ddl.DataValueField = "MachineBrandId";
            ddl.DataSource = dt;
            ddl.DataBind();
        }


        private void LoadSpareTypesDropdown(DropDownList ddl)
        {
            CommonMethods.LoadPartTypeDropdown(ddl, 1, 0);
        }


        private void LoadSpareChangeReasons(DropDownList ddl)
        {
            CommonMethods.LoadWhyNeedleChangeDropdown(ddl, 1, 0);
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            this.AddNewRowToGrid();
        }

        private void AddNewRowToGrid()
        {

            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
   
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {

                        DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
                        DropDownList ddlMachineNames = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlMachineNames");
                        DropDownList ddlKnittingSpareTypes = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlKnittingSpareTypes");
                        DropDownList ddlSpareNames = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlSpareNames");

                        TextBox tbxQuantityRequested = (TextBox)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("tbxQuantityRequested");
                        DropDownList ddlRequestReasons = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlRequestReasons");
                        TextBox tbxRemarks = (TextBox)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("tbxRemarks");



                        dtCurrentTable.Rows[rowIndex]["MachineBrandId"] = (ddlMachineBrands.SelectedValue == "" ? "0" : ddlMachineBrands.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["MachineId"] = (ddlMachineNames.SelectedValue == "" ? "0" : ddlMachineNames.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["SpareTypeId"] = (ddlKnittingSpareTypes.SelectedValue == "" ? "0" : ddlKnittingSpareTypes.SelectedValue);

                        dtCurrentTable.Rows[rowIndex]["SpareId"] = (ddlSpareNames.SelectedValue == "" ? "0" : ddlSpareNames.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["Quantity"] = tbxQuantityRequested.Text;

                        dtCurrentTable.Rows[rowIndex]["RequestReasonId"] = (ddlRequestReasons.SelectedValue == "" ? "0" : ddlRequestReasons.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["Remarks"] = tbxRemarks.Text;



                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["MachineBrandId"] = string.Empty;
                            drCurrentRow["MachineId"] = string.Empty;
                            drCurrentRow["SpareTypeId"] = string.Empty;
                            drCurrentRow["SpareId"] = string.Empty;
                            drCurrentRow["Quantity"] = string.Empty;
                            drCurrentRow["RequestReasonId"] = string.Empty;
                            drCurrentRow["Remarks"] = string.Empty;

                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptKnittingSpareRequestEntryInfo.DataSource = dtCurrentTable;
                    this.rptKnittingSpareRequestEntryInfo.DataBind();
                }
            }
            this.SetPreviousData();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
                        DropDownList ddlMachineNames = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlMachineNames");
                        DropDownList ddlKnittingSpareTypes = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlKnittingSpareTypes");
                        DropDownList ddlSpareNames = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlSpareNames");

                        TextBox tbxQuantityRequested = (TextBox)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("tbxQuantityRequested");
                        DropDownList ddlRequestReasons = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlRequestReasons");
                        TextBox tbxRemarks = (TextBox)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("tbxRemarks");


                        LoadMachineBrandsByUnit(ddlMachineBrands, int.Parse(ddlKnittingUnits.SelectedValue));
                        ddlMachineBrands.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrands, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["MachineBrandId"].ToString()) ? "0" : dt.Rows[i]["MachineBrandId"].ToString())));


                        if (ddlMachineBrands.SelectedValue != "" && ddlKnittingUnits.SelectedValue != "")
                        {
                            ddlMachineNames.Items.Clear();
                            LoadMachinesByBrandAndUnit(ddlMachineNames, int.Parse(ddlMachineBrands.SelectedValue), int.Parse(ddlKnittingUnits.SelectedValue));
                            ddlMachineNames.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineNames, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["MachineId"].ToString()) ? "0" : dt.Rows[i]["MachineId"].ToString())));
                        }
                        else
                        {
                            ddlMachineNames.Items.Clear();
                        }

                        LoadSpareTypesDropdown(ddlKnittingSpareTypes);
                        ddlKnittingSpareTypes.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnittingSpareTypes, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["SpareTypeId"].ToString()) ? "0" : dt.Rows[i]["SpareTypeId"].ToString())));


                        if (ddlKnittingSpareTypes.SelectedValue != "")
                        {
                            this.LoadSpareNames(ddlSpareNames, int.Parse(ddlKnittingSpareTypes.SelectedValue), int.Parse(ddlMachineBrands.SelectedValue));
                            ddlSpareNames.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSpareNames, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["SpareId"].ToString()) ? "0" : dt.Rows[i]["SpareId"].ToString())));

                        }

                        LoadSpareChangeReasons(ddlRequestReasons);
                        ddlRequestReasons.SelectedIndex = CommonMethods.MatchDropDownItem(ddlRequestReasons, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["RequestReasonId"].ToString()) ? "0" : dt.Rows[i]["RequestReasonId"].ToString())));

                        tbxQuantityRequested.Text = dt.Rows[i]["Quantity"].ToString();
                        tbxRemarks.Text = dt.Rows[i]["Remarks"].ToString();
  

                        rowIndex++;
                    }
                }
            }
        }

        protected void ddlKnittingSpareTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlKnittingSpareTypes = (DropDownList)sender;
            DropDownList ddlMachineBrands = (DropDownList)((RepeaterItem)ddlKnittingSpareTypes.NamingContainer).FindControl("ddlMachineBrands");
            DropDownList ddlSpareNames = (DropDownList)((RepeaterItem)ddlKnittingSpareTypes.NamingContainer).FindControl("ddlSpareNames");

            if (ddlKnittingSpareTypes.SelectedValue != "" && ddlMachineBrands.SelectedValue != "")
            {
                ddlSpareNames.Items.Clear();
                LoadSpareNames(ddlSpareNames, int.Parse(ddlKnittingSpareTypes.SelectedValue), int.Parse(ddlMachineBrands.SelectedValue));
            }
            else
            {
                ddlSpareNames.Items.Clear();
            }
        }

        protected void ddlMachineBrands_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlMachineBrands = (DropDownList)sender;
            DropDownList ddlMachineNames = (DropDownList)((RepeaterItem)ddlMachineBrands.NamingContainer).FindControl("ddlMachineNames");

            


            DropDownList ddlKnittingSpareTypes = (DropDownList)((RepeaterItem)ddlMachineBrands.NamingContainer).FindControl("ddlKnittingSpareTypes");
            DropDownList ddlSpareNames = (DropDownList)((RepeaterItem)ddlMachineBrands.NamingContainer).FindControl("ddlSpareNames");


            if (ddlMachineBrands.SelectedValue != "" && ddlKnittingUnits.SelectedValue != "" )
            {
                ddlMachineNames.Items.Clear();
                LoadMachinesByBrandAndUnit(ddlMachineNames, int.Parse(ddlMachineBrands.SelectedValue), int.Parse(ddlKnittingUnits.SelectedValue));

                ddlSpareNames.Items.Clear();
                if (ddlKnittingSpareTypes.SelectedValue != "")
                {
                    LoadSpareNames(ddlSpareNames, int.Parse(ddlKnittingSpareTypes.SelectedValue), int.Parse(ddlMachineBrands.SelectedValue));
                }
            }
            else
            {
                ddlMachineNames.Items.Clear();
                ddlSpareNames.Items.Clear();
            }
        }

        private void LoadSpareNames(DropDownList ddl, int spareTypeId, int machineBrandId)
        {
            CommonMethods.LoadSpareNamesDropdown(ddl, 5, 0, spareTypeId, machineBrandId);
        }


        private void LoadMachinesByBrandAndUnit(DropDownList ddl, int machineBrandId, int knittingUnitId)
        {
            CommonMethods.LoadKnittingMachinesDropdown(ddl, 3, 0, machineBrandId, knittingUnitId);
        }


        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {

            if (ddlKnittingUnits.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a knitting unit.')", true);
            }
            else
            {
                try
                {

                    List<KnittingSpareRequestDetails> listKnittingSpareRequestDetails = new List<KnittingSpareRequestDetails>();

                    var ksr = new KnittingSpareRequests()
                    {
                        KnittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue),
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };

                    unitOfWork.GenericRepositories<KnittingSpareRequests>().Insert(ksr);
                    unitOfWork.Save();

                    for (int rowIndex = 0; rowIndex < rptKnittingSpareRequestEntryInfo.Items.Count; rowIndex++)
                    {

                        DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
                        DropDownList ddlMachineNames = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlMachineNames");
                        DropDownList ddlKnittingSpareTypes = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlKnittingSpareTypes");
                        DropDownList ddlSpareNames = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlSpareNames");

                        TextBox tbxQuantityRequested = (TextBox)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("tbxQuantityRequested");
                        DropDownList ddlRequestReasons = (DropDownList)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("ddlRequestReasons");
                        TextBox tbxRemarks = (TextBox)this.rptKnittingSpareRequestEntryInfo.Items[rowIndex].FindControl("tbxRemarks");



                        if (ddlMachineBrands.SelectedIndex != 0 && ddlMachineNames.SelectedIndex != 0 && ddlKnittingSpareTypes.SelectedIndex != 0 && ddlSpareNames.SelectedIndex != 0 && !string.IsNullOrEmpty(tbxQuantityRequested.Text) && ddlRequestReasons.SelectedIndex != 0)
                        {
                            var spareRequestDetails = new KnittingSpareRequestDetails()
                            {
                                KnittingSpareRequestId = ksr.Id,
                                KnittingMachineBrandId = int.Parse(ddlMachineBrands.SelectedValue),
                                KnittingMachineId = int.Parse(ddlMachineNames.SelectedValue),
                                KnittingSpareTypeId = int.Parse(ddlKnittingSpareTypes.SelectedValue),
                                KnittingSpareId = int.Parse(ddlSpareNames.SelectedValue),
                                QuantityRequested = int.Parse(tbxQuantityRequested.Text),
                                ReasonForRequestId = int.Parse(ddlRequestReasons.SelectedValue),
                                Remarks = tbxRemarks.Text.ToString(),
                                Status = 0,

                                CreateDate = DateTime.Now,
                                CreatedBy = CommonMethods.SessionInfo.UserName
                            };
                            listKnittingSpareRequestDetails.Add(spareRequestDetails);
                        }
                    }


                    foreach (var item in listKnittingSpareRequestDetails)
                    {
                        unitOfWork.GenericRepositories<KnittingSpareRequestDetails>().Insert(item);
                    }
                    unitOfWork.Save();


                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
        }

        //protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        //{

        //    //if (ddlSuppliers.SelectedValue == "")
        //    //{
        //    //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a supplier.')", true);
        //    //}
        //    //else if (String.IsNullOrEmpty(tbxRequistionNumber.Text))
        //    //{
        //    //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter requisition number.')", true);
        //    //}
        //    //else if (String.IsNullOrEmpty(tbxInvoiceNumber.Text))
        //    //{
        //    //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter invoice/bill number.')", true);
        //    //}
        //    //else if (String.IsNullOrEmpty(tbxChalanNumber.Text))
        //    //{
        //    //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter chalan number.')", true);
        //    //}
        //    //else
        //    //{
        //    //    try
        //    //    {

        //    //        var receiveSpare = unitOfWork.GenericRepositories<KnittingSpareReceives>().GetByID(SpareReceiveId);

        //    //        receiveSpare.ReceiveDate = DateTime.Parse(tbxDateReceived.Text);
        //    //        receiveSpare.SupplierId = int.Parse(ddlSuppliers.SelectedValue);
        //    //        receiveSpare.RequisitionNumber = tbxRequistionNumber.Text;
        //    //        receiveSpare.InvoiceNumber = tbxInvoiceNumber.Text;
        //    //        receiveSpare.ChalanNumber = tbxChalanNumber.Text;
        //    //        receiveSpare.OtherRefNumber = tbxOtherRefNumber.Text;
        //    //        receiveSpare.UpdateDate = DateTime.Now;
        //    //        receiveSpare.UpdatedBy = CommonMethods.SessionInfo.UserName;

        //    //        unitOfWork.GenericRepositories<KnittingSpareReceives>().Update(receiveSpare);
        //    //        unitOfWork.Save();


        //    //        partManager.DeleteSpareReceiveDetails(SpareReceiveId);

        //    //        List<KnittingSpareReceiveDetails> listKnittingSpareReceiveDetails = new List<KnittingSpareReceiveDetails>();

        //    //        for (int rowIndex = 0; rowIndex < rptKnittingPartsReceiveEntryInfo.Items.Count; rowIndex++)
        //    //        {


        //    //            DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
        //    //            DropDownList ddlKnittingSpareTypes = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlKnittingSpareTypes");
        //    //            DropDownList ddlSpareNames = (DropDownList)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("ddlSpareNames");

        //    //            TextBox tbxSupplierCode = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxSupplierCode");
        //    //            TextBox tbxSpareDescription = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxSpareDescription");

        //    //            TextBox tbxQuantity = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxQuantity");
        //    //            TextBox tbxUnitPrice = (TextBox)this.rptKnittingPartsReceiveEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");


        //    //            if (ddlKnittingSpareTypes.SelectedIndex != 0 && ddlSpareNames.SelectedIndex != 0 && !string.IsNullOrEmpty(tbxQuantity.Text) && !string.IsNullOrEmpty(tbxUnitPrice.Text))
        //    //            {
        //    //                var spareReceiveDetails = new KnittingSpareReceiveDetails()
        //    //                {
        //    //                    KnittingSpareReceiveId = SpareReceiveId,
        //    //                    KnittingSpareId = int.Parse(ddlSpareNames.SelectedValue),
        //    //                    SupplierCode = tbxSupplierCode.Text,
        //    //                    SpareDescription = tbxSpareDescription.Text,
        //    //                    SpareQuantity = int.Parse(tbxQuantity.Text),
        //    //                    SpareUnitPrice = decimal.Parse(tbxUnitPrice.Text),
        //    //                    CreateDate = DateTime.Now,
        //    //                    CreatedBy = CommonMethods.SessionInfo.UserName
        //    //                };
        //    //                listKnittingSpareReceiveDetails.Add(spareReceiveDetails);
        //    //            }
        //    //        }

        //    //        foreach (var item in listKnittingSpareReceiveDetails)
        //    //        {
        //    //            unitOfWork.GenericRepositories<KnittingSpareReceiveDetails>().Insert(item);
        //    //        }
        //    //        unitOfWork.Save();
        //    //        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
        //    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
        //    //        //Response.Redirect("ViewKnittingMachineSpareReceives.aspx");
        //    //    }
        //    //    catch (Exception ex)
        //    //    {
        //    //        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
        //    //    }
        //    //}

        //}

    }
}