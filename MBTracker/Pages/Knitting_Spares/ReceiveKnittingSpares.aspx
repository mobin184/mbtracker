﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ReceiveKnittingSpares.aspx.cs" Inherits="MBTracker.Pages.Knitting_Parts.ReceiveKnittingSpares" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid">
        <div class="span6">

            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblSpareReceiveEntry" Text="Receive Knitting Spares:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row-fluid" runat="server">
                        <div class="form-horizontal">

                            <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 15px">
                                <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field
                            </div>

                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <span style="font-weight: 700; color: #CC0000">*</span>
                                        <asp:Label ID="lblDateReceived" runat="server" Text="Date Received:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxDateReceived" runat="server" TextMode="Date" Width="100%"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <span style="font-weight: 700; color: #CC0000">*</span>
                                        <asp:Label ID="Label2" runat="server" Text="Select Supplier:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlSuppliers" runat="server" Width="100%"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <span style="font-weight: 700; color: #CC0000">*</span>
                                        <asp:Label ID="lblRequisitionNumber" runat="server" Text="Requisiton Number:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxRequistionNumber" runat="server" PlaceHoder="Requisition Number" Width="100%" type="text"></asp:TextBox>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                        <asp:Label ID="lblInvoiceNumber" runat="server" Text="Invoice Number:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxInvoiceNumber" runat="server" PlaceHoder="Invoice Number" Width="100%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                        <asp:Label ID="lblChalanNumber" runat="server" Text="Chalan Number:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxChalanNumber" runat="server" PlaceHoder="Chalan Number" Width="100%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblOtherRef" runat="server" Text="Other Reference(s):"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxOtherRefNumber" runat="server" PlaceHoder="Other referece" Width="100%"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>

        </div>
    </div>



    <div class="row-fluid">

        <div class="span12">

            <div class="widget">

                <div class="widget-body">
                    <div class="row-fluid" runat="server">
                        <div class="form-horizontal">
                            <div id="dt_example" class="example_alt_pagination">


                                <label for="inputOrder" class="control-label" style="text-align: left; height: 50px; width: 100%">
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                    <asp:Label ID="lblMessage" runat="server" Text="Enter Spare Information:" Font-Bold="true"></asp:Label></label>

                                <asp:Repeater ID="rptKnittingPartsReceiveEntryInfo" runat="server" OnItemDataBound="rptKnittingPartsReceiveEntryInfo_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>

                                                    <th>
                                                        <asp:Label ID="lblSparesType" runat="server" Text="Spares Type"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblMachineBrand" runat="server" Text="Machine Brand"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblSpareName" runat="server" Text="Spare Name"></asp:Label></th>
                                                     <th>
                                                        <asp:Label ID="lblSpareBrand" runat="server" Text="Spare Brand"></asp:Label></th>

                                                    <th>
                                                        <asp:Label ID="lblSupplierCode" runat="server" Text="Item Code"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblSpareDescription" runat="server" Text="Spare Description"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblQuantity" runat="server" Text="Quantity"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label></th>


                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>

                                            <td style="width: 10%">
                                                <asp:Label ID="lblKnittingSpareReceiveDetailsId" runat="server" Text="" Visible="false"></asp:Label>

                                                <asp:DropDownList ID="ddlKnittingSpareTypes" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlKnittingSpareTypes_SelectedIndexChanged"></asp:DropDownList></td>

                                            <td style="width: 12%">
                                                <asp:DropDownList ID="ddlMachineBrands" runat="server" AutoPostBack="true" CssClass="form-control" Style="min-width: 125px" OnSelectedIndexChanged="ddlMachineBrands_SelectedIndexChanged"></asp:DropDownList>
                                            </td>

                                            <td style="width: 15%">
                                                <asp:DropDownList ID="ddlSpareNames" runat="server" CssClass="form-control"></asp:DropDownList></td>
                                            
                                             <td style="width: 15%">
                                                <asp:DropDownList ID="ddlSpareBrands" runat="server" CssClass="form-control"></asp:DropDownList></td>
                                            
                                            <td style="width: 15%">
                                                <asp:TextBox ID="tbxSupplierCode" Width="100%" runat="server"></asp:TextBox></td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="tbxSpareDescription" Width="100%" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                            <td style="width: 7%">
                                                <asp:TextBox ID="tbxQuantity" Width="100%" runat="server" TextMode="Number"></asp:TextBox></td>
                                            <td style="width: 8%">
                                                <asp:TextBox ID="tbxUnitPrice" Width="100%" runat="server"></asp:TextBox></td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                 </table>
                                            <div class="pull-right" style="margin-right: 0px; padding-top: 10px">
                                                <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="71px" Text="Add Row" OnClick="btnAddRow_Click" />
                                            </div>
                                    </FooterTemplate>
                                </asp:Repeater>

                            </div>
                            <div class="clearfix"></div>

                        </div>
                    </div>

                </div>

            </div>
            <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
            <asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>


        </div>

    </div>


</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
