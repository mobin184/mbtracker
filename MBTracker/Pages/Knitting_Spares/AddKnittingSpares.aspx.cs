﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Knitting_Parts
{
    public partial class AddKnittingSpares : System.Web.UI.Page
    {

        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                //if (Request.QueryString["unitId"] != null && Request.QueryString["knittingDate"] != null)
                //{
                //    UnitId = int.Parse(Tools.UrlDecode(Request.QueryString["unitId"]));
                //    KnittingDate = DateTime.Parse(Tools.UrlDecode(Request.QueryString["knittingDate"]));
                //    ShiftId = int.Parse(Tools.UrlDecode(Request.QueryString["shiftId"]));
                //    lblProductionEntry.Text = "Update Daily Knitting Machine Status:";
                //}

                PopulateEntryInfo();
            }

        }


        private void PopulateEntryInfo()
        {

            SetInitialRowCount();
           
        }


        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("KnittingPartTypeId", typeof(string)));
            dt.Columns.Add(new DataColumn("MachineBrandId", typeof(string)));
            dt.Columns.Add(new DataColumn("FixedOrMultiId", typeof(string)));
            dt.Columns.Add(new DataColumn("PartCode", typeof(string)));
            dt.Columns.Add(new DataColumn("PartName", typeof(string)));
            //dt.Columns.Add(new DataColumn("IntItemCode", typeof(string)));

            dr = dt.NewRow();
            dr["KnittingPartTypeId"] = string.Empty;
            dr["MachineBrandId"] = string.Empty;
            dr["FixedOrMultiId"] = string.Empty;
            dr["PartCode"] = string.Empty;
            dr["PartName"] = string.Empty;
            //dr["IntItemCode"] = string.Empty;
            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptKnittingPartsEntryInfo.DataSource = dt;
            rptKnittingPartsEntryInfo.DataBind();
        }



        protected void rptKnittingPartsEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
               
                var ddlKnittingSpareTypes = (DropDownList)e.Item.FindControl("ddlKnittingSpareTypes");
                LoadPartTypesDropdown(ddlKnittingSpareTypes);

                var ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                LoadMachineBrands(ddlMachineBrands);

            }

        }


        private void LoadPartTypesDropdown(DropDownList ddl)
        {
            CommonMethods.LoadPartTypeDropdown(ddl, 1, 0);
        }

        private void LoadMachineBrands(DropDownList ddl)
        {
            CommonMethods.LoadMachineBrandDropdown(ddl, 1, 0);
        }



        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            this.AddNewRowToGrid();
        }


        private void AddNewRowToGrid()
        {

            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {
                        DropDownList ddlKnittingSpareTypes = (DropDownList)this.rptKnittingPartsEntryInfo.Items[rowIndex].FindControl("ddlKnittingSpareTypes");
                        DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingPartsEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
                        DropDownList ddlFixedOrMulti = (DropDownList)this.rptKnittingPartsEntryInfo.Items[rowIndex].FindControl("ddlFixedOrMulti");


                        TextBox tbxSpareCode = (TextBox)this.rptKnittingPartsEntryInfo.Items[rowIndex].FindControl("tbxSpareCode");
                        TextBox tbxSpareName = (TextBox)this.rptKnittingPartsEntryInfo.Items[rowIndex].FindControl("tbxSpareName");
                        //TextBox tbxIntItemCode = (TextBox)this.rptKnittingPartsEntryInfo.Items[rowIndex].FindControl("tbxIntItemCode");


                        dtCurrentTable.Rows[rowIndex]["KnittingPartTypeId"] = (ddlKnittingSpareTypes.SelectedValue == "" ? "0" : ddlKnittingSpareTypes.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["MachineBrandId"] = (ddlMachineBrands.SelectedValue == "" ? "0" : ddlMachineBrands.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["FixedOrMultiId"] = (ddlFixedOrMulti.SelectedValue == "" ? "0" : ddlFixedOrMulti.SelectedValue);


                        dtCurrentTable.Rows[rowIndex]["PartCode"] = tbxSpareCode.Text;
                        dtCurrentTable.Rows[rowIndex]["PartName"] = tbxSpareName.Text;
                        //dtCurrentTable.Rows[rowIndex]["IntItemCode"] = tbxIntItemCode.Text;



                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["KnittingPartTypeId"] = string.Empty;
                            drCurrentRow["MachineBrandId"] = string.Empty;
                            drCurrentRow["FixedOrMultiId"] = string.Empty;
                            drCurrentRow["PartCode"] = string.Empty;
                            drCurrentRow["PartName"] = string.Empty;
                            //drCurrentRow["IntItemCode"] = string.Empty;

                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;
 
                    this.rptKnittingPartsEntryInfo.DataSource = dtCurrentTable;
                    this.rptKnittingPartsEntryInfo.DataBind();
                }
            }
            this.SetPreviousData();
        }



        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {

                for (int i = 0; i < rptKnittingPartsEntryInfo.Items.Count; i++)
                {


                    DropDownList ddlKnittingSpareTypes = (DropDownList)this.rptKnittingPartsEntryInfo.Items[i].FindControl("ddlKnittingSpareTypes");
                    DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingPartsEntryInfo.Items[i].FindControl("ddlMachineBrands");
                    DropDownList ddlFixedOrMulti = (DropDownList)this.rptKnittingPartsEntryInfo.Items[i].FindControl("ddlFixedOrMulti");


                    TextBox tbxSpareCode = (TextBox)this.rptKnittingPartsEntryInfo.Items[i].FindControl("tbxSpareCode");
                    TextBox tbxSpareName = (TextBox)this.rptKnittingPartsEntryInfo.Items[i].FindControl("tbxSpareName");
                    //TextBox tbxIntItemCode = (TextBox)this.rptKnittingPartsEntryInfo.Items[i].FindControl("tbxIntItemCode");



                    if (ddlKnittingSpareTypes.SelectedIndex != 0 && ddlMachineBrands.SelectedIndex != 0 && !string.IsNullOrEmpty(tbxSpareCode.Text) && !string.IsNullOrEmpty(tbxSpareName.Text))
                    {
                        var knittingSpare = new KnittingSpares()
                        {
                            KnittingSpareTypeId = int.Parse(ddlKnittingSpareTypes.SelectedValue),
                            MachineBrandId = int.Parse(ddlMachineBrands.SelectedValue),
                            FixedOrMulti = int.Parse(ddlFixedOrMulti.SelectedValue),
                            SpareCode = tbxSpareCode.Text.ToString(),
                            SpareName = tbxSpareName.Text.ToString(),
                            CreatedBy = CommonMethods.SessionInfo.UserName,
                            CreateDate = DateTime.Now
                        };
                        unitOfWork.GenericRepositories<KnittingSpares>().Insert(knittingSpare);
                    }
                }

                unitOfWork.Save();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "')", true);
            }
        }


        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    var unitId = int.Parse(((DropDownList)rptGeneralInfo.Items[0].FindControl("ddlUserUnits")).SelectedValue);
            //    var stShift = ((DropDownList)rptGeneralInfo.Items[0].FindControl("ddlShift")).SelectedValue;

            //    var stproductionDate = ((TextBox)rptGeneralInfo.Items[0].FindControl("tbxProductionDate")).Text;
            //    if (!string.IsNullOrEmpty(stShift))
            //    {
            //        var shiftId = int.Parse(stShift);
            //        if (!string.IsNullOrEmpty(stproductionDate))
            //        {
            //            var prductionDate = DateTime.Parse(stproductionDate);
            //            if (prductionDate <= DateTime.Today)
            //            {
            //                var flag = true;
            //                for (int i = 0; i < rptEntryInfo.Items.Count; i++)
            //                {
            //                    var lblDailyKnittingMachineStatusId = (Label)rptEntryInfo.Items[i].FindControl("lblDailyKnittingMachineStatusId");
            //                    var lblMachineBrandId = (Label)rptEntryInfo.Items[i].FindControl("lblMachineBrandId");
            //                    var tbxNoOfRunningMachine = (TextBox)rptEntryInfo.Items[i].FindControl("tbxNoOfRunningMachine");
            //                    var tbxUsedForSample = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForSample");
            //                    var tbxLongTimeStoped = (TextBox)rptEntryInfo.Items[i].FindControl("tbxLongTimeStoped");
            //                    var tbxTemporaryStoped = (TextBox)rptEntryInfo.Items[i].FindControl("tbxTemporaryStoped");
            //                    var tbxIdleMachine = (TextBox)rptEntryInfo.Items[i].FindControl("tbxIdleMachine");
            //                    var tbxUsedForSetup = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForSetup");

            //                    var tbxUsedForOtherWorks = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForOtherWorks");
            //                    var lblNumberOfMachines = (Label)rptEntryInfo.Items[i].FindControl("lblNumberOfMachines");
            //                    var noOfMc = int.Parse(lblNumberOfMachines.Text);

            //                    var DailyKnittingMachineStatusId = string.IsNullOrEmpty(lblDailyKnittingMachineStatusId.Text) ? 0 : int.Parse(lblDailyKnittingMachineStatusId.Text);
            //                    var machineBrandId = string.IsNullOrEmpty(lblMachineBrandId.Text) ? 0 : int.Parse(lblMachineBrandId.Text);
            //                    var runningMachine = string.IsNullOrEmpty(tbxNoOfRunningMachine.Text) ? (int?)null : int.Parse(tbxNoOfRunningMachine.Text);
            //                    var forSample = string.IsNullOrEmpty(tbxUsedForSample.Text) ? (int?)null : int.Parse(tbxUsedForSample.Text);
            //                    var longTimeStop = string.IsNullOrEmpty(tbxLongTimeStoped.Text) ? (int?)null : int.Parse(tbxLongTimeStoped.Text);
            //                    var tempStop = string.IsNullOrEmpty(tbxTemporaryStoped.Text) ? (int?)null : int.Parse(tbxTemporaryStoped.Text);
            //                    var idleMachine = string.IsNullOrEmpty(tbxIdleMachine.Text) ? (int?)null : int.Parse(tbxIdleMachine.Text);
            //                    var usedForSetup = string.IsNullOrEmpty(tbxUsedForSetup.Text) ? (int?)null : int.Parse(tbxUsedForSetup.Text);
            //                    var otherWorks = string.IsNullOrEmpty(tbxUsedForOtherWorks.Text) ? (int?)null : int.Parse(tbxUsedForOtherWorks.Text);
            //                    int totalUsed = ((runningMachine ?? 0) + (forSample ?? 0) + (longTimeStop ?? 0) + (tempStop ?? 0) + (idleMachine ?? 0) + (usedForSetup ?? 0) + (otherWorks ?? 0));
            //                    if (noOfMc != totalUsed)
            //                    {
            //                        flag = false;
            //                    }
            //                    else
            //                    {

            //                        var info = unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().GetByID(DailyKnittingMachineStatusId);
            //                        if (info == null)
            //                        {


            //                            var dailyMachineStatus = new DailyKnittingMachineStatus()
            //                            {
            //                                ProductionDate = prductionDate,
            //                                KnittingUnitId = unitId,
            //                                ShiftId = shiftId,
            //                                MachineBrandId = machineBrandId,
            //                                NoOfRunningMachine = runningMachine,
            //                                UsedForSample = forSample,
            //                                LongTimeStop = longTimeStop,
            //                                TemporaryStop = tempStop,
            //                                IdleMachine = idleMachine,
            //                                //UsedForSetup = usedForSetup,
            //                                //UsedForOtherWorks = otherWorks,
            //                                CreatedBy = CommonMethods.SessionInfo.UserName,
            //                                CreateDate = DateTime.Now
            //                            };
            //                            unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().Insert(dailyMachineStatus);
            //                        }
            //                        else
            //                        {
            //                            info.MachineBrandId = machineBrandId;
            //                            info.NoOfRunningMachine = runningMachine;
            //                            info.UsedForSample = forSample;
            //                            info.LongTimeStop = longTimeStop;
            //                            info.TemporaryStop = tempStop;
            //                            info.IdleMachine = idleMachine;
            //                            //info.UsedForSetup = usedForSetup;
            //                            //info.UsedForOtherWorks = otherWorks;
            //                            info.UpdatedBy = CommonMethods.SessionInfo.UserName;
            //                            info.UpdateDate = DateTime.Now;
            //                            unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().Update(info);
            //                        }
            //                    }
            //                }

            //                if (flag)
            //                {
            //                    unitOfWork.Save();
            //                    unitOfWork.ExeccuteRawQyery($"EXEC usp_Insert_DailyMachineStatusSummary '{prductionDate.Date}',{unitId}");
            //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
            //                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewKnittingMachineStatus.aspx');", true);
            //                }
            //                else
            //                {
            //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Total no of machine and entered machine should be equal!')", true);
            //                }
            //            }
            //            else
            //            {
            //                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('You cant enter future date data!')", true);
            //            }
            //        }
            //        else
            //        {
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter production date!')", true);
            //        }
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select shift !')", true);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "')", true);
            //}

        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        DropDownList ddlKnittingSpareTypes = (DropDownList)this.rptKnittingPartsEntryInfo.Items[rowIndex].FindControl("ddlKnittingSpareTypes");
                        DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingPartsEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
                        DropDownList ddlFixedOrMulti = (DropDownList)this.rptKnittingPartsEntryInfo.Items[rowIndex].FindControl("ddlFixedOrMulti");


                        TextBox tbxSpareCode = (TextBox)this.rptKnittingPartsEntryInfo.Items[rowIndex].FindControl("tbxSpareCode");
                        TextBox tbxSpareName = (TextBox)this.rptKnittingPartsEntryInfo.Items[rowIndex].FindControl("tbxSpareName");
                        //TextBox tbxIntItemCode = (TextBox)this.rptKnittingPartsEntryInfo.Items[rowIndex].FindControl("tbxIntItemCode");



                        this.LoadPartTypesDropdown(ddlKnittingSpareTypes);
                        this.LoadMachineBrands(ddlMachineBrands);

                        ddlKnittingSpareTypes.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnittingSpareTypes, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["KnittingPartTypeId"].ToString()) ? "0" : dt.Rows[i]["KnittingPartTypeId"].ToString())));
                        ddlMachineBrands.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrands, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["MachineBrandId"].ToString()) ? "0" : dt.Rows[i]["MachineBrandId"].ToString())));

                        ddlFixedOrMulti.SelectedIndex = CommonMethods.MatchDropDownItem(ddlFixedOrMulti, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["FixedOrMultiId"].ToString()) ? "0" : dt.Rows[i]["FixedOrMultiId"].ToString())));

                        if (dt.Rows[i]["KnittingPartTypeId"].ToString() == "1")
                        {
                            ddlFixedOrMulti.Enabled = true;
                        }
                        else
                        {
                            ddlFixedOrMulti.Enabled = false;
                        }


                        tbxSpareCode.Text = dt.Rows[i]["PartCode"].ToString();
                        tbxSpareName.Text = dt.Rows[i]["PartName"].ToString();
                        //tbxIntItemCode.Text = dt.Rows[i]["IntItemCode"].ToString();

                        rowIndex++;
                    }
                }
            }
        }

        protected void ddlKnittingPartTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlKnittingSpareTypes = (DropDownList)sender;
            DropDownList ddlFixedOrMulti = (DropDownList)((RepeaterItem)ddlKnittingSpareTypes.NamingContainer).FindControl("ddlFixedOrMulti");

            if (ddlKnittingSpareTypes.SelectedIndex == 1)
            {
                ddlFixedOrMulti.Enabled = true;
            }
            else
            {
                ddlFixedOrMulti.Enabled = false;
            }
        }

    }
}