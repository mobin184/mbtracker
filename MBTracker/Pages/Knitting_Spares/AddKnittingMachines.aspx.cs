﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Knitting_Parts
{
    public partial class AddKnittingMachines : System.Web.UI.Page
    {

        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //if (Request.QueryString["unitId"] != null && Request.QueryString["knittingDate"] != null)
                //{
                //    UnitId = int.Parse(Tools.UrlDecode(Request.QueryString["unitId"]));
                //    KnittingDate = DateTime.Parse(Tools.UrlDecode(Request.QueryString["knittingDate"]));
                //    ShiftId = int.Parse(Tools.UrlDecode(Request.QueryString["shiftId"]));
                //    lblProductionEntry.Text = "Update Daily Knitting Machine Status:";
                //}

                PopulateEntryInfo();
            }

        }

        private void PopulateEntryInfo()
        {

            SetInitialRowCount();

        }


        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("MachineBrandId", typeof(string)));
            dt.Columns.Add(new DataColumn("KnittingUnitId", typeof(string)));
            dt.Columns.Add(new DataColumn("MachineCode", typeof(string)));
            dt.Columns.Add(new DataColumn("MachineName", typeof(string)));
            dt.Columns.Add(new DataColumn("NeedleCapacity", typeof(string)));

            dr = dt.NewRow();
            dr["MachineCode"] = string.Empty;
            dr["MachineName"] = string.Empty;
            dr["MachineBrandId"] = string.Empty;
            dr["KnittingUnitId"] = string.Empty;
            dr["NeedleCapacity"] = string.Empty;
            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptKnittingMachinesEntryInfo.DataSource = dt;
            rptKnittingMachinesEntryInfo.DataBind();
        }


        protected void rptKnittingMachinesEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                var ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                LoadMachineBrands(ddlMachineBrands);

                var ddlKnittingUnits = (DropDownList)e.Item.FindControl("ddlKnittingUnits");
                LoadKnittigUnitDropdown(ddlKnittingUnits);

            }

        }

        private void LoadMachineBrands(DropDownList ddl)
        {
            CommonMethods.LoadMachineBrandDropdown(ddl, 1, 0);
        }

        private void LoadKnittigUnitDropdown(DropDownList ddl)
        {
            CommonMethods.LoadKnittingUnitDropdown(ddl, 1, 0);
        }

       

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            this.AddNewRowToGrid();
        }

        private void AddNewRowToGrid()
        {

            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {


                        DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
                        DropDownList ddlKnittingUnits = (DropDownList)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("ddlKnittingUnits");
                        TextBox tbxMachineCode = (TextBox)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("tbxMachineCode");
                        TextBox tbxMachineName = (TextBox)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("tbxMachineName");
                        TextBox tbxNeedleCapacity = (TextBox)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("tbxNeedleCapacity");

                  
                        dtCurrentTable.Rows[rowIndex]["MachineBrandId"] = (ddlMachineBrands.SelectedValue == "" ? "0" : ddlMachineBrands.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["KnittingUnitId"] = (ddlKnittingUnits.SelectedValue == "" ? "0" : ddlKnittingUnits.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["MachineCode"] = tbxMachineCode.Text;
                        dtCurrentTable.Rows[rowIndex]["MachineName"] = tbxMachineName.Text;
                        dtCurrentTable.Rows[rowIndex]["NeedleCapacity"] = tbxNeedleCapacity.Text;


                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["MachineCode"] = string.Empty;
                            drCurrentRow["MachineName"] = string.Empty;
                            drCurrentRow["MachineBrandId"] = string.Empty;
                            drCurrentRow["KnittingUnitId"] = string.Empty;
                            drCurrentRow["NeedleCapacity"] = string.Empty;

                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptKnittingMachinesEntryInfo.DataSource = dtCurrentTable;
                    this.rptKnittingMachinesEntryInfo.DataBind();
                }
            }
            this.SetPreviousData();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("ddlMachineBrands");
                        DropDownList ddlKnittingUnits = (DropDownList)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("ddlKnittingUnits");
                        TextBox tbxMachineCode = (TextBox)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("tbxMachineCode");
                        TextBox tbxMachineName = (TextBox)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("tbxMachineName");
                        TextBox tbxNeedleCapacity = (TextBox)this.rptKnittingMachinesEntryInfo.Items[rowIndex].FindControl("tbxNeedleCapacity");


                        this.LoadMachineBrands(ddlMachineBrands);
                        this.LoadKnittigUnitDropdown(ddlKnittingUnits);


                       
                        ddlMachineBrands.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrands, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["MachineBrandId"].ToString()) ? "0" : dt.Rows[i]["MachineBrandId"].ToString())));
                        ddlKnittingUnits.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnittingUnits, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["KnittingUnitId"].ToString()) ? "0" : dt.Rows[i]["KnittingUnitId"].ToString())));

                        tbxMachineCode.Text = dt.Rows[i]["MachineCode"].ToString();
                        tbxMachineName.Text = dt.Rows[i]["MachineName"].ToString();
                        tbxNeedleCapacity.Text = dt.Rows[i]["NeedleCapacity"].ToString();

                        rowIndex++;
                    }
                }
            }
        }

        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {

                for (int i = 0; i < rptKnittingMachinesEntryInfo.Items.Count; i++)
                {


                    DropDownList ddlMachineBrands = (DropDownList)this.rptKnittingMachinesEntryInfo.Items[i].FindControl("ddlMachineBrands");
                    DropDownList ddlKnittingUnits = (DropDownList)this.rptKnittingMachinesEntryInfo.Items[i].FindControl("ddlKnittingUnits");
                    TextBox tbxMachineCode = (TextBox)this.rptKnittingMachinesEntryInfo.Items[i].FindControl("tbxMachineCode");
                    TextBox tbxMachineName = (TextBox)this.rptKnittingMachinesEntryInfo.Items[i].FindControl("tbxMachineName");
                    TextBox tbxNeedleCapacity = (TextBox)this.rptKnittingMachinesEntryInfo.Items[i].FindControl("tbxNeedleCapacity");


                    if (ddlMachineBrands.SelectedIndex != 0 && ddlKnittingUnits.SelectedIndex != 0 && !string.IsNullOrEmpty(tbxMachineCode.Text) && !string.IsNullOrEmpty(tbxMachineName.Text) && !string.IsNullOrEmpty(tbxNeedleCapacity.Text))
                    {
                        var knittingMachine = new KnittingMachines()
                        {
                            MachineCode = tbxMachineCode.Text.ToString(),
                            MachineName = tbxMachineName.Text.ToString(),
                            MachineBrandId = int.Parse(ddlMachineBrands.SelectedValue),
                            KnittingUnitId = int.Parse(ddlKnittingUnits.SelectedValue),
                            NeedleCapacity = int.Parse(tbxNeedleCapacity.Text),
                            CreatedBy = CommonMethods.SessionInfo.UserName,
                            CreateDate = DateTime.Now
                        };
                        unitOfWork.GenericRepositories<KnittingMachines>().Insert(knittingMachine);
                    }
                }

                unitOfWork.Save();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "')", true);
            }
        }


        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    var unitId = int.Parse(((DropDownList)rptGeneralInfo.Items[0].FindControl("ddlUserUnits")).SelectedValue);
            //    var stShift = ((DropDownList)rptGeneralInfo.Items[0].FindControl("ddlShift")).SelectedValue;

            //    var stproductionDate = ((TextBox)rptGeneralInfo.Items[0].FindControl("tbxProductionDate")).Text;
            //    if (!string.IsNullOrEmpty(stShift))
            //    {
            //        var shiftId = int.Parse(stShift);
            //        if (!string.IsNullOrEmpty(stproductionDate))
            //        {
            //            var prductionDate = DateTime.Parse(stproductionDate);
            //            if (prductionDate <= DateTime.Today)
            //            {
            //                var flag = true;
            //                for (int i = 0; i < rptEntryInfo.Items.Count; i++)
            //                {
            //                    var lblDailyKnittingMachineStatusId = (Label)rptEntryInfo.Items[i].FindControl("lblDailyKnittingMachineStatusId");
            //                    var lblMachineBrandId = (Label)rptEntryInfo.Items[i].FindControl("lblMachineBrandId");
            //                    var tbxNoOfRunningMachine = (TextBox)rptEntryInfo.Items[i].FindControl("tbxNoOfRunningMachine");
            //                    var tbxUsedForSample = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForSample");
            //                    var tbxLongTimeStoped = (TextBox)rptEntryInfo.Items[i].FindControl("tbxLongTimeStoped");
            //                    var tbxTemporaryStoped = (TextBox)rptEntryInfo.Items[i].FindControl("tbxTemporaryStoped");
            //                    var tbxIdleMachine = (TextBox)rptEntryInfo.Items[i].FindControl("tbxIdleMachine");
            //                    var tbxUsedForSetup = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForSetup");

            //                    var tbxUsedForOtherWorks = (TextBox)rptEntryInfo.Items[i].FindControl("tbxUsedForOtherWorks");
            //                    var lblNumberOfMachines = (Label)rptEntryInfo.Items[i].FindControl("lblNumberOfMachines");
            //                    var noOfMc = int.Parse(lblNumberOfMachines.Text);

            //                    var DailyKnittingMachineStatusId = string.IsNullOrEmpty(lblDailyKnittingMachineStatusId.Text) ? 0 : int.Parse(lblDailyKnittingMachineStatusId.Text);
            //                    var machineBrandId = string.IsNullOrEmpty(lblMachineBrandId.Text) ? 0 : int.Parse(lblMachineBrandId.Text);
            //                    var runningMachine = string.IsNullOrEmpty(tbxNoOfRunningMachine.Text) ? (int?)null : int.Parse(tbxNoOfRunningMachine.Text);
            //                    var forSample = string.IsNullOrEmpty(tbxUsedForSample.Text) ? (int?)null : int.Parse(tbxUsedForSample.Text);
            //                    var longTimeStop = string.IsNullOrEmpty(tbxLongTimeStoped.Text) ? (int?)null : int.Parse(tbxLongTimeStoped.Text);
            //                    var tempStop = string.IsNullOrEmpty(tbxTemporaryStoped.Text) ? (int?)null : int.Parse(tbxTemporaryStoped.Text);
            //                    var idleMachine = string.IsNullOrEmpty(tbxIdleMachine.Text) ? (int?)null : int.Parse(tbxIdleMachine.Text);
            //                    var usedForSetup = string.IsNullOrEmpty(tbxUsedForSetup.Text) ? (int?)null : int.Parse(tbxUsedForSetup.Text);
            //                    var otherWorks = string.IsNullOrEmpty(tbxUsedForOtherWorks.Text) ? (int?)null : int.Parse(tbxUsedForOtherWorks.Text);
            //                    int totalUsed = ((runningMachine ?? 0) + (forSample ?? 0) + (longTimeStop ?? 0) + (tempStop ?? 0) + (idleMachine ?? 0) + (usedForSetup ?? 0) + (otherWorks ?? 0));
            //                    if (noOfMc != totalUsed)
            //                    {
            //                        flag = false;
            //                    }
            //                    else
            //                    {

            //                        var info = unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().GetByID(DailyKnittingMachineStatusId);
            //                        if (info == null)
            //                        {


            //                            var dailyMachineStatus = new DailyKnittingMachineStatus()
            //                            {
            //                                ProductionDate = prductionDate,
            //                                KnittingUnitId = unitId,
            //                                ShiftId = shiftId,
            //                                MachineBrandId = machineBrandId,
            //                                NoOfRunningMachine = runningMachine,
            //                                UsedForSample = forSample,
            //                                LongTimeStop = longTimeStop,
            //                                TemporaryStop = tempStop,
            //                                IdleMachine = idleMachine,
            //                                //UsedForSetup = usedForSetup,
            //                                //UsedForOtherWorks = otherWorks,
            //                                CreatedBy = CommonMethods.SessionInfo.UserName,
            //                                CreateDate = DateTime.Now
            //                            };
            //                            unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().Insert(dailyMachineStatus);
            //                        }
            //                        else
            //                        {
            //                            info.MachineBrandId = machineBrandId;
            //                            info.NoOfRunningMachine = runningMachine;
            //                            info.UsedForSample = forSample;
            //                            info.LongTimeStop = longTimeStop;
            //                            info.TemporaryStop = tempStop;
            //                            info.IdleMachine = idleMachine;
            //                            //info.UsedForSetup = usedForSetup;
            //                            //info.UsedForOtherWorks = otherWorks;
            //                            info.UpdatedBy = CommonMethods.SessionInfo.UserName;
            //                            info.UpdateDate = DateTime.Now;
            //                            unitOfWork.GenericRepositories<DailyKnittingMachineStatus>().Update(info);
            //                        }
            //                    }
            //                }

            //                if (flag)
            //                {
            //                    unitOfWork.Save();
            //                    unitOfWork.ExeccuteRawQyery($"EXEC usp_Insert_DailyMachineStatusSummary '{prductionDate.Date}',{unitId}");
            //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
            //                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewKnittingMachineStatus.aspx');", true);
            //                }
            //                else
            //                {
            //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Total no of machine and entered machine should be equal!')", true);
            //                }
            //            }
            //            else
            //            {
            //                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('You cant enter future date data!')", true);
            //            }
            //        }
            //        else
            //        {
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter production date!')", true);
            //        }
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select shift !')", true);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "')", true);
            //}

        }

    }
}