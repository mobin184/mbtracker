﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Knitting_Parts;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Knitting_Parts
{
    public partial class ViewKnittingSpares : System.Web.UI.Page
    {

        PartsManager partManager = new PartsManager();
        UnitOfWork unitOfWork = new UnitOfWork();


        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                CommonMethods.LoadMachineBrandDropdown(ddlMachineBrands, 1, 0);
                CommonMethods.LoadPartTypeDropdown(ddlSpareTypes, 1, 0);

            }

        }


        protected void btnViewKnittingParts_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();


            int spareTypeId = 0;
            int machineBrandId = 0;

            if (ddlMachineBrands.SelectedValue != "")
            {
                machineBrandId = int.Parse(ddlMachineBrands.SelectedValue);
            }
            if (ddlSpareTypes.SelectedValue != "")
            {
                spareTypeId = int.Parse(ddlSpareTypes.SelectedValue);
            }

            dt = partManager.GetExistingKnittingSparesByTypeAndMachineBrand(spareTypeId, machineBrandId);

            if (dt.Rows.Count > 0)
            {
                rptKnittingParts.DataSource = dt;
                rptKnittingParts.DataBind();
                lblNoPartFound.Visible = false;
                pnlViewKnittingParts.Visible = true;
            }
            else
            {
                lblNoPartFound.Visible = true;
                pnlViewKnittingParts.Visible = false;
            }

        }


        //protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        //{
        //    int knittingMachinePartId = int.Parse(e.CommandArgument.ToString());
        //    Response.Redirect("AddKnittingSpares.aspx?knittingSpareId=" + Tools.UrlEncode(knittingMachinePartId + ""));
        //}

        protected void ddlKnittingPartTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlKnittingPartTypes = (DropDownList)sender;
            DropDownList ddlFixedOrMulti = (DropDownList)((RepeaterItem)ddlKnittingPartTypes.NamingContainer).FindControl("ddlFixedOrMulti");

            if (ddlKnittingPartTypes.SelectedIndex == 1)
            {
                ddlFixedOrMulti.Enabled = true;
            }
            else
            {
                ddlFixedOrMulti.Enabled = false;
            }
        }

        protected void rptKnittingParts_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {

                DropDownList ddlKnittingPartTypes = (DropDownList)e.Item.FindControl("ddlKnittingPartTypes");
                DropDownList ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                DropDownList ddlFixedOrMulti = (DropDownList)e.Item.FindControl("ddlFixedOrMulti");
                TextBox tbxIntItemCode = (TextBox)e.Item.FindControl("tbxIntItemCode");
                TextBox tbxPartName = (TextBox)e.Item.FindControl("tbxPartName");

                Label lblSpareTypeId = (Label)e.Item.FindControl("lblSpareTypeId");
                Label lblMachineBrandId = (Label)e.Item.FindControl("lblMachineBrandId");
                Label lblFixedOrMultiId = (Label)e.Item.FindControl("lblFixedOrMultiId");


                LoadPartTypesDropdown(ddlKnittingPartTypes);
                LoadMachineBrands(ddlMachineBrands);

                ddlKnittingPartTypes.SelectedIndex = CommonMethods.MatchDropDownItem(ddlKnittingPartTypes, string.IsNullOrEmpty(lblSpareTypeId.Text.ToString()) ? 0 : int.Parse(lblSpareTypeId.Text.ToString()));
                ddlMachineBrands.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrands, string.IsNullOrEmpty(lblMachineBrandId.Text.ToString()) ? 0 : int.Parse(lblMachineBrandId.Text.ToString()));
                ddlFixedOrMulti.SelectedIndex = CommonMethods.MatchDropDownItem(ddlFixedOrMulti, string.IsNullOrEmpty(lblFixedOrMultiId.Text.ToString()) ? 0 : int.Parse(lblFixedOrMultiId.Text.ToString()));

                if (ddlKnittingPartTypes.SelectedIndex == 1)
                {
                    ddlFixedOrMulti.Enabled = true;
                }
                else
                {
                    ddlFixedOrMulti.Enabled = false;
                }

                ddlKnittingPartTypes.Visible = true;
                ddlMachineBrands.Visible = true;
                ddlFixedOrMulti.Visible = true;
                tbxIntItemCode.Visible = true;
                tbxPartName.Visible = true;


                Label lblPartTypeValue = (Label)e.Item.FindControl("lblPartTypeValue");
                Label lblMachineBrandValue = (Label)e.Item.FindControl("lblMachineBrandValue");
                Label lblFixedOrMulti = (Label)e.Item.FindControl("lblFixedOrMulti");
                Label lblIntCode = (Label)e.Item.FindControl("lblIntCode");
                Label lblPartsNameValue = (Label)e.Item.FindControl("lblPartsNameValue");


                lblPartTypeValue.Visible = false;
                lblMachineBrandValue.Visible = false;
                lblFixedOrMulti.Visible = false;
                lblIntCode.Visible = false;
                lblPartsNameValue.Visible = false;


                e.Item.FindControl("lnkbtnEdit").Visible = false;
                e.Item.FindControl("lnkbtnUpdate").Visible = true;
                e.Item.FindControl("lnkbtnCancel").Visible = true;

            }
            else if (e.CommandName == "Update")
            {
                try
                {
                    DropDownList ddlKnittingPartTypes = (DropDownList)e.Item.FindControl("ddlKnittingPartTypes");
                    DropDownList ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                    DropDownList ddlFixedOrMulti = (DropDownList)e.Item.FindControl("ddlFixedOrMulti");
                    TextBox tbxItemCode = (TextBox)e.Item.FindControl("tbxIntItemCode");
                    TextBox tbxPartName = (TextBox)e.Item.FindControl("tbxPartName");


                    if (ddlKnittingPartTypes.SelectedIndex == 0 || ddlMachineBrands.SelectedIndex == 0 || tbxPartName.Text.ToString() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Selection or data is invalid.')", true);
                    }
                    else
                    {
                        int spareId = Convert.ToInt32(e.CommandArgument.ToString());

                        var spare = unitOfWork.GenericRepositories<KnittingSpares>().GetByID(spareId);

                        spare.KnittingSpareTypeId = int.Parse(ddlKnittingPartTypes.SelectedValue);
                        spare.MachineBrandId = int.Parse(ddlMachineBrands.SelectedValue);

                        spare.FixedOrMulti = int.Parse(ddlFixedOrMulti.SelectedValue);
                        spare.SpareName = tbxPartName.Text.ToString();
                        spare.SpareCode = tbxItemCode.Text.ToString();
                        spare.UpdatedBy = CommonMethods.SessionInfo.UserName;
                        spare.UpdateDate = DateTime.Now;

                        unitOfWork.GenericRepositories<KnittingSpares>().Update(spare);
                        unitOfWork.Save();

                        ddlKnittingPartTypes.Visible = false;
                        ddlMachineBrands.Visible = false;
                        ddlFixedOrMulti.Visible = false;
                        tbxItemCode.Visible = false;
                        tbxPartName.Visible = false;

                        Label lblPartTypeValue = (Label)e.Item.FindControl("lblPartTypeValue");
                        Label lblMachineBrandValue = (Label)e.Item.FindControl("lblMachineBrandValue");
                        Label lblFixedOrMulti = (Label)e.Item.FindControl("lblFixedOrMulti");
                        Label lblIntCode = (Label)e.Item.FindControl("lblIntCode");
                        Label lblPartsNameValue = (Label)e.Item.FindControl("lblPartsNameValue");


                        lblPartTypeValue.Visible = true;
                        lblMachineBrandValue.Visible = true;
                        lblFixedOrMulti.Visible = true;
                        lblIntCode.Visible = true;
                        lblPartsNameValue.Visible = true;


                        lblPartTypeValue.Text = ddlKnittingPartTypes.SelectedItem.Text;
                        lblMachineBrandValue.Text = ddlMachineBrands.SelectedItem.Text;
                        lblFixedOrMulti.Text = ddlFixedOrMulti.SelectedIndex == 0 ? "" : ddlFixedOrMulti.SelectedItem.Text;
                        lblIntCode.Text = tbxItemCode.Text;
                        lblPartsNameValue.Text = tbxPartName.Text;

                        Label lblSpareTypeId = (Label)e.Item.FindControl("lblSpareTypeId");
                        Label lblMachineBrandId = (Label)e.Item.FindControl("lblMachineBrandId");
                        Label lblFixedOrMultiId = (Label)e.Item.FindControl("lblFixedOrMultiId");

                        lblSpareTypeId.Text = ddlKnittingPartTypes.SelectedValue;
                        lblMachineBrandId.Text = ddlMachineBrands.SelectedValue;
                        lblFixedOrMultiId.Text = ddlFixedOrMulti.SelectedValue;


                        e.Item.FindControl("lnkbtnEdit").Visible = true;
                        e.Item.FindControl("lnkbtnUpdate").Visible = false;
                        e.Item.FindControl("lnkbtnCancel").Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }
            else if (e.CommandName == "Cancel")
            {


                DropDownList ddlKnittingPartTypes = (DropDownList)e.Item.FindControl("ddlKnittingPartTypes");
                DropDownList ddlMachineBrands = (DropDownList)e.Item.FindControl("ddlMachineBrands");
                DropDownList ddlFixedOrMulti = (DropDownList)e.Item.FindControl("ddlFixedOrMulti");
                TextBox tbxIntItemCode = (TextBox)e.Item.FindControl("tbxIntItemCode");
                TextBox tbxPartName = (TextBox)e.Item.FindControl("tbxPartName");

                ddlKnittingPartTypes.Visible = false;
                ddlMachineBrands.Visible = false;
                ddlFixedOrMulti.Visible = false;
                tbxIntItemCode.Visible = false;
                tbxPartName.Visible = false;


                Label lblPartTypeValue = (Label)e.Item.FindControl("lblPartTypeValue");
                Label lblMachineBrandValue = (Label)e.Item.FindControl("lblMachineBrandValue");
                Label lblFixedOrMulti = (Label)e.Item.FindControl("lblFixedOrMulti");
                Label lblIntCode = (Label)e.Item.FindControl("lblIntCode");
                Label lblPartsNameValue = (Label)e.Item.FindControl("lblPartsNameValue");
            
                lblPartTypeValue.Visible = true;
                lblMachineBrandValue.Visible = true;
                lblFixedOrMulti.Visible = true;
                lblIntCode.Visible = true;
                lblPartsNameValue.Visible = true;

                e.Item.FindControl("lnkbtnEdit").Visible = true;
                e.Item.FindControl("lnkbtnUpdate").Visible = false;
                e.Item.FindControl("lnkbtnCancel").Visible = false;
            }
        }


        private void LoadPartTypesDropdown(DropDownList ddl)
        {
            CommonMethods.LoadPartTypeDropdown(ddl, 1, 0);
        }

        private void LoadMachineBrands(DropDownList ddl)
        {
            CommonMethods.LoadMachineBrandDropdown(ddl, 1, 0);
        }


    }
}