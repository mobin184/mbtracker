﻿using Repositories;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Knitting_Spares
{
    public partial class ViewKnittingMachineSpareReceives : System.Web.UI.Page
    {

        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
            //pnlSummary.Visible = false;
            //pnlDetails.Visible = false;

        }

        protected void LoadData()
        {
            pnlDetails.Visible = false;
            Div1.Visible = false;

            var date = string.IsNullOrEmpty(tbxFilterDate.Text) ? "" : Convert.ToDateTime(tbxFilterDate.Text).ToString();

            if (!string.IsNullOrEmpty(date))
            {
                DateTime receiveDate = DateTime.Parse((tbxFilterDate.Text).ToString());
                lblLabelMessage.Text = "Spare received on: " + receiveDate.ToString("dd-MMM-yyyy");

                //var userId = CommonMethods.SessionInfo.UserId;
                string sql = $"Exec usp_GetKnittingSpareReceivesByDate '{date}'";
                var dt = unitOfWork.GetDataTableFromSql(sql);

                if (dt.Rows.Count > 0)
                {
                    rpt.Visible = true;
                    dataDiv.Visible = true;

                    rpt.DataSource = dt;
                    rpt.DataBind();
                    lblNoDataFound.Visible = false;
                    lblLabelMessage.Visible = true;
                }
                else
                {
                    rpt.Visible = false;
                    dataDiv.Visible = false;

                    lblNoDataFound.Visible = true;
                    lblLabelMessage.Visible = false;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select spare receive date!')", true);
            }


        }


        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "ViewDetails")
            {
                pnlDetails.Visible = true;
                Div1.Visible = true;


                Label lblReceiveDate = (Label)e.Item.FindControl("lblReceiveDate");
                Label lblSupplierName = (Label)e.Item.FindControl("lblSupplierName");
                Label lblRequsitionNumber = (Label)e.Item.FindControl("lblRequsitionNumber");
                Label lblInvoiceNumber = (Label)e.Item.FindControl("lblInvoiceNumber");
                Label lblChalanNumber = (Label)e.Item.FindControl("lblChalanNumber");
                Label lblOtherRefNumber = (Label)e.Item.FindControl("lblOtherRefNumber");


                lblDateReceivedValue.Text = DateTime.Parse(lblReceiveDate.Text).ToString("dd-MMM-yyyy");
                lblSupplierValue.Text = lblSupplierName.Text.ToString();
                lblReqNumberValue.Text = lblRequsitionNumber.Text.ToString();
                lblInvoiceValue.Text = lblInvoiceNumber.Text.ToString();
                lblChalanNumberValue.Text = lblChalanNumber.Text.ToString();
                lblOtherRefValue.Text = lblOtherRefNumber.Text.ToString();



                string spareReceiveId = e.CommandArgument.ToString();

                string sql = $"Exec usp_GetKnittingSpareReceiveDetailsByReceiveId '{spareReceiveId}'";
                var dt = unitOfWork.GetDataTableFromSql(sql);

                if (dt.Rows.Count > 0)
                {
                    rptKnittingSpareReceiveInfo.Visible = true;

                    rptKnittingSpareReceiveInfo.DataSource = dt;
                    rptKnittingSpareReceiveInfo.DataBind();

                }
                else
                {
                    rptKnittingSpareReceiveInfo.Visible = false;

                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);

            }
            else if (e.CommandName == "Edit")
            {
                string spareReceiveId = e.CommandArgument.ToString();
                Response.Redirect("ReceiveKnittingSpares.aspx?spareReceiveId=" + Tools.UrlEncode(spareReceiveId));
            }
          
        }


    }
}