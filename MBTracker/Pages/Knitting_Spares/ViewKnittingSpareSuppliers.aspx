﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewKnittingSpareSuppliers.aspx.cs" Inherits="MBTracker.Pages.Knitting_Parts.ViewKnittingSpareSuppliers" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">


    <div class="row-fluid">
        <div class="span9">
            <asp:Label ID="lblNoSpareSupplierFound" Style="width: 100%" runat="server" Visible="false" Text="Spare supplier was not found." BackColor="#ffff00"></asp:Label>
            <asp:Panel ID="pnlViewSpareSuppliers" runat="server" Visible="false">

                <div class="widget">
                    <div class="widget-body">
                        <div class="form-horizontal">

                            <div style="text-align: left; font-size: 16px; font-weight: 800; line-height: 20px; text-underline-position: below; padding-bottom: 25px; padding-top: 10px">
                                List of Knitting Machine Spare Suppliers:
                            </div>


                            <div class="control-group" style="overflow-x: auto">
                                <div class="controls-row">
                                    <asp:Repeater ID="rptKnittingSpareSuppliers" runat="server" OnItemCommand="rptKnittingSpareSuppliers_ItemCommand">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblKnittingSpareSupplierName" runat="server" Text="Supplier Name"></asp:Label>
                                                        </th>
                                                        <th>
                                                            <asp:Label ID="lblKnittingSapreSupplierAddress" runat="server" Text="Supplier Address"></asp:Label>

                                                        </th>
                                                        <th>
                                                            <asp:Label ID="lblPhone" runat="server" Text="Phone Number"></asp:Label>
                                                        </th>
                                                        <th>
                                                            <asp:Label ID="lblEmail" runat="server" Text="Email Address"></asp:Label>
                                                        </th>
                                                        <th>
                                                            <asp:Label ID="lblKnittingSpareSupplierAdditionalInfo" runat="server" Text="Additional Info"></asp:Label>
                                                        </th>
                                                        <th>
                                                            <asp:Label ID="LabelAction" runat="server" Text="Action"></asp:Label>

                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="width:15%">
                                                    <asp:Label ID="lblKnittingSpareSupplierNameValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SupplierName")%>' Width="100%" CssClass="text-left" Style="padding-top: 5px;"></asp:Label>
                                                    <asp:TextBox ID="tbxKnittingSpareSupplierName" Width="100%" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "SupplierName")%>'></asp:TextBox>
                                                </td>
                                                <td style="width:20%">
                                                    <asp:Label ID="lblKnittingSapreSupplierAddressValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SupplierAddress")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                    <asp:TextBox ID="tbxKnittingSapreSupplierAddress" Width="100%" runat="server" TextMode="MultiLine" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "SupplierAddress")%>'></asp:TextBox>
                                                </td>
                                                <td style="width:14%">
                                                    <asp:Label ID="lblPhoneValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SupplierPhone")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                    <asp:TextBox ID="tbxPhoneValue" Width="100%" runat="server" TextMode="MultiLine" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "SupplierPhone")%>'></asp:TextBox>
                                                </td>
                                                <td style="width:18%">
                                                    <asp:Label ID="lblEmailValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SupplierEmail")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                    <asp:TextBox ID="tbxEmailValue" Width="100%" runat="server" TextMode="MultiLine" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "SupplierEmail")%>'></asp:TextBox>
                                                </td>
                                               <td style="width:18%">
                                                    <asp:Label ID="lblKnittingSpareSupplierAdditionalInfoValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SupplierAdditionalInfo")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                    <asp:TextBox ID="tbxKnittingSpareSupplierAdditionalInfo" Width="100%" runat="server" TextMode="MultiLine" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "SupplierAdditionalInfo")%>'></asp:TextBox>
                                                </td>
                                                <td style="vertical-align: middle; text-align: center; width=20%">
                                                    <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="&nbsp;&nbsp;Edit&nbsp;&nbsp;"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnUpdate" runat="server" Visible="false" CommandName="Update" CommandArgument='<%#Eval("Id") %>' class="btn btn-warning2 btn-mini" Text="Update"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnCancel" runat="server" Visible="false" CommandName="Cancel" CommandArgument='<%#Eval("Id") %>' class="btn btn-warning2 btn-mini" Text="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;&nbsp;"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </asp:Panel>


        </div>
    </div>


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
