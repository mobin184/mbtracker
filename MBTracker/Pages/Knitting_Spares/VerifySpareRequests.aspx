﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="VerifySpareRequests.aspx.cs" Inherits="MBTracker.Pages.Knitting_Spares.VerifySpareRequests" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid" id="divUnitSelection" runat="server">

        <div class="span6">

            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="Verify Spares Requests:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 5px">
                            <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>

                        <div class="span7">

                            <div class="control-group">
                                <label for="inputBuyer" class="control-label" style="padding-top: 5px; padding-bottom: 10px">
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                    <asp:Label ID="lblKnittingUnitMessage" runat="server" Text="Knitting Unit:"></asp:Label></label>
                                <div class="controls controls-row" style="padding-top: 5px;">
                                    <asp:DropDownList ID="ddlKnittingUnits" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" CssClass="form-control" OnSelectedIndexChanged="ddlKnittingUnits_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="lblMachineBrand1" runat="server" Text="Machine Brand (Optional):"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlMachineBrands" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputBuyer" class="control-label">
                                    <%--  <span style="font-weight: 700; color: #CC0000">*</span>--%>
                                    <asp:Label ID="Label3" runat="server" Text="From Date (Default: 30 days):"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxFromDate" runat="server" placeholder="From date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="tbxFromDate"><span style="font-weight: 700; color: #CC0000">Please select From date.</span></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>

                        </div>


                        <div class="span4">

                            <div class="control-group">
                                <label for="inputBuyer" class="control-label" style="padding-top: 55px; padding-bottom: 10px">&nbsp;</label>

                                <div class="controls controls-row" style="padding-top: 55px;">
                                    <asp:Button ID="btnViewKnittingParts" runat="server" class="btn btn-success btn-midium pull-right btnStyle" Text="View Spare Requests" OnClick="btnViewKnittingParts_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>
        </div>

    </div>


    <div class="row-fluid">

        <div class="span12">
            <asp:Label ID="lblNoRequestFound" Style="width: 100%" runat="server" Visible="false" Text="No spare request found during this period." BackColor="#ffff00"></asp:Label>
            <asp:Panel ID="pnlViewKnittingSpareRequests" runat="server" Visible="false">

                <div class="widget">
                    <div class="widget-body">
                        <div class="form-horizontal">
                            <div class="control-group" style="overflow-x: auto">
                                <label for="inputStyle" class="control-label" style="font-weight: bold; text-align: left">
                                    <asp:Label ID="Label4" runat="server" Style="font-weight: bold; text-align: left" Font-Bold="false" Text=""></asp:Label></label>
                                <div class="controls-row">
                                    <asp:Repeater ID="rptKnittingSpareRequests" runat="server" >
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblRequestDate" runat="server" Text="Request Date<br/>&nbsp;"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblMachineName" runat="server" Text="Machine Name <br/>&nbsp;"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblSpareName" runat="server" Text="Spare Name<br/>&nbsp;"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblReasonForRequest" runat="server" Text=" Request Reason<br/>&nbsp;"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblRequestedQty" runat="server" Text="Quantity<br/>Requested"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblRemarks" runat="server" Text=" Request Note<br/>&nbsp;"></asp:Label></th>
                                                         <th>
                                                            <asp:Label ID="lblCurrentBalance" runat="server" Text="Quantity<br/> Available"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblVerifiedQty" runat="server" Text="Verified Quantity<br/>&nbsp;"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblVerificationNote" runat="server" Text=" Verification Note<br/>&nbsp;"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="vertical-align: middle; text-align: left; width: 8%">
                                                    <asp:Label ID="lblDateRequestedValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DateRequested")%>'></asp:Label>
                                                    <asp:Label ID="lblSpareRequestDetailId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "Id")%>'></asp:Label>
                                                </td>
                                                <td style="vertical-align: middle; text-align: center; width: 17%">
                                                    <asp:Label ID="lblMachineNameValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingMachineName")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                </td>
                                                <td style="vertical-align: middle; text-align: left; width: 12%">
                                                    <asp:Label ID="lblSpareNameValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingSpareName")%>' Width="100%"></asp:Label>
                                                </td>

                                                <td style="vertical-align: middle; text-align: left; width: 8%">
                                                    <asp:Label ID="lblRequestReason" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ReasonName")%>' Width="100%"></asp:Label>
                                                </td>

                                                <td style="vertical-align: middle; text-align: center; width: 5%">
                                                    <asp:Label ID="lblQuantity" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "QuantityRequested")%>' Width="100%"></asp:Label>
                                                </td>

                                                <td style="vertical-align: middle; text-align: center; width: 15%">
                                                    <asp:Label ID="lblRemarksValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Remarks")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                </td>

                                                <td style="vertical-align: middle; text-align: center; width: 5%">
                                                    <asp:Label ID="lblQtyAvailableValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CurrentBalance")%>' Width="100%" ></asp:Label>
                                                </td>

                                                <td style="vertical-align: middle; text-align: center; width: 10%">
                                                    <asp:TextBox ID="tbxVerifiedQty" Width="100%" runat="server" TextMode="Number" ></asp:TextBox>
                                                </td>

                                                <td style="vertical-align: middle; text-align: center; width: 15%">
                                                    <asp:TextBox ID="tbxVerificationNote" Width="100%" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                  <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>

            </asp:Panel>

          

        </div>
    
    </div>


</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
