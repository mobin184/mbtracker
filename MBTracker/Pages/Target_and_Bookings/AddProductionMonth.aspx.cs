﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Target_and_Bookings
{
    public partial class AddProductionMonth : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadProductionYear();
                LoadWeekend();
                if (Request.QueryString["productionMonthId"] != null)
                {
                    ProductionMonthId = int.Parse(Tools.UrlDecode(Request.QueryString["productionMonthId"]));
                    PopulateBooking(ProductionMonthId);
                }
            }
        }



        private void PopulateBooking(int productionMonthId)
        {
            CommonMethods.LoadDropdown(ddlProductionMonth, "Months", 1, 0);

            var pmInfo = unitOfWork.GenericRepositories<ProductionMonths>().GetByID(productionMonthId);
            ddlProductionYear.SelectedIndex = CommonMethods.MatchDropDownItem(ddlProductionYear, pmInfo.ProductionYear);
            ddlProductionMonth.SelectedIndex = CommonMethods.MatchDropDownItem(ddlProductionMonth, pmInfo.MonthId);
            ddlWeekend.SelectedValue = pmInfo.Weekend;
            tbxStartDate.Text = pmInfo.StartDate.ToString("yyyy-MM-dd");
            tbxEndDate.Text = pmInfo.EndDate.ToString("yyyy-MM-dd");
            tbxOtherHolidays.Text = pmInfo.OtherHolidays;
            tbxNoOfOtherHolidays.Text = pmInfo.NoOfOtherHolidays + "";
            tbxExceptionalWorkingDays.Text = pmInfo.ExceptionalWorkingDays + "";
            tbxNoOfExceptionalWorkingDays.Text = pmInfo.NoOfExceptionalWorkingDays + "";
            userActionTitle.Text = "Update Production Month";
            btnSave.Visible = false;
            btnUpdate.Visible = true;

        }
        private void LoadWeekend()
        {
            ListItem[] lstDays = new ListItem[] {
                new ListItem() { Text = "Friday" },
                new ListItem() { Text = "Saturday" },
                new ListItem() { Text = "Sunday" },
                new ListItem() { Text = "Monday" },
                new ListItem() { Text = "Tuesday" },
                new ListItem() { Text = "Wednesday" },
                new ListItem() { Text = "Thursday" }
            };
            ddlWeekend.Items.AddRange(lstDays);
            ddlWeekend.SelectedIndex = 0;
            ddlWeekend.DataTextField = "Text";
            ddlWeekend.DataValueField = "Text";
        }

        private void LoadProductionYear()
        {
            ListItem[] lstYear = new ListItem[] { new ListItem() { Text = "---Select---", Value = "" }, new ListItem() { Text = DateTime.Now.Year + "", Value = DateTime.Now.Year + "" }, new ListItem() { Text = DateTime.Now.Year + 1 + "", Value = DateTime.Now.Year + 1 + "" } };
            ddlProductionYear.Items.AddRange(lstYear);
            ddlProductionYear.SelectedIndex = 0;
            ddlProductionYear.DataTextField = "Text";
            ddlProductionYear.DataValueField = "Value";
        }

        int ProductionMonthId
        {
            set { ViewState["productionMonthId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["productionMonthId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                ProductionMonths productionMonths = new ProductionMonths()
                {
                    ProductionYear = Convert.ToInt32(ddlProductionYear.SelectedValue),
                    MonthId = Convert.ToInt32(ddlProductionMonth.SelectedValue),
                    StartDate = DateTime.Parse(tbxStartDate.Text),
                    EndDate = DateTime.Parse(tbxEndDate.Text),
                    Weekend = ddlWeekend.SelectedValue,
                    IsActive = 1,
                    NoOfWeekends = CountDays((DayOfWeek)Enum.Parse(typeof(DayOfWeek), ddlWeekend.SelectedValue), DateTime.Parse(tbxStartDate.Text), DateTime.Parse(tbxEndDate.Text)),
                    OtherHolidays = tbxOtherHolidays.Text,
                    NoOfOtherHolidays = string.IsNullOrEmpty(tbxNoOfOtherHolidays.Text) ? (int?)null : Int32.Parse(tbxNoOfOtherHolidays.Text),
                    ExceptionalWorkingDays = tbxOtherHolidays.Text,
                    NoOfExceptionalWorkingDays = string.IsNullOrEmpty(tbxNoOfExceptionalWorkingDays.Text) ? (decimal?)null : Decimal.Parse(tbxNoOfExceptionalWorkingDays.Text),
                    CreateDate = DateTime.Now,
                    CreatedBy = CommonMethods.SessionInfo.UserName
                };

                productionMonths.NoOfWorkingDays = (Decimal)((((productionMonths.EndDate - productionMonths.StartDate).TotalDays + 1 - productionMonths.NoOfWeekends) - (productionMonths.NoOfOtherHolidays ?? 0)) + (Double)(productionMonths.NoOfExceptionalWorkingDays ?? decimal.Parse("0")));
                unitOfWork.GenericRepositories<ProductionMonths>().Insert(productionMonths);
                unitOfWork.Save();
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                //Response.AddHeader("REFRESH", "1;URL=AddProductionMonth.aspx");
                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('AddProductionMonth.aspx');", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "')", true);
            }

        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                var pmInfo = unitOfWork.GenericRepositories<ProductionMonths>().GetByID(ProductionMonthId);
                if (pmInfo != null)
                {
                    pmInfo.ProductionYear = Convert.ToInt32(ddlProductionYear.SelectedValue);
                    pmInfo.MonthId = Convert.ToInt32(ddlProductionMonth.SelectedValue);
                    pmInfo.StartDate = DateTime.Parse(tbxStartDate.Text);
                    pmInfo.EndDate = DateTime.Parse(tbxEndDate.Text);
                    pmInfo.Weekend = ddlWeekend.SelectedValue;
                    pmInfo.NoOfWeekends = CountDays((DayOfWeek)Enum.Parse(typeof(DayOfWeek), ddlWeekend.SelectedValue), DateTime.Parse(tbxStartDate.Text), DateTime.Parse(tbxEndDate.Text));
                    pmInfo.OtherHolidays = tbxOtherHolidays.Text;
                    pmInfo.NoOfOtherHolidays = string.IsNullOrEmpty(tbxNoOfOtherHolidays.Text) ? (int?)null : Int32.Parse(tbxNoOfOtherHolidays.Text);
                    pmInfo.ExceptionalWorkingDays = tbxExceptionalWorkingDays.Text;
                    pmInfo.NoOfExceptionalWorkingDays = string.IsNullOrEmpty(tbxNoOfExceptionalWorkingDays.Text) ? (decimal?)null : Decimal.Parse(tbxNoOfExceptionalWorkingDays.Text);
                    pmInfo.UpdateDate = DateTime.Now;
                    pmInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;

                    pmInfo.NoOfWorkingDays = (Decimal)((((pmInfo.EndDate - pmInfo.StartDate).TotalDays + 1 - pmInfo.NoOfWeekends) - (pmInfo.NoOfOtherHolidays ?? 0)) + (Double)(pmInfo.NoOfExceptionalWorkingDays?? Decimal.Parse("0")));
                    unitOfWork.GenericRepositories<ProductionMonths>().Update(pmInfo);
                    unitOfWork.Save();
                    ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);

                    //Response.AddHeader("REFRESH", "2;URL=ViewProductionMonths.aspx");
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewProductionMonths.aspx');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Invalid production month Id.')", true);
                }



            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "')", true);
            }
        }

        private static int CountDays(DayOfWeek day, DateTime start, DateTime end)
        {
            TimeSpan ts = end - start;                       // Total duration
            int count = (int)Math.Floor(ts.TotalDays / 7);   // Number of whole weeks
            int remainder = (int)(ts.TotalDays % 7);         // Number of remaining days
            int sinceLastDay = (int)(end.DayOfWeek - day);   // Number of days since last [day]
            if (sinceLastDay < 0) sinceLastDay += 7;         // Adjust for negative days since last [day]
            if (remainder >= sinceLastDay) count++;

            return count;
        }

        protected void ddlProductionYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProductionYear.SelectedValue != "")
            {
                CommonMethods.LoadDropdown(ddlProductionMonth, "Months", 1, 0);
            }
            else
            {
                ddlProductionMonth.Items.Clear();
            }

        }

        protected void ddlProductionMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddlProductionMonth.SelectedValue != "")
            {
                var year = Int32.Parse(ddlProductionYear.SelectedValue);
                var month = Int32.Parse(ddlProductionMonth.SelectedValue);
                var productionMonthInfo = unitOfWork.GenericRepositories<ProductionMonths>().IsExist(x => x.ProductionYear == year && x.MonthId == month);
                if (productionMonthInfo)
                {
                    ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Production month " + ddlProductionMonth.SelectedItem.Text +"-"+ year+" already exist!')", true);
                    ddlProductionMonth.SelectedIndex = 0;
                }
            }
            
        }

        protected void tbxEndDate_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbxStartDate.Text))
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please select start date.')", true);
                tbxEndDate.Text = "";
            }
            else if (!string.IsNullOrEmpty(tbxEndDate.Text) && (DateTime.Parse(tbxEndDate.Text) <= DateTime.Parse(tbxStartDate.Text)))
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Month end date should be greater than start date.')", true);
            }          
        }
    }
}