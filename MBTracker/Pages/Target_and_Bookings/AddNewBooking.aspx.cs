﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Target_and_Bookings
{
    public partial class AddNewBooking : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //CommonMethods.LoadDropdown(ddlBuyerName, "Buyers WHERE IsActive = 1 ORDER BY BuyerName", 1, 0);
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyerName, 1, 0);
                CommonMethods.LoadDropdown(ddlShipmentMonth, "Months", 1, 0);
                LoadShipmentYear();
                LoadMachinBrand();
                CommonMethods.LoadDropdown(ddlFinishingUnit, "FinishingUnits", 1, 0);
                CommonMethods.LoadDropdown(ddlLinkingMachineGauge, "LinkingMachineGauges", 1, 0);
                CommonMethods.LoadDropdown(ddlLinkingMachineGaugeTwo, "LinkingMachineGauges", 1, 0);
                //LoadProductionMonth();

                if (Request.QueryString["bookingId"] != null)
                {
                    BookingId = int.Parse(Tools.UrlDecode(Request.QueryString["bookingId"]));
                    PopulateBooking(BookingId);
                }
                else
                {

                }
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmQtySave")
                {
                    SaveOperation();
                }
                if (parameter == "confirmQtyUpdate")
                {
                    UpdateOperation();
                }
            }
        }






        private void LoadBookingMonths(int month, int year)
        {
            if (ddlMachineBrand.SelectedValue != "" && tbxKnittingTime.Text != "" && ddlFinishingUnit.SelectedValue != "" && ddlLinkingMachineGauge.SelectedValue != "" && tbxLinkingPcsperDay.Text != "")
            {
                var machineBrandId = int.Parse(ddlMachineBrand.SelectedValue);
                var knittingTime = int.Parse(tbxKnittingTime.Text);
                var finishingUnitId = int.Parse(ddlFinishingUnit.SelectedValue);
                var linkingMCGauge = int.Parse(ddlLinkingMachineGauge.SelectedValue);
                var linkingPerDay = int.Parse(tbxLinkingPcsperDay.Text);
                var linkingMCGaugeTwo = int.Parse(string.IsNullOrEmpty(ddlLinkingMachineGaugeTwo.SelectedValue) ? "0" : ddlLinkingMachineGaugeTwo.SelectedValue);
                if (linkingMCGauge == linkingMCGaugeTwo)
                {
                    linkingMCGaugeTwo = 0;
                }
                var date = year + "-" + month + "-" + "01";
                var dtBookingMonths = unitOfWork.GetDataTableFromSql($"EXEC usp_GetPastSixMonths '{date}'");
                dtBookingMonths.Columns.Add("AvailableBookingCapacity", typeof(int));


                foreach (DataRow item in dtBookingMonths.Rows)
                {
                    var productionMonthId = int.Parse(item["ProductionMonthId"].ToString());
                    string sql = $"Exec usp_GetAvailableKnittingCapacityAndLinkingCapacity {productionMonthId},{machineBrandId},{knittingTime},{finishingUnitId},{linkingMCGauge},{linkingPerDay},{linkingMCGaugeTwo}";
                    var availablQty = unitOfWork.GetRecordSet<AvailableQty>(sql).FirstOrDefault();
                    MinQty = availablQty.AvailableKnittingQty > availablQty.AvailableLinkingQty ? availablQty.AvailableLinkingQty : availablQty.AvailableKnittingQty;

                    if (UpdateBooking != null && UpdateBooking.Id != 0)
                    {
                        if (UpdateBooking.MachineBrandId == machineBrandId && UpdateBooking.KnittingTime == knittingTime && UpdateBooking.FinishingUnitId == finishingUnitId && UpdateBooking.LinkingQtyPerDay == linkingPerDay)
                        {
                            foreach (var itemD in UpdateBooking.lstBookingDetails)
                            {
                                if (itemD.ProductionMonthId == productionMonthId)
                                {
                                    MinQty += itemD.BookingQuantity;
                                }
                            }
                        }
                    }


                    item["AvailableBookingCapacity"] = MinQty;
                }
                rptBookingMonths.DataSource = dtBookingMonths;
                rptBookingMonths.DataBind();
                rptBookingMonths.Visible = true;
                divProductionMonth.Visible = true;
                InfoTextDiv.Visible = false;
            }
            else
            {
                rptBookingMonths.Visible = false;
                divProductionMonth.Visible = false;
                InfoTextDiv.Visible = true;
            }

        }


        int MinQty
        {
            set { ViewState["minQty"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["minQty"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        int BookingId
        {
            set { ViewState["bookingId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["bookingId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        BookingVM UpdateBooking
        {
            set { ViewState["updateBooking"] = value; }
            get
            {
                try
                {
                    return (BookingVM)(ViewState["updateBooking"]);
                }
                catch
                {
                    return new BookingVM();
                }
            }
        }


        private void PopulateBooking(int bookingId)
        {
            var bookingInfo = unitOfWork.GenericRepositories<Booking>().GetByID(bookingId);
            var lstBookingDetails = unitOfWork.GenericRepositories<BookingDetails>().Get(x => x.BookingId == bookingId).ToList();
            List<BookingDetailsVM> lstbookingDetails = new List<BookingDetailsVM>();
            foreach (var item in lstBookingDetails)
            {
                var UpdateBookingDetails = new BookingDetailsVM()
                {
                    ProductionMonthId = item.ProductionMonthId,
                    BookingQuantity = item.BookingQuantity,
                    Id = item.Id
                };
                lstbookingDetails.Add(UpdateBookingDetails);
            }

            UpdateBooking = new BookingVM()
            {
                Id = 1,
                BookingQuantity = bookingInfo.BookingQuantity,
                FinishingUnitId = bookingInfo.FinishingUnitId,
                KnittingTime = bookingInfo.KnittingTime,
                LinkingMachineGaugeId = bookingInfo.LinkingMachineGaugeId,
                LinkingQtyPerDay = bookingInfo.LinkingQtyPerDay ?? 0,
                MachineBrandId = bookingInfo.MachineBrandId,
                lstBookingDetails = lstbookingDetails
            };
            BindStylesByBuyer(bookingInfo.BuyerId);
            CommonMethods.LoadDropdown(ddlBookingForYear, "Years WHERE IsActive = 1", 1, 0);
            BindSeasonByBuyer(bookingInfo.BuyerId);
            BindDataToUI(bookingInfo);
        }




        private void BindDataToUI(Booking bookingInfo)
        {
            tbxBookingDate.Text = bookingInfo.BookingDate.ToString("yyyy-MM-dd");
            ddlBuyerName.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyerName, bookingInfo.BuyerId);
            ddlStyleName.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyleName, bookingInfo.StyleId ?? 0);

            ddlBuyerName.Enabled = false;
            ddlStyleName.Enabled = false;


            //tbxBookingQuantity.Text = bookingInfo.BookingQuantity + "";
            tbxYarnComposition.Text = bookingInfo.YarnComposition + "";
            ddlBookingForYear.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBookingForYear, bookingInfo.SeasonYearId);
            ddlBuyersSeason.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyersSeason, bookingInfo.SeasonId ?? 0);
            //ddlShipmentMonth.SelectedIndex = CommonMethods.MatchDropDownItem(ddlShipmentMonth, bookingInfo.ShipmentMonthId);
            //ddlShipmentYear.SelectedIndex = CommonMethods.MatchDropDownItem(ddlShipmentYear, Convert.ToInt32(bookingInfo.ShipmentYear ?? "0"));
            ddlMachineBrand.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrand, bookingInfo.MachineBrandId);
            tbxKnittingMachineGauge.Text = bookingInfo.KnittingMachineGauge;
            ddlLinkingMachineGauge.SelectedIndex = CommonMethods.MatchDropDownItem(ddlLinkingMachineGauge, bookingInfo.LinkingMachineGaugeId);
            ddlFinishingUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlFinishingUnit, bookingInfo.FinishingUnitId);
            tbxKnittingTime.Text = bookingInfo.KnittingTime + "";
            //ddlProductionMonth.SelectedIndex = CommonMethods.MatchDropDownItem(ddlProductionMonth, bookingInfo.ProductionMonthId);
            //tbxKnittingStartDate.Text = bookingInfo.KnittingStartDate?.ToString("yyyy-MM-dd");
            tbxLinkingPcsperDay.Text = bookingInfo.LinkingQtyPerDay + "";
            //tbxLinkingStartDate.Text = bookingInfo.LinkingStartDate?.ToString("yyyy-MM-dd");
            //tbxSampleApprovalDate.Text = bookingInfo.SampleApprovalDate?.ToString("yyyy-MM-dd");
            //tbxYarnInHouseDate.Text = bookingInfo.YarnInHouseDate?.ToString("yyyy-MM-dd");
            //tbxPPApprovalDate.Text = bookingInfo.PPApprovalDate?.ToString("yyyy-MM-dd");
            ddlYarnInHanksOrCone.SelectedValue = bookingInfo.YarnInHanksOrCone;
            ddlYarnLocalOrImported.SelectedValue = bookingInfo.YarnLocalOrImported;
            tbxWeightPerDz.Text = bookingInfo.WeightPerDz + "";
            ddlLinkingMachineGaugeTwo.SelectedIndex = CommonMethods.MatchDropDownItem(ddlLinkingMachineGaugeTwo, bookingInfo.SecondLinkingMachineGaugeId ?? 0);
            ddlAutoLinking.SelectedValue = bookingInfo.AutoLinking;
            tbxTrimmingOutput.Text = bookingInfo.TrimmingOutput + "";
            tbxMendingOutput.Text = bookingInfo.MendingOutput + "";
            tbxIroningOutput.Text = bookingInfo.IroningOutput + "";
            tbxAdditionalWork.Text = bookingInfo.AdditionalWork;
            userActionTitle.Text = "Update Booking";
            btnSave.Visible = false;
            btnUpdate.Visible = true;

            //LoadBookingMonths(bookingInfo.ShipmentMonthId, int.Parse(bookingInfo.ShipmentYear));
        }

        private void LoadMachinBrand()
        {
            //ddlMachineBrand.DataTextField = "MachineBrand";
            //ddlMachineBrand.DataValueField = "Id";
            //ddlMachineBrand.DataSource = new OrderManager().GetMachineBrands();
            //ddlMachineBrand.DataBind();
            var itemList = new OrderManager().GetMachineBrands();
            List<ListItem> lstMachinBrand = new List<ListItem>() { new ListItem() { Text = "---Select---", Value = "" } };
            lstMachinBrand.AddRange(itemList.Rows.OfType<DataRow>().Select(a => new ListItem() { Text = a[1].ToString(), Value = a[0].ToString() }).ToList());
            ddlMachineBrand.DataTextField = "MachineBrand";
            ddlMachineBrand.DataValueField = "Id";
            ddlMachineBrand.Items.AddRange(lstMachinBrand.ToArray());
            ddlMachineBrand.SelectedIndex = 0;
        }

        private void LoadShipmentYear()
        {
            ListItem[] lstYear = new ListItem[] { new ListItem() { Text = DateTime.Now.Year + "", Value = DateTime.Now.Year + "" }, new ListItem() { Text = DateTime.Now.Year + 1 + "", Value = DateTime.Now.Year + 1 + "" } };
            ddlShipmentYear.Items.AddRange(lstYear);
            ddlShipmentYear.SelectedIndex = 0;
            ddlShipmentYear.DataTextField = "Text";
            ddlShipmentYear.DataValueField = "Value";
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyerName.SelectedValue != "")
            {

                BindStylesByBuyer(Convert.ToInt32(ddlBuyerName.SelectedValue));
                CommonMethods.LoadDropdown(ddlBookingForYear, "Years WHERE IsActive = 1", 1, 0);
                //

            }
            else
            {
                ddlStyleName.Items.Clear();
                ddlBookingForYear.Items.Clear();
                ddlBuyersSeason.Items.Clear();
            }
        }

        protected void ddlBookingForYear_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBookingForYear.SelectedValue != "")
            {
                BindSeasonByBuyer(Convert.ToInt32(ddlBuyerName.SelectedValue));
            }
            else
            {
                ddlBuyersSeason.Items.Clear();
            }
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyleName, buyerId, "BuyerStyles", 1, 0);
        }

        private void BindSeasonByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdown(ddlBuyersSeason, "BuyerSeasons WHERE BuyerId = " + buyerId, 1, 0);
        }

        protected void ChangeProductionMonthInfo(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                var shipmentMonth = int.Parse(ddlShipmentMonth.SelectedValue);
                var shipmentYear = int.Parse(ddlShipmentYear.SelectedValue);
                LoadBookingMonths(shipmentMonth, shipmentYear);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var shipmentMonth = Convert.ToInt32(ddlShipmentMonth.SelectedValue);
                var shipmentYear = Convert.ToInt32(ddlShipmentYear.SelectedValue);
                var currentMonth = DateTime.Now.Month;
                var currentYear = DateTime.Now.Year;
                var isBookingQuantityEntered = false;
                if ((shipmentMonth >= currentMonth && shipmentYear == currentYear) || shipmentYear > currentYear)
                {
                    string title = $"Available Capacity:";
                    string msg = "";
                    for (int i = 0; i < rptBookingMonths.Items.Count; i++)
                    {
                        var productionMonthId = int.Parse(((Label)rptBookingMonths.Items[i].FindControl("lblProductionMonthId")).Text);
                        var monthName = ((Label)rptBookingMonths.Items[i].FindControl("lblMonthName")).Text;

                        var tbxBookingQty = (TextBox)rptBookingMonths.Items[i].FindControl("tbxBookingQuantity");
                        var bookingQuantity = string.IsNullOrEmpty(tbxBookingQty.Text) ? 0 : int.Parse(tbxBookingQty.Text);

                        if (bookingQuantity > 0)
                        {
                            var machineBrandId = int.Parse(ddlMachineBrand.SelectedValue);
                            var knittingTime = int.Parse(tbxKnittingTime.Text);
                            var finishingUnitId = int.Parse(ddlFinishingUnit.SelectedValue);
                            var linkingMCGauge = int.Parse(ddlLinkingMachineGauge.SelectedValue);
                            var linkingPerDay = int.Parse(tbxLinkingPcsperDay.Text);
                            var linkingMCGaugeTwo = int.Parse(string.IsNullOrEmpty(ddlLinkingMachineGaugeTwo.SelectedValue) ? "0" : ddlLinkingMachineGaugeTwo.SelectedValue);
                            if (linkingMCGauge == linkingMCGaugeTwo)
                            {
                                linkingMCGaugeTwo = 0;
                            }

                            string sql = $"Exec usp_GetAvailableKnittingCapacityAndLinkingCapacity {productionMonthId},{machineBrandId},{knittingTime},{finishingUnitId},{linkingMCGauge},{linkingPerDay},{linkingMCGaugeTwo}";
                            var availablQty = unitOfWork.GetRecordSet<AvailableQty>(sql).FirstOrDefault();
                            var MinQty = availablQty.AvailableKnittingQty > availablQty.AvailableLinkingQty ? availablQty.AvailableLinkingQty : availablQty.AvailableKnittingQty;

                            if (bookingQuantity > MinQty)
                            {
                                msg += $"{monthName} available capacity: {MinQty}</br>";
                                tbxBookingQty.Text = MinQty + "";
                            }

                            isBookingQuantityEntered = true;
                        }
                    }

                    if (msg == "" && isBookingQuantityEntered == true)
                    {
                        SaveOperation();
                    }
                    else if (msg != "" && isBookingQuantityEntered == true)
                    {
                        msg += "<br /> Do you want to book available capacity?";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmQtySave','350');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter booking quantity!');", true);
                    }

                    //var machineBrandId = int.Parse(ddlMachineBrand.SelectedValue);
                    //var knittingTime = int.Parse(tbxKnittingTime.Text);
                    //var finishingUnitId = int.Parse(ddlFinishingUnit.SelectedValue);
                    //var linkingMCGauge = int.Parse(ddlLinkingMachineGauge.SelectedValue);
                    //var linkingPerDay = int.Parse(tbxLinkingPcsperDay.Text);

                    //string sql = $"Exec usp_GetAvailableKnittingCapacityAndLinkingCapacity {productionMonthId},{machineBrandId},{knittingTime},{finishingUnitId},{linkingMCGauge},{linkingPerDay}";
                    //var availablQty = unitOfWork.GetRecordSet<AvailableQty>(sql).FirstOrDefault();
                    //MinQty = availablQty.AvailableKnittingQty > availablQty.AvailableLinkingQty ? availablQty.AvailableLinkingQty : availablQty.AvailableKnittingQty;

                    //if (bookingQuantity > MinQty)
                    //{
                    //    string title = $"Available Capacity:";
                    //    string msg = "";
                    //    if (MinQty > 0)
                    //    {
                    //        msg = $"Available knitting capacity: {availablQty.AvailableKnittingQty} <br />Available linking capacity: {availablQty.AvailableLinkingQty} <br /><br /> Do you want to book quantity? {MinQty}";
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmQtySave','350');", true);
                    //    }
                    //    else
                    //    {
                    //        msg = $"Available knitting capacity: {availablQty.AvailableKnittingQty} <br />Available linking capacity: {availablQty.AvailableLinkingQty} <br /><br /> Capacity is not available.";
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmQtySave','350','true');", true);
                    //    }
                    //}
                    //else
                    //{
                    //    MinQty = bookingQuantity;
                    //    SaveOperation();
                    //}
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Sorry, you cannot book for the month in the past.');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "');", true);
            }
        }
        public void SaveOperation()
        {
            try
            {

                var booking = new Booking()
                {
                    BookingDate = Convert.ToDateTime(tbxBookingDate.Text),
                    BuyerId = Convert.ToInt32(ddlBuyerName.SelectedValue),
                    StyleId = Convert.ToInt32(ddlStyleName.SelectedValue),
                    YarnComposition = tbxYarnComposition.Text,
                    // BookingQuantity = MinQty,
                    SeasonYearId = Convert.ToInt32(ddlBookingForYear.SelectedValue),
                    SeasonId = string.IsNullOrEmpty(ddlBuyersSeason.SelectedValue) ? (int?)null : Int32.Parse(ddlBuyersSeason.SelectedValue),
                    //ShipmentMonthId = Convert.ToInt32(ddlShipmentMonth.SelectedValue),
                   // ShipmentYear = ddlShipmentYear.SelectedValue,
                    MachineBrandId = Convert.ToInt32(ddlMachineBrand.SelectedValue),
                    KnittingMachineGauge = tbxKnittingMachineGauge.Text,
                    LinkingMachineGaugeId = Convert.ToInt32(ddlLinkingMachineGauge.SelectedValue),
                    FinishingUnitId = Convert.ToInt32(ddlFinishingUnit.SelectedValue),
                    KnittingTime = Convert.ToInt32(tbxKnittingTime.Text),
                    // ProductionMonthId = int.Parse(ddlProductionMonth.SelectedValue),
                    //KnittingStartDate = string.IsNullOrEmpty(tbxKnittingStartDate.Text) ? (DateTime?)null : DateTime.Parse(tbxKnittingStartDate.Text),
                    LinkingQtyPerDay = string.IsNullOrEmpty(tbxLinkingPcsperDay.Text) ? (int?)null : Int32.Parse(tbxLinkingPcsperDay.Text),
                    //LinkingStartDate = string.IsNullOrEmpty(tbxLinkingStartDate.Text) ? (DateTime?)null : DateTime.Parse(tbxLinkingStartDate.Text),
                    //SampleApprovalDate = string.IsNullOrEmpty(tbxSampleApprovalDate.Text) ? (DateTime?)null : DateTime.Parse(tbxSampleApprovalDate.Text),
                    //YarnInHouseDate = string.IsNullOrEmpty(tbxYarnInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxYarnInHouseDate.Text),
                    //PPApprovalDate = string.IsNullOrEmpty(tbxPPApprovalDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPPApprovalDate.Text),
                    YarnInHanksOrCone = ddlYarnInHanksOrCone.SelectedValue,
                    YarnLocalOrImported = ddlYarnLocalOrImported.SelectedValue,
                    WeightPerDz = decimal.Parse(string.IsNullOrEmpty(tbxWeightPerDz.Text) ? "0" : tbxWeightPerDz.Text),
                    SecondLinkingMachineGaugeId = int.Parse(string.IsNullOrEmpty(ddlLinkingMachineGaugeTwo.SelectedValue) ? "0" : ddlLinkingMachineGaugeTwo.SelectedValue),
                    AutoLinking = ddlAutoLinking.SelectedValue,
                    TrimmingOutput = int.Parse(string.IsNullOrEmpty(tbxTrimmingOutput.Text) ? "0" : tbxTrimmingOutput.Text),
                    MendingOutput = int.Parse(string.IsNullOrEmpty(tbxMendingOutput.Text) ? "0" : tbxMendingOutput.Text),
                    IroningOutput = int.Parse(string.IsNullOrEmpty(tbxIroningOutput.Text) ? "0" : tbxIroningOutput.Text),
                    AdditionalWork = tbxAdditionalWork.Text,
                    CreateDate = DateTime.Now,
                    CreatedBy = CommonMethods.SessionInfo.UserName
                };

                var totalBookingQty = 0;
                for (int i = 0; i < rptBookingMonths.Items.Count; i++)
                {
                    var productionMonthId = int.Parse(((Label)rptBookingMonths.Items[i].FindControl("lblProductionMonthId")).Text);
                    var stBookingQty = ((TextBox)rptBookingMonths.Items[i].FindControl("tbxBookingQuantity")).Text;
                    var bookingQuantity = string.IsNullOrEmpty(stBookingQty) ? 0 : int.Parse(stBookingQty);
                    totalBookingQty += bookingQuantity;
                    if (bookingQuantity > 0)
                    {
                        var bookingDetails = new BookingDetails()
                        {
                            ProductionMonthId = productionMonthId,
                            BookingId = booking.Id,
                            BookingQuantity = bookingQuantity,
                            KnittingTime = booking.KnittingTime,
                            FinishingUnitId = booking.FinishingUnitId,
                            LinkingMachineGaugeId = booking.LinkingMachineGaugeId,
                            LinkingQtyPerDay = booking.LinkingQtyPerDay ?? 0,
                            MachineBrandId = booking.MachineBrandId
                        };
                        unitOfWork.GenericRepositories<BookingDetails>().Insert(bookingDetails);
                    }
                }

                if (totalBookingQty > 0)
                {
                    booking.BookingQuantity = totalBookingQty;
                    unitOfWork.GenericRepositories<Booking>().Insert(booking);
                    unitOfWork.Save();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('AddNewBooking.aspx');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter booking quantity!');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    var shipmentMonth = Convert.ToInt32(ddlShipmentMonth.SelectedValue);
                    var shipmentYear = Convert.ToInt32(ddlShipmentYear.SelectedValue);
                    var currentMonth = DateTime.Now.Month;
                    var currentYear = DateTime.Now.Year;
                    var isBookingQuantityEntered = false;
                    if ((shipmentMonth >= currentMonth && shipmentYear == currentYear) || shipmentYear > currentYear)
                    {
                        string title = $"Available Capacity:";
                        string msg = "";
                        for (int i = 0; i < rptBookingMonths.Items.Count; i++)
                        {
                            var productionMonthId = int.Parse(((Label)rptBookingMonths.Items[i].FindControl("lblProductionMonthId")).Text);
                            var monthName = ((Label)rptBookingMonths.Items[i].FindControl("lblMonthName")).Text;

                            var tbxBookingQty = (TextBox)rptBookingMonths.Items[i].FindControl("tbxBookingQuantity");
                            var bookingQuantity = string.IsNullOrEmpty(tbxBookingQty.Text) ? 0 : int.Parse(tbxBookingQty.Text);

                            if (bookingQuantity > 0)
                            {
                                var machineBrandId = int.Parse(ddlMachineBrand.SelectedValue);
                                var knittingTime = int.Parse(tbxKnittingTime.Text);
                                var finishingUnitId = int.Parse(ddlFinishingUnit.SelectedValue);
                                var linkingMCGauge = int.Parse(ddlLinkingMachineGauge.SelectedValue);
                                var linkingPerDay = int.Parse(tbxLinkingPcsperDay.Text);
                                var linkingMCGaugeTwo = int.Parse(string.IsNullOrEmpty(ddlLinkingMachineGaugeTwo.SelectedValue) ? "0" : ddlLinkingMachineGaugeTwo.SelectedValue);
                                if (linkingMCGauge == linkingMCGaugeTwo)
                                {
                                    linkingMCGaugeTwo = 0;
                                }

                                string sql = $"Exec usp_GetAvailableKnittingCapacityAndLinkingCapacity {productionMonthId},{machineBrandId},{knittingTime},{finishingUnitId},{linkingMCGauge},{linkingPerDay},{linkingMCGaugeTwo}";
                                var availablQty = unitOfWork.GetRecordSet<AvailableQty>(sql).FirstOrDefault();
                                var MinQty = availablQty.AvailableKnittingQty > availablQty.AvailableLinkingQty ? availablQty.AvailableLinkingQty : availablQty.AvailableKnittingQty;

                                if (UpdateBooking != null && UpdateBooking.Id != 0)
                                {
                                    if (UpdateBooking.MachineBrandId == machineBrandId && UpdateBooking.KnittingTime == knittingTime && UpdateBooking.FinishingUnitId == finishingUnitId && UpdateBooking.LinkingQtyPerDay == linkingPerDay)
                                    {
                                        foreach (var itemD in UpdateBooking.lstBookingDetails)
                                        {
                                            if (itemD.ProductionMonthId == productionMonthId)
                                            {
                                                MinQty += itemD.BookingQuantity;
                                            }
                                        }
                                    }
                                }

                                if (bookingQuantity > MinQty)
                                {
                                    msg += $"{monthName} available capacity: {MinQty}</br>";
                                    tbxBookingQty.Text = MinQty + "";
                                }

                                isBookingQuantityEntered = true;
                            }
                        }

                        if (msg == "" && isBookingQuantityEntered == true)
                        {
                            UpdateOperation();
                        }
                        else if (msg != "" && isBookingQuantityEntered == true)
                        {
                            msg += "<br /> Do you want to book available capacity?";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmQtyUpdate','350');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter booking quantity!');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Sorry, you cannot book for the month in the past.');", true);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        private void UpdateOperation()
        {
            try
            {
                var bookingInfo = unitOfWork.GenericRepositories<Booking>().GetByID(BookingId);
                var bookingHistory= new BookingHistory();
                if (bookingInfo != null)
                {
                    Tools.CopyClass(bookingHistory, bookingInfo);
                    bookingHistory.UpdatedBy = CommonMethods.SessionInfo.UserName;
                    bookingHistory.UpdateDate = DateTime.Now;
                    unitOfWork.GenericRepositories<BookingHistory>().Insert(bookingHistory);


                    bookingInfo.BookingDate = Convert.ToDateTime(tbxBookingDate.Text);
                    bookingInfo.BuyerId = Convert.ToInt32(ddlBuyerName.SelectedValue);
                    bookingInfo.StyleId = Convert.ToInt32(ddlStyleName.SelectedValue);
                    bookingInfo.BookingQuantity = MinQty;
                    bookingInfo.YarnComposition = tbxYarnComposition.Text;
                    bookingInfo.SeasonYearId = Convert.ToInt32(ddlBookingForYear.SelectedValue);
                    bookingInfo.SeasonId = string.IsNullOrEmpty(ddlBuyersSeason.SelectedValue) ? (int?)null : Int32.Parse(ddlBuyersSeason.SelectedValue);
                    //bookingInfo.ShipmentMonthId = Convert.ToInt32(ddlShipmentMonth.SelectedValue);
                    //bookingInfo.ShipmentYear = ddlShipmentYear.SelectedValue;
                    bookingInfo.MachineBrandId = Convert.ToInt32(ddlMachineBrand.SelectedValue);
                    bookingInfo.KnittingMachineGauge = tbxKnittingMachineGauge.Text;
                    bookingInfo.LinkingMachineGaugeId = Convert.ToInt32(ddlLinkingMachineGauge.SelectedValue);
                    bookingInfo.FinishingUnitId = Convert.ToInt32(ddlFinishingUnit.SelectedValue);
                    bookingInfo.KnittingTime = Convert.ToInt32(tbxKnittingTime.Text);
                    bookingInfo.LinkingQtyPerDay = string.IsNullOrEmpty(tbxLinkingPcsperDay.Text) ? (int?)null : Int32.Parse(tbxLinkingPcsperDay.Text);
                    //bookingInfo.SampleApprovalDate = string.IsNullOrEmpty(tbxSampleApprovalDate.Text) ? (DateTime?)null : DateTime.Parse(tbxSampleApprovalDate.Text);
                    //bookingInfo.YarnInHouseDate = string.IsNullOrEmpty(tbxYarnInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxYarnInHouseDate.Text);
                    //bookingInfo.PPApprovalDate = string.IsNullOrEmpty(tbxPPApprovalDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPPApprovalDate.Text);
                    bookingInfo.YarnInHanksOrCone = ddlYarnInHanksOrCone.SelectedValue;
                    bookingInfo.YarnLocalOrImported = ddlYarnLocalOrImported.SelectedValue;
                    bookingInfo.WeightPerDz = decimal.Parse(string.IsNullOrEmpty(tbxWeightPerDz.Text) ? "0" : tbxWeightPerDz.Text);
                    bookingInfo.SecondLinkingMachineGaugeId = int.Parse(string.IsNullOrEmpty(ddlLinkingMachineGaugeTwo.SelectedValue) ? "0" : ddlLinkingMachineGaugeTwo.SelectedValue);
                    bookingInfo.AutoLinking = ddlAutoLinking.SelectedValue;
                    bookingInfo.TrimmingOutput = int.Parse(string.IsNullOrEmpty(tbxTrimmingOutput.Text) ? "0" : tbxTrimmingOutput.Text);
                    bookingInfo.MendingOutput = int.Parse(string.IsNullOrEmpty(tbxMendingOutput.Text) ? "0" : tbxMendingOutput.Text);
                    bookingInfo.IroningOutput = int.Parse(string.IsNullOrEmpty(tbxIroningOutput.Text) ? "0" : tbxIroningOutput.Text);
                    bookingInfo.AdditionalWork = tbxAdditionalWork.Text;
                    bookingInfo.UpdateDate = DateTime.Now;
                    bookingInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;

                    var lstBookingDetails = unitOfWork.GenericRepositories<BookingDetails>().Get(x => x.BookingId == BookingId).ToList();

                    foreach (var item in lstBookingDetails)
                    {
                        var historyDetails = new BookingDetailsHistory();
                        Tools.CopyClass(historyDetails, item);
                        historyDetails.HistoryId = bookingHistory.HistoryId;
                        unitOfWork.GenericRepositories<BookingDetails>().Delete(item.Id);
                        unitOfWork.GenericRepositories<BookingDetailsHistory>().Insert(historyDetails);
                    }

                    var totalBookingQty = 0;
                    for (int i = 0; i < rptBookingMonths.Items.Count; i++)
                    {
                        var productionMonthId = int.Parse(((Label)rptBookingMonths.Items[i].FindControl("lblProductionMonthId")).Text);
                        var stBookingQty = ((TextBox)rptBookingMonths.Items[i].FindControl("tbxBookingQuantity")).Text;
                        var bookingQuantity = string.IsNullOrEmpty(stBookingQty) ? 0 : int.Parse(stBookingQty);
                        totalBookingQty += bookingQuantity;
                        if (bookingQuantity > 0)
                        {
                            var bookingDetails = new BookingDetails()
                            {
                                ProductionMonthId = productionMonthId,
                                BookingId = bookingInfo.Id,
                                BookingQuantity = bookingQuantity,
                                KnittingTime = bookingInfo.KnittingTime,
                                FinishingUnitId = bookingInfo.FinishingUnitId,
                                LinkingMachineGaugeId = bookingInfo.LinkingMachineGaugeId,
                                LinkingQtyPerDay = bookingInfo.LinkingQtyPerDay ?? 0,
                                MachineBrandId = bookingInfo.MachineBrandId
                            };
                            unitOfWork.GenericRepositories<BookingDetails>().Insert(bookingDetails);
                        }
                    }

                    if (totalBookingQty > 0)
                    {
                        bookingInfo.BookingQuantity = totalBookingQty;
                        unitOfWork.GenericRepositories<Booking>().Update(bookingInfo);
                        unitOfWork.Save();

                        CommonMethods.SendEventNotification(4, bookingInfo.BuyerId, bookingInfo.StyleId, null, BookingId);

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewBookings.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter booking quantity!');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Invalid Booking Id!');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        public class AvailableQty
        {
            public int AvailableKnittingQty { get; set; }
            public int AvailableLinkingQty { get; set; }
        }

        [Serializable]
        public class BookingVM
        {
            public int Id { get; set; }
            public int BookingQuantity { get; set; }
            public int MachineBrandId { get; set; }
            public int LinkingMachineGaugeId { get; set; }
            public int FinishingUnitId { get; set; }
            public int KnittingTime { get; set; }
            //public int ProductionMonthId { get; set; }
            public int LinkingQtyPerDay { get; set; }
            public List<BookingDetailsVM> lstBookingDetails { get; set; }
        }

        [Serializable]
        public class BookingDetailsVM
        {
            public int ProductionMonthId { get; set; }
            public int BookingQuantity { get; set; }
            public int Id { get; set; }

        }

        protected void rptBookingMonths_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (BookingId > 0)
                {
                    var productionMonthId = int.Parse(((Label)e.Item.FindControl("lblProductionMonthId")).Text);
                    var tbxBookingQuantity = (TextBox)e.Item.FindControl("tbxBookingQuantity");
                    foreach (var item in UpdateBooking.lstBookingDetails)
                    {
                        if (productionMonthId == item.ProductionMonthId)
                        {
                            tbxBookingQuantity.Text = item.BookingQuantity + "";
                        }
                    }
                }
            }
        }
    }
}