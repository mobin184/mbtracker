﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddProductionMonth.aspx.cs" Inherits="MBTracker.Pages.Target_and_Bookings.AddProductionMonth" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .form-horizontal .control-label {
            float: left;
            width: 170px;
            padding-top: 5px;
            text-align: right;
        }

        .btn-info {
            margin: 1px 0px 1px 3px;
        }
    </style>
    <div class="row-fluid">
        <div class="span9">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="userActionTitle" Text="Add a Production Month"></asp:Label>
                    </div>

                </div>

                <div class="widget-body" >
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 30px; padding-bottom: 0px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <br />

                        <div class="col-md-5">
                            <div class="control-group">
                                <label for="inputProductionYear" class="control-label">
                                    <asp:Label ID="lblProductionYear" runat="server" Text="Production Year:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlProductionYear" runat="server" placeholder="Select production year" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlProductionYear_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlProductionYear">
			                        <span style="font-weight: 700; color: #CC0000">Please select Production Year.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputProductionMonth" class="control-label">
                                    <asp:Label ID="lblProductionMonth" runat="server" Text="Production Month:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlProductionMonth" runat="server" placeholder="Select production month" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlProductionMonth_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlProductionMonth">
			                        <span style="font-weight: 700; color: #CC0000">Please select production month.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputStartDate" class="control-label">
                                    <asp:Label ID="lblStartDate" runat="server" Text="Month Start Date:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxStartDate" runat="server" placeholder="Enter month start date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="tbxStartDate">
			                        <span style="font-weight: 700; color: #CC0000">Enter start date.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputEndDate" class="control-label">
                                    <asp:Label ID="lblEndDate" runat="server" Text="Month End Date:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxEndDate" runat="server" placeholder="Enter month end date" Width="100%" AutoPostBack="true" OnTextChanged="tbxEndDate_TextChanged" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="tbxEndDate">
			                        <span style="font-weight: 700; color: #CC0000">Enter end date.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputWeekend" class="control-label">
                                    <asp:Label ID="lblWeekend" runat="server" Text="Weekend:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlWeekend" runat="server" placeholder="Select weekend" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlWeekend">
			                        <span style="font-weight: 700; color: #CC0000">Please select weekend.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>


                        </div>

                        <div class="col-md-5 col-md-offset-1">
                            <div class="control-group">
                                <label for="inputOtherHolidays" class="control-label">
                                    <asp:Label ID="lblOtherHolidays" runat="server" Text="Other Holidays:"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxOtherHolidays" runat="server" placeholder="Enter other holidays" Width="100%" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputNoOfOtherHolidyas" class="control-label">
                                    <asp:Label ID="lblNoOfOtherHolidays" runat="server" Text="# of Other Holidays:"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxNoOfOtherHolidays" runat="server" placeholder="# of other holidays" Width="100%" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputExceptionalWorkingDays" class="control-label">
                                    <asp:Label ID="lblExceptionalWorkingDays" runat="server" Text="Special Working Days:"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxExceptionalWorkingDays" runat="server" placeholder="Enter special working days" Width="100%" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputNoOfExceptionalWorkingDays" class="control-label">
                                    <asp:Label ID="lblNoOfExceptionalWorkingDays" runat="server" Text="# of Special Working Days:"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxNoOfExceptionalWorkingDays" runat="server" placeholder="# of special working days" Width="100%" TextMode="Number" step="0.01" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <br />
                                <br />
                                <asp:Button ID="btnSave" runat="server" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnSave_Click" />
                                <asp:Button ID="btnUpdate" runat="server" Visible="False" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdate_Click" CssClass="btn btn-info pull-right" ValidationGroup="save" />
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <%-- <div class="form-actions no-margin" style="padding-left: 0px; padding-bottom: 10px; padding-right: 0px; margin-left: 28%">
                            <div class="col-md-11" style="margin-left: -20px;">
                                <asp:Button ID="btnSave" runat="server" Text="Submit" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnSave_Click" />
                                <asp:Button ID="btnUpdate" runat="server" Visible="False" Text="Update" OnClick="btnUpdate_Click" CssClass="btn btn-info pull-right" ValidationGroup="save" />
                            </div>

                        </div>--%>
                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
