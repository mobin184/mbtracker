﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace MBTracker.Pages.Target_and_Bookings
{
    public partial class AddNewBookingV2 : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        int initialLoadForUpdate = 0;
        DataTable dtOcWisePoDetails;
        decimal totalGoods = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyerName, 1, 0);
                //CommonMethods.LoadDropdown(ddlShipmentMonth, "Months", 1, 0);
                LoadShipmentYear();
                //LoadMachinBrand();
                CommonMethods.LoadMachineBrandDropdown(ddlMachineBrand, 1, 0);
                CommonMethods.LoadMachineBrandDropdown(ddlMachineBrandB1, 1, 0);
                CommonMethods.LoadMachineBrandDropdown(ddlMachineBrandB2, 1, 0);
                CommonMethods.LoadMachineBrandDropdown(ddlMachineBrandC, 1, 0);
                CommonMethods.LoadDropdown(ddlFinishingUnit, "FinishingUnits", 1, 0);
                CommonMethods.LoadDropdown(ddlLinkingMachineGauge, "LinkingMachineGauges", 1, 0);
                CommonMethods.LoadDropdown(ddlLinkingMachineGaugeTwo, "LinkingMachineGauges", 1, 0);
                CommonMethods.LoadDropdown(ddlShipmentMode, "ShipmentMode", 1, 0);
                CommonMethods.LoadDropdown(ddlEmbroidary, "YesOrNo", 1, 0);
                CommonMethods.LoadDropdown(ddlWashType, "WashType", 1, 0);
                CommonMethods.LoadDropdown(ddlPrintType, "PrintType", 1, 0);
                CommonMethods.LoadDropdown(ddlAdditionalWork, "AdditionalWork", 1, 0);
                CommonMethods.LoadDropdown(ddlGaugeType, "GaugeType", 1, 0);
             

                if ((CommonMethods.SessionInfo.RoleId == 10 || CommonMethods.SessionInfo.RoleId == 3))
                {
                    ddlStatus.Enabled = false;
                    
                }

                LoadDropdown(ddlStatus, "BookingStatus", 1, 0);
                //LoadProductionMonth();
                if (CommonMethods.SessionInfo.RoleId != 5) 
                {
                    HideDropdownItemByValue(ddlStatus, "3");
                }

                if (Request.QueryString["bookingId"] != null)
                {
                    BookingId = int.Parse(Tools.UrlDecode(Request.QueryString["bookingId"]));
                    initialLoadForUpdate = BookingId;
                    SetInitialRowCountByBookingId(BookingId);
                    PopulateBooking(BookingId);
                }
                else
                {
                    SetInitialRowCount();
                }
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmQtySave")
                {
                    SaveOperation();
                }
                if (parameter == "confirmQtyUpdate")
                {
                    UpdateOperation();
                }
                if (parameter == "confirmDelete")
                {
                    DeleteOperation();
                }
               // SetInitialRowCount();
            }
        }

        private void HideDropdownItemByValue(DropDownList ddl, string valueToHide)
        {
            ListItem itemToRemove = ddl.Items.FindByValue(valueToHide);
            if (itemToRemove != null)
            {
                ddl.Items.Remove(itemToRemove);
            }
        }
        
        int MinQty
        {
            set { ViewState["minQty"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["minQty"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        int BookingId
        {
            set { ViewState["bookingId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["bookingId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        BookingVM UpdateBooking
        {
            set { ViewState["updateBooking"] = value; }
            get
            {
                try
                {
                    return (BookingVM)(ViewState["updateBooking"]);
                }
                catch
                {
                    return new BookingVM();
                }
            }
        }

        private void PopulateBooking(int bookingId)
        {
            var bookingInfo = unitOfWork.GenericRepositories<Booking>().GetByID(bookingId);
            var lstBookingDetails = unitOfWork.GenericRepositories<BookingDetails>().Get(x => x.BookingId == bookingId).ToList();
            List<BookingDetailsVM> lstbookingDetails = new List<BookingDetailsVM>();
            foreach (var item in lstBookingDetails)
            {
                var UpdateBookingDetails = new BookingDetailsVM()
                {
                    ProductionMonthId = item.ProductionMonthId,
                    BookingQuantity = item.BookingQuantity,
                    KnittingBookingQty = item.KnittingBookingQty,
                    LinkingBookingQty = item.LinkingBookingQty,
                    Id = item.Id,
                    FinishingUnitId = item.FinishingUnitId,
                    KnittingUnitId = item.KnittingUnitId
                };
                lstbookingDetails.Add(UpdateBookingDetails);
            }

            UpdateBooking = new BookingVM()
            {
                Id = 1,
                BookingQuantity = bookingInfo.BookingQuantity,
                FinishingUnitId = bookingInfo.FinishingUnitId,
                KnittingTime = bookingInfo.KnittingTime,
                LinkingMachineGaugeId = bookingInfo.LinkingMachineGaugeId,
                LinkingQtyPerDay = bookingInfo.LinkingQtyPerDay ?? 0,
                MachineBrandId = bookingInfo.MachineBrandId,
                lstBookingDetails = lstbookingDetails
            };
            BindStylesByBuyer(bookingInfo.BuyerId);
            CommonMethods.LoadDropdown(ddlBookingForYear, "Years WHERE IsActive = 1", 1, 0);
            BindSeasonByBuyer(bookingInfo.BuyerId, Convert.ToInt32(bookingInfo.StyleId));
            BindDataToUI(bookingInfo);

        }

        private void CreateDataTableForViewState()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("PoNo", typeof(string)));
            dt.Columns.Add(new DataColumn("MonthId", typeof(string)));
            dt.Columns.Add(new DataColumn("DeliveryDate", typeof(string)));
            dt.Columns.Add(new DataColumn("YarnInhouseDate", typeof(string)));
            dt.Columns.Add(new DataColumn("PPApprovalDate", typeof(string)));
            dt.Columns.Add(new DataColumn("SampleApprovalDate", typeof(string)));
            dt.Columns.Add(new DataColumn("KnittingStartDeadlineDate", typeof(string)));
            dt.Columns.Add(new DataColumn("PackingAccesInhouseDate", typeof(string)));

            for (int rowIndex = 0; rowIndex < dtOcWisePoDetails.Rows.Count; rowIndex++)
            {
                dr = dt.NewRow();

                dr["Quantity"] = dtOcWisePoDetails.Rows[rowIndex]["Quantity"];
                dr["PoNo"] = dtOcWisePoDetails.Rows[rowIndex]["PoNo"];
                dr["MonthId"] = dtOcWisePoDetails.Rows[rowIndex]["MonthId"];
                dr["DeliveryDate"] = dtOcWisePoDetails.Rows[rowIndex]["DeliveryDate"];
                dr["YarnInhouseDate"] = dtOcWisePoDetails.Rows[rowIndex]["YarnInhouseDate"];
                dr["PPApprovalDate"] = dtOcWisePoDetails.Rows[rowIndex]["PPApprovalDate"];
                dr["SampleApprovalDate"] = dtOcWisePoDetails.Rows[rowIndex]["SampleApprovalDate"];
                dr["KnittingStartDeadlineDate"] = dtOcWisePoDetails.Rows[rowIndex]["KnittingStartDeadlineDate"];
                dr["PackingAccesInhouseDate"] = dtOcWisePoDetails.Rows[rowIndex]["PackingAccesInhouseDate"];

                dt.Rows.Add(dr);
            }

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

        }

        private void BindDataToUI(Booking bookingInfo)
        {
            //Permission Start
            //ddlStatus.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStatus, bookingInfo.StatusId ?? 0);
            if ((CommonMethods.SessionInfo.RoleId == 10 || CommonMethods.SessionInfo.RoleId == 3) && ((int.Parse(bookingInfo.StatusId.ToString()) == 2) || (int.Parse(bookingInfo.StatusId.ToString()) == 3)))
            {
                pnlBookingInfo1.Enabled = false;
                pnlBookingInfo2.Enabled = false;
                ddlStyleName.Enabled = false;
                //pnlNonEdit3.Enabled = true;
            }
            else
            {
                pnlBookingInfo1.Enabled = true;
                pnlBookingInfo2.Enabled = true;
                if (CommonMethods.SessionInfo.RoleId == 1){ 
                ddlStyleName.Enabled = true;
                }
                // pnlNonEdit3.Enabled = false;
            }
            //Permission End


            //OCPO part Start
            string sql3 = $"Exec usp_GetOCPOInfoByBookingId '{bookingInfo.Id}'";
            dtOcWisePoDetails = unitOfWork.GetDataTableFromSql(sql3);

            if (dtOcWisePoDetails.Rows.Count > 0)
            {
                rptOCPOEntryInfo.Visible = true;
                rptOCPOEntryInfo.DataSource = dtOcWisePoDetails;
                rptOCPOEntryInfo.DataBind();
                CreateDataTableForViewState();
            }
            else
            {
                SetInitialRowCount();
            }
            //OCPO part End
            tbxBookingDate.Text = bookingInfo.BookingDate.ToString("yyyy-MM-dd");
            ddlBuyerName.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyerName, bookingInfo.BuyerId);
            ddlStyleName.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyleName, bookingInfo.StyleId ?? 0);
            ddlBuyerName.Enabled = false;
            //ddlStyleName.Enabled = false;
            tbxYarnComposition.Text = bookingInfo.YarnComposition + "";
            tbxYarnSupplierName.Text = bookingInfo.Beneficiary + "";
            ddlBookingForYear.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBookingForYear, bookingInfo.SeasonYearId);
            ddlBuyersSeason.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyersSeason, bookingInfo.SeasonId ?? 0);
            ddlShipmentMode.SelectedIndex = CommonMethods.MatchDropDownItem(ddlShipmentMode, bookingInfo.ShipmentModeId);
            ddlMachineBrand.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrand, bookingInfo.MachineBrandId);
            ddlMachineBrandB1.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrandB1, bookingInfo.MachineBrandB1Id);
            ddlMachineBrandB2.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrandB2, bookingInfo.MachineBrandB2Id);
            ddlMachineBrandC.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrandC, bookingInfo.MachineBrandCId);
            tbxKnittingMachineGauge.Text = bookingInfo.KnittingMachineGauge;
            ddlGaugeType.SelectedIndex = CommonMethods.MatchDropDownItem(ddlGaugeType, bookingInfo.GaugeTypeId ?? 0);
            ddlLinkingMachineGauge.SelectedIndex = CommonMethods.MatchDropDownItem(ddlLinkingMachineGauge, bookingInfo.LinkingMachineGaugeId);
            ddlFinishingUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlFinishingUnit, bookingInfo.FinishingUnitId);
            tbxKnittingTime.Text = bookingInfo.KnittingTime + "";
            tbxLinkingTime.Text = bookingInfo.LinkingTime + "";
            tbxFinishingTime.Text = bookingInfo.FinishingTime + "";
            ddlYarnInHanksOrCone.SelectedValue = bookingInfo.YarnInHanksOrCone;
            ddlYarnLocalOrImported.SelectedValue = bookingInfo.YarnLocalOrImported;
            tbxWeightPerDz.Text = bookingInfo.WeightPerDz + "";
            ddlLinkingMachineGaugeTwo.SelectedIndex = CommonMethods.MatchDropDownItem(ddlLinkingMachineGaugeTwo, bookingInfo.SecondLinkingMachineGaugeId ?? 0);
            ddlAutoLinking.SelectedValue = bookingInfo.AutoLinking;
            tbxTrimmingOutput.Text = bookingInfo.TrimmingOutput + "";
            tbxMendingOutput.Text = bookingInfo.MendingOutput + "";
            tbxIroningOutput.Text = bookingInfo.IroningOutput + "";
            tbxRemarks.Text = bookingInfo.Remarks;
            ddlEmbroidary.SelectedIndex = CommonMethods.MatchDropDownItem(ddlEmbroidary, bookingInfo.EmbroidaryId ?? 0);
            ddlWashType.SelectedIndex = CommonMethods.MatchDropDownItem(ddlWashType, bookingInfo.WashTypeId ?? 0);
            ddlPrintType.SelectedIndex = CommonMethods.MatchDropDownItem(ddlPrintType, bookingInfo.PrintTypeId ?? 0);
            ddlAdditionalWork.SelectedIndex = CommonMethods.MatchDropDownItem(ddlAdditionalWork, bookingInfo.AdditionalWorkId ?? 0);
            ddlStatus.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStatus, bookingInfo.StatusId ?? 0);
            userActionTitle.Text = "Update Booking";
            btnSave.Visible = false;
            btnUpdate.Visible = true;
            tbxBookingNumber.Text = bookingInfo.BookingNumber;
        }


        protected void CalculateGrandTotal(object sender, EventArgs e)
        {
            TextBox tbxSender = (TextBox)sender;
            decimal grandQtyTotal = 0;
        
            for (int i = 0; i < this.rptOCPOEntryInfo.Items.Count; i++)
            {
                TextBox tbxQuantity = (TextBox)this.rptOCPOEntryInfo.Items[i].FindControl("tbxQuantity");
               
                if (tbxQuantity.Text != "" )
                {
                    decimal quantity = Convert.ToDecimal(tbxQuantity.Text);
                  
                    grandQtyTotal = grandQtyTotal + quantity;
                   
                }

            }

            Label lblTotalQuantity = (Label)rptOCPOEntryInfo.Controls[rptOCPOEntryInfo.Controls.Count - 1].Controls[0].FindControl("lblTotalQuantity");
            
            lblTotalQuantity.Text = Math.Round(grandQtyTotal, 2).ToString();
           
        }


        private void LoadShipmentYear()
        {
            ListItem[] lstYear = new ListItem[] { new ListItem() { Text = DateTime.Now.Year + "", Value = DateTime.Now.Year + "" }, new ListItem() { Text = DateTime.Now.Year + 1 + "", Value = DateTime.Now.Year + 1 + "" } };
            //ddlShipmentYear.Items.AddRange(lstYear);
            //ddlShipmentYear.SelectedIndex = 0;
            //ddlShipmentYear.DataTextField = "Text";
            //ddlShipmentYear.DataValueField = "Value";
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyerName.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyerName.SelectedValue));
                CommonMethods.LoadDropdown(ddlBookingForYear, "Years WHERE IsActive = 1", 1, 0);
            }
            else
            {
                ddlStyleName.Items.Clear();
                ddlBookingForYear.Items.Clear();
                ddlBuyersSeason.Items.Clear();
            }
        }

        protected void ddlBookingForYear_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBookingForYear.SelectedValue != "" && ddlStyleName.SelectedValue != "")
            {
                BindSeasonByBuyer(Convert.ToInt32(ddlBuyerName.SelectedValue), Convert.ToInt32(ddlStyleName.SelectedValue));
                var buyerId = Convert.ToInt32(ddlBuyerName.SelectedValue);
                var OCNumberTable = unitOfWork.GetDataTableFromSql($"SELECT TOP 1 BuyerWiseBookingId FROM Booking WHERE BuyerId = " + buyerId+ " ORDER BY Id DESC");
                if(ddlBuyersSeason.SelectedItem != null) { 
                if (OCNumberTable.Rows.Count > 0)
                {
                        if (Convert.ToInt32(OCNumberTable.Rows[0]["BuyerWiseBookingId"]) != 0)
                        {
                            int OcNumber = (Convert.ToInt32(OCNumberTable.Rows[0]["BuyerWiseBookingId"]) + 1);
                            tbxBookingNumber.Text = "Masihata." + ddlBuyerName.SelectedItem + "." + OcNumber + "." + ddlBuyersSeason.SelectedItem + "";
                        }
                        else
                        {
                            tbxBookingNumber.Text = "Masihata." + ddlBuyerName.SelectedItem + ".001" + ddlBuyersSeason.SelectedItem + "";
                        }

                    }
                    else
                    {
                        tbxBookingNumber.Text = "Masihata." + ddlBuyerName.SelectedItem + ".001" + ddlBuyersSeason.SelectedItem + "";
                    }
                   
                }


                tbxBookingNumber.Enabled = false;

                //If Exist Check start
                var booking = new Booking()
                {
                    BuyerId = Convert.ToInt32(ddlBuyerName.SelectedValue),
                    StyleId = Convert.ToInt32(ddlStyleName.SelectedValue),
                    SeasonYearId = Convert.ToInt32(ddlBookingForYear.SelectedValue)
                };

                var isExist = unitOfWork.GenericRepositories<Booking>().Get(x => x.BuyerId == booking.BuyerId && x.StyleId == booking.StyleId && x.SeasonYearId == booking.SeasonYearId).Any();
                if (isExist)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Booking already exist for this Buyer, Year and Style.')", true);
                }
                    //If Exist Check End
                }
            else
            {
                ddlBuyersSeason.Items.Clear();
            }
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyleName, buyerId, "BuyerStyles", 1, 0);
        }

        private void BindSeasonByBuyer(int buyerId, int StyleId)
        {
            CommonMethods.LoadDropdownSeasonByBuyerAndStyleId(ddlBuyersSeason, buyerId, StyleId, 1, 0);
            ddlBuyersSeason.Enabled = false;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var currentMonth = DateTime.Now.Month;
                var currentYear = DateTime.Now.Year;
                var isBookingQuantityEntered = false;


                if (ddlBuyersSeason.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Buyers Season.')", true);
                }
                else if (ddlShipmentMode.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Shipment Mode.')", true);
                }

                else if (tbxKnittingMachineGauge.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Garments Gauge.')", true);
                }
                else if (ddlLinkingMachineGauge.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Linking Machine Gauge.')", true);
                }
                else if (ddlYarnInHanksOrCone.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Yarn Winding Type.')", true);
                }
                else if (tbxWeightPerDz.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Weight Per Pcs.')", true);
                }
                else if (ddlAutoLinking.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter AutoLinking.')", true);
                }
                else if (ddlEmbroidary.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Embroidary.')", true);
                }
                else if (ddlWashType.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Wash Type.')", true);
                }
                else if (!CheckItems())
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter TNA Information correctly.')", true);
                }
                else if (ddlPrintType.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Print Type.')", true);
                }
                else
                {
                    string title = $"Overbooking Quantity:";
                    string msg = "";
                    SaveOperation();
                }

                    
               
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "');", true);
            }
        }
        public void SaveOperation()
        {
            try
            {
                int OcNumber = 0;
                var BookingNumber = "";
                var OCNumberTable = unitOfWork.GetDataTableFromSql($"SELECT TOP 1 BuyerWiseBookingId FROM Booking WHERE BuyerId = " + Convert.ToInt32(ddlBuyerName.SelectedValue) + " ORDER BY Id DESC");
                if (OCNumberTable.Rows.Count > 0)
                {
                    if (Convert.ToInt32(OCNumberTable.Rows[0]["BuyerWiseBookingId"]) != 0)
                    {
                        OcNumber = (Convert.ToInt32(OCNumberTable.Rows[0]["BuyerWiseBookingId"]) + 1);
                        BookingNumber = "Masihata." + ddlBuyerName.SelectedItem + "." + OcNumber + "." + ddlBuyersSeason.SelectedItem + "";
                    }
                    else
                    {
                        OcNumber = 1;
                        BookingNumber = "Masihata." + ddlBuyerName.SelectedItem + ".001" + ddlBuyersSeason.SelectedItem + "";
                    }

                }
                else
                {
                    OcNumber = 1;
                    BookingNumber = "Masihata." + ddlBuyerName.SelectedItem + ".001" + ddlBuyersSeason.SelectedItem + "";
                }

                var booking = new Booking()
                {
                    BookingDate = Convert.ToDateTime(tbxBookingDate.Text),
                    BuyerId = Convert.ToInt32(ddlBuyerName.SelectedValue),
                    StyleId = Convert.ToInt32(ddlStyleName.SelectedValue),
                    YarnComposition = tbxYarnComposition.Text,
                    Beneficiary = tbxYarnSupplierName.Text,
                    SeasonYearId = Convert.ToInt32(ddlBookingForYear.SelectedValue),
                    SeasonId = string.IsNullOrEmpty(ddlBuyersSeason.SelectedValue) ? (int?)null : Int32.Parse(ddlBuyersSeason.SelectedValue),
                    ShipmentModeId = int.TryParse(ddlShipmentMode.SelectedValue, out int result) ? result : 0,
                    MachineBrandId = Convert.ToInt32(ddlMachineBrand.SelectedValue),
                    MachineBrandB1Id = Convert.ToInt32(ddlMachineBrandB1.SelectedValue),
                    MachineBrandB2Id = Convert.ToInt32(ddlMachineBrandB2.SelectedValue),
                    MachineBrandCId = Convert.ToInt32(ddlMachineBrandC.SelectedValue),
                    KnittingMachineGauge = tbxKnittingMachineGauge.Text,
                    LinkingMachineGaugeId = Convert.ToInt32(ddlLinkingMachineGauge.SelectedValue),
                    FinishingUnitId = Convert.ToInt32(ddlFinishingUnit.SelectedValue),
                    GaugeTypeId = Convert.ToInt32(ddlGaugeType.SelectedValue),
                    KnittingTime = Convert.ToInt32(tbxKnittingTime.Text),
                    LinkingTime = tbxLinkingTime.Text,
                    FinishingTime = tbxFinishingTime.Text,
                    //LinkingQtyPerDay = string.IsNullOrEmpty(tbxLinkingPcsperDay.Text) ? (int?)null : Int32.Parse(tbxLinkingPcsperDay.Text),
                    YarnInHanksOrCone = ddlYarnInHanksOrCone.SelectedValue,
                    YarnLocalOrImported = ddlYarnLocalOrImported.SelectedValue,
                    WeightPerDz = decimal.Parse(string.IsNullOrEmpty(tbxWeightPerDz.Text) ? "0" : tbxWeightPerDz.Text),
                    SecondLinkingMachineGaugeId = int.Parse(string.IsNullOrEmpty(ddlLinkingMachineGaugeTwo.SelectedValue) ? "0" : ddlLinkingMachineGaugeTwo.SelectedValue),
                    AutoLinking = ddlAutoLinking.SelectedValue,
                    TrimmingOutput = int.Parse(string.IsNullOrEmpty(tbxTrimmingOutput.Text) ? "0" : tbxTrimmingOutput.Text),
                    MendingOutput = int.Parse(string.IsNullOrEmpty(tbxMendingOutput.Text) ? "0" : tbxMendingOutput.Text),
                    IroningOutput = int.Parse(string.IsNullOrEmpty(tbxIroningOutput.Text) ? "0" : tbxIroningOutput.Text),
                    EmbroidaryId = Convert.ToInt32(ddlEmbroidary.SelectedValue),
                    WashTypeId = Convert.ToInt32(ddlWashType.SelectedValue),
                    PrintTypeId = Convert.ToInt32(ddlPrintType.SelectedValue),
                    AdditionalWorkId = Convert.ToInt32(ddlAdditionalWork.SelectedValue),
                    StatusId = Convert.ToInt32(ddlStatus.SelectedValue),
                    BuyerWiseBookingId = OcNumber,
                    BookingNumber = BookingNumber,
                    Remarks = tbxRemarks.Text,
                    CreateDate = DateTime.Now,
                    CreatedBy = CommonMethods.SessionInfo.UserName
                };

                var isExist = unitOfWork.GenericRepositories<Booking>().Get(x => x.BuyerId == booking.BuyerId && x.StyleId == booking.StyleId && x.SeasonYearId == booking.SeasonYearId ).Any();
                if (!isExist)
                {
                    unitOfWork.GenericRepositories<Booking>().Insert(booking);
                    unitOfWork.Save();
                

                //OC And PO Start
                int currentIndex = 0;
                List<OcWisePo> listOCWisePo = new List<OcWisePo>();

                for (int rowIndex = 0; rowIndex < rptOCPOEntryInfo.Items.Count; rowIndex++)
                {
                    TextBox tbxQuantity = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxQuantity");
                    TextBox tbxPoNo = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxPoNo");
                    DropDownList ddlMonth = (DropDownList)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("ddlMonth");
                    TextBox tbxDeliveryDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxDeliveryDate");
                    TextBox tbxYarnInhouseDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxYarnInhouseDate");
                    TextBox tbxPPApprovalDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxPPApprovalDate");
                    TextBox tbxKnittingStartDeadlineDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxKnittingStartDeadlineDate");
                    TextBox tbxPackingAccesInhouseDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxPackingAccesInhouseDate");

                    if (!string.IsNullOrEmpty(tbxQuantity.Text))
                    {
                        // var calculatedValue = currentIndex + (int)ViewState["CurrentRowCount"];

                        var OcWisePos = new OcWisePo()
                        {
                            BookingId = booking.Id,
                            BuyerId = booking.BuyerId,
                            StyleId = booking.StyleId ?? 0,
                            Quantity = double.Parse(tbxQuantity.Text),
                            PoNo = tbxPoNo.Text,
                            MonthId = int.Parse(ddlMonth.SelectedValue),
                            DeliveryDate = DateTime.TryParse(tbxDeliveryDate.Text, out DateTime DeliveryDate) ? (DateTime?)DeliveryDate : null,
                            YarnInhouseDate = DateTime.TryParse(tbxYarnInhouseDate.Text, out DateTime yarnInhouseDate) ? (DateTime?)yarnInhouseDate : null,
                            PPApprovalDate = DateTime.TryParse(tbxPPApprovalDate.Text, out DateTime PPApprovalDate) ? (DateTime?)PPApprovalDate : null,
                            KnittingStartDeadlineDate = DateTime.TryParse(tbxKnittingStartDeadlineDate.Text, out DateTime KnittingStartDeadlineDate) ? (DateTime?)KnittingStartDeadlineDate : null,
                            PackingAccesInhouseDate = DateTime.TryParse(tbxPackingAccesInhouseDate.Text, out DateTime PackingAccesInhouseDate) ? (DateTime?)PackingAccesInhouseDate : null,
                            CreateDate = DateTime.Now,
                            CreatedBy = CommonMethods.SessionInfo.UserName
                        };

                        listOCWisePo.Add(OcWisePos);

                        // Increment currentIndex for the next iteration
                        currentIndex++;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Booking Quantity.')", true);
                    }
                }

                foreach (var ocinfo in listOCWisePo)
                {
                    unitOfWork.GenericRepositories<OcWisePo>().Insert(ocinfo);
                }

                unitOfWork.Save();

                //OC And Po End
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('AddNewBookingV2.aspx');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Booking already exist.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    var currentMonth = DateTime.Now.Month;
                    var currentYear = DateTime.Now.Year;
                    var isBookingQuantityEntered = false;

                    if (ddlBuyersSeason.SelectedValue == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Buyers Season.')", true);
                    }
                    else if (ddlShipmentMode.SelectedValue == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Shipment Mode.')", true);
                    }
                    else if (tbxKnittingMachineGauge.Text == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Garments Gauge.')", true);
                    }
                    else if (ddlLinkingMachineGauge.SelectedValue == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Linking Machine Gauge.')", true);
                    }
                    
                    else if (ddlYarnInHanksOrCone.SelectedValue == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Yarn Winding Type.')", true);
                    }
                    else if (tbxWeightPerDz.Text == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Weight Per Pcs.')", true);
                    }
                    else if (ddlAutoLinking.SelectedValue == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter AutoLinking.')", true);
                    }
                    else if (ddlEmbroidary.SelectedValue == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Embroidary.')", true);
                    }
                    else if (ddlWashType.SelectedValue == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Wash Type.')", true);
                    }
                    else if (!CheckItems())
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter TNA Information correctly.')", true);
                    }
                    else if (ddlPrintType.SelectedValue == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Print Type.')", true);
                    }
                    else
                        {
                            string title = $"Available Capacity:";
                            string msg = "";
                            UpdateOperation();
                        }

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        private void UpdateOperation()
        {
            try
            {
                var bookingInfo = unitOfWork.GenericRepositories<Booking>().GetByID(BookingId);
               // var bookingHistory= new BookingHistory();
                if (bookingInfo != null)
                {
                    bookingInfo.BookingDate = Convert.ToDateTime(tbxBookingDate.Text);
                    bookingInfo.BuyerId = Convert.ToInt32(ddlBuyerName.SelectedValue);
                    bookingInfo.StyleId = Convert.ToInt32(ddlStyleName.SelectedValue);
                    bookingInfo.YarnComposition = tbxYarnComposition.Text;
                    bookingInfo.Beneficiary = tbxYarnSupplierName.Text;
                    bookingInfo.SeasonYearId = Convert.ToInt32(ddlBookingForYear.SelectedValue);
                    bookingInfo.SeasonId = string.IsNullOrEmpty(ddlBuyersSeason.SelectedValue) ? (int?)null : Int32.Parse(ddlBuyersSeason.SelectedValue);
                    bookingInfo.ShipmentModeId = Convert.ToInt32(ddlShipmentMode.SelectedValue);
                    bookingInfo.MachineBrandId = Convert.ToInt32(ddlMachineBrand.SelectedValue);
                    bookingInfo.MachineBrandB1Id = Convert.ToInt32(ddlMachineBrandB1.SelectedValue);
                    bookingInfo.MachineBrandB2Id = Convert.ToInt32(ddlMachineBrandB2.SelectedValue);
                    bookingInfo.MachineBrandCId = Convert.ToInt32(ddlMachineBrandC.SelectedValue);
                    bookingInfo.KnittingMachineGauge = tbxKnittingMachineGauge.Text;
                    bookingInfo.GaugeTypeId = Convert.ToInt32(ddlGaugeType.SelectedValue);
                    bookingInfo.LinkingMachineGaugeId = Convert.ToInt32(ddlLinkingMachineGauge.SelectedValue);
                    bookingInfo.FinishingUnitId = Convert.ToInt32(ddlFinishingUnit.SelectedValue);
                    bookingInfo.KnittingTime = Convert.ToInt32(tbxKnittingTime.Text);
                    bookingInfo.LinkingTime = tbxLinkingTime.Text;
                    bookingInfo.FinishingTime = tbxFinishingTime.Text;
                    bookingInfo.YarnInHanksOrCone = ddlYarnInHanksOrCone.SelectedValue;
                    bookingInfo.YarnLocalOrImported = ddlYarnLocalOrImported.SelectedValue;
                    bookingInfo.WeightPerDz = decimal.Parse(string.IsNullOrEmpty(tbxWeightPerDz.Text) ? "0" : tbxWeightPerDz.Text);
                    bookingInfo.SecondLinkingMachineGaugeId = int.Parse(string.IsNullOrEmpty(ddlLinkingMachineGaugeTwo.SelectedValue) ? "0" : ddlLinkingMachineGaugeTwo.SelectedValue);
                    bookingInfo.AutoLinking = ddlAutoLinking.SelectedValue;
                    bookingInfo.TrimmingOutput = int.Parse(string.IsNullOrEmpty(tbxTrimmingOutput.Text) ? "0" : tbxTrimmingOutput.Text);
                    bookingInfo.MendingOutput = int.Parse(string.IsNullOrEmpty(tbxMendingOutput.Text) ? "0" : tbxMendingOutput.Text);
                    bookingInfo.IroningOutput = int.Parse(string.IsNullOrEmpty(tbxIroningOutput.Text) ? "0" : tbxIroningOutput.Text);
                    bookingInfo.EmbroidaryId = Convert.ToInt32(ddlEmbroidary.SelectedValue);
                    bookingInfo.WashTypeId = Convert.ToInt32(ddlWashType.SelectedValue);
                    bookingInfo.PrintTypeId = Convert.ToInt32(ddlPrintType.SelectedValue);
                    bookingInfo.AdditionalWorkId = Convert.ToInt32(ddlAdditionalWork.SelectedValue);
                    bookingInfo.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                    bookingInfo.Remarks = tbxRemarks.Text;
                    bookingInfo.UpdateDate = DateTime.Now;
                    bookingInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;

                    var totalBookingQty = 0;
                    
                    unitOfWork.GenericRepositories<Booking>().Update(bookingInfo);
                    UpdateOcAndPoInfo(BookingId, bookingInfo.BuyerId, bookingInfo.StyleId ?? 0);
                    unitOfWork.Save();
                    
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewBookings.aspx');", true);
                    
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Invalid Booking Id!');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        private bool CheckItems()
        {
            bool hasItem = false;
            for (int rowIndex = 0; rowIndex < rptOCPOEntryInfo.Items.Count; rowIndex++)
            {
                DropDownList ddlMonth = (DropDownList)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("ddlMonth");
                TextBox tbxQuantity = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxQuantity");

                try
                {
                   double Quantity = double.Parse(tbxQuantity.Text);

                    if (Quantity > double.Parse("0.00") && ddlMonth.SelectedValue != "") 
                    {
                        hasItem = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    hasItem = false;
                    break;
                }
            }

            return hasItem;
        }

        public class AvailableQty
        {
            public int AvailableKnittingQty { get; set; }
            public int AvailableLinkingQty { get; set; }
        }

        [Serializable]
        public class BookingVM
        {
            public int Id { get; set; }
            public int BookingQuantity { get; set; }
            public int MachineBrandId { get; set; }
            public int LinkingMachineGaugeId { get; set; }
            public int FinishingUnitId { get; set; }
            public int KnittingTime { get; set; }
           
            public int LinkingQtyPerDay { get; set; }
            public List<BookingDetailsVM> lstBookingDetails { get; set; }
        }

        [Serializable]
        public class BookingDetailsVM
        {
            public int ProductionMonthId { get; set; }
            public int BookingQuantity { get; set; }
            public int KnittingBookingQty { get; set; }
            public int LinkingBookingQty { get; set; }
            public int Id { get; set; }
            public int? FinishingUnitId { get; set; }
            public int? KnittingUnitId { get; set; }

        }

        protected void rptBookingMonths_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (BookingId > 0)
                {
                    var productionMonthId = int.Parse(((Label)e.Item.FindControl("lblProductionMonthId")).Text);
                    var tbxBookingQuantity = (TextBox)e.Item.FindControl("tbxBookingQuantity");
                    foreach (var item in UpdateBooking.lstBookingDetails)
                    {
                        if (productionMonthId == item.ProductionMonthId)
                        {
                            tbxBookingQuantity.Text = string.IsNullOrEmpty(tbxBookingQuantity.Text)?item.BookingQuantity + "" : int.Parse(tbxBookingQuantity.Text)+ item.BookingQuantity + "";
                        }
                    }
                }
            }
            
        }

        public static void LoadDropdown(DropDownList ddl, string table, int dataTextField, int dataValueField, int selectedIndexValue = 0)
        {
            DataTable dt = new CommonManager().LoadDropdown(table);
            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            //ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = selectedIndexValue;
        }

        int RowId
        {
            set { ViewState["rowId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["rowId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {
                        TextBox tbxQuantity = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxQuantity");
                        TextBox tbxPoNo = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxPoNo");
                        DropDownList ddlMonth = (DropDownList)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("ddlMonth");
                        TextBox tbxDeliveryDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxDeliveryDate");
                        TextBox tbxYarnInhouseDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxYarnInhouseDate");
                        TextBox tbxPPApprovalDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxPPApprovalDate");
                        TextBox tbxKnittingStartDeadlineDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxKnittingStartDeadlineDate");
                        TextBox tbxPackingAccesInhouseDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxPackingAccesInhouseDate");

                        dtCurrentTable.Rows[rowIndex]["Quantity"] = tbxQuantity.Text;
                        dtCurrentTable.Rows[rowIndex]["PoNo"] = tbxPoNo.Text;
                        dtCurrentTable.Rows[rowIndex]["MonthId"] = ddlMonth.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["DeliveryDate"] = tbxDeliveryDate.Text;
                        dtCurrentTable.Rows[rowIndex]["YarnInhouseDate"] = tbxYarnInhouseDate.Text;
                        dtCurrentTable.Rows[rowIndex]["PPApprovalDate"] = tbxPPApprovalDate.Text;
                        dtCurrentTable.Rows[rowIndex]["KnittingStartDeadlineDate"] = tbxKnittingStartDeadlineDate.Text;
                        dtCurrentTable.Rows[rowIndex]["PackingAccesInhouseDate"] = tbxPackingAccesInhouseDate.Text;

                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["Quantity"] = string.Empty;
                            drCurrentRow["PoNo"] = string.Empty;
                            drCurrentRow["MonthId"] = string.Empty;
                            drCurrentRow["DeliveryDate"] = string.Empty;
                            drCurrentRow["YarnInhouseDate"] = string.Empty;
                            drCurrentRow["PPApprovalDate"] = string.Empty;
                            drCurrentRow["SampleApprovalDate"] = string.Empty;
                            drCurrentRow["KnittingStartDeadlineDate"] = string.Empty;
                            drCurrentRow["PackingAccesInhouseDate"] = string.Empty;
                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;
                    this.rptOCPOEntryInfo.DataSource = dtCurrentTable;
                    this.rptOCPOEntryInfo.DataBind();

                }
            }
            this.SetPreviousData();
          
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        TextBox tbxQuantity = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxQuantity");
                        TextBox tbxPoNo = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxPoNo");
                        DropDownList ddlMonth = (DropDownList)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("ddlMonth");
                        TextBox tbxDeliveryDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxDeliveryDate");
                        TextBox tbxYarnInhouseDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxYarnInhouseDate");
                        TextBox tbxPPApprovalDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxPPApprovalDate");
                        TextBox tbxKnittingStartDeadlineDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxKnittingStartDeadlineDate");
                        TextBox tbxPackingAccesInhouseDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxPackingAccesInhouseDate");
                        
                        CommonMethods.LoadDropdown(ddlMonth, "Months", 1, 0);
                        ddlMonth.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMonth, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["MonthId"].ToString()) ? "0" : dt.Rows[i]["MonthId"].ToString())));
                        tbxQuantity.Text = dt.Rows[i]["Quantity"].ToString();
                        tbxPoNo.Text = dt.Rows[i]["PoNo"].ToString();
                        tbxDeliveryDate.Text = dt.Rows[i]["DeliveryDate"].ToString();
                        tbxYarnInhouseDate.Text = dt.Rows[i]["YarnInhouseDate"].ToString();
                        tbxPPApprovalDate.Text = dt.Rows[i]["PPApprovalDate"].ToString();
                        tbxKnittingStartDeadlineDate.Text = dt.Rows[i]["KnittingStartDeadlineDate"].ToString();
                        tbxPackingAccesInhouseDate.Text = dt.Rows[i]["PackingAccesInhouseDate"].ToString();

                        rowIndex++;
                    }
                }
            }
        }

        protected void rptOCPOEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var qty = DataBinder.Eval(e.Item.DataItem, "Quantity").ToString();
                var tbxQuantity = (TextBox)e.Item.FindControl("tbxQuantity");
                tbxQuantity.Text = qty.ToString();

                if ((CommonMethods.SessionInfo.RoleId == 10 || CommonMethods.SessionInfo.RoleId == 3))
                {
                    TextBox tbxKnittingStartDeadlineDate = e.Item.FindControl("tbxKnittingStartDeadlineDate") as TextBox;
                    tbxKnittingStartDeadlineDate.Enabled = false;

                }

                var ddlMonth = (DropDownList)e.Item.FindControl("ddlMonth");
                CommonMethods.LoadDropdown(ddlMonth, "Months", 1, 0);

                if (initialLoadForUpdate > 0)
                {
                    var MonthId = DataBinder.Eval(e.Item.DataItem, "MonthId").ToString();
                    
                    ddlMonth.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMonth, int.Parse(string.IsNullOrEmpty(MonthId) ? "0" : MonthId));

                    //int selectedMonthId = 0;
                    //if (ddlMonth.SelectedValue != "")
                    //{
                    //    selectedMonthId = int.Parse(ddlMonth.SelectedValue);
                    //}

                }

                if (qty != "")
                {
                    totalGoods += decimal.Parse(qty);
                }

            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {

                Label lblTotalQuantity = (Label)e.Item.FindControl("lblTotalQuantity");
                // Label lblGrandTotalAmount = (Label)e.Item.FindControl("lblGrandTotalAmount");


                lblTotalQuantity.Text = totalGoods.ToString();
                // lblGrandTotalAmount.Text = grandTotalAmount.ToString();

            }

        }

        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
          
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("PoNo", typeof(string)));
            dt.Columns.Add(new DataColumn("MonthId", typeof(string)));
            dt.Columns.Add(new DataColumn("DeliveryDate", typeof(string)));
            dt.Columns.Add(new DataColumn("YarnInhouseDate", typeof(string)));
            dt.Columns.Add(new DataColumn("PPApprovalDate", typeof(string)));
            dt.Columns.Add(new DataColumn("SampleApprovalDate", typeof(string)));
            dt.Columns.Add(new DataColumn("KnittingStartDeadlineDate", typeof(string)));
            dt.Columns.Add(new DataColumn("PackingAccesInhouseDate", typeof(string)));

            dr = dt.NewRow();

            dr["Quantity"] = string.Empty;
            dr["PoNo"] = string.Empty;
            dr["MonthId"] = string.Empty;
            dr["DeliveryDate"] = string.Empty;
            dr["YarnInhouseDate"] = string.Empty;
            dr["PPApprovalDate"] = string.Empty;
            dr["SampleApprovalDate"] = string.Empty;
            dr["KnittingStartDeadlineDate"] = string.Empty;
            dr["PackingAccesInhouseDate"] = string.Empty;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptOCPOEntryInfo.DataSource = dt;
            rptOCPOEntryInfo.DataBind();

        }


        private void SetInitialRowCountByBookingId(int BookingId)
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
          
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("PoNo", typeof(string)));
            dt.Columns.Add(new DataColumn("MonthId", typeof(string)));
            dt.Columns.Add(new DataColumn("DeliveryDate", typeof(string)));
            dt.Columns.Add(new DataColumn("YarnInhouseDate", typeof(string)));
            dt.Columns.Add(new DataColumn("PPApprovalDate", typeof(string)));
            dt.Columns.Add(new DataColumn("SampleApprovalDate", typeof(string)));
            dt.Columns.Add(new DataColumn("KnittingStartDeadlineDate", typeof(string)));
            dt.Columns.Add(new DataColumn("PackingAccesInhouseDate", typeof(string)));

            dr = dt.NewRow();

            dr["Quantity"] = string.Empty;
            dr["PoNo"] = string.Empty;
            dr["MonthId"] = string.Empty;
            dr["DeliveryDate"] = string.Empty;
            dr["YarnInhouseDate"] = string.Empty;
            dr["PPApprovalDate"] = string.Empty;
            dr["SampleApprovalDate"] = string.Empty;
            dr["KnittingStartDeadlineDate"] = string.Empty;
            dr["PackingAccesInhouseDate"] = string.Empty;
            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;
            rptOCPOEntryInfo.DataSource = dt;
            rptOCPOEntryInfo.DataBind();

        }
        protected void btnRemove_Command(object sender, CommandEventArgs e)
        {
            try
            {
                RowId = Convert.ToInt32(e.CommandArgument.ToString());
                //var IsDeletable = bool.Parse(unitOfWork.GetSingleStringValue($"EXEC usp_CheckBookingIsDeleteable {BookingId}"));
                var IsDeletable = true;
                if (IsDeletable)
                {
                    var title = "Warning";
                    var msg = "Are you sure you want to Delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                }
                else
                {
                    var title = "Warning";
                    var msg = "Sorry, you can not delete it.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }

        }

        protected void btnCopy_Click(object sender, CommandEventArgs e)
        {
            int rowNumberWillCopy = Convert.ToInt32(e.CommandArgument.ToString());

            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    var Quantity = "";
                    var PoNo = "";
                    var MonthId = "";
                    var DeliveryDate = "";
                    var YarnInhouseDate = "";
                    var PPApprovalDate = "";
                    var SampleApprovalDate = "";
                    var KnittingStartDeadlineDate = "";
                    var PackingAccesInhouseDate = "";

                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {

                        TextBox tbxQuantity = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxQuantity");
                        TextBox tbxPoNo = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxPoNo");
                        DropDownList ddlMonth = (DropDownList)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("ddlMonth");
                        TextBox tbxDeliveryDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxDeliveryDate");
                        TextBox tbxYarnInhouseDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxYarnInhouseDate");
                        TextBox tbxPPApprovalDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxPPApprovalDate");
                        //TextBox tbxSampleApprovalDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxSampleApprovalDate");
                        TextBox tbxKnittingStartDeadlineDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxKnittingStartDeadlineDate");
                        TextBox tbxPackingAccesInhouseDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxPackingAccesInhouseDate");

                        dtCurrentTable.Rows[rowIndex]["Quantity"] = tbxQuantity.Text;
                        dtCurrentTable.Rows[rowIndex]["PoNo"] = tbxPoNo.Text;
                        dtCurrentTable.Rows[rowIndex]["MonthId"] = ddlMonth.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["DeliveryDate"] = tbxDeliveryDate.Text;
                        dtCurrentTable.Rows[rowIndex]["YarnInhouseDate"] = tbxYarnInhouseDate.Text;
                        dtCurrentTable.Rows[rowIndex]["PPApprovalDate"] = tbxPPApprovalDate.Text;
                        dtCurrentTable.Rows[rowIndex]["KnittingStartDeadlineDate"] = tbxKnittingStartDeadlineDate.Text;
                        dtCurrentTable.Rows[rowIndex]["PackingAccesInhouseDate"] = tbxPackingAccesInhouseDate.Text;

                        if (rowIndex == rowNumberWillCopy)
                        {

                            Quantity = tbxQuantity.Text;
                            PoNo = tbxPoNo.Text;
                            MonthId = ddlMonth.SelectedValue;
                            DeliveryDate = tbxDeliveryDate.Text;
                            YarnInhouseDate = tbxYarnInhouseDate.Text;
                            PPApprovalDate = tbxPPApprovalDate.Text;
                            KnittingStartDeadlineDate = tbxKnittingStartDeadlineDate.Text;
                            PackingAccesInhouseDate = tbxPackingAccesInhouseDate.Text;
                        }


                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["Quantity"] = Quantity;
                            drCurrentRow["PoNo"] = PoNo;
                            drCurrentRow["MonthId"] = MonthId;
                            drCurrentRow["DeliveryDate"] = DeliveryDate;
                            drCurrentRow["YarnInhouseDate"] = YarnInhouseDate;
                            drCurrentRow["PPApprovalDate"] = PPApprovalDate;
                            drCurrentRow["KnittingStartDeadlineDate"] = KnittingStartDeadlineDate;
                            drCurrentRow["PackingAccesInhouseDate"] = PackingAccesInhouseDate;
                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptOCPOEntryInfo.DataSource = dtCurrentTable;
                    this.rptOCPOEntryInfo.DataBind();

                }
            }
            this.SetPreviousData();
           
        }

        private void DeleteOperation()
        {
            //int rowWillRemove = Convert.ToInt32(e.CommandArgument.ToString());

            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    dtCurrentTable.Rows.RemoveAt(RowId);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptOCPOEntryInfo.DataSource = dtCurrentTable;
                    this.rptOCPOEntryInfo.DataBind();
                }
            }
            this.SetPreviousData();
           // CalculateGrandTotal();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Row deleted successfully.');", true);
        }


        private void UpdateOcAndPoInfo(int BookingId, int BuyerId, int StyleId)
        {

            var OcAndPos = unitOfWork.GenericRepositories<OcWisePo>().Get(x => x.BookingId == BookingId).ToList();

            foreach (var OcAndPo in OcAndPos)
            {
                unitOfWork.GenericRepositories<OcWisePo>().Delete(OcAndPo.Id);
            }

            List<OcWisePo> listOCWisePo = new List<OcWisePo>();

            for (int rowIndex = 0; rowIndex < rptOCPOEntryInfo.Items.Count; rowIndex++)
            {
                TextBox tbxQuantity = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxQuantity");
                TextBox tbxPoNo = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxPoNo");
                DropDownList ddlMonth = (DropDownList)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("ddlMonth");
                TextBox tbxDeliveryDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxDeliveryDate");
                TextBox tbxYarnInhouseDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxYarnInhouseDate");
                TextBox tbxPPApprovalDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxPPApprovalDate");
                TextBox tbxKnittingStartDeadlineDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxKnittingStartDeadlineDate");
                TextBox tbxPackingAccesInhouseDate = (TextBox)this.rptOCPOEntryInfo.Items[rowIndex].FindControl("tbxPackingAccesInhouseDate");

                if (!string.IsNullOrEmpty(tbxQuantity.Text)) //&& ddlUnit.SelectedValue != ""
                {
                  
                    var OcWisePos = new OcWisePo()
                    {
                        BookingId = BookingId,
                        BuyerId = BuyerId,
                        StyleId = StyleId,
                        Quantity = double.Parse(tbxQuantity.Text),
                        PoNo = tbxPoNo.Text,
                        MonthId = int.Parse(ddlMonth.SelectedValue),
                        DeliveryDate = DateTime.TryParse(tbxDeliveryDate.Text, out DateTime DeliveryDate) ? (DateTime?)DeliveryDate : null,
                        YarnInhouseDate = DateTime.TryParse(tbxYarnInhouseDate.Text, out DateTime yarnInhouseDate) ? (DateTime?)yarnInhouseDate : null,
                        PPApprovalDate = DateTime.TryParse(tbxPPApprovalDate.Text, out DateTime PPApprovalDate) ? (DateTime?)PPApprovalDate : null,
                        //SampleApprovalDate = DateTime.TryParse(tbxSampleApprovalDate.Text, out DateTime SampleApprovalDate) ? (DateTime?)SampleApprovalDate : null,
                        KnittingStartDeadlineDate = DateTime.TryParse(tbxKnittingStartDeadlineDate.Text, out DateTime KnittingStartDeadlineDate) ? (DateTime?)KnittingStartDeadlineDate : null,
                        PackingAccesInhouseDate = DateTime.TryParse(tbxPackingAccesInhouseDate.Text, out DateTime PackingAccesInhouseDate) ? (DateTime?)PackingAccesInhouseDate : null,
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };


                    listOCWisePo.Add(OcWisePos);
                }

            }

            foreach (var OCWisePo in listOCWisePo)
            {
                unitOfWork.GenericRepositories<OcWisePo>().Insert(OCWisePo);
            }

        }

    }
}