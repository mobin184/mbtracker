﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddNewBookingV2.aspx.cs" Inherits="MBTracker.Pages.Target_and_Bookings.AddNewBookingV2" Async="true" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .btn-info {
            margin: 1px 0px 1px 3px;
        }

        /*.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 1px;
            line-height: 1.42857143;
            vertical-align: top;Development Machine Brand
            border-top: 1px solid #ddd;
        }*/
        .form-horizontal .control-label {
            float: left;
            width: 170px;
            padding-top: 1px;
            text-align: right;
        }
        .ui-dialog-content h6{
            font-size:10.8px;
        }
    </style>
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="userActionTitle" Text="Add a New Booking"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 30px; padding-bottom: 0px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <asp:Panel ID="pnlBookingInfo1" runat="server">
                        <div class="col-md-5">
                            <div class="control-group">
                                <label for="inputBookingDate" class="control-label">
                                    <asp:Label ID="lblBookingDate" runat="server" Text="Booking Date:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxBookingDate" runat="server" placeholder="Enter Booking Date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="tbxBookingDate">
			                        <span style="font-weight: 700; color: #CC0000">Enter Booking Date.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputBuyerName" class="control-label">
                                    <asp:Label ID="lblBuyerName" runat="server" Text="Buyer Name:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlBuyerName" runat="server" placeholder="Select Buyer Name" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlBuyerName">
			                        <span style="font-weight: 700; color: #CC0000">Please select Buyer Name.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputStyleName" class="control-label">
                                    <asp:Label ID="lblStyleName" runat="server" Text="Style Name:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlStyleName" runat="server" placeholder="Select Style Name" Width="100%"  CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlStyleName">
			                        <span style="font-weight: 700; color: #CC0000">Please select a Style.</span>
                                    </asp:RequiredFieldValidator>

                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label for="inputYarnComposition" class="control-label">
                                    <asp:Label ID="lblYarnComposition" runat="server" Text="Yarn Composition:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxYarnComposition" runat="server" placeholder="Enter Yarn Composition" Width="100%" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="tbxYarnComposition">
			                        <span style="font-weight: 700; color: #CC0000">Enter YarnComposition.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="Yarn Supplier Name" class="control-label">
                                    <asp:Label ID="Label13" runat="server" Text="Yarn Supplier Name:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxYarnSupplierName" runat="server" placeholder="Enter Yarn Supplier Name" Width="100%" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="tbxYarnSupplierName">
			                        <span style="font-weight: 700; color: #CC0000">Enter Yarn Supplier Name.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            
                            <%--<div class="control-group">
                                <label for="inputYarn" class="control-label">
                                    <asp:Label ID="lblYarnComposition" runat="server" Text="YarnComposition:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlYarnComposition" runat="server" AutoPostBack="true" placeholder="Select Yarn Composition" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlYarnComposition">
			                        <span style="font-weight: 700; color: #CC0000">Please select YarnComposition.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>--%>

                            <%--<div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblBeneficiaryName" runat="server" Text="Yarn Supplier Name:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlYarnAndAccssSuppliers" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>--%>


                            <div class="control-group">
                                <label for="inputBookingForYear" class="control-label">
                                    <asp:Label ID="lblBookingForYear" runat="server" Text="Booking for Year:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlBookingForYear" runat="server" placeholder="Select Year (Season)" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlBookingForYear_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlBookingForYear">
			                        <span style="font-weight: 700; color: #CC0000">Please select Booking For Year.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputBuyer’sSeason" class="control-label">
                                    <asp:Label ID="Label3" runat="server" Text="Style’s Season:"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlBuyersSeason" runat="server"  placeholder="Select Buyer’s Season" Width="100%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            

                             <div class="control-group">
                                <label for="inputYarn" class="control-label">
                                    <asp:Label ID="lblShipmentMode" runat="server" Text="Shipment Mode:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlShipmentMode" runat="server" AutoPostBack="true" placeholder="Select Yarn Composition" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlShipmentMode">
			                        <span style="font-weight: 700; color: #CC0000">Please select Shipment Mode.</span>
                                    </asp:RequiredFieldValidator>--%>
                                </div>
                            </div>


                            <div class="control-group">
                                <label for="inputMachineBrand" class="control-label">
                                    <asp:Label ID="lblMachineBrand" runat="server" Text="Development Machine Brand:(Option A)"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlMachineBrand" runat="server" AutoPostBack="true" placeholder="Select Machine Brand" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlMachineBrand">
			                        <span style="font-weight: 700; color: #CC0000">Please select Development Machine Brand (Option A).</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputMachineBrand" class="control-label">
                                    <asp:Label ID="lblMachineBrandB1" runat="server" Text="MC Flexibility/MC Mapping:(Option B-1)"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlMachineBrandB1" runat="server" AutoPostBack="true" placeholder="Select Machine Brand" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlMachineBrand">
			                        <span style="font-weight: 700; color: #CC0000">Please select MC Flexibility/MC Mapping (Option B-1).</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputMachineBrand" class="control-label">
                                    <asp:Label ID="lblMachineBrandB2" runat="server" Text="MC Flexibility/MC Mapping:(Option B-2)"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlMachineBrandB2" runat="server" AutoPostBack="true" placeholder="Select Machine Brand" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlMachineBrand">
			                        <span style="font-weight: 700; color: #CC0000">Please select MC Flexibility/MC Mapping (Option B-2).</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputMachineBrand" class="control-label">
                                    <asp:Label ID="lblMachineBrandC" runat="server" Text="MC Flexibility/MC Mapping:(Option C)"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlMachineBrandC" runat="server" AutoPostBack="true" placeholder="Select Machine Brand" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlMachineBrand">
			                        <span style="font-weight: 700; color: #CC0000">Please select MC Flexibility/MC Mapping (Option C).</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputKnittingMachineGauge" class="control-label">
                                    <asp:Label ID="lblKnittingMachineGauge" runat="server" Text="Garments Gauge:"></asp:Label>
                                     <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxKnittingMachineGauge" runat="server" placeholder="Enter GG" Width="100%" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="tbxKnittingMachineGauge">
			                        <span style="font-weight: 700; color: #CC0000">Please Enter Knitting Machine Gauge.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                           
                        </div>

                        <div class="col-md-5">

                            <div class="control-group">
                                <label for="inputGaugeType" class="control-label">
                                    <asp:Label ID="Label12" runat="server" Text="Gauge Type"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlGaugeType" runat="server" AutoPostBack="true" placeholder="Select Machine Brand" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlMachineBrand">
			                        <span style="font-weight: 700; color: #CC0000">Please select Gauge Type.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                             <div class="control-group">
                                <label for="inputLinkingMachineGauge" class="control-label">
                                    <asp:Label ID="lblLinkingMachineGauge" runat="server" Text="Linking Machine Gauge:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    
                                    <asp:DropDownList ID="ddlLinkingMachineGauge" runat="server" AutoPostBack="true" placeholder="Select Linking Machine Gauge" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlLinkingMachineGauge">
			                        <span style="font-weight: 700; color: #CC0000">Please select linking machine gauge.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                             <div class="control-group">
                                <label for="inputLinkingMachineGaugeTwo" class="control-label">
                                    <asp:Label ID="Label5" runat="server" Text="Linking Machine Gauge (2):"></asp:Label>
                                    
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlLinkingMachineGaugeTwo" runat="server" AutoPostBack="true" placeholder="Select Linking Machine Gauge" Width="100%" CssClass="form-control"></asp:DropDownList>
                                   
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputFinishingUnit" class="control-label">
                                    <asp:Label ID="lblFinishingUnit" runat="server" Text="Finishing Unit:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlFinishingUnit" runat="server" AutoPostBack="true" placeholder="Select Finishing Unit" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlFinishingUnit">
			                        <span style="font-weight: 700; color: #CC0000">Please select Finishing Unit.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                           
                            <div class="control-group">
                                <label for="inputKnittingTime" class="control-label">
                                    <asp:Label ID="Label2" runat="server" Text="Knitting Time (Minutes):"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxKnittingTime" runat="server" AutoPostBack="true" placeholder="Enter SMV" Width="100%" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="tbxKnittingTime">
			                        <span style="font-weight: 700; color: #CC0000">Enter Knitting Time (Minutes).</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="Linking Time" class="control-label">
                                    <asp:Label ID="Label29" runat="server" Text="Linking Time:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxLinkingTime" runat="server" AutoPostBack="true" placeholder="Enter Linking Time" Width="100%" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="tbxLinkingTime">
			                        <span style="font-weight: 700; color: #CC0000">Linking Time</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="Finishing Time" class="control-label">
                                    <asp:Label ID="Label30" runat="server" Text="Finishing Time:"></asp:Label>
                                    <%--<span style="font-weight: 700; color: #CC0000">*</span>--%>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxFinishingTime" runat="server" AutoPostBack="true" placeholder="Enter Finishing Time" Width="100%" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator11" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="tbxFinishingTime">
			                        <span style="font-weight: 700; color: #CC0000">Finishing Time</span>
                                    </asp:RequiredFieldValidator>--%>
                                </div>
                            </div>

                           
                           
                            <div class="control-group">
                                <label for="" class="control-label">
                                    <asp:Label ID="Label4" runat="server" Text="Yarn Winding Type:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlYarnInHanksOrCone" runat="server" placeholder="" Width="100%" CssClass="form-control">
                                        <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Hanks" Value="Hanks"></asp:ListItem>
                                        <asp:ListItem Text="Cone" Value="Cone"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group" style="display:none;">
                                <label for="" class="control-label">
                                    <asp:Label ID="Label6" runat="server" Text="Yarn Local/Imported:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlYarnLocalOrImported" runat="server" placeholder="" Width="100%" CssClass="form-control">
                                        <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Local" Value="Local"></asp:ListItem>
                                        <asp:ListItem Text="Imported" Value="Imported"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="" class="control-label">
                                    <asp:Label ID="Label7" runat="server" Text="Weight Per Pcs (lbs):"></asp:Label>
                                     <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxWeightPerDz" runat="server" placeholder="Enter Weight per Pcs (lbs)" Width="100%" TextMode="Number" step="0.01" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="tbxWeightPerDz">
			                        <span style="font-weight: 700; color: #CC0000"> Enter Weight PerDz.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="" class="control-label">
                                    <asp:Label ID="Label8" runat="server" Text="Auto Linking:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlAutoLinking" runat="server" placeholder="" Width="100%" CssClass="form-control">
                                        <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                        <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group" style="display:none;">
                                <label for="" class="control-label">
                                    <asp:Label ID="Label9" runat="server" Text="Trimming Output (10h):"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxTrimmingOutput" TextMode="Number" runat="server" placeholder="Enter Trimming Output" Width="100%" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group" style="display:none;">
                                <label for="" class="control-label">
                                    <asp:Label ID="Label10" runat="server" Text="Mending Output (10h):"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxMendingOutput" TextMode="Number" runat="server" placeholder="Enter Mending Output" Width="100%" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group" style="display:none;">
                                <label for="" class="control-label">
                                    <asp:Label ID="Label11" runat="server" Text="Ironing Output (10h):"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxIroningOutput" TextMode="Number" runat="server" placeholder="Enter Ironing Output" Width="100%" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            

                            <div class="control-group">
                                <label for="Embroidary" class="control-label">
                                    <asp:Label ID="lblEmbroidary" runat="server" Text="Embroidary:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlEmbroidary" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="WashType" class="control-label">
                                    <asp:Label ID="lblWashType" runat="server" Text="Wash Type:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlWashType" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="PrintType" class="control-label">
                                    <asp:Label ID="lblPrintType" runat="server" Text="Print Type:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlPrintType" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="AdditionalWork" class="control-label">
                                    <asp:Label ID="lblAdditionalWork" runat="server" Text="Additional Work:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlAdditionalWork" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlAdditionalWork">
			                        <span style="font-weight: 700; color: #CC0000">Please select Additional Work.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="BookingNumber" class="control-label">
                                    <asp:Label ID="lblBookingNumber" runat="server" Text="Booking Number:"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxBookingNumber" runat="server" placeholder="Enter Booking Number" Width="100%" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            
                        </div>
                        
                       </asp:Panel>

                        

                        <div style="width: 100%; float: left; border-top: 1px solid #ddd;padding-top: 10px;">
                            <div class="col-md-12">
                                <h5 style="color:#00897B">TNA Information: </h5>
                            </div>
                            <asp:Panel ID="pnlBookingInfo2" runat="server">


                                 <!--OC AND PO Part Start--->
                            <div class="col-md-12" id="divOCANDPO" runat="server">
                                <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                        <asp:Label ID="lblPIEntry" runat="server" Text="Booking Data:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    </div>
                            <div class="control-group">
                                    
                                    <div class="controls" style="overflow-x: auto; margin-left:0px;">
                                        <asp:Repeater ID="rptOCPOEntryInfo" runat="server" OnItemDataBound="rptOCPOEntryInfo_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table22" class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th><asp:Label ID="lblSL" runat="server" Text="SL No"></asp:Label></th>
                                                           
                                                            <th>
                                                                <asp:Label ID="lblQty" runat="server" Text="Booking Qty"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblPoNo" runat="server" Text="Po No"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblMonth" runat="server" Text="Booking For Month"></asp:Label></th>
                                                            
                                                            <th>
                                                                <asp:Label ID="lblDeliveryDate" runat="server" Text="Ex Factory Date"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblYarnInhouseDate" runat="server" Text="Yarn In-house Date"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblPPApprovalDate" runat="server" Text="PP Approval Date"></asp:Label></th>
                                                            
                                                            <th>
                                                                <asp:Label ID="lblKnittingStartDeadlineDate" runat="server" Text="Knitting Start Deadline Date"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblPackingAccesInhouseDate" runat="server" Text="Packing Accessories Inhouse Date"></asp:Label></th>
                                                            
                                                             <th>
                                                                <asp:Label ID="Action" runat="server" Text="Action"></asp:Label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 3%; text-align:center;vertical-align:middle; font-weight:bold"><%# (Container.ItemIndex+1) %></td>
                                                  
                                                    <td style="width: 9%; padding-top: 18px">
                                                        <asp:TextBox ID="tbxQuantity" Width="100%" runat="server" TextMode="Number" OnTextChanged="CalculateGrandTotal" Text='<%#Eval("Quantity") %>' ></asp:TextBox>
                                                    </td>

                                                    <td style="width: 9%; padding-top: 18px">
                                                        <asp:TextBox ID="tbxPoNo" Width="100%" runat="server"  Text='<%#Eval("PoNo") %>'></asp:TextBox>
                                                    </td>

                                                    <td style="width: 12%; padding-top: 18px">
                                                        <asp:DropDownList ID="ddlMonth" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </td>
                                                   
                                                    <td style="width: 12%; padding-top: 18px">
                                                        <asp:TextBox TextMode="Date" ID="tbxDeliveryDate" Width="100%" runat="server"  Text='<%#Eval("DeliveryDate") %>' ></asp:TextBox>
                                                    </td>
                                                   <td style="width: 12%; padding-top: 18px">
                                                        <asp:TextBox TextMode="Date" ID="tbxYarnInhouseDate" Width="100%" runat="server" Text='<%#Eval("YarnInhouseDate") %>' ></asp:TextBox>
                                                    </td>
                                                    <td style="width: 12%; padding-top: 18px">
                                                        <asp:TextBox TextMode="Date" ID="tbxPPApprovalDate" Width="100%" runat="server"  Text='<%#Eval("PPApprovalDate") %>'></asp:TextBox>
                                                    </td>
                                                   
                                                    <td style="width: 12%; padding-top: 18px">
                                                        <asp:TextBox TextMode="Date" ID="tbxKnittingStartDeadlineDate" Width="100%" runat="server"  Text='<%#Eval("KnittingStartDeadlineDate") %>'></asp:TextBox>
                                                    </td>
                                                    <td style="width: 12%; padding-top: 18px">
                                                        <asp:TextBox TextMode="Date" ID="tbxPackingAccesInhouseDate" Width="100%" runat="server"  Text='<%#Eval("PackingAccesInhouseDate") %>'></asp:TextBox>
                                                    </td>
                                                    
                                                    <td style="min-width:70px; padding-top: 18px">
                                                       <asp:LinkButton ID="btnCopy" runat="server" Display="Dynamic" style="font-size:13px;padding: 4px 5px; line-height: 13px; " CssClass="btn btn-sm btn-warning" CommandArgument="<%# (Container.ItemIndex) %>"  AutoPostBack="true" OnCommand="btnCopy_Click" Text="Copy."  />
                                                        <asp:LinkButton ID="btnRemove" runat="server" Display="Dynamic" style="font-size:13px;padding: 4px 5px; line-height: 13px; " CssClass="btn btn-sm btn-danger" CommandArgument="<%# (Container.ItemIndex) %>"  AutoPostBack="true" OnCommand="btnRemove_Command" Text="Del."  />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <td style="font-weight: 500; text-align: center; background-color: lavender"></td>
                                                
                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>

                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>

                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                
                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                               
                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                               
                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                <%-- <td style="font-weight: 500; text-align: left; background-color: lavender"></td>--%>
                                                 <td style="font-weight: 500; text-align: left; background-color: lavender">
                                                     <asp:Label ID="lblTotalQuantity" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                 </td>
                                                 <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                 <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                </tbody>
                                            </table>
                                           

                                            <div class="pull-right" style="margin-right: 0px">
                                                <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="81px" Text="Add Row" OnClick="btnAddRow_Click" />
                                            </div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                </div>
                            <!--OC AND PO Part END---->

                            <div class="col-md-8">
                                <div class="control-group">
                                    <label for="" class="control-label">
                                        <asp:Label ID="Label28" runat="server" Text="Remarks:"></asp:Label>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox TextMode="MultiLine" Rows="2" Height="60" ID="tbxRemarks" runat="server" Width="100%" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="Label33" runat="server" Text="Status:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control" Width="100%">
                                    </asp:DropDownList>
                                </div>
                               </div>
                            </div>
                            
                           </asp:Panel>

                            
                            <div class="col-md-4" style="text-align:right">
                                <div class="control-group">
                                <br />
                                <div class="controls-row">
                                    <asp:Button ID="btnSave" runat="server" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSave_Click" CssClass="btn btn-info pull-right" ValidationGroup="save" />
                                    <asp:Button ID="btnUpdate" runat="server" Visible="False" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdate_Click" CssClass="btn btn-info pull-right" ValidationGroup="save" />
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <%--<div class="form-actions no-margin" style="padding-left: 0px; padding-bottom: 10px; padding-right: 0px; margin-left: 28%">
                            <div class="col-md-11" style="margin-left: -20px;">
                                <asp:Button ID="btnSave" runat="server" Text="Submit" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnSave_Click"  />
                                <asp:Button ID="btnUpdate" runat="server" Visible="False" Text="Update" OnClick="btnUpdate_Click" CssClass="btn btn-info pull-right" ValidationGroup="save" />
                            </div>

                        </div>--%>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-4 col-md-offset-4">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
