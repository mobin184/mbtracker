﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Target_and_Bookings
{
    public partial class ViewBookings : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        DataTable dtOcWisePoDetails;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                if (Request.QueryString["showInfo"] != null)
                {
                    var bookingId = int.Parse(Tools.UrlDecode(Request.QueryString["showInfo"]));
                    ShowBookingDetails(bookingId);

                    FilterDiv.Visible = false;
                    //showFilterButtonDiv.Visible = false;
                    bookingListDiv.Visible = false;
                }
                else
                {
                   // FilterDiv.Visible = true;
                    //showFilterButtonDiv.Visible = true;
                    bookingListDiv.Visible = true;


                    EnableDisableEditButton();
                    EnableDisableDeleteButton();
                    CommonMethods.LoadBuyerDropdownForUser(ddlBuyerName, 1, 0);
                    //CommonMethods.LoadDropdown(ddlShipmentMonth, "Months", 1, 0);
                   // LoadShipmentYear();
                    LoadMachinBrand();
                    CommonMethods.LoadDropdown(ddlFinishingUnit, "FinishingUnits", 1, 0);
                    BindData();
                }
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteOperation();
                }
            }
        }

        

        private void EnableDisableEditButton()
        {
            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Merchandiser");

            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewBookings", 1, 1);

        }
        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");

            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewBookings", 2, 1);
        }

        private void LoadShipmentYear()
        {
            ListItem[] lstYear = new ListItem[] { new ListItem() { Text = "---Select---", Value = "" }, new ListItem() { Text = DateTime.Now.Year + "", Value = DateTime.Now.Year + "" }, new ListItem() { Text = DateTime.Now.Year + 1 + "", Value = DateTime.Now.Year + 1 + "" } };
            ddlShipmentYear.Items.AddRange(lstYear);
            ddlShipmentYear.SelectedIndex = 0;
            ddlShipmentYear.DataTextField = "Text";
            ddlShipmentYear.DataValueField = "Value";
        }

        private void LoadMachinBrand()
        {
            var itemList = new OrderManager().GetMachineBrands();
            List<ListItem> lstMachinBrand = new List<ListItem>() { new ListItem() { Text = "---Select---", Value = "" } };
            lstMachinBrand.AddRange(itemList.Rows.OfType<DataRow>().Select(a => new ListItem() { Text = a[1].ToString(), Value = a[0].ToString() }).ToList());
            ddlMachineBrand.DataTextField = "MachineBrand";
            ddlMachineBrand.DataValueField = "Id";
            ddlMachineBrand.Items.AddRange(lstMachinBrand.ToArray());
            ddlMachineBrand.SelectedIndex = 0;
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyerName.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyerName.SelectedValue));
            }
            else
            {
                ddlStyleName.Items.Clear();
            }
        }

        private void BindStylesByBuyer(int buyerId)
        {

            CommonMethods.LoadDropdownById(ddlStyleName, buyerId, "BuyerStyles", 1, 0);

        }
        private void BindData()
        {
            var userId = CommonMethods.SessionInfo.UserId;
            var userRole = CommonMethods.SessionInfo.RoleId;
            if (userRole == 5 )
            {
                DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetAllBookingsForPlanning '','','','','','',{userId}");
                if (dt.Rows.Count > 0)
                {
                    rpt.DataSource = dt;
                    rpt.DataBind();
                    lblNoDataFound.Visible = false;
                }
                else
                {
                    rpt.DataSource = null;
                    rpt.DataBind();
                    lblNoDataFound.Text = "No booking found!";
                    lblNoDataFound.Visible = true;
                }
            }
            else
            {
                DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetAllBookings '','','','','','',{userId}");
                if (dt.Rows.Count > 0)
                {
                    rpt.DataSource = dt;
                    rpt.DataBind();
                    lblNoDataFound.Visible = false;
                }
                else
                {
                    rpt.DataSource = null;
                    rpt.DataBind();
                    lblNoDataFound.Text = "No booking found!";
                    lblNoDataFound.Visible = true;
                }
            }
            
            
        }

        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            int bookingId = Convert.ToInt32(e.CommandArgument.ToString());
            Response.Redirect("AddNewBookingV2.aspx?bookingId=" + Tools.UrlEncode(bookingId + ""));
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            detailsDiv.Visible = false;
            var BuyerId = string.IsNullOrEmpty(ddlBuyerName.SelectedValue) ? (int?)null : Int32.Parse(ddlBuyerName.SelectedValue);
            var StyleId = string.IsNullOrEmpty(ddlStyleName.SelectedValue) ? (int?)null : Int32.Parse(ddlStyleName.SelectedValue);
            var ShipmentMonthId = string.IsNullOrEmpty(ddlShipmentMonth.SelectedValue) ? (int?)null : Int32.Parse(ddlShipmentMonth.SelectedValue);
            var ShipmentYear = ddlShipmentYear.SelectedValue;
            var MachineBrandId = string.IsNullOrEmpty(ddlMachineBrand.SelectedValue) ? (int?)null : Int32.Parse(ddlMachineBrand.SelectedValue);
            var FinishingUnitId = string.IsNullOrEmpty(ddlFinishingUnit.SelectedValue) ? (int?)null : Int32.Parse(ddlFinishingUnit.SelectedValue);

            var userId = CommonMethods.SessionInfo.UserId;
            DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetAllBookings '{BuyerId}','{StyleId}','{ShipmentMonthId}','{ShipmentYear}','{MachineBrandId}','{FinishingUnitId}',{userId}");
            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                lblNoDataFound.Visible = false;
            }
            else
            {
                rpt.DataSource = null;
                rpt.DataBind();
                lblNoDataFound.Text = "No booking found!";
                lblNoDataFound.Visible = true;
            }
        }
        protected void btnShowFilter_Click(object sender, EventArgs e)
        {
            FilterDiv.Visible = true;
            //btnShowFilter.Visible = false;
            //btnHideFilter.Visible = true;
        }
        protected void btnHineFilter_Click(object sender, EventArgs e)
        {
            FilterDiv.Visible = false;
            //btnShowFilter.Visible = true;
            //btnHideFilter.Visible = false;
        }

        protected void btnResetFilter_Click(object sender, EventArgs e)
        {
            ddlBuyerName.SelectedIndex = 0;
            ddlStyleName.Items.Clear();
            ddlShipmentMonth.SelectedIndex = 0;
            ddlShipmentYear.SelectedIndex = 0;
            ddlMachineBrand.SelectedIndex = 0;
            ddlFinishingUnit.SelectedIndex = 0;
            BindData();
        }

        protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        {

            int bookingId = Convert.ToInt32(e.CommandArgument.ToString());
            ShowBookingDetails(bookingId);
            
        }


        void ShowBookingDetails(int bookingId)
        {

            try
            {
                detailsDiv.Visible = true;

                var bookingInfo = unitOfWork.GetDataTableFromSql($" Exec [usp_GetBookingInfoById] {bookingId}");
                if (bookingInfo.Rows.Count > 0)
                {
                    int buyerId = bookingInfo.Rows[0].Field<int>("BuyerId");

                    if (CommonMethods.HasBuyerAssignedToUser(buyerId, CommonMethods.SessionInfo.UserId))
                    {
                        lblBookingDate.Text = bookingInfo.Rows[0].Field<DateTime>("BookingDate").ToString("dd-MMM-yyyy");
                        lblBuyer.Text = bookingInfo.Rows[0].Field<string>("Buyer");
                        lblStyle.Text = bookingInfo.Rows[0].Field<string>("Style");
                        //lblBookingQuantity.Text = bookingInfo.Rows[0].Field<int>("BookingQuantity") + "";
                        lblYarnComposition.Text = bookingInfo.Rows[0].Field<string>("YarnCompositionName");
                        lblBeneficiaryName.Text = bookingInfo.Rows[0].Field<string>("SupplierName");
                        lblBookingForYear.Text = bookingInfo.Rows[0].Field<string>("SeasonYear");
                        lblBuyerSeason.Text = bookingInfo.Rows[0].Field<string>("Season");
                        //lblShipmentMonth.Text = bookingInfo.Rows[0].Field<string>("ShipmentMonth");
                        //lblShipmentYear.Text = bookingInfo.Rows[0].Field<string>("ShipmentYear");
                        lblShipmentMode.Text = bookingInfo.Rows[0].Field<string>("ShipmentModeName");
                        lblMachineBrand.Text = bookingInfo.Rows[0].Field<string>("MachineBrand");
                        lblMachineBrandOptionB1.Text = bookingInfo.Rows[0].Field<string>("MachineBrandOption1");
                        lblMachineBrandOptionB2.Text = bookingInfo.Rows[0].Field<string>("MachineBrandOption2");
                        lblMachineBrandOptionC.Text = bookingInfo.Rows[0].Field<string>("MachineBrandOptionC");
                        lblKnittingMachineGauge.Text = bookingInfo.Rows[0].Field<string>("KnittingMachineGauge");
                        lblBookingNumber.Text = bookingInfo.Rows[0].Field<string>("BookingNumber");
                        lblKnittingTime.Text = bookingInfo.Rows[0].Field<int?>("KnittingTime") + "";
                        lblFinishingUnit.Text = bookingInfo.Rows[0].Field<string>("FinishingUnit");
                        //lblProductionMonth.Text = bookingInfo.Rows[0].Field<string>("ProductionMonth");
                        //lblKnittingStartDate.Text = bookingInfo.Rows[0].Field<DateTime?>("KnittingStartDate")?.ToString("dd-MM-yyyy");
                        lblLinkingMachineGauge.Text = bookingInfo.Rows[0].Field<int?>("LinkingMachineGauge") + "";
                        // lblLinkingStartDate.Text = bookingInfo.Rows[0].Field<DateTime?>("LinkingStartDate")?.ToString("dd-MM-yyyy");
                        //lblLinkingPcsperDay.Text = bookingInfo.Rows[0].Field<int?>("LinkingQtyPerDay") + "";
                        //lblSampleApprovalDate.Text = bookingInfo.Rows[0].Field<DateTime?>("SampleApprovalDate")?.ToString("dd-MM-yyyy");
                        //lblYarnInHouseDate.Text = bookingInfo.Rows[0].Field<DateTime?>("YarnInHouseDate")?.ToString("dd-MM-yyyy");
                        //lblPPApprovalDate.Text = bookingInfo.Rows[0].Field<DateTime?>("PPApprovalDate")?.ToString("dd-MM-yyyy");
                        lblYarnInHanksOrCone.Text = bookingInfo.Rows[0].Field<string>("YarnInHanksOrCone") + "";
                       // lblYarnLocalOrImported.Text = bookingInfo.Rows[0].Field<string>("YarnLocalOrImported") + "";
                        //lblWeightPerDz.Text = bookingInfo.Rows[0].Field<decimal?>("WeightPerDz") + "";
                        //lblLinkingMachineGaugeTwo.Text = bookingInfo.Rows[0].Field<int?>("LinkingMachineGaugeTwo") + "";
                        lblAutoLinking.Text = bookingInfo.Rows[0].Field<string>("AutoLinking") + "";
                        lblAdditionalWork.Text = bookingInfo.Rows[0].Field<string>("AdditionalWorkName") + "";
                        // lblTrimmingOutput.Text = bookingInfo.Rows[0].Field<int?>("TrimmingOutput") + "";
                        // lblMendingOutput.Text = bookingInfo.Rows[0].Field<int?>("MendingOutput") + "";
                        // lblIroningOutput.Text = bookingInfo.Rows[0].Field<int?>("IroningOutput") + "";
                        // lblAdditionalWork.Text = bookingInfo.Rows[0].Field<string>("AdditionalWork") + "";


                        lblRemarks.Text = bookingInfo.Rows[0].Field<string>("Remarks") + "";



                        ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown()", true);
                    }
                    else
                    {
                        lblNotAuthorizedToView.Visible = true;
                        detailsDiv.Visible = false;
                    }

                    //OC Wise PO Start
                    //OCPO part Start
                    string sql3 = $"Exec usp_GetOCPOInfoByBookingId '{bookingId}'";
                    dtOcWisePoDetails = unitOfWork.GetDataTableFromSql(sql3);

                    if (dtOcWisePoDetails.Rows.Count > 0)
                    {
                        rptOCPOEntryInfo.Visible = true;

                        rptOCPOEntryInfo.DataSource = dtOcWisePoDetails;
                        rptOCPOEntryInfo.DataBind();

                       // CreateDataTableForViewState();
                    }
                    
                    //OCPO part End
                    //OC Wise PO End

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Booking details not found!');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }

        }



        int BookingId
        {
            set { ViewState["bookingId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["bookingId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }
        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            try
            {
                BookingId = Convert.ToInt32(e.CommandArgument.ToString());
                //var IsDeletable = bool.Parse(unitOfWork.GetSingleStringValue($"EXEC usp_CheckBookingIsDeleteable {BookingId}"));
                var IsDeletable = true;
                if (IsDeletable)
                {
                    var title = "Warning";
                    var msg = "Are you sure you want to delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                }
                else
                {
                    var title = "Warning";
                    var msg = "Sorry, you can not delete it.<br />There was time & action plans for this booking.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }
        private void DeleteOperation()
        {
            try
            {
                var bookingInfo = unitOfWork.GenericRepositories<Booking>().GetByID(BookingId);
                var bookingDetails = new List<BookingDetails>();
                //var OcWisePo = new List<OcWisePo>();
                var tnaItems = new List<TimeAndActionPlans>();
                tnaItems = bookingInfo.TimeAndActionPlans.ToList();
                bookingDetails = bookingInfo.BookingDetails.ToList();
                //OcWisePo = bookingInfo.OcWisePo.ToList();
                var notes = new List<TimeAndActionPlans_Notes>();
                foreach (var tna in tnaItems)
                {
                    notes = tna.TimeAndActionPlans_Notes.ToList();
                    foreach (var note in notes)
                    {
                        unitOfWork.GenericRepositories<TimeAndActionPlans_Notes>().Delete(note);
                    }
                }
                foreach (var tna in tnaItems)
                {
                    unitOfWork.GenericRepositories<TimeAndActionPlans>().Delete(tna);
                }

                foreach (var item in bookingDetails)
                {
                    unitOfWork.GenericRepositories<BookingDetails>().Delete(item);
                }

                //foreach (var OCWisep in OcWisePo)
                //{
                //    unitOfWork.GenericRepositories<OcWisePo>().Delete(OCWisep);
                //}

                unitOfWork.GenericRepositories<Booking>().Delete(BookingId);
                unitOfWork.Save();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "returnToPage('ViewBookings.aspx');", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        protected void rptOCPOEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {


                //var qty = DataBinder.Eval(e.Item.DataItem, "Quantity").ToString();
                //var deliverydate = DataBinder.Eval(e.Item.DataItem, "DeliveryDate").ToString();




                // var ddlOrders = (DropDownList)e.Item.FindControl("ddlOrders");
                // //CommonMethods.LoadDropdown(ddlOrders, "Orders WHERE IsActive = 1", 6, 0);

                // var tbxQty = (TextBox)e.Item.FindControl("tbxQty");

                // var tbxDeliveryDate = (TextBox)e.Item.FindControl("tbxDeliveryDate");


                // tbxQty.Text = qty.ToString();
                // tbxDeliveryDate.Text = deliverydate.ToString();

                // if (initialLoadForUpdate > 0)
                // {



                //     var orderId = DataBinder.Eval(e.Item.DataItem, "OrderId").ToString();


                //     ddlOrders.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrders, int.Parse(string.IsNullOrEmpty(orderId) ? "0" : orderId));


                //int selectedStyleId = 0;
                //if (ddlStyles.SelectedValue != "")
                //{
                //    selectedStyleId = int.Parse(ddlStyles.SelectedValue);
                //}

                //int selectedOrderId = 0;

                //if (ddlOrders.SelectedValue != "")
                //{
                //    selectedOrderId = int.Parse(ddlOrders.SelectedValue);
                //}

                //   ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse(string.IsNullOrEmpty(itemId) ? "0" : itemId));

                // }

            }

        }

    }
}