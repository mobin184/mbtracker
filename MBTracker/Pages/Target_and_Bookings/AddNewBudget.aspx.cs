﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.DAL;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Target_and_Bookings
{
    public partial class AddNewBudget : System.Web.UI.Page
    {
        DatabaseManager dm = new DatabaseManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //EnableDisableDeleteButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                CommonMethods.LoadDropdown(ddlBudgetYear, "Years WHERE IsActive = 1", 1, 0);
                CommonMethods.LoadDropdown(ddlMonths, "Months", 1, 0);
                CommonMethods.LoadDropdown(ddlGaugeType, "GaugeType", 1, 0);
                CommonMethods.LoadMerchandiserLeaderDropDown(ddlMerchandisingLeader, 1, 0);
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    //DeleteStyle(BuyerStyleId);
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            { 
                if (ddlBuyers.SelectedValue != "" && ddlBudgetYear.SelectedValue != "" && ddlMonths.SelectedValue != "" && ddlGaugeType.SelectedValue != "" && ddlMerchandisingLeader.SelectedValue != "" && tbxQuantity.Text != "")
                {
                    var nBudget = new YearlyBudget()
                    {
                        BuyerId = Convert.ToInt32(ddlBuyers.SelectedValue),
                        YearId = Convert.ToInt32(ddlBudgetYear.SelectedValue),
                        MonthId = Convert.ToInt32(ddlMonths.SelectedValue),
                        GaugeTypeId = Convert.ToInt32(ddlGaugeType.SelectedValue),
                        Quantity = decimal.Parse(string.IsNullOrEmpty(tbxQuantity.Text) ? "0" : tbxQuantity.Text),
                        TeamLeadId = Convert.ToInt32(ddlMerchandisingLeader.SelectedValue),
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };

                    var isExist = unitOfWork.GenericRepositories<YearlyBudget>().Get(x => x.BuyerId == nBudget.BuyerId && x.YearId == nBudget.YearId && x.MonthId == nBudget.MonthId && x.GaugeTypeId == nBudget.GaugeTypeId).Any();
                    if (!isExist)
                    {
                        unitOfWork.GenericRepositories<YearlyBudget>().Insert(nBudget);
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                        ClearBudgetInfo();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Budget already exist.')", true);
                    }
   
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please Fill in the star marks data')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int buyerId = !string.IsNullOrEmpty(ddlBuyers.SelectedValue) ? Convert.ToInt32(ddlBuyers.SelectedValue) : 0;
            int YearId = !string.IsNullOrEmpty(ddlBudgetYear.SelectedValue) ? Convert.ToInt32(ddlBudgetYear.SelectedValue) : 0;
            int MonthId = !string.IsNullOrEmpty(ddlMonths.SelectedValue) ? Convert.ToInt32(ddlMonths.SelectedValue) : 0;
            int GaugeTypeId = !string.IsNullOrEmpty(ddlGaugeType.SelectedValue) ? Convert.ToInt32(ddlGaugeType.SelectedValue) : 0;
            BindData(buyerId, YearId, MonthId, GaugeTypeId);
        }
        private void BindData(int buyerId,int YearId, int MonthId, int GaugeTypeId)
        {
            DataTable dt = new DataTable();

            dt = LoadAllBudget(buyerId, YearId, MonthId, GaugeTypeId);
            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                pnlBudget.Visible = true;
            }
            else
            {
                pnlBudget.Visible = false;
            }
        }

        //protected void btnViewDetails_Click(object sender, EventArgs e)
        //{
        //    if (ddlBuyers.SelectedValue == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Buyers Name.')", true);
        //    }
        //    else if(ddlBudgetYear.SelectedValue == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter Budget Year.')", true);
        //    }
        //    else
        //    {
        //        ShowDetails(Convert.ToInt32(ddlBuyers.SelectedValue), Convert.ToInt32(ddlBudgetYear.SelectedValue));
        //    }
        //}

        //private void ShowDetails(int buyerId, int YearId)
        //{
        //    DataTable dt = new DataTable();

        //    dt = LoadAllBudgetDetails(buyerId, YearId);
        //    if (dt.Rows.Count > 0)
        //    {
        //        rpt.DataSource = dt;
        //        rpt.DataBind();
        //        pnlBudget.Visible = true;
        //    }
        //    else
        //    {
        //        pnlBudget.Visible = false;
        //    }
        //}

        public DataTable LoadAllBudget(int buyerId, int YearId, int MonthId, int GaugeTypeId)
        {
            dm.AddParameteres("@BuyerId", buyerId);
            dm.AddParameteres("@YearId", YearId);
            dm.AddParameteres("@MonthId", MonthId);
            dm.AddParameteres("@GaugeTypeId", GaugeTypeId);
            return dm.ExecuteQuery("[usp_GetBudgetByFilter]");
        }
        //public DataTable LoadAllBudgetDetails(int buyerId, int YearId)
        //{
        //    dm.AddParameteres("@BuyerId", buyerId);
        //    dm.AddParameteres("@YearId", YearId);
        //    return dm.ExecuteQuery("[Yearly_Budget_Booking_Details]");
        //}

        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int budgetId = Convert.ToInt32(e.CommandArgument.ToString());
                PopulateBudget(budgetId);
                //Session["budgetId"] = budgetId;
            }
             
        }

        private void PopulateBudget(int budgetId)
        {
            var currentUserRoleId = CommonMethods.SessionInfo.RoleId;
            var budgetInfo = unitOfWork.GenericRepositories<YearlyBudget>().GetByID(budgetId);
            if (budgetInfo != null)
            {
                BudgetId = budgetInfo.Id;
                ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, budgetInfo.BuyerId);
                CommonMethods.LoadDropdown(ddlBudgetYear, "Years WHERE IsActive = 1", 1, 0);
                ddlBudgetYear.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBudgetYear, budgetInfo.YearId);
                CommonMethods.LoadDropdown(ddlMonths, "Months", 1, 0);
                ddlMonths.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMonths, budgetInfo.MonthId);
                CommonMethods.LoadDropdown(ddlGaugeType, "GaugeType", 1, 0);
                ddlGaugeType.SelectedIndex = CommonMethods.MatchDropDownItem(ddlGaugeType, budgetInfo.GaugeTypeId);
                
                tbxQuantity.Text = budgetInfo.Quantity?.ToString();
                //CommonMethods.LoadDropdown(ddlMerchandisingLeader, "Users", 1, 0);
                CommonMethods.LoadMerchandiserLeaderDropDown(ddlMerchandisingLeader, 1, 0);
                ddlMerchandisingLeader.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMerchandisingLeader, budgetInfo.TeamLeadId ?? 0);
                
                styleActionTitle.Text = "Update Budget:";
                btnSave.Visible = false;
                btnUpdate.Visible = true;

            }
        }

        private void ClearBudgetInfo()
        {
            ddlBuyers.Text = string.Empty;
            ddlBudgetYear.Text = string.Empty;
            ddlMonths.Text = string.Empty;
            ddlGaugeType.Text = string.Empty;
            ddlMerchandisingLeader.Text = string.Empty;
            tbxQuantity.Text = string.Empty;
        }


        int BudgetId
        {
            set { ViewState["budgetId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["budgetId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlBuyers.SelectedValue != "" || ddlBudgetYear.SelectedValue != "" || tbxQuantity.Text != "")
                {
                    var budgetInfo = unitOfWork.GenericRepositories<YearlyBudget>().GetByID(BudgetId);
                    budgetInfo.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                    budgetInfo.YearId = int.Parse(ddlBudgetYear.SelectedValue);
                    budgetInfo.MonthId = int.Parse(ddlMonths.SelectedValue);
                    budgetInfo.GaugeTypeId = int.Parse(ddlGaugeType.SelectedValue);
                    budgetInfo.TeamLeadId = int.Parse(ddlMerchandisingLeader.SelectedValue);
                    budgetInfo.Quantity = decimal.Parse(tbxQuantity.Text);
                    budgetInfo.UpdateDate = DateTime.Now;
                    budgetInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                    unitOfWork.GenericRepositories<YearlyBudget>().Update(budgetInfo);
                    unitOfWork.Save();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                    styleActionTitle.Text = "Add a New Budget:";
                    btnSave.Visible = true;
                    btnUpdate.Visible = false;
                    ClearBudgetInfo();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please Fill in the star marks data')", true);
                }
               // Session.Remove("budgetId");
            }
            catch (Exception)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Budget update was failed.')", true);
            }
        }

    }
}