﻿using MBTracker.Code_Folder;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Target_and_Bookings
{
    public partial class ViewProductionMonths : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            EnableDisableEditButton();
            BindData();
        }


        private void EnableDisableEditButton()
        {
            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Merchandiser");

            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewProductionMonths", 1, 1);

        }

        private void BindData()
        {
            DataTable dt = unitOfWork.GetDataTableFromSql("EXEC usp_GetAllProductionMonths");
            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                lblNoDataFound.Visible = false;
            }
            else
            {
                rpt.DataSource = null;
                rpt.DataBind();
                lblNoDataFound.Text = "No booking found!";
                lblNoDataFound.Visible = true;
            }
        }

        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            int productionMonthId = Convert.ToInt32(e.CommandArgument.ToString());
            Response.Redirect("AddProductionMonth.aspx?productionMonthId=" + Tools.UrlEncode(productionMonthId+""));
        }
    }
}