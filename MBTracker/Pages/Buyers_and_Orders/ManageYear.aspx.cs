﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Newtonsoft.Json.Linq;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class ManageYear : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
                // EnableDisableDeleteButton();
                // CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                //if (parameter == "confirmDelete")
                //{
                //    DeleteYear(YearsId);
                //}
               
            }

        }

        int YearsId
        {
            set { ViewState["YearsId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["YearsId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void BindData()
        {

            DataTable dt = unitOfWork.GetDataTableFromSql("Select Id,[Year],IsActive from Years");

            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                pnlYear.Visible = true;
            }
            else
            {
                pnlYear.Visible = false;
            }
        }

       

        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                 YearsId = Convert.ToInt32(e.CommandArgument.ToString());
                PopulateYear(YearsId);
            }
            //else if (e.CommandName == "Delete")
            //{
            //    ColorId = Convert.ToInt32(e.CommandArgument.ToString());

            //    var colorCannotBeDeleted = unitOfWork.GenericRepositories<viewBuyerColorsCanNotBeDeleted>().GetByID(ColorId);
            //    if (colorCannotBeDeleted == null)
            //    {
            //        var title = "Warning";
            //        var msg = "Are you sure you want to delete?";
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
            //    }
            //    else
            //    {
            //        var title = "Warning";
            //        var msg = "Sorry, you can not delete it.<br />There was some dependencies of this color.";
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            //    }
            //}
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (tbxYear.Text.Trim() != "")
                {
                    var InputYears = tbxYear.Text.Trim();
                    var isExist = unitOfWork.GenericRepositories<Years>().IsExist(x => x.Year == InputYears);
                    if (!isExist)
                    {
                        var AllYear = new Years()
                        {
                            Year = InputYears,
                            IsActive = 1,
                            CreatedBy = CommonMethods.SessionInfo.UserName,
                            CreateDate = DateTime.Now
                        };
                        unitOfWork.GenericRepositories<Years>().Insert(AllYear);
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                        BindData();
                        ClearFormData();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Year already exist.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter Year.')", true);
                }
                
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private void ClearFormData()
        {
            tbxYear.Text = string.Empty;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            //YearsId 
                if (tbxYear.Text.Trim() != "")
                {
                var InputYears = tbxYear.Text.Trim();

                var yearsInfo = unitOfWork.GenericRepositories<Years>().GetByID(YearsId);
                    if (yearsInfo != null)
                    {
                        yearsInfo.Year = InputYears;
                        yearsInfo.UpdateDate = DateTime.Now;
                        yearsInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                        unitOfWork.GenericRepositories<Years>().Update(yearsInfo);
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                        BindData();
                        ClearFormData();

                        btnSave.Visible = true;
                        btnUpdate.Visible = false;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Invalid Year Id.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter Year description.')", true);
                }
               
            }
           


        private void PopulateYear(int YearsId)
        {
            var YearInfo = unitOfWork.GenericRepositories<Years>().GetByID(YearsId);
            if (YearInfo != null)
            {

                tbxYear.Text = YearInfo.Year;
                btnSave.Visible = false;
                btnUpdate.Visible = true;

            }
        }


    }
}