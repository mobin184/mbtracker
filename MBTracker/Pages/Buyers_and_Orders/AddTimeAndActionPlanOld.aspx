﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddTimeAndActionPlanOld.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.AddTimeAndActionPlanOld" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <asp:UpdatePanel ID="orderUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="userActionTitle" Text="Add Time & Action Plan"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <label for="inputDeliveryCountry" class="control-label" style="width: 50%; float: left; min-height: 50px;">
                        <span id="sp0011" style="font-weight: bold; font-size: 15px;">Basic Informations:<span style="font-weight: 700; color: #CC0000"></span></span>
                    </label>
                    <div style="text-align: right; font-size: 12px; line-height: 30px; padding-bottom: 0px; width: 50%; float: left">
                        <span style="font-weight: 700; color: #CC0000">*</span>Required Field
                    </div>
                    <div class="control-group">
                        <div class="col-md-12 col-sm-12">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label">
                                        <asp:Label ID="lblStyle" runat="server" Text="Select a Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlStyle" runat="server" Display="Dynamic" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlStyle_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlStyle"><span style="font-weight: 700; color: #CC0000">Please select a style.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyerDepartment" class="control-label">
                                        <asp:Label ID="Label9" runat="server" Text="Year:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlYear" runat="server" Display="Dynamic" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlYear"><span style="font-weight: 700; color: #CC0000">Select Year.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label4" runat="server" Text="Order Season:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlSeason" runat="server" Display="Dynamic" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlSeason"><span style="font-weight: 700; color: #CC0000">Select Buyer Season.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label3" runat="server" Text="Booked Capacity Qty:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxBookedCapacityQty" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxBookedCapacityQty"><span style="font-weight: 700; color: #CC0000">Enter Booked Capacity Qty.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label8" runat="server" Text="Order Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlOrder" runat="server" Display="Dynamic" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlOrder_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlOrder"><span style="font-weight: 700; color: #CC0000">Please Select Order.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-horizontal">

                                <div class="control-group">
                                    <label for="inputOrderNumber" class="control-label">
                                        <asp:Label ID="Label1" runat="server" Text="Order Quantity:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxOrderQuantity" runat="server" placeholder="Enter order quantity:" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxOrderQuantity"><span style="font-weight: 700; color: #CC0000">Enter Order Quantity.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderDate" class="control-label">
                                        <asp:Label ID="Label2" runat="server" Text="Order Sheet Received Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxOrderSheetReceivedDate" runat="server" placeholder="" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxOrderSheetReceivedDate"><span style="font-weight: 700; color: #CC0000">Enter Order Sheet Received Date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderDate" class="control-label">
                                        <asp:Label ID="Label5" runat="server" Text="Initial Shipment Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxInitialShipmentDate" runat="server" placeholder="" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxInitialShipmentDate"><span style="font-weight: 700; color: #CC0000">Enter Initital Shipment Date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderDate" class="control-label">
                                        <asp:Label ID="Label6" runat="server" Text="Extend Shipment Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxExtendedShipmentDate" runat="server" placeholder="Enter order date:" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxExtendedShipmentDate"><span style="font-weight: 700; color: #CC0000">Enter Extended Shipment Date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label7" runat="server" Text="FOB:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxFOB" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxFOB"><span style="font-weight: 700; color: #CC0000">Enter Booked Capacity Qty.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label10" runat="server" Text="Total FOB:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxTotalFOB" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxTotalFOB"><span style="font-weight: 700; color: #CC0000">Enter Booked Capacity Qty.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-horizontal">

                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label15" runat="server" Text="Actual EX Factory:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxActualExFactory" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxActualExFactory"><span style="font-weight: 700; color: #CC0000">Enter Booked Capacity Qty.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label16" runat="server" Text="Plus Ship Qty:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxPlusShipQty" runat="server" TextMode="Number" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxPlusShipQty"><span style="font-weight: 700; color: #CC0000">Enter Booked Capacity Qty.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label11" runat="server" Text="Less Ship Qty:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxLessShipQty" runat="server" TextMode="Number" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxLessShipQty"><span style="font-weight: 700; color: #CC0000">Enter Booked Capacity Qty.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderDate" class="control-label">
                                        <asp:Label ID="Label12" runat="server" Text="Master LC Received Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxMasterLcOrContactReceivedDate" runat="server" placeholder="" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxMasterLcOrContactReceivedDate"><span style="font-weight: 700; color: #CC0000">Enter Order Sheet Received Date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label13" runat="server" Text="Master LC Value:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxMasterLCValue" runat="server" TextMode="Number" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxMasterLCValue"><span style="font-weight: 700; color: #CC0000">Enter Booked Capacity Qty.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label14" runat="server" Text="Shipment Value:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxShipmentValue" runat="server" TextMode="Number" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxShipmentValue"><span style="font-weight: 700; color: #CC0000">Enter Booked Capacity Qty.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label17" runat="server" Text="Finishing Unit:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlFinishingUnit" runat="server" Display="Dynamic" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlFinishingUnit"><span style="font-weight: 700; color: #CC0000">Select Buyer Season.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-body">
                    <label for="inputDeliveryCountry" class="control-label" style="width: 50%; float: left; min-height: 50px;">
                        <span id="cpMain_lblDeliveryCountry" style="font-weight: bold; font-size: 15px;">Style & Sample Information:<span style="font-weight: 700; color: #CC0000"></span></span>
                    </label>
                    <div class="control-group">
                        <div class="col-md-12 col-sm-12">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="save" />
                        </div>
                    </div>
                    <div class="row">                        
                        <div class="col-md-4">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label">
                                        <asp:Label ID="Label20" runat="server" Text="Yarn com, count & Ply:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxYarnComCountAndPly" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxYarnComCountAndPly"><span style="font-weight: 700; color: #CC0000">Enter Yarn Com, Count & Ply.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>                                
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label21" runat="server" Text="Sample MC Brand:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlSampleMCBrand" runat="server" Display="Dynamic" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator22" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlSampleMCBrand"><span style="font-weight: 700; color: #CC0000">Select Machine Brand.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderNumber" class="control-label">
                                        <asp:Label ID="Label25" runat="server" Text="Sample Timing:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxSampleTiming" runat="server" TextMode="Number" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxSampleTiming"><span style="font-weight: 700; color: #CC0000">Enter Sample Timing.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderDate" class="control-label">
                                        <asp:Label ID="Label26" runat="server" Text="Knitting Output:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxKnittingOutput" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxKnittingOutput"><span style="font-weight: 700; color: #CC0000">Enter Knitting Output.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputOrderDate" class="control-label">
                                        <asp:Label ID="Label27" runat="server" Text="Knitting MC Gauge:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxKnittingMCGauge" runat="server" placeholder="" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator28" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxKnittingMCGauge"><span style="font-weight: 700; color: #CC0000">Enter Knitting MC Gauge.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderDate" class="control-label">
                                        <asp:Label ID="Label28" runat="server" Text="Linking MC Gauge:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <%--<asp:TextBox ID="tbxLinkingMCGauge" runat="server" placeholder="" TextMode="Number" CssClass="form-control"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlLinkingMCGauge" runat="server" Display="Dynamic" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator29" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlLinkingMCGauge"><span style="font-weight: 700; color: #CC0000">Enter Linking MC Gauge.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label29" runat="server" Text="Linking Timing:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxLinkingTiming" runat="server" TextMode="Number" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator30" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxLinkingTiming"><span style="font-weight: 700; color: #CC0000">Enter Linking Timing.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label30" runat="server" Text="Linking Output:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxLinkingOutput" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator31" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxLinkingOutput"><span style="font-weight: 700; color: #CC0000">Enter Linking Output.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label">
                                        <asp:Label ID="Label19" runat="server" Text="Weight/Dz:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxWeightPerDz" runat="server" placeholder="" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxWeightPerDz"><span style="font-weight: 700; color: #CC0000">Enter Yarn Com, Count & Ply.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group" style="min-height: 125px;">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label18" runat="server" Text="Sample Picture:"></asp:Label><asp:Label runat="server" ID="redAsterisk1"><span style="font-weight: 700; color: #CC0000">*</span></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:FileUpload ID="FileUpload1" onchange="javascript:updateFileList()" accept=".png,.jpg,.jpeg,.gif" runat="server" AllowMultiple="false" />
                                        <asp:RequiredFieldValidator ID="reqFileUpload1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="FileUpload1"><span style="font-weight: 700; color: #CC0000">Select file(s) to upload.</span></asp:RequiredFieldValidator>
                                       <%-- <br />
                                        <asp:Label ID="lblShowFileNames" runat="server" Text="" Style="min-height: 95px; width: 600px;"></asp:Label>--%>
                                        <image id="samplePic" class="hidden" width="150px"; />
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-body">
                    <label for="inputDeliveryCountry" class="control-label" style="width: 50%; float: left; min-height: 50px;">
                        <span id="sp001" style="font-weight: bold; font-size: 15px;">Yarn Information:<span style="font-weight: 700; color: #CC0000"></span></span>
                    </label>
                    <div class="control-group">
                        <div class="col-md-12 col-sm-12">
                            <asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="save" />
                        </div>
                    </div>
                    <div class="row">                        
                        <div class="col-md-4">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label">
                                        <asp:Label ID="Label24" runat="server" Text="Dyeing Order Place Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxDyeingOrderPlaceDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxDyeingOrderPlaceDate"><span style="font-weight: 700; color: #CC0000">Enter Dyeing Order Place Date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label">
                                        <asp:Label ID="Label23" runat="server" Text="Yarn Supplier:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxYarnSupplier" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator38" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxYarnSupplier"><span style="font-weight: 700; color: #CC0000">Enter Yarn Supplier Name.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label">
                                        <asp:Label ID="Label31" runat="server" Text="PI Received Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxPIReceivedDate" runat="server" placeholder="" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxPIReceivedDate"><span style="font-weight: 700; color: #CC0000">Enter PI Received Date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label32" runat="server" Text="PI Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxPINumber" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxPINumber"><span style="font-weight: 700; color: #CC0000">Enter PI Number.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>  
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputOrderNumber" class="control-label">
                                        <asp:Label ID="Label33" runat="server" Text="PI Value:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxPIValue" runat="server" TextMode="Number" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator32" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxPIValue"><span style="font-weight: 700; color: #CC0000">Enter PI Value.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderDate" class="control-label">
                                        <asp:Label ID="Label34" runat="server" Text="Yarn ETD:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxYarnETD" runat="server" placeholder="" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator33" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxYarnETD"><span style="font-weight: 700; color: #CC0000">Enter Yarn ETD.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderDate" class="control-label">
                                        <asp:Label ID="Label35" runat="server" Text="Yarn ETA:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxYarnETA" runat="server" placeholder="" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator34" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxYarnETA"><span style="font-weight: 700; color: #CC0000">Enter Yarn ETA.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderDate" class="control-label">
                                        <asp:Label ID="Label36" runat="server" Text="Yarn In-house Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxYarnInHouseDate" runat="server" placeholder="" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator35" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxYarnInHouseDate"><span style="font-weight: 700; color: #CC0000">Enter Yarn In-house Date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label37" runat="server" Text="Knitting Acc. In-house Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxKnittingAccessoriesInHouseDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator36" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxKnittingAccessoriesInHouseDate"><span style="font-weight: 700; color: #CC0000">Enter Knit. Acc. In-house Date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label38" runat="server" Text="Linking Addi. In-house Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxLinkingAdditionalInHouseDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator37" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxLinkingAdditionalInHouseDate"><span style="font-weight: 700; color: #CC0000">Enter Link. Addi. In-house Date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group" style="min-height: 80px;">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label22" runat="server" Text="Dyeing Order Sample File:"></asp:Label><asp:Label runat="server" ID="redAsterisk2"><span style="font-weight: 700; color: #CC0000">*</span></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:FileUpload ID="FileUpload2" onchange="javascript:updateFileList2nd()" runat="server" AllowMultiple="false" Style="margin-bottom: 5px;" />
                                        <asp:RequiredFieldValidator ID="reqFileUpload2" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="FileUpload2"><span style="font-weight: 700; color: #CC0000">Select file to upload.</span></asp:RequiredFieldValidator>

                                        <asp:Label ID="lblShowFileNames2nd" runat="server" Text="" Style="min-height: 95px; width: 600px;"></asp:Label>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-body">
                    <label for="inputDeliveryCountry" class="control-label" style="width: 50%; float: left; min-height: 50px;">
                        <span id="sp006" style="font-weight: bold; font-size: 15px;">Accessories Information:<span style="font-weight: 700; color: #CC0000"></span></span>
                    </label>
                    <div class="control-group">
                        <div class="col-md-12 col-sm-12">
                            <asp:ValidationSummary ID="ValidationSummary4" runat="server" ValidationGroup="save" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="padding-left: 30px;">
                            <table class="table table-bordered table-condensed">
                                <thead>
                                    <tr>
                                        <th style="width: 28%">Item</th>
                                        <th style="width: 8%">Booking Date<span style="font-weight: 700; color: #CC0000">*</span></th>
                                        <th style="width: 8%">PI Received Date<span style="font-weight: 700; color: #CC0000">*</span></th>
                                        <th style="width: 16%">PI Number<span style="font-weight: 700; color: #CC0000">*</span></th>
                                        <th style="width: 16%">PI Value<span style="font-weight: 700; color: #CC0000">*</span></th>
                                        <th style="width: 8%">PI Send to Comm.<span style="font-weight: 700; color: #CC0000">*</span></th>
                                        <th style="width: 8%">LC Open Date<span style="font-weight: 700; color: #CC0000">*</span></th>
                                        <th style="width: 8%">Good In-house Date<span style="font-weight: 700; color: #CC0000">*</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle">
                                            <asp:Label ID="Label39" Style="font-size: 13px; font-weight: bold;" runat="server" Text="Main, Size & Care Label"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="tbxMainSizeCareBookingDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator39" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxMainSizeCareBookingDate"><span style="font-weight: 700; color: #CC0000">Enter Booking Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxMainSizeCarePIReceivedDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator40" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxMainSizeCarePIReceivedDate"><span style="font-weight: 700; color: #CC0000">Enter PI Received Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxMainSizeCarePINumber" runat="server" placeholder="" Style="min-width: 80px;" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator41" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxMainSizeCarePINumber"><span style="font-weight: 700; color: #CC0000">Enter PI Number.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxMainSizeCarePIValue" runat="server" placeholder="" Style="min-width: 70px;" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator42" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxMainSizeCarePIValue"><span style="font-weight: 700; color: #CC0000">Enter PI Value.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxMainSizeCarePISignAndSendToCom" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator43" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxMainSizeCarePISignAndSendToCom"><span style="font-weight: 700; color: #CC0000">Enter PI Send Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxMainSizeCareLCOpenDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator44" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxMainSizeCareLCOpenDate"><span style="font-weight: 700; color: #CC0000">Enter LC Open Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxMainSizeCareGoodsInHouseDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator45" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxMainSizeCareGoodsInHouseDate"><span style="font-weight: 700; color: #CC0000">Enter Goods In-house Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle">
                                            <asp:Label ID="Label40" Style="font-size: 13px; font-weight: bold;" runat="server" Text="Hang Tag/ Price Tag"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="tbxHangOrPriceTagBookingDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator46" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxHangOrPriceTagBookingDate"><span style="font-weight: 700; color: #CC0000">Enter Booking Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxHangOrPriceTagPIReceivedDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator47" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxHangOrPriceTagPIReceivedDate"><span style="font-weight: 700; color: #CC0000">Enter PI Received Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxHangOrPriceTagPINumber" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator48" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxHangOrPriceTagPINumber"><span style="font-weight: 700; color: #CC0000">Enter PI Number.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxHangOrPriceTagPIValue" runat="server" placeholder="" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator49" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxHangOrPriceTagPIValue"><span style="font-weight: 700; color: #CC0000">Enter PI Value.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxHangOrPriceTagPISignAndSendToCom" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator50" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxHangOrPriceTagPISignAndSendToCom"><span style="font-weight: 700; color: #CC0000">Enter PI Send Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxHangOrPriceTagLCOpenDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator51" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxHangOrPriceTagLCOpenDate"><span style="font-weight: 700; color: #CC0000">Enter LC Open Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxHangOrPriceTagGoodsInHouseDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator52" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxHangOrPriceTagGoodsInHouseDate"><span style="font-weight: 700; color: #CC0000">Enter Goods In-house Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle">
                                            <asp:Label ID="Label41" Style="font-size: 13px; font-weight: bold;" runat="server" Text="Button/ Zipper"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="tbxButtonZipperBookingDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator53" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxButtonZipperBookingDate"><span style="font-weight: 700; color: #CC0000">Enter Booking Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxButtonZipperPIReceivedDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator54" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxButtonZipperPIReceivedDate"><span style="font-weight: 700; color: #CC0000">Enter PI Received Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxButtonZipperPINumber" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator55" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxButtonZipperPINumber"><span style="font-weight: 700; color: #CC0000">Enter PI Number.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxButtonZipperPIValue" runat="server" placeholder="" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator56" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxButtonZipperPIValue"><span style="font-weight: 700; color: #CC0000">Enter PI Value.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxButtonZipperPISignAndSendToCom" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator57" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxButtonZipperPISignAndSendToCom"><span style="font-weight: 700; color: #CC0000">Enter PI Send Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxButtonZipperLCOpenDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator58" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxButtonZipperLCOpenDate"><span style="font-weight: 700; color: #CC0000">Enter LC Open Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxButtonZipperGoodsInHouseDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator59" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxButtonZipperGoodsInHouseDate"><span style="font-weight: 700; color: #CC0000">Enter Goods In-house Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle">
                                            <asp:Label ID="Label42" Style="font-size: 13px; font-weight: bold;" runat="server" Text="Badge/Flower/Fabrics"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="tbxBadgeFlowerFabricsBookingDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator60" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxBadgeFlowerFabricsBookingDate"><span style="font-weight: 700; color: #CC0000">Enter Booking Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxBadgeFlowerFabricsPIReceivedDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator61" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxBadgeFlowerFabricsPIReceivedDate"><span style="font-weight: 700; color: #CC0000">Enter PI Received Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxBadgeFlowerFabricsPINumber" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator62" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxBadgeFlowerFabricsPINumber"><span style="font-weight: 700; color: #CC0000">Enter PI Number.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxBadgeFlowerFabricsPIValue" runat="server" placeholder="" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator63" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxBadgeFlowerFabricsPIValue"><span style="font-weight: 700; color: #CC0000">Enter PI Value.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxBadgeFlowerFabricsPISignAndSendToCom" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator64" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxBadgeFlowerFabricsPISignAndSendToCom"><span style="font-weight: 700; color: #CC0000">Enter PI Send Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxBadgeFlowerFabricsLCOpenDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator65" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxBadgeFlowerFabricsLCOpenDate"><span style="font-weight: 700; color: #CC0000">Enter LC Open Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxBadgeFlowerFabricsGoodsInHouseDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator66" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxBadgeFlowerFabricsGoodsInHouseDate"><span style="font-weight: 700; color: #CC0000">Enter Goods In-house Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle">
                                            <asp:Label ID="Label43" Style="font-size: 13px; font-weight: bold;" runat="server" Text="Print/ Embroidery"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="tbxPrintEmbroideryBookingDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator67" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxPrintEmbroideryBookingDate"><span style="font-weight: 700; color: #CC0000">Enter Booking Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxPrintEmbroideryPIReceivedDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator68" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxPrintEmbroideryPIReceivedDate"><span style="font-weight: 700; color: #CC0000">Enter PI Received Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxPrintEmbroideryPINumber" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator69" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxPrintEmbroideryPINumber"><span style="font-weight: 700; color: #CC0000">Enter PI Number.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxPrintEmbroideryPIValue" runat="server" placeholder="" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator70" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxPrintEmbroideryPIValue"><span style="font-weight: 700; color: #CC0000">Enter PI Value.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxPrintEmbroideryPISignAndSendToCom" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator71" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxPrintEmbroideryPISignAndSendToCom"><span style="font-weight: 700; color: #CC0000">Enter PI Send Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxPrintEmbroideryLCOpenDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator72" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxPrintEmbroideryLCOpenDate"><span style="font-weight: 700; color: #CC0000">Enter LC Open Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxPrintEmbroideryGoodsInHouseDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator73" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxPrintEmbroideryGoodsInHouseDate"><span style="font-weight: 700; color: #CC0000">Enter Goods In-house Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle">
                                            <asp:Label ID="Label44" Style="font-size: 13px; font-weight: bold;" runat="server" Text="Sewing/ Shoulder Tap"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="tbxSewingShoulderTapBookingDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator74" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxSewingShoulderTapBookingDate"><span style="font-weight: 700; color: #CC0000">Enter Booking Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxSewingShoulderTapPIReceivedDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator75" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxSewingShoulderTapPIReceivedDate"><span style="font-weight: 700; color: #CC0000">Enter PI Received Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxSewingShoulderTapPINumber" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator76" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxSewingShoulderTapPINumber"><span style="font-weight: 700; color: #CC0000">Enter PI Number.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxSewingShoulderTapPIValue" runat="server" placeholder="" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator77" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxSewingShoulderTapPIValue"><span style="font-weight: 700; color: #CC0000">Enter PI Value.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxSewingShoulderTapPISignAndSendToCom" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator78" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxSewingShoulderTapPISignAndSendToCom"><span style="font-weight: 700; color: #CC0000">Enter PI Send Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxSewingShoulderTapLCOpenDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator79" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxSewingShoulderTapLCOpenDate"><span style="font-weight: 700; color: #CC0000">Enter LC Open Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxSewingShoulderTapGoodsInHouseDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator80" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxSewingShoulderTapGoodsInHouseDate"><span style="font-weight: 700; color: #CC0000">Enter Goods In-house Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle">
                                            <asp:Label ID="Label45" Style="font-size: 13px; font-weight: bold;" runat="server" Text="Poly"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="tbxPolyBookingDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator81" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxPolyBookingDate"><span style="font-weight: 700; color: #CC0000">Enter Booking Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxPolyPIReceivedDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator82" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxPolyPIReceivedDate"><span style="font-weight: 700; color: #CC0000">Enter PI Received Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxPolyPINumber" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator83" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxPolyPINumber"><span style="font-weight: 700; color: #CC0000">Enter PI Number.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxPolyPIValue" runat="server" placeholder="" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator84" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxPolyPIValue"><span style="font-weight: 700; color: #CC0000">Enter PI Value.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxPolyPISignAndSendToCom" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator85" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxPolyPISignAndSendToCom"><span style="font-weight: 700; color: #CC0000">Enter PI Send Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxPolyLCOpenDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator86" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxPolyLCOpenDate"><span style="font-weight: 700; color: #CC0000">Enter LC Open Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxPolyGoodsInHouseDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator87" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxPolyGoodsInHouseDate"><span style="font-weight: 700; color: #CC0000">Enter Goods In-house Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle">
                                            <asp:Label ID="Label46" Style="font-size: 13px; font-weight: bold;" runat="server" Text="Cartoon"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="tbxCartoonBookingDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator88" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxCartoonBookingDate"><span style="font-weight: 700; color: #CC0000">Enter Booking Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxCartoonPIReceivedDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator89" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxCartoonPIReceivedDate"><span style="font-weight: 700; color: #CC0000">Enter PI Received Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxCartoonPINumber" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator90" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxCartoonPINumber"><span style="font-weight: 700; color: #CC0000">Enter PI Number.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxCartoonPIValue" runat="server" placeholder="" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator91" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxCartoonPIValue"><span style="font-weight: 700; color: #CC0000">Enter PI Value.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxCartoonPISignAndSendToCom" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator92" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxCartoonPISignAndSendToCom"><span style="font-weight: 700; color: #CC0000">Enter PI Send Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxCartoonLCOpenDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator93" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxCartoonLCOpenDate"><span style="font-weight: 700; color: #CC0000">Enter LC Open Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxCartoonGoodsInHouseDate" runat="server" TextMode="Date" placeholder="" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator94" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxCartoonGoodsInHouseDate"><span style="font-weight: 700; color: #CC0000">Enter Goods In-house Date.</span></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="control-group" style="padding-top: 0px;">
            <asp:Button ID="btnSave" runat="server" Style="margin-right: 0px" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" ValidationGroup="save" OnClick="btnSave_Click" />
            <asp:Button ID="btnUpdate" runat="server" Visible="false" Style="margin-right: 0px" class="btn btn-success btn-samll pull-right btnStyle"  Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" ValidationGroup="save" OnClick="btnUpdate_Click"  />
        </div>
    </div>
 </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btnUpdate" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
    <script>
        updateFileList = function () {

            //alert("I am just in the function!");
            var input = document.getElementById('cpMain_FileUpload1');
           // var output = document.getElementById('cpMain_lblShowFileNames');

            //alert("What is output?" + output);
            //output.innerHTML = '<br />Selected Files:<br />';
            //output.innerHTML += '<ul>';



            for (var i = 0; i < input.files.length; ++i) {
                console.log(input.files.item(i));
                if (input.files && input.files[0]) {
                    var ImageDir = new FileReader();
                    ImageDir.onload = function (e) {
                        $('#samplePic').attr('src', e.target.result);
                        $('#samplePic').removeClass('hidden');
                    }
                    ImageDir.readAsDataURL(input.files[0]);
                }  
              //  output.innerHTML += '<li>' + input.files.item(i).name + '</li>';
            }
           // output.innerHTML += '</ul>';
            //alert("InnerHTML: " + output.innerHTML);
        }

        updateFileList2nd = function () {

            //alert("I am just in the function!");
            var input = document.getElementById('cpMain_FileUpload2');
            var output = document.getElementById('cpMain_lblShowFileNames2nd');

            //alert("What is output?" + output);
            output.innerHTML = '<br />Selected Files:<br />';
            output.innerHTML += '<ul>';



            for (var i = 0; i < input.files.length; ++i) {
                output.innerHTML += '<li>' + input.files.item(i).name + '</li>';
            }
            output.innerHTML += '</ul>';
            //alert("InnerHTML: " + output.innerHTML);
        }
    </script>
</asp:Content>
