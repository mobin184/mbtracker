﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddTimeAndActionPlanNote.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.AddTimeAndActionPlanNote" Async="true" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid">
        <div class="span6">

            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="Add a Note to TNA:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row" style="padding-left: 5px;">
                        <div class="form-horizontal">
                            <div style="text-align: right; font-size: 12px; line-height: 10px; padding-right: 18px; padding-bottom: 10px">
                                <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                            </div>
                            <div class="control-group">
                                <div class="col-md-12 col-sm-12">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="Label2" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="Label1" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <label for="inputStyle" class="control-label">
                                            <asp:Label ID="Label12" runat="server" Text="Select Year:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="ddlYear"><span style="font-weight: 700; color: #CC0000">Please select year.</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="inputFinishingFloor" class="control-label">
                                            <asp:Label ID="lblTNAMessage" runat="server" Text="Delivery Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlTNAs" runat="server" CssClass="form-control" Display="Dynamic" AutoPostBack="true" Width="100%" OnSelectedIndexChanged="ddlTNAs_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="ddlTNAs"><span style="font-weight: 700; color: #CC0000">Select a TNA.</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="pull-right" style="text-align: right; font-size: 2px; line-height: 5px; padding-bottom: 2px">
                                        <span style="font-weight: 100; padding-right: 400px"></span><span style="font-weight: 100; color: #CC0000">&nbsp;</span>
                                    </div>
                                </div>
                            </div>

                            <div id="divNote" runat="server" visible="false">

                            <div class="col-md-12">

                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="lblNoteMessage" runat="server" Text="Note"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxNote" runat="server" placeholder="Enter Your Note" CssClass="form-control" Width="100%" TextMode="MultiLine" Height="100px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxNote"><span style="font-weight: 700; color: #CC0000">Enter your note</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                            
                            <div class="control-group">
                                <br />
                                <div class="controls-row">
                                    <asp:button id="btnSave" runat="server" text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" cssclass="btn btn-info pull-right" validationgroup="save" onclick="btnSave_Click" />
                                    <asp:button id="btnUpdate" runat="server" visible="False" text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" onclick="btnUpdate_Click" cssclass="btn btn-info pull-right" validationgroup="save" />
                                </div>
                            </div>
                            
                            
                            </div>
                        
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="span6" id="divNotes" runat="server" visible="false">

                <div class="widget">
                    <div class="widget-header">
                        <div class="title">
                            <span class="fs1" aria-hidden="true" data-icon=""></span>Previous Note(s):
                        </div>
                    </div>
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">
                            <asp:Repeater ID="rpt" runat="server">
                                <HeaderTemplate>
                                    <table id="data-table2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                               
                                                <th>
                                                    <asp:Label ID="Label5" runat="server" Text="Time Created"></asp:Label></th>
                                                 <th>
                                                    <asp:Label ID="Label2" runat="server" Text="Created By"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label3" runat="server" Text="Notes"></asp:Label></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="text-align:left"><%#Eval("CreateDate") %></td>
                                        <td><%#Eval("CreatedBy") %> </td>
                                         <td style="text-align:left"><%#Eval("Notes") %></td>
                                       <%-- <td>
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-small hidden-phone" Text="Delete"></asp:LinkButton>
                                        </td>--%>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                         </table>
           <%--                 <div class="clearfix">--%>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>



        </div>

    </div>


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
