﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="AddNewOrder.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.AddNewOrder" Async="true" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        input[type="radio"], input[type=checkbox] {
            margin: -2px 0 0;
        }

        
        .cblClass label
        {
            /*display: block;*/
            padding-left: 2px !important;
            padding-right: 14px !important;

        }




    </style>

    <asp:UpdatePanel ID="orderUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div class="row-fluid">
                <div class="span9">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title">
                                <span class="fs1" aria-hidden="true" data-icon=""></span>
                                <asp:Label runat="server" ID="userActionTitle" Text="Add a New Order"></asp:Label>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div style="text-align: right; font-size: 12px; line-height: 30px; padding-bottom: 0px">
                                <span style="font-weight: 700; color: #CC0000">*</span>Required Field
                            </div>
                            <div class="control-group">
                                <div class="col-md-12 col-sm-12">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-horizontal">

                                        <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputBuyerDepartment" class="control-label">
                                                <asp:Label ID="Label9" runat="server" Text="Buyer's Department:"></asp:Label></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlBuyerDeprtments" runat="server" Display="Dynamic" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputStyle" class="control-label">
                                                <asp:Label ID="lblStyle" runat="server" Text="Select a Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlStyle" runat="server" Display="Dynamic" CssClass="form-control"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlStyle"><span style="font-weight: 700; color: #CC0000">Please select a style.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputOrderSeason" class="control-label">
                                                <asp:Label ID="Label4" runat="server" Text="Order Season:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                            </label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlSeason" runat="server" Display="Dynamic" CssClass="form-control"></asp:DropDownList>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlSeason"><span style="font-weight: 700; color: #CC0000">Select Order Season.</span></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>



                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <label for="inputOrderSeason" class="control-label">
                                                <asp:Label ID="Label8" runat="server" Text="Order Year:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                            </label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlOrderYear" runat="server" Display="Dynamic" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputOrderNumber" class="control-label">
                                                <asp:Label ID="Label1" runat="server" Text="Order Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                            </label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxOrderNumber" runat="server" placeholder="Enter order number:" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxOrderNumber"><span style="font-weight: 700; color: #CC0000">Enter Order Number.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputOrderDate" class="control-label">
                                                <asp:Label ID="Label2" runat="server" Text="Order Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                            </label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxOrderDate" runat="server" placeholder="Enter order date:" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxOrderDate"><span style="font-weight: 700; color: #CC0000">Enter Order Date.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group hidden">
                                            <label for="inputOrderItem" class="control-label">
                                                <asp:Label runat="server" CssClass="hidden" ID="Label5"></asp:Label>
                                                <asp:Label ID="Label7" runat="server" Text="Order Item:"></asp:Label>
                                            </label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxOrderItem" runat="server" placeholder="Enter order item" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <%-- <div class="control-group">
                                    <label for="inputPaymentTerm" class="control-label">
                                        <asp:Label ID="Label3" runat="server" Text="Payment Term:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxPaymentTerm" runat="server" placeholder="Enter payment term" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputLCValue" class="control-label">
                                        <asp:Label ID="lblLCValue" runat="server" Text="LC Value:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxLCValue" runat="server" placeholder="Enter LC value" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>--%>
                                        <div class="control-group">
                                            <label for="inputOrderStatus" class="control-label">
                                                <asp:Label ID="Label11" runat="server" Text="Order Status:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlOrderStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlOrderStatus"><span style="font-weight: 700; color: #CC0000">Select order status</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 0px">
                                            <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>



                <div class="row-fluid">
                    <div class="span12">
                        <asp:Panel ID="pnlColorAndSize" runat="server" Visible="false">
                            <div class="widget">
                                <div class="widget-body">
                                    <%--<div class="form-horizontal">--%>
                                    <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                    <div class="control-group">
                                        <label for="inputDeliveryCountry" class="control-label">
                                            <asp:Label ID="lblDeliveryCountry" runat="server" Visible="false" Font-Bold="true">Select Delivery Country:<span style="font-weight: 700; color: #CC0000">*</span></asp:Label>
                                        </label>
                                        <div class="controls controls-row" style="padding-top: 5px">
                                            <div class="form-inline cblClass">
                                                <asp:Label ID="lblNoCountryFound" runat="server" Visible="false" Text="No country found." BackColor="#ffff00"></asp:Label>
                                                <asp:CheckBoxList ID="cblDeliveryCountries" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="5" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" ></asp:CheckBoxList>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="controls controls-row">
                                        <asp:Label ID="Label14" runat="server" Visible="false"><span style="font-weight: 700; color: #CC0000">Please select a delivery country.</span></asp:Label>
                                        <%--<div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 0px">
                                            <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                        </div>--%>
                                        <asp:Button ID="btnSelectAllCountries" runat="server" Visible="false" Text="Select All Countries" OnClick="btnSelectAllCountries_Click" />
                                        <asp:Button ID="btnUnselectAllCountries" runat="server" Visible="false" Text="Unselect All Countries" OnClick="btnUnselectAllCountries_Click" />
                                    </div>

                                    <div class="control-group" style="padding-top: 40px">
                                        <label for="inputDeliveryCountry" class="control-label">
                                            <asp:Label ID="lblColorAndSize" runat="server" Visible="false" Text="Enter Colors and Sizes:" Font-Bold="true"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>

                                        <div class="controls controls-row">
                                            <div id="dt_example" class="example_alt_pagination" style="overflow-x: auto">
                                                <asp:Repeater ID="rpt" runat="server" OnItemDataBound="rpt_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="data-table" class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <%--<th style="width: 120px">
                                                                    <asp:Label ID="lblBuyerColorId" runat="server" Text="Color Code"></asp:Label></th>--%>
                                                                    <th style="width: 150px">
                                                                        <asp:Label ID="lblColor" runat="server" Text="Color"></asp:Label></th>
                                                                    <%-- <th style="width: 100px">
                                                                    <asp:Label ID="lblUnitCost" runat="server" Text="Unit Cost"></asp:Label></th>--%>
                                                                    <th style="width: 150px">
                                                                        <asp:Label ID="lblOrderQtyByColor" runat="server" Text="Order Quantity"></asp:Label></th>
                                                                    <th style="width: 350px">
                                                                        <asp:Label ID="lblSizeAndColor" runat="server" Text="Size & Quantity"></asp:Label></th>
                                                                    <th style="width: 100px">
                                                                        <asp:Label ID="lblDeleteRow" runat="server" Text="Action"></asp:Label></th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <%-- <td width="10%">
                                                            <asp:TextBox ID="tbxColorCode" CssClass="dummyClass" Height="30" runat="server"></asp:TextBox>
                                                        </td>--%>
                                                            <td width="10%">
                                                                <asp:DropDownList ID="ddlColors" runat="server" CssClass="form-control" Style="min-width: 125px"></asp:DropDownList>
                                                                <%--<asp:TextBox ID="tbxColorDescription" CssClass="dummyClass" Height="30" runat="server"></asp:TextBox>--%>
                                                            </td>
                                                            <%-- <td width="10%">
                                                            <asp:TextBox ID="tbxUnitCost" CssClass="dummyClass" Height="30" runat="server"></asp:TextBox></td>--%>
                                                            <td width="10%">
                                                                <asp:TextBox ID="tbxOrderQtyByColor" CssClass="dummyClass" Height="30" runat="server"></asp:TextBox></td>

                                                            <td width="45%">

                                                                <%--  <asp:Repeater ID="rptSizeAndQty" runat="server" Visible="false" OnItemDataBound="rptSizeAndQty_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="data-table" class="table table-bordered table-hover">
                                                                            <tbody>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblSizeId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "Id")%>'></asp:Label><asp:Label ID="lblSize" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SizeName")%>' Width="100px"></asp:Label></th></td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbxOrderQuantity" CssClass="dummyClass" Height="30" runat="server" Width="70px"></asp:TextBox>

                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </tbody>
                                                            </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>--%>
                                                                <asp:Label runat="server" ID="lblOrderColorId" Visible="false"></asp:Label>
                                                                <asp:Label runat="server" ID="lblBuyerId" Visible="false"></asp:Label>
                                                                <asp:GridView ID="gvSizeQuantity1" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity1_RowCreated">
                                                                </asp:GridView>
                                                                <asp:Label ID="lblNoSizeFound" runat="server" Visible="false" Text="No size found." BackColor="#ffff00"></asp:Label>
                                                            </td>

                                                            <td width="10%">
                                                                <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("RowNumber") %>' class="btn btn-danger btn-small hidden-phone" OnCommand="btnDelete_Command" Text="Delete Row"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                        </table>
                                                    <div class="pull-right" style="width: 10%; margin-right: -8px">
                                                        <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="81px" Text="Add Row" OnClick="btnAddRow_Click" />
                                                    </div>


                                                        <%-- <div class="clearfix">--%>
                                                        <%-- <div class="clearfix">--%>
                                                    </FooterTemplate>
                                                </asp:Repeater>

                                                <div class="control-group" id="fileUpload" runat="server" visible="false">
                                                    <label for="inputBuyer" class="control-label" style="width: 110px; text-align: left;padding-top: 20px">
                                                        <asp:Label ID="Label10" runat="server" Text="Upload Order Files:" CssClass="text-left"></asp:Label></label>
                                                    <div class="controls controls-row" style="margin-left: 120px;">
                                                       
                                                        <asp:FileUpload ID="FileUpload1" onchange="javascript:updateFileList()" runat="server" AllowMultiple="true" />
                                                        <br />
                                                        <asp:Label ID="lblShowFileNames" runat="server" Text="" Width="600px"></asp:Label>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <%--</div>--%>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>


                <asp:Panel ID="pnlFileUploads" runat="server" Visible="false">
                    <div class="span11">
                        <%--   <div class="widget">--%>
                        <div class="widget-body">
                            <div class="form-horizontal">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <%-- </div>--%>
                    </div>
                    <div class="row-fluid">
                        <div class="control-group" style="padding-top: 0px;">
                            <%-- <label for="inputBuyer" class="control-label">
                            <asp:Label ID="Label12" runat="server" Text="&nbsp;" CssClass="text-left"></asp:Label></label>
                        <div class="controls controls-row">--%>
                            <%--  <div style="text-align: right; font-size: 12px; line-height: 30px; padding-bottom: 0px">
                            <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                        </div>--%>
                            
                            <asp:Button ID="btnSaveOrder" runat="server" style="margin-right:60px" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Save Order&nbsp;&nbsp;&nbsp;" ValidationGroup="save" OnClick="btnSaveOrder_Click" />
                            <asp:Button ID="btnUpdateOrder" runat="server" Visible="false" style="margin-right:60px"  class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Update Order&nbsp;&nbsp;&nbsp;" OnClick="btnUpdateOrder_Click" />
                        </div>
                    </div>
                </asp:Panel>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSaveOrder" />
            <asp:PostBackTrigger ControlID="btnUpdateOrder" />
        </Triggers>
    </asp:UpdatePanel>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
    <script>
        updateFileList = function () {

            //alert("I am just in the function!");
            var input = document.getElementById('cpMain_FileUpload1');
            var output = document.getElementById('cpMain_lblShowFileNames');

            //alert("What is output?" + output);
            output.innerHTML = '<br />Selected Files:<br />';
            output.innerHTML += '<ul>';



            for (var i = 0; i < input.files.length; ++i) {
                output.innerHTML += '<li>' + input.files.item(i).name + '</li>';
            }
            output.innerHTML += '</ul>';
            //alert("InnerHTML: " + output.innerHTML);
        }
    </script>
</asp:Content>
