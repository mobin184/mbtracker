﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using Repositories;
using MBTracker.EF;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class ManageStylesV2 : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                EnableDisableDeleteButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteStyle(BuyerStyleId);
                }
            }

        }


        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Knitting - Mgmt");
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ManageStylesV2", 2, 1);

        }

        int BuyerStyleId
        {
            set { ViewState["styleId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["styleId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue != "")
            {

                BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            else
            {
                pnlStyles.Visible = false;
                pnlBuyerColors.Visible = false;
                pnlBuyerSizes.Visible = false;

            }
        }

        private void BindData(int buyerId)
        {


            LoadBuyerColors(buyerId);
            LoadBuyerSizes(buyerId);

            DataTable dt = new DataTable();

            dt = buyerManager.LoadStylesByBuyer(buyerId);
            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                pnlStyles.Visible = true;
            }
            else
            {
                pnlStyles.Visible = false;
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                int styleId = buyerManager.InsertStyle(tbxStyleName.Text, Convert.ToInt32(ddlBuyers.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now);

                if (styleId > 0)
                {

                    int styleColorSaveResult = SaveStyleColors(styleId);
                    int styleSizeSaveResult = SaveStyleSizes(styleId);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Style name exists.')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }


            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearStyleName();


        }


        public int SaveStyleColors(int styleId)
        {


            DataTable dt = StyleColors();


            for (int i = 0; i < cblBuyerColors.Items.Count; i++)
            {
                if (cblBuyerColors.Items[i].Selected)
                {
                    dt.Rows.Add(styleId, cblBuyerColors.Items[i].Value, CommonMethods.SessionInfo.UserName, DateTime.Now);
                }
            }

            buyerManager.DeleteStyleColors(styleId);
            return buyerManager.SaveStyleColors(dt);

        }




        public DataTable StyleColors()
        {
            DataTable dt = new DataTable("StyleColors");
            dt.Columns.Add("SId", typeof(int));
            dt.Columns.Add("CId", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }


        public int SaveStyleSizes(int styleId)
        {


            DataTable dt = StyleSizes();


            for (int i = 0; i < cblBuyerSizes.Items.Count; i++)
            {
                if (cblBuyerSizes.Items[i].Selected)
                {
                    dt.Rows.Add(styleId, cblBuyerSizes.Items[i].Value, CommonMethods.SessionInfo.UserName, DateTime.Now);
                }
            }

            buyerManager.DeleteStyleSizes(styleId);
            return buyerManager.SaveStyleSizes(dt);

        }

        public DataTable StyleSizes()
        {
            DataTable dt = new DataTable("StyleSizes");
            dt.Columns.Add("SId", typeof(int));
            dt.Columns.Add("SizeId", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int returnValue = buyerManager.UpdateBuyerStyle(BuyerStyleId, tbxStyleName.Text, Convert.ToInt32(ddlBuyers.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now);
            if (returnValue > 0)
            {

                int styleColorSaveResult = SaveStyleColors(BuyerStyleId);
                int styleSizeSaveResult = SaveStyleSizes(BuyerStyleId);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Style update was failed.')", true);
            }


            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearStyleName();

            styleActionTitle.Text = "Add a New Style:";
            btnSave.Visible = true;
            btnUpdate.Visible = false;

        }


        private void ClearStyleName()
        {
            tbxStyleName.Text = string.Empty;

        }


        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)

        {

            if (e.CommandName == "Edit")
            {

                int buyerStyleId = Convert.ToInt32(e.CommandArgument.ToString());
                PopulateStyles(buyerStyleId);


            }
            else if (e.CommandName == "Delete")
            {
                BuyerStyleId = Convert.ToInt32(e.CommandArgument.ToString());

                var styleCannotBeDeleted = unitOfWork.GenericRepositories<viewStylesCanNotBeDeleted>().GetByID(BuyerStyleId);
                if (styleCannotBeDeleted == null)
                {
                    var title = "Warning";
                    var msg = "Are you sure you want to delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                }
                else
                {
                    var title = "Warning";
                    var msg = "Sorry, you can not delete it.<br />There was some dependencies of this style.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                }
            }
        }

        private void PopulateStyles(int buyerStyleId)
        {
            DataTable dt = buyerManager.GetStyleById(buyerStyleId);
            if (dt.Rows.Count > 0)
            {
                BuyerStyleId = Convert.ToInt32(dt.Rows[0]["Id"]);
                tbxStyleName.Text = dt.Rows[0]["StyleName"].ToString();
                ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, Convert.ToInt32(dt.Rows[0]["BuyerId"]));

                styleActionTitle.Text = "Update Style:";
                btnSave.Visible = false;
                btnUpdate.Visible = true;

                LoadBuyerColors(int.Parse(ddlBuyers.SelectedValue));
                LoadBuyerSizes(int.Parse(ddlBuyers.SelectedValue));

                SelectStyleColorCheckBoxListItems(cblBuyerColors, BuyerStyleId);
                SelectStyleSizeCheckBoxListItems(cblBuyerSizes, BuyerStyleId);
            }

        }


        private void DeleteStyle(int buyerStyleId)
        {

            int returnValue = buyerManager.DeleteBuyerStyle(buyerStyleId, CommonMethods.SessionInfo.UserName, DateTime.Now);

            if (returnValue > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Style deletion was failed.');", true);
            }

            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearStyleName();


            styleActionTitle.Text = "Add a New Style:";
            btnSave.Visible = true;
            btnUpdate.Visible = false;

        }


        protected void btnCheckAllBuyerColors_Click(object sender, EventArgs e)
        {
            SelectAllColors();
        }

        protected void btnUnCheckAllBuyerColors_Click(object sender, EventArgs e)
        {
            UnSelectAllColors();
        }


        private void SelectAllColors()
        {
            foreach (ListItem li in cblBuyerColors.Items)
            {
                li.Selected = true;
            }

        }

        private void UnSelectAllColors()
        {
            foreach (ListItem li in cblBuyerColors.Items)
            {
                li.Selected = false;
            }

        }


        protected void btnCheckAllBuyerSizes_Click(object sender, EventArgs e)
        {
            SelectAllSizes();
        }

        protected void btnUnCheckAllBuyerSizes_Click(object sender, EventArgs e)
        {
            UnSelectAllSizes();
        }



        private void SelectAllSizes()
        {
            foreach (ListItem li in cblBuyerSizes.Items)
            {
                li.Selected = true;
            }

        }

        private void UnSelectAllSizes()
        {
            foreach (ListItem li in cblBuyerSizes.Items)
            {
                li.Selected = false;
            }

        }



        protected void LoadBuyerColors(int buyerId)
        {

            pnlBuyerColors.Visible = true;

            DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetColorsByBuyer '{buyerId}'");

            if (dt.Rows.Count > 0)
            {
                cblBuyerColors.DataValueField = "Id";
                cblBuyerColors.DataTextField = "ColorDescription";
                cblBuyerColors.DataSource = dt;
                cblBuyerColors.DataBind();
            }
        }


        protected void LoadBuyerSizes(int buyerId)
        {

            pnlBuyerSizes.Visible = true;

            DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetSizesByBuyer '{buyerId}'");

            if (dt.Rows.Count > 0)
            {
                cblBuyerSizes.DataValueField = "Id";
                cblBuyerSizes.DataTextField = "SizeName";
                cblBuyerSizes.DataSource = dt;
                cblBuyerSizes.DataBind();
            }
        }


        public void SelectStyleColorCheckBoxListItems(CheckBoxList cbl, int styleId)
        {
            DataTable dt = new CommonManager().GetStyleColorsByStyle(styleId);

            foreach (ListItem item in cbl.Items)
            {

                foreach (DataRow row in dt.Rows)
                {
                    if (item.Value.ToString() == row["BuyerColorId"].ToString())
                    {
                        item.Selected = true;
                    }
                }
            }

        }

        public void SelectStyleSizeCheckBoxListItems(CheckBoxList cbl, int styleId)
        {
            DataTable dt = new CommonManager().GetStyleSizesByStyle(styleId);

            foreach (ListItem item in cbl.Items)
            {

                foreach (DataRow row in dt.Rows)
                {
                    if (item.Value.ToString() == row["Id"].ToString())
                    {
                        item.Selected = true;
                    }
                }
            }

        }



    }
}