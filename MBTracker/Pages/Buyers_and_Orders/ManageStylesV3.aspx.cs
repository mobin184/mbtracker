﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using Repositories;
using MBTracker.EF;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class ManageStylesV3 : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                EnableDisableDeleteButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                LoadStyleYear();
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteStyle(BuyerStyleId);
                }
            }

        }

        private void LoadStyleSeason(int buyerId)
        {
            var dt = unitOfWork.GenericRepositories<BuyerSeasons>().Get(x => x.IsActive == 1 && x.BuyerId == buyerId).ToList();
            ddlStyleSeasons.DataSource = dt;
            ddlStyleSeasons.DataTextField = "SeasonName";
            ddlStyleSeasons.DataValueField = "Id";
            ddlStyleSeasons.DataBind();
            ddlStyleSeasons.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlStyleSeasons.SelectedIndex = 0;
        }

        private void LoadStyleYear()
        {
            var dt = unitOfWork.GenericRepositories<Years>().Get(x => x.IsActive == 1).ToList();
            ddlStyleYear.DataSource = dt;
            ddlStyleYear.DataTextField = "Year";
            ddlStyleYear.DataValueField = "Year";
            ddlStyleYear.DataBind();
            ddlStyleYear.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlStyleYear.SelectedIndex = 0;
        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Knitting - Mgmt");
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ManageStylesV2", 1, 1);
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ManageStylesV2", 2, 1);

        }

        int BuyerStyleId
        {
            set { ViewState["styleId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["styleId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
           // DataTable dt = new DataTable();
           // ddlBuyerDeprtments.DataSource = dt;
            //ddlBuyerDeprtments.DataBind();

            if (ddlBuyers.SelectedValue != "")
            {
                //int selectedBuyerValue = 0;
                //selectedBuyerValue = Convert.ToInt32(ddlBuyers.SelectedValue);
                //CommonMethods.LoadDropdownById(ddlBuyerDeprtments, selectedBuyerValue, "BuyerDepartments", 1, 0);
                BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
                LoadStyleSeason(Convert.ToInt32(ddlBuyers.SelectedValue));
               
            }
            else
            {
                pnlStyles.Visible = false;
                pnlBuyerColors.Visible = false;
                pnlBuyerSizes.Visible = false;
                ddlStyleSeasons.Items.Clear();
            }
        }

        protected void ddlBuyersSizes_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue != "")
            {
              Int32 buyerId = Convert.ToInt32(ddlBuyers.SelectedValue);
                LoadBuyerSizes(buyerId);
                if (System.Web.HttpContext.Current.Session["buyerStyleId"] != null)
                {
                    int buyerStyleId = Convert.ToInt32(Session["buyerStyleId"].ToString());
                    SelectStyleSizeCheckBoxListItems(cblBuyerSizes, buyerStyleId);
                }
            }
            else
            {
                //pnlStyles.Visible = false;
                //pnlBuyerColors.Visible = false;
                //pnlBuyerSizes.Visible = false;
                //ddlStyleSeasons.Items.Clear();
            }
        }


        protected void ddlBuyersColors_SelectedIndexChanged(object sender, EventArgs e)
        {
            Int32 buyerId = Convert.ToInt32(ddlBuyers.SelectedValue);
            LoadBuyerColors(buyerId);

            if (ddlBuyers.SelectedValue != "")
            {
                if (System.Web.HttpContext.Current.Session["buyerStyleId"] != null) {
                    int buyerStyleId = Convert.ToInt32(Session["buyerStyleId"].ToString());
                    SelectStyleColorCheckBoxListItems(cblBuyerColors, buyerStyleId);
                    PopulateStyles(buyerStyleId);
                } 
            }
            else
            {
                //pnlStyles.Visible = false;
                //pnlBuyerColors.Visible = false;
                //pnlBuyerSizes.Visible = false;
                //ddlStyleSeasons.Items.Clear();
            }
        }

        protected void ddlBuyersStyleSeason_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue != "")
            {
                LoadStyleSeason(Convert.ToInt32(ddlBuyers.SelectedValue));

                
            }
            else
            {
                //pnlStyles.Visible = false;
                //pnlBuyerColors.Visible = false;
                //pnlBuyerSizes.Visible = false;
                //ddlStyleSeasons.Items.Clear();
            }
        }

        private void BindData(int buyerId)
        {


            LoadBuyerColors(buyerId);
            LoadBuyerSizes(buyerId);

            DataTable dt = new DataTable();

            dt = buyerManager.LoadStylesByBuyer(buyerId);
            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                pnlStyles.Visible = true;
               // Session["UserRoleID"] = CommonMethods.SessionInfo.RoleId;
            }
            else
            {
                pnlStyles.Visible = false;
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {

                //int styleId = buyerManager.InsertStyle(tbxStyleName.Text, Convert.ToInt32(ddlBuyers.SelectedValue), ddlStyleSeasons.SelectedValue, ddlStyleYear.SelectedValue, CommonMethods.SessionInfo.UserName, DateTime.Now);
               
                var isExist = unitOfWork.GenericRepositories<BuyerStyles>().IsExist(x => x.StyleName == tbxStyleName.Text.Trim());
                if(ddlStyleSeasons.SelectedValue != "" || ddlStyleYear.SelectedValue != "") { 
                if (!isExist || tbxStyleName.Text.Trim() == "N/A")
                {
                    var nStyle = new BuyerStyles()
                    {
                        StyleName = tbxStyleName.Text.Trim(),
                        BuyerId = int.Parse(ddlBuyers.SelectedValue),
                        IsActive = 1,
                        IsRunning = 1,
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };
                    if (ddlStyleSeasons.SelectedValue != "")
                    {
                        nStyle.SeasonId = int.Parse(ddlStyleSeasons.SelectedValue);
                    }
                    if (ddlStyleYear.SelectedValue != "")
                    {
                        nStyle.StyleYear = int.Parse(ddlStyleYear.SelectedValue);
                    }

                    unitOfWork.GenericRepositories<BuyerStyles>().Insert(nStyle);
                    unitOfWork.Save();

                    int styleColorSaveResult = SaveStyleColors(nStyle.Id);
                    int styleSizeSaveResult = SaveStyleSizes(nStyle.Id);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);

                    ddlStyleYear.SelectedIndex = 0;
                    ddlStyleSeasons.SelectedIndex = 0;

                    BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
                    ClearStyleName();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Style name exists.')", true);
                }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please Fill in the star marks data')", true);
                }

                //int styleId = buyerManager.InsertStyle(tbxStyleName.Text, Convert.ToInt32(ddlBuyers.SelectedValue), ddlStyleSeasons.SelectedValue, ddlStyleYear.SelectedValue, CommonMethods.SessionInfo.UserName, DateTime.Now);

                //if (styleId > 0)
                //{

                //    int styleColorSaveResult = SaveStyleColors(styleId);
                //    int styleSizeSaveResult = SaveStyleSizes(styleId);
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);

                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Style name exists.')", true);
                //}
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }



        }


        public int SaveStyleColors(int styleId)
        {


            DataTable dt = StyleColors();


            for (int i = 0; i < cblBuyerColors.Items.Count; i++)
            {
                if (cblBuyerColors.Items[i].Selected)
                {
                    dt.Rows.Add(styleId, cblBuyerColors.Items[i].Value, CommonMethods.SessionInfo.UserName, DateTime.Now);
                }
            }

            buyerManager.DeleteStyleColors(styleId);
            return buyerManager.SaveStyleColors(dt);

        }




        public DataTable StyleColors()
        {
            DataTable dt = new DataTable("StyleColors");
            dt.Columns.Add("SId", typeof(int));
            dt.Columns.Add("CId", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }


        public int SaveStyleSizes(int styleId)
        {


            DataTable dt = StyleSizes();


            for (int i = 0; i < cblBuyerSizes.Items.Count; i++)
            {
                if (cblBuyerSizes.Items[i].Selected)
                {
                    dt.Rows.Add(styleId, cblBuyerSizes.Items[i].Value, CommonMethods.SessionInfo.UserName, DateTime.Now);
                }
            }

            buyerManager.DeleteStyleSizes(styleId);
            return buyerManager.SaveStyleSizes(dt);

        }

        public DataTable StyleSizes()
        {
            DataTable dt = new DataTable("StyleSizes");
            dt.Columns.Add("SId", typeof(int));
            dt.Columns.Add("SizeId", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if(ddlStyleSeasons.SelectedValue != "" || ddlStyleYear.SelectedValue != "") { 
                var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(BuyerStyleId);
                styleInfo.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                styleInfo.StyleName = tbxStyleName.Text.Trim();
                if (ddlStyleSeasons.SelectedValue != "")
                {
                    styleInfo.SeasonId = int.Parse(ddlStyleSeasons.SelectedValue);
                }
                else
                {
                    styleInfo.SeasonId = null;
                }

                if (ddlStyleYear.SelectedValue != "")
                {
                    styleInfo.StyleYear = int.Parse(ddlStyleYear.SelectedValue);
                }
                else
                {
                    styleInfo.StyleYear = null;
                }
                styleInfo.UpdateDate = DateTime.Now;
                styleInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;

                unitOfWork.GenericRepositories<BuyerStyles>().Update(styleInfo);

                var olsStyleColors = unitOfWork.GenericRepositories<StyleColors>().Get(x => x.StyleId == BuyerStyleId).ToList();
                var oldStyleSizes = unitOfWork.GenericRepositories<StyleSizes>().Get(x => x.StyleId == BuyerStyleId).ToList();
                foreach (var item in olsStyleColors)
                {
                    unitOfWork.GenericRepositories<StyleColors>().Delete(item);
                }

                foreach (var item in oldStyleSizes)
                {
                    unitOfWork.GenericRepositories<StyleSizes>().Delete(item);
                }

                unitOfWork.Save();

                //int returnValue = buyerManager.UpdateBuyerStyle(BuyerStyleId, tbxStyleName.Text, Convert.ToInt32(ddlBuyers.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now);
                //if (returnValue > 0)
                //{

                int styleColorSaveResult = SaveStyleColors(BuyerStyleId);
                int styleSizeSaveResult = SaveStyleSizes(BuyerStyleId);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Style update was failed.')", true);
                //}


                BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
                ClearStyleName();
                ddlStyleYear.SelectedIndex = 0;
                ddlStyleSeasons.SelectedIndex = 0;


                styleActionTitle.Text = "Add a New Style:";
                btnSave.Visible = true;
                btnUpdate.Visible = false;
               }
               else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please Fill in the star marks data')", true);
                }
                Session.Remove("buyerStyleId");
            }
            catch (Exception)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Style update was failed.')", true);
            }


        }


        private void ClearStyleName()
        {
            tbxStyleName.Text = string.Empty;

        }


        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)

        {

            if (e.CommandName == "Edit")
            {

                int buyerStyleId = Convert.ToInt32(e.CommandArgument.ToString());
                PopulateStyles(buyerStyleId);
                Session["buyerStyleId"] = buyerStyleId;

            }
            else if (e.CommandName == "Delete")
            {
                BuyerStyleId = Convert.ToInt32(e.CommandArgument.ToString());

                var styleCannotBeDeleted = unitOfWork.GenericRepositories<viewStylesCanNotBeDeleted>().GetByID(BuyerStyleId);
                if (styleCannotBeDeleted == null)
                {
                    var title = "Warning";
                    var msg = "Are you sure you want to delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                }
                else
                {
                    var title = "Warning";
                    var msg = "Sorry, you can not delete it.<br />There was some dependencies of this style.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                }
            }
            else if (e.CommandName == "RunningStatus")
            {

                int buyerStyleId = Convert.ToInt32(e.CommandArgument.ToString());
                var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(buyerStyleId);
                if(styleInfo != null)
                {
                    if(styleInfo.IsRunning == 1)
                    {
                        styleInfo.IsRunning = 0;
                    }
                    else
                    {
                        styleInfo.IsRunning = 1;
                    }
                    unitOfWork.GenericRepositories<BuyerStyles>().Update(styleInfo);
                    unitOfWork.Save();
                    BindData(styleInfo.BuyerId);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Running status updated successfully.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Style not found.');", true);
                }
            }
        }

        private void PopulateStyles(int buyerStyleId)
        {
            var currentUserRoleId = CommonMethods.SessionInfo.RoleId;
            var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(buyerStyleId);
            if (styleInfo != null)
            {
                BuyerStyleId = styleInfo.Id;
                tbxStyleName.Text = styleInfo.StyleName;
                ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
                LoadStyleSeason(styleInfo.BuyerId);
                LoadStyleYear();

                ddlStyleSeasons.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyleSeasons, styleInfo.SeasonId ?? 0);
                ddlStyleYear.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyleYear, styleInfo.StyleYear ?? 0);
                tbxStyleName.Enabled = (currentUserRoleId == 1) ? true : false;
                ddlBuyers.Enabled = (currentUserRoleId == 1) ? true : false;

                styleActionTitle.Text = "Update Style:";
                btnSave.Visible = false;
                btnUpdate.Visible = true;

                LoadBuyerColors(styleInfo.BuyerId);
                LoadBuyerSizes(styleInfo.BuyerId);

                SelectStyleColorCheckBoxListItems(cblBuyerColors, buyerStyleId);
                SelectStyleSizeCheckBoxListItems(cblBuyerSizes, buyerStyleId);
            }
        }


        private void DeleteStyle(int buyerStyleId)
        {

            int returnValue = buyerManager.DeleteBuyerStyle(buyerStyleId, CommonMethods.SessionInfo.UserName, DateTime.Now);

            if (returnValue > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Style deletion was failed.');", true);
            }

            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearStyleName();


            styleActionTitle.Text = "Add a New Style:";
            btnSave.Visible = true;
            btnUpdate.Visible = false;

        }


        protected void btnCheckAllBuyerColors_Click(object sender, EventArgs e)
        {
            SelectAllColors();
        }

        protected void btnUnCheckAllBuyerColors_Click(object sender, EventArgs e)
        {
            UnSelectAllColors();
        }


        private void SelectAllColors()
        {
            foreach (ListItem li in cblBuyerColors.Items)
            {
                li.Selected = true;
            }

        }

        private void UnSelectAllColors()
        {
            foreach (ListItem li in cblBuyerColors.Items)
            {
                li.Selected = false;
            }

        }


        protected void btnCheckAllBuyerSizes_Click(object sender, EventArgs e)
        {
            SelectAllSizes();
        }

        protected void btnUnCheckAllBuyerSizes_Click(object sender, EventArgs e)
        {
            UnSelectAllSizes();
        }



        private void SelectAllSizes()
        {
            foreach (ListItem li in cblBuyerSizes.Items)
            {
                li.Selected = true;
            }

        }

        private void UnSelectAllSizes()
        {
            foreach (ListItem li in cblBuyerSizes.Items)
            {
                li.Selected = false;
            }

        }



        protected void LoadBuyerColors(int buyerId)
        {

            pnlBuyerColors.Visible = true;

            DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetColorsByBuyer '{buyerId}'");

            if (dt.Rows.Count > 0)
            {
                cblBuyerColors.DataValueField = "Id";
                cblBuyerColors.DataTextField = "ColorDescription";
                cblBuyerColors.DataSource = dt;
                cblBuyerColors.DataBind();
            }
            else
            {
                pnlBuyerColors.Visible = false;
            }
        }


        protected void LoadBuyerSizes(int buyerId)
        {

            pnlBuyerSizes.Visible = true;

            DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetSizesByBuyer '{buyerId}'");

            if (dt.Rows.Count > 0)
            {
                cblBuyerSizes.DataValueField = "Id";
                cblBuyerSizes.DataTextField = "SizeName";
                cblBuyerSizes.DataSource = dt;
                cblBuyerSizes.DataBind();
            }
            else
            {
                pnlBuyerSizes.Visible = false;
            }
        }


        public void SelectStyleColorCheckBoxListItems(CheckBoxList cbl, int styleId)
        {
            DataTable dt = new CommonManager().GetStyleColorsByStyle(styleId);

            foreach (ListItem item in cbl.Items)
            {

                foreach (DataRow row in dt.Rows)
                {
                    if (item.Value.ToString() == row["BuyerColorId"].ToString())
                    {
                        item.Selected = true;
                    }
                }
            }

        }

        public void SelectStyleSizeCheckBoxListItems(CheckBoxList cbl, int styleId)
        {
            DataTable dt = new CommonManager().GetStyleSizesByStyle(styleId);

            foreach (ListItem item in cbl.Items)
            {

                foreach (DataRow row in dt.Rows)
                {
                    if (item.Value.ToString() == row["Id"].ToString())
                    {
                        item.Selected = true;
                    }
                }
            }

        }



    }
}