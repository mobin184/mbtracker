﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class ViewTimeAndActionPlans : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableDeleteButton();
                EnableDisableEditButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyerName, 1, 0);
                CommonMethods.LoadDropdown(ddlYear, "Years WHERE IsActive = 1", 1, 0);

                if (Request.QueryString["showInfo"] != null)
                {
                    TNAId = int.Parse(Tools.UrlDecode(Request.QueryString["showInfo"]));
                    ddlBuyerName.Enabled = false;
                    ddlStyleName.Enabled = false;
                    ddlYear.Enabled = false;
                    //btnUpdate.Visible = true;
                    btnSearch.Visible = false;
                    var tnaInfo = unitOfWork.GenericRepositories<TimeAndActionPlans>().GetByID(TNAId);
                    BindStylesByBuyer(tnaInfo.BuyerId);
                    ddlBuyerName.SelectedValue = tnaInfo.BuyerId + "";
                    ddlStyleName.SelectedValue = tnaInfo.StyleId + "";
                    ddlYear.SelectedValue = tnaInfo.YearId + "";

                    if (CommonMethods.HasBuyerAssignedToUser(tnaInfo.BuyerId, CommonMethods.SessionInfo.UserId))
                    {
                        ShowTimeAndActionPlans(tnaInfo.BuyerId, tnaInfo.StyleId, tnaInfo.YearId);
                    }
                    else
                    {
                        lblNotAuthorizedToView.Visible = true;
                        FilterDiv.Visible = false;
                    }
                }
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteTNA();
                }
            }
        }

        int TNAId
        {
            set { ViewState["tna"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["tna"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void DeleteTNA()
        {
            try
            {
                unitOfWork.GenericRepositories<TimeAndActionPlans>().Delete(TNAId);
                unitOfWork.Save();
                BindData();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");

            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewTimeAndActionPlans", 2, 1);
        }
        private void EnableDisableEditButton()
        {
            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Merchandiser, QCE");
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewTimeAndActionPlans", 1, 1);

        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBuyerName.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyerName.SelectedValue));
            }
            else
            {
                ddlStyleName.Items.Clear();
            }
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyleName, buyerId, "BuyerStyles", 1, 0);
        }
        private void BindData()
        {


            if (ddlBuyerName.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a buyer.')", true);
            }
            else if (ddlStyleName.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select an style.')", true);
            }
            if (ddlYear.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a year.')", true);
            }
            else
            {
                var BuyerId = string.IsNullOrEmpty(ddlBuyerName.SelectedValue) ? (int?)null : Int32.Parse(ddlBuyerName.SelectedValue);
                var StyleId = string.IsNullOrEmpty(ddlStyleName.SelectedValue) ? (int?)null : Int32.Parse(ddlStyleName.SelectedValue);
                var YearId = int.Parse(ddlYear.SelectedValue);
                ShowTimeAndActionPlans(BuyerId, StyleId, YearId);
            }
        }


        private void ShowTimeAndActionPlans(int? buyerId, int? styleId, int? yearId)
        {
            divTimeAndActionPlans.Visible = true;
            var userId = CommonMethods.SessionInfo.UserId;
            DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetAllTimeAndActionPlans '{buyerId}','{styleId}','{yearId}',{userId}");
            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                lblNoDataFound.Visible = false;
            }
            else
            {
                rpt.DataSource = null;
                rpt.DataBind();
                lblNoDataFound.Text = "No data found!";
                lblNoDataFound.Visible = true;
            }
        }


        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            int tnaId = Convert.ToInt32(e.CommandArgument.ToString());
            Response.Redirect("AddTimeAndActionPlan.aspx?TNAId=" + Tools.UrlEncode(tnaId + ""));
        }


        protected void lnkbtnAddNote_Command(object sender, CommandEventArgs e)
        {
            int tnaId = Convert.ToInt32(e.CommandArgument.ToString());
            //Response.Redirect("AddTimeAndActionPlanNote.aspx?TNAId=" + Tools.UrlEncode(tnaId + ""));
            Response.Redirect("AddTimeAndActionPlanNote.aspx?TNAId=" + tnaId);
        }



        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }
        protected void btnShowFilter_Click(object sender, EventArgs e)
        {
            FilterDiv.Visible = true;
            btnShowFilter.Visible = false;
            btnHideFilter.Visible = true;
        }
        protected void btnHineFilter_Click(object sender, EventArgs e)
        {
            FilterDiv.Visible = false;
            btnShowFilter.Visible = true;
            btnHideFilter.Visible = false;
        }

        protected void btnResetFilter_Click(object sender, EventArgs e)
        {
            ddlBuyerName.SelectedIndex = 0;
            ddlStyleName.Items.Clear();
            ddlYear.SelectedIndex = 0;
            //ddlMachineBrand.SelectedIndex = 0;
            //ddlFinishingUnit.SelectedIndex = 0;
            BindData();
        }

        //protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        //{

        //    try
        //    {
        //        detailsDiv.Visible = true;
        //        int tnaId = Convert.ToInt32(e.CommandArgument.ToString());
        //        var info = unitOfWork.GetRecordSet<VM_TimeAndActionPlans>($"EXEC usp_GetATimeAndActionPlanInfoById {tnaId}").FirstOrDefault();
        //        if (info != null)
        //        {
        //            lblBuyer.Text = info.BuyerName;
        //            lblStyle.Text = info.StyleName;
        //            lblYear.Text = info.Year + "";
        //            lblSeason.Text = info.SeasonName;
        //            lblBookedCapacityQty.Text = info.BookedCapacityQuantity + "";
        //            lblOrderSheetReceivedDate.Text = info.OrderSheetReceivedDate?.ToString("dd-MMM-yyyy");
        //            lblPONumber.Text = info.OrderNumber;
        //            lblOrderQuantity.Text = info.OrderQuantity + "";
        //            lblInitialShipmentDate.Text = info.InitialShipmentDate?.ToString("dd-MMM-yyyy");
        //            lblExtendShipmentDate.Text = info.ExtendedShipmentDate?.ToString("dd-MMM-yyyy");
        //            lblFOB.Text = info.FOB;
        //            lblTotalFOB.Text = info.TotalFOB;
        //            lblActualExFactory.Text = info.ActualExFactoryDate?.ToString("dd-MMM-yyyy");
        //            lblPlusShipQty.Text = info.PlusShipQty + "";
        //            lblLessShipQty.Text = info.LessShipQty + "";
        //            lblMasterLCReceivedDate.Text = info.MasterLCOrContactReceivedDate?.ToString("dd-MMM-yyyy");
        //            lblMasterLCValue.Text = info.MasterLCValue + "";
        //            lblShipmentValue.Text = info.ShipmentValue + "";
        //            lblFinishingUnit.Text = info.FinishingUnit;
        //            lblYarnComCountPly.Text = info.YarnComCountAndPly;
        //            lblSampleMCBrand.Text = info.KnittingMachineBrand;
        //            lblSampleTiming.Text = info.SampleTiming + "";
        //            lblKnittingOutput.Text = info.KnittingOutput;
        //            lblKnittingMCGauge.Text = info.KnittingMCGauge + "";
        //            lblLinkingMCGauge.Text = info.LinkingMCGauge;
        //            lblLinkingTiming.Text = info.LinkingTiming + "";
        //            lblLinkingOutput.Text = info.LinkingOutput;
        //            lblWeightPerDz.Text = info.WeightPerDz + "";
        //            lblDyeingOrderPlaceDate.Text = info.DyeingOrderPlaceDate?.ToString("dd-MMM-yyyy");
        //            lblYarnSupplier.Text = info.YarnSupplier;
        //            lblPIReceivedDate.Text = info.PIReceivedDate?.ToString("dd-MMM-yyyy");
        //            lblPINumber.Text = info.PINumber;
        //            lblPIValue.Text = info.PIValue + "";
        //            lblYarnETD.Text = info.YarnETD?.ToString("dd-MMM-yyyy");
        //            lblYarnETA.Text = info.YarnETA?.ToString("dd-MMM-yyyy");
        //            lblYarnInHouseDate.Text = info.YarnInHouseDate?.ToString("dd-MMM-yyyy");
        //            lblKnittingAccesoriesInHouseDate.Text = info.KnittingAccessoriesInHouseDate?.ToString("dd-MMM-yyyy");
        //            lblLinkingAdditinalInHouseDate.Text = info.LinkingAdditionalInHouseDate?.ToString("dd-MMM-yyyy");
        //            lblMainSizeCareBookingDate.Text = info.MainSizeCareLabelBookingDate?.ToString("dd-MMM-yyyy");
        //            lblMainSizeCarePIReceivedDate.Text = info.MainSizeCareLabelPIReceivedDate?.ToString("dd-MMM-yyyy");
        //            lblMainSizeCarePINumber.Text = info.MainSizeCareLabelPINumber;
        //            lblMainSizeCarePIValue.Text = info.MainSizeCareLabelPIValue + "";
        //            lblMainSizeCarePISignAndSendToCom.Text = info.MainSizeCareLabelPISendToCommercialDate?.ToString("dd-MMM-yyyy");
        //            lblMainSizeCareLCOpenDate.Text = info.MainSizeCareLabelLCOpenDate?.ToString("dd-MMM-yyyy");
        //            lblMainSizeCareGoodsInHouseDate.Text = info.MainSizeCareLabelGoodsInHouseDate?.ToString("dd-MMM-yyyy");
        //            lblHangOrPriceTagBookingDate.Text = info.HangOrPriceTagBookingDate?.ToString("dd-MMM-yyyy");
        //            lblHangOrPriceTagPIReceivedDate.Text = info.HangOrPriceTagPIReceivedDate?.ToString("dd-MMM-yyyy");
        //            lblHangOrPriceTagPINumber.Text = info.HangOrPriceTagPINumber;
        //            lblHangOrPriceTagPIValue.Text = info.HangOrPriceTagPIValue + "";
        //            lblHangOrPriceTagPISignAndSendToCom.Text = info.HangOrPriceTagPISendToCommercialDate?.ToString("dd-MMM-yyyy");
        //            lblHangOrPriceTagLCOpenDate.Text = info.HangOrPriceTagLCOpenDate?.ToString("dd-MMM-yyyy");
        //            lblHangOrPriceTagGoodsInHouseDate.Text = info.HangOrPriceTagGoodsInHouseDate?.ToString("dd-MMM-yyyy");
        //            lblButtonZipperBookingDate.Text = info.ButtonZipperBookingDate?.ToString("dd-MMM-yyyy");
        //            lblButtonZipperPIReceivedDate.Text = info.ButtonZipperPIReceivedDate?.ToString("dd-MMM-yyyy");
        //            lblButtonZipperPINumber.Text = info.ButtonZipperPINumber;
        //            lblButtonZipperPIValue.Text = info.ButtonZipperPIValue + "";
        //            lblButtonZipperPISignAndSendToCom.Text = info.ButtonZipperPISendToCommercialDate?.ToString("dd-MMM-yyyy");
        //            lblButtonZipperLCOpenDate.Text = info.ButtonZipperLCOpenDate?.ToString("dd-MMM-yyyy");
        //            lblButtonZipperGoodsInHouseDate.Text = info.ButtonZipperGoodsInHouseDate?.ToString("dd-MMM-yyyy");
        //            lblBadgeFlowerFabricsBookingDate.Text = info.BadgeFlowerFabricsBookingDate?.ToString("dd-MMM-yyyy");
        //            lblBadgeFlowerFabricsPIReceivedDate.Text = info.BadgeFlowerFabricsPIReceivedDate?.ToString("dd-MMM-yyyy");
        //            lblBadgeFlowerFabricsPINumber.Text = info.BadgeFlowerFabricsPINumber;
        //            lblBadgeFlowerFabricsPIValue.Text = info.BadgeFlowerFabricsPIValue + "";
        //            lblBadgeFlowerFabricsPISignAndSendToCom.Text = info.BadgeFlowerFabricsPISendToCommercialDate?.ToString("dd-MMM-yyyy");
        //            lblBadgeFlowerFabricsLCOpenDate.Text = info.BadgeFlowerFabricsLCOpenDate?.ToString("dd-MMM-yyyy");
        //            lblBadgeFlowerFabricsGoodsInHouseDate.Text = info.BadgeFlowerFabricsGoodsInHouseDate?.ToString("dd-MMM-yyyy");
        //            lblPrintEmbroideryBookingDate.Text = info.PrintEmbroideryBookingDate?.ToString("dd-MMM-yyyy");
        //            lblPrintEmbroideryPIReceivedDate.Text = info.HangOrPriceTagPIReceivedDate?.ToString("dd-MMM-yyyy");
        //            lblPrintEmbroideryPINumber.Text = info.PrintEmbroideryPINumber;
        //            lblPrintEmbroideryPIValue.Text = info.PrintEmbroideryPIValue + "";
        //            lblPrintEmbroideryPISignAndSendToCom.Text = info.PrintEmbroideryPISendToCommercialDate?.ToString("dd-MMM-yyyy");
        //            lblPrintEmbroideryLCOpenDate.Text = info.PrintEmbroideryLCOpenDate?.ToString("dd-MMM-yyyy");
        //            lblPrintEmbroideryGoodsInHouseDate.Text = info.PrintEmbroideryGoodsInHouseDate?.ToString("dd-MMM-yyyy");
        //            lblSewingShoulderTapBookingDate.Text = info.SewingShoulderTapBookingDate?.ToString("dd-MMM-yyyy");
        //            lblSewingShoulderTapPIReceivedDate.Text = info.SewingShoulderTapPIReceivedDate?.ToString("dd-MMM-yyyy");
        //            lblSewingShoulderTapPINumber.Text = info.SewingShoulderTapPINumber;
        //            lblSewingShoulderTapPIValue.Text = info.SewingShoulderTapPIValue + "";
        //            lblSewingShoulderTapPISignAndSendToCom.Text = info.SewingShoulderTapPISendToCommercialDate?.ToString("dd-MMM-yyyy");
        //            lblSewingShoulderTapLCOpenDate.Text = info.SewingShoulderTapLCOpenDate?.ToString("dd-MMM-yyyy");
        //            lblSewingShoulderTapGoodsInHouseDate.Text = info.SewingShoulderTapGoodsInHouseDate?.ToString("dd-MMM-yyyy");
        //            lblPolyBookingDate.Text = info.PolyBookingDate?.ToString("dd-MMM-yyyy");
        //            lblPolyPIReceivedDate.Text = info.PolyPIReceivedDate?.ToString("dd-MMM-yyyy");
        //            lblPolyPINumber.Text = info.PolyPINumber;
        //            lblPolyPIValue.Text = info.PolyPIValue + "";
        //            lblPolyPISignAndSendToCom.Text = info.PolyPISendToCommercialDate?.ToString("dd-MMM-yyyy");
        //            lblPolyLCOpenDate.Text = info.PolyLCOpenDate?.ToString("dd-MMM-yyyy");
        //            lblPolyGoodsInHouseDate.Text = info.PolyGoodsInHouseDate?.ToString("dd-MMM-yyyy");
        //            lblCartoonBookingDate.Text = info.CartoonBookingDate?.ToString("dd-MMM-yyyy");
        //            lblCartoonPIReceivedDate.Text = info.CartoonPIReceivedDate?.ToString("dd-MMM-yyyy");
        //            lblCartoonPINumber.Text = info.CartoonPINumber;
        //            lblCartoonPIValue.Text = info.CartoonPIValue + "";
        //            lblCartoonPISignAndSendToCom.Text = info.CartoonPISendToCommercialDate?.ToString("dd-MMM-yyyy");
        //            lblCartoonLCOpenDate.Text = info.CartoonLCOpenDate?.ToString("dd-MMM-yyyy");
        //            lblCartoonGoodsInHouseDate.Text = info.CartoonGoodsInHouseDate?.ToString("dd-MMM-yyyy");

        //            var dtSamplePicture = unitOfWork.GetDataTableFromSql($"SELECT Id,FileName FROM TimeAndActionPlanSamplePictures WHERE TimeAndActionPlanId={tnaId}");
        //            var dtDyeingOrderFiles = unitOfWork.GetDataTableFromSql($"SELECT Id,FileName FROM TimeAndActionPlanDyeingOrderDocuments WHERE TimeAndActionPlanId={tnaId}");

        //            //samplePicture.ImageUrl 
        //            var img = unitOfWork.GenericRepositories<TimeAndActionPlanSamplePictures>().Get(x => x.TimeAndActionPlanId == tnaId).LastOrDefault().FileData;
        //            string base64String = Convert.ToBase64String(img, 0, img.Length);
        //            samplePicture.ImageUrl = "data:image/png;base64," + base64String;
        //            //if(dtSamplePicture.Rows.Count > 0)
        //            //{
        //            //    rptSamplePictures.DataSource = dtSamplePicture;
        //            //    rptSamplePictures.DataBind();
        //            //    lblNoPictureFound.Visible = false;
        //            //    rptSamplePictures.Visible = true;
        //            //}
        //            //else
        //            //{
        //            //    lblNoPictureFound.Visible = true;
        //            //    rptSamplePictures.Visible = false;
        //            //}

        //            if (dtDyeingOrderFiles.Rows.Count > 0)
        //            {
        //                rptDyeingOrderSampleFile.DataSource = dtDyeingOrderFiles;
        //                rptDyeingOrderSampleFile.DataBind();
        //                rptDyeingOrderSampleFile.Visible = true;
        //                lblNoDocumentFound.Visible = false;
        //            }
        //            else
        //            {
        //                rptDyeingOrderSampleFile.Visible = false;
        //                lblNoDocumentFound.Visible = true;
        //            }

        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown()", true);
        //        }
        //        else
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Tima & action plan details not found!');", true);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
        //    }
        //}

        protected void ddlStyleName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        public class VM_TimeAndActionPlans
        {
            public int Id { get; set; }
            public int BuyerId { get; set; }
            public int StyleId { get; set; }
            public Nullable<int> YearId { get; set; }
            public Nullable<int> SeasonId { get; set; }
            public Nullable<int> BookedCapacityQuantity { get; set; }
            public Nullable<int> OrderId { get; set; }
            public Nullable<int> OrderQuantity { get; set; }
            public Nullable<System.DateTime> OrderSheetReceivedDate { get; set; }
            public Nullable<System.DateTime> InitialShipmentDate { get; set; }
            public Nullable<System.DateTime> ExtendedShipmentDate { get; set; }
            public string FOB { get; set; }
            public string TotalFOB { get; set; }
            public Nullable<System.DateTime> ActualExFactoryDate { get; set; }
            public Nullable<int> PlusShipQty { get; set; }
            public Nullable<int> LessShipQty { get; set; }
            public Nullable<System.DateTime> MasterLCOrContactReceivedDate { get; set; }
            public Nullable<decimal> MasterLCValue { get; set; }
            public Nullable<decimal> ShipmentValue { get; set; }
            public Nullable<int> FinishingUnitId { get; set; }
            public string YarnComCountAndPly { get; set; }
            public Nullable<int> SampleMCBrandId { get; set; }
            public Nullable<int> SampleTiming { get; set; }
            public string KnittingOutput { get; set; }
            public Nullable<int> KnittingMCGauge { get; set; }
            public Nullable<int> LinkingMCGaugeId { get; set; }
            public Nullable<int> LinkingTiming { get; set; }
            public string LinkingOutput { get; set; }
            public Nullable<decimal> WeightPerDz { get; set; }
            public Nullable<System.DateTime> DyeingOrderPlaceDate { get; set; }
            public string YarnSupplier { get; set; }
            public Nullable<System.DateTime> PIReceivedDate { get; set; }
            public string PINumber { get; set; }
            public Nullable<decimal> PIValue { get; set; }
            public Nullable<System.DateTime> YarnETD { get; set; }
            public Nullable<System.DateTime> YarnETA { get; set; }
            public Nullable<System.DateTime> YarnInHouseDate { get; set; }
            public Nullable<System.DateTime> KnittingAccessoriesInHouseDate { get; set; }
            public Nullable<System.DateTime> LinkingAdditionalInHouseDate { get; set; }
            public Nullable<System.DateTime> MainSizeCareLabelBookingDate { get; set; }
            public Nullable<System.DateTime> MainSizeCareLabelPIReceivedDate { get; set; }
            public string MainSizeCareLabelPINumber { get; set; }
            public Nullable<decimal> MainSizeCareLabelPIValue { get; set; }
            public Nullable<System.DateTime> MainSizeCareLabelPISendToCommercialDate { get; set; }
            public Nullable<System.DateTime> MainSizeCareLabelLCOpenDate { get; set; }
            public Nullable<System.DateTime> MainSizeCareLabelGoodsInHouseDate { get; set; }
            public Nullable<System.DateTime> HangOrPriceTagBookingDate { get; set; }
            public Nullable<System.DateTime> HangOrPriceTagPIReceivedDate { get; set; }
            public string HangOrPriceTagPINumber { get; set; }
            public Nullable<decimal> HangOrPriceTagPIValue { get; set; }
            public Nullable<System.DateTime> HangOrPriceTagPISendToCommercialDate { get; set; }
            public Nullable<System.DateTime> HangOrPriceTagLCOpenDate { get; set; }
            public Nullable<System.DateTime> HangOrPriceTagGoodsInHouseDate { get; set; }
            public Nullable<System.DateTime> ButtonZipperBookingDate { get; set; }
            public Nullable<System.DateTime> ButtonZipperPIReceivedDate { get; set; }
            public string ButtonZipperPINumber { get; set; }
            public Nullable<decimal> ButtonZipperPIValue { get; set; }
            public Nullable<System.DateTime> ButtonZipperPISendToCommercialDate { get; set; }
            public Nullable<System.DateTime> ButtonZipperLCOpenDate { get; set; }
            public Nullable<System.DateTime> ButtonZipperGoodsInHouseDate { get; set; }
            public Nullable<System.DateTime> BadgeFlowerFabricsBookingDate { get; set; }
            public Nullable<System.DateTime> BadgeFlowerFabricsPIReceivedDate { get; set; }
            public string BadgeFlowerFabricsPINumber { get; set; }
            public Nullable<decimal> BadgeFlowerFabricsPIValue { get; set; }
            public Nullable<System.DateTime> BadgeFlowerFabricsPISendToCommercialDate { get; set; }
            public Nullable<System.DateTime> BadgeFlowerFabricsLCOpenDate { get; set; }
            public Nullable<System.DateTime> BadgeFlowerFabricsGoodsInHouseDate { get; set; }
            public Nullable<System.DateTime> PrintEmbroideryBookingDate { get; set; }
            public Nullable<System.DateTime> PrintEmbroideryPIReceivedDate { get; set; }
            public string PrintEmbroideryPINumber { get; set; }
            public Nullable<decimal> PrintEmbroideryPIValue { get; set; }
            public Nullable<System.DateTime> PrintEmbroideryPISendToCommercialDate { get; set; }
            public Nullable<System.DateTime> PrintEmbroideryLCOpenDate { get; set; }
            public Nullable<System.DateTime> PrintEmbroideryGoodsInHouseDate { get; set; }
            public Nullable<System.DateTime> SewingShoulderTapBookingDate { get; set; }
            public Nullable<System.DateTime> SewingShoulderTapPIReceivedDate { get; set; }
            public string SewingShoulderTapPINumber { get; set; }
            public Nullable<decimal> SewingShoulderTapPIValue { get; set; }
            public Nullable<System.DateTime> SewingShoulderTapPISendToCommercialDate { get; set; }
            public Nullable<System.DateTime> SewingShoulderTapLCOpenDate { get; set; }
            public Nullable<System.DateTime> SewingShoulderTapGoodsInHouseDate { get; set; }
            public Nullable<System.DateTime> PolyBookingDate { get; set; }
            public Nullable<System.DateTime> PolyPIReceivedDate { get; set; }
            public string PolyPINumber { get; set; }
            public Nullable<decimal> PolyPIValue { get; set; }
            public Nullable<System.DateTime> PolyPISendToCommercialDate { get; set; }
            public Nullable<System.DateTime> PolyLCOpenDate { get; set; }
            public Nullable<System.DateTime> PolyGoodsInHouseDate { get; set; }
            public Nullable<System.DateTime> CartoonBookingDate { get; set; }
            public Nullable<System.DateTime> CartoonPIReceivedDate { get; set; }
            public string CartoonPINumber { get; set; }
            public Nullable<decimal> CartoonPIValue { get; set; }
            public Nullable<System.DateTime> CartoonPISendToCommercialDate { get; set; }
            public Nullable<System.DateTime> CartoonLCOpenDate { get; set; }
            public Nullable<System.DateTime> CartoonGoodsInHouseDate { get; set; }
            public string CreatedBy { get; set; }
            public Nullable<System.DateTime> CreateDate { get; set; }
            public string UpdatedBy { get; set; }
            public Nullable<System.DateTime> UpdateDate { get; set; }
            public string BuyerName { get; set; }
            public string StyleName { get; set; }
            public string Year { get; set; }
            public string SeasonName { get; set; }
            public string KnittingMachineBrand { get; set; }
            public string LinkingMCGauge { get; set; }
            public string FinishingUnit { get; set; }
            public string OrderNumber { get; set; }

        }

        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            TNAId = Convert.ToInt32(e.CommandArgument.ToString());

            if (TNAId != 0)
            {
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
            }
            else
            {
                var title = "Error";
                var msg = "Sorry, you can not delete it.<br />Something went wrong.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            }
        }
    }
}