﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class AddConsumptionSheet : Page
    {

        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        UnitOfWork unitOfWork = new UnitOfWork();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);

                if (Request.QueryString["consumptionSheetId"] != null)
                {
                    ConsumptionSheetId = int.Parse(Tools.UrlDecode(Request.QueryString["consumptionSheetId"]));
                    PopulateConsumptionSheet(ConsumptionSheetId);
                    //redAsterisk.Visible = false;
                }
            }
        }

        private void PopulateConsumptionSheet(int consumptionSheetId)
        {
            var conInfo = unitOfWork.GenericRepositories<OrderConsumptionSheets>().GetByID(consumptionSheetId);
            var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().Get(x => x.Id == conInfo.StyleId).FirstOrDefault();

            if (conInfo != null)
            {
                BindStylesByBuyer(styleInfo.BuyerId);
                //BindOrdersByStyle(orderInfo.StyleId);
                SetYarnInfoControlForUpdate(consumptionSheetId);
               
                CommonMethods.LoadMachineBrandDropdown(ddlMachineBrands, 1, 0);

                ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
                ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleInfo.Id);
                //ddlOrders.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrders, orderInfo.Id);
                ddlMachineBrands.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMachineBrands, conInfo.KnittingMachineBrandId);
                ddlStage.ClearSelection();
                ddlStage.Items.FindByText(conInfo.Stage).Selected = true;
                tbxKnittingMCGauge.Text = conInfo.KnittingMachineGauge;
                //tbxNeedlePerCM.Text = conInfo.NeedlePerCM;
                tbxMCSpeed.Text = conInfo.MachineSpeed;
                tbxKnittingTime.Text = conInfo.KnittingTime;
                tbxDesignerName.Text = conInfo.DesignerName;
                tbxDesignerPhone.Text = conInfo.DesignerContactNumber;
                ddlBuyers.Enabled = false;
                ddlStyles.Enabled = false;
                //ddlOrders.Enabled = false;


                pnlYarnConsumption.Visible = true;
                pnlFileUploads.Visible = true;
                btnSave.Visible = false;
                btnUpdate.Visible = true;
                actionTitle.Text = "Update Consumption Sheet";
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Invalid consumption sheet Id!')", true);
            }
        }

        int ConsumptionSheetId
        {
            set { ViewState["consumptionSheetId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["consumptionSheetId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }



        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue != "")
            {

                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            else
            {
                ddlStyles.Items.Clear();
                //ddlOrders.Items.Clear();
                pnlYarnConsumption.Visible = false;
                pnlFileUploads.Visible = false;

            }
        }

        private void BindStylesByBuyer(int buyerId)
        {

            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);

        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlStyles.SelectedValue != "")
            {
                var styleId = int.Parse(ddlStyles.SelectedValue);
                var isExist = unitOfWork.GenericRepositories<OrderConsumptionSheets>().IsExist(x => x.StyleId == styleId);

                if (!isExist)
                {
                    SetYarnInfoControl();
                    CommonMethods.LoadMachineBrandDropdown(ddlMachineBrands, 1, 0);
                    pnlYarnConsumption.Visible = true;
                    pnlFileUploads.Visible = true;
                    lblConsumptionSheetExist.Visible = false;
                }
                else
                {
                    lblConsumptionSheetExist.Visible = true;
                    pnlYarnConsumption.Visible = false;
                    pnlFileUploads.Visible = false;
                }
            }
            else
            {
                //ddlOrders.Items.Clear();
                pnlYarnConsumption.Visible = false;
                pnlFileUploads.Visible = false;

            }


        }

        //private void BindOrdersByStyle(int styleId)
        //{

        //    CommonMethods.LoadOrderDropdownByStyle(ddlOrders, styleId, 1, 0);

        //}

        //protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddlOrders.SelectedValue != "")
        //    {

        //        SetYarnInfoControl();
        //        CommonMethods.LoadMachineBrandDropdown(ddlMachineBrands, 1, 0);
        //        pnlYarnConsumption.Visible = true;
        //        pnlFileUploads.Visible = true;


        //    }
        //    else
        //    {
        //        pnlYarnConsumption.Visible = false;
        //        pnlFileUploads.Visible = false;

        //    }
        //}


        private void SetYarnInfoControl()
        {
            var dt = new DataTable();
            dt.Columns.Add("YarnId");
            dt.Columns.Add("YarnCount");
            dt.Columns.Add("PlyMainYarn");
            dt.Columns.Add("PlyLycra");
            dt.Columns.Add("Weight");
            DataRow drCurrentRow = dt.NewRow();

            drCurrentRow["YarnId"] = 0;
            drCurrentRow["YarnCount"] = "";
            drCurrentRow["PlyMainYarn"] = "";
            drCurrentRow["PlyLycra"] = "";
            drCurrentRow["Weight"] = "";
            dt.Rows.Add(drCurrentRow);

            //DataTable dt = new DataTable();
            //dt = orderManager.GetSweaterParts();
            rpt.DataSource = dt;
            rpt.DataBind();
            ViewState["CurrentTable"] = dt;
        }

        private void SetYarnInfoControlForUpdate(int conSheetid)
        {
            var dt = unitOfWork.GetDataTableFromSql($"SELECT CAST(YarnItemId as nvarchar(max)) as YarnId, YarnCount,PlyMainYarn,PlyLycra, CAST(AverageWeight as nvarchar(max)) as Weight FROM YarnConsumptionByParts WHERE ConsumptionSheetId = '{conSheetid}'");
            rpt.DataSource = dt;
            rpt.DataBind();
            ViewState["CurrentTable"] = dt;
        }







        protected void btnSaveConsumptionSheet_Click(object sender, EventArgs e)
        {

            try
            {

                if (ddlStyles.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a style.')", true);
                }
                if (ddlStage.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a stage.')", true);
                }
                else if (ddlMachineBrands.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a machine brand.')", true);

                }
                //else if (tbxKnittingMCGauge.Text == "")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter required machine gauge.')", true);
                //}
                //else if (tbxNeedlePerCM.Text == "")
                //{
                //    ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please enter needle per CM.')", true);
                //}
                //else if (tbxMCSpeed.Text == "")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter machine speed.')", true);
                //}
                else if (tbxKnittingTime.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter knitting time.');", true);
                }
                //else if (tbxDesignerName.Text == "")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter deisgner name.');", true);
                //}
                //else if (tbxDesignerPhone.Text == "")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter deisgner phone number.');", true);
                //}
                //else if (FileUpload1.HasFile != true)
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select consumption sheet file to upload.');", true);
                //}
                else
                {
                    var consumptionSheet = new OrderConsumptionSheets()
                    {
                        StyleId = Convert.ToInt32(ddlStyles.SelectedValue),
                        Stage = ddlStage.SelectedItem.Text,
                        KnittingMachineBrandId = Convert.ToInt32(ddlMachineBrands.SelectedValue),
                        KnittingMachineGauge = tbxKnittingMCGauge.Text,
                        NeedlePerCM = "0",
                        KnittingTime = tbxKnittingTime.Text,
                        MachineSpeed = tbxMCSpeed.Text,
                        DesignerContactNumber = tbxDesignerPhone.Text,
                        DesignerName = tbxDesignerName.Text,
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };
                    List<YarnConsumptionByParts> lstYarnConByParts = new List<YarnConsumptionByParts>();
                    for (int i = 0; i < this.rpt.Items.Count; i++)
                    {
                        var ddlYarnItemId = (DropDownList)rpt.Items[i].FindControl("ddlYarnItemId");
                        var tbxYarnCount = (TextBox)rpt.Items[i].FindControl("tbxYarnCount");
                        var tbxPlyMainYarn = (TextBox)rpt.Items[i].FindControl("tbxPlyMainYarn");
                        var tbxPlyLycra = (TextBox)rpt.Items[i].FindControl("tbxPlyLycra");
                        var tbxAverageWeight = (TextBox)rpt.Items[i].FindControl("tbxAverageWeight");
                        if(ddlYarnItemId.SelectedValue != "")
                        {
                            var byParts = new YarnConsumptionByParts()
                            {
                                ConsumptionSheetId = consumptionSheet.Id,
                                PlacementId = 9,
                                YarnComposition = "",
                                YarnCount = tbxYarnCount.Text,
                                PlyLycra = tbxPlyLycra.Text,
                                PlyMainYarn = tbxPlyMainYarn.Text,
                                AverageWeight = 0,
                                CreatedBy = CommonMethods.SessionInfo.UserName,
                                CreateDate = DateTime.Now,
                                YarnItemId = int.Parse(ddlYarnItemId.SelectedValue)

                            };
                            if (!string.IsNullOrEmpty(tbxAverageWeight.Text))
                            {
                                byParts.AverageWeight = decimal.Parse(tbxAverageWeight.Text);
                            }
                            lstYarnConByParts.Add(byParts);
                        }
                       
                    }

                    if(lstYarnConByParts.Count() > 0)
                    {
                        consumptionSheet.YarnConsumptionByParts = lstYarnConByParts;
                        unitOfWork.GenericRepositories<OrderConsumptionSheets>().Insert(consumptionSheet);
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                        Response.AddHeader("REFRESH", "2;URL=AddConsumptionSheet.aspx");
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter correct yarn composition information')", true);
                    }

                    //ConsumptionSheetId = orderManager.InsertConsumptionSheet(Convert.ToInt32(ddlStyles.SelectedValue), ddlStage.SelectedItem.Text, Convert.ToInt32(ddlMachineBrands.SelectedValue), tbxKnittingMCGauge.Text, "0"/*tbxNeedlePerCM.Text*/, tbxMCSpeed.Text, tbxKnittingTime.Text, tbxDesignerName.Text, tbxDesignerPhone.Text, CommonMethods.SessionInfo.UserName, DateTime.Now);

                    //if (ConsumptionSheetId > 0)
                    //{
                    //    int saveYarnConsumptionByPartResult = SaveYarnConsumptionByPart(ConsumptionSheetId);

                    //    if (saveYarnConsumptionByPartResult > 0)
                    //    {

                    //        //UploadFiles(ConsumptionSheetId);
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                    //    }
                    //    else
                    //    {
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Yarn consumption by parts could not be saved.');", true);
                    //    }
                    //}
                    //else
                    //{
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Consumption sheet could not be saved.');", true);
                    //}

                   

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "')", true);
            }


        }

        public int SaveYarnConsumptionByPart(int consumptionSheetId)
        {

            // DataTable dt = YarnByPart();
            double averageYarnWeight = 0.0;

            for (int i = 0; i < rpt.Items.Count; i++)
            {
                Label sweaterPartId = (Label)rpt.Items[i].FindControl("lblSweatherPartsId");
                //TextBox yarnComposition = (TextBox)rpt.Items[i].FindControl("tbxYarnComposition");
                DropDownList ddlYarnItemId = (DropDownList)rpt.Items[i].FindControl("ddlYarnItemId");
                TextBox yarnCount = (TextBox)rpt.Items[i].FindControl("tbxYarnCount");
                TextBox plyMainYarn = (TextBox)rpt.Items[i].FindControl("tbxPlyMainYarn");
                TextBox plyLycra = (TextBox)rpt.Items[i].FindControl("tbxPlyLycra");
                TextBox averageWeight = (TextBox)rpt.Items[i].FindControl("tbxAverageWeight");

                if (averageWeight.Text == "")
                {
                    averageYarnWeight = 0.0;
                }
                else
                {
                    averageYarnWeight = Convert.ToDouble(averageWeight.Text);
                }

                // dt.Rows.Add(consumptionSheetId, Convert.ToInt32(sweaterPartId.Text), yarnComposition.Text, yarnCount.Text, plyMainYarn.Text, plyLycra.Text, averageYarnWeight, CommonMethods.SessionInfo.UserName, DateTime.Now);
            }

            //return orderManager.SaveYarnConsumptionByParts(dt);
            return 0;

        }

        public DataTable YarnByPart()
        {
            DataTable dt = new DataTable("YarnByPart");
            dt.Columns.Add("ConsumptionSheetId", typeof(int));
            dt.Columns.Add("PlacementId", typeof(int));
            dt.Columns.Add("YarnComposition", typeof(string));
            dt.Columns.Add("YarnCount", typeof(string));
            dt.Columns.Add("PlyMainYarn", typeof(string));
            dt.Columns.Add("PlyLycra", typeof(string));
            dt.Columns.Add("AverageWeight", typeof(double));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            dt.Columns.Add("YarnItemId", typeof(DateTime));
            return dt;
        }


        //public void UploadFiles(int ConsumptionSheetId)
        //{
        //    foreach (HttpPostedFile postedFile in FileUpload1.PostedFiles)
        //    {
        //        string fileName = Path.GetFileName(postedFile.FileName);
        //        string contentType = postedFile.ContentType;
        //        int fileSize = postedFile.ContentLength;
        //        if (fileSize > 0)
        //        {
        //            using (Stream fs = postedFile.InputStream)
        //            {
        //                using (BinaryReader br = new BinaryReader(fs))
        //                {
        //                    byte[] bytes = br.ReadBytes((Int32)fs.Length);
        //                    orderManager.SaveConsumptionSheetFiles(ConsumptionSheetId, fileName, contentType, fileSize, bytes, CommonMethods.SessionInfo.UserName, DateTime.Now);
        //                }
        //            }
        //        }
        //    }
        //}

        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //var lblSweatherPartsId = (Label)e.Item.FindControl("lblSweatherPartsId");
                var ddlYarnItemId = (DropDownList)e.Item.FindControl("ddlYarnItemId");
                var tbxYarnCount = (TextBox)e.Item.FindControl("tbxYarnCount");
                var tbxPlyMainYarn = (TextBox)e.Item.FindControl("tbxPlyMainYarn");
                var tbxPlyLycra = (TextBox)e.Item.FindControl("tbxPlyLycra");
                var tbxAverageWeight = (TextBox)e.Item.FindControl("tbxAverageWeight");

                var yarnId = DataBinder.Eval(e.Item.DataItem, "YarnId").ToString();
                LoadYarnItemDropdown(ddlYarnItemId, yarnId);
                tbxYarnCount.Text = DataBinder.Eval(e.Item.DataItem, "YarnCount").ToString();
                tbxPlyMainYarn.Text = DataBinder.Eval(e.Item.DataItem, "PlyMainYarn").ToString();
                tbxPlyLycra.Text = DataBinder.Eval(e.Item.DataItem, "PlyLycra").ToString();
                tbxAverageWeight.Text = DataBinder.Eval(e.Item.DataItem, "Weight").ToString();
            }
        }


        protected void LoadYarnItemDropdown(DropDownList ddl, string selectedVlue)
        {
            var items = unitOfWork.GenericRepositories<YarnAccessoriesItems>().Get(x => x.ItemTypeId == 2 && x.IsActive == 1).ToList(); // ItemTypeId  2 = Yarn and 1 = Accessories
            ddl.DataSource = items;
            ddl.DataTextField = "ItemName";
            ddl.DataValueField = "Id";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedValue = selectedVlue;
        }

        protected void btnUpdateConsumptionSheet_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlStyles.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a style.');", true);
                }
                else if (ddlStage.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a stage.');", true);
                }
                else if (ddlMachineBrands.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a machine brand.');", true);

                }
                //else if (tbxKnittingMCGauge.Text == "")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter required machine gauge.');", true);
                //}
                //else if (tbxNeedlePerCM.Text == "")
                //{
                //    ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please enter needle per CM.');", true);
                //}
                //else if (tbxMCSpeed.Text == "")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter machine speed.');", true);
                //}
                else if (tbxKnittingTime.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter knitting time.');", true);
                }
                //else if (tbxDesignerName.Text == "")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter deisgner name.');", true);
                //}
                //else if (tbxDesignerPhone.Text == "")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter deisgner phone number.');", true);
                //}
                else
                {
                    var conInfo = unitOfWork.GenericRepositories<OrderConsumptionSheets>().GetByID(ConsumptionSheetId);
                    if (conInfo != null)
                    {
                        conInfo.StyleId = int.Parse(ddlStyles.SelectedValue);
                        conInfo.Stage = ddlStage.SelectedItem.Text;
                        conInfo.KnittingMachineBrandId = int.Parse(ddlMachineBrands.SelectedValue);
                        conInfo.KnittingMachineGauge = tbxKnittingMCGauge.Text;
                        //conInfo.NeedlePerCM = tbxNeedlePerCM.Text;
                        conInfo.MachineSpeed = tbxMCSpeed.Text;
                        conInfo.KnittingTime = tbxKnittingTime.Text;
                        conInfo.DesignerName = tbxDesignerName.Text;
                        conInfo.DesignerContactNumber = tbxDesignerPhone.Text;
                        conInfo.UpdateDate = DateTime.Now;
                        conInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;

                       
                        var lstOldConByPars = unitOfWork.GenericRepositories<YarnConsumptionByParts>().Get(x => x.ConsumptionSheetId == ConsumptionSheetId).ToList();
                        foreach (var item in lstOldConByPars)
                        {
                            unitOfWork.GenericRepositories<YarnConsumptionByParts>().Delete(item);
                        }

                        List<YarnConsumptionByParts> lstYarnConByParts = new List<YarnConsumptionByParts>();
                        for (int i = 0; i < this.rpt.Items.Count; i++)
                        {
                            var ddlYarnItemId = (DropDownList)rpt.Items[i].FindControl("ddlYarnItemId");
                            var tbxYarnCount = (TextBox)rpt.Items[i].FindControl("tbxYarnCount");
                            var tbxPlyMainYarn = (TextBox)rpt.Items[i].FindControl("tbxPlyMainYarn");
                            var tbxPlyLycra = (TextBox)rpt.Items[i].FindControl("tbxPlyLycra");
                            var tbxAverageWeight = (TextBox)rpt.Items[i].FindControl("tbxAverageWeight");
                            if (ddlYarnItemId.SelectedValue != "")
                            {
                                var byParts = new YarnConsumptionByParts()
                                {
                                    ConsumptionSheetId = conInfo.Id,
                                    PlacementId = 9,
                                    YarnComposition = "",
                                    YarnCount = tbxYarnCount.Text,
                                    PlyLycra = tbxPlyLycra.Text,
                                    PlyMainYarn = tbxPlyMainYarn.Text,
                                    AverageWeight = 0,
                                    CreatedBy = CommonMethods.SessionInfo.UserName,
                                    CreateDate = DateTime.Now,
                                    YarnItemId = int.Parse(ddlYarnItemId.SelectedValue)

                                };
                                if (!string.IsNullOrEmpty(tbxAverageWeight.Text))
                                {
                                    byParts.AverageWeight = decimal.Parse(tbxAverageWeight.Text);
                                }
                                lstYarnConByParts.Add(byParts);
                            }

                        }

                        if (lstYarnConByParts.Count() > 0)
                        {
                            conInfo.YarnConsumptionByParts = lstYarnConByParts;
                            unitOfWork.GenericRepositories<OrderConsumptionSheets>().Update(conInfo);
                            unitOfWork.Save();
                            CommonMethods.SendEventNotification(3, int.Parse(ddlBuyers.SelectedValue), conInfo.StyleId, null, conInfo.Id);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
                            ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewConsumptionSheets.aspx');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter correct yarn composition information')", true);
                        }


                        //for (int i = 0; i < rpt.Items.Count; i++)
                        //{
                        //    //var lblYarnConsumptionByPartsId = (Label)rpt.Items[i].FindControl("lblYarnConsumptionByPartsId");
                        //    //var yarnConByPartId = string.IsNullOrEmpty(lblYarnConsumptionByPartsId.Text) ? 0 : int.Parse(lblYarnConsumptionByPartsId.Text);
                        //    var yarnConByPartInfo = unitOfWork.GenericRepositories<YarnConsumptionByParts>().GetByID(yarnConByPartId);
                        //    if (yarnConByPartInfo != null)
                        //    {
                        //        var tbxYarnComposition = (TextBox)rpt.Items[i].FindControl("tbxYarnComposition");
                        //        var tbxYarnCount = (TextBox)rpt.Items[i].FindControl("tbxYarnCount");
                        //        var tbxPlyMainYarn = (TextBox)rpt.Items[i].FindControl("tbxPlyMainYarn");
                        //        var tbxPlyLycra = (TextBox)rpt.Items[i].FindControl("tbxPlyLycra");
                        //        var tbxAverageWeight = (TextBox)rpt.Items[i].FindControl("tbxAverageWeight");
                        //        yarnConByPartInfo.YarnComposition = tbxYarnComposition.Text;
                        //        yarnConByPartInfo.YarnCount = tbxYarnCount.Text;
                        //        yarnConByPartInfo.PlyMainYarn = tbxPlyMainYarn.Text;
                        //        yarnConByPartInfo.PlyLycra = tbxPlyLycra.Text;
                        //        yarnConByPartInfo.AverageWeight = string.IsNullOrEmpty(tbxAverageWeight.Text) ? decimal.Parse("0") : decimal.Parse(tbxAverageWeight.Text);

                        //        unitOfWork.GenericRepositories<YarnConsumptionByParts>().Update(yarnConByPartInfo);
                        //    }
                        //}

                        //unitOfWork.Save();
                        //UploadFiles(ConsumptionSheetId);
                        //CommonMethods.SendEventNotification(3, int.Parse(ddlBuyers.SelectedValue), conInfo.StyleId, null, conInfo.Id);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
                        //ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewConsumptionSheets.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Invalid Id!');", true);
                    }
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "');", true);
            }
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            this.AddNewRowToGrid();
        }


        private void AddNewRowToGrid()
        {
            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {
                        //var lblYarnConsumptionByPartsId = (Label)rpt.Items[rowIndex].FindControl("lblYarnConsumptionByPartsId");
                        var ddlYarnItemId = (DropDownList)rpt.Items[rowIndex].FindControl("ddlYarnItemId");
                        //var tbxYarnComposition = (TextBox)rpt.Items[rowIndex].FindControl("tbxYarnComposition");
                        var tbxYarnCount = (TextBox)rpt.Items[rowIndex].FindControl("tbxYarnCount");
                        var tbxPlyMainYarn = (TextBox)rpt.Items[rowIndex].FindControl("tbxPlyMainYarn");
                        var tbxPlyLycra = (TextBox)rpt.Items[rowIndex].FindControl("tbxPlyLycra");
                        var tbxAverageWeight = (TextBox)rpt.Items[rowIndex].FindControl("tbxAverageWeight");

                        dtCurrentTable.Rows[rowIndex]["YarnId"] = (ddlYarnItemId.SelectedValue == "" ? "0" : ddlYarnItemId.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["YarnCount"] = tbxYarnCount.Text;
                        dtCurrentTable.Rows[rowIndex]["PlyMainYarn"] = tbxPlyMainYarn.Text;
                        dtCurrentTable.Rows[rowIndex]["PlyLycra"] = tbxPlyLycra.Text;
                        dtCurrentTable.Rows[rowIndex]["Weight"] = tbxAverageWeight.Text;

                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["YarnId"] = 0;
                            drCurrentRow["YarnCount"] = "";
                            drCurrentRow["PlyMainYarn"] = "";
                            drCurrentRow["PlyLycra"] = "";
                            drCurrentRow["Weight"] = "";
                        }
                    }
                    dtCurrentTable.Rows.InsertAt(drCurrentRow, dtCurrentTable.Rows.Count);
                    this.ViewState["CurrentTable"] = dtCurrentTable;
                    this.rpt.DataSource = dtCurrentTable;
                    this.rpt.DataBind();
                }
            }
            this.SetPreviousData();
        }


        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        var ddlYarnItemId = (DropDownList)rpt.Items[i].FindControl("ddlYarnItemId");
                        //var tbxYarnComposition = (TextBox)rpt.Items[rowIndex].FindControl("tbxYarnComposition");
                        var tbxYarnCount = (TextBox)rpt.Items[rowIndex].FindControl("tbxYarnCount");
                        var tbxPlyMainYarn = (TextBox)rpt.Items[i].FindControl("tbxPlyMainYarn");
                        var tbxPlyLycra = (TextBox)rpt.Items[i].FindControl("tbxPlyLycra");
                        var tbxAverageWeight = (TextBox)rpt.Items[i].FindControl("tbxAverageWeight");

                        ddlYarnItemId.SelectedIndex = CommonMethods.MatchDropDownItem(ddlYarnItemId, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["YarnId"].ToString()) ? "0" : dt.Rows[i]["YarnId"].ToString())));
                        tbxYarnCount.Text = dt.Rows[i]["YarnCount"].ToString();
                        tbxPlyMainYarn.Text = dt.Rows[i]["PlyMainYarn"].ToString();
                        tbxPlyLycra.Text = dt.Rows[i]["PlyLycra"].ToString();
                        tbxAverageWeight.Text = dt.Rows[i]["Weight"].ToString();

                        rowIndex++;
                    }
                }
            }
        }
    }


}
