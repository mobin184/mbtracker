﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewOrders.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.ViewOrders" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

     <div class="row-fluid">
        <div class="col-md-12" style="padding-right: 0px; text-align:right">
        
        
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label" > </label>
                                
                            <div class="controls controls-row">
                                <asp:Label runat="server" ID="lblDisplayOrders" Text="List Orders By:"></asp:Label>
                                <asp:DropDownList ID="ddlOrderDisplayOptions" runat="server" AutoPostBack="true" Width="195" OnSelectedIndexChanged="ddlOrderDisplayOptions_SelectedIndexChanged">
                                    <asp:ListItem Text="--Select--" Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Shipments in 60 days" Value="Shipmentsin60days"></asp:ListItem>
                                    <asp:ListItem Text="Buyer" Value="Buyer"></asp:ListItem>
                                    <asp:ListItem Text="Style" Value="Style"></asp:ListItem>
                                      <%--<asp:ListItem Text="Risky Orders" Value="RiskyOrders"></asp:ListItem>--%>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <asp:Panel ID="pnlOrderSearch" runat="server" Visible="false">

                            <div class="control-group">
                                <label for="inputBuyerDepartment" class="control-label">
                                   </label>
                                <div class="controls controls-row">
                                     <asp:Label runat="server" ID="lblDisplayInput" Text=""></asp:Label>
                                    <asp:TextBox ID="tbxStyle" runat="server" Width="195" Visible="false"></asp:TextBox>
                                    <asp:DropDownList ID="ddlBuyers" runat="server" Width="195" Visible="false"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputBuyerDepartment" class="control-label">
                                    <asp:Label runat="server" ID="Label9" Text=""></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:LinkButton ID="btnViewOrders2" runat="server" class="btn btn-warning2 btn-mini" Text="View Orders" OnClick="btnViewOrders2_Click"></asp:LinkButton>
                                </div>
                            </div>

                        </asp:Panel>

                    </div>

                </div>
        
        </div>
    </div>



    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="LabelWidgetHeader" Text="List of orders:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <asp:Repeater ID="rptOrders" runat="server" OnItemDataBound="rptOrders_ItemDataBound">
                            <HeaderTemplate>
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="Label1" runat="server" Text="Buyer<br/>Name"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblCol2" runat="server" Text="Buyer<br/>Style"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblCol3" runat="server" Text="Order<br/>Number"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblCol4" runat="server" Text="Order<br/>Date"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblCol5" runat="server" Text="Order<br/>Quantity"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblCol6" runat="server" Text="Earliest <br/>Shipment Date"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblCol10" runat="server" Text="Shipment<br/> Quantity"></asp:Label></th>
                                            <th class="hidden">
                                                <asp:Label ID="lblCol7" runat="server" Text="LC<br/> Value"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblCol8" runat="server" Text="Order<br/>Status"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label3" runat="server" Text="Approved<br/>Status"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblCol9" runat="server" Text="Take<br/>Actions"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td width="10%"><%#Eval("BuyerName") %> </td>
                                    <td width="10%"><%#Eval("StyleName") %></td>
                                    <td width="10%"><%#Eval("OrderNumber") %></td>
                                    <td width="8%"><%#String.Format("{0:dd-MMM-yyyy}",Eval("OrderDate")) %></td>
                                    <td width="8%"><%#Eval("OrderQuantity") %> </td>
                                     <td width="8%"><%# String.Format("{0:dd-MMM-yyyy}",Eval("ShipmentDate")) == DateTime.Now.ToString("dd-MMM-yyyy") ? "N/A" : String.Format("{0:dd-MMM-yyyy}",Eval("ShipmentDate")) %> </td>
                                    <td width="8%"><%#Eval("ShipmentQuantity") %> </td>
                                    <td class="hidden" width="8%"><%#Eval("LCValue") %> </td>
                                    <td width="14%">
                                        <asp:Label ID="lblOrderStatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderStatusName")%>'></asp:Label>
                                    </td>
                                    <td width="14%">
                                        <asp:Label ID="lblStatusName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "StatusName")%>'></asp:Label>
                                    </td>
                                    <td width="14%">
                                        <asp:LinkButton ID="lnkbtnEdit" runat="server" Visible='<%# ViewState["editEnabled"]%>' CommandName="Edit" CommandArgument='<%#Eval("Id") %>' OnCommand="lnkbtnEdit_Command" class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnDelete" Visible='<%# ViewState["deleteEnabled"]%>' runat="server" CommandName="Delete" CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-small hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                        <%--<asp:LinkButton ID="lnkbtnView" runat="server" CommandName="View" CommandArgument='<%#Eval("Id")+","+Eval("BuyerName")+","+Eval("StyleName")+","+Eval("OrderNumber") %>' OnCommand="lnkbtnView_Command" class="btn btn-success btn-small hidden-phone" Text="View Details"></asp:LinkButton>--%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                         </table>
                            <div class="clearfix">
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:Label ID="lblNoOrderFound" runat="server" Visible="false" Text="No order found." BackColor="#ffff00"></asp:Label>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div>
        </div>
    </div>


    <div class="row-fluid" runat="server" visible="false" id="divBasicInfo">
        <div class="span12" style="">
            <div class="widget">
                <div class="widget-body">
                
                    <label for="inputOrder" class="control-label" style="line-height:18px;text-align:center; padding-bottom:10px;">
                                <asp:Label ID="lblOrderInfo" runat="server"   Visible="false" Text="Order's Basic Information" Font-Bold="true"></asp:Label>
                    </label>
                    <div class="row" style="padding-top:10px">
                        <div class="col-md-6">
                            <div class="form-horizontal">

                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label10" runat="server" Text="Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row">
                                      <asp:Label ID="lblBuyer" CssClass="form-control" Text="" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyerDepartment" class="control-label">
                                        <asp:Label ID="Label11" runat="server" Text="Buyer's Department:"></asp:Label></label>
                                    <div class="controls controls-row">
                                       <asp:Label ID="lblDepartment" CssClass="form-control" Text="" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label">
                                        <asp:Label ID="Label12" runat="server" Text="Style:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row">
                                        <asp:Label ID="lblStyle" Text="" CssClass="form-control" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label13" runat="server" Text="Order Season:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:Label ID="lblSeason" Text="" CssClass="form-control" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderSeason" class="control-label">
                                        <asp:Label ID="Label14" runat="server" Text="Order Year:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:Label ID="lblYear" Text="" CssClass="form-control" runat="server"></asp:Label>
                                    </div>
                                </div>
                                

                            </div>
                        </div>
                     <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputOrderNumber" class="control-label">
                                        <asp:Label ID="Label1" runat="server" Text="Order Number:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                    </label>
                                    <div class="controls controls-row">
                                       <asp:Label ID="lblOrderNumber" Text="" CssClass="form-control" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderDate" class="control-label">
                                        <asp:Label ID="Label15" runat="server" Text="Order Date:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                    </label>
                                    <div class="controls controls-row">
                                       <asp:Label ID="lblOrderDate" Text="" CssClass="form-control" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderItem" class="control-label">
                                        <asp:Label ID="Label17" runat="server" Text="Order Item:"></asp:Label>
                                    </label>
                                    <div class="controls controls-row">
                                        <asp:Label ID="lblOrderItem" Text="" CssClass="form-control" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="control-group hidden">
                                    <label for="inputPaymentTerm" class="control-label">
                                        <asp:Label ID="Label18" runat="server" Text="Payment Term:"></asp:Label></label>
                                    <div class="controls controls-row"> 
                                       <asp:Label ID="lblPaymentTerm" Text="" CssClass="form-control" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div  class="control-group hidden">
                                    <label for="inputLCValue" class="control-label">
                                        <asp:Label ID="lblLCValue" runat="server" Text="LC Value:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row">
                                        <asp:Label ID="lblLcVlaue" Text="" CssClass="form-control" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputOrderStatus" class="control-label">
                                        <asp:Label ID="Label19" runat="server" Text="Order Status:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row">
                                        <asp:Label ID="lblOrderStatus" Text="" CssClass="form-control" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                     


                   <%-- <div id="dt_example" class="example_alt_pagination">
                        <div class="control-group">
                            <label for="inputOrder" class="control-label" style="line-height:18px">
                                <asp:Label ID="lblOrderInfo" runat="server"   Visible="false" Text="Order's Basic Information" Font-Bold="true"></asp:Label></label>
                            <div class="controls controls-row">
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="lbl1" runat="server" Text="Buyer <br/>Name"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lbl2" runat="server" Text="Buyer's<br/> Department"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lbl3" runat="server" Text="Style <br/>Name"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lbl4" runat="server" Text="Order <br/>Season"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lbl66" runat="server" Text="Order <br/>Year"></asp:Label></th>
                                             <th>
                                                <asp:Label ID="Label2" runat="server" Text="Order <br/>Number"></asp:Label></th>
                                             <th>
                                                <asp:Label ID="Label3" runat="server" Text="Order <br/>Date"></asp:Label></th>
                                             <th>
                                                <asp:Label ID="Label4" runat="server" Text="Order <br/>Item"></asp:Label></th>
                                             <th>
                                                <asp:Label ID="Label5" runat="server" Text="Payment <br/>Term"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label6" runat="server" Text="LC <br/>Value"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label7" runat="server" Text="Order <br/>Status"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label8" runat="server" Text="Delivery <br/>Country"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr>
                                            <td><asp:Label ID="lblBuyer" Text="" runat="server"></asp:Label></td>
                                            <td><asp:Label ID="lblDepartment" Text="" runat="server"></asp:Label></td>
                                            <td><asp:Label ID="lblStyle" Text="" runat="server"></asp:Label></td>
                                            <td><asp:Label ID="lblSeason" Text="" runat="server"></asp:Label></td>
                                            <td><asp:Label ID="lblYear" Text="" runat="server"></asp:Label></td>
                                            <td><asp:Label ID="lblOrderNumber" Text="" runat="server"></asp:Label></td>
                                            <td><asp:Label ID="lblOrderDate" Text="" runat="server"></asp:Label></td>
                                            <td><asp:Label ID="lblOrderItem" Text="" runat="server"></asp:Label></td>
                                            <td><asp:Label ID="lblPaymentTerm" Text="" runat="server"></asp:Label></td>
                                            <td><asp:Label ID="lblLcVlaue" Text="" runat="server"></asp:Label></td>
                                            <td><asp:Label ID="lblOrderStatus" Text="" runat="server"></asp:Label></td>
                                            <td><asp:Label ID="lblDeleveryCountry" Text="" runat="server"></asp:Label></td>
                                            
                                        </tr>

                                    </tbody>
                                </table>
                                <asp:Label runat="server" Visible="false" ID="lblBasicInfoNotFound" Text="No data found!"></asp:Label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>--%>
                </div>
            </div>
            </div>
        </div>
    
    
    <div class="row-fluid" runat="server" visible="false" id="pnlDetails">       
        <div class="span8" style="">
            <div class="widget">
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <div class="control-group">
                            <label for="inputOrder" class="control-label" style="line-height:18px">
                                <asp:Label ID="Label2" CssClass="" runat="server"  Visible="true" Text="Color & Size Information" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblColorInfoDetails" CssClass="hidden" runat="server"  Visible="false" Text="Order Color Information" Font-Bold="true"></asp:Label></label>
                            <div class="controls controls-row" style="overflow-x:auto">
                                <asp:Repeater ID="rptColorInfo" runat="server" OnItemDataBound="rptColorInfo_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>                                                   
                                                    <th>
                                                        <asp:Label ID="lbl2" runat="server" Text="Color"></asp:Label></th>
                                                    <th class="hidden">
                                                        <asp:Label ID="lbl3" runat="server" Text="Unit Cost"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lbl4" runat="server" Text="Order Quantity"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lbl6" runat="server" Text="Size & Quantity"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <%#Eval("ColorDescription") %>
                                                 <asp:Label ID="lblOrderId" Text='<%#Eval("OrderId") %>' runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblColorId" Text='<%#Eval("Id") %>' runat="server" Visible="false"></asp:Label>       
                                            </td>
                                            <td class="hidden">
                                                 <%#Eval("UnitCost") %>
                                            </td>
                                            <td>
                                                  <%#Eval("OrderQuantity") %>
                                            </td>
                                            <td>                                                
                                                 <asp:GridView ID="gvSizeInfo" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeInfo_RowCreated">
                                                </asp:GridView>
                                            </td>                                            
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                 </table>
                                    <div class="clearfix"></div>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Label ID="lblNoDataFoundForColor" runat="server" Text="No data found." Visible="false" BackColor="#ffff00"></asp:Label>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="span4" style="">
            <div class="widget" style="border:none">
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <div class="control-group">
                            <label for="inputOrder" class="control-label">
                                <asp:Label ID="lblDeliveryCountry" runat="server" Visible="false" Text="Delivery Country Information" Font-Bold="true"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:Repeater ID="rptDeliveryCountries" runat="server">
                                    <HeaderTemplate>
                                        <table id="data-table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lbl1" runat="server" Text="Delivery Countries"></asp:Label></th>                                                                                                       
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                             <td style="text-align:center"><asp:Label ID="lblCountryName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryCountryName")%>' Width="300px"></asp:Label></td>               
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                 </table>
                                    <div class="clearfix">
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Label ID="lblNoCountryFound" runat="server" Text="No country found!" Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="widget" style="border:none">
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <div class="control-group">
                            <label for="inputOrder" class="control-label">
                                <asp:Label ID="lblOrderDocuments" runat="server" Visible="false" Text="Order Documents Information" Font-Bold="true"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:Repeater ID="rptOrderDocument" runat="server">
                                    <HeaderTemplate>
                                        <table id="data-table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lbl1" runat="server" Text="File Name"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lbl2" runat="server" Text="Action"></asp:Label></th>                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                             <td style="text-align:center"><asp:Label ID="lblFileName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FileName")%>' Width="300px"></asp:Label></td>
                                             <td style="text-align:center"><asp:HyperLink runat="server" NavigateUrl='<%# Eval("Id", "GetFile.aspx?ID={0}&Type=OrderDocument") %>' Text="Download"></asp:HyperLink></td>                                           
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                 </table>
                                    <div class="clearfix">
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Label ID="lblNoDocumnet" runat="server" Text="No document found!" Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

    </div>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
    <script src="../../js/jquery.dataTables.js"></script>
</asp:Content>
