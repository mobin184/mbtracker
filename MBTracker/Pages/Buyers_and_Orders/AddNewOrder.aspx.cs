﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class AddNewOrder : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        CommonManager commonManager = new CommonManager();

        DataTable dtSizes;
        DataTable dtColors;
        int selectedBuyerValue;
        UnitOfWork unitOfWork = new UnitOfWork();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                LoadOrderStatusDropDown();

                if (Request.QueryString["orderId"] != null)
                {
                    OrderId = int.Parse(Tools.UrlDecode(Request.QueryString["orderId"]));
                    var notEditAble = unitOfWork.GenericRepositories<viewOrderCantEditDelete>().IsExist(x => x.OrderId == OrderId);
                    if (!notEditAble)
                    {
                        PopulateOrder(OrderId);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('You cannot update this order, it has related entity!')", true);
                        //Response.AddHeader("REFRESH", "2;URL=ViewOrders.aspx");
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewOrders.aspx');", true);
                    }
                }
            }
        }


        int OrderId
        {
            set { ViewState["orderId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["orderId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }


        private void PopulateOrder(int orderId)
        {
            var orderInfo = unitOfWork.GenericRepositories<Orders>().GetByID(orderId);
            CommonMethods.LoadDropdownById(ddlBuyerDeprtments, orderInfo.BuyerId, "BuyerDepartments", 1, 0);
            CommonMethods.LoadDropdownById(ddlStyle, orderInfo.BuyerId, "BuyerStyles", 1, 0);
            CommonMethods.LoadDropdownById(ddlSeason, orderInfo.BuyerId, "BuyerSeasons", 1, 0);
            CommonMethods.LoadDropdown(ddlOrderYear, "Years WHERE IsActive = 1", 1, 0);
            CommonMethods.LoadDeliveryCountriesCheckBoxList(cblDeliveryCountries, orderInfo.BuyerId, lblNoCountryFound, btnSelectAllCountries, btnUnselectAllCountries, btnSaveOrder);

            BindDataToUI(orderInfo);
        }

        private void BindDataToUI(Orders orderInfo)
        {
            ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, orderInfo.BuyerId);
            ddlBuyerDeprtments.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyerDeprtments, orderInfo.BuyerDepartmentId ?? 0);
            ddlStyle.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyle, orderInfo.StyleId);

            ddlBuyers.Enabled = false;
            ddlStyle.Enabled = false;


            ddlSeason.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSeason, orderInfo.OrderSeasonId ?? 0);
            ddlOrderYear.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrderYear, orderInfo.OrderYearId ?? 0);
            ddlOrderStatus.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrderStatus, orderInfo.StatusId);

            tbxOrderNumber.Text = orderInfo.OrderNumber + "";
            tbxOrderDate.Text = orderInfo.OrderDate.ToString("yyyy-MM-dd");
            tbxOrderItem.Text = orderInfo.OrderItem;
            //tbxPaymentTerm.Text = orderInfo.PaymentTerms;
            //tbxLCValue.Text = orderInfo.LCValue?.ToString();

            var selectedCountris = unitOfWork.GenericRepositories<OrderDeliveryCountries>().Get(x => x.OrderId == orderInfo.Id).ToList(); ;
            foreach (ListItem item in cblDeliveryCountries.Items)
            {
                item.Selected = selectedCountris.Exists(x => x.DeliveryCountryId == Int32.Parse(item.Value));
            }
            dtSizes = buyerManager.GetSizes(orderInfo.BuyerId);
            selectedBuyerValue = orderInfo.BuyerId;
            //SetInitialRowCount();

            lblDeliveryCountry.Visible = true;
            lblColorAndSize.Visible = true;
            pnlColorAndSize.Visible = true;
            pnlFileUploads.Visible = true;

            var orderColor = unitOfWork.GenericRepositories<OrderColors>().Get(x => x.OrderId == orderInfo.Id);
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Column0", typeof(string)));
            dt.Columns.Add(new DataColumn("Column1", typeof(string)));
            dt.Columns.Add(new DataColumn("Column2", typeof(string)));
            dt.Columns.Add(new DataColumn("Column3", typeof(string)));
            dt.Columns.Add(new DataColumn("ColorId", typeof(string)));
            dt.Columns.Add(new DataColumn("OrderId", typeof(string)));
            int counter = 1;
            foreach (var item in orderColor)
            {
                DataRow dr = dt.NewRow();
                dr["RowNumber"] = counter;
                dr["Column0"] = item.BuyerColorId.ToString();
                dr["Column1"] = string.Empty;
                dr["Column2"] = item.UnitCost.ToString();
                dr["Column3"] = item.OrderQuantity.ToString();
                dr["ColorId"] = item.Id;
                dr["OrderId"] = item.OrderId;
                dt.Rows.Add(dr);
                counter++;
            }

            ViewState["CurrentTable"] = dt;

            rpt.DataSource = dt;
            rpt.DataBind();

            userActionTitle.Text = "Update Order";
            btnSaveOrder.Visible = false;
            btnUpdateOrder.Visible = true;
        }

        private void LoadOrderStatusDropDown()
        {
            DataTable dt = commonManager.LoadOrderStatusDropdown(CommonMethods.SessionInfo.DepartmentId);
            ddlOrderStatus.DataSource = dt;
            ddlOrderStatus.DataBind();

            ddlOrderStatus.Items.Add(new ListItem("", ""));
            ddlOrderStatus.DataSource = dt;
            ddlOrderStatus.DataTextField = dt.Columns[1].ToString();
            ddlOrderStatus.DataValueField = dt.Columns[0].ToString();
            ddlOrderStatus.DataBind();
            ddlOrderStatus.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlOrderStatus.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrderStatus, 1);

        }



        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Column0", typeof(string)));
            dt.Columns.Add(new DataColumn("Column1", typeof(string)));
            dt.Columns.Add(new DataColumn("Column2", typeof(string)));
            dt.Columns.Add(new DataColumn("Column3", typeof(string)));

            dr = dt.NewRow();
            dr["RowNumber"] = 1;
            dr["Column0"] = string.Empty;
            dr["Column1"] = string.Empty;
            dr["Column2"] = string.Empty;
            dr["Column3"] = string.Empty;
            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rpt.DataSource = dt;
            rpt.DataBind();
        }


        protected void LoadSizeInfo()
        {
            dtSizes = buyerManager.GetSizes(selectedBuyerValue);
        }

        protected void LoadColorInfo()
        {
            dtColors = buyerManager.GetColors(selectedBuyerValue);
        }

        protected void btnSelectAllCountries_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in cblDeliveryCountries.Items)
            {
                li.Selected = true;
            }
        }

        protected void btnUnselectAllCountries_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in cblDeliveryCountries.Items)
            {
                li.Selected = false;
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBuyers.SelectedValue == "")
            {
                DataTable dt = new DataTable();
                ddlBuyerDeprtments.DataSource = dt;
                ddlBuyerDeprtments.DataBind();

                ddlStyle.DataSource = dt;
                ddlStyle.DataBind();

                lblNoCountryFound.Visible = false;

                lblDeliveryCountry.Visible = false;
                cblDeliveryCountries.Visible = false;
                btnSelectAllCountries.Visible = false;
                btnUnselectAllCountries.Visible = false;

                lblColorAndSize.Visible = false;
                pnlColorAndSize.Visible = false;
                pnlFileUploads.Visible = false;

            }
            else
            {
                selectedBuyerValue = Convert.ToInt32(ddlBuyers.SelectedValue);
                CommonMethods.LoadDropdownById(ddlBuyerDeprtments, selectedBuyerValue, "BuyerDepartments", 1, 0);
                CommonMethods.LoadDropdownById(ddlStyle, selectedBuyerValue, "BuyerStyles", 1, 0);
                CommonMethods.LoadDropdownById(ddlSeason, selectedBuyerValue, "BuyerSeasons", 1, 0);

                CommonMethods.LoadDropdown(ddlOrderYear, "Years WHERE IsActive = 1", 1, 0);


                CommonMethods.LoadDeliveryCountriesCheckBoxList(cblDeliveryCountries, selectedBuyerValue, lblNoCountryFound, btnSelectAllCountries, btnUnselectAllCountries, btnSaveOrder);



                LoadColorInfo();
                LoadSizeInfo();
                SetInitialRowCount();


                lblDeliveryCountry.Visible = true;

                lblColorAndSize.Visible = true;
                pnlColorAndSize.Visible = true;
                pnlFileUploads.Visible = true;


            }

        }

        //int OrderColorId { get; set; }

        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //Repeater rptSizes = (Repeater)e.Item.FindControl("rptSizeAndQty");
            GridView gvSizeQuantity1 = (GridView)e.Item.FindControl("gvSizeQuantity1");
            Label noSizeFound = (Label)e.Item.FindControl("lblNoSizeFound");
            Repeater sourceRptr = (Repeater)sender;
            var dt = (DataTable)sourceRptr.DataSource;
            var index = e.Item.ItemIndex;

            if (index != -1 && sourceRptr != null && sourceRptr.DataSource != null && dt.Columns.Contains("ColorId"))
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    var ddlColor = (DropDownList)e.Item.FindControl("ddlColors");
                    LoadColorDropdown(ddlColor);
                    ddlColor.SelectedIndex = CommonMethods.MatchDropDownItem(ddlColor, int.Parse(string.IsNullOrEmpty(dt.Rows[index].Field<string>("Column0")) ? "0" : dt.Rows[index].Field<string>("Column0")));
                    //((TextBox)e.Item.FindControl("tbxColorCode")).Text = dt.Rows[index].Field<string>("Column0");
                    //((TextBox)e.Item.FindControl("tbxColorDescription")).Text = dt.Rows[index].Field<string>("Column1");

                    //((TextBox)e.Item.FindControl("tbxUnitCost")).Text = dt.Rows[index].Field<string>("Column2");
                    ((TextBox)e.Item.FindControl("tbxOrderQtyByColor")).Text = dt.Rows[index].Field<string>("Column3");

                    ((Label)e.Item.FindControl("lblOrderColorId")).Text = string.IsNullOrEmpty(dt.Rows[index].Field<string>("ColorId")) ? "" : dt.Rows[index].Field<string>("ColorId");
                    ((Label)e.Item.FindControl("lblBuyerId")).Text = selectedBuyerValue + "";

                    if (dtSizes.Rows.Count > 0)
                    {
                        DataTable dtOneRow = new DataTable();
                        DataRow dr = null;
                        dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                        dr = dtOneRow.NewRow();
                        dr["RowNumber"] = 1;
                        dtOneRow.Rows.Add(dr);

                        TemplateField templateField = null;

                        for (int i = 0; i < dtSizes.Rows.Count; i++)
                        {
                            templateField = new TemplateField();
                            templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                            gvSizeQuantity1.Columns.Add(templateField);
                        }

                        gvSizeQuantity1.DataSource = dtOneRow;
                        gvSizeQuantity1.DataBind();
                        gvSizeQuantity1.Visible = true;
                        noSizeFound.Visible = false;
                        if (OrderId == 0)
                        {
                            btnSaveOrder.Visible = true;
                        }

                    }
                    else
                    {
                        gvSizeQuantity1.Visible = false;
                        noSizeFound.Visible = true;
                        btnSaveOrder.Visible = false;
                    }
                }
            }
            else
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    var ddlColor = (DropDownList)e.Item.FindControl("ddlColors");
                    LoadColorDropdown(ddlColor);
                    ((Label)e.Item.FindControl("lblOrderColorId")).Text = "";
                    ((Label)e.Item.FindControl("lblBuyerId")).Text = selectedBuyerValue + "";
                    if (dtSizes.Rows.Count > 0)
                    {
                        DataTable dtOneRow = new DataTable();
                        DataRow dr = null;
                        dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                        dr = dtOneRow.NewRow();
                        dr["RowNumber"] = 1;
                        dtOneRow.Rows.Add(dr);

                        TemplateField templateField = null;

                        for (int i = 0; i < dtSizes.Rows.Count; i++)
                        {
                            templateField = new TemplateField();
                            templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                            gvSizeQuantity1.Columns.Add(templateField);
                        }

                        gvSizeQuantity1.DataSource = dtOneRow;
                        gvSizeQuantity1.DataBind();
                        gvSizeQuantity1.Visible = true;
                        noSizeFound.Visible = false;
                        if (OrderId == 0)
                        {
                            btnSaveOrder.Visible = true;
                        }
                    }
                    else
                    {
                        gvSizeQuantity1.Visible = false;
                        noSizeFound.Visible = true;
                        btnSaveOrder.Visible = false;
                    }

                }
            }

        }

        protected void LoadColorDropdown(DropDownList ddl)
        {
            if (dtColors == null || dtColors.Rows.Count == 0)
            {
                LoadColorInfo();
            }
            ddl.DataSource = dtColors;
            ddl.DataTextField = "ColorDescription";
            ddl.DataValueField = "Id";
            ddl.SelectedIndex = 0;
            ddl.DataBind();
        }


        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
        }

        private void AddNewRowToGrid()
        {

            int rowIndex;
            List<DataTable> sizeAndQtyTableList = new List<DataTable>();
            DataTable sizeAndQtyTable;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {
                        DropDownList ddlColor = (DropDownList)rpt.Items[rowIndex].FindControl("ddlColors");
                        //TextBox box1 = (TextBox)rpt.Items[rowIndex].FindControl("tbxColorCode");
                        //TextBox box2 = (TextBox)rpt.Items[rowIndex].FindControl("tbxColorDescription");
                        //TextBox box3 = (TextBox)rpt.Items[rowIndex].FindControl("tbxUnitCost");
                        TextBox box4 = (TextBox)rpt.Items[rowIndex].FindControl("tbxOrderQtyByColor");
                        //Repeater sizeAndQtyRpt = (Repeater)rpt.Items[rowIndex].FindControl("rptSizeAndQty");
                        GridView gvSizeQuantity1 = (GridView)rpt.Items[rowIndex].FindControl("gvSizeQuantity1");



                        dtCurrentTable.Rows[rowIndex]["Column0"] = ddlColor.SelectedValue;
                        //dtCurrentTable.Rows[rowIndex]["Column1"] = box2.Text;
                        //dtCurrentTable.Rows[rowIndex]["Column2"] = box3.Text;
                        dtCurrentTable.Rows[rowIndex]["Column3"] = box4.Text;



                        sizeAndQtyTable = new DataTable();
                        sizeAndQtyTable.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                        sizeAndQtyTable.Columns.Add(new DataColumn("Column0", typeof(string)));


                        for (int j = 0; j < gvSizeQuantity1.Columns.Count; j++)
                        {

                            DataRow sizeAndQtyRow = null;
                            sizeAndQtyRow = sizeAndQtyTable.NewRow();
                            sizeAndQtyRow["RowNumber"] = j + 1;
                            sizeAndQtyRow["Column0"] = string.Empty;
                            sizeAndQtyTable.Rows.Add(sizeAndQtyRow);


                            //TextBox box5 = (TextBox)sizeAndQtyRpt.Items[j].FindControl("tbxOrderQuantity");
                            //sizeAndQtyTable.Rows[j]["Column0"] = box5.Text;
                            var controlId = "tbxOrderQuantity" + j.ToString();
                            TextBox tbxSizeQty = (TextBox)gvSizeQuantity1.Rows[0].Cells[j].FindControl(controlId);
                            sizeAndQtyTable.Rows[j]["Column0"] = tbxSizeQty.Text;
                        }

                        sizeAndQtyTableList.Add(sizeAndQtyTable);


                        if ((rowIndex + 1) >= dtCurrentTable.Rows.Count)
                        {

                            sizeAndQtyTable = new DataTable();
                            sizeAndQtyTable.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                            sizeAndQtyTable.Columns.Add(new DataColumn("Column0", typeof(string)));
                            for (int j = 0; j < gvSizeQuantity1.Columns.Count; j++)
                            {

                                DataRow sizeAndQtyRow = null;
                                sizeAndQtyRow = sizeAndQtyTable.NewRow();
                                sizeAndQtyRow["RowNumber"] = j + 1;
                                sizeAndQtyRow["Column0"] = string.Empty;
                                sizeAndQtyTable.Rows.Add(sizeAndQtyRow);

                            }

                            sizeAndQtyTableList.Add(sizeAndQtyTable);

                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["RowNumber"] = rowIndex + 2;
                        }

                    }


                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;
                    ViewState["SizeAndQtyTableList"] = sizeAndQtyTableList;


                    selectedBuyerValue = Convert.ToInt32(ddlBuyers.SelectedValue);
                    LoadSizeInfo();

                    rpt.DataSource = dtCurrentTable;
                    rpt.DataBind();
                }
                else
                {
                    DataTable dt = new DataTable();
                    DataRow dr = null;
                    dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column0", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column1", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column2", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column3", typeof(string)));

                    dr = dt.NewRow();
                    dr["RowNumber"] = 1;
                    dr["Column0"] = string.Empty;
                    dr["Column1"] = string.Empty;
                    dr["Column2"] = string.Empty;
                    dr["Column3"] = string.Empty;
                    dt.Rows.Add(dr);

                    ViewState["CurrentTable"] = dt;

                    selectedBuyerValue = Convert.ToInt32(ddlBuyers.SelectedValue);
                    LoadSizeInfo();

                    rpt.DataSource = dt;
                    rpt.DataBind();

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData();

        }


        private void SetPreviousData()
        {

            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null && ViewState["SizeAndQtyTableList"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                List<DataTable> sizeAndQtyTableList = (List<DataTable>)ViewState["SizeAndQtyTableList"];

                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DropDownList ddlColor = (DropDownList)rpt.Items[rowIndex].FindControl("ddlColors");
                        //TextBox box1 = (TextBox)rpt.Items[rowIndex].FindControl("tbxColorCode");
                        //TextBox box2 = (TextBox)rpt.Items[rowIndex].FindControl("tbxColorDescription");
                        //TextBox box3 = (TextBox)rpt.Items[rowIndex].FindControl("tbxUnitCost");
                        TextBox box4 = (TextBox)rpt.Items[rowIndex].FindControl("tbxOrderQtyByColor");
                        //Repeater sizeAndQtyRpt = (Repeater)rpt.Items[rowIndex].FindControl("rptSizeAndQty");
                        GridView gvSizeQuantity1 = (GridView)rpt.Items[rowIndex].FindControl("gvSizeQuantity1");

                        ddlColor.SelectedIndex = CommonMethods.MatchDropDownItem(ddlColor, int.Parse(string.IsNullOrEmpty(dt.Rows[i]["Column0"].ToString()) ? "0" : dt.Rows[i]["Column0"].ToString()));
                        //box1.Text = dt.Rows[i]["Column0"].ToString();
                        //box2.Text = dt.Rows[i]["Column1"].ToString();
                        // box3.Text = dt.Rows[i]["Column2"].ToString();
                        box4.Text = dt.Rows[i]["Column3"].ToString();



                        if (i < sizeAndQtyTableList.Count)
                        {
                            DataTable tableFromList = (DataTable)sizeAndQtyTableList[i];

                            for (int j = 0; j < tableFromList.Rows.Count; j++)
                            {
                                //TextBox box5 = (TextBox)sizeAndQtyRpt.Items[j].FindControl("tbxOrderQuantity");
                                TextBox tbxSizeQty = (TextBox)gvSizeQuantity1.Rows[0].Cells[j].FindControl("tbxOrderQuantity" + j);
                                tbxSizeQty.Text = tableFromList.Rows[j]["Column0"].ToString();
                            }
                        }

                        rowIndex++;
                    }
                }
            }


        }


        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {

            int rowNumber = Convert.ToInt32(e.CommandArgument.ToString());

            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
            List<DataTable> sizeAndQtyTableList = (List<DataTable>)ViewState["SizeAndQtyTableList"];


            //ViewState["CurrentTable"] = dtCurrentTable;
            DataRow drDeleteRow = dtCurrentTable.Rows[rowNumber - 1];



            drDeleteRow.Delete();

            if (sizeAndQtyTableList != null)
            {
                sizeAndQtyTableList.RemoveAt(rowNumber - 1);
            }


            for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
            {
                dtCurrentTable.Rows[i - 1]["RowNumber"] = i;
            }


            ViewState["CurrentTable"] = dtCurrentTable;
            ViewState["SizeAndQtyTableList"] = sizeAndQtyTableList;

            selectedBuyerValue = Convert.ToInt32(ddlBuyers.SelectedValue);
            LoadSizeInfo();

            rpt.DataSource = dtCurrentTable;
            rpt.DataBind();
            SetPreviousData();

        }




        protected void btnSaveOrder_Click(object sender, EventArgs e)
        {

            try
            {
                int buyerDepartment = 0;
                int buyerSeason = 0;
                int buyerYear = 0;

                if (ddlBuyerDeprtments.SelectedValue != "")
                {
                    buyerDepartment = Convert.ToInt32(ddlBuyerDeprtments.SelectedValue);
                }
                if (ddlSeason.SelectedValue != "")
                {
                    buyerSeason = Convert.ToInt32(ddlSeason.SelectedValue);
                }

                if (ddlOrderYear.SelectedValue != "")
                {
                    buyerYear = Convert.ToInt32(ddlOrderYear.SelectedValue);
                }


                if (ddlStyle.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a style.')", true);
                }
                else if (tbxOrderNumber.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter order number.')", true);

                }
                else if (tbxOrderDate.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter order date.')", true);
                }
                //else if (tbxLCValue.Text == "")
                //{
                //    ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please enter LC value.')", true);
                //}
                else if (ddlOrderStatus.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select order status.')", true);
                }
                else if (cblDeliveryCountries.SelectedIndex == -1)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a delivery country.')", true);
                }
                else
                {
                    var colorQtyAndSizeQtyEqual = true;
                    for (int l = 0; l < rpt.Items.Count; l++)
                    {
                        var tbxOrderQtyByColor = (TextBox)rpt.Items[l].FindControl("tbxOrderQtyByColor");
                        var colorQty = 0;
                        var totalSizeQty = 0;
                        if (!string.IsNullOrEmpty(tbxOrderQtyByColor.Text))
                        {
                            colorQty = int.Parse(tbxOrderQtyByColor.Text);
                        }
                        GridView gvSizeQuantity1 = (GridView)rpt.Items[l].FindControl("gvSizeQuantity1");

                        for (int j = 0; j < gvSizeQuantity1.Columns.Count; j++)
                        {
                            TextBox box5 = (TextBox)gvSizeQuantity1.Rows[0].Cells[j].FindControl("tbxOrderQuantity" + j);
                            if (box5.Text != "")
                            {
                                totalSizeQty += Convert.ToInt32(box5.Text);
                            }
                        }

                        if (colorQty != totalSizeQty)
                        {
                            colorQtyAndSizeQtyEqual = false;
                            break;
                        }
                    }

                    if (colorQtyAndSizeQtyEqual)
                    {
                        OrderId = orderManager.InsertOrderBasicInfo(Convert.ToInt32(ddlBuyers.SelectedValue), buyerDepartment, Convert.ToInt32(ddlStyle.SelectedValue), buyerSeason, buyerYear, tbxOrderNumber.Text, Convert.ToDateTime(tbxOrderDate.Text), tbxOrderItem.Text, /*tbxPaymentTerm.Text*/ "", /*Convert.ToDouble(tbxLCValue.Text)*/ 0, Convert.ToInt32(ddlOrderStatus.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now);

                        if (OrderId > 0)
                        {
                            int saveDeliveryCountryResult = SaveDeliveryCountry();
                            if (saveDeliveryCountryResult > 0)
                            {
                                int saveColorAndSizeResult = saveColorsAndSizes(OrderId);

                                if (saveColorAndSizeResult > 0)
                                {
                                    //UploadFiles(OrderId);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                                    Response.AddHeader("REFRESH", "2;URL=AddNewOrder.aspx");
                                }

                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Delivery country for this order could not be saved.')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('The order already exists.');", true);
                        }

                        // ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Order quantity for the color and sum of size quantities do not match.');", true);
                    }

                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "')", true);
            }

        }


        public int saveColorsAndSizes(int orderId)
        {

            int orderColorId = 0;
            double unitCost = 0.0;
            int orderQtyByColor = 0;


            for (int i = 0; i < rpt.Items.Count; i++)
            {

                //TextBox box1 = (TextBox)rpt.Items[i].FindControl("tbxColorCode");
                //TextBox box2 = (TextBox)rpt.Items[i].FindControl("tbxColorDescription");
                DropDownList ddlColor = (DropDownList)rpt.Items[i].FindControl("ddlColors");
                //TextBox box3 = (TextBox)rpt.Items[i].FindControl("tbxUnitCost");
                TextBox box4 = (TextBox)rpt.Items[i].FindControl("tbxOrderQtyByColor");

                //if (box3.Text != "")
                //{
                //    unitCost = Convert.ToDouble(box3.Text);
                //}

                if (box4.Text != "")
                {
                    orderQtyByColor = Convert.ToInt32(box4.Text);
                }

                orderColorId = orderManager.SaveOrderColor(orderId, int.Parse(ddlColor.SelectedValue), unitCost, orderQtyByColor, CommonMethods.SessionInfo.UserName, DateTime.Now);

                if (orderColorId > 0)
                {
                    //Repeater sizeAndQtyRpt = (Repeater)rpt.Items[i].FindControl("rptSizeAndQty");
                    GridView gvSizeQuantity1 = (GridView)rpt.Items[i].FindControl("gvSizeQuantity1");

                    DataTable dtColorSizes = OrderColorSizes();

                    for (int j = 0; j < gvSizeQuantity1.Columns.Count; j++)
                    {
                        int orderQtyBySize = 0;
                        Label sizeId = (Label)gvSizeQuantity1.Rows[0].Cells[j].FindControl("lblSizeId" + j);
                        TextBox box5 = (TextBox)gvSizeQuantity1.Rows[0].Cells[j].FindControl("tbxOrderQuantity" + j);

                        if (box5.Text != "")
                        {
                            orderQtyBySize = Convert.ToInt32(box5.Text);
                        }

                        dtColorSizes.Rows.Add(orderColorId, Convert.ToInt32(sizeId.Text), orderQtyBySize, CommonMethods.SessionInfo.UserName, DateTime.Now);
                    }
                    orderManager.SaveOrderColorSizes(dtColorSizes);
                }
            }

            return orderColorId;
        }

        public int SaveDeliveryCountry()
        {

            DataTable dt = OrderDeliveryCountries();
            for (int i = 0; i < cblDeliveryCountries.Items.Count; i++)
            {
                if (cblDeliveryCountries.Items[i].Selected)
                {
                    dt.Rows.Add(OrderId, cblDeliveryCountries.Items[i].Value, CommonMethods.SessionInfo.UserName, DateTime.Now);
                }
            }

            return orderManager.SaveOrderDeliveryCountries(dt);

        }



        public DataTable OrderDeliveryCountries()
        {
            DataTable dt = new DataTable("OrderDeliveryCountries");
            dt.Columns.Add("OrderId", typeof(int));
            dt.Columns.Add("DeliveryCountryId", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }


        public DataTable OrderColorSizes()
        {
            DataTable dt = new DataTable("OrderColorSizes");
            dt.Columns.Add("OrderColorId", typeof(int));
            dt.Columns.Add("SizeId", typeof(int));
            dt.Columns.Add("Quantity", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }

        public void UploadFiles(int orderId)
        {
            foreach (HttpPostedFile postedFile in FileUpload1.PostedFiles)
            {
                if (postedFile.ContentLength > 0)
                {
                    string fileName = Path.GetFileName(postedFile.FileName);
                    string contentType = postedFile.ContentType;
                    int fileSize = postedFile.ContentLength;
                    using (Stream fs = postedFile.InputStream)
                    {
                        using (BinaryReader br = new BinaryReader(fs))
                        {
                            byte[] bytes = br.ReadBytes((Int32)fs.Length);
                            orderManager.SaveOrderFiles(orderId, fileName, contentType, fileSize, bytes, CommonMethods.SessionInfo.UserName, DateTime.Now);
                        }
                    }
                }
            }
        }

        //protected void rptSizeAndQty_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    Repeater sourceRptr = (Repeater)sender;
        //    var dt = (DataTable)sourceRptr.DataSource;
        //    var index = e.Item.ItemIndex;
        //    if (OrderColorId != 0 && dt != null && index != -1)
        //    {
        //        var sizeId = dt.Rows[index].Field<int>("Id");
        //        var sizeInfo = unitOfWork.GenericRepositories<OrderColorSizes>().Get(x => x.OrderColorId == OrderColorId && x.SizeId == sizeId).ToList();
        //        if (sizeInfo != null && sizeInfo.Count != 0)
        //        {
        //            ((TextBox)e.Item.FindControl("tbxOrderQuantity")).Text = sizeInfo[0].Quantity.ToString();
        //        }                
        //    }
        //}

        protected void btnUpdateOrder_Click(object sender, EventArgs e)
        {
            try
            {
                int buyerDepartment = 0;
                int buyerSeason = 0;
                int buyerYear = 0;

                if (ddlBuyerDeprtments.SelectedValue != "")
                {
                    buyerDepartment = Convert.ToInt32(ddlBuyerDeprtments.SelectedValue);
                }
                if (ddlSeason.SelectedValue != "")
                {
                    buyerSeason = Convert.ToInt32(ddlSeason.SelectedValue);
                }

                if (ddlOrderYear.SelectedValue != "")
                {
                    buyerYear = Convert.ToInt32(ddlOrderYear.SelectedValue);
                }


                if (ddlStyle.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a style.');", true);
                }
                else if (tbxOrderNumber.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter order number.');", true);

                }
                else if (tbxOrderDate.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter order date.');", true);
                }
                //else if (tbxLCValue.Text == "")
                //{
                //    ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please enter LC value.')", true);
                //}
                else if (ddlOrderStatus.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select order status.');", true);
                }
                else if (cblDeliveryCountries.SelectedIndex == -1)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a delivery country.');", true);
                }
                else
                {
                    var stColor = "";
                    var colorQtyAndSizeQtyEqual = true;
                    for (int l = 0; l < rpt.Items.Count; l++)
                    {
                        var tbxOrderQtyByColor = (TextBox)rpt.Items[l].FindControl("tbxOrderQtyByColor");
                        stColor = ((DropDownList)rpt.Items[l].FindControl("ddlColors")).SelectedItem.Text;
                        var colorQty = 0;
                        var totalSizeQty = 0;
                        if (!string.IsNullOrEmpty(tbxOrderQtyByColor.Text))
                        {
                            colorQty = int.Parse(tbxOrderQtyByColor.Text);
                        }
                        GridView gvSizeQuantity1 = (GridView)rpt.Items[l].FindControl("gvSizeQuantity1");

                        for (int j = 0; j < gvSizeQuantity1.Columns.Count; j++)
                        {
                            TextBox box5 = (TextBox)gvSizeQuantity1.Rows[0].Cells[j].FindControl("tbxOrderQuantity" + j);
                            if (box5.Text != "")
                            {
                                totalSizeQty += Convert.ToInt32(box5.Text);
                            }
                        }

                        if (colorQty != totalSizeQty)
                        {
                            colorQtyAndSizeQtyEqual = false;
                            break;
                        }
                    }

                    if (colorQtyAndSizeQtyEqual)
                    {
                        var orderInfo = unitOfWork.GenericRepositories<Orders>().GetByID(OrderId);
                        if (orderInfo != null)
                        {
                            //Order Updates
                            orderInfo.BuyerId = Convert.ToInt32(ddlBuyers.SelectedValue);
                            orderInfo.BuyerDepartmentId = buyerDepartment;
                            orderInfo.StyleId = Convert.ToInt32(ddlStyle.SelectedValue);
                            orderInfo.OrderNumber = tbxOrderNumber.Text;
                            orderInfo.OrderSeasonId = buyerSeason;
                            orderInfo.OrderYearId = buyerYear;
                            orderInfo.OrderDate = Convert.ToDateTime(tbxOrderDate.Text);
                            orderInfo.OrderItem = tbxOrderItem.Text;
                            orderInfo.PaymentTerms = "";//tbxPaymentTerm.Text;
                            orderInfo.LCValue = 0;//Convert.ToDecimal(tbxLCValue.Text);
                            orderInfo.StatusId = Convert.ToInt32(ddlOrderStatus.SelectedValue);
                            if (Convert.ToInt32(ddlOrderStatus.SelectedValue) == 49)
                            {
                                orderInfo.IsActive = 0;
                            }

                            orderInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                            orderInfo.UpdateDate = DateTime.Now;
                            unitOfWork.GenericRepositories<Orders>().Update(orderInfo);

                            //Delete and Update DeliveryCountries
                            UpdateDeliveryCountry(orderInfo.Id);


                            //Delete and Update Color and Order
                            UpdateColorAndSize(orderInfo.Id);
                            unitOfWork.Save();
                            //UploadFiles(orderInfo.Id);
                            CommonMethods.SendEventNotification(1, orderInfo.BuyerId, orderInfo.StyleId, orderInfo.Id, orderInfo.Id);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('The order dose not exists.');", true);
                        }
                        //Response.AddHeader("REFRESH", "2;URL=ViewOrders.aspx");
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewOrders.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + stColor + " color order quantity and sum of sizes quantities do not match.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "');", true);
            }
        }

        private void UpdateColorAndSize(int orderId)
        {
            //delete order color and sizes
            var colorsInfo = unitOfWork.GenericRepositories<OrderColors>().Get(x => x.OrderId == orderId);
            foreach (var itemColor in colorsInfo)
            {
                //var sizeInfo = unitOfWork.GenericRepositories<OrderColorSizes>().Get(x => x.OrderColorId == itemColor.Id);
                //foreach (var itemSize in sizeInfo)
                //{
                //    unitOfWork.GenericRepositories<OrderColorSizes>().Delete(itemSize);
                //}
                unitOfWork.GenericRepositories<OrderColors>().Delete(itemColor);
            }
            // End of delete
            saveColorsAndSizes(orderId);
        }

        protected void UpdateDeliveryCountry(int OrderId)
        {
            var deliveryCountris = unitOfWork.GenericRepositories<OrderDeliveryCountries>().Get(x => x.OrderId == OrderId);
            foreach (var item in deliveryCountris)
            {
                unitOfWork.GenericRepositories<OrderDeliveryCountries>().Delete(item);
            }
            for (int i = 0; i < cblDeliveryCountries.Items.Count; i++)
            {
                if (cblDeliveryCountries.Items[i].Selected)
                {
                    var deleveryCountry = new OrderDeliveryCountries()
                    {
                        OrderId = OrderId,
                        DeliveryCountryId = Convert.ToInt32(cblDeliveryCountries.Items[i].Value),
                        CreatedBy = CommonMethods.SessionInfo.UserName,
                        CreateDate = DateTime.Now
                    };
                    unitOfWork.GenericRepositories<OrderDeliveryCountries>().Insert(deleveryCountry);
                }
            }
        }

        protected void gvSizeQuantity1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblBuyerId = (Label)((GridView)sender).DataItemContainer.FindControl("lblBuyerId");
                selectedBuyerValue = int.Parse(lblBuyerId.Text);

                TextBox txtSizeQty = null;
                Label lblSizeId = null;
                LoadSizeInfo();

                for (int i = 0; i < dtSizes.Rows.Count; i++)
                {
                    txtSizeQty = new TextBox();
                    txtSizeQty.ID = "tbxOrderQuantity" + i.ToString();
                    txtSizeQty.Text = "";
                    txtSizeQty.Width = 60;
                    txtSizeQty.TextMode = TextBoxMode.Number;
                    e.Row.Cells[i].Controls.Add(txtSizeQty);


                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId" + i.ToString();
                    lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);

                    //var enitryId = new Label();
                    //enitryId.ID = "lblenitryId2" + i.ToString();
                    //enitryId.Text = "";
                    //enitryId.Visible = false;
                    //e.Row.Cells[i].Controls.Add(enitryId);

                    if (OrderId != 0)
                    {
                        var sizeId = int.Parse(dtSizes.Rows[i][0].ToString());
                        var lblOrderColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblOrderColorId");
                        var orderColorId = int.Parse(string.IsNullOrEmpty(lblOrderColorId.Text) ? "0" : lblOrderColorId.Text);
                        if (orderColorId != 0)
                        {
                            //var sizeInfo = unitOfWork.GenericRepositories<OrderColorSizes>().Get(x => x.OrderColorId == orderColorId && x.SizeId == sizeId).FirstOrDefault();
                            //if (sizeInfo != null)
                            //{
                            //    txtSizeQty.Text = sizeInfo.Quantity + "";
                            //}
                        }

                    }
                }
            }
        }
    }
}