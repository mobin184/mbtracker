﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;



namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class AddNewBuyer : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();


        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                //if (Request.QueryString["buyerId"] != null)
                //{
                //    BuyerId = int.Parse(Request.QueryString["buyerId"]);

                //    PopulateBuyer(BuyerId);
                //}

                EnableDisableDeleteButton();
                BindData();

            }

        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");

            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("AddNewBuyer", 2, 1);

        }


        protected void BindData()
        {

            DataTable dt = new CommonManager().GetBuyersByUser(CommonMethods.SessionInfo.UserId);

            if (dt.Rows.Count > 0)
            {
                rptBuyers.DataSource = dt;
                rptBuyers.DataBind();
            }
            else
            {
                rptBuyers.DataSource = null;
                rptBuyers.DataBind();
            }
        }



        int BuyerId
        {
            set { ViewState["buyerId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["buyerId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }


        protected void PopulateBuyer(int buyerId)
        {

            DataTable dt = buyerManager.GetBuyerById(buyerId);

            if (dt.Rows.Count > 0)
            {
                tbxBuyerName.Text = dt.Rows[0]["BuyerName"].ToString();
                tbxDescription.Text = dt.Rows[0]["BuyerInfo"].ToString();
            }
            //btnSubmit.Text = " Update ";
            roleActionTitle.Text = "Update Buyer";
            btnSubmit.Visible = false;
            btnUpdate.Visible = true;
        }


        protected void rptBuyer_ItemCommand(object source, RepeaterCommandEventArgs e)

        {

            if (e.CommandName == "Edit")
            {

                BuyerId = Convert.ToInt32(e.CommandArgument.ToString());
                PopulateBuyer(BuyerId);


            }
            else if (e.CommandName == "Delete")
            {
               

                try
                {
                    BuyerId = Convert.ToInt32(e.CommandArgument.ToString());
                    int result = buyerManager.DeleteBuyer(BuyerId, CommonMethods.SessionInfo.UserName, DateTime.Now);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                }
                catch (Exception ex)
                {

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Error - something went wrong.')", true);
                }

                BindData();

                //var styleCannotBeDeleted = unitOfWork.GenericRepositories<viewStylesCanNotBeDeleted>().GetByID(BuyerStyleId);
                //if (styleCannotBeDeleted == null)
                //{
                //    var title = "Warning";
                //    var msg = "Are you sure you want to delete?";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                //}
                //else
                //{
                //    var title = "Warning";
                //    var msg = "Sorry, you can not delete it.<br />There was some dependencies of this style.";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                //}
            }
        }



        protected void btnSave_Click(object sender, EventArgs e)
        {

            string actionTaker = CommonMethods.SessionInfo.UserName;

            BuyerId = buyerManager.InsertBuyer(tbxBuyerName.Text, tbxDescription.Text, actionTaker, DateTime.Now);

            if (BuyerId > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                SetPermissionForUser(BuyerId);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Buyer name already exists. Please try another name')", true);
            }

            ClearAll();
            BindData();

        }


        protected void ClearAll()
        {
            tbxBuyerName.Text = "";
            tbxDescription.Text = "";

        }

        protected void SetPermissionForUser(int newBuyerId)
        {

            int userId = CommonMethods.SessionInfo.UserId;
            string actionTaker = CommonMethods.SessionInfo.UserName;

            int result = buyerManager.SetBuyerPermissionForUser(userId, newBuyerId, actionTaker, DateTime.Now);


            if (result < 1)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Buyer permission to user failed.')", true);
            }

        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string actionTaker = CommonMethods.SessionInfo.UserName;

            int value = buyerManager.UpdateBuyer(BuyerId, tbxBuyerName.Text, tbxDescription.Text, actionTaker, DateTime.Now);
            if (value > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Buyer update was failed.')", true);
            }

            //Response.AddHeader("REFRESH", "2;URL=ViewRoles.aspx");
            ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('AddNewBuyer.aspx');", true);
        }


    }
}