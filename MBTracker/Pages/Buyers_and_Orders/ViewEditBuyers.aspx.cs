﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;



namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class ViewEditBuyers : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                EnableDisableDeleteButton();

                BindData();
            }

        }


        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewEditBuyers", 2, 1);
        }


        protected void BindData()
        {
           
            DataTable dt = new CommonManager().GetBuyersByUser(CommonMethods.SessionInfo.UserId);

            if (dt.Rows.Count > 0)
            {
                rptBuyers.DataSource = dt;
                rptBuyers.DataBind();
            }
            else
            {
                rptBuyers.DataSource = null;
                rptBuyers.DataBind();
            }
        }


        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {

            int buyerId = Convert.ToInt32(e.CommandArgument.ToString());
            Response.Redirect("AddNewBuyer.aspx?buyerId=" + buyerId);

        }


        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {

            try
            {
                int buyerId = Convert.ToInt32(e.CommandArgument.ToString());
                int result = buyerManager.DeleteBuyer(buyerId, CommonMethods.SessionInfo.UserName, DateTime.Now);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
            }
            catch (Exception ex)
            {

                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Error - something went wrong.')", true);
            }

            BindData();

        }



    }
}