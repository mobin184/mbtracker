﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using Repositories;
using MBTracker.EF;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class ManageSizes : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableDeleteButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                LoadSizeTypes();
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteSize(BuyerSizeId);
                }
            }
        }

        private void LoadSizeTypes()
        {
            var sizeTypes = unitOfWork.GenericRepositories<SizeTypes>().Get().ToList();
            ddlSizeType.DataSource = sizeTypes;
            ddlSizeType.DataTextField = "Name";
            ddlSizeType.DataValueField = "SizeTypeId";
            ddlSizeType.DataBind();
        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");

            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ManageSizes", 2, 1);

        }
        int BuyerSizeId
        {
            set { ViewState["sizeId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["sizeId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue != "")
            {

                BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            else
            {
                pnlSizes.Visible = false;
            }
        }

        private void BindData(int buyerId)
        {

            DataTable dt = new DataTable();

            dt = buyerManager.LoadSizesByBuyer(buyerId);
            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                pnlSizes.Visible = true;
            }
            else
            {
                pnlSizes.Visible = false;
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        { 
            int SequenceNo = 0;

            if(tbxposition_sequence_No.Text != "")
            {
                SequenceNo = Convert.ToInt32(tbxposition_sequence_No.Text);
            }
            int sizeId = buyerManager.InsertSize(tbxSizeName.Text, Convert.ToInt32(ddlBuyers.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now, Convert.ToInt32(ddlSizeType.SelectedValue), SequenceNo);

            if (sizeId > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Size name exists.')", true);
            }


            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearStyleName();


        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int returnValue = buyerManager.UpdateBuyerSize(BuyerSizeId, tbxSizeName.Text, Convert.ToInt32(ddlBuyers.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now, Convert.ToInt32(ddlSizeType.SelectedValue), Convert.ToInt32(tbxposition_sequence_No.Text));
            if (returnValue > 0)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Size update was failed.')", true);
            }


            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearStyleName();

            sizeActionTitle.Text = "Add a New Size:";
            btnSave.Visible = true;
            btnUpdate.Visible = false;

        }


        private void ClearStyleName()
        {
            tbxSizeName.Text = string.Empty;

        }

        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)

        {

            if (e.CommandName == "Edit")
            {

                int buyerSizeId = Convert.ToInt32(e.CommandArgument.ToString());
                PopulateSize(buyerSizeId);


            }
            else if (e.CommandName == "Delete")
            {

               BuyerSizeId = Convert.ToInt32(e.CommandArgument.ToString());

                var sizeCannotBeDeleted = unitOfWork.GenericRepositories<viewSizesCanNotBeDeleted>().GetByID(BuyerSizeId);
                if (sizeCannotBeDeleted == null)
                {
                    var title = "Warning";
                    var msg = "Are you sure you want to delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                }
                else
                {
                    var title = "Warning";
                    var msg = "Sorry, you can not delete it.<br />There was some dependencies of this size.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                }
            }
        }

        private void PopulateSize(int buyerStyleId)
        {
            DataTable dt = buyerManager.GetSizeById(buyerStyleId);
            if (dt.Rows.Count > 0)
            {
                BuyerSizeId = Convert.ToInt32(dt.Rows[0]["Id"]);
                tbxSizeName.Text = dt.Rows[0]["SizeName"].ToString();
                tbxposition_sequence_No.Text = dt.Rows[0]["OrderBy"].ToString();
                ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, Convert.ToInt32(dt.Rows[0]["BuyerId"]));
                ddlSizeType.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSizeType, Convert.ToInt32(dt.Rows[0]["SizeTypeId"]));

                sizeActionTitle.Text = "Update Size:";
                btnSave.Visible = false;
                btnUpdate.Visible = true;
            }


        }


        private void DeleteSize(int buyerSizeId)
        {

            int returnValue = buyerManager.DeleteBuyerSize(buyerSizeId, CommonMethods.SessionInfo.UserName, DateTime.Now);

            if (returnValue > 0)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Size deletion was failed.')", true);
            }

            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearStyleName();


            sizeActionTitle.Text = "Add a New Size:";
            btnSave.Visible = true;
            btnUpdate.Visible = false;

        }
    }
}