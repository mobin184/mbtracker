﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using Repositories;
using MBTracker.EF;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class ManageDeliveryCountries : System.Web.UI.Page
    {


        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableDeleteButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteDeliveryCountry(BuyerDeliveryCountryId);
                }
            }
        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ManageDeliveryCountries", 2, 1);
        }

        int BuyerDeliveryCountryId
        {
            set { ViewState["deliveryCountryId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["deliveryCountryId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue != "")
            {

                BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            else
            {
                pnlStyles.Visible = false;
            }
        }

        private void BindData(int buyerId)
        {

            DataTable dt = new DataTable();

            dt = buyerManager.LoadDeliveryCountriesByBuyer(buyerId);
            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                pnlStyles.Visible = true;
            }
            else
            {
                pnlStyles.Visible = false;
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {


            int countryId = buyerManager.InsertDeliveryCountry(tbxCountryName.Text, txbCountryCode.Text, Convert.ToInt32(ddlBuyers.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now);

            if (countryId > 0)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Delivery country name exists.')", true);
            }


            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearDeliveryCountryName();


        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int returnValue = buyerManager.UpdateBuyerDeliveryCountry(BuyerDeliveryCountryId, tbxCountryName.Text, txbCountryCode.Text, Convert.ToInt32(ddlBuyers.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now);
            if (returnValue > 0)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Delivery country update was failed.')", true);
            }


            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearDeliveryCountryName();

            delieveryCountryActionTitle.Text = "Add a New Country:";
            btnSave.Visible = true;
            btnUpdate.Visible = false;

        }


        private void ClearDeliveryCountryName()
        {
            tbxCountryName.Text = string.Empty;
            txbCountryCode.Text = string.Empty;
        }


        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)

        {

            if (e.CommandName == "Edit")
            {

                int buyerDeliveryCountryId = Convert.ToInt32(e.CommandArgument.ToString());
                PopulateDeliveryCountry(buyerDeliveryCountryId);


            }
            else if (e.CommandName == "Delete")
            {

                BuyerDeliveryCountryId = Convert.ToInt32(e.CommandArgument.ToString());
                var countryCannotBeDeleted = unitOfWork.GenericRepositories<viewDeliveryCountriesCanNotBeDeleted>().GetByID(BuyerDeliveryCountryId);
                if (countryCannotBeDeleted == null)
                {
                    var title = "Warning";
                    var msg = "Are you sure you want to delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                }
                else
                {
                    var title = "Warning";
                    var msg = "Sorry, you can not delete it.<br />There was some dependencies of this country.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                }
            }
        }

        private void PopulateDeliveryCountry(int buyerDeliveryCountryId)
        {
            DataTable dt = buyerManager.GetDeliveryCountryById(buyerDeliveryCountryId);
            if (dt.Rows.Count > 0)
            {
                BuyerDeliveryCountryId = Convert.ToInt32(dt.Rows[0]["Id"]);
                tbxCountryName.Text = dt.Rows[0]["DeliveryCountryName"].ToString();
                txbCountryCode.Text = dt.Rows[0]["DeliveryCountryCode"].ToString();


                ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, Convert.ToInt32(dt.Rows[0]["BuyerId"]));

                delieveryCountryActionTitle.Text = "Update Delivery Country:";
                btnSave.Visible = false;
                btnUpdate.Visible = true;
            }


        }


        private void DeleteDeliveryCountry(int buyerDeliveryCountryId)
        {

            int returnValue = buyerManager.DeleteBuyerDeliveryCountry(buyerDeliveryCountryId, CommonMethods.SessionInfo.UserName, DateTime.Now);

            if (returnValue > 0)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Delivery country deletion was failed.')", true);
            }

            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearDeliveryCountryName();

            delieveryCountryActionTitle.Text = "Add a New Country:";
            btnSave.Visible = true;
            btnUpdate.Visible = false;

        }

    }
}