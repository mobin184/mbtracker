﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddShipmentV2.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.AddShipmentV2"  Async="true" %>


<%@ Register Src="~/User_Controls/Header.ascx" TagPrefix="uc1" TagName="Header" %>
<%@ Register Src="~/User_Controls/MainMenu.ascx" TagPrefix="uc1" TagName="MainMenu" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>MBTracker</title>
    <link href="../../css/web.css" rel="stylesheet" />
    <script type="text/javascript" src="../../js/gridviewscroll.js"></script>
    <script type="text/javascript">
        var gridViewScroll = null;
        window.onload = function () {
            gridViewScroll = new GridViewScroll({
                elementID: "tblColorAndDeliveryCountries",
                width: 1450,
                height: 500,
                freezeColumn: true,
                freezeFooter: false,
                freezeColumnCssClass: "GridViewScrollItemFreeze",
                freezeFooterCssClass: "GridViewScrollFooterFreeze",
                freezeHeaderRowCount: 1,
                freezeColumnCount: 3,
                onscroll: function (scrollTop, scrollLeft) {
                    console.log(scrollTop + " - " + scrollLeft);
                }
            });
            gridViewScroll.enhance();
        }
        function getScrollPosition() {
            var position = gridViewScroll.scrollPosition;
            alert("scrollTop: " + position.scrollTop + ", scrollLeft: " + position.scrollLeft);
        }
        function setScrollPosition() {
            var scrollPosition = { scrollTop: 50, scrollLeft: 50 };

            gridViewScroll.scrollPosition = scrollPosition;
        }
    </script>

    <script src="../../js/html5-trunk.js"></script>
    <link href="~/css/bootstrap.min.css" rel="stylesheet" />
    <link type="text/css" href="~/icomoon/style.css" rel="stylesheet" />
    <%--<link type="text/css" href="/css/nvd-charts.css" rel="stylesheet" />--%>
    <link type="text/css" href="~/css/main.css" rel="stylesheet" />
    <%--   <link type="text/css" href='/css/fullcalendar/fullcalendar.css' rel='stylesheet' />
    <link type="text/css" href='/css/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />--%>

    <link type="text/css" href="~/css/custom.css" rel="stylesheet" />
    <link type="text/css" href="~/content/toastr.css" rel="stylesheet" />
    <script src="../../js/jquery.min.js"></script>
    <script src="../../js/bootstrap.js"></script>
    <script src="../../Scripts/toastr.js"></script>
    <%--  <link href="../../FancyBox/jquery.fancybox.css" rel="stylesheet" />--%>
    <link href="../../css/font-awesome.min.css" rel="stylesheet" />

    <%--<link href="../../css/all.css" rel="stylesheet" />--%>
    <link href="../../css/sweetalert.css" rel="stylesheet" />
    <script src="../../Scripts/sweetalert.min.js"></script>
    <link href="../../css/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.min.js"></script>
    <script src="../../js/jquery.blockUI.js"></script>
    <script src="../../Scripts/jquery.signalR-2.2.2.js"></script>
<%--    <script src="/SignalR/hubs"></script>--%>

    <script src="../../js/jquery.dataTables.js"></script>
    <link href="../../content/css/select2.css" rel="stylesheet" />
    <script src="../../Scripts/select2.js"></script>


    <script type="text/javascript">

         $(function () {
            //$('.custom-select2').select2();
            $('select').select2();
             //$('select').select2({
             //    sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)),
             //});
        })

        $(document).ready(function () {

            $("#main-nav ul li ul li").hover(function () {
                $(this).children("ul").toggleClass('test-SubMenu-open', 1, "easeOutSine");
            }, function () {
                $(this).children("ul").toggleClass('test-SubMenu-open', 1, "easeOutSine");
            });
        });

        try {

            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

        } catch (e) {

        }

        function beginRequest(sender, args) {

            $.blockUI({
                //message: '<img src="../../images/loader.gif" />',
                //message: '<div class="spinner"></div>',
                message: '<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>',
                //message: '<div class="lds-ripple"><div></div><div></div></div>',
                css: {
                    border: 'none',
                    backgroundColor: 'transparent'
                },
                overlayCSS: { backgroundColor: '#0d0d0d' }
            });
        }

        function endRequest(sender, args) {
            $.unblockUI();
        }

        function blockUI() {
            $.blockUI({
                message: '<img src="../../images/loader.gif" />',
                css: {
                    border: 'none',
                    backgroundColor: 'transparent'
                },
                overlayCSS: { backgroundColor: '#0d0d0d' }
            });
        }

        function unblockUI() {
            $.unblockUI();
        }

        function scrollDown(position) {
            console.log(position);
            var n = $(document).height();
            if (position != "" && position != 'undefined' && position != null) {
                n = position;
            }
            $('html, body').animate({ scrollTop: n }, 'slow');
        };

        function reloadPage() {
            setTimeout(function () { location.reload(); }, 1000);
        }

        function resetPage() {
            debugger
            $('#form1')[0].reset();
        }


        function returnToPage(url) {
            setTimeout(function () {
                $(location).attr('href', url);
            }, 2000);
        }

        function showConfirm(title, msg, purpose, width, onlyClose) {
            var dialogWidth = "auto";
            var dialogButtons = {
                Yes: function () {
                    $(this).dialog("close");
                    __doPostBack("Confirm", purpose);
                },
                No: function () {
                    $(this).dialog("close");
                }
            };

            if (width != null && width != 'undefined') {
                dialogWidth = width;
            }
            if (onlyClose == 'true') {
                dialogButtons = {
                    Close: function () {
                        $(this).dialog("close");
                    }
                }
            }

            $('<div></div>').appendTo('body')
                .html('<div><h6>' + msg + '</h6></div>')
                .dialog({
                    modal: true,
                    title: title,
                    zIndex: 10000,
                    autoOpen: true,
                    width: dialogWidth,
                    resizable: false,
                    buttons: dialogButtons,
                    close: function (event, ui) {
                        $(this).remove();
                    }
                });
        };


        function showConfirmSA(title, msg, purpose) {
            swal({
                title: title,
                text: msg,
                html: true,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        __doPostBack("Confirm", "ConfirmBtnClicked", purpose);
                    }
                });
        };
    </script>

</head>



<body style="margin: 0px; width: 100%; height: 100%;">
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <header id="header">
            <uc1:Header runat="server" ID="Header" />
        </header>

        <div class="container" id="menu" style="margin: 0px;">
            <div id="main-nav" class="hidden-phone hidden-tablet">
                <uc1:MainMenu runat="server" ID="MainMenu" />
                <div class="clearfix"></div>
            </div>
        </div>




        <div class="container-fluid">
            <div class="dashboard-wrapper">
                <div class="main-container" id="body">
                    <div class="page-header">
                        <div class="clearfix"></div>
                    </div>
                    <div>
                    </div>

                    <%-- Start Main Content --%>


                    <div class="row-fluid">
                        <div class="span9">
                            <div class="widget">
                                <div class="widget-header">
                                    <div class="title">
                                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                                        <asp:Label runat="server" ID="actionTitle" Text="Setup Shipment Date:"></asp:Label>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="form-horizontal">
                                        <div class="pull-right" style="text-align: center; font-size: 12px; line-height: 20px; padding-bottom: 20px">
                                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                                        </div>
                                        <div class="control-group">
                                            <div class="col-md-12 col-sm-12">
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="control-group">
                                                <label for="inputBuyer" class="control-label">
                                                    <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                <div class="controls controls-row">
                                                    <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label for="inputBuyer" class="control-label">
                                                    <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                <div class="controls controls-row">
                                                    <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label for="inputBuyer" class="control-label">
                                                    <asp:Label ID="lblOrder" runat="server" Text="Select Order:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                <div class="controls controls-row">
                                                    <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlOrders_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="ddlOrders"><span style="font-weight: 700; color: #CC0000">Please select an order.</span></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="control-group">
                                                <label for="inputEmail3" class="control-label">
                                                    <asp:Label ID="lblShippingDate" runat="server" Text="Shipment Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                <div class="controls controls-row">
                                                    <asp:TextBox ID="tbxShippingDate" runat="server" placeholder="Enter Shipment Date" CssClass="form-control" Width="100%" TextMode="Date"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbxShippingDate"><span style="font-weight: 700; color: #CC0000">Enter shipment date</span></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label for="inputEmail3" class="control-label">
                                                    <asp:Label ID="lblShipmentMode" runat="server" Text="Shipment Mode:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                <div class="controls controls-row">
                                                    <asp:DropDownList ID="ddlShipmentMode" runat="server" Display="Dynamic" Width="100%" CssClass="form-control">
                                                        <asp:ListItem Value="Sea">Sea</asp:ListItem>
                                                        <asp:ListItem Value="Air">Air</asp:ListItem>
                                                        <asp:ListItem Value="Sea & Air">Sea & Air</asp:ListItem>
                                                    </asp:DropDownList><br />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="ddlShipmentMode"><span style="font-weight: 700; color: #CC0000">Please select shipment mode.</span></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label for="inputEmail3" class="control-label">
                                                    <asp:Label ID="lblShimentCancelDate" runat="server" Text="Cancel Date (if any):"></asp:Label></label>
                                                <div class="controls controls-row">
                                                    <asp:TextBox ID="tbxShipmentCancelDate" runat="server" placeholder="Enter Shipment Cancel Date (if any)" CssClass="form-control" Width="100%" TextMode="Date"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group hidden">
                                                <label for="inputEmail3" class="control-label">
                                                    <asp:Label ID="lblInspecAvailDate" runat="server" Text="Inspection Date (if any):"></asp:Label></label>
                                                <div class="controls controls-row">
                                                    <asp:TextBox ID="tbxInspecAvailDate" runat="server" placeholder="Inspection Availabity Date (if any)" CssClass="form-control" Width="100%" TextMode="Date"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="controls controls-row">
                                            <asp:Label ID="lblColorOrDeliveryCountryNotFound" runat="server" Text="" Visible="false" BackColor="#ffff00"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="span12" style="margin-left: 0px" runat="server" id="pnlColorDeliveryCountryAndQuantity" visible="false">
                            <div class="widget">
                                <div class="widget-body">
                                    <asp:Panel ID="pnl" runat="server">
                                        <%--<div class="form-horizontal">--%>
                                        <div class="control-group">
                                            <label for="inputDeliveryCountry" class="control-label">
                                                <asp:Label ID="lblColorAndSize" runat="server" Text="Shipment Information:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputDeliveryCountry" class="control-label"></label>
                                            <div class="controls controls-row">
                                                <div id="dt_example2" class="example_alt_pagination">
                                                    <asp:Repeater ID="rptColorAndDeliveryCountries" runat="server" OnItemDataBound="rptColorAndDeliveryCountries_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="tblColorAndDeliveryCountries" class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr class="GridViewScrollHeader">
                                                                        <th style="text-align: center">
                                                                            <asp:Label ID="lblColorDesc" runat="server" Text="Color"></asp:Label></th>
                                                                        <th style="text-align: center">
                                                                            <asp:Label ID="lblQuantity" runat="server" Text="Order Qty"></asp:Label></th>
                                                                        <th style="text-align: center">
                                                                            <asp:Label ID="lblShipmentScheduled" runat="server" Text="Already Scheduled Qty"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="lblColorDescription" runat="server" Text="Delivery Country, Size & Quantity"></asp:Label></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr class="GridViewScrollItem">
                                                                <td style="vertical-align: middle">
                                                                    <asp:Label ID="lblColorDesc" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ColorDescription")%>'></asp:Label>
                                                                    <asp:Label ID="lblColorId" Visible="false" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>'></asp:Label>
                                                                    <asp:Label ID="lblStyleId" runat="server" Visible="false" Text=''></asp:Label>
                                                                </td>
                                                                <td style="vertical-align: middle">
                                                                    <asp:Label ID="lblOrderQty" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "OrderQuantity")%>'></asp:Label></td>
                                                                <td style="vertical-align: middle">
                                                                    <asp:Label ID="lblShipmentAlreadyScheduled" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ScheduledShipment")%>'></asp:Label>
                                                                    <asp:Label ID="lblShipmentInThisDate" runat="server" Text="" Visible="false"></asp:Label>

                                                                </td>
                                                                <td>
                                                                    <asp:Repeater ID="rptDeliveryCountryAndQty" runat="server" OnItemDataBound="rptDeliveryCountryAndQty_ItemDataBound">
                                                                        <HeaderTemplate>
                                                                            <table id="tblDeliveryCountryAndQty" class="table table-bordered table-hover">

                                                                                <tr>
                                                                                    <th style="text-align: center">
                                                                                        <asp:Label ID="lblCountryCodeMessage" runat="server" Text="Country Code"></asp:Label></th>
                                                                                    <th>
                                                                                        <asp:Label ID="lblSizeAndQtyMessage" runat="server" Text="&nbsp;"></asp:Label></th>
                                                                                    <th>
                                                                                        <asp:Label ID="lblCountryTotalMessage" runat="server" Text="&nbsp;"></asp:Label></th>
                                                                                </tr>
                                                                                <tbody>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td style="vertical-align: middle; padding-top: 35px; height: 100px">
                                                                                    <asp:Label ID="lblDeliveryCountryId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryCountryId")%>'></asp:Label><asp:Label ID="lblDeliveryCountryName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryCountryCode")%>'></asp:Label>

                                                                                </td>
                                                                                <td style="padding: 1px; height: 90px">
                                                                                    <asp:GridView ID="gvSizeAndQty" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeAndQty_RowCreated"></asp:GridView>
                                                                                </td>
                                                                                <td style="vertical-align: middle">
                                                                                    <asp:Label ID="lblCountryTotal" runat="server" Text="Total: 0"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            </tbody>
                                                                </table>
                                                                <div class="clearfix">
                                                                        </FooterTemplate>
                                                                    </asp:Repeater>
                                                                    <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>'></asp:Label>


                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody>
                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="Label12" runat="server" Text="&nbsp;" CssClass="text-left"></asp:Label></label>
                                            <div class="controls controls-row">
                                                <%-- <div style="text-align: right; font-size: 12px; line-height: 30px; padding-bottom: 0px">
                                    <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                </div>--%>

                                                <asp:LinkButton ID="lnkbtnCalculateSizeTotal" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="Calculate Total" OnClick="lnkbtnCalculateSizeTotal_Click"></asp:LinkButton>
                                                <asp:Button ID="btnSaveShipment" runat="server" class="btn btn-success btn-samll pull-left btnStyle" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSave_Click" />
                                                <asp:Button ID="btnUpdateShipment" runat="server" Visible="false" class="btn btn-success btn-samll pull-left btnStyle" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdateShipment_Click" />

                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>

                    </div>


                    <%-- End Main Content --%>
                </div>
            </div>
        </div>

        <div class="col-md-12" id="footer">
            <div class="row" style="text-align: center; color: #f79825">
                <b>Copyright © 2020 <a style="text-decoration: none; color: #f79825;" href="http://www.masihata.com/">Masihata Group</a> </b>
            </div>
        </div>

    </form>
</body>
</html>
