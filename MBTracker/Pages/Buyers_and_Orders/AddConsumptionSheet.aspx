﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddConsumptionSheet.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.AddConsumptionSheet" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <asp:UpdatePanel ID="ConSheetUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div class="row-fluid">
                <div class="span6">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title">
                                <span class="fs1" aria-hidden="true" data-icon=""></span>
                                <asp:Label runat="server" ID="actionTitle" Text="Add a Consumption Sheet:"></asp:Label>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="form-horizontal">
                                <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 10px">
                                    <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                                </div>
                                <div class="control-group">
                                    <div class="col-md-12 col-sm-12">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label">
                                        <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <%--  <div class="control-group">
                            <label for="inputOrder" class="control-label">
                                <asp:Label ID="lblOrder" runat="server" Text="Select Order:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlOrders_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlOrders"><span style="font-weight: 700; color: #CC0000">Please select an order.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>--%>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row-fluid">
                <div class="span10">
                    <asp:Label ID="lblConsumptionSheetExist" runat="server" Visible="false" Text="<br/>Consumption sheet already exist for this style." BackColor="#ffff00"></asp:Label>
                    <asp:Panel ID="pnlYarnConsumption" runat="server" Visible="false">
                        <div class="widget">
                            <div class="widget-body" style="overflow-x: auto">

                                <div style="text-align: left; font-size: 12px; line-height: 30px; padding-bottom: 10px">
                                    <span style="font-weight: 700">Please Enter the information below:</span>
                                </div>
                                <div class="form-horizontal">
                                    <div class="span6">
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="Label2" runat="server" Text="Select Stage:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlStage" runat="server" Width="70%">
                                                    <asp:ListItem Text="--Select--" Value="" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Development Sample" Value="DevelopmentSample"></asp:ListItem>
                                                    <asp:ListItem Text="Quotation Sample" Value="Quotation"></asp:ListItem>
                                                    <asp:ListItem Text="Production" Value="Preproduction"></asp:ListItem>
                                                    <asp:ListItem Text="Size Setting" Value="SizeSetting"></asp:ListItem>
                                                    <asp:ListItem Text="PP Sample" Value="PPSample"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="Label1" runat="server" Text="Machine Brand:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlMachineBrands" runat="server" Display="Dynamic" Width="70%" CssClass="form-control"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlMachineBrands"><span style="font-weight: 700; color: #CC0000">Enter knitting machine brand</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblKnittingMCGauge" runat="server" Text="Required Gauge:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxKnittingMCGauge" runat="server" placeholder="Enter Knitting Machine Gauge" CssClass="form-control" Width="70%"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxKnittingMCGauge"><span style="font-weight: 700; color: #CC0000">Enter knitting machine gauge</span></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                        <%--<div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblNeedlePerCM" runat="server" Text="Needle Per CM:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxNeedlePerCM" runat="server" placeholder="Enter needle per CM" CssClass="form-control" Width="250"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxNeedlePerCM"><span style="font-weight: 700; color: #CC0000">Enter needle per CM</span></asp:RequiredFieldValidator>

                                            </div>
                                        </div>--%>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblMCSpeed" runat="server" Text="Machine Speed:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxMCSpeed" runat="server" placeholder="Enter machine speed" CssClass="form-control" Width="70%"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxMCSpeed"><span style="font-weight: 700; color: #CC0000">Enter machine speed</span></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="span6">
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblKnittingTime" runat="server" Text="Avg. Knitting Time:" Width="70%"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxKnittingTime" runat="server" placeholder="Enter avg. knitting time" CssClass="form-control" Width="70%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxKnittingTime"><span style="font-weight: 700; color: #CC0000">Enter avg. knitting time</span></asp:RequiredFieldValidator>

                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblDesigner" runat="server" Text="Designer Name:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxDesignerName" runat="server" placeholder="Enter designer name" CssClass="form-control" Width="70%"></asp:TextBox>
                                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator9" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxDesignerName"><span style="font-weight: 700; color: #CC0000">Enter designer name</span></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblDesignerPhone" runat="server" Text="Designer Phone:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxDesignerPhone" runat="server" placeholder="Enter designer name" CssClass="form-control" Width="70%" TextMode="Phone"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator10" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxDesignerPhone"><span style="font-weight: 700; color: #CC0000">Enter designer phone</span></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span12" style="margin-left: 0px;">
                                        <span style="font-weight: 700; color: #CC0000">&nbsp;</span>

                                        <div class="control-group">
                                            <label for="inputYarnConsumption" class="control-label">
                                                <asp:Label ID="lblYarnConsumption" runat="server" Text="Yarn Consumption:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                        </div>

                                        <div class="control-group">
                                            <div id="dt_example" class="example_alt_pagination">
                                                <asp:Repeater ID="rpt" runat="server" OnItemDataBound="rpt_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="data-table-1" class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th class="hidden">
                                                                        <asp:Label ID="lblPlacement" runat="server" Text="Placement"></asp:Label></th>
                                                                    <th style="width: 50%; max-width: 50%">
                                                                        <asp:Label ID="lblYarnComposition" runat="server" Text="Yarn Composition"></asp:Label>

                                                                    </th>
                                                                    <th style="width: 10%">
                                                                        <asp:Label ID="lblYarnCount" runat="server" Text="Yarn Count"></asp:Label></th>
                                                                    <th style="width: 10%">
                                                                        <asp:Label ID="lblPlyMainYarn" runat="server" Text="Ply (main yarn)"></asp:Label></th>
                                                                    <th style="width: 15%">
                                                                        <asp:Label ID="lblPlyLycra" runat="server" Text="Ply (Lycra)"></asp:Label></th>
                                                                    <th style="width: 15%">
                                                                        <asp:Label ID="lblAverageWeight" runat="server" Text="Average Weight/Dzn"></asp:Label></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td class="hidden">
                                                               <%-- <asp:Label ID="lblSweatherPartsId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "Id")%>'></asp:Label>
                                                                <asp:Label ID="lblYarnConsumptionByPartsId" runat="server" Visible="false" Text=""></asp:Label>
                                                                <asp:Label ID="lblSweatherParts" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SweatherPartName")%>' Width="100px" CssClass="text-right"></asp:Label>--%>
                                                            </td>
                                                            <td style="width: 50%; max-width: 50%">
                                                                <%--<asp:TextBox ID="tbxYarnComposition" CssClass="" Height="30" Width="150" runat="server"></asp:TextBox>--%>
                                                                <asp:DropDownList ID="ddlYarnItemId" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                            </td>
                                                            <td style="width: 10%">
                                                                <asp:TextBox ID="tbxYarnCount" CssClass="" Height="30" runat="server"></asp:TextBox></td>
                                                            <td style="width: 10%">
                                                                <asp:TextBox ID="tbxPlyMainYarn" CssClass="" Height="30" runat="server"></asp:TextBox></td>
                                                            <td style="width: 15%">
                                                                <asp:TextBox ID="tbxPlyLycra" CssClass="" Height="30" runat="server"></asp:TextBox></td>
                                                            <td style="width: 15%">
                                                                <asp:TextBox ID="tbxAverageWeight" TextMode="Number" step="0.01" min="0" CssClass="" Height="30" runat="server"></asp:TextBox></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                                    </table>
                                                         <br />
                                                        <div class="pull-right" style="margin-right: 0px">
                                                            <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="81px" Text="Add Row" OnClick="btnAddRow_Click" />
                                                        </div>
                                                        <br />
                                                        <br />
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>

                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>





            <asp:Panel ID="pnlFileUploads" runat="server" Visible="false">
                <div class="row-fluid">
                    <div class="widget-body">
                        <div class="span10">

                            <%-- <div class="span12">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="Label10" runat="server" Text="Upload Consumption Sheet:" CssClass="text-left"></asp:Label><asp:Label runat="server" ID="redAsterisk"><span style="font-weight: 700; color: #CC0000">*</span></asp:Label></label>
                                        <div class="controls controls-row">
                                            <asp:Label ID="Label18" runat="server" Text="&nbsp;"></asp:Label><br />

                                            <asp:FileUpload ID="FileUpload1" onchange="javascript:updateFileList()" runat="server" AllowMultiple="true" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="FileUpload1"><span style="font-weight: 700; color: #CC0000">Select file(s) to upload.</span></asp:RequiredFieldValidator>
                                            <br />
                                            <asp:Label ID="lblShowFileNames" runat="server" Text="" Width="600px"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>

                            <div class="span12" style="margin-left: 0px;">
                                <div class="control-row" style="padding-top: 0px;">
                                    <asp:Button ID="btnSave" runat="server" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSaveConsumptionSheet_Click" />
                                    <asp:Button ID="btnUpdate" runat="server" Visible="false" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdateConsumptionSheet_Click" />
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>

            </asp:Panel>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btnUpdate" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
    <script>
        updateFileList = function () {
            debugger

            var input = document.getElementById('cpMain_FileUpload1');
            var output = document.getElementById('cpMain_lblShowFileNames');

            output.innerHTML = '<br />Selected Files:<br />';
            output.innerHTML += '<ul>';

            for (var i = 0; i < input.files.length; ++i) {
                output.innerHTML += '<li>' + input.files.item(i).name + '</li>';
            }
            output.innerHTML += '</ul>';

        }
    </script>
</asp:Content>
