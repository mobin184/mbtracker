﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class ViewOrders : System.Web.UI.Page
    {
        DataTable dtOrderStatus;
        DataTable dtOrders;
        DataTable dtSizeInfo;

        OrderManager orderManager = new OrderManager();
        CommonManager commonManager = new CommonManager();
        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableEditButton();
                var user = CommonMethods.SessionInfo;

                if (user != null)
                {
                    BindData("Shipmentsin60days");
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteOperation();
                }
            }
        }

        private void EnableDisableEditButton()
        {
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewOrders", 1, 1);
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewOrders", 3,1);

        }

        string OrderDisplayOption
        {
            set { ViewState["orderDisplayOption"] = value; }
            get
            {
                try
                {
                    return ViewState["orderDisplayOption"].ToString();
                }
                catch
                {
                    return "";
                }
            }
        }

        int BuyerId
        {
            set { ViewState["buyerId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["buyerId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        string StyleName
        {
            set { ViewState["styleName"] = value; }
            get
            {
                try
                {
                    return ViewState["styleName"].ToString();
                }
                catch
                {
                    return "";
                }
            }
        }

        int OrderId
        {
            set { ViewState["OrderId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["OrderId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            try
            {
                OrderId = Convert.ToInt32(e.CommandArgument.ToString());
                
                var IsDeletable = true;
                if (IsDeletable)
                {
                    var title = "Warning";
                    var msg = "Are you sure you want to delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                }
                else
                {
                    var title = "Warning";
                    var msg = "Sorry, you can not delete it.<br />There was time & action plans for this booking.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }
        protected void BindData(string selectedValue)
        {

            if (selectedValue == "Buyer")
            {
                if (ddlBuyers.SelectedValue != "")
                {
                    dtOrders = orderManager.GetOrdersByBuyerId(Convert.ToInt32(ddlBuyers.SelectedValue));
                    LoadDataToControl();
                    LabelWidgetHeader.Text = "Orders of Buyer: " + ddlBuyers.SelectedItem.Text;
                }
                else
                {
                    if (BuyerId != 0)
                    {
                        dtOrders = orderManager.GetOrdersByBuyerId(BuyerId);
                        LoadDataToControl();

                        DataTable dt = buyerManager.GetBuyerById(BuyerId);
                        LabelWidgetHeader.Text = "Orders of Buyer: " + dt.Rows[0][1].ToString();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please select a buyer.')", true);
                    }

                }
            }
            else if (selectedValue == "Style")
            {

                if (tbxStyle.Text != "")
                {
                    dtOrders = orderManager.GetOrdersByStyleName(tbxStyle.Text, CommonMethods.SessionInfo.UserId);
                    LoadDataToControl();
                    LabelWidgetHeader.Text = "Orders of Style: " + tbxStyle.Text;
                }
                else
                {

                    if (StyleName != "")
                    {
                        dtOrders = orderManager.GetOrdersByStyleName(StyleName, CommonMethods.SessionInfo.UserId);
                        LoadDataToControl();
                        LabelWidgetHeader.Text = "Orders of Style: " + StyleName;

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please enter the style.')", true);
                    }


                }

            }
            else
            {
                dtOrders = orderManager.GetOrders(CommonMethods.SessionInfo.UserId);
                LoadDataToControl();
                LabelWidgetHeader.Text = "Orders - Shipments in 60 days:";
            }
        }

        protected void LoadDataToControl()
        {

            if (dtOrders.Rows.Count > 0)
            {
                dtOrderStatus = commonManager.LoadOrderStatusDropdown(CommonMethods.SessionInfo.DepartmentId);
                rptOrders.DataSource = dtOrders;
                rptOrders.DataBind();
                lblNoOrderFound.Visible = false;
            }
            else
            {
                rptOrders.DataSource = null;
                rptOrders.DataBind();
                lblNoOrderFound.Visible = true;

            }

        }

        protected void ddlOrderDisplayOptions_SelectedIndexChanged(object sender, EventArgs e)
        {

            pnlOrderSearch.Visible = false;
            ddlBuyers.Visible = false;
            tbxStyle.Visible = false;
            lblDisplayInput.Visible = false;
            //btnViewOrders.Visible = false;

            if (ddlOrderDisplayOptions.SelectedValue == "Buyer")
            {
                lblDisplayInput.Text = "Select buyer: ";
                lblDisplayInput.Visible = true;
                ddlBuyers.Visible = true;
                //btnViewOrders.Visible = true;
                pnlOrderSearch.Visible = true;
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);


            }
            else if (ddlOrderDisplayOptions.SelectedValue == "Style")
            {
                lblDisplayInput.Text = "Enter style: ";
                lblDisplayInput.Visible = true;
                tbxStyle.Visible = true;
                //btnViewOrders.Visible = true;
                pnlOrderSearch.Visible = true;
            }
            else
            {
                BindData(ddlOrderDisplayOptions.SelectedValue);
                OrderDisplayOption = ddlOrderDisplayOptions.SelectedValue;
            }
        }

        protected void btnViewOrders2_Click(object sender, EventArgs e)
        {
            OrderDisplayOption = ddlOrderDisplayOptions.SelectedValue;

            if (OrderDisplayOption == "Buyer")
            {
                BuyerId = Convert.ToInt32(ddlBuyers.SelectedValue);
            }
            else if (OrderDisplayOption == "Style")
            {
                StyleName = tbxStyle.Text;
            }
            BindData(ddlOrderDisplayOptions.SelectedValue);
            pnlOrderSearch.Visible = false;
            ddlOrderDisplayOptions.SelectedIndex = 0;
            pnlDetails.Visible = false;
            divBasicInfo.Visible = false;
        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            int orderId = Convert.ToInt32(e.CommandArgument.ToString());
            Response.Redirect("AddNewOrderV2.aspx?orderId=" + Tools.UrlEncode(orderId+""));
        }

        protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        {
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            int orderId = int.Parse(commandArgs[0]);
            var buyerName = commandArgs[1];
            var style = commandArgs[2];
            var orderNumber = commandArgs[3];
            //int orderId = Convert.ToInt32(e.CommandArgument.ToString());
            PupulateOrderForView(orderId);
            pnlDetails.Visible = true;
            lblColorInfoDetails.Text = $"Color & Size Information <br />Order Number: <span style=''>{orderNumber}</span><br /> Buyer: <span style=''>{buyerName}</span> <br /> Style:<span style=''>{style}</span>";
            lblColorInfoDetails.Visible = true;
            lblOrderInfo.Text = $"Details information <br />Order Number: <span style=''>{orderNumber}</span> <br /> Buyer: <span style=''>{buyerName}</span><br /> Style:<span style=''>{style}</span>";
            
             ScriptManager.RegisterStartupScript(this,this.GetType(), "message", "scrollDown()", true);
        }

        private void PupulateOrderForView(int orderId)
        {
            //unitOfWork = new UnitOfWork(); 
            var orderInfo = unitOfWork.GenericRepositories<Orders>().GetByID(orderId);
            //var orderColors = unitOfWork.GenericRepositories<OrderColors>().Get(x => x.OrderId == orderId).ToList(); ;
            //var dtColors = Tools.ToDataTable(orderColors);
            var dtColors = orderManager.GetOrderColorsByOrderId(orderId);
            var dtBasicInfo = unitOfWork.GetDataTableFromSql($"Exec usp_GetOrderInfoByOrderId {orderInfo.Id}");
            var dtOrderDocuments = unitOfWork.GetDataTableFromSql($"SELECT Id, FileName FROM OrderDocuments WHERE OrderId={orderInfo.Id}");
            BuyerId = orderInfo.BuyerId;
            var dtCountries = unitOfWork.GetDataTableFromSql($"EXEC usp_GetDeliveryCountries_ByOrderId {orderId}");
            SetDeliveryCounties(dtCountries);
            SetBasicInfo(dtBasicInfo);            
            PopulateOrderDocuments(dtOrderDocuments);           
            loadSizeInfo();
            if(dtColors.Rows.Count>0)
            {
                rptColorInfo.DataSource = dtColors;
                rptColorInfo.DataBind();
                lblNoDataFoundForColor.Visible = false;
                rptColorInfo.Visible = true;
            }
            else
            {
                rptColorInfo.Visible = false;
                lblNoDataFoundForColor.Visible = true;
            }
        }

        private void SetDeliveryCounties(DataTable dtCountries)
        {
            lblDeliveryCountry.Visible = true;
            if (dtCountries.Rows.Count > 0)
            {
                rptDeliveryCountries.DataSource = dtCountries;
                rptDeliveryCountries.DataBind();
                rptDeliveryCountries.Visible = true;
                lblNoCountryFound.Visible = false;
            }
            else
            {
                rptDeliveryCountries.DataSource = null;
                rptDeliveryCountries.DataBind();
                rptDeliveryCountries.Visible = false;
                lblNoCountryFound.Visible = true;
            }
        }

        private void PopulateOrderDocuments(DataTable dtOrderDocuments)
        {
            lblOrderDocuments.Visible = true;
            if (dtOrderDocuments.Rows.Count > 0)
            {
                lblNoDocumnet.Visible = false;
                rptOrderDocument.Visible = true;
                rptOrderDocument.DataSource = dtOrderDocuments;
                rptOrderDocument.DataBind();
            }
            else
            {
                lblNoDocumnet.Visible = true;
                rptOrderDocument.Visible = false ;
            }
        }

        private void SetBasicInfo(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                lblOrderInfo.Visible = true;
                divBasicInfo.Visible = true;
                //lblBasicInfoNotFound.Visible = false;
                lblBuyer.Text = dt.Rows[0].Field<string>("BuyerName");
                lblDepartment.Text = dt.Rows[0].Field<string>("DepartmentName");
                lblStyle.Text = dt.Rows[0].Field<string>("StyleName");
                lblSeason.Text = dt.Rows[0].Field<string>("SeasonName");
                lblYear.Text = dt.Rows[0].Field<string>("SeasonYear");
                lblOrderNumber.Text = dt.Rows[0].Field<string>("OrderNumber");
                lblOrderDate.Text = dt.Rows[0].Field<DateTime>("OrderDate").ToString("dd-MM-yyyy");
                lblOrderItem.Text = dt.Rows[0].Field<string>("OrderItem");
                lblLcVlaue.Text = dt.Rows[0].Field<Decimal>("LCValue").ToString();
                lblOrderStatus.Text = dt.Rows[0].Field<string>("OrderStatusName");
                //lblDeleveryCountry.Text = dt.Rows[0].Field<string>("DeleveryCountry");
                lblPaymentTerm.Text = dt.Rows[0].Field<string>("PaymentTerms");
            }
            else
            {
                lblOrderInfo.Visible = true;
                divBasicInfo.Visible = false;
                //lblBasicInfoNotFound.Visible = true;
            }
            
        }

        private void loadSizeInfo()
        {
            dtSizeInfo = buyerManager.GetSizes(BuyerId);
        }

        protected void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                UnitOfWork unitOfWork = new UnitOfWork();
                var orderId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Id").ToString());
                var IsExist = unitOfWork.GenericRepositories<viewOrderCantEditDelete>().IsExist(x => x.OrderId == orderId);
                if (IsExist)
                {
                    var btnEdit = (LinkButton)e.Item.FindControl("lnkbtnEdit");
                    btnEdit.Attributes["title"] = "The order cannot be updated, it has related entity.";
                    btnEdit.Attributes["disabled"] = "disabled";
                }
            }

        }

       
        protected void gvSizeInfo_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                loadSizeInfo();
                 var unitOfWork = new UnitOfWork();
                Label lblSizeId = null;
                Label lblSizeQty = null;
                for (int i = 0; i < dtSizeInfo.Rows.Count; i++)
                {
                    lblSizeQty = new Label();
                    lblSizeQty.ID = "txtSizeQty2" + i.ToString();
                    lblSizeQty.Text = "";
                    lblSizeQty.Width = 60;
                    


                    lblSizeId = new Label();
                    lblSizeId.ID = "lblSizeId2" + i.ToString();
                    lblSizeId.Text = dtSizeInfo.Rows[i][0].ToString();
                    lblSizeId.Visible = false;
                    e.Row.Cells[i].Controls.Add(lblSizeId);

                    //var lblOrderId = (Label)((GridView)sender).DataItemContainer.FindControl("lblOrderId");
                    var lblOrderColorId = (Label)((GridView)sender).DataItemContainer.FindControl("lblColorId");
                    //var orderId = int.Parse(lblOrderId.Text);
                    var colorId = int.Parse(lblOrderColorId.Text);
                    var sizeId = int.Parse(dtSizeInfo.Rows[i][0].ToString());

                    //var pInfo = unitOfWork.GenericRepositories<OrderColorSizes>().Get(x => x.OrderColorId == colorId && x.SizeId == sizeId).FirstOrDefault();
                    //if (pInfo != null)
                    //{
                    //    lblSizeQty.Text = pInfo.Quantity + "";
                    //}
                    e.Row.Cells[i].Controls.Add(lblSizeQty);
                }
            }

        }

        protected void rptColorInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var gvColorSizes = (GridView)e.Item.FindControl("gvSizeInfo");
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);
                loadSizeInfo();
                if (dtSizeInfo.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizeInfo.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizeInfo.Rows[i][1].ToString();
                        gvColorSizes.Columns.Add(templateField);

                    }
                }
                gvColorSizes.DataSource = dtOneRow;
                gvColorSizes.DataBind();
            }
        }

        private void DeleteOperation()
        {
            try
            {
                var PoOrderInfo = unitOfWork.GenericRepositories<Orders>().GetByID(OrderId);
                
                unitOfWork.GenericRepositories<Orders>().Delete(OrderId);
                unitOfWork.Save();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "returnToPage('ViewOrders.aspx');", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }


    }
}