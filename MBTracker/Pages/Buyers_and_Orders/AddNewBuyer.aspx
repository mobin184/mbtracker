﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddNewBuyer.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.AddNewBuyer" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid">
        <%--<div class="span2"></div>--%>
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="roleActionTitle" Text="Add a New Buyer"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal no-margin">
                        <div style="text-align: right; font-size: 12px; line-height: 50px; padding-bottom: 7px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Buyer Name<span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxBuyerName" required="true" CssClass="span12 form-control" type="text" placeholder="Buyer Name"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Buyer Description
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxDescription" CssClass="span12 form-control" TextMode="MultiLine" placeholder="Buyer Description"></asp:TextBox>
                            </div>
                        </div>

                        <div class="control-group">
                            <br />
                            <div class="controls-row">
                                <asp:Button ID="btnSubmit" runat="server" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnSave_Click" />
                                <asp:Button ID="btnUpdate" runat="server" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" Visible="false" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnUpdate_Click" />
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>


        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>Existing Buyers
                    </div>
                </div>
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
                            <asp:Repeater ID="rptBuyers" runat="server" OnItemCommand="rptBuyer_ItemCommand">
                                <HeaderTemplate>
                                    <thead>
                                        <tr>
                                            <th style="width: 35%">Buyer Name</th>
                                            <th style="width: 35%">Description</th>
                                            <th style="width: 30%" class="hidden-phone">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="gradeX">
                                        <td style="text-align: left"><%#Eval("BuyerName") %> </td>
                                        <td style="text-align: left"><%#Eval("BuyerInfo") %> </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("BuyerId") %>' class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>&nbsp;&nbsp;
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" Visible='<%#ViewState["deleteEnabled"]%>' CommandName="Delete" CommandArgument='<%#Eval("BuyerId") %>' class="btn btn-danger btn-small hidden-phone" Text="Delete"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
