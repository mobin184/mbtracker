﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using Repositories;
using MBTracker.Code_Folder.Productions;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class GetFile : System.Web.UI.Page
    {

        OrderManager orderManager = new OrderManager();
        ProductionManager productionManager = new ProductionManager();


        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get the file id from the query string
            int fileId = Convert.ToInt16(Request.QueryString["ID"]);
            string type = Request.QueryString["Type"];
            // Get the file from the database
            DataTable file = GetAFile(fileId, type);
            DataRow row = file.Rows[0];

            string name = (string)row["FileName"];
            string contentType = (string)row["ContentType"];
            Byte[] data = (Byte[])row["FileData"];

            // Send the file to the browser
            Response.AddHeader("Content-type", contentType);
            Response.AddHeader("Content-Disposition", "attachment; filename=" + name);
            Response.BinaryWrite(data);
            Response.Flush();
            Response.End();
        }


        // Get a file from the database by ID
        public DataTable GetAFile(int fileId, string type)
        {
            DataTable file = null;
            if (type == "ConsumptionSheet")
            {
                file = orderManager.GetConsumptionSheetFile(fileId);
            }
            else if (type == "ReportedProblem")
            {
                file = productionManager.GetReportedProblemFile(fileId);
            }
            else if (type == "OrderDocument")
            {
                file = unitOfWork.GetDataTableFromSql($"Exec [usp_GetOrderDocument_ByDocumentId] {fileId}");
            }
            else if (type == "TNAPlanSamplePicture")
            {
                file = unitOfWork.GetDataTableFromSql($"SELECT [Id],[FileName],[ContentType] ,[FileSize],[FileData] FROM TimeAndActionPlanSamplePictures WHERE Id ={fileId}");
            }
            else if (type == "TNAPlanDyeingOrderFile")
            {
                file = unitOfWork.GetDataTableFromSql($"SELECT [Id],[FileName],[ContentType] ,[FileSize],[FileData] FROM TimeAndActionPlanDyeingOrderDocuments WHERE Id={fileId}");
            }
            return file;
        }



    }
}