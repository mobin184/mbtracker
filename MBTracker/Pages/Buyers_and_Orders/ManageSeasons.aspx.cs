﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class ManageSeasons : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableDeleteButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                //CommonMethods.LoadDropdown(ddlSeasonYears, "SeasonYears ORDER BY SeasonYear", 1, 0);
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteSeason(BuyerSeasonId);
                }
            }
        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");

            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ManageSeasons", 2, 1);

        }
        int BuyerSeasonId
        {
            set { ViewState["seasonId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["seasonId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue != "")
            {

                BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            else
            {
                pnlSeasons.Visible = false;
            }
        }

        private void BindData(int buyerId)
        {

            DataTable dt = new DataTable();

            dt = buyerManager.LoadSeasonsByBuyer(buyerId);
            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                pnlSeasons.Visible = true;
            }
            else
            {
                pnlSeasons.Visible = false;
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {


            int seasonId = buyerManager.InsertSeason(tbxSeasonName.Text, Convert.ToInt32(ddlBuyers.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now);

            if (seasonId > 0)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Season name exists.')", true);
            }


            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearStyleName();


        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int returnValue = buyerManager.UpdateBuyerSeason(BuyerSeasonId, tbxSeasonName.Text, Convert.ToInt32(ddlBuyers.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now);
            if (returnValue > 0)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Season update was failed.')", true);
            }


            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearStyleName();

            sizeActionTitle.Text = "Add a New Season:";
            btnSave.Visible = true;
            btnUpdate.Visible = false;

        }


        private void ClearStyleName()
        {
            tbxSeasonName.Text = string.Empty;

        }


        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)

        {

            if (e.CommandName == "Edit")
            {
                int buyerSeasonId = Convert.ToInt32(e.CommandArgument.ToString());
                PopulateSeason(buyerSeasonId);
            }
            else if (e.CommandName == "Delete")
            {
                BuyerSeasonId = Convert.ToInt32(e.CommandArgument.ToString());
                var seasonCannotBeDeleted = unitOfWork.GenericRepositories<viewSeasonsCanNotBeDeleted>().GetByID(BuyerSeasonId);
                if (seasonCannotBeDeleted == null)
                {
                    var title = "Warning";
                    var msg = "Are you sure you want to delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                }
                else
                {
                    var title = "Warning";
                    var msg = "Sorry, you can not delete it.<br />There was some dependencies of this season.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                }
            }


        }

        private void PopulateSeason(int buyerSeasonId)
        {
            DataTable dt = buyerManager.GetSeasonById(buyerSeasonId);
            if (dt.Rows.Count > 0)
            {
                BuyerSeasonId = Convert.ToInt32(dt.Rows[0]["Id"]);
                tbxSeasonName.Text = dt.Rows[0]["SeasonName"].ToString();
                ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, Convert.ToInt32(dt.Rows[0]["BuyerId"]));
                //ddlSeasonYears.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSeasonYears, Convert.ToInt32(dt.Rows[0]["YearId"]));

                sizeActionTitle.Text = "Update Season:";
                btnSave.Visible = false;
                btnUpdate.Visible = true;
            }


        }


        private void DeleteSeason(int buyerSeasonId)
        {

            int returnValue = buyerManager.DeleteBuyerSeason(buyerSeasonId, CommonMethods.SessionInfo.UserName, DateTime.Now);

            if (returnValue > 0)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Season deletion was failed.')", true);
            }

            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearStyleName();


            sizeActionTitle.Text = "Add a New Season:";
            btnSave.Visible = true;
            btnUpdate.Visible = false;

        }


    }
}