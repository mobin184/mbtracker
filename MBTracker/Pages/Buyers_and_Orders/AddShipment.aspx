﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddShipment.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.AddShipment" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    
    <div class="row-fluid">
        <div class="span9">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="actionTitle" Text="Setup Shipment Date:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="pull-right" style="text-align: center; font-size: 12px; line-height: 20px; padding-bottom: 20px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="control-group">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="lblOrder" runat="server" Text="Select Order:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlOrders_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlOrders"><span style="font-weight: 700; color: #CC0000">Please select an order.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblShippingDate" runat="server" Text="Shipment Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxShippingDate" runat="server" placeholder="Enter Shipment Date" CssClass="form-control" Width="100%" TextMode="Date"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="tbxShippingDate"><span style="font-weight: 700; color: #CC0000">Enter shipment date</span></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblShipmentMode" runat="server" Text="Shipment Mode:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlShipmentMode" runat="server" Display="Dynamic" Width="100%" CssClass="form-control">
                                        <asp:ListItem Value="Sea">Sea</asp:ListItem>
                                        <asp:ListItem Value="Air">Air</asp:ListItem>
                                        <asp:ListItem Value="Sea & Air">Sea & Air</asp:ListItem>
                                    </asp:DropDownList><br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlShipmentMode"><span style="font-weight: 700; color: #CC0000">Please select shipment mode.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblShimentCancelDate" runat="server" Text="Cancel Date (if any):"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxShipmentCancelDate" runat="server" placeholder="Enter Shipment Cancel Date (if any)" CssClass="form-control" Width="100%" TextMode="Date"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group hidden">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblInspecAvailDate" runat="server" Text="Inspection Date (if any):"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxInspecAvailDate" runat="server" placeholder="Inspection Availabity Date (if any)" CssClass="form-control" Width="100%" TextMode="Date"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="controls controls-row">
                            <asp:Label ID="lblColorOrDeliveryCountryNotFound" runat="server" Text="" Visible="false" BackColor="#ffff00"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="span12" style="margin-left: 0px" runat="server" id="pnlColorDeliveryCountryAndQuantity" visible="false">
            <div class="widget">
                <div class="widget-body">
                    <asp:Panel ID="pnl" runat="server">
                        <%--<div class="form-horizontal">--%>
                        <div class="control-group">
                            <label for="inputDeliveryCountry" class="control-label">
                                <asp:Label ID="lblColorAndSize" runat="server" Text="Shipment Information:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                        </div>
                        <div class="control-group">
                            <label for="inputDeliveryCountry" class="control-label"></label>
                            <div class="controls controls-row">
                                <div id="dt_example2" class="example_alt_pagination" style="overflow-x: auto">
                                    <asp:Repeater ID="rptColorAndDeliveryCountries" runat="server" OnItemDataBound="rptColorAndDeliveryCountries_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center">
                                                            <asp:Label ID="lblColorDesc" runat="server" Text="Color"></asp:Label></th>
                                                        <th style="text-align: center">
                                                            <asp:Label ID="lblQuantity" runat="server" Text="Order Qty"></asp:Label></th>
                                                        <th style="text-align: center">
                                                            <asp:Label ID="lblShipmentScheduled" runat="server" Text="Already Scheduled Qty"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblColorDescription" runat="server" Text="Delivery Country, Size & Quantity"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="vertical-align: middle">
                                                    <asp:Label ID="lblColorDesc" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ColorDescription")%>'></asp:Label>
                                                    <asp:Label ID="lblColorId" Visible="false" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>'></asp:Label>
                                                    <asp:Label ID="lblStyleId" runat="server" Visible="false" Text=''></asp:Label>
                                                </td>
                                                <td style="vertical-align: middle">
                                                    <asp:Label ID="lblOrderQty" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "OrderQuantity")%>'></asp:Label></td>
                                                <td style="vertical-align: middle">
                                                    <asp:Label ID="lblShipmentAlreadyScheduled" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ScheduledShipment")%>'></asp:Label>
                                                    <asp:Label ID="lblShipmentInThisDate" runat="server" Text="" Visible="false"></asp:Label>

                                                </td>
                                                <td>
                                                    <asp:Repeater ID="rptDeliveryCountryAndQty" runat="server" OnItemDataBound="rptDeliveryCountryAndQty_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="data-table" class="table table-bordered table-hover">
                                                                <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td style="vertical-align: middle">
                                                                    <asp:Label ID="lblDeliveryCountryId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryCountryId")%>'></asp:Label><asp:Label ID="lblDeliveryCountryName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryCountryCode")%>'></asp:Label>

                                                                </td>
                                                                <td>
                                                                    <asp:GridView ID="gvSizeAndQty" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeAndQty_RowCreated"></asp:GridView>
                                                                </td>
                                                                <td style="vertical-align: middle">
                                                                    <asp:Label ID="lblCountryTotal" runat="server" Text="Total: 0"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody>
                                                                </table>
                                                                <div class="clearfix">
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                    <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>'></asp:Label>


                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>



                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label12" runat="server" Text="&nbsp;" CssClass="text-left"></asp:Label></label>
                            <div class="controls controls-row">
                                <%-- <div style="text-align: right; font-size: 12px; line-height: 30px; padding-bottom: 0px">
                                    <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                </div>--%>

                                <asp:LinkButton ID="lnkbtnCalculateSizeTotal" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="Calculate Total" OnClick="lnkbtnCalculateSizeTotal_Click"></asp:LinkButton>
                                <asp:Button ID="btnSaveShipment" runat="server" class="btn btn-success btn-samll pull-left btnStyle" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSave_Click" />
                                <asp:Button ID="btnUpdateShipment" runat="server" Visible="false" class="btn btn-success btn-samll pull-left btnStyle" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdateShipment_Click" />

                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>

    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">


</asp:Content>
