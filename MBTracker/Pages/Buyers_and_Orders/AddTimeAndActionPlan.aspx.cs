﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class AddTimeAndActionPlan : System.Web.UI.Page
    {

        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                CommonMethods.LoadDropdown(ddlYear, "Years WHERE IsActive = 1", 1, 0);
                if (Request.QueryString["TNAId"] != null)
                {
                    TNAId = int.Parse(Tools.UrlDecode(Request.QueryString["TNAId"]));
                    ddlStyles.Enabled = false;
                    ddlBuyers.Enabled = false;
                    ddlYear.Enabled = false;
                    //btnUpdate.Visible = true;
                    btnSave.Visible = false;
                    var tnaInfo = unitOfWork.GenericRepositories<TimeAndActionPlans>().GetByID(TNAId);
                    BindStylesByBuyer(tnaInfo.BuyerId);
                    ddlBuyers.SelectedValue = tnaInfo.BuyerId + "";
                    ddlStyles.SelectedValue = tnaInfo.StyleId + "";
                    ddlYear.SelectedValue = tnaInfo.YearId + "";

                    SetInitialRowCount(rpt);
                    pnlTimeAndActionInfo.Visible = true;
                    sizeActionTitle.Text = "Update Time & Action Plans";
                    //BindData(tnaInfo);
                }
            }
        }

        //private void BindData(TimeAndActionPlans tna)
        //{
        //    DataTable dt = new DataTable();
        //    DataRow dr = null;
        //    dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Column0", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Column1", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Column2", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Column3", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Column4", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Column5", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Column6", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Column7", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Column8", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Column9", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Column10", typeof(string)));


        //    dr = dt.NewRow();
        //    dr["RowNumber"] = 1;
        //    dr["Column0"] = tna.Quantity + "";
        //    dr["Column1"] = tna.DeliveryDate+"";
        //    dr["Column2"] = tna.YarnInHouseDate+"";
        //    dr["Column3"] = tna.PPApprovalDate + "";
        //    dr["Column4"] = tna.SampleApprovalDate + "";
        //    dr["Column5"] = tna.KnittingStartDeadlineDate + "";
        //    dr["Column6"] = tna.AdditionalWorkAccessoriesInHouseDate + "";
        //    dr["Column7"] = tna.PackingAccessoriesInHouseDate + "";
        //    dr["Column8"] = tna.LabelInHouseDate + "";
        //    dr["Column9"] = tna.CartonInHouseDate + "";
        //    dr["Column10"] = tna.OtherInHouseDate + "";
        //    dt.Rows.Add(dr);

        //    //Store the DataTable in ViewState
        //    ViewState[rpt.UniqueID] = dt;

        //    rpt.DataSource = dt;
        //    rpt.DataBind();
        //}

        int TNAId
        {
            set { ViewState["tnaId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["tnaId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            else
            {
                ddlStyles.Items.Clear();
            }

            pnlTimeAndActionInfo.Visible = false;
            pnlSummary.Visible = false;

        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlStyles.SelectedValue != "")
            {
                ShowEntryInfo();
            }
            else
            {
                pnlTimeAndActionInfo.Visible = false;
                pnlSummary.Visible = false;
            }
        }

        private void ShowEntryInfo()
        {
            var year = ddlYear.SelectedValue;
            var style = ddlStyles.SelectedValue;
            if (year != "" && style != "")
            {
                var yearId = int.Parse(ddlYear.SelectedValue);
                var styleId = int.Parse(ddlStyles.SelectedValue);
                var bookedQty = unitOfWork.GenericRepositories<Booking>().Get(x => x.StyleId == styleId && x.SeasonYearId == yearId).Sum(x => x.BookingQuantity);
                var bookinginfo = unitOfWork.GenericRepositories<Booking>().Get(x => x.StyleId == styleId && x.SeasonYearId == yearId).FirstOrDefault();
                var yarnComposition = "";
                lblBookedQuantity.Text = bookedQty + "";
                if (bookinginfo != null)
                {
                    yarnComposition = bookinginfo.YarnComposition ?? "";
                }
                lblYarnConsumptionCountAndPly.Text = yarnComposition;
                SetInitialRowCount(rpt);
                pnlTimeAndActionInfo.Visible = true;
                pnlSummary.Visible = true;
            }
            else
            {
                pnlTimeAndActionInfo.Visible = false;
                pnlSummary.Visible = false;
            }
        }


        private void SetInitialRowCount(Repeater rpt)
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Column0", typeof(string)));
            dt.Columns.Add(new DataColumn("Column1", typeof(string)));
            dt.Columns.Add(new DataColumn("Column2", typeof(string)));
            dt.Columns.Add(new DataColumn("Column3", typeof(string)));
            dt.Columns.Add(new DataColumn("Column4", typeof(string)));
            dt.Columns.Add(new DataColumn("Column5", typeof(string)));
            dt.Columns.Add(new DataColumn("Column6", typeof(string)));
            dt.Columns.Add(new DataColumn("Column7", typeof(string)));
            dt.Columns.Add(new DataColumn("Column8", typeof(string)));
            dt.Columns.Add(new DataColumn("Column9", typeof(string)));
            dt.Columns.Add(new DataColumn("Column10", typeof(string)));

            dt.Columns.Add(new DataColumn("Column11", typeof(string)));
            dt.Columns.Add(new DataColumn("Column12", typeof(string)));
            dt.Columns.Add(new DataColumn("Column13", typeof(string)));
            dt.Columns.Add(new DataColumn("Column14", typeof(string)));
            dt.Columns.Add(new DataColumn("Column15", typeof(string)));



            dr = dt.NewRow();
            dr["RowNumber"] = 1;
            dr["Column0"] = string.Empty;
            dr["Column1"] = string.Empty;
            dr["Column2"] = string.Empty;
            dr["Column3"] = string.Empty;
            dr["Column4"] = string.Empty;
            dr["Column5"] = string.Empty;
            dr["Column6"] = string.Empty;
            dr["Column7"] = string.Empty;
            dr["Column8"] = string.Empty;
            dr["Column9"] = string.Empty;
            dr["Column10"] = string.Empty;

            dr["Column11"] = string.Empty;
            dr["Column12"] = string.Empty;
            dr["Column13"] = string.Empty;
            dr["Column14"] = string.Empty;
            dr["Column15"] = string.Empty;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState[rpt.UniqueID] = dt;

            rpt.DataSource = dt;
            rpt.DataBind();
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid(rpt);
        }


        private void AddNewRowToGrid(Repeater rpt)
        {
            int rowIndex;
            if (ViewState[rpt.UniqueID] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState[rpt.UniqueID];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {
                        TextBox tbxQuantity = (TextBox)rpt.Items[rowIndex].FindControl("tbxQuantity");
                        TextBox tbxDeliveryDate = (TextBox)rpt.Items[rowIndex].FindControl("tbxDeliveryDate");
                        TextBox tbxYarnInHouseDate = (TextBox)rpt.Items[rowIndex].FindControl("tbxYarnInHouseDate");
                        TextBox tbxPPApprovalDate = (TextBox)rpt.Items[rowIndex].FindControl("tbxPPApprovalDate");
                        TextBox tbxSampleApprovalDate = (TextBox)rpt.Items[rowIndex].FindControl("tbxSampleApprovalDate");
                        TextBox tbxKnittingStartDeadlineDate = (TextBox)rpt.Items[rowIndex].FindControl("tbxKnittingStartDeadlineDate");

                        TextBox tbxmsclabelInshouseDate = (TextBox)rpt.Items[rowIndex].FindControl("tbxmsclabelInshouseDate");
                        TextBox tbxhpTagInHouseDate = (TextBox)rpt.Items[rowIndex].FindControl("tbxhpTagInHouseDate");
                        TextBox tbxbzInHouseDate = (TextBox)rpt.Items[rowIndex].FindControl("tbxbzInHouseDate");
                        TextBox tbxbffInHouseDate = (TextBox)rpt.Items[rowIndex].FindControl("tbxbffInHouseDate");

                        TextBox tbxpeInhouseDate = (TextBox)rpt.Items[rowIndex].FindControl("tbxpeInhouseDate");
                        TextBox tbxssTagInhouseDate = (TextBox)rpt.Items[rowIndex].FindControl("tbxssTagInhouseDate");
                        TextBox tbxPloyInhouseDate = (TextBox)rpt.Items[rowIndex].FindControl("tbxPloyInhouseDate");
                        TextBox tbxCartoonInhouseDate = (TextBox)rpt.Items[rowIndex].FindControl("tbxCartoonInhouseDate");
                        TextBox tbxAdditionalWorkAccInHouseDate = (TextBox)rpt.Items[rowIndex].FindControl("tbxAdditionalWorkAccInHouseDate");
                        TextBox tbxOtherDeadlineDate = (TextBox)rpt.Items[rowIndex].FindControl("tbxOtherDeadlineDate");


                        dtCurrentTable.Rows[rowIndex]["Column0"] = tbxQuantity.Text;
                        dtCurrentTable.Rows[rowIndex]["Column1"] = tbxDeliveryDate.Text;
                        dtCurrentTable.Rows[rowIndex]["Column2"] = tbxYarnInHouseDate.Text;
                        dtCurrentTable.Rows[rowIndex]["Column3"] = tbxPPApprovalDate.Text;
                        dtCurrentTable.Rows[rowIndex]["Column4"] = tbxSampleApprovalDate.Text;
                        dtCurrentTable.Rows[rowIndex]["Column5"] = tbxKnittingStartDeadlineDate.Text;


                        dtCurrentTable.Rows[rowIndex]["Column6"] = tbxmsclabelInshouseDate.Text;
                        dtCurrentTable.Rows[rowIndex]["Column7"] = tbxhpTagInHouseDate.Text;
                        dtCurrentTable.Rows[rowIndex]["Column8"] = tbxbzInHouseDate.Text;
                        dtCurrentTable.Rows[rowIndex]["Column9"] = tbxbffInHouseDate.Text;


                        dtCurrentTable.Rows[rowIndex]["Column10"] = tbxpeInhouseDate.Text;
                        dtCurrentTable.Rows[rowIndex]["Column11"] = tbxssTagInhouseDate.Text;
                        dtCurrentTable.Rows[rowIndex]["Column12"] = tbxPloyInhouseDate.Text;
                        dtCurrentTable.Rows[rowIndex]["Column13"] = tbxCartoonInhouseDate.Text;


                        dtCurrentTable.Rows[rowIndex]["Column14"] = tbxAdditionalWorkAccInHouseDate.Text;
                        dtCurrentTable.Rows[rowIndex]["Column15"] = tbxOtherDeadlineDate.Text;


                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["RowNumber"] = rowIndex + 1;

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState[rpt.UniqueID] = dtCurrentTable;

                    rpt.DataSource = dtCurrentTable;
                    rpt.DataBind();
                }
                else
                {
                    DataTable dt = new DataTable();
                    DataRow dr = null;
                    dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column0", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column1", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column2", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column3", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column4", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column5", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column6", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column7", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column8", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column9", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column10", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column11", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column12", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column13", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column14", typeof(string)));
                    dt.Columns.Add(new DataColumn("Column15", typeof(string)));


                    dr = dt.NewRow();
                    dr["RowNumber"] = 1;
                    dr["Column0"] = string.Empty;
                    dr["Column1"] = string.Empty;
                    dr["Column2"] = string.Empty;
                    dr["Column3"] = string.Empty;
                    dr["Column4"] = string.Empty;
                    dr["Column5"] = string.Empty;
                    dr["Column6"] = string.Empty;
                    dr["Column7"] = string.Empty;
                    dr["Column8"] = string.Empty;
                    dr["Column9"] = string.Empty;
                    dr["Column10"] = string.Empty;

                    dr["Column11"] = string.Empty;
                    dr["Column12"] = string.Empty;
                    dr["Column13"] = string.Empty;
                    dr["Column14"] = string.Empty;
                    dr["Column15"] = string.Empty;


                    dt.Rows.Add(dr);

                    ViewState[rpt.UniqueID] = dt;
                    rpt.DataSource = dt;
                    rpt.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData(rpt);

        }


        private void SetPreviousData(Repeater rpt)
        {

            if (ViewState[rpt.UniqueID] != null)
            {
                DataTable dt = (DataTable)ViewState[rpt.UniqueID];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox tbxQuantity = (TextBox)rpt.Items[i].FindControl("tbxQuantity");
                        TextBox tbxDeliveryDate = (TextBox)rpt.Items[i].FindControl("tbxDeliveryDate");
                        TextBox tbxYarnInHouseDate = (TextBox)rpt.Items[i].FindControl("tbxYarnInHouseDate");
                        TextBox tbxPPApprovalDate = (TextBox)rpt.Items[i].FindControl("tbxPPApprovalDate");
                        TextBox tbxSampleApprovalDate = (TextBox)rpt.Items[i].FindControl("tbxSampleApprovalDate");
                        TextBox tbxKnittingStartDeadlineDate = (TextBox)rpt.Items[i].FindControl("tbxKnittingStartDeadlineDate");

                        TextBox tbxmsclabelInshouseDate = (TextBox)rpt.Items[i].FindControl("tbxmsclabelInshouseDate");
                        TextBox tbxhpTagInHouseDate = (TextBox)rpt.Items[i].FindControl("tbxhpTagInHouseDate");
                        TextBox tbxbzInHouseDate = (TextBox)rpt.Items[i].FindControl("tbxbzInHouseDate");

                        TextBox tbxbffInHouseDate = (TextBox)rpt.Items[i].FindControl("tbxbffInHouseDate");
                        TextBox tbxpeInhouseDate = (TextBox)rpt.Items[i].FindControl("tbxpeInhouseDate");
                        TextBox tbxssTagInhouseDate = (TextBox)rpt.Items[i].FindControl("tbxssTagInhouseDate");

                        TextBox tbxPloyInhouseDate = (TextBox)rpt.Items[i].FindControl("tbxPloyInhouseDate");
                        TextBox tbxCartoonInhouseDate = (TextBox)rpt.Items[i].FindControl("tbxCartoonInhouseDate");
       
                        TextBox tbxAdditionalWorkAccInHouseDate = (TextBox)rpt.Items[i].FindControl("tbxAdditionalWorkAccInHouseDate");
                        TextBox tbxOtherDeadlineDate = (TextBox)rpt.Items[i].FindControl("tbxOtherDeadlineDate");



                        tbxQuantity.Text = dt.Rows[i]["Column0"].ToString();
                        tbxDeliveryDate.Text = dt.Rows[i]["Column1"].ToString();
                        tbxYarnInHouseDate.Text = dt.Rows[i]["Column2"].ToString();
                        tbxPPApprovalDate.Text = dt.Rows[i]["Column3"].ToString();
                        tbxSampleApprovalDate.Text = dt.Rows[i]["Column4"].ToString();
                        tbxKnittingStartDeadlineDate.Text = dt.Rows[i]["Column5"].ToString();

                        tbxmsclabelInshouseDate.Text = dt.Rows[i]["Column6"].ToString();
                        tbxhpTagInHouseDate.Text = dt.Rows[i]["Column7"].ToString();
                        tbxbzInHouseDate.Text = dt.Rows[i]["Column8"].ToString();

                        tbxbffInHouseDate.Text = dt.Rows[i]["Column9"].ToString();
                        tbxpeInhouseDate.Text = dt.Rows[i]["Column10"].ToString();
                        tbxssTagInhouseDate.Text = dt.Rows[i]["Column11"].ToString();

                        tbxPloyInhouseDate.Text = dt.Rows[i]["Column12"].ToString();
                        tbxCartoonInhouseDate.Text = dt.Rows[i]["Column13"].ToString();

                        tbxAdditionalWorkAccInHouseDate.Text = dt.Rows[i]["Column14"].ToString();
                        tbxOtherDeadlineDate.Text = dt.Rows[i]["Column15"].ToString();

                    }
                }
            }
        }


        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {
            var rpt = (Repeater)((LinkButton)sender).Parent.Parent;
            int rowIndex = Convert.ToInt32(e.CommandArgument.ToString());
            DataTable dtCurrentTable = (DataTable)ViewState[rpt.UniqueID];
            DataRow drDeleteRow = dtCurrentTable.Rows[rowIndex];

            drDeleteRow.Delete();
            dtCurrentTable.AcceptChanges();

            ViewState[rpt.UniqueID] = dtCurrentTable;
            rpt.DataSource = dtCurrentTable;
            rpt.DataBind();
            SetPreviousData(rpt);
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlYear.SelectedValue != "")
            {
                ShowEntryInfo();
            }
            else
            {
                pnlTimeAndActionInfo.Visible = false;
                pnlSummary.Visible = false;
            }
        }

        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (TNAId > 0)
                {
                    var tnaInfo = unitOfWork.GenericRepositories<TimeAndActionPlans>().GetByID(TNAId);

                    TextBox tbxQuantity = (TextBox)e.Item.FindControl("tbxQuantity");
                    TextBox tbxDeliveryDate = (TextBox)e.Item.FindControl("tbxDeliveryDate");
                    TextBox tbxYarnInHouseDate = (TextBox)e.Item.FindControl("tbxYarnInHouseDate");
                    TextBox tbxPPApprovalDate = (TextBox)e.Item.FindControl("tbxPPApprovalDate");
                    TextBox tbxSampleApprovalDate = (TextBox)e.Item.FindControl("tbxSampleApprovalDate");
                    TextBox tbxKnittingStartDeadlineDate = (TextBox)e.Item.FindControl("tbxKnittingStartDeadlineDate");


                    TextBox tbxmsclabelInshouseDate = (TextBox)e.Item.FindControl("tbxmsclabelInshouseDate");
                    TextBox tbxhpTagInHouseDate = (TextBox)e.Item.FindControl("tbxhpTagInHouseDate");
                    TextBox tbxbzInHouseDate = (TextBox)e.Item.FindControl("tbxbzInHouseDate");

                    TextBox tbxbffInHouseDate = (TextBox)e.Item.FindControl("tbxbffInHouseDate");
                    TextBox tbxpeInhouseDate = (TextBox)e.Item.FindControl("tbxpeInhouseDate");
                    TextBox tbxssTagInhouseDate = (TextBox)e.Item.FindControl("tbxssTagInhouseDate");

                    TextBox tbxPloyInhouseDate = (TextBox)e.Item.FindControl("tbxPloyInhouseDate");
                    TextBox tbxCartoonInhouseDate = (TextBox)e.Item.FindControl("tbxCartoonInhouseDate");

                    TextBox tbxAdditionalWorkAccInHouseDate = (TextBox)e.Item.FindControl("tbxAdditionalWorkAccInHouseDate");
                    TextBox tbxOtherDeadlineDate = (TextBox)e.Item.FindControl("tbxOtherDeadlineDate");
                    LinkButton lnkbtnDelete = (LinkButton)e.Item.FindControl("lnkbtnDelete");


                    lnkbtnDelete.Visible = false;
                    Button btnUpdate = (Button)e.Item.FindControl("btnUpdate");
                    btnUpdate.Visible = true;

                    tbxQuantity.Text = tnaInfo.Quantity + "";
                    tbxDeliveryDate.Text = tnaInfo.DeliveryDate?.ToString("yyyy-MM-dd");
                    tbxYarnInHouseDate.Text = tnaInfo.YarnInHouseDate?.ToString("yyyy-MM-dd");
                    tbxPPApprovalDate.Text = tnaInfo.PPApprovalDate?.ToString("yyyy-MM-dd");
                    tbxSampleApprovalDate.Text = tnaInfo.SampleApprovalDate?.ToString("yyyy-MM-dd");
                    tbxKnittingStartDeadlineDate.Text = tnaInfo.KnittingStartDeadlineDate?.ToString("yyyy-MM-dd");


                    tbxmsclabelInshouseDate.Text = tnaInfo.MainSizeAndCareLabelInHouseDate?.ToString("yyyy-MM-dd");
                    tbxhpTagInHouseDate.Text = tnaInfo.HangAndPriceTagInHouseDate?.ToString("yyyy-MM-dd");
                    tbxbzInHouseDate.Text = tnaInfo.ButtonAndZipperInHouseDate?.ToString("yyyy-MM-dd");


                    tbxbffInHouseDate.Text = tnaInfo.BadgeFlowersAndFabricInHouseDate?.ToString("yyyy-MM-dd");
                    tbxpeInhouseDate.Text = tnaInfo.PrintAndEmbroideryInHouseDate?.ToString("yyyy-MM-dd");
                    tbxssTagInhouseDate.Text = tnaInfo.SewingShoulderTagInHouseDate?.ToString("yyyy-MM-dd");

                    tbxPloyInhouseDate.Text = tnaInfo.PolyInHouseDate?.ToString("yyyy-MM-dd");
                    tbxCartoonInhouseDate.Text = tnaInfo.CartonInHouseDate?.ToString("yyyy-MM-dd");

                    tbxAdditionalWorkAccInHouseDate.Text = tnaInfo.AdditionalWorkAccessoriesInHouseDate?.ToString("yyyy-MM-dd");
                    tbxOtherDeadlineDate.Text = tnaInfo.OtherDeadlineDate?.ToString("yyyy-MM-dd");

                }
            }
            if (e.Item.ItemType == ListItemType.Footer)
            {
                if (TNAId > 0)
                {
                    ((Button)e.Item.FindControl("btnAddRow")).Visible = false;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var buyerId = int.Parse(ddlBuyers.SelectedValue);
                var styleId = int.Parse(ddlStyles.SelectedValue);
                var yearId = int.Parse(ddlYear.SelectedValue);
                for (int i = 0; i < rpt.Items.Count; i++)
                {

                    TextBox tbxQuantity = (TextBox)rpt.Items[i].FindControl("tbxQuantity");
                    TextBox tbxDeliveryDate = (TextBox)rpt.Items[i].FindControl("tbxDeliveryDate");
                    TextBox tbxYarnInHouseDate = (TextBox)rpt.Items[i].FindControl("tbxYarnInHouseDate");
                    TextBox tbxPPApprovalDate = (TextBox)rpt.Items[i].FindControl("tbxPPApprovalDate");
                    TextBox tbxSampleApprovalDate = (TextBox)rpt.Items[i].FindControl("tbxSampleApprovalDate");
                    TextBox tbxKnittingStartDeadlineDate = (TextBox)rpt.Items[i].FindControl("tbxKnittingStartDeadlineDate");
                    TextBox tbxAdditionalWorkAccInHouseDate = (TextBox)rpt.Items[i].FindControl("tbxAdditionalWorkAccInHouseDate");


                    TextBox tbxmsclabelInshouseDate = (TextBox)rpt.Items[i].FindControl("tbxmsclabelInshouseDate");
                    TextBox tbxhpTagInHouseDate = (TextBox)rpt.Items[i].FindControl("tbxhpTagInHouseDate");
                    TextBox tbxbzInHouseDate = (TextBox)rpt.Items[i].FindControl("tbxbzInHouseDate");

                    TextBox tbxbffInHouseDate = (TextBox)rpt.Items[i].FindControl("tbxbffInHouseDate");
                    TextBox tbxpeInhouseDate = (TextBox)rpt.Items[i].FindControl("tbxpeInhouseDate");
                    TextBox tbxssTagInhouseDate = (TextBox)rpt.Items[i].FindControl("tbxssTagInhouseDate");

                    TextBox tbxPloyInhouseDate = (TextBox)rpt.Items[i].FindControl("tbxPloyInhouseDate");
                    TextBox tbxCartoonInhouseDate = (TextBox)rpt.Items[i].FindControl("tbxCartoonInhouseDate");

                    TextBox tbxOtherDeadlineDate = (TextBox)rpt.Items[i].FindControl("tbxOtherDeadlineDate");
                    TextBox tbxNote = (TextBox)rpt.Items[i].FindControl("tbxNote");

                    var tna = new TimeAndActionPlans();
                    tna.BuyerId = buyerId;
                    tna.StyleId = styleId;
                    tna.YearId = yearId;

                    tna.Quantity = int.Parse(string.IsNullOrEmpty(tbxQuantity.Text) ? "0" : tbxQuantity.Text);
                    tna.DeliveryDate = string.IsNullOrEmpty(tbxDeliveryDate.Text) ? (DateTime?)null : DateTime.Parse(tbxDeliveryDate.Text);
                    tna.YarnInHouseDate = string.IsNullOrEmpty(tbxYarnInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxYarnInHouseDate.Text);
                    tna.PPApprovalDate = string.IsNullOrEmpty(tbxPPApprovalDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPPApprovalDate.Text);
                    tna.SampleApprovalDate = string.IsNullOrEmpty(tbxSampleApprovalDate.Text) ? (DateTime?)null : DateTime.Parse(tbxSampleApprovalDate.Text);
                    tna.KnittingStartDeadlineDate = string.IsNullOrEmpty(tbxKnittingStartDeadlineDate.Text) ? (DateTime?)null : DateTime.Parse(tbxKnittingStartDeadlineDate.Text);
                    tna.AdditionalWorkAccessoriesInHouseDate = string.IsNullOrEmpty(tbxAdditionalWorkAccInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxAdditionalWorkAccInHouseDate.Text);
                    tna.MainSizeAndCareLabelInHouseDate = string.IsNullOrEmpty(tbxmsclabelInshouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxmsclabelInshouseDate.Text);
                    tna.HangAndPriceTagInHouseDate = string.IsNullOrEmpty(tbxhpTagInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxhpTagInHouseDate.Text);
                    tna.ButtonAndZipperInHouseDate = string.IsNullOrEmpty(tbxbzInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxbzInHouseDate.Text);
                    tna.BadgeFlowersAndFabricInHouseDate = string.IsNullOrEmpty(tbxbffInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxbffInHouseDate.Text);
                    tna.PrintAndEmbroideryInHouseDate = string.IsNullOrEmpty(tbxpeInhouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxpeInhouseDate.Text);
                    tna.SewingShoulderTagInHouseDate = string.IsNullOrEmpty(tbxssTagInhouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxssTagInhouseDate.Text);
                    tna.PolyInHouseDate = string.IsNullOrEmpty(tbxPloyInhouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPloyInhouseDate.Text);
                    tna.CartonInHouseDate = string.IsNullOrEmpty(tbxCartoonInhouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxCartoonInhouseDate.Text);
                    tna.OtherDeadlineDate = string.IsNullOrEmpty(tbxOtherDeadlineDate.Text) ? (DateTime?)null : DateTime.Parse(tbxOtherDeadlineDate.Text);


                    tna.CreatedBy = CommonMethods.SessionInfo.UserName;
                    tna.CreateDate = DateTime.Now;


                    //var buyerId = int.Parse(ddlBuyers.SelectedValue);
                    //var styleId = int.Parse(ddlStyles.SelectedValue);
                    //var tnaId = int.Parse(ddlTNAs.SelectedValue);



                    if (!string.IsNullOrEmpty(tbxNote.Text))
                    {
                        var tnaNote = new TimeAndActionPlans_Notes();
                        tnaNote.TNAId = tna.Id;
                        tnaNote.Notes = tbxNote.Text.ToString();
                        tnaNote.CreatedBy = CommonMethods.SessionInfo.UserName;
                        tnaNote.CreateDate = DateTime.Now;
                        tna.TimeAndActionPlans_Notes.Add(tnaNote);
                    }

                    unitOfWork.GenericRepositories<TimeAndActionPlans>().Insert(tna);

                }
                unitOfWork.Save();

                //CommonMethods.SendEventNotification(11, buyerId, styleId, null, tnaId);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "reloadPage();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                var tna = unitOfWork.GenericRepositories<TimeAndActionPlans>().GetByID(TNAId);

                TextBox tbxQuantity = (TextBox)rpt.Items[0].FindControl("tbxQuantity");
                TextBox tbxDeliveryDate = (TextBox)rpt.Items[0].FindControl("tbxDeliveryDate");
                TextBox tbxYarnInHouseDate = (TextBox)rpt.Items[0].FindControl("tbxYarnInHouseDate");
                TextBox tbxPPApprovalDate = (TextBox)rpt.Items[0].FindControl("tbxPPApprovalDate");
                TextBox tbxSampleApprovalDate = (TextBox)rpt.Items[0].FindControl("tbxSampleApprovalDate");
                TextBox tbxKnittingStartDeadlineDate = (TextBox)rpt.Items[0].FindControl("tbxKnittingStartDeadlineDate");
                TextBox tbxAdditionalWorkAccInHouseDate = (TextBox)rpt.Items[0].FindControl("tbxAdditionalWorkAccInHouseDate");



                TextBox tbxmsclabelInshouseDate = (TextBox)rpt.Items[0].FindControl("tbxmsclabelInshouseDate");
                TextBox tbxhpTagInHouseDate = (TextBox)rpt.Items[0].FindControl("tbxhpTagInHouseDate");
                TextBox tbxbzInHouseDate = (TextBox)rpt.Items[0].FindControl("tbxbzInHouseDate");

                TextBox tbxbffInHouseDate = (TextBox)rpt.Items[0].FindControl("tbxbffInHouseDate");
                TextBox tbxpeInhouseDate = (TextBox)rpt.Items[0].FindControl("tbxpeInhouseDate");
                TextBox tbxssTagInhouseDate = (TextBox)rpt.Items[0].FindControl("tbxssTagInhouseDate");

                TextBox tbxPloyInhouseDate = (TextBox)rpt.Items[0].FindControl("tbxPloyInhouseDate");
                TextBox tbxCartoonInhouseDate = (TextBox)rpt.Items[0].FindControl("tbxCartoonInhouseDate");

                TextBox tbxOtherDeadlineDate = (TextBox)rpt.Items[0].FindControl("tbxOtherDeadlineDate");

                TextBox tbxNote = (TextBox)rpt.Items[0].FindControl("tbxNote");



                //tna.Quantity = int.Parse(string.IsNullOrEmpty(tbxQuantity.Text) ? "0" : tbxQuantity.Text);
                //tna.DeliveryDate = string.IsNullOrEmpty(tbxDeliveryDate.Text) ? (DateTime?)null : DateTime.Parse(tbxDeliveryDate.Text);
                //tna.YarnInHouseDate = string.IsNullOrEmpty(tbxYarnInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxYarnInHouseDate.Text);
                //tna.PPApprovalDate = string.IsNullOrEmpty(tbxPPApprovalDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPPApprovalDate.Text);
                //tna.SampleApprovalDate = string.IsNullOrEmpty(tbxSampleApprovalDate.Text) ? (DateTime?)null : DateTime.Parse(tbxSampleApprovalDate.Text);
                //tna.KnittingStartDeadlineDate = string.IsNullOrEmpty(tbxKnittingStartDeadlineDate.Text) ? (DateTime?)null : DateTime.Parse(tbxKnittingStartDeadlineDate.Text);
                //tna.AdditionalWorkAccessoriesInHouseDate = string.IsNullOrEmpty(tbxAdditionalWorkAccInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxAdditionalWorkAccInHouseDate.Text);
                //tna.PackingAccessoriesInHouseDate = string.IsNullOrEmpty(tbxPackingAccInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPackingAccInHouseDate.Text);
                //tna.LabelInHouseDate = string.IsNullOrEmpty(tbxLabelInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxLabelInHouseDate.Text);
                //tna.CartonInHouseDate = string.IsNullOrEmpty(tbxCartonInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxCartonInHouseDate.Text);
                //tna.OtherDeadlineDate = string.IsNullOrEmpty(tbxOtherDeadlineDate.Text) ? (DateTime?)null : DateTime.Parse(tbxOtherDeadlineDate.Text);

                tna.Quantity = int.Parse(string.IsNullOrEmpty(tbxQuantity.Text) ? "0" : tbxQuantity.Text);
                tna.DeliveryDate = string.IsNullOrEmpty(tbxDeliveryDate.Text) ? (DateTime?)null : DateTime.Parse(tbxDeliveryDate.Text);
                tna.YarnInHouseDate = string.IsNullOrEmpty(tbxYarnInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxYarnInHouseDate.Text);
                tna.PPApprovalDate = string.IsNullOrEmpty(tbxPPApprovalDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPPApprovalDate.Text);
                tna.SampleApprovalDate = string.IsNullOrEmpty(tbxSampleApprovalDate.Text) ? (DateTime?)null : DateTime.Parse(tbxSampleApprovalDate.Text);
                tna.KnittingStartDeadlineDate = string.IsNullOrEmpty(tbxKnittingStartDeadlineDate.Text) ? (DateTime?)null : DateTime.Parse(tbxKnittingStartDeadlineDate.Text);
                tna.AdditionalWorkAccessoriesInHouseDate = string.IsNullOrEmpty(tbxAdditionalWorkAccInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxAdditionalWorkAccInHouseDate.Text);
                tna.MainSizeAndCareLabelInHouseDate = string.IsNullOrEmpty(tbxmsclabelInshouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxmsclabelInshouseDate.Text);
                tna.HangAndPriceTagInHouseDate = string.IsNullOrEmpty(tbxhpTagInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxhpTagInHouseDate.Text);
                tna.ButtonAndZipperInHouseDate = string.IsNullOrEmpty(tbxbzInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxbzInHouseDate.Text);
                tna.BadgeFlowersAndFabricInHouseDate = string.IsNullOrEmpty(tbxbffInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxbffInHouseDate.Text);
                tna.PrintAndEmbroideryInHouseDate = string.IsNullOrEmpty(tbxpeInhouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxpeInhouseDate.Text);
                tna.SewingShoulderTagInHouseDate = string.IsNullOrEmpty(tbxssTagInhouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxssTagInhouseDate.Text);
                tna.PolyInHouseDate = string.IsNullOrEmpty(tbxPloyInhouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPloyInhouseDate.Text);
                tna.CartonInHouseDate = string.IsNullOrEmpty(tbxCartoonInhouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxCartoonInhouseDate.Text);
                tna.OtherDeadlineDate = string.IsNullOrEmpty(tbxOtherDeadlineDate.Text) ? (DateTime?)null : DateTime.Parse(tbxOtherDeadlineDate.Text);


                tna.CreatedBy = CommonMethods.SessionInfo.UserName;
                tna.CreateDate = DateTime.Now;

                if (!string.IsNullOrEmpty(tbxNote.Text))
                {
                    var tnaNote = new TimeAndActionPlans_Notes();
                    tnaNote.TNAId = tna.Id;
                    tnaNote.Notes = tbxNote.Text.ToString();
                    tnaNote.CreatedBy = CommonMethods.SessionInfo.UserName;
                    tnaNote.CreateDate = DateTime.Now;
                    tna.TimeAndActionPlans_Notes.Add(tnaNote);
                }            

                unitOfWork.GenericRepositories<TimeAndActionPlans>().Update(tna);
                unitOfWork.Save();


                CommonMethods.SendEventNotification(5, int.Parse(ddlBuyers.SelectedValue), int.Parse(ddlStyles.SelectedValue), null, TNAId);


                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewTimeAndActionPlans.aspx');", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }
    }
}