﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using Repositories;
using MBTracker.EF;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class ViewShipments : System.Web.UI.Page
    {


        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        DataTable dtShipmentColors;
        DataTable dtOrderColors;

        int selectedOrderValue = 0;
        int shipmentDateId;
        int BuyerColorId;
        int deliveryCountryId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableEditButton();
                EnableDisableDeleteButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);

                if (Request.QueryString["showInfo"] != null)
                {
                    var shipmentId = int.Parse(Tools.UrlDecode(Request.QueryString["showInfo"]));
                    var shipmentInfo = unitOfWork.GenericRepositories<OrderShipmentDates>().GetByID(shipmentId);
                    var orderInfo = unitOfWork.GenericRepositories<Orders>().GetByID(shipmentInfo.OrderId);

                    if (CommonMethods.HasBuyerAssignedToUser(orderInfo.BuyerId, CommonMethods.SessionInfo.UserId))
                    {
                        BindStylesByBuyer(orderInfo.BuyerId);
                        BindOrdersByStyle(orderInfo.StyleId);

                        ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, orderInfo.BuyerId);
                        ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, orderInfo.StyleId);
                        ddlOrders.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrders, orderInfo.Id);
                        ddlBuyers.Enabled = false;
                        ddlStyles.Enabled = false;
                        ddlOrders.Enabled = false;

                        viewDetails();
                    }
                    else
                    {
                        lblNotAuthorizedToView.Visible = true;
                        divBuyerAndStyle.Visible = false;
                    }


                }
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteShipment();
                }
            }

        }

        private void DeleteShipment()
        {
            try
            {
                var lstShipmentColorCountrySizes = unitOfWork.GenericRepositories<ShipmentColorCountrySizes>().Get(x => x.OrderShipmentDateId == ShipmentId).ToList();
                foreach (var item in lstShipmentColorCountrySizes)
                {
                    unitOfWork.GenericRepositories<ShipmentColorCountrySizes>().Delete(item);
                }
                unitOfWork.GenericRepositories<OrderShipmentDates>().Delete(ShipmentId);
                unitOfWork.Save();
                pnlColorDeliveryCountryAndQuantity.Visible = true;
                pnlShipmentSummary.Visible = true;
                //LoadOrderColors();
                PopulateShipmentDates();
                PopulateSummary();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }
        }

        protected void viewDetails()
        {
            pnlColorDeliveryCountryAndQuantity.Visible = true;
            pnlShipmentSummary.Visible = true;
            //LoadOrderColors();
            PopulateShipmentDates();
            PopulateSummary();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);
        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");

            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewShipments", 2, 1);

        }

        private void EnableDisableEditButton()
        {
            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Merchandiser,QCE");
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewShipments", 1, 1);
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {


            ddlStyles.Items.Clear();
            ddlOrders.Items.Clear();
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            pnlShipmentSummary.Visible = false;


            if (ddlBuyers.SelectedValue != "")
            {

                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            //else
            //{
            //    ddlStyles.Items.Clear();
            //    ddlOrders.Items.Clear();
            //    pnlColorDeliveryCountryAndQuantity.Visible = false;
            //    pnlShipmentSummary.Visible = false;
            //}
        }

        private void BindStylesByBuyer(int buyerId)
        {

            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);

        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlOrders.Items.Clear();
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            pnlShipmentSummary.Visible = false;

            if (ddlStyles.SelectedValue != "")
            {

                BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
            }
            //else
            //{
            //    ddlOrders.Items.Clear();
            //    pnlColorDeliveryCountryAndQuantity.Visible = false;
            //    pnlShipmentSummary.Visible = false;
            //}


        }

        protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlOrders.SelectedValue != "")
            //{

            //    //pnlColorDeliveryCountryAndQuantity.Visible = true;
            //}
            //else
            //{
               pnlColorDeliveryCountryAndQuantity.Visible = false;
                pnlShipmentSummary.Visible = false;
            //}


        }

        private void BindOrdersByStyle(int styleId)
        {

            CommonMethods.LoadOrderDropdownByStyle(ddlOrders, styleId, 1, 0);

        }

        protected void btnViewShipment_Click(object sender, EventArgs e)
        {

            if (ddlOrders.SelectedValue != "")
            {

                pnlColorDeliveryCountryAndQuantity.Visible = true;
                pnlShipmentSummary.Visible = true;
               // LoadOrderColors();
                PopulateShipmentDates();
                PopulateSummary();

            }
            else
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Please select an order number.')", true);
            }
        }

        //private void LoadOrderColors()
        //{

        //    if (ddlOrders.SelectedValue != "")
        //    {
        //        selectedOrderValue = Convert.ToInt32(ddlOrders.SelectedValue);
        //    }

        //    dtOrderColors = orderManager.GetShipmentColorsByShipmentDateId(selectedOrderValue);

        //}

        private void GetShipmentColorsByShipmentDateId(int orderShipmentDateId)
        {
            dtShipmentColors = orderManager.GetShipmentColorsByShipmentDateId(orderShipmentDateId);

        }


        private void PopulateShipmentDates()
        {

            DataTable dt = new DataTable();
            if (ddlOrders.SelectedValue != "")
            {
                selectedOrderValue = Convert.ToInt32(ddlOrders.SelectedValue);
            }

            dt = orderManager.GetOrderShipmentDatesByOrderId(selectedOrderValue);

            if(dt.Rows.Count > 0)
            {
                rptOrderShipmentDates.DataSource = dt;
                rptOrderShipmentDates.DataBind();
                rptOrderShipmentDates.Visible = true;
                pnlColorDeliveryCountryAndQuantity.Visible = true ;
            }
            else
            {
                rptOrderShipmentDates.DataSource = null;
                rptOrderShipmentDates.DataBind();
                rptOrderShipmentDates.Visible = false;
                pnlColorDeliveryCountryAndQuantity.Visible = false;
            }
           
        }


        private void PopulateSummary()
        {
            DataTable dt = ShipmentSummaryTable();
            int shipmentQuantity = 0;

            for (int i = 0; i < rptOrderShipmentDates.Items.Count; i++)
            {
                shipmentQuantity = 0;
                Label lblShipmenDatetId = (Label)rptOrderShipmentDates.Items[i].FindControl("lblShipmenDatetId");
                Label lblShipmentDate = (Label)rptOrderShipmentDates.Items[i].FindControl("lblShipmentDate");
                Repeater rptOrderColors = (Repeater)rptOrderShipmentDates.Items[i].FindControl("rptOrderColors");
                for (int j = 0; j < rptOrderColors.Items.Count; j++)
                {
                    Repeater rptShipmentCountry = (Repeater)rptOrderColors.Items[j].FindControl("rptShipmentCountry");
                    for (int k = 0; k < rptShipmentCountry.Items.Count; k++)
                    {
                        //Repeater rptSizeAndQty = (Repeater)rptShipmentCountry.Items[k].FindControl("rptSizeAndQty");
                        //for (int l = 0; l < rptSizeAndQty.Items.Count; l++)
                        //{
                        //    Label lblSizeQuantity = (Label)rptSizeAndQty.Items[l].FindControl("lblSizeQuantity");
                        //    if (lblSizeQuantity.Text != "")
                        //    {
                        //        shipmentQuantity += Convert.ToInt32(lblSizeQuantity.Text);
                        //    }
                        //}

                        GridView gvSizeAndQty = (GridView)rptShipmentCountry.Items[k].FindControl("gvSizeAndQty");
                        for (int l = 1; l <= gvSizeAndQty.Columns.Count; l++)
                        {
                            Label lblSizeQuantity = (Label)gvSizeAndQty.Rows[0].Cells[l-1].FindControl("lblSizeQuantity" + l);
                            if (lblSizeQuantity.Text != "")
                            {
                                shipmentQuantity += Convert.ToInt32(string.IsNullOrEmpty(lblSizeQuantity.Text)? "0" : lblSizeQuantity.Text);
                            }
                        }
                    }

                }

                dt.Rows.Add(lblShipmenDatetId.Text,lblShipmentDate.Text, shipmentQuantity);
            }

            if(dt.Rows.Count > 0)
            {
                rptShipmentSummary.DataSource = dt;
                rptShipmentSummary.DataBind();
                rptShipmentSummary.Visible = true;
                lblNoShipmentDataFound.Visible = false;
                pnlShipmentSummary.Visible = true;

                var total = dt.Compute("Sum(ShipmentQuantity)", "").ToString();
                (rptShipmentSummary.Controls[rptShipmentSummary.Controls.Count - 1].Controls[0].FindControl("lblShipmentSummaryTotal") as Label).Text = total;
            }
            else
            {
                rptShipmentSummary.DataSource = null;
                rptShipmentSummary.DataBind();
                rptShipmentSummary.Visible = false;
                lblNoShipmentDataFound.Visible = true;
                pnlShipmentSummary.Visible = false;
            }          
        }


        private DataTable ShipmentSummaryTable()
        {
            DataTable dt = new DataTable("ShipmentColorCountrySizes");
            dt.Columns.Add("Id", typeof(string));
            dt.Columns.Add("ShipmentDate", typeof(string));
            dt.Columns.Add("ShipmentQuantity", typeof(int));
            return dt;
        }



        protected void rptOrderShipmentDates_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptOrderColors = (Repeater)e.Item.FindControl("rptOrderColors");
                Label lblShipmentDateId = (Label)e.Item.FindControl("lblShipmenDatetId");
                shipmentDateId = Convert.ToInt32(lblShipmentDateId.Text);

                GetShipmentColorsByShipmentDateId(shipmentDateId);



                if (dtShipmentColors.Rows.Count > 0)
                {
                    
                    rptOrderColors.DataSource = dtShipmentColors;
                    rptOrderColors.DataBind();
                }
            }
        }

        

        protected void rptOrderColors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptShipmentCountry = (Repeater)e.Item.FindControl("rptShipmentCountry");

                Label lblOrderColorId = (Label)e.Item.FindControl("lblOrderColorId");
                BuyerColorId = Convert.ToInt32(lblOrderColorId.Text);

                DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetDeliveryCountries_ByShipmentAndColorForView {shipmentDateId},{BuyerColorId}");
                //orderManager.GetShipmentCountriesByDateAndColor(shipmentDateId, BuyerColorId);

                if (dt.Rows.Count > 0)
                {
                    rptShipmentCountry.DataSource = dt;
                    rptShipmentCountry.DataBind();
                }
            }
        }

        protected void rptShipmentCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                //Repeater rptSizeAndQty = (Repeater)e.Item.FindControl("rptSizeAndQty"); //

                Label lblDeliveryCountryId = (Label)e.Item.FindControl("lblDeliveryCountryId");
                deliveryCountryId = Convert.ToInt32(lblDeliveryCountryId.Text);

                //DataTable dt = orderManager.GetSizesAndQuantityByShipmentColorCountry(shipmentDateId, OrderColorId, deliveryCountryId); //

                //if (dt.Rows.Count > 0)
                //{
                //    rptSizeAndQty.DataSource = dt;
                //    rptSizeAndQty.DataBind();
                //}
                DataTable dtOneRow = new DataTable();
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                

                DataTable dt = orderManager.GetSizesAndQuantityByShipmentColorCountry(shipmentDateId, BuyerColorId, deliveryCountryId);

                for (int i = 0; i < dt.Rows.Count; i++)
                {                   
                    dtOneRow.Columns.Add(new DataColumn(dt.Rows[i][0].ToString(), typeof(string)));
                }
                DataRow dr = null;

                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                
                GridView gvSizeAndQty = (GridView)e.Item.FindControl("gvSizeAndQty");
                if (dt.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dt.Rows[i][0].ToString();
                        gvSizeAndQty.Columns.Add(templateField);
                        dr[dt.Rows[i][0].ToString()] = dt.Rows[i][1].ToString();
                    }
                }

                dtOneRow.Rows.Add(dr);
                gvSizeAndQty.DataSource = dtOneRow;
                gvSizeAndQty.DataBind();
                var countryTotal = dt.Compute("Sum(SizeQuantity)", "").ToString();
                Label lblCountryTotal = (Label)e.Item.FindControl("CountryTotal");
                lblCountryTotal.Text = countryTotal;
            }
        }

        protected void gvSizeAndQty_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //var index = sende
                Label lblSizeQuantity = null;
                //LoadYarnIssueForSizes();
                var dtSizes = (DataTable)((GridView)sender).DataSource;
                if(dtSizes!=null && dtSizes.Rows.Count > 0)
                {
                    for (int i = 1; i < dtSizes.Columns.Count; i++)
                    {

                        lblSizeQuantity = new Label();
                        lblSizeQuantity.ID = "lblSizeQuantity" + i.ToString();
                        lblSizeQuantity.Text = int.Parse(dtSizes.Rows[0][i].ToString()) > 0 ? dtSizes.Rows[0][i].ToString() : "";
                        lblSizeQuantity.Width = 50;
                        e.Row.Cells[i-1].Controls.Add(lblSizeQuantity);

                    }
                }
                
            }
        }

        int ShipmentId
        {
            set { ViewState["shipmentId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["shipmentId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            var shipmenDatetId = Convert.ToInt32(e.CommandArgument.ToString());            
            Response.Redirect("AddShipmentV3.aspx?shipmentId=" + Tools.UrlEncode(shipmenDatetId+""));
        }

        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            ShipmentId = Convert.ToInt32(e.CommandArgument.ToString());

            if (ShipmentId != 0)
            {
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
            }
            else
            {
                var title = "Error";
                var msg = "Sorry, you can not delete it.<br />Something went wrong.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            }
        }
    }
}