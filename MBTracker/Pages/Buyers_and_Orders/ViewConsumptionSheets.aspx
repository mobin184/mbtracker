﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewConsumptionSheets.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.ViewConsumptionSheets" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table > tbody > tr > td {
            vertical-align: middle;
        }
    </style>

    <div class="row-fluid" id="divBuyerAndStyle" runat="server">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="View Consumption Sheets:"></asp:Label>
                    </div>
                </div>
          
            <div class="widget-body">
                <div class="form-horizontal">

                    <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 10px">
                                   <%-- <span style="font-weight: 700; color: #CC0000">*</span> Required Field--%>
                                </div>

                    <div class="control-group">
                        <label for="inputBuyer" class="control-label">
                            <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                        <div class="controls controls-row">
                            <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="inputStyle" class="control-label">
                            <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                        <div class="controls controls-row">
                            <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" CssClass="form-control" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" ></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <%--<div class="control-group">
                        <label for="inputOrder" class="control-label">
                            <asp:Label ID="lblOrder" runat="server" Text="Select Order:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                        <div class="controls controls-row">
                            <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlOrders_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                ControlToValidate="ddlOrders"><span style="font-weight: 700; color: #CC0000">Please select an order.</span></asp:RequiredFieldValidator>
                        </div>
                    </div>--%>
                    <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="&nbsp;"></asp:Label></label>
                            <div class="controls controls-row">
                                <br />
                                <asp:Button ID="btnViewConsSheet" runat="server"  class="btn btn-success btn-midium pull-left btnStyle" Text="View Consumption Sheet" OnClick="btnViewConsSheet_Click" />
                            </div>
                         </div>
                </div>
            </div>
       
                </div>
    </div>
  </div>

     <label for="inputStyle" class="control-label" style="font-weight:bold; padding-top:100px; margin-left:200px; text-align:left">
        <asp:Label ID="lblNotAuthorizedToView" Font-Bold="true" Visible="false" Font-Size="Large" runat="server" Text="You are not authorized to view this information."></asp:Label>
     </label>

    <div class="row-fluid">
        <div class="span12">
            <asp:Label ID="lblNoConsumptionSheetFound" style="width:100%" runat="server" Visible="false" Text="Consumption sheet was not found." BackColor="#ffff00"></asp:Label>
            <asp:Panel ID="pnlViewConsumptionSheets" runat="server" Visible="false">
                
                <div class="widget">
                <div class="widget-body">
                    <div class="form-horizontal">
                       <%-- <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 50px">
                            <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                        </div>--%>
                        <div class="control-group" style="overflow-x:auto">
                           <label for="inputStyle" class="control-label" style="font-weight:bold; text-align:left">
                                <asp:Label ID="Label1" runat="server" style="font-weight:bold; text-align:left" Font-Bold="false" Text="Consumption Sheets:"></asp:Label></label>
                            <div class="controls-row">                                 
                                <asp:Repeater ID="rptConsumptionSheets" runat="server" OnItemDataBound="rptConsumptionSheets_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table-1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lblStage" runat="server" Text="Consumption </br>Stage "></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblMCBrand" runat="server" Text="Machine </br>Brand"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblMCGauge" runat="server" Text="Machine </br>Gauge"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblMCSpeed" runat="server" Text="Machine </br>Speed"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblKnittingTime" runat="server" Text="Knitting </br>Time"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblAverageWeight1" runat="server" Text="Average </br>Weight (Per dozen)"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblFiles" runat="server" Text="Consumption </br>Sheet Files"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="LabelAction" runat="server" Text="Take </br>Actions"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblConsumptionSheetId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "Id")%>'></asp:Label><asp:Label ID="lblStageInfo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Stage")%>' CssClass="text-center"></asp:Label></td>
                                            <td style="white-space:nowrap">
                                                <asp:Label ID="lblMachineBrand" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BrandName")%>' Width="60px" CssClass="text-center"></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblMCgg" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingMachineGauge")%>' Width="60px" CssClass="text-center"></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblMCSpd" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MachineSpeed")%>' Width="60px" CssClass="text-center"></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblKntTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingTime")%>' Width="60px" CssClass="text-center"></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblWeight" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AverageWight")%>' Width="60px" CssClass="text-center"></asp:Label></td>
                                            <td>
                                                <asp:Repeater ID="rptFiles" runat="server">
                                                    <HeaderTemplate>
                                                        <table id="data-table" class="table table-bordered table-hover" style="margin-bottom: 0px">
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="text-align: center">
                                                                <asp:Label ID="lblFileName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FileName")%>' Width="200px"></asp:Label></td>
                                                            <td style="text-align: center">
                                                                <asp:HyperLink runat="server" NavigateUrl='<%# Eval("Id", "GetFile.aspx?ID={0}&Type=ConsumptionSheet") %>' Text="Download"></asp:HyperLink></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                                            </table>
                                                            <div class="clearfix">
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                            <td style="vertical-align: middle; text-align:center; width=25%">
                                                <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" Visible='<%# ViewState["editEnabled"]%>' CommandArgument='<%#Eval("Id") %>' OnCommand="lnkbtnEdit_Command" class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>&nbsp;&nbsp;
                                                <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="View" CommandArgument='<%#Eval("Id") %>' OnCommand="lnkbtnView_Command" class="btn btn-success btn-small hidden-phone" Text="View Details"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>                               

                            </div>
                        </div>




                    </div>
                </div>
                </div>

            </asp:Panel>


        </div>
    </div>

    <div class="row-fluid">
        <div class="span12"> 
            <div class="span12">
                <asp:Panel ID="pnlYarnConsumption" runat="server" Visible="false">

                     <div class="widget">
                    <div class="widget-body">
                        <div class="form-horizontal">

                            <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            <div class="control-group">
                                <label for="inputYarnConsumption" class="control-label" style="text-align:left">
                                    <asp:Label ID="lblYarnConsumption"  runat="server" style="font-weight:bold; text-align:left" Text="Yarn Consumption:"></asp:Label></label>
                            </div>
                            <div class="controls-row">
                                <div id="dt_example" class="example_alt_pagination">
                                    <%--<label for="inputYarnConsumption" class="control-label">
                                    <asp:Label ID="lblYarnConsumption" Font-Bold="true" runat="server" Text="Yarn Consumption:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>--%>
                                    <asp:Repeater ID="rpt" runat="server" OnItemDataBound="rpt_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table-2" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="hidden">
                                                            <asp:Label ID="lblPlacement" runat="server" Text="Placement"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblYarnComposition" runat="server" Text="Yarn Composition"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblYarnCount" runat="server" Text="Yarn Count"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblPlyMainYarn" runat="server" Text="Ply (main yarn)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblPlyLycra" runat="server" Text="Ply (Lycra)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblAverageWeight" runat="server" Text="Average Weight(Per dozen)"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="hidden">
                                                 <%--   <asp:Label ID="lblSweatherPartsId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "Id")%>'></asp:Label>
                                                    <asp:Label ID="lblYarnConsumptionByPartsId" runat="server" Visible="false" Text=""></asp:Label>
                                                    <asp:Label ID="lblSweatherParts" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SweatherPartName")%>' Width="100px" CssClass="text-right"></asp:Label>--%>

                                                </td>
                                                <td>
                                                    <asp:Label ID="tbxYarnComposition" runat="server"></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="tbxYarnCount" runat="server"></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="tbxPlyMainYarn" runat="server"></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="tbxPlyLycra" runat="server"></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="tbxAverageWeight" runat="server"></asp:Label></td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
