﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Productions;
using MBTracker.EF;
using Repositories;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class AddTimeAndActionPlanNote : System.Web.UI.Page
    {

        UnitOfWork unitOfWork = new UnitOfWork();
        ProductionManager productionManager = new ProductionManager();


        protected void Page_Load(object sender, EventArgs e)
        {



            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                CommonMethods.LoadDropdown(ddlYear, "Years WHERE IsActive = 1", 1, 0);


                if (Request.QueryString["TNAId"] != null)
                {
                    TNAId = int.Parse(Request.QueryString["TNAId"]);

                    var tnaInfo = unitOfWork.GenericRepositories<TimeAndActionPlans>().GetByID(TNAId);
                    BindStylesByBuyer(tnaInfo.BuyerId);
                    ddlBuyers.SelectedValue = tnaInfo.BuyerId + "";
                    ddlStyles.SelectedValue = tnaInfo.StyleId + "";
                    ddlYear.SelectedValue = tnaInfo.YearId + "";

                    ShowTNAs();
                    ddlTNAs.SelectedValue = tnaInfo.Id + "";
                    BindData();
                    divNote.Visible = true;
                    ddlStyles.Enabled = false;
                    ddlBuyers.Enabled = false;
                    ddlYear.Enabled = false;
                    ddlTNAs.Enabled = false;

                }


            }
        }


        int TNAId
        {
            set { ViewState["tnaId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["tnaId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
               
            }
            else
            {
                ddlStyles.Items.Clear();
               
            }

        }

        private void BindStylesByBuyer(int buyerId)
        {

            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);

        }


        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {

         

            if (ddlStyles.SelectedValue != "")
            {

               
            }

        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowTNAs();
        }


        protected void ddlTNAs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTNAs.SelectedValue != "")
            {
               
                divNote.Visible = true;
                BindData();
            }
            else
            {
                divNote.Visible = false;

            }
        }



        private void ShowTNAs()
        {


            if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a buyer.')", true);
            }
            else if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select an style.')", true);
            }
            if (ddlYear.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a year.')", true);
            }
            else
            {
                var buyerId = string.IsNullOrEmpty(ddlBuyers.SelectedValue) ? (int?)null : Int32.Parse(ddlBuyers.SelectedValue);
                var styleId = string.IsNullOrEmpty(ddlStyles.SelectedValue) ? (int?)null : Int32.Parse(ddlStyles.SelectedValue);
                var yearId = int.Parse(ddlYear.SelectedValue);

                var userId = CommonMethods.SessionInfo.UserId;
                DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetAllTimeAndActionPlans '{buyerId}','{styleId}','{yearId}',{userId}");


                ddlTNAs.DataSource = dt;
                ddlTNAs.DataTextField = "DeliveryDate";
                ddlTNAs.DataValueField = "Id";
                ddlTNAs.DataBind();
                ddlTNAs.Items.Insert(0, new ListItem("---Select---", string.Empty));
                ddlTNAs.SelectedIndex = 0;

            }

        }

        private void BindData()
        {

            DataTable dt = new DataTable();

            dt = productionManager.GetTNANotes(int.Parse(ddlTNAs.SelectedValue));

            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                divNotes.Visible = true;
            }
            else
            {
                divNotes.Visible = false;
            }

        }



        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var buyerId = int.Parse(ddlBuyers.SelectedValue);
                var styleId = int.Parse(ddlStyles.SelectedValue);
                var tnaId = int.Parse(ddlTNAs.SelectedValue);
       


                var tnaNote = new TimeAndActionPlans_Notes();
                tnaNote.TNAId = tnaId;
                tnaNote.Notes = tbxNote.Text.ToString();
                tnaNote.CreatedBy = CommonMethods.SessionInfo.UserName;
                tnaNote.CreateDate = DateTime.Now;
                unitOfWork.GenericRepositories<TimeAndActionPlans_Notes>().Insert(tnaNote);

                unitOfWork.Save();
                CommonMethods.SendEventNotification(11, buyerId, styleId, null, tnaId);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "reloadPage();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }


        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

        }

    }
}