﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewShipments.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.ViewShipments" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid" id="divBuyerAndStyle" runat="server">
        <div class="span6">

            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="View Shipment Information"></asp:Label>
                    </div>
                </div>


                <div class="widget-body">
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 10px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblOrder" runat="server" Text="Select Order:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control" OnSelectedIndexChanged="ddlOrders_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </div>

                        <%--<div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label1" runat="server" Text="&nbsp;"></asp:Label></label>
                            <div class="controls controls-row">
                            </div>
                        </div>--%>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="&nbsp;"></asp:Label></label>
                            <div class="controls controls-row">
                                <br />
                                <asp:Button ID="btnViewShipment" runat="server" class="btn btn-success btn-midium pull-left btnStyle" Text="View Shipments" OnClick="btnViewShipment_Click" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <label for="inputStyle" class="control-label" style="font-weight: bold; padding-top: 100px; margin-left: 200px; text-align: left">
        <asp:Label ID="lblNotAuthorizedToView" Font-Bold="true" Visible="false" Font-Size="Large" runat="server" Text="You are not authorized to view this information."></asp:Label>
    </label>


    <div class="col-md-10 ">
        <asp:Label ID="lblNoShipmentDataFound" runat="server" Text="No shipment information found." Visible="false" BackColor="#ffff00"></asp:Label>
    </div>
    <div class="row-fluid">
        <asp:Panel ID="pnlShipmentSummary" runat="server" Visible="false">
            <div class="span6">
                <div class="widget">
                    <div class="widget-body">
                        <div class="form-horizontal">
                            <div style="text-align: left; font-size: 20px; line-height: 30px; padding-bottom: 15px">Shipment Summary:</div>
                            <asp:Repeater ID="rptShipmentSummary" runat="server">
                                <HeaderTemplate>
                                    <table id="data-table" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width: 100px">
                                                    <asp:Label ID="lblCol1" runat="server" Text=" Dates"></asp:Label></th>
                                                <th style="width: 100px">
                                                    <asp:Label ID="lblCol2" runat="server" Text="Quantity"></asp:Label></th>
                                                <th style="width: 100px">
                                                    <asp:Label ID="Label3" runat="server" Text="Action"></asp:Label></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="text-align: center">
                                            <asp:Label ID="lblShipmentDates" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShipmentDate")%>' Width="100px"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:Label ID="lblShipmentQuantity" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShipmentQuantity")%>' Width="80px"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:LinkButton ID="lnkbtnEdit" Visible='<%# ViewState["editEnabled"]%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' OnCommand="lnkbtnEdit_Command" class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-small hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                    <tr style="border: 0px;">
                                        <td colspan="3" style="font-weight: bold;">Total:
                                            <asp:Label runat="server" ID="lblShipmentSummaryTotal"></asp:Label></td>
                                    </tr>
                                    </table>
                                <div class="clearfix">
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>


    <div class="row-fluid">
        <div class="span12">
            <asp:Panel ID="pnlColorDeliveryCountryAndQuantity" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-body" style="overflow-x: auto">
                        <div class="form-horizontal">
                            <div style="text-align: left; font-size: 20px; line-height: 30px; padding-bottom: 15px">Shipment Details:</div>

                            <div id="dt_example2" class="example_alt_pagination">
                                <asp:Repeater ID="rptOrderShipmentDates" runat="server" OnItemDataBound="rptOrderShipmentDates_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="width: 100px">
                                                        <asp:Label ID="lblCol1" runat="server" Text="Shipment Dates"></asp:Label></th>
                                                    <th style="width: 100px">
                                                        <asp:Label ID="lblCol2" runat="server" Text="Shipment Mode"></asp:Label></th>
                                                    <th style="width: 100px">
                                                        <asp:Label ID="lblCol3" runat="server" Text="Color, Country, Size & Quantity"></asp:Label></th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>

                                    <ItemTemplate>
                                        <tr>
                                            <td style="text-align: center; vertical-align: middle">
                                                <asp:Label ID="lblShipmentDate" runat="server" Text=' <%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "ShipmentDate"))%>'></asp:Label><asp:Label ID="lblShipmenDatetId" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Id")%>' Visible="false"></asp:Label></td>
                                            <td style="text-align: center; vertical-align: middle">
                                                <asp:Label ID="lblShipmentMode" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ShipmentMode")%>'></asp:Label></td>
                                            <td style="vertical-align: middle">
                                                <asp:Repeater ID="rptOrderColors" runat="server" OnItemDataBound="rptOrderColors_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="data-table" class="table table-bordered table-hover">

                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="text-align: center; vertical-align: middle">
                                                                <asp:Label ID="lblColor" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ColorDescription")%>'></asp:Label><asp:Label ID="lblOrderColorId" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>' Visible="false"></asp:Label></td>
                                                            <td style="text-align: center">

                                                                <asp:Repeater ID="rptShipmentCountry" runat="server" OnItemDataBound="rptShipmentCountry_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="data-table" class="table table-bordered table-hover">
                                                                            <%--   <tr>
                                                                                <th>Country</th>
                                                                                <th>Size Wise Quantity</th>
                                                                                <th>Total</th>
                                                                            </tr>--%>
                                                                            <tbody>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td style="text-align: center; vertical-align: middle">
                                                                                <asp:Label ID="lblCountry" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryCountryCode")%>'></asp:Label><asp:Label ID="lblDeliveryCountryId" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "OrderDeliveryCountryId")%>' Visible="false"></asp:Label></td>
                                                                            <td>
                                                                                <%--<asp:Repeater ID="rptSizeAndQty" runat="server" >
                                                                                                    <HeaderTemplate>
                                                                                                        <table id="data-table" class="table table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th style="width:100px">
                                                                                                                        <asp:Label ID="lblCol1" runat="server" Text=" Size"></asp:Label></th>
                                                                                                                    <th style="width:100px">
                                                                                                                        <asp:Label ID="lblCol2" runat="server" Text="Quantity"></asp:Label></th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                    </HeaderTemplate>
                                                                                                    <ItemTemplate>
                                                                                                        <tr>
                                                                                                            <td style="text-align:center"><asp:Label ID="lblSizeName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SizeName")%>' Width="50px"></asp:Label></td>
                                                                                                            <td style="text-align:center"><asp:Label ID="lblSizeQuantity" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SizeQuantity")%>' Width="50px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </ItemTemplate>
                                                                                                    <FooterTemplate>
                                                                                                        </tbody>
                                                                                                        </table>
                                                                                                        <div class="clearfix">
                                                                                                    </FooterTemplate>
                                                                                                </asp:Repeater>--%>
                                                                                <asp:GridView ID="gvSizeAndQty" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeAndQty_RowCreated"></asp:GridView>
                                                                            </td>
                                                                            <td style="text-align: center; vertical-align: middle; font-weight:bold">
                                                                               Total: <asp:Label runat="server" ID="CountryTotal"></asp:Label>
                                                                            </td>

                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </tbody>
                                                                                        </table>
                                                                                        <div class="clearfix">
                                                                    </FooterTemplate>
                                                                </asp:Repeater>

                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                                                    </table>
                                                                    <div class="clearfix">
                                                    </FooterTemplate>
                                                </asp:Repeater>

                                            </td>

                                        </tr>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        </tbody>
                                                    </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
