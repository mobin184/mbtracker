﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class AddShipmentV3 : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        DataTable dtDeliveryCountries;
        DataTable dtSizes;
        int entryRowNumber = 0;
        UnitOfWork unitOfWork = new UnitOfWork();
        List<int> ColorIds = new List<int>();
        List<int> CountryIds = new List<int>();

        public class ColorCountry
        {
            public int ColorId { get; set; }
            public int CountryId { get; set; }
        }
        List<ColorCountry> colorCountries = new List<ColorCountry>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                if (Request.QueryString["shipmentId"] != null)
                {
                    ShipmentId = int.Parse(Tools.UrlDecode(Request.QueryString["shipmentId"]));
                    PopulateShipment(ShipmentId);
                }

            }
        }

        private void PopulateShipment(int shipmentId)
        {
            var shipmentInfo = unitOfWork.GenericRepositories<OrderShipmentDates>().GetByID(shipmentId);
            if (shipmentInfo != null)
            {
                var orderInfo = unitOfWork.GenericRepositories<Orders>().GetByID(shipmentInfo.OrderId);
                CommonMethods.LoadDropdownById(ddlStyles, orderInfo.BuyerId, "BuyerStyles", 1, 0);
                CommonMethods.LoadOrderDropdownByStyle(ddlOrders, orderInfo.StyleId, 1, 0);

                ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, orderInfo.BuyerId);
                ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, orderInfo.StyleId);
                ddlOrders.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrders, orderInfo.Id);
                ddlShipmentMode.SelectedValue = shipmentInfo.ShipmentMode;
                tbxShippingDate.Text = shipmentInfo.ShipmentDate.ToString("yyyy-MM-dd");
                if (shipmentInfo.ShipmentCancelDate != null && shipmentInfo.ShipmentCancelDate > DateTime.Parse("2000-01-01"))
                {
                    tbxShipmentCancelDate.Text = shipmentInfo.ShipmentCancelDate?.ToString("yyyy-MM-dd");
                }
                if (shipmentInfo.InspectionAvailDate != null && shipmentInfo.InspectionAvailDate > DateTime.Parse("2000-01-01"))
                {
                    tbxInspecAvailDate.Text = shipmentInfo.InspectionAvailDate?.ToString("yyyy-MM-dd");
                }

                LoadSizeInfo();
                LoadOrderCountryCheckbox();
                LoadDeliveryCountriesByOrderId(orderInfo.Id);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Invalid shipment Id!')", true);
            }
            actionTitle.Text = "Update Shipment Setup";
            ddlBuyers.Enabled = false;
            ddlStyles.Enabled = false;
            ddlOrders.Enabled = false;

            btnSaveShipment.Visible = false;
            btnUpdateShipment.Visible = true;
        }

        int ShipmentId
        {
            set { ViewState["shipmentId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["shipmentId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }



        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlColorDeliveryCountryAndQuantity.Visible = false;
            cblCountries.Items.Clear();
            divCountryCheckBox.Visible = false;

            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
                rptColorAndDeliveryCountries.DataSource = null;
                rptColorAndDeliveryCountries.DataBind();
                //pnlColorDeliveryCountryAndQuantity.Visible = false;
                ddlOrders.Items.Clear();
            }
            else
            {
                ddlStyles.Items.Clear();
                ddlOrders.Items.Clear();
                //pnlColorDeliveryCountryAndQuantity.Visible = false;
            }
        }

        private void BindStylesByBuyer(int buyerId)
        {

            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);

        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {

            pnlColorDeliveryCountryAndQuantity.Visible = false;
            cblCountries.Items.Clear();
            divCountryCheckBox.Visible = false;

            if (ddlStyles.SelectedValue != "")
            {

                BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
            }
            else
            {
                ddlOrders.Items.Clear();
                //pnlColorDeliveryCountryAndQuantity.Visible = false;
            }


        }

        private void BindOrdersByStyle(int styleId)
        {

            CommonMethods.LoadOrderDropdownByStyle(ddlOrders, styleId, 1, 0);

        }


        protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        {

            LoadOrderCountryCheckbox();

        }

        private void LoadOrderCountryCheckbox()
        {
            if (ddlOrders.SelectedValue != "")
            {
                divCountryCheckBox.Visible = true;

                DataTable dt = orderManager.GetDeliveryCountrisByOrderId(int.Parse(ddlOrders.SelectedValue));

                if (dt.Rows.Count > 0)
                {

                    cblCountries.DataValueField = "DeliveryCountryId";
                    cblCountries.DataTextField = "DeliveryCountryCode";
                    cblCountries.DataSource = dt;
                    cblCountries.DataBind();
                    lblNoCountryFound.Visible = false;

                    if (ShipmentId > 0)
                    {
                        DataTable dt2 = orderManager.GetDeliveryCountrisByShipmentDateId(ShipmentId);

                        foreach (ListItem item in cblCountries.Items)
                        {

                            foreach (DataRow row in dt2.Rows)
                            {
                                if (item.Value.ToString() == row["OrderDeliveryCountryId"].ToString())
                                {
                                    item.Selected = true;
                                }
                            }
                        }

                    }
                }
                else
                {
                    cblCountries.Items.Clear();
                    lblNoCountryFound.Visible = true;
                }

            }
            else
            {
                cblCountries.Items.Clear();
                divCountryCheckBox.Visible = false;
                pnlColorDeliveryCountryAndQuantity.Visible = false;
            }
        }

        protected void btnLoadForm_Click(object sender, EventArgs e)
        {

            pnlColorDeliveryCountryAndQuantity.Visible = false;

            if (ddlOrders.SelectedValue != "" && cblCountries.SelectedIndex != -1)
            {
                LoadSizeInfo();
                LoadDeliveryCountriesByOrderId(Convert.ToInt32(ddlOrders.SelectedValue));
            }
            else
            {
                pnlColorDeliveryCountryAndQuantity.Visible = false;
            }

        }

        private void LoadDeliveryCountriesByOrderId(int orderId)
        {

            StringBuilder sbStyleIds = new StringBuilder();

            for (int i = 0; i < cblCountries.Items.Count; i++)
            {
                if (cblCountries.Items[i].Selected)
                {
                    sbStyleIds.Append(",");
                    sbStyleIds.Append(cblCountries.Items[i].Value).ToString();
                }
            }


            dtDeliveryCountries = orderManager.GetSelectedDeliveryCountrisByOrderId(orderId, sbStyleIds.ToString());

            if (dtDeliveryCountries.Rows.Count > 0)
            {
                lblColorOrDeliveryCountryNotFound.Visible = false;
                //LoadOrderByOrderId(Convert.ToInt32(ddlOrders.SelectedValue));
                LoadOrderByOrderId(orderId);
            }
            else
            {
                lblColorOrDeliveryCountryNotFound.Text = "No delivery country found!";
                lblColorOrDeliveryCountryNotFound.Visible = true;
                pnlColorDeliveryCountryAndQuantity.Visible = false;
            }
        }

        private void LoadOrderByOrderId(int orderId)
        {
            DataTable dt = new DataTable();
            dt = orderManager.GetOrderColorsByOrderId(orderId);

            if (dt.Rows.Count > 0)
            {
                rptColorAndDeliveryCountries.DataSource = dt;
                rptColorAndDeliveryCountries.DataBind();
                pnlColorDeliveryCountryAndQuantity.Visible = true;
                lblColorOrDeliveryCountryNotFound.Visible = false;
            }
            else
            {
                lblColorOrDeliveryCountryNotFound.Text = "No color found!";
                lblColorOrDeliveryCountryNotFound.Visible = true;
                pnlColorDeliveryCountryAndQuantity.Visible = false;

            }


        }


        protected void btnSave_Click(object sender, EventArgs e)
        {


            if (tbxShippingDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter shipment date.')", true);
            }
            else if (ddlShipmentMode.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select shipment mode.')", true);
            }
            else
            {
                var flag = true;
                for (int i = 0; i < rptColorAndDeliveryCountries.Items.Count; i++)
                {
                    int sizeQuantity = 0;
                    Label lblBuyerColorId = (Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblBuyerColorId");
                    Label lblOrderQuantity = (Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblOrderQty");

                    Label lblShipmentAlreadyScheduled = (Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblShipmentAlreadyScheduled");
                    var ShipmentAlreadyScheduled = int.Parse(lblShipmentAlreadyScheduled.Text);

                    Label lblColorDesc = (Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblColorDesc");
                    var ColorOrderQty = int.Parse(lblOrderQuantity.Text);



                    Repeater deliveryCountryAndQtyRpt = (Repeater)rptColorAndDeliveryCountries.Items[i].FindControl("rptDeliveryCountryAndQty");
                    var CountrySizeQty = 0;
                    for (int j = 0; j < deliveryCountryAndQtyRpt.Items.Count; j++)
                    {
                        Label deliveryCountryId = (Label)deliveryCountryAndQtyRpt.Items[j].FindControl("lblDeliveryCountryId");
                        DataTable dtShipmentColorCountrySizes = ShipmentDeliveryCountry();
                        GridView gridView = (GridView)deliveryCountryAndQtyRpt.Items[j].FindControl("gvSizeAndQty");
                        for (int k = 0; k < gridView.Columns.Count; k++)
                        {


                            string sizeIdLabel1Id = "lblSizeId1" + k.ToString();
                            string sizeQuantityTextBox1Id = "txtSizeQty1" + k.ToString();

                            Label tbxSizeId = (Label)gridView.Rows[0].Cells[k].FindControl(sizeIdLabel1Id);
                            TextBox sizeQty = (TextBox)gridView.Rows[0].Cells[k].FindControl(sizeQuantityTextBox1Id);

                            if (sizeQty.Text != "")
                            {
                                sizeQuantity = Convert.ToInt32(sizeQty.Text);
                            }
                            else
                            {
                                sizeQuantity = 0;
                            }

                            CountrySizeQty += sizeQuantity;
                        }

                        if (ColorOrderQty < (CountrySizeQty + ShipmentAlreadyScheduled))
                        {
                            flag = false;
                            string msg = $"{lblColorDesc.Text} color shipment quantity can not be greater than order quantity";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + msg + "');", true);
                            break;
                        }
                    }
                }

                if (flag)
                {
                    ShipmentId = orderManager.InsertShipmentDate(Convert.ToInt32(ddlOrders.SelectedValue), tbxShippingDate.Text, ddlShipmentMode.SelectedValue, tbxShipmentCancelDate.Text, tbxInspecAvailDate.Text, CommonMethods.SessionInfo.UserName, DateTime.Now);

                    if (ShipmentId > 0)
                    {

                        int saveColorAndDeliveryCountryResult = saveColorsAndDeliveryCountry(ShipmentId);

                        if (saveColorAndDeliveryCountryResult > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Color and delivery country could not be saved.')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Shipment date could not be saved!')", true);
                    }

                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                    Response.AddHeader("REFRESH", "2;URL=AddShipmentV3.aspx");
                }
            }


        }


        public int saveColorsAndDeliveryCountry(int shipmentDateId)
        {

            int sizeQuantity = 0;
            int shipmentDeliveryCountryResult = 0;


            for (int i = 0; i < rptColorAndDeliveryCountries.Items.Count; i++)
            {
                Label lblBuyerColorId = (Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblBuyerColorId");
                Label lblOrderQuantity = (Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblOrderQty");
                Label lblColorDesc = (Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblColorDesc");
                var ColorOrderQty = int.Parse(lblOrderQuantity.Text);

                Repeater deliveryCountryAndQtyRpt = (Repeater)rptColorAndDeliveryCountries.Items[i].FindControl("rptDeliveryCountryAndQty");

                for (int j = 0; j < deliveryCountryAndQtyRpt.Items.Count; j++)
                {
                    Label deliveryCountryId = (Label)deliveryCountryAndQtyRpt.Items[j].FindControl("lblDeliveryCountryId");
                    DataTable dtShipmentColorCountrySizes = ShipmentDeliveryCountry();
                    var CountrySizeQty = 0;
                    GridView gridView = (GridView)deliveryCountryAndQtyRpt.Items[j].FindControl("gvSizeAndQty");
                    for (int k = 0; k < gridView.Columns.Count; k++)
                    {

                        string sizeIdLabel1Id = "lblSizeId1" + k.ToString();
                        string sizeQuantityTextBox1Id = "txtSizeQty1" + k.ToString();

                        Label tbxSizeId = (Label)gridView.Rows[0].Cells[k].FindControl(sizeIdLabel1Id);
                        TextBox sizeQty = (TextBox)gridView.Rows[0].Cells[k].FindControl(sizeQuantityTextBox1Id);

                        if (sizeQty.Text != "")
                        {
                            sizeQuantity = Convert.ToInt32(sizeQty.Text);

                            //else
                            //{
                            //    sizeQuantity = 0;
                            //}

                            CountrySizeQty += sizeQuantity;
                            dtShipmentColorCountrySizes.Rows.Add(shipmentDateId, Convert.ToInt32(lblBuyerColorId.Text), Convert.ToInt32(deliveryCountryId.Text), Convert.ToInt32(tbxSizeId.Text), sizeQuantity, CommonMethods.SessionInfo.UserName, DateTime.Now);
                        }
                    }

                    if (CountrySizeQty > 0)
                    {

                        //if (CountrySizeQty <= ColorOrderQty)
                        //{
                        //    shipmentDeliveryCountryResult = orderManager.SaveShipmentColorCountrySizes(dtShipmentColorCountrySizes);
                        //}
                        //else
                        //{
                        //    string msg = $"{lblColorDesc.Text} color shipment quantity should be less than order quantity";
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + msg + "');", true);
                        //    break;
                        //}

                        shipmentDeliveryCountryResult = orderManager.SaveShipmentColorCountrySizes(dtShipmentColorCountrySizes);

                    }
                }
            }

            return shipmentDeliveryCountryResult;
        }

        public DataTable ShipmentDeliveryCountry()
        {
            DataTable dt = new DataTable("ShipmentColorCountrySizes");
            dt.Columns.Add("OrderShipmentDateId", typeof(int));
            dt.Columns.Add("BuyerColorId", typeof(int));
            dt.Columns.Add("OrderDeliveryCountryId", typeof(int));
            dt.Columns.Add("SizeId", typeof(int));
            dt.Columns.Add("SizeQuantity", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            //int returnValue = buyerManager.UpdateBuyerSeason(BuyerSeasonId, tbxSeasonName.Text, Convert.ToInt32(ddlSeasonYears.SelectedValue), Convert.ToInt32(ddlBuyers.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now);
            //if (returnValue > 0)
            //{
            //    ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Season was updated successfully.')", true);
            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Season update failed.')", true);
            //}


            //BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            //ClearStyleName();

            //sizeActionTitle.Text = "Add a New Season:";
            //btnSave.Visible = true;
            //btnUpdate.Visible = false;

        }

        int ColorId { get; set; }
        int DeleveryCountryId { get; set; }


        protected void rptColorAndDeliveryCountries_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (ShipmentId != 0)
                {
                    ColorId = Convert.ToInt32(string.IsNullOrEmpty(((Label)e.Item.FindControl("lblColorId")).Text) ? "" : ((Label)e.Item.FindControl("lblColorId")).Text);
                    Label lblShipmentInThisDate = (Label)e.Item.FindControl("lblShipmentInThisDate");

                    var dtShipmentQtyInThisDate = unitOfWork.GetDataTableFromSql($"Exec usp_GetSingleShipmentQty_ByShipmentAndColorId '{ShipmentId}', {ColorId} ");
                    lblShipmentInThisDate.Text = dtShipmentQtyInThisDate.Rows[0][0].ToString();
                }
                Repeater rptDeliveryCountryAndQty = (Repeater)e.Item.FindControl("rptDeliveryCountryAndQty");
                var lblStyleId = (Label)e.Item.FindControl("lblStyleId");
                lblStyleId.Text = ddlStyles.SelectedValue;
                if (dtDeliveryCountries.Rows.Count > 0)
                {
                    rptDeliveryCountryAndQty.DataSource = dtDeliveryCountries;
                    rptDeliveryCountryAndQty.DataBind();
                }
            }

        }

        protected void rptDeliveryCountryAndQty_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                if (ShipmentId != 0)
                {
                    DeleveryCountryId = Convert.ToInt32(string.IsNullOrEmpty(((Label)e.Item.FindControl("lblDeliveryCountryId")).Text) ? "" : ((Label)e.Item.FindControl("lblDeliveryCountryId")).Text);
                }
                GridView gvSizeAndQty = (GridView)e.Item.FindControl("gvSizeAndQty");
                DataTable dtOneRow = new DataTable();
                DataRow dr = null;
                dtOneRow.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dr = dtOneRow.NewRow();
                dr["RowNumber"] = 1;
                dtOneRow.Rows.Add(dr);

                if (dtSizes.Rows.Count > 0)
                {
                    TemplateField templateField = null;

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        templateField = new TemplateField();
                        templateField.HeaderText = dtSizes.Rows[i][1].ToString();
                        gvSizeAndQty.Columns.Add(templateField);

                    }

                }

                gvSizeAndQty.DataSource = dtOneRow;
                gvSizeAndQty.DataBind();



                //if (ShipmentId != 0)
                //{

                var CountrySizeQty = 0;

                for (int k = 0; k < gvSizeAndQty.Columns.Count; k++)
                {

                    string sizeIdLabel1Id = "lblSizeId1" + k.ToString();
                    string sizeQuantityTextBox1Id = "txtSizeQty1" + k.ToString();

                    Label tbxSizeId = (Label)gvSizeAndQty.Rows[0].Cells[k].FindControl(sizeIdLabel1Id);
                    TextBox sizeQty = (TextBox)gvSizeAndQty.Rows[0].Cells[k].FindControl(sizeQuantityTextBox1Id);

                    if (sizeQty.Text != "")
                    {
                        CountrySizeQty += Convert.ToInt32(sizeQty.Text);
                    }
                }

                Label lblCountryTotal = (Label)e.Item.FindControl("lblCountryTotal");
                lblCountryTotal.Text = "Total: " + CountrySizeQty.ToString();
                //lblCountryTotal.Visible = true;
                //}

            }
        }

        protected void LoadSizeInfo()
        {
            if (ddlOrders.SelectedValue != "")
            {
                int orderId = int.Parse(ddlOrders.SelectedValue);
                dtSizes = buyerManager.GetOrderSizes(orderId);
            }
        }

        protected void gvSizeAndQty_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                entryRowNumber++;

                TextBox txtSizeQty = null;
                Label lblSizeId = null;
                LoadSizeInfo();

                if (dtSizes != null)
                {

                    for (int i = 0; i < dtSizes.Rows.Count; i++)
                    {
                        txtSizeQty = new TextBox();
                        txtSizeQty.TextMode = TextBoxMode.Number;
                        txtSizeQty.ID = "txtSizeQty1" + i.ToString();
                        var sizeId = Convert.ToInt32(dtSizes.Rows[i][0].ToString());
                        var lblDeliveryCountryId = (Label)((GridView)sender).DataItemContainer.FindControl("lblDeliveryCountryId");
                        var lblStyleId = (Label)((GridView)sender).DataItemContainer.NamingContainer.NamingContainer.FindControl("lblStyleId");
                        var styleId = int.Parse(lblStyleId.Text);
                        var deleveryCountryId = Convert.ToInt32(lblDeliveryCountryId.Text);

                        var lblColorId = (Label)((GridView)sender).DataItemContainer.NamingContainer.NamingContainer.FindControl("lblColorId");
                        var colorId = Convert.ToInt32(lblColorId.Text);
                        //var sizeQty = CommonMethods.GetSizeQuantityByStyleAndColor(styleId, colorId, sizeId);
                        var lblShipmentColorCountrySize = new Label();
                        lblShipmentColorCountrySize.ID = "lblShipmentColorCountrySize1" + i.ToString();
                        lblShipmentColorCountrySize.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblShipmentColorCountrySize);

                        if (ShipmentId != 0)
                        {
                            var shipmentColorCountrySize = unitOfWork.GenericRepositories<ShipmentColorCountrySizes>().Get(x => x.BuyerColorId == colorId && x.OrderDeliveryCountryId == deleveryCountryId && x.SizeId == sizeId && x.OrderShipmentDateId == ShipmentId).FirstOrDefault();

                            if (shipmentColorCountrySize != null)
                            {
                                txtSizeQty.Text = shipmentColorCountrySize.SizeQuantity + "";
                                lblShipmentColorCountrySize.Text = shipmentColorCountrySize.Id + "";

                            }
                            else
                            {
                                txtSizeQty.Text = "";
                                lblShipmentColorCountrySize.Text = "";
                            }
                        }
                        else
                        {
                            txtSizeQty.Text = "";
                        }
                        //if (sizeQty > 0)
                        //{
                        //    txtSizeQty.Enabled = true;
                        //}
                        //else
                        //{
                        //    txtSizeQty.Enabled = false;
                        //    txtSizeQty.Text = "";
                        //}

                        txtSizeQty.Width = 70;
                        e.Row.Cells[i].Controls.Add(txtSizeQty);

                        lblSizeId = new Label();
                        lblSizeId.ID = "lblSizeId1" + i.ToString();
                        lblSizeId.Text = dtSizes.Rows[i][0].ToString();
                        lblSizeId.Visible = false;
                        e.Row.Cells[i].Controls.Add(lblSizeId);
                    }
                }
            }
        }

        protected void btnUpdateShipment_Click(object sender, EventArgs e)
        {
            if (tbxShippingDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter shipment date.')", true);
            }
            else if (ddlShipmentMode.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select shipment mode.')", true);
            }
            else
            {
                var flag = true;
                for (int i = 0; i < rptColorAndDeliveryCountries.Items.Count; i++)
                {
                    int sizeQuantity = 0;
                    Label lblBuyerColorId = (Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblBuyerColorId");
                    Label lblOrderQuantity = (Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblOrderQty");
                    Label lblColorDesc = (Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblColorDesc");
                    var ColorOrderQty = int.Parse(lblOrderQuantity.Text);

                    Label lblShipmentAlreadyScheduled = (Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblShipmentAlreadyScheduled");
                    var ShipmentAlreadyScheduled = int.Parse(lblShipmentAlreadyScheduled.Text);

                    Label lblShipmentInThisDate = (Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblShipmentInThisDate");
                    var ShipmentScheduledOnThisDate = int.Parse(lblShipmentInThisDate.Text);

                    Repeater deliveryCountryAndQtyRpt = (Repeater)rptColorAndDeliveryCountries.Items[i].FindControl("rptDeliveryCountryAndQty");
                    var CountrySizeQty = 0;
                    for (int j = 0; j < deliveryCountryAndQtyRpt.Items.Count; j++)
                    {
                        Label deliveryCountryId = (Label)deliveryCountryAndQtyRpt.Items[j].FindControl("lblDeliveryCountryId");
                        DataTable dtShipmentColorCountrySizes = ShipmentDeliveryCountry();
                        GridView gridView = (GridView)deliveryCountryAndQtyRpt.Items[j].FindControl("gvSizeAndQty");
                        for (int k = 0; k < gridView.Columns.Count; k++)
                        {


                            string sizeIdLabel1Id = "lblSizeId1" + k.ToString();
                            string sizeQuantityTextBox1Id = "txtSizeQty1" + k.ToString();

                            Label tbxSizeId = (Label)gridView.Rows[0].Cells[k].FindControl(sizeIdLabel1Id);
                            TextBox sizeQty = (TextBox)gridView.Rows[0].Cells[k].FindControl(sizeQuantityTextBox1Id);

                            if (sizeQty.Text != "")
                            {
                                sizeQuantity = Convert.ToInt32(sizeQty.Text);
                            }
                            else
                            {
                                sizeQuantity = 0;
                            }

                            CountrySizeQty += sizeQuantity;
                        }

                        if (ColorOrderQty < (CountrySizeQty + ShipmentAlreadyScheduled - ShipmentScheduledOnThisDate))
                        {
                            flag = false;
                            string msg = $"{lblColorDesc.Text} color shipment quantity can not be greater than order quantity";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + msg + "');", true);
                            break;
                        }
                    }
                }

                if (flag)
                {

                    var shipmentInfo = unitOfWork.GenericRepositories<OrderShipmentDates>().GetByID(ShipmentId);
                    if (shipmentInfo != null)
                    {
                        shipmentInfo.OrderId = Convert.ToInt32(ddlOrders.SelectedValue);
                        shipmentInfo.ShipmentDate = Convert.ToDateTime(tbxShippingDate.Text);
                        shipmentInfo.ShipmentCancelDate = string.IsNullOrEmpty(tbxShipmentCancelDate.Text) ? (DateTime?)null : Convert.ToDateTime(tbxShipmentCancelDate.Text);
                        shipmentInfo.ShipmentMode = ddlShipmentMode.SelectedValue;
                        shipmentInfo.InspectionAvailDate = string.IsNullOrEmpty(tbxInspecAvailDate.Text) ? (DateTime?)null : Convert.ToDateTime(tbxInspecAvailDate.Text);
                        shipmentInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                        shipmentInfo.UpdateDate = DateTime.Now;
                        unitOfWork.GenericRepositories<OrderShipmentDates>().Update(shipmentInfo);



                        orderManager.DeleteOrderShipmentItems(shipmentInfo.Id);



                        int sizeQuantity = 0;
                        for (int i = 0; i < rptColorAndDeliveryCountries.Items.Count; i++)
                        {
                            Label lblBuyerColorId = (Label)rptColorAndDeliveryCountries.Items[i].FindControl("lblBuyerColorId");
                            Repeater deliveryCountryAndQtyRpt = (Repeater)rptColorAndDeliveryCountries.Items[i].FindControl("rptDeliveryCountryAndQty");

                            for (int j = 0; j < deliveryCountryAndQtyRpt.Items.Count; j++)
                            {
                                Label deliveryCountryId = (Label)deliveryCountryAndQtyRpt.Items[j].FindControl("lblDeliveryCountryId");
                                DataTable dtShipmentColorCountrySizes = ShipmentDeliveryCountry();
                                GridView gridView = (GridView)deliveryCountryAndQtyRpt.Items[j].FindControl("gvSizeAndQty");
                                for (int k = 0; k < gridView.Columns.Count; k++)
                                {
                                    string sizeIdLabel1Id = "lblSizeId1" + k.ToString();
                                    string sizeQuantityTextBox1Id = "txtSizeQty1" + k.ToString();

                                    Label lblSizeId = (Label)gridView.Rows[0].Cells[k].FindControl(sizeIdLabel1Id);
                                    TextBox sizeQty = (TextBox)gridView.Rows[0].Cells[k].FindControl(sizeQuantityTextBox1Id);
                                    Label lblShipmentColorCountrySize = (Label)gridView.Rows[0].Cells[k].FindControl("lblShipmentColorCountrySize1" + k);
                                    var ShipmentColorCountrySizeId = Int32.Parse(string.IsNullOrEmpty(lblShipmentColorCountrySize.Text) ? "0" : lblShipmentColorCountrySize.Text);


                                    if (sizeQty.Text != "")
                                    {
                                        sizeQuantity = Convert.ToInt32(sizeQty.Text);
                                    }
                                    else
                                    {
                                        sizeQuantity = 0;
                                    }

                                    if (sizeQuantity > 0)
                                    {
                                        var colorCountrySizeInfo = new ShipmentColorCountrySizes();
                                        colorCountrySizeInfo.OrderShipmentDateId = shipmentInfo.Id;
                                        colorCountrySizeInfo.BuyerColorId = Convert.ToInt32(lblBuyerColorId.Text);
                                        colorCountrySizeInfo.OrderDeliveryCountryId = Convert.ToInt32(deliveryCountryId.Text);
                                        colorCountrySizeInfo.SizeId = Convert.ToInt32(lblSizeId.Text);
                                        colorCountrySizeInfo.SizeQuantity = sizeQuantity;
                                        colorCountrySizeInfo.CreatedBy = CommonMethods.SessionInfo.UserName;
                                        colorCountrySizeInfo.CreateDate = DateTime.Now;
                                        unitOfWork.GenericRepositories<ShipmentColorCountrySizes>().Insert(colorCountrySizeInfo);
                                    }


                                    //if (ShipmentColorCountrySizeId != 0)
                                    //{
                                    //    var colorCountrySizeInfo = unitOfWork.GenericRepositories<ShipmentColorCountrySizes>().GetByID(ShipmentColorCountrySizeId);
                                    //    if (colorCountrySizeInfo != null)
                                    //    {
                                    //        colorCountrySizeInfo.OrderShipmentDateId = shipmentInfo.Id;
                                    //        colorCountrySizeInfo.BuyerColorId = Convert.ToInt32(lblBuyerColorId.Text);
                                    //        colorCountrySizeInfo.OrderDeliveryCountryId = Convert.ToInt32(deliveryCountryId.Text);
                                    //        colorCountrySizeInfo.SizeId = Convert.ToInt32(lblSizeId.Text);
                                    //        colorCountrySizeInfo.SizeQuantity = sizeQuantity;
                                    //        colorCountrySizeInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                                    //        colorCountrySizeInfo.UpdateDate = DateTime.Now;
                                    //        unitOfWork.GenericRepositories<ShipmentColorCountrySizes>().Update(colorCountrySizeInfo);
                                    //    }
                                    //    else
                                    //    {
                                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Color and delivery country could not be saved.')", true);
                                    //        return;
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    if (sizeQuantity > 0)
                                    //    {
                                    //        var colorCountrySizeInfo = new ShipmentColorCountrySizes();
                                    //        colorCountrySizeInfo.OrderShipmentDateId = shipmentInfo.Id;
                                    //        colorCountrySizeInfo.BuyerColorId = Convert.ToInt32(lblBuyerColorId.Text);
                                    //        colorCountrySizeInfo.OrderDeliveryCountryId = Convert.ToInt32(deliveryCountryId.Text);
                                    //        colorCountrySizeInfo.SizeId = Convert.ToInt32(lblSizeId.Text);
                                    //        colorCountrySizeInfo.SizeQuantity = sizeQuantity;
                                    //        colorCountrySizeInfo.CreatedBy = CommonMethods.SessionInfo.UserName;
                                    //        colorCountrySizeInfo.CreateDate = DateTime.Now;
                                    //        unitOfWork.GenericRepositories<ShipmentColorCountrySizes>().Insert(colorCountrySizeInfo);
                                    //    }
                                    //}
                                }
                            }
                        }

                        unitOfWork.Save();
                        var orderInfo = unitOfWork.GenericRepositories<Orders>().GetByID(shipmentInfo.OrderId);
                        CommonMethods.SendEventNotification(2, orderInfo.BuyerId, orderInfo.StyleId, orderInfo.Id, shipmentInfo.Id);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                        Response.AddHeader("REFRESH", "2;URL=ViewShipments.aspx");
                        //ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewShipments.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Shipment date could not be saved.')", true);
                    }
                }
            }
        }

        protected void lnkbtnCalculateSizeTotal_Click(object sender, EventArgs e)
        {


            for (int i = 0; i < rptColorAndDeliveryCountries.Items.Count; i++)
            {

                Repeater deliveryCountryAndQtyRpt = (Repeater)rptColorAndDeliveryCountries.Items[i].FindControl("rptDeliveryCountryAndQty");


                for (int j = 0; j < deliveryCountryAndQtyRpt.Items.Count; j++)
                {

                    GridView gridView = (GridView)deliveryCountryAndQtyRpt.Items[j].FindControl("gvSizeAndQty");
                    Label lblCountryTotal = (Label)deliveryCountryAndQtyRpt.Items[j].FindControl("lblCountryTotal");


                    var CountrySizeQty = 0;

                    for (int k = 0; k < gridView.Columns.Count; k++)
                    {

                        string sizeIdLabel1Id = "lblSizeId1" + k.ToString();
                        string sizeQuantityTextBox1Id = "txtSizeQty1" + k.ToString();

                        Label tbxSizeId = (Label)gridView.Rows[0].Cells[k].FindControl(sizeIdLabel1Id);
                        TextBox sizeQty = (TextBox)gridView.Rows[0].Cells[k].FindControl(sizeQuantityTextBox1Id);

                        if (sizeQty.Text != "")
                        {
                            CountrySizeQty += Convert.ToInt32(sizeQty.Text);
                        }
                    }


                    lblCountryTotal.Text = "Total: " + CountrySizeQty.ToString();
                }
            }


        }


    }
}