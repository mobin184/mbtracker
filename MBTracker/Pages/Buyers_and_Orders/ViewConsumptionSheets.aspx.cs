﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class ViewConsumptionSheets : System.Web.UI.Page
    {


        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        int consumptionSheetId;
        DataTable dtConsumptionSheetFiles;
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableEditButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                if (Request.QueryString["styleId"] != null)
                {
                    var styleId = int.Parse(Tools.UrlDecode(Request.QueryString["styleId"]));
                    var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(styleId);
                    ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
                    BindStylesByBuyer(styleInfo.BuyerId);
                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, styleId);

                    pnlViewConsumptionSheets.Visible = true;
                    LoadExistingConsumptionSheets(styleId);
                    pnlYarnConsumption.Visible = false;

                }
                else if (Request.QueryString["showInfo"] != null)
                {
                    ConsumptionSheetId = int.Parse(Tools.UrlDecode(Request.QueryString["showInfo"]));
                    var consumptionSheetInfo = unitOfWork.GenericRepositories<OrderConsumptionSheets>().GetByID(ConsumptionSheetId);
                    var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(consumptionSheetInfo.StyleId);

                    if (CommonMethods.HasBuyerAssignedToUser(styleInfo.BuyerId, CommonMethods.SessionInfo.UserId))
                    {
                        ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, styleInfo.BuyerId);
                        BindStylesByBuyer(styleInfo.BuyerId);
                        ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, consumptionSheetInfo.StyleId);

                        ddlBuyers.Enabled = false;
                        ddlStyles.Enabled = false;

                        pnlViewConsumptionSheets.Visible = true;
                        LoadExistingConsumptionSheets(consumptionSheetInfo.StyleId);
                        SetYarnInfoControl();
                        pnlYarnConsumption.Visible = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown()", true);
                    }
                    else
                    {
                        lblNotAuthorizedToView.Visible = true;
                        divBuyerAndStyle.Visible = false;
                    }
                }

            }
        }

        private void EnableDisableEditButton()
        {
            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Merchandiser,QCE");
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewConsumptionSheets", 1, 1);

        }
        int ConsumptionSheetId
        {
            set { ViewState["consumptionSheetId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["consumptionSheetId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }




        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlStyles.Items.Clear();
            //ddlOrders.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {

                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            //else
            //{
            //    ddlStyles.Items.Clear();
            //    ddlOrders.Items.Clear();

            //}
            pnlViewConsumptionSheets.Visible = false;
            pnlYarnConsumption.Visible = false;
            rptConsumptionSheets.DataSource = null;
            rptConsumptionSheets.DataBind();
        }

        private void BindStylesByBuyer(int buyerId)
        {

            CommonMethods.LoadDropdownById(ddlStyles, buyerId, "BuyerStyles", 1, 0);

        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {

            pnlViewConsumptionSheets.Visible = false;
            pnlYarnConsumption.Visible = false;
            rptConsumptionSheets.DataSource = null;
            rptConsumptionSheets.DataBind();
            //if (ddlStyles.SelectedValue != "")
            //{
            //    pnlViewConsumptionSheets.Visible = true;
            //    LoadExistingConsumptionSheets(Convert.ToInt32(ddlStyles.SelectedValue));
            //    pnlYarnConsumption.Visible = false;
            //    //BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
            //}
            //else
            //{
            //    pnlViewConsumptionSheets.Visible = false;
            //    pnlYarnConsumption.Visible = false;
            //    rptConsumptionSheets.DataSource = null;
            //    rptConsumptionSheets.DataBind();
            //}


        }

        //private void BindOrdersByStyle(int styleId)
        //{

        //    CommonMethods.LoadOrderDropdownByStyle(ddlOrders, styleId, 1, 0);

        //}


        //protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddlOrders.SelectedValue != "")
        //    {
        //        pnlViewConsumptionSheets.Visible = true;
        //        LoadExistingConsumptionSheets(Convert.ToInt32(ddlOrders.SelectedValue));
        //        pnlYarnConsumption.Visible = false;


        //    }
        //    else
        //    {
        //        pnlViewConsumptionSheets.Visible = false;

        //    }
        //}

        private void LoadExistingConsumptionSheets(int styleId)
        {
            DataTable dt = new DataTable();
            dt = orderManager.GetExistingConsumptionSheets(styleId);

            if (dt.Rows.Count > 0)
            {
                rptConsumptionSheets.DataSource = dt;
                rptConsumptionSheets.DataBind();
                lblNoConsumptionSheetFound.Visible = false;
                rptConsumptionSheets.Visible = true;
                pnlViewConsumptionSheets.Visible = true;
            }
            else
            {
                lblNoConsumptionSheetFound.Visible = true;
                rptConsumptionSheets.Visible = false;
                pnlViewConsumptionSheets.Visible = false;
            }


        }


        protected void rptConsumptionSheets_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptConsumptionFiles = (Repeater)e.Item.FindControl("rptFiles");

                Label lblConsumptionSheetId = (Label)e.Item.FindControl("lblConsumptionSheetId");
                consumptionSheetId = Convert.ToInt32(lblConsumptionSheetId.Text);

                dtConsumptionSheetFiles = orderManager.GetConsumptionSheetFileInfo(consumptionSheetId);

                if (dtConsumptionSheetFiles.Rows.Count > 0)
                {
                    rptConsumptionFiles.DataSource = dtConsumptionSheetFiles;
                    rptConsumptionFiles.DataBind();
                }
            }
        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            ConsumptionSheetId = int.Parse(e.CommandArgument.ToString());
            Response.Redirect("AddConsumptionSheet.aspx?consumptionSheetId=" + Tools.UrlEncode(ConsumptionSheetId + ""));
        }

        protected void lnkbtnView_Command(object sender, CommandEventArgs e)
        {
            ConsumptionSheetId = int.Parse(e.CommandArgument.ToString());
            //var dtConsumptionInfo = unitOfWork.GetDataTableFromSql($"EXEC usp_GetConsumptionSheetById {ConsumptionSheetId}");
            //SetBasicInfo(dtConsumptionInfo);
            SetYarnInfoControlForUpdate(ConsumptionSheetId);
            pnlYarnConsumption.Visible = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown()", true);
        }

        private void SetYarnInfoControlForUpdate(int conSheetid)
        {
            var dt = unitOfWork.GetDataTableFromSql($"SELECT CAST(YarnItemId as nvarchar(max)) as YarnId, CASE WHEN yc.YarnItemId IS NULL THEN yc.YarnComposition ELSE itm.ItemName END as YarnComposition, YarnCount,PlyMainYarn,PlyLycra, CAST(AverageWeight as nvarchar(max)) as Weight FROM YarnConsumptionByParts yc LEFT JOIN YarnAccessoriesItems itm ON yc.YarnItemId = itm.Id WHERE ConsumptionSheetId = '{conSheetid}'");
            rpt.DataSource = dt;
            rpt.DataBind();
            ViewState["CurrentTable"] = dt;
        }

        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (ConsumptionSheetId != 0)
                {
                    //var lblPlacement = (Label)e.Item.FindControl("lblPlacement");
                    var lblYarnComposition = (Label)e.Item.FindControl("tbxYarnComposition");
                    var lblYarnCount = (Label)e.Item.FindControl("tbxYarnCount");
                    var lblPlyMainYarn = (Label)e.Item.FindControl("tbxPlyMainYarn");
                    var lblPlyLycra = (Label)e.Item.FindControl("tbxPlyLycra");
                    var lblWeight = (Label)e.Item.FindControl("tbxAverageWeight");

                    //var yarnId = DataBinder.Eval(e.Item.DataItem, "YarnId").ToString();
                    //LoadYarnItemDropdown(ddlYarnItemId, yarnId);

                    lblYarnComposition.Text = DataBinder.Eval(e.Item.DataItem, "YarnComposition").ToString();
                    lblYarnCount.Text = DataBinder.Eval(e.Item.DataItem, "YarnCount").ToString();
                    lblPlyMainYarn.Text = DataBinder.Eval(e.Item.DataItem, "PlyMainYarn").ToString();
                    lblPlyLycra.Text = DataBinder.Eval(e.Item.DataItem, "PlyLycra").ToString();
                    lblWeight.Text = DataBinder.Eval(e.Item.DataItem, "Weight").ToString();

                    //var lblSweatherPartsId = (Label)e.Item.FindControl("lblSweatherPartsId");
                    //var partId = string.IsNullOrEmpty(lblSweatherPartsId.Text) ? 0 : int.Parse(lblSweatherPartsId.Text);
                    //if (partId != 0)
                    //{
                    //    unitOfWork = new UnitOfWork();
                    //    var partConsumption = unitOfWork.GenericRepositories<YarnConsumptionByParts>().Get(x => x.ConsumptionSheetId == ConsumptionSheetId && x.PlacementId == partId).FirstOrDefault();
                    //    if (partConsumption != null)
                    //    {
                    //        var lblYarnComposition = (Label)e.Item.FindControl("tbxYarnComposition");
                    //        var lblYarnCount = (Label)e.Item.FindControl("tbxYarnCount");
                    //        var lblPlyMainYarn = (Label)e.Item.FindControl("tbxPlyMainYarn");
                    //        var lblPlyLycra = (Label)e.Item.FindControl("tbxPlyLycra");
                    //        var lblAverageWeight = (Label)e.Item.FindControl("tbxAverageWeight");
                    //        var lblYarnConsumptionByPartsId = (Label)e.Item.FindControl("lblYarnConsumptionByPartsId");

                    //        lblYarnComposition.Text = partConsumption.YarnComposition;
                    //        lblYarnCount.Text = partConsumption.YarnCount;
                    //        lblPlyMainYarn.Text = partConsumption.PlyMainYarn;
                    //        lblPlyLycra.Text = partConsumption.PlyLycra;
                    //        lblAverageWeight.Text = partConsumption.AverageWeight + "";
                    //        lblYarnConsumptionByPartsId.Text = partConsumption.Id + "";
                    //    }
                    //}
                }
            }

        }

        private void SetYarnInfoControl()
        {
            DataTable dt = new DataTable();
            dt = orderManager.GetSweaterParts();
            rpt.DataSource = dt;
            rpt.DataBind();
        }

        protected void btnViewConsSheet_Click(object sender, EventArgs e)
        {
            if (ddlBuyers.SelectedValue != "")
            {
                if (ddlStyles.SelectedValue != "")
                {
                    pnlViewConsumptionSheets.Visible = true;
                    LoadExistingConsumptionSheets(Convert.ToInt32(ddlStyles.SelectedValue));
                    //BindOrdersByStyle(Convert.ToInt32(ddlStyles.SelectedValue));
                }
                else
                {
                    pnlViewConsumptionSheets.Visible = false;
                    pnlYarnConsumption.Visible = false;
                    rptConsumptionSheets.DataSource = null;
                    rptConsumptionSheets.DataBind();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a style.');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a buyer');", true);
            }

        }

        //private void SetBasicInfo(DataTable dt)
        //{
        //    if (dt.Rows.Count > 0)
        //    {
        //        lblBuyerName.Text = dt.Rows[0].Field<string>("BuyerName");
        //        lblStyle.Text = dt.Rows[0].Field<string>("StyleName");
        //        lblOrder.Text = dt.Rows[0].Field<string>("OrderNumber");
        //        lblStage.Text = dt.Rows[0].Field<string>("Stage");
        //        lblKnittingMc.Text = dt.Rows[0].Field<string>("BrandName");
        //        lblKnigginMcGauge.Text = dt.Rows[0].Field<string>("KnittingMachineGauge");
        //        lblNeedlePerCm.Text = dt.Rows[0].Field<string>("NeedlePerCM");
        //        lblMcSpeed.Text = dt.Rows[0].Field<string>("MachineSpeed");
        //        lblAvgKnittingTime.Text = dt.Rows[0].Field<string>("KnittingTime");
        //        lblDesignerName.Text = dt.Rows[0].Field<string>("DesignerName");
        //        lblDesignerPhone.Text = dt.Rows[0].Field<string>("DesignerContactNumber");
        //    }
        //}
    }
}