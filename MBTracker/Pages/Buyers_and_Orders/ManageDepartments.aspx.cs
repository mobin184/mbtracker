﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using Repositories;
using MBTracker.EF;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class ManageDepartments : System.Web.UI.Page
    {


        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableDeleteButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);          
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteDepartment(BuyerDepartmentId);
                }
            }
        }
        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");

            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ManageDepartments", 2, 1);

        }

        int BuyerDepartmentId
        {
            set { ViewState["departmentId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["departmentId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue != "")
            {

                BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            else
            {
                pnlDepartments.Visible = false;
            }
        }

        private void BindData(int buyerId)
        {

            DataTable dt = new DataTable();

            dt = buyerManager.LoadDepartmentsByBuyer(buyerId);
            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                pnlDepartments.Visible = true;
            }
            else
            {
                pnlDepartments.Visible = false;
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {


            int departmentId = buyerManager.InsertDepartment(txtDepartmentName.Text, Convert.ToInt32(ddlBuyers.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now);

            if (departmentId > 0)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Department name exists.')", true);
            }

         
            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearDepartmentName();


        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int returnValue = buyerManager.UpdateBuyerDepartment(BuyerDepartmentId, txtDepartmentName.Text, Convert.ToInt32(ddlBuyers.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now);
            if (returnValue > 0)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Department update was failed.')", true);
            }

       
            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearDepartmentName();

            departmentActionTitle.Text = "Add a New Department:";
            btnSave.Visible = true;
            btnUpdate.Visible = false;

        }

        private void ClearAll()
        {
            txtDepartmentName.Text = string.Empty;
            ddlBuyers.ClearSelection();

        }

        private void ClearDepartmentName()
        {
            txtDepartmentName.Text = string.Empty;

        }


        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int buyerDepartmentId = Convert.ToInt32(e.CommandArgument.ToString());
                PopulateDepartments(buyerDepartmentId);
            }
            else if (e.CommandName == "Delete")
            {
                BuyerDepartmentId = Convert.ToInt32(e.CommandArgument.ToString());

                var departmentCannotBeDeleted = unitOfWork.GenericRepositories<viewDepartmentsCanNotBeDeleted>().GetByID(BuyerDepartmentId);
                if (departmentCannotBeDeleted == null)
                {
                    var title = "Warning";
                    var msg = "Are you sure you want to delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                }
                else
                {
                    var title = "Warning";
                    var msg = "Sorry, you can not delete it.<br />There was some dependencies of this department.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                }
            }
           

        }

        private void PopulateDepartments(int buyerDepartmentId)
        {
            DataTable dt = buyerManager.GetDepartmentById(buyerDepartmentId);
            if (dt.Rows.Count > 0)
            {
                BuyerDepartmentId = Convert.ToInt32(dt.Rows[0]["Id"]);
                txtDepartmentName.Text = dt.Rows[0]["DepartmentName"].ToString();
                ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, Convert.ToInt32(dt.Rows[0]["BuyerId"]));

                departmentActionTitle.Text = "Update Department:";
                btnSave.Visible = false;
                btnUpdate.Visible = true;
            }

 
        }


        private void DeleteDepartment(int buyerDepartmentId)
        {

            int returnValue = buyerManager.DeleteBuyerDepartment(buyerDepartmentId, CommonMethods.SessionInfo.UserName, DateTime.Now);

            if (returnValue > 0)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Department deletion was failed.')", true);
            }

            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearDepartmentName();


            departmentActionTitle.Text = "Add a New Department:";
            btnSave.Visible = true;
            btnUpdate.Visible = false;

        }



    }
}