﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class AddNewOrderV2 : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        OrderManager orderManager = new OrderManager();
        CommonManager commonManager = new CommonManager();

        //DataTable dtSizes;
        DataTable dtOrderColors;
        DataTable dtStyleColors;

        int selectedBuyerValue;
        bool updating = false;

        UnitOfWork unitOfWork = new UnitOfWork();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                LoadOrderStatusDropDown();
               
                if ((CommonMethods.SessionInfo.RoleId == 10 || CommonMethods.SessionInfo.RoleId == 3))
                {
                    ddlStatus.Enabled = false;
                }
                LoadDropdown(ddlStatus, "BookingStatus", 1, 0);

                if (Request.QueryString["orderId"] != null)
                {
                    //var CommandName = "";
                    //CommandName = Tools.UrlDecode(Request.QueryString["CommandName"].ToString());

                    OrderId = int.Parse(Tools.UrlDecode(Request.QueryString["orderId"]));

                    //if (Request.QueryString["PIId"] != null && CommandName == "StatusChange")
                    //{
                    //    PopulateOrder(OrderId);

                    //    btnSaveOrder.Visible = false;
                    //    btnUpdateOrder.Visible = false;
                    //    btnStatusChange.Visible = true;
                    //    pnlBasicInfo.Enabled = false;
                    //    pnlColorAndSize.Enabled = false;
                    //    ddlStatus.Enabled = true;

                    //}
                    //else { 

                        var notEditAble = unitOfWork.GenericRepositories<viewOrderCantEditDelete>().IsExist(x => x.OrderId == OrderId);
                    if (!notEditAble)
                    {
                        updating = true;
                        PopulateOrder(OrderId);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('You cannot update this order, it has related entity!')", true);
                        //Response.AddHeader("REFRESH", "2;URL=ViewOrders.aspx");
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewOrders.aspx');", true);
                    }
                    //}
                }
            }

        }


        int OrderId
        {
            set { ViewState["orderId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["orderId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }



        private void PopulateOrder(int orderId)
        {
            var orderInfo = unitOfWork.GenericRepositories<Orders>().GetByID(orderId);
            CommonMethods.LoadDropdownById(ddlBuyerDeprtments, orderInfo.BuyerId, "BuyerDepartments", 1, 0);
            CommonMethods.LoadDropdownById(ddlStyle, orderInfo.BuyerId, "BuyerStyles", 1, 0);
            CommonMethods.LoadDropdownById(ddlSeason, orderInfo.BuyerId, "BuyerSeasons", 1, 0);
            CommonMethods.LoadDropdown(ddlOrderYear, "Years WHERE IsActive = 1", 1, 0);
            CommonMethods.LoadDropdownBookingNumberByBuyerAndStyleId(ddlBookingNumber, orderInfo.BuyerId, orderInfo.StyleId, 1, 0);

            CommonMethods.LoadStyleSizeCheckBoxList(cblStyleSizes, orderInfo.StyleId, lblNoSizeFound, pnlStyleSizes);
            CommonMethods.LoadDeliveryCountriesCheckBoxList(cblDeliveryCountries, orderInfo.BuyerId, lblNoCountryFound, btnSelectAllCountries, btnUnselectAllCountries, btnSaveOrder);

            BindDataToUI(orderInfo);
        }

        private void BindDataToUI(Orders orderInfo)
        {
            if ((CommonMethods.SessionInfo.RoleId == 10 || CommonMethods.SessionInfo.RoleId == 3) && ((int.Parse(orderInfo.ApprovedStatus.ToString()) == 2) || (int.Parse(orderInfo.ApprovedStatus.ToString()) == 3)))
            {
                pnlBasicInfo.Enabled = false;
                pnlColorAndSize.Enabled = false;
                //pnlNonEdit3.Enabled = true;
            }
            else
            {
                pnlBasicInfo.Enabled = true;
                pnlColorAndSize.Enabled = true;
                // pnlNonEdit3.Enabled = false;
            }

            ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, orderInfo.BuyerId);
            ddlBuyerDeprtments.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyerDeprtments, orderInfo.BuyerDepartmentId ?? 0);
            ddlStyle.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyle, orderInfo.StyleId);

            ddlBuyers.Enabled = false;
            ddlStyle.Enabled = false;
            ddlSeason.Enabled = false;

            ddlSeason.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSeason, orderInfo.OrderSeasonId ?? 0);
            ddlOrderYear.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrderYear, orderInfo.OrderYearId ?? 0);
            ddlOrderStatus.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrderStatus, orderInfo.StatusId);
            ddlStatus.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStatus, orderInfo.ApprovedStatus);
            //ddlBookingNumber.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBookingNumber, orderInfo.BookingId);

            tbxOrderNumber.Text = orderInfo.OrderNumber + "";
            tbxOrderDate.Text = orderInfo.OrderDate.ToString("yyyy-MM-dd");
            tbxExFactoryDate.Text = orderInfo.ExFactoryDate?.ToString("yyyy-MM-dd");
            tbxOrderItem.Text = orderInfo.OrderItem;
            //tbxPaymentTerm.Text = orderInfo.PaymentTerms;
            //tbxLCValue.Text = orderInfo.LCValue?.ToString();

            var selectedSizes = unitOfWork.GenericRepositories<OrderSizes>().Get(x => x.OrderId == orderInfo.Id).ToList();

            foreach (ListItem item in cblStyleSizes.Items)
            {
                item.Selected = selectedSizes.Exists(x => x.SizeId == Int32.Parse(item.Value));
            }

            var selectedCountris = unitOfWork.GenericRepositories<OrderDeliveryCountries>().Get(x => x.OrderId == orderInfo.Id).ToList();

            foreach (ListItem item in cblDeliveryCountries.Items)
            {
                item.Selected = selectedCountris.Exists(x => x.DeliveryCountryId == Int32.Parse(item.Value));
            }

            //dtSizes = buyerManager.GetSizes(orderInfo.BuyerId);

            selectedBuyerValue = orderInfo.BuyerId;
            //SetInitialRowCount();

            lblDeliveryCountry.Visible = true;
            lblColorAndSize.Visible = true;
            pnlColorAndSize.Visible = true;
            pnlFileUploads.Visible = true;

            // var orderColor = unitOfWork.GenericRepositories<OrderColors>().Get(x => x.OrderId == orderInfo.Id);


             dtOrderColors = orderManager.GetOrderColorsByOrderId(orderInfo.Id);
             dtStyleColors = new CommonManager().GetColorsByStyle(int.Parse(ddlStyle.SelectedValue));

            rptColor.DataSource = dtStyleColors;
            rptColor.DataBind();

            userActionTitle.Text = "Update Order";
            btnSaveOrder.Visible = false;
            btnUpdateOrder.Visible = true;


        }

        protected void rptColor_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater sourceRptr = (Repeater)sender;
            var dt = (DataTable)sourceRptr.DataSource;
            var index = e.Item.ItemIndex;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (updating)
                {

                    TextBox tbxOrderQtyByColor = (TextBox)e.Item.FindControl("tbxOrderQtyByColor");
                    TextBox tbxUnitCost = (TextBox)e.Item.FindControl("tbxUnitCost");

                    Label lblColorId = (Label)e.Item.FindControl("lblColorId");

                    for (int i =0; i< dtOrderColors.Rows.Count; i++)
                    {
                        if (int.Parse(lblColorId.Text) == dtOrderColors.Rows[i].Field<int>("BuyerColorId"))
                        {
                            tbxOrderQtyByColor.Text = dtOrderColors.Rows[i].Field<int>("OrderQuantity").ToString();
                            tbxUnitCost.Text = dtOrderColors.Rows[i].Field<decimal>("UnitCost").ToString();
                        }
                    }
                }
            }
        }

        private void LoadOrderStatusDropDown()
        {
            DataTable dt = commonManager.LoadOrderStatusDropdown(CommonMethods.SessionInfo.DepartmentId);
            ddlOrderStatus.DataSource = dt;
            ddlOrderStatus.DataBind();

            ddlOrderStatus.Items.Add(new ListItem("", ""));
            ddlOrderStatus.DataSource = dt;
            ddlOrderStatus.DataTextField = dt.Columns[1].ToString();
            ddlOrderStatus.DataValueField = dt.Columns[0].ToString();
            ddlOrderStatus.DataBind();
            ddlOrderStatus.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlOrderStatus.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrderStatus, 1);

        }



        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();

            ddlStyle.DataSource = dt;
            ddlStyle.DataBind();

            ddlBuyerDeprtments.DataSource = dt;
            ddlBuyerDeprtments.DataBind();

            ddlSeason.DataSource = dt;
            ddlSeason.DataBind();

            ddlOrderYear.DataSource = dt;
            ddlOrderYear.DataBind();

            lblNoCountryFound.Visible = false;
            lblDeliveryCountry.Visible = false;
            cblDeliveryCountries.Visible = false;
            btnSelectAllCountries.Visible = false;
            btnUnselectAllCountries.Visible = false;

            lblColorAndSize.Visible = false;
            pnlColorAndSize.Visible = false;
            pnlFileUploads.Visible = false;

            if (ddlBuyers.SelectedValue != "")
            {
                selectedBuyerValue = Convert.ToInt32(ddlBuyers.SelectedValue);
                CommonMethods.LoadDropdownById(ddlStyle, selectedBuyerValue, "BuyerStyles", 1, 0);
                CommonMethods.LoadDropdownById(ddlBuyerDeprtments, selectedBuyerValue, "BuyerDepartments", 1, 0);
                CommonMethods.LoadDropdownById(ddlSeason, selectedBuyerValue, "BuyerSeasons", 1, 0);
                CommonMethods.LoadDropdown(ddlOrderYear, "Years WHERE IsActive = 1", 1, 0);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "CacheItems()", true);
            }
        }

        protected void ddlStyle_SelectedIndexChanged(object sender, EventArgs e)
        {

            rptColor.DataSource = null;
            rptColor.DataBind();

            cblDeliveryCountries.DataSource = null;
            cblDeliveryCountries.DataBind();

            cblStyleSizes.DataSource = null;
            cblStyleSizes.DataBind();


            lblColorAndSize.Visible = false;
            pnlColorAndSize.Visible = false;
            pnlFileUploads.Visible = false;

            if (ddlStyle.SelectedValue != "")
            {

                DataTable dt = new CommonManager().GetColorsByStyle(int.Parse(ddlStyle.SelectedValue));
                CommonMethods.LoadDropdownSeasonByBuyerAndStyleId(ddlSeason, int.Parse(ddlBuyers.SelectedValue), int.Parse(ddlStyle.SelectedValue), 1, 0);
                CommonMethods.LoadDropdownBookingNumberByBuyerAndStyleId(ddlBookingNumber, int.Parse(ddlBuyers.SelectedValue), int.Parse(ddlStyle.SelectedValue), 1, 0);
                ddlSeason.Enabled = false;
                ddlBookingNumber.Enabled = false;
                if (dt.Rows.Count > 0)
                {
                    rptColor.DataSource = dt;
                    rptColor.DataBind();

                    CommonMethods.LoadDeliveryCountriesCheckBoxList(cblDeliveryCountries, int.Parse(ddlBuyers.SelectedValue), lblNoCountryFound, btnSelectAllCountries, btnUnselectAllCountries, btnSaveOrder);
                    LoadStyleSizes(int.Parse(ddlStyle.SelectedValue));

                    lblDeliveryCountry.Visible = true;
                    lblColorAndSize.Visible = true;
                    pnlColorAndSize.Visible = true;
                    pnlFileUploads.Visible = true;
                }
            }
           // ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "CacheItems()", true);
        }

        protected void ddlOrderYear_SelectedIndexChanged(object sender, EventArgs e)
        {

            rptColor.DataSource = null;
            rptColor.DataBind();

            cblDeliveryCountries.DataSource = null;
            cblDeliveryCountries.DataBind();

            cblStyleSizes.DataSource = null;
            cblStyleSizes.DataBind();


            lblColorAndSize.Visible = false;
            pnlColorAndSize.Visible = false;
            pnlFileUploads.Visible = false;

            if (ddlStyle.SelectedValue != "")
            {
                DataTable dt = new CommonManager().GetColorsByStyle(int.Parse(ddlStyle.SelectedValue));

                if (dt.Rows.Count > 0)
                {
                    rptColor.DataSource = dt;
                    rptColor.DataBind();

                    CommonMethods.LoadDeliveryCountriesCheckBoxList(cblDeliveryCountries, int.Parse(ddlBuyers.SelectedValue), lblNoCountryFound, btnSelectAllCountries, btnUnselectAllCountries, btnSaveOrder);
                    LoadStyleSizes(int.Parse(ddlStyle.SelectedValue));

                    lblDeliveryCountry.Visible = true;
                    lblColorAndSize.Visible = true;
                    pnlColorAndSize.Visible = true;
                    pnlFileUploads.Visible = true;
                }
            }
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "CacheItems()", true);
        }

        protected void btnStatusChange_Click(object sender, EventArgs e)
        {

            try
            {
                var orderInfo = unitOfWork.GenericRepositories<Orders>().GetByID(OrderId);

                if (orderInfo != null)
                {
                    orderInfo.ApprovedStatus = Convert.ToInt32(ddlStatus.SelectedValue);
                    orderInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                    orderInfo.UpdateDate = DateTime.Now;
                    unitOfWork.GenericRepositories<Orders>().Update(orderInfo);
                    unitOfWork.Save();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }

        }

        protected void btnSaveOrder_Click(object sender, EventArgs e)
        {

            try
            {
                int buyerDepartment = 0;
                int buyerSeason = 0;
                //int buyerYear = 0;

                if (ddlBuyerDeprtments.SelectedValue != "")
                {
                    buyerDepartment = Convert.ToInt32(ddlBuyerDeprtments.SelectedValue);
                }
                if (ddlSeason.SelectedValue != "")
                {
                    buyerSeason = Convert.ToInt32(ddlSeason.SelectedValue);
                }

                //if (ddlOrderYear.SelectedValue != "")
                //{
                //    buyerYear = Convert.ToInt32(ddlOrderYear.SelectedValue);
                //}


                if (ddlStyle.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a style.')", true);
                }
                if (ddlBuyerDeprtments.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a Department.')", true);
                }
                if (ddlOrderYear.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select order year.')", true);
                }
                else if (tbxOrderNumber.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter order number.')", true);

                }
                else if (tbxOrderDate.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter order date.')", true);
                }
                else if (ddlOrderStatus.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select order status.')", true);
                }
                else if (cblStyleSizes.SelectedIndex == -1)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select sizes for the order.')", true);
                }
                else if (cblDeliveryCountries.SelectedIndex == -1)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select delivery countries.')", true);
                }
                else
                {
                    var colorQty = 0;

                    for (int l = 0; l < rptColor.Items.Count; l++)
                    {
                        var tbxOrderQtyByColor = (TextBox)rptColor.Items[l].FindControl("tbxOrderQtyByColor");

                        if (!string.IsNullOrEmpty(tbxOrderQtyByColor.Text))
                        {
                            colorQty = int.Parse(tbxOrderQtyByColor.Text);
                            break;
                        }

                    }

                    if (colorQty > 0)
                    {
                        int bookingNumberValue = 0;

                        if (!string.IsNullOrEmpty(ddlBookingNumber.SelectedValue))
                        {
                            bookingNumberValue = Convert.ToInt32(ddlBookingNumber.SelectedValue);
                        }

                        OrderId = orderManager.InsertOrderBasicInfo(Convert.ToInt32(ddlBuyers.SelectedValue), bookingNumberValue, buyerDepartment, Convert.ToInt32(ddlStyle.SelectedValue), buyerSeason, Convert.ToInt32(ddlOrderYear.SelectedValue), tbxOrderNumber.Text, Convert.ToDateTime(tbxOrderDate.Text), Convert.ToDateTime(tbxExFactoryDate.Text), tbxOrderItem.Text, /*tbxPaymentTerm.Text*/ "",  0, Convert.ToInt32(ddlOrderStatus.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now, Convert.ToInt32(ddlStatus.SelectedValue));

                        if (OrderId > 0)
                        {

                            int saveColorAndQuantityResult = saveColorsAndQuantity(OrderId);

                            if (saveColorAndQuantityResult > 0)
                            {
                                int saveOrderSizesResult = SaveOrderSizes();

                                if (saveOrderSizesResult > 0)
                                {
                                    int saveDeliveryCountryResult = SaveDeliveryCountry();
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                                    Response.AddHeader("REFRESH", "2;URL=AddNewOrderV2.aspx");
                                    ddlBuyers.SelectedValue = "";
                                }

                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Delivery country for this order could not be saved.')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('The order already exists.');", true);
                        }

                        // ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter order quantities for colors.');", true);
                    }

                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "')", true);
            }

        }

        public int saveColorsAndQuantity(int orderId)
        {

            int orderColorId = 0;
            int orderQtyByColor = 0;


            for (int i = 0; i < rptColor.Items.Count; i++)
            {

                TextBox tbxOrderQtyByColor = (TextBox)rptColor.Items[i].FindControl("tbxOrderQtyByColor");
                TextBox tbxUnitCost = (TextBox)rptColor.Items[i].FindControl("tbxUnitCost");

                decimal unitCost = 0;

                if (tbxUnitCost.Text != "")
                {
                    unitCost = decimal.Parse(tbxUnitCost.Text);

                }

                Label lblColorId = (Label)rptColor.Items[i].FindControl("lblColorId");

                if (tbxOrderQtyByColor.Text != "")
                {
                    orderQtyByColor = Convert.ToInt32(tbxOrderQtyByColor.Text);
                    orderColorId = orderManager.SaveOrderColor(orderId, int.Parse(lblColorId.Text), unitCost, orderQtyByColor, CommonMethods.SessionInfo.UserName, DateTime.Now);
                }

               

            }

            return orderColorId;
        }

        public int SaveOrderSizes()
        {

            DataTable dt = OrderSizes();

            for (int i = 0; i < cblStyleSizes.Items.Count; i++)
            {
                if (cblStyleSizes.Items[i].Selected)
                {
                    dt.Rows.Add(OrderId, cblStyleSizes.Items[i].Value, CommonMethods.SessionInfo.UserName, DateTime.Now);
                }
            }

            return orderManager.SaveOrderSizes(dt);

        }



        public int SaveDeliveryCountry()
        {

            DataTable dt = OrderDeliveryCountries();
            for (int i = 0; i < cblDeliveryCountries.Items.Count; i++)
            {
                if (cblDeliveryCountries.Items[i].Selected)
                {
                    dt.Rows.Add(OrderId, cblDeliveryCountries.Items[i].Value, CommonMethods.SessionInfo.UserName, DateTime.Now);
                }
            }

            return orderManager.SaveOrderDeliveryCountries(dt);

        }


        public DataTable OrderDeliveryCountries()
        {
            DataTable dt = new DataTable("OrderDeliveryCountries");
            dt.Columns.Add("OrderId", typeof(int));
            dt.Columns.Add("DeliveryCountryId", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }


        public DataTable OrderSizes()
        {
            DataTable dt = new DataTable("OrderSizes");
            dt.Columns.Add("OrderId", typeof(int));
            dt.Columns.Add("SizeId", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;

        }


        protected void btnUpdateOrder_Click(object sender, EventArgs e)
        {
            try
            {
                int buyerDepartment = 0;
                int buyerSeason = 0;
                int buyerYear = 0;

                if (ddlBuyerDeprtments.SelectedValue != "")
                {
                    buyerDepartment = Convert.ToInt32(ddlBuyerDeprtments.SelectedValue);
                }
                if (ddlSeason.SelectedValue != "")
                {
                    buyerSeason = Convert.ToInt32(ddlSeason.SelectedValue);
                }

                if (ddlOrderYear.SelectedValue != "")
                {
                    buyerYear = Convert.ToInt32(ddlOrderYear.SelectedValue);
                }


                if (ddlStyle.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a style.');", true);
                }
                else if (ddlBuyerDeprtments.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select Department.');", true);
                }
                else if (tbxOrderNumber.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter order number.');", true);

                }
                else if (tbxOrderDate.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter order date.');", true);
                }
                else if (ddlOrderStatus.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select order status.');", true);
                }
                else if (cblStyleSizes.SelectedIndex == -1)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select sizes for the order.')", true);
                }

                else if (cblDeliveryCountries.SelectedIndex == -1)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please select a delivery country.');", true);
                }
                else
                {
                    var colorQty = 0;

                    for (int l = 0; l < rptColor.Items.Count; l++)
                    {
                        var tbxOrderQtyByColor = (TextBox)rptColor.Items[l].FindControl("tbxOrderQtyByColor");

                        if (!string.IsNullOrEmpty(tbxOrderQtyByColor.Text))
                        {
                            colorQty = int.Parse(tbxOrderQtyByColor.Text);
                            break;
                        }

                    }
           

                    if (colorQty > 0)
                    {
                        var orderInfo = unitOfWork.GenericRepositories<Orders>().GetByID(OrderId);

                        if (orderInfo != null)
                        {
                            orderInfo.BookingId = 0;
                            //Order Updates
                            orderInfo.BuyerId = Convert.ToInt32(ddlBuyers.SelectedValue);
                            orderInfo.BuyerDepartmentId = buyerDepartment;
                            orderInfo.StyleId = Convert.ToInt32(ddlStyle.SelectedValue);
                            orderInfo.OrderNumber = tbxOrderNumber.Text;
                            orderInfo.OrderSeasonId = buyerSeason;
                            orderInfo.OrderYearId = buyerYear;
                            orderInfo.OrderDate = Convert.ToDateTime(tbxOrderDate.Text);
                            orderInfo.ExFactoryDate = Convert.ToDateTime(tbxExFactoryDate.Text);
                            orderInfo.OrderItem = tbxOrderItem.Text;
                            orderInfo.PaymentTerms = "";//tbxPaymentTerm.Text;
                            orderInfo.LCValue = 0;//Convert.ToDecimal(tbxLCValue.Text);
                            orderInfo.StatusId = Convert.ToInt32(ddlOrderStatus.SelectedValue);
                            orderInfo.ApprovedStatus = Convert.ToInt32(ddlStatus.SelectedValue);
                            orderInfo.BookingId = string.IsNullOrEmpty(ddlBookingNumber.SelectedValue) ? 0 : Convert.ToInt32(ddlBookingNumber.SelectedValue);
                            //orderInfo.BookingId = Convert.ToInt32(ddlBookingNumber.SelectedValue);
                            if (Convert.ToInt32(ddlOrderStatus.SelectedValue) == 49 || Convert.ToInt32(ddlOrderStatus.SelectedValue) == 50 || Convert.ToInt32(ddlOrderStatus.SelectedValue) == 51)
                            {
                                orderInfo.IsActive = 0;
                            }

                            orderInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                            orderInfo.UpdateDate = DateTime.Now;
                            unitOfWork.GenericRepositories<Orders>().Update(orderInfo);

                            //Delete and Update Color and Order
                            UpdateColorAndQuantity(orderInfo.Id);

                            //Delete and Update Order Sizes
                            UpdateOrderSizes(orderInfo.Id);

                            //Delete and Update DeliveryCountries
                            UpdateDeliveryCountry(orderInfo.Id);

                            unitOfWork.Save();
                            //UploadFiles(orderInfo.Id);
                            CommonMethods.SendEventNotification(1, orderInfo.BuyerId, orderInfo.StyleId, orderInfo.Id, orderInfo.Id);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('The order dose not exists.');", true);
                        }
                        //Response.AddHeader("REFRESH", "2;URL=ViewOrders.aspx");
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewOrders.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Please enter order quantities for colors.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + ex.Message + "');", true);
            }
        }

        private void UpdateColorAndQuantity(int orderId)
        {
            //delete order color
            var colorsInfo = unitOfWork.GenericRepositories<OrderColors>().Get(x => x.OrderId == orderId);
            foreach (var itemColor in colorsInfo)
            {
                unitOfWork.GenericRepositories<OrderColors>().Delete(itemColor);
            }
            // End of delete

            saveColorsAndQuantity(orderId);
        }


        protected void UpdateOrderSizes(int OrderId)
        {
            var orderSizes = unitOfWork.GenericRepositories<OrderSizes>().Get(x => x.OrderId == OrderId);

            foreach (var item in orderSizes)
            {
                unitOfWork.GenericRepositories<OrderSizes>().Delete(item);
            }
            for (int i = 0; i < cblStyleSizes.Items.Count; i++)
            {
                if (cblStyleSizes.Items[i].Selected)
                {
                    var orderSize = new OrderSizes()
                    {
                        OrderId = OrderId,
                        SizeId = Convert.ToInt32(cblStyleSizes.Items[i].Value),
                        CreatedBy = CommonMethods.SessionInfo.UserName,
                        CreateDate = DateTime.Now
                    };
                    unitOfWork.GenericRepositories<OrderSizes>().Insert(orderSize);
                }
            }
        }


        protected void UpdateDeliveryCountry(int OrderId)
        {
            var deliveryCountris = unitOfWork.GenericRepositories<OrderDeliveryCountries>().Get(x => x.OrderId == OrderId);
            foreach (var item in deliveryCountris)
            {
                unitOfWork.GenericRepositories<OrderDeliveryCountries>().Delete(item);
            }
            for (int i = 0; i < cblDeliveryCountries.Items.Count; i++)
            {
                if (cblDeliveryCountries.Items[i].Selected)
                {
                    var deleveryCountry = new OrderDeliveryCountries()
                    {
                        OrderId = OrderId,
                        DeliveryCountryId = Convert.ToInt32(cblDeliveryCountries.Items[i].Value),
                        CreatedBy = CommonMethods.SessionInfo.UserName,
                        CreateDate = DateTime.Now
                    };
                    unitOfWork.GenericRepositories<OrderDeliveryCountries>().Insert(deleveryCountry);
                }
            }
        }





        protected void btnCheckAllSizes_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in cblStyleSizes.Items)
            {
                li.Selected = true;
            }
        }

        protected void btnUnCheckAllSizes_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in cblStyleSizes.Items)
            {
                li.Selected = false;
            }
        }



        protected void btnSelectAllCountries_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in cblDeliveryCountries.Items)
            {
                li.Selected = true;
            }
        }

        protected void btnUnselectAllCountries_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in cblDeliveryCountries.Items)
            {
                li.Selected = false;
            }
        }

        public static void LoadDropdown(DropDownList ddl, string table, int dataTextField, int dataValueField, int selectedIndexValue = 0)
        {
            DataTable dt = new CommonManager().LoadDropdown(table);
            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            //ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = selectedIndexValue;
        }
        protected void LoadStyleSizes(int styleId)
        {

            pnlStyleSizes.Visible = true;

            //DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetSizesByBuyer '{buyerId}'");

            DataTable dt = buyerManager.GetStyleSizes(styleId);

            if (dt.Rows.Count > 0)
            {
                cblStyleSizes.DataValueField = "Id";
                cblStyleSizes.DataTextField = "SizeName";
                cblStyleSizes.DataSource = dt;
                cblStyleSizes.DataBind();
            }
        }

    }
}