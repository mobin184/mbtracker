﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class ManageColors : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableDeleteButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteColor(ColorId);
                }
            }
        }

        private void DeleteColor(int colorId)
        {
            try
            {
                var color = unitOfWork.GenericRepositories<BuyerColors>().GetByID(colorId);
                color.IsActive = false;
                color.UpdatedBy = CommonMethods.SessionInfo.UserName;
                color.UpdateDate = DateTime.Now;
                unitOfWork.GenericRepositories<BuyerColors>().Update(color);
                unitOfWork.Save();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
                BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
                ClearFormData();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private void EnableDisableDeleteButton()
        {
            //ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ManageColors", 2, 1);
        }

        int ColorId
        {
            set { ViewState["colorId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["colorId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue != "")
            {
                BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            else
            {
                pnlColors.Visible = false;
            }
        }

        private void BindData(int buyerId)
        {
            var dt = buyerManager.LoadColorsByBuyer(buyerId);
            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                pnlColors.Visible = true;
            }
            else
            {
                pnlColors.Visible = false;
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlBuyers.SelectedValue != "")
                {
                    var buyerId = int.Parse(ddlBuyers.SelectedValue);
                    //if (tbxColorCode.Text.Trim() != "")
                    //{
                    var colorCode = tbxColorCode.Text.Trim();
                    if (tbxColorDescription.Text.Trim() != "")
                    {
                        var colorDes = tbxColorDescription.Text.Trim();
                        var isExist = unitOfWork.GenericRepositories<BuyerColors>().IsExist(x => x.BuyerId == buyerId && x.ColorCode == colorCode && x.ColorDescription == colorDes);
                        if (!isExist)
                        {
                            var color = new BuyerColors()
                            {
                                BuyerId = buyerId,
                                ColorCode = colorCode,
                                ColorDescription = colorDes,
                                IsActive = true,
                                CreatedBy = CommonMethods.SessionInfo.UserName,
                                CreateDate = DateTime.Now
                            };
                            unitOfWork.GenericRepositories<BuyerColors>().Insert(color);
                            unitOfWork.Save();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
                            ClearFormData();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Color already exist.')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter color description.')", true);
                    }
                    //}
                    //else
                    //{
                    //    ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please enter color code.')", true);
                    //}
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a buyer.')", true);
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (ddlBuyers.SelectedValue != "")
            {
                var buyerId = int.Parse(ddlBuyers.SelectedValue);
                //if (tbxColorCode.Text.Trim() != "")
                //{
                var colorCode = tbxColorCode.Text.Trim();
                if (tbxColorDescription.Text.Trim() != "")
                {
                    var colorDes = tbxColorDescription.Text.Trim();

                    var colorInfo = unitOfWork.GenericRepositories<BuyerColors>().GetByID(ColorId);
                    if (colorInfo != null)
                    {
                        colorInfo.ColorCode = colorCode;
                        colorInfo.ColorDescription = colorDes;
                        colorInfo.BuyerId = buyerId;
                        colorInfo.UpdateDate = DateTime.Now;
                        colorInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                        unitOfWork.GenericRepositories<BuyerColors>().Update(colorInfo);
                        unitOfWork.Save();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                        BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
                        ClearFormData();


                        styleActionTitle.Text = "Add a New Color:";
                        btnSave.Visible = true;
                        btnUpdate.Visible = false;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Invalid color Id.')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter color description.')", true);
                }
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this,GetType(), "toastr_message", "toastr.error('Please enter color code.')", true);
                //}
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select a buyer.')", true);
            }


        }


        private void ClearFormData()
        {
            tbxColorCode.Text = string.Empty;
            tbxColorDescription.Text = string.Empty;
        }


        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int buyerColorId = Convert.ToInt32(e.CommandArgument.ToString());
                PopulateColor(buyerColorId);
            }
            else if (e.CommandName == "Delete")
            {
                ColorId = Convert.ToInt32(e.CommandArgument.ToString());

                var colorCannotBeDeleted = unitOfWork.GenericRepositories<viewBuyerColorsCanNotBeDeleted>().GetByID(ColorId);
                if (colorCannotBeDeleted == null)
                {
                    var title = "Warning";
                    var msg = "Are you sure you want to delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                }
                else
                {
                    var title = "Warning";
                    var msg = "Sorry, you can not delete it.<br />There was some dependencies of this color.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                }
            }
        }

        private void PopulateColor(int buyerColorId)
        {
            var colorInfo = unitOfWork.GenericRepositories<BuyerColors>().GetByID(buyerColorId);
            if (colorInfo != null)
            {
                ColorId = colorInfo.Id;
                tbxColorCode.Text = colorInfo.ColorCode;
                tbxColorDescription.Text = colorInfo.ColorDescription;
                ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, colorInfo.BuyerId);
                styleActionTitle.Text = "Update Color:";
                btnSave.Visible = false;
                btnUpdate.Visible = true;
                styleActionTitle.Text = "Update Color Info:";
            }
        }
    }
}