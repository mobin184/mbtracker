﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddTimeAndActionPlan.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.AddTimeAndActionPlan" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        input[type="radio"], input[type="checkbox"] {
            margin: -3px 0 0;
        }

        label {
            display: inline;
            padding-left: 3px !important;
        }

        td, th {
            padding: 5px;
        }

        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 7px;
        }

        .table {
            width: 100%;
            margin-bottom: 5px;
        }

        /* Removes the spin button */
        input[type="date"]::-webkit-clear-button {
            display: none;
        }

        /* Removes the spin button */
        input[type="date"]::-webkit-inner-spin-button {
            display: none;
        }

        /*[type="date"]::-webkit-calendar-picker-indicator {
            opacity: 1;
        }*/
    </style>
    <div class="row-fluid">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="Time & Action Plan:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 10px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputStyle" class="control-label">
                                <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputStyle" class="control-label">
                                <asp:Label ID="Label12" runat="server" Text="Select Year:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlYear"><span style="font-weight: 700; color: #CC0000">Please select year.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="span3">
        </div>
        <div class="span3" runat="server" id="pnlSummary" visible="false">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="Label1" Text="Booking Information:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class=" controls-row">
                        <table class="table table-bordered">
                            <tr>
                                <td>Booked Quantity:</td>
                                <td>
                                    <asp:Label runat="server" ID="lblBookedQuantity"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Yarn Information:</td>
                                <td>
                                    <asp:Label runat="server" ID="lblYarnConsumptionCountAndPly"></asp:Label></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="span12" style="margin-left: 0px" runat="server" id="pnlTimeAndActionInfo" visible="false">
            <div class="widget"  >
                <div class="widget-body" style="overflow-x: auto">
                    <asp:Panel runat="server">
                        <div class="control-group">
                            <label for="inputDeliveryCountry" class="control-label" style="text-align: left">
                                <asp:Label ID="lblColorAndSize" Font-Bold="true" runat="server" Text="Time & Aciton Info:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                        </div>
                        <div class=" controls-row">
                            <div id="dt_example" class="example_alt_pagination">
                                <asp:Repeater ID="rpt" runat="server" OnItemDataBound="rpt_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="">
                                                        <asp:Label ID="lbl1" runat="server" Text="Delivery<br/>Quantity<br/>&nbsp;"></asp:Label></th>
                                                    <th style="">
                                                        <asp:Label ID="Label3" runat="server" Text="Delivery<br/>Date<br/>&nbsp;"></asp:Label></th>
                                                    <th style="">
                                                        <asp:Label ID="Label2" runat="server" Text="Yarn In-house<br/>Date<br/>&nbsp;"></asp:Label></th>
                                                    <th style="">
                                                        <asp:Label ID="Label4" runat="server" Text="PP Approval<br/>Date<br/>&nbsp;"></asp:Label></th>
                                                    <th style="">
                                                        <asp:Label ID="Label5" runat="server" Text="Sample<br/>Approval Date<br/>&nbsp;"></asp:Label></th>
                                                    <th style="">
                                                        <asp:Label ID="Label6" runat="server" Text="Knitting Start<br/>Deadline Date<br/>&nbsp;"></asp:Label></th>
                                                    <th style="">
                                                        <asp:Label ID="Label8" runat="server" Text="Main,Size & Care Label<br/>In-house Date"></asp:Label></th>
                                                    <th style="">
                                                        <asp:Label ID="Label9" runat="server" Text="Hang,Price Tag<br/>In-house Date<br/>&nbsp;"></asp:Label></th>
                                                    <th style="">
                                                        <asp:Label ID="Label10" runat="server" Text="Button,Zipper<br/>In-house Date<br/>&nbsp;"></asp:Label></th>
                                                     <th style="">
                                                        <asp:Label ID="Label13" runat="server" Text="Badge,Flower,Fabrics<br/>In-house Date<br/>&nbsp;"></asp:Label></th>
                                                    <th style="">
                                                        <asp:Label ID="Label14" runat="server" Text="Print,Embroidery<br/>In-house Date<br/>&nbsp;"></asp:Label></th>
                                                    <th style="">
                                                        <asp:Label ID="Label15" runat="server" Text="Sewing, Shoulder Tap<br/>In-house Date"></asp:Label></th>
                                                     <th style="">
                                                        <asp:Label ID="Label16" runat="server" Text="Poly<br/>In-house Date<br/>&nbsp;"></asp:Label></th>
                                                    <th style="">
                                                        <asp:Label ID="Label18" runat="server" Text="Carton <br/>In-house Date<br/>&nbsp;"></asp:Label></th>
                                                     <th style="">
                                                        <asp:Label ID="Label7" runat="server" Text="Addi. Wrok Acc.<br/>In-house Date<br/>&nbsp;"></asp:Label></th>
                                                    <th style="">
                                                        <asp:Label ID="Label11" runat="server" Text="Other <br/>Deadline Date<br/>&nbsp;"></asp:Label></th>
                                                     <th style="width:150px">
                                                        <asp:Label ID="Label17" runat="server" Text="Remarks <br/>&nbsp;"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblDeleteRow" runat="server" Text="Take<br/>Action<br/>&nbsp;"></asp:Label></th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbxQuantity" TextMode="Number" CssClass="dummyClass" Height="30" runat="server" Style="min-width: 105px;"></asp:TextBox>
                                            </td>
                                            <td style="padding-left: 2px; padding-right: 2px;">
                                                <asp:TextBox ID="tbxDeliveryDate" TextMode="Date" CssClass="dummyClass" Height="30" runat="server" Style="min-width: 50px; width: 100%"></asp:TextBox>
                                            </td>
                                            <td style="padding-left: 2px; padding-right: 2px;">
                                                <asp:TextBox ID="tbxYarnInHouseDate" TextMode="Date" CssClass="dummyClass" Height="30" runat="server" Style="min-width: 50px; width: 100%"></asp:TextBox>
                                            </td>
                                            <td style="padding-left: 2px; padding-right: 2px;">
                                                <asp:TextBox ID="tbxPPApprovalDate" TextMode="Date" CssClass="dummyClass" Height="30" runat="server" Style="min-width: 50px; width: 100%"></asp:TextBox>
                                            </td>
                                            <td style="padding-left: 2px; padding-right: 2px;">
                                                <asp:TextBox ID="tbxSampleApprovalDate" TextMode="Date" CssClass="dummyClass" Height="30" runat="server" Style="min-width: 50px; width: 100%"></asp:TextBox>
                                            </td>
                                            <td style="padding-left: 2px; padding-right: 2px;">
                                                <asp:TextBox ID="tbxKnittingStartDeadlineDate" TextMode="Date" CssClass="dummyClass" Height="30" runat="server" Style="min-width: 50px; width: 100%"></asp:TextBox>
                                            </td>

                                            <td style="padding-left: 2px; padding-right: 2px;">
                                                <asp:TextBox ID="tbxmsclabelInshouseDate" TextMode="Date" CssClass="dummyClass" Height="30" runat="server" Style="min-width: 50px; width: 100%"></asp:TextBox>
                                            </td>
                                           
                                            <td style="padding-left: 2px; padding-right: 2px;">
                                                <asp:TextBox ID="tbxhpTagInHouseDate" TextMode="Date" CssClass="dummyClass" Height="30" runat="server" Style="min-width: 50px; width: 100%"></asp:TextBox>
                                            </td>
                                            <td style="padding-left: 2px; padding-right: 2px;">
                                                <asp:TextBox ID="tbxbzInHouseDate" TextMode="Date" CssClass="dummyClass" Height="30" runat="server" Style="min-width: 50px; width: 100%"></asp:TextBox>
                                            </td>
                                            <td style="padding-left: 2px; padding-right: 2px;">
                                                <asp:TextBox ID="tbxbffInHouseDate" TextMode="Date" CssClass="dummyClass" Height="30" runat="server" Style="min-width: 50px; width: 100%"></asp:TextBox>
                                            </td>
                                           <td style="padding-left: 2px; padding-right: 2px;">
                                                <asp:TextBox ID="tbxpeInhouseDate" TextMode="Date" CssClass="dummyClass" Height="30" runat="server" Style="min-width: 50px; width: 100%"></asp:TextBox>
                                            </td>
                                            <td style="padding-left: 2px; padding-right: 2px;">
                                                <asp:TextBox ID="tbxssTagInhouseDate" TextMode="Date" CssClass="dummyClass" Height="30" runat="server" Style="min-width: 50px; width: 100%"></asp:TextBox>
                                            </td>
                                            <td style="padding-left: 2px; padding-right: 2px;">
                                                <asp:TextBox ID="tbxPloyInhouseDate" TextMode="Date" CssClass="dummyClass" Height="30" runat="server" Style="min-width: 50px; width: 100%"></asp:TextBox>
                                            </td>
                                           <td style="padding-left: 2px; padding-right: 2px;">
                                                <asp:TextBox ID="tbxCartoonInhouseDate" TextMode="Date" CssClass="dummyClass" Height="30" runat="server" Style="min-width: 50px; width: 100%"></asp:TextBox>
                                            </td>
                                            <td style="padding-left: 2px; padding-right: 2px;">
                                                <asp:TextBox ID="tbxAdditionalWorkAccInHouseDate" TextMode="Date" CssClass="dummyClass" Height="30" runat="server" Style="min-width: 50px; width: 100%"></asp:TextBox>
                                            </td>
                                            <td style="padding-left: 2px; padding-right: 2px;">
                                                <asp:TextBox ID="tbxOtherDeadlineDate" TextMode="Date" CssClass="dummyClass" Height="30" runat="server" Style="min-width: 50px; width: 100%"></asp:TextBox>
                                            </td>
                                             <td style="padding-left: 2px; padding-right: 2px;">
                                                <asp:TextBox ID="tbxNote" TextMode="multiline" CssClass="dummyClass" Height="60" Rows="2" runat="server" Style="max-width: 250px; width: 250px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Container.ItemIndex %>' class="btn btn-danger btn-mini pull-right " OnCommand="btnDelete_Command" Text="Delete Row"></asp:LinkButton>
                                                <asp:Button ID="btnUpdate" Visible="false" runat="server" class="btn btn-success btn-mini pull-right " Text="Update" OnClick="btnUpdate_Click"></asp:Button>
                                            </td>

                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                                </table>
                                                  <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-mini pull-left btnStyle" Style="margin-right: 8px" Text="&nbsp;&nbsp;Add Row&nbsp;&nbsp;" OnClick="btnAddRow_Click" />
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Label ID="lblNoColorFound" runat="server" Text="This style does not have any colors!" Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </asp:Panel>
                </div>
            </div>
            <div class="control-group">
                <div class="controls-row" style="padding-top:0px;">
                    <asp:Button ID="btnSave" runat="server" style="margin-right:15px;" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSave_Click" />
                    <%--<asp:Button ID="btnUpdate" Visible="false" runat="server" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdate_Click" />--%>
                </div>
            </div>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
