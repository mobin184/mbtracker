﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class AddTimeAndActionPlanOld : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                CommonMethods.LoadDropdown(ddlFinishingUnit, "FinishingUnits", 1, 0);
                CommonMethods.LoadDropdown(ddlLinkingMCGauge, "LinkingMachineGauges", 1, 0);
                LoadMachinBrand();
                if (Request.QueryString["TNAId"] != null)
                {
                    TNAId = int.Parse(Tools.UrlDecode(Request.QueryString["TNAId"]));
                    PopulateDataForUpdate(TNAId);
                    reqFileUpload1.Enabled = false;
                    reqFileUpload2.Enabled = false;
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                    redAsterisk1.Visible = false;
                    redAsterisk2.Visible = false;
                }

            }
        }

        private void PopulateDataForUpdate(int tNAId)
        {
            //try
            //{
            //    var tnaInfo = unitOfWork.GenericRepositories<TimeAndActionPlans>().GetByID(tNAId);
            //    BindStylesByBuyer(tnaInfo.BuyerId);
            //    CommonMethods.LoadDropdown(ddlYear, "Years WHERE IsActive = 1", 1, 0);
            //    BindSeasonByBuyer(tnaInfo.BuyerId);
            //    CommonMethods.LoadOrderDropdownByStyle(ddlOrder, tnaInfo.StyleId, 1, 0);

            //    //ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, tnaInfo.BuyerId);
            //    //ddlStyle.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyle, tnaInfo.StyleId);
            //    //ddlYear.SelectedIndex = CommonMethods.MatchDropDownItem(ddlYear, tnaInfo.YearId ?? 0);
            //    //ddlSeason.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSeason, tnaInfo.SeasonId ?? 0);
            //    //tbxBookedCapacityQty.Text = tnaInfo.BookedCapacityQuantity + "";
            //    //ddlOrder.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrder, tnaInfo.OrderId ?? 0);
            //    //tbxOrderQuantity.Text = tnaInfo.OrderQuantity + "";
            //    //tbxOrderSheetReceivedDate.Text = tnaInfo.OrderSheetReceivedDate?.ToString("yyyy-MM-dd");
            //    //tbxInitialShipmentDate.Text = tnaInfo.InitialShipmentDate?.ToString("yyyy-MM-dd");
            //    //tbxExtendedShipmentDate.Text = tnaInfo.ExtendedShipmentDate?.ToString("yyyy-MM-dd");
            //    //tbxFOB.Text = tnaInfo.FOB;
            //    //tbxTotalFOB.Text = tnaInfo.TotalFOB;
            //    //tbxActualExFactory.Text = tnaInfo.ActualExFactoryDate?.ToString("yyyy-MM-dd");
            //    //tbxPlusShipQty.Text = tnaInfo.PlusShipQty + "";
            //    //tbxLessShipQty.Text = tnaInfo.LessShipQty + "";
            //    //tbxMasterLcOrContactReceivedDate.Text = tnaInfo.MasterLCOrContactReceivedDate?.ToString("yyyy-MM-dd");
            //    //tbxMasterLCValue.Text = tnaInfo.MasterLCValue + "";
            //    //tbxShipmentValue.Text = tnaInfo.ShipmentValue + "";
            //    //ddlFinishingUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlFinishingUnit, tnaInfo.FinishingUnitId ?? 0);
            //    //tbxYarnComCountAndPly.Text = tnaInfo.YarnComCountAndPly;
            //    //ddlSampleMCBrand.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSampleMCBrand, tnaInfo.SampleMCBrandId ?? 0);
            //    //tbxSampleTiming.Text = tnaInfo.SampleTiming + "";
            //    //tbxKnittingOutput.Text = tnaInfo.KnittingOutput;
            //    //tbxKnittingMCGauge.Text = tnaInfo.KnittingMCGauge + "";
            //    //ddlLinkingMCGauge.SelectedIndex = CommonMethods.MatchDropDownItem(ddlLinkingMCGauge, tnaInfo.LinkingMCGaugeId ?? 0);
            //    //tbxLinkingTiming.Text = tnaInfo.LinkingTiming + "";
            //    //tbxLinkingOutput.Text = tnaInfo.LinkingOutput + "";
            //    //tbxWeightPerDz.Text = tnaInfo.WeightPerDz + "";
            //    //tbxDyeingOrderPlaceDate.Text = tnaInfo.DyeingOrderPlaceDate?.ToString("yyyy-MM-dd");
            //    //tbxYarnSupplier.Text = tnaInfo.YarnSupplier;
            //    //tbxPIReceivedDate.Text = tnaInfo.PIReceivedDate?.ToString("yyyy-MM-dd");
            //    //tbxPINumber.Text = tnaInfo.PINumber;
            //    //tbxPIValue.Text = tnaInfo.PIValue + "";
            //    //tbxYarnETD.Text = tnaInfo.YarnETD?.ToString("yyyy-MM-dd");
            //    //tbxYarnETA.Text = tnaInfo.YarnETA?.ToString("yyyy-MM-dd");
            //    //tbxYarnInHouseDate.Text = tnaInfo.YarnInHouseDate?.ToString("yyyy-MM-dd");
            //    //tbxKnittingAccessoriesInHouseDate.Text = tnaInfo.KnittingAccessoriesInHouseDate?.ToString("yyyy-MM-dd");
            //    //tbxLinkingAdditionalInHouseDate.Text = tnaInfo.LinkingAdditionalInHouseDate?.ToString("yyyy-MM-dd");
            //    //tbxMainSizeCareBookingDate.Text = tnaInfo.MainSizeCareLabelBookingDate?.ToString("yyyy-MM-dd");
            //    //tbxMainSizeCarePIReceivedDate.Text = tnaInfo.MainSizeCareLabelPIReceivedDate?.ToString("yyyy-MM-dd");
            //    //tbxMainSizeCarePINumber.Text = tnaInfo.MainSizeCareLabelPINumber;
            //    //tbxMainSizeCarePIValue.Text = tnaInfo.MainSizeCareLabelPIValue + "";
            //    //tbxMainSizeCarePISignAndSendToCom.Text = tnaInfo.MainSizeCareLabelPISendToCommercialDate?.ToString("yyyy-MM-dd");
            //    //tbxMainSizeCareLCOpenDate.Text = tnaInfo.MainSizeCareLabelLCOpenDate?.ToString("yyyy-MM-dd");
            //    //tbxMainSizeCareGoodsInHouseDate.Text = tnaInfo.MainSizeCareLabelGoodsInHouseDate?.ToString("yyyy-MM-dd");
            //    //tbxHangOrPriceTagBookingDate.Text = tnaInfo.HangOrPriceTagBookingDate?.ToString("yyyy-MM-dd");
            //    //tbxHangOrPriceTagPIReceivedDate.Text = tnaInfo.HangOrPriceTagPIReceivedDate?.ToString("yyyy-MM-dd");
            //    //tbxHangOrPriceTagPINumber.Text = tnaInfo.HangOrPriceTagPINumber;
            //    //tbxHangOrPriceTagPIValue.Text = tnaInfo.HangOrPriceTagPIValue + "";
            //    //tbxHangOrPriceTagPISignAndSendToCom.Text = tnaInfo.HangOrPriceTagPISendToCommercialDate?.ToString("yyyy-MM-dd");
            //    //tbxHangOrPriceTagLCOpenDate.Text = tnaInfo.HangOrPriceTagLCOpenDate?.ToString("yyyy-MM-dd");
            //    //tbxHangOrPriceTagGoodsInHouseDate.Text = tnaInfo.HangOrPriceTagGoodsInHouseDate?.ToString("yyyy-MM-dd");
            //    //tbxButtonZipperBookingDate.Text = tnaInfo.ButtonZipperBookingDate?.ToString("yyyy-MM-dd");
            //    //tbxButtonZipperPIReceivedDate.Text = tnaInfo.ButtonZipperPIReceivedDate?.ToString("yyyy-MM-dd");
            //    //tbxButtonZipperPINumber.Text = tnaInfo.ButtonZipperPINumber;
            //    //tbxButtonZipperPIValue.Text = tnaInfo.ButtonZipperPIValue + "";
            //    //tbxButtonZipperPISignAndSendToCom.Text = tnaInfo.ButtonZipperPISendToCommercialDate?.ToString("yyyy-MM-dd");
            //    //tbxButtonZipperLCOpenDate.Text = tnaInfo.ButtonZipperLCOpenDate?.ToString("yyyy-MM-dd");
            //    //tbxButtonZipperGoodsInHouseDate.Text = tnaInfo.ButtonZipperGoodsInHouseDate?.ToString("yyyy-MM-dd");
            //    //tbxBadgeFlowerFabricsBookingDate.Text = tnaInfo.BadgeFlowerFabricsBookingDate?.ToString("yyyy-MM-dd");
            //    //tbxBadgeFlowerFabricsPIReceivedDate.Text = tnaInfo.BadgeFlowerFabricsPIReceivedDate?.ToString("yyyy-MM-dd");
            //    //tbxBadgeFlowerFabricsPINumber.Text = tnaInfo.BadgeFlowerFabricsPINumber;
            //    //tbxBadgeFlowerFabricsPIValue.Text = tnaInfo.BadgeFlowerFabricsPIValue + "";
            //    //tbxBadgeFlowerFabricsPISignAndSendToCom.Text = tnaInfo.BadgeFlowerFabricsPISendToCommercialDate?.ToString("yyyy-MM-dd");
            //    //tbxBadgeFlowerFabricsLCOpenDate.Text = tnaInfo.BadgeFlowerFabricsLCOpenDate?.ToString("yyyy-MM-dd");
            //    //tbxBadgeFlowerFabricsGoodsInHouseDate.Text = tnaInfo.BadgeFlowerFabricsGoodsInHouseDate?.ToString("yyyy-MM-dd");
            //    //tbxPrintEmbroideryBookingDate.Text = tnaInfo.PrintEmbroideryBookingDate?.ToString("yyyy-MM-dd");
            //    //tbxPrintEmbroideryPIReceivedDate.Text = tnaInfo.HangOrPriceTagPIReceivedDate?.ToString("yyyy-MM-dd");
            //    //tbxPrintEmbroideryPINumber.Text = tnaInfo.PrintEmbroideryPINumber;
            //    //tbxPrintEmbroideryPIValue.Text = tnaInfo.PrintEmbroideryPIValue + "";
            //    //tbxPrintEmbroideryPISignAndSendToCom.Text = tnaInfo.PrintEmbroideryPISendToCommercialDate?.ToString("yyyy-MM-dd");
            //    //tbxPrintEmbroideryLCOpenDate.Text = tnaInfo.PrintEmbroideryLCOpenDate?.ToString("yyyy-MM-dd");
            //    //tbxPrintEmbroideryGoodsInHouseDate.Text = tnaInfo.PrintEmbroideryGoodsInHouseDate?.ToString("yyyy-MM-dd");
            //    //tbxSewingShoulderTapBookingDate.Text = tnaInfo.SewingShoulderTapBookingDate?.ToString("yyyy-MM-dd");
            //    //tbxSewingShoulderTapPIReceivedDate.Text = tnaInfo.SewingShoulderTapPIReceivedDate?.ToString("yyyy-MM-dd");
            //    //tbxSewingShoulderTapPINumber.Text = tnaInfo.SewingShoulderTapPINumber;
            //    //tbxSewingShoulderTapPIValue.Text = tnaInfo.SewingShoulderTapPIValue + "";
            //    //tbxSewingShoulderTapPISignAndSendToCom.Text = tnaInfo.SewingShoulderTapPISendToCommercialDate?.ToString("yyyy-MM-dd");
            //    //tbxSewingShoulderTapLCOpenDate.Text = tnaInfo.SewingShoulderTapLCOpenDate?.ToString("yyyy-MM-dd");
            //    //tbxSewingShoulderTapGoodsInHouseDate.Text = tnaInfo.SewingShoulderTapGoodsInHouseDate?.ToString("yyyy-MM-dd");
            //    //tbxPolyBookingDate.Text = tnaInfo.PolyBookingDate?.ToString("yyyy-MM-dd");
            //    //tbxPolyPIReceivedDate.Text = tnaInfo.PolyPIReceivedDate?.ToString("yyyy-MM-dd");
            //    //tbxPolyPINumber.Text = tnaInfo.PolyPINumber;
            //    //tbxPolyPIValue.Text = tnaInfo.PolyPIValue + "";
            //    //tbxPolyPISignAndSendToCom.Text = tnaInfo.PolyPISendToCommercialDate?.ToString("yyyy-MM-dd");
            //    //tbxPolyLCOpenDate.Text = tnaInfo.PolyLCOpenDate?.ToString("yyyy-MM-dd");
            //    //tbxPolyGoodsInHouseDate.Text = tnaInfo.PolyGoodsInHouseDate?.ToString("yyyy-MM-dd");
            //    //tbxCartoonBookingDate.Text = tnaInfo.CartoonBookingDate?.ToString("yyyy-MM-dd");
            //    //tbxCartoonPIReceivedDate.Text = tnaInfo.CartoonPIReceivedDate?.ToString("yyyy-MM-dd");
            //    //tbxCartoonPINumber.Text = tnaInfo.CartoonPINumber;
            //    //tbxCartoonPIValue.Text = tnaInfo.CartoonPIValue + "";
            //    //tbxCartoonPISignAndSendToCom.Text = tnaInfo.CartoonPISendToCommercialDate?.ToString("yyyy-MM-dd");
            //    //tbxCartoonLCOpenDate.Text = tnaInfo.CartoonLCOpenDate?.ToString("yyyy-MM-dd");
            //    //tbxCartoonGoodsInHouseDate.Text = tnaInfo.CartoonGoodsInHouseDate?.ToString("yyyy-MM-dd");


            //}
            //catch (Exception ex)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            //}
        }

        int TNAId
        {
            set { ViewState["tnaId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["tnaId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        private void LoadMachinBrand()
        {
            var itemList = new OrderManager().GetMachineBrands();
            List<ListItem> lstMachinBrand = new List<ListItem>() { new ListItem() { Text = "---Select---", Value = "" } };
            lstMachinBrand.AddRange(itemList.Rows.OfType<DataRow>().Select(a => new ListItem() { Text = a[1].ToString(), Value = a[0].ToString() }).ToList());
            ddlSampleMCBrand.DataTextField = "MachineBrand";
            ddlSampleMCBrand.DataValueField = "Id";
            ddlSampleMCBrand.Items.AddRange(lstMachinBrand.ToArray());
            ddlSampleMCBrand.SelectedIndex = 0;
        }
        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBuyers.SelectedValue != "")
            {
                BindStylesByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
                CommonMethods.LoadDropdown(ddlYear, "Years WHERE IsActive = 1", 1, 0);
            }
            else
            {
                ddlStyle.Items.Clear();
                ddlOrder.Items.Clear();
                ddlYear.Items.Clear();
                ddlSeason.Items.Clear();
            }

        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlYear.SelectedValue != "")
            {
                BindSeasonByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            else
            {
                ddlSeason.Items.Clear();
            }
        }

        private void BindStylesByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdownById(ddlStyle, buyerId, "BuyerStyles", 1, 0);
        }

        private void BindSeasonByBuyer(int buyerId)
        {
            CommonMethods.LoadDropdown(ddlSeason, "BuyerSeasons WHERE BuyerId = " + buyerId, 1, 0);
        }

        protected void ddlStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlStyle.SelectedValue != "")
            {
                CommonMethods.LoadOrderDropdownByStyle(ddlOrder, Convert.ToInt32(ddlStyle.SelectedValue), 1, 0);
            }
            else
            {
                ddlOrder.Items.Clear();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    TimeAndActionPlans timeAndActionPlan = new TimeAndActionPlans();
            //    timeAndActionPlan.BuyerId = int.Parse(ddlBuyers.SelectedValue);
            //    timeAndActionPlan.StyleId = int.Parse(ddlStyle.SelectedValue);
            //    //timeAndActionPlan.YearId = string.IsNullOrEmpty(ddlYear.SelectedValue) ? (int?)null : int.Parse(ddlYear.SelectedValue);
            //    //timeAndActionPlan.SeasonId = string.IsNullOrEmpty(ddlSeason.SelectedValue) ? (int?)null : int.Parse(ddlSeason.SelectedValue);
            //    //timeAndActionPlan.BookedCapacityQuantity = string.IsNullOrEmpty(tbxBookedCapacityQty.Text) ? (int?)null : int.Parse(tbxBookedCapacityQty.Text);
            //    //timeAndActionPlan.OrderId = string.IsNullOrEmpty(ddlOrder.SelectedValue) ? (int?)null : int.Parse(ddlOrder.SelectedValue);
            //    //timeAndActionPlan.OrderQuantity = string.IsNullOrEmpty(tbxOrderQuantity.Text) ? (int?)null : int.Parse(tbxOrderQuantity.Text);
            //    //timeAndActionPlan.OrderSheetReceivedDate = string.IsNullOrEmpty(tbxOrderSheetReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxOrderSheetReceivedDate.Text);
            //    //timeAndActionPlan.InitialShipmentDate = string.IsNullOrEmpty(tbxInitialShipmentDate.Text) ? (DateTime?)null : DateTime.Parse(tbxInitialShipmentDate.Text);
            //    //timeAndActionPlan.ExtendedShipmentDate = string.IsNullOrEmpty(tbxExtendedShipmentDate.Text) ? (DateTime?)null : DateTime.Parse(tbxExtendedShipmentDate.Text);
            //    //timeAndActionPlan.FOB = tbxFOB.Text;
            //    //timeAndActionPlan.TotalFOB = tbxTotalFOB.Text;
            //    //timeAndActionPlan.ActualExFactoryDate = string.IsNullOrEmpty(tbxActualExFactory.Text) ? (DateTime?)null : DateTime.Parse(tbxActualExFactory.Text);
            //    //timeAndActionPlan.PlusShipQty = string.IsNullOrEmpty(tbxPlusShipQty.Text) ? (int?)null : int.Parse(tbxPlusShipQty.Text);
            //    //timeAndActionPlan.LessShipQty = string.IsNullOrEmpty(tbxLessShipQty.Text) ? (int?)null : int.Parse(tbxLessShipQty.Text);
            //    //timeAndActionPlan.MasterLCOrContactReceivedDate = string.IsNullOrEmpty(tbxMasterLcOrContactReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxMasterLcOrContactReceivedDate.Text);
            //    //timeAndActionPlan.MasterLCValue = string.IsNullOrEmpty(tbxMasterLCValue.Text) ? (decimal?)null : decimal.Parse(tbxMasterLCValue.Text);
            //    //timeAndActionPlan.ShipmentValue = string.IsNullOrEmpty(tbxShipmentValue.Text) ? (decimal?)null : decimal.Parse(tbxShipmentValue.Text);
            //    //timeAndActionPlan.FinishingUnitId = string.IsNullOrEmpty(ddlFinishingUnit.SelectedValue) ? (int?)null : int.Parse(ddlFinishingUnit.SelectedValue);
            //    //timeAndActionPlan.YarnComCountAndPly = tbxYarnComCountAndPly.Text;
            //    //timeAndActionPlan.SampleMCBrandId = string.IsNullOrEmpty(ddlSampleMCBrand.SelectedValue) ? (int?)null : int.Parse(ddlSampleMCBrand.SelectedValue);
            //    //timeAndActionPlan.SampleTiming = string.IsNullOrEmpty(tbxSampleTiming.Text) ? (int?)null : int.Parse(tbxSampleTiming.Text);
            //    //timeAndActionPlan.KnittingOutput = tbxKnittingOutput.Text;
            //    //timeAndActionPlan.KnittingMCGauge = string.IsNullOrEmpty(tbxKnittingMCGauge.Text) ? (int?)null : int.Parse(tbxKnittingMCGauge.Text);
            //    //timeAndActionPlan.LinkingMCGaugeId = string.IsNullOrEmpty(ddlLinkingMCGauge.SelectedValue) ? (int?)null : int.Parse(ddlLinkingMCGauge.SelectedValue);
            //    //timeAndActionPlan.LinkingTiming = string.IsNullOrEmpty(tbxLinkingTiming.Text) ? (int?)null : int.Parse(tbxLinkingTiming.Text);
            //    //timeAndActionPlan.LinkingOutput = tbxLinkingOutput.Text;
            //    //timeAndActionPlan.WeightPerDz = string.IsNullOrEmpty(tbxWeightPerDz.Text) ? (decimal?)null : decimal.Parse(tbxWeightPerDz.Text);
            //    //timeAndActionPlan.DyeingOrderPlaceDate = string.IsNullOrEmpty(tbxDyeingOrderPlaceDate.Text) ? (DateTime?)null : DateTime.Parse(tbxDyeingOrderPlaceDate.Text);
            //    //timeAndActionPlan.YarnSupplier = tbxYarnSupplier.Text;
            //    //timeAndActionPlan.PIReceivedDate = string.IsNullOrEmpty(tbxPIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPIReceivedDate.Text);
            //    //timeAndActionPlan.PINumber = tbxPINumber.Text;
            //    //timeAndActionPlan.PIValue = string.IsNullOrEmpty(tbxPIValue.Text) ? (decimal?)null : decimal.Parse(tbxPIValue.Text);
            //    //timeAndActionPlan.YarnETD = string.IsNullOrEmpty(tbxYarnETD.Text) ? (DateTime?)null : DateTime.Parse(tbxYarnETD.Text);
            //    //timeAndActionPlan.YarnETA = string.IsNullOrEmpty(tbxYarnETA.Text) ? (DateTime?)null : DateTime.Parse(tbxYarnETA.Text);
            //    //timeAndActionPlan.YarnInHouseDate = string.IsNullOrEmpty(tbxYarnInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxYarnInHouseDate.Text);
            //    //timeAndActionPlan.KnittingAccessoriesInHouseDate = string.IsNullOrEmpty(tbxKnittingAccessoriesInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxKnittingAccessoriesInHouseDate.Text);
            //    //timeAndActionPlan.LinkingAdditionalInHouseDate = string.IsNullOrEmpty(tbxLinkingAdditionalInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxLinkingAdditionalInHouseDate.Text);
            //    //timeAndActionPlan.MainSizeCareLabelBookingDate = string.IsNullOrEmpty(tbxMainSizeCareBookingDate.Text) ? (DateTime?)null : DateTime.Parse(tbxMainSizeCareBookingDate.Text);
            //    //timeAndActionPlan.MainSizeCareLabelPIReceivedDate = string.IsNullOrEmpty(tbxMainSizeCarePIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxMainSizeCarePIReceivedDate.Text);
            //    //timeAndActionPlan.MainSizeCareLabelPINumber = tbxMainSizeCarePINumber.Text;
            //    //timeAndActionPlan.MainSizeCareLabelPIValue = string.IsNullOrEmpty(tbxMainSizeCarePIValue.Text) ? (decimal?)null : decimal.Parse(tbxMainSizeCarePIValue.Text);
            //    //timeAndActionPlan.MainSizeCareLabelPISendToCommercialDate = string.IsNullOrEmpty(tbxMainSizeCarePISignAndSendToCom.Text) ? (DateTime?)null : DateTime.Parse(tbxMainSizeCarePISignAndSendToCom.Text);
            //    //timeAndActionPlan.MainSizeCareLabelLCOpenDate = string.IsNullOrEmpty(tbxMainSizeCareLCOpenDate.Text) ? (DateTime?)null : DateTime.Parse(tbxMainSizeCareLCOpenDate.Text);
            //    //timeAndActionPlan.MainSizeCareLabelGoodsInHouseDate = string.IsNullOrEmpty(tbxMainSizeCareGoodsInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxMainSizeCareGoodsInHouseDate.Text);
            //    //timeAndActionPlan.HangOrPriceTagBookingDate = string.IsNullOrEmpty(tbxHangOrPriceTagBookingDate.Text) ? (DateTime?)null : DateTime.Parse(tbxHangOrPriceTagBookingDate.Text);
            //    //timeAndActionPlan.HangOrPriceTagPIReceivedDate = string.IsNullOrEmpty(tbxHangOrPriceTagPIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxHangOrPriceTagPIReceivedDate.Text);
            //    //timeAndActionPlan.HangOrPriceTagPINumber = tbxHangOrPriceTagPINumber.Text;
            //    //timeAndActionPlan.HangOrPriceTagPIValue = string.IsNullOrEmpty(tbxHangOrPriceTagPIValue.Text) ? (decimal?)null : decimal.Parse(tbxHangOrPriceTagPIValue.Text);
            //    //timeAndActionPlan.HangOrPriceTagPISendToCommercialDate = string.IsNullOrEmpty(tbxHangOrPriceTagPISignAndSendToCom.Text) ? (DateTime?)null : DateTime.Parse(tbxHangOrPriceTagPISignAndSendToCom.Text);
            //    //timeAndActionPlan.HangOrPriceTagLCOpenDate = string.IsNullOrEmpty(tbxHangOrPriceTagLCOpenDate.Text) ? (DateTime?)null : DateTime.Parse(tbxHangOrPriceTagLCOpenDate.Text);
            //    //timeAndActionPlan.HangOrPriceTagGoodsInHouseDate = string.IsNullOrEmpty(tbxHangOrPriceTagGoodsInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxHangOrPriceTagGoodsInHouseDate.Text);
            //    //timeAndActionPlan.ButtonZipperBookingDate = string.IsNullOrEmpty(tbxButtonZipperBookingDate.Text) ? (DateTime?)null : DateTime.Parse(tbxButtonZipperBookingDate.Text);
            //    //timeAndActionPlan.ButtonZipperPIReceivedDate = string.IsNullOrEmpty(tbxButtonZipperPIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxButtonZipperPIReceivedDate.Text);
            //    //timeAndActionPlan.ButtonZipperPINumber = tbxButtonZipperPINumber.Text;
            //    //timeAndActionPlan.ButtonZipperPIValue = string.IsNullOrEmpty(tbxButtonZipperPIValue.Text) ? (decimal?)null : decimal.Parse(tbxButtonZipperPIValue.Text);
            //    //timeAndActionPlan.ButtonZipperPISendToCommercialDate = string.IsNullOrEmpty(tbxButtonZipperPISignAndSendToCom.Text) ? (DateTime?)null : DateTime.Parse(tbxButtonZipperPISignAndSendToCom.Text);
            //    //timeAndActionPlan.ButtonZipperLCOpenDate = string.IsNullOrEmpty(tbxButtonZipperLCOpenDate.Text) ? (DateTime?)null : DateTime.Parse(tbxButtonZipperLCOpenDate.Text);
            //    //timeAndActionPlan.ButtonZipperGoodsInHouseDate = string.IsNullOrEmpty(tbxButtonZipperGoodsInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxButtonZipperGoodsInHouseDate.Text);
            //    //timeAndActionPlan.BadgeFlowerFabricsBookingDate = string.IsNullOrEmpty(tbxBadgeFlowerFabricsBookingDate.Text) ? (DateTime?)null : DateTime.Parse(tbxBadgeFlowerFabricsBookingDate.Text);
            //    //timeAndActionPlan.BadgeFlowerFabricsPIReceivedDate = string.IsNullOrEmpty(tbxBadgeFlowerFabricsPIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxBadgeFlowerFabricsPIReceivedDate.Text);
            //    //timeAndActionPlan.BadgeFlowerFabricsPINumber = tbxBadgeFlowerFabricsPINumber.Text;
            //    //timeAndActionPlan.BadgeFlowerFabricsPIValue = string.IsNullOrEmpty(tbxBadgeFlowerFabricsPIValue.Text) ? (decimal?)null : decimal.Parse(tbxBadgeFlowerFabricsPIValue.Text);
            //    //timeAndActionPlan.BadgeFlowerFabricsPISendToCommercialDate = string.IsNullOrEmpty(tbxBadgeFlowerFabricsPISignAndSendToCom.Text) ? (DateTime?)null : DateTime.Parse(tbxBadgeFlowerFabricsPISignAndSendToCom.Text);
            //    //timeAndActionPlan.BadgeFlowerFabricsLCOpenDate = string.IsNullOrEmpty(tbxBadgeFlowerFabricsLCOpenDate.Text) ? (DateTime?)null : DateTime.Parse(tbxBadgeFlowerFabricsLCOpenDate.Text);
            //    //timeAndActionPlan.BadgeFlowerFabricsGoodsInHouseDate = string.IsNullOrEmpty(tbxBadgeFlowerFabricsGoodsInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxBadgeFlowerFabricsGoodsInHouseDate.Text);
            //    //timeAndActionPlan.PrintEmbroideryBookingDate = string.IsNullOrEmpty(tbxPrintEmbroideryBookingDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPrintEmbroideryBookingDate.Text);
            //    //timeAndActionPlan.PrintEmbroideryPIReceivedDate = string.IsNullOrEmpty(tbxPrintEmbroideryPIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPrintEmbroideryPIReceivedDate.Text);
            //    //timeAndActionPlan.PrintEmbroideryPINumber = tbxPrintEmbroideryPINumber.Text;
            //    //timeAndActionPlan.PrintEmbroideryPIValue = string.IsNullOrEmpty(tbxPrintEmbroideryPIValue.Text) ? (decimal?)null : decimal.Parse(tbxPrintEmbroideryPIValue.Text);
            //    //timeAndActionPlan.PrintEmbroideryPISendToCommercialDate = string.IsNullOrEmpty(tbxPrintEmbroideryPISignAndSendToCom.Text) ? (DateTime?)null : DateTime.Parse(tbxPrintEmbroideryPISignAndSendToCom.Text);
            //    //timeAndActionPlan.PrintEmbroideryLCOpenDate = string.IsNullOrEmpty(tbxPrintEmbroideryLCOpenDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPrintEmbroideryLCOpenDate.Text);
            //    //timeAndActionPlan.PrintEmbroideryGoodsInHouseDate = string.IsNullOrEmpty(tbxPrintEmbroideryGoodsInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPrintEmbroideryGoodsInHouseDate.Text);
            //    //timeAndActionPlan.SewingShoulderTapBookingDate = string.IsNullOrEmpty(tbxSewingShoulderTapBookingDate.Text) ? (DateTime?)null : DateTime.Parse(tbxSewingShoulderTapBookingDate.Text);
            //    //timeAndActionPlan.SewingShoulderTapPIReceivedDate = string.IsNullOrEmpty(tbxSewingShoulderTapPIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxSewingShoulderTapPIReceivedDate.Text);
            //    //timeAndActionPlan.SewingShoulderTapPINumber = tbxSewingShoulderTapPINumber.Text;
            //    //timeAndActionPlan.SewingShoulderTapPIValue = string.IsNullOrEmpty(tbxSewingShoulderTapPIValue.Text) ? (decimal?)null : decimal.Parse(tbxSewingShoulderTapPIValue.Text);
            //    //timeAndActionPlan.SewingShoulderTapPISendToCommercialDate = string.IsNullOrEmpty(tbxSewingShoulderTapPISignAndSendToCom.Text) ? (DateTime?)null : DateTime.Parse(tbxSewingShoulderTapPISignAndSendToCom.Text);
            //    //timeAndActionPlan.SewingShoulderTapLCOpenDate = string.IsNullOrEmpty(tbxSewingShoulderTapLCOpenDate.Text) ? (DateTime?)null : DateTime.Parse(tbxSewingShoulderTapLCOpenDate.Text);
            //    //timeAndActionPlan.SewingShoulderTapGoodsInHouseDate = string.IsNullOrEmpty(tbxSewingShoulderTapGoodsInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxSewingShoulderTapGoodsInHouseDate.Text);
            //    //timeAndActionPlan.PolyBookingDate = string.IsNullOrEmpty(tbxPolyBookingDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPolyBookingDate.Text);
            //    //timeAndActionPlan.PolyPIReceivedDate = string.IsNullOrEmpty(tbxPolyPIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPolyPIReceivedDate.Text);
            //    //timeAndActionPlan.PolyPINumber = tbxPolyPINumber.Text;
            //    //timeAndActionPlan.PolyPIValue = string.IsNullOrEmpty(tbxPolyPIValue.Text) ? (decimal?)null : decimal.Parse(tbxPolyPIValue.Text);
            //    //timeAndActionPlan.PolyPISendToCommercialDate = string.IsNullOrEmpty(tbxPolyPISignAndSendToCom.Text) ? (DateTime?)null : DateTime.Parse(tbxPolyPISignAndSendToCom.Text);
            //    //timeAndActionPlan.PolyLCOpenDate = string.IsNullOrEmpty(tbxPolyLCOpenDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPolyLCOpenDate.Text);
            //    //timeAndActionPlan.PolyGoodsInHouseDate = string.IsNullOrEmpty(tbxPolyGoodsInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPolyGoodsInHouseDate.Text);
            //    //timeAndActionPlan.CartoonBookingDate = string.IsNullOrEmpty(tbxCartoonBookingDate.Text) ? (DateTime?)null : DateTime.Parse(tbxCartoonBookingDate.Text);
            //    //timeAndActionPlan.CartoonPIReceivedDate = string.IsNullOrEmpty(tbxCartoonPIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxCartoonPIReceivedDate.Text);
            //    //timeAndActionPlan.CartoonPINumber = tbxCartoonPINumber.Text;
            //    //timeAndActionPlan.CartoonPIValue = string.IsNullOrEmpty(tbxCartoonPIValue.Text) ? (decimal?)null : decimal.Parse(tbxCartoonPIValue.Text);
            //    //timeAndActionPlan.CartoonPISendToCommercialDate = string.IsNullOrEmpty(tbxCartoonPISignAndSendToCom.Text) ? (DateTime?)null : DateTime.Parse(tbxCartoonPISignAndSendToCom.Text);
            //    //timeAndActionPlan.CartoonLCOpenDate = string.IsNullOrEmpty(tbxCartoonLCOpenDate.Text) ? (DateTime?)null : DateTime.Parse(tbxCartoonLCOpenDate.Text);
            //    //timeAndActionPlan.CartoonGoodsInHouseDate = string.IsNullOrEmpty(tbxCartoonGoodsInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxCartoonGoodsInHouseDate.Text);
            //    //timeAndActionPlan.CreatedBy = CommonMethods.SessionInfo.UserName;
            //    timeAndActionPlan.CreateDate = DateTime.Now;
            //    unitOfWork.GenericRepositories<TimeAndActionPlans>().Insert(timeAndActionPlan);

            //    //foreach (HttpPostedFile postedFile in FileUpload1.PostedFiles)
            //    //{
            //    //    TimeAndActionPlanSamplePictures picture = new TimeAndActionPlanSamplePictures();

            //    //    if (postedFile.ContentLength > 0)
            //    //    {
            //    //        picture.TimeAndActionPlanId = timeAndActionPlan.Id;
            //    //        picture.FileName = Path.GetFileName(postedFile.FileName);
            //    //        picture.ContentType = postedFile.ContentType;
            //    //        picture.FileSize = postedFile.ContentLength;
            //    //        picture.CreatedBy = timeAndActionPlan.CreatedBy;
            //    //        picture.CreateDate = DateTime.Now;
            //    //        using (Stream fs = postedFile.InputStream)
            //    //        {
            //    //            using (BinaryReader br = new BinaryReader(fs))
            //    //            {
            //    //                byte[] bytes = br.ReadBytes((Int32)fs.Length);
            //    //                picture.FileData = bytes;
            //    //            }
            //    //        }

            //    //        unitOfWork.GenericRepositories<TimeAndActionPlanSamplePictures>().Insert(picture);
            //    //    }
            //    //}

            //    //foreach (HttpPostedFile postedFile in FileUpload2.PostedFiles)
            //    //{
            //    //    TimeAndActionPlanDyeingOrderDocuments dyeingOrderDoc = new TimeAndActionPlanDyeingOrderDocuments();

            //    //    if (postedFile.ContentLength > 0)
            //    //    {
            //    //        dyeingOrderDoc.TimeAndActionPlanId = timeAndActionPlan.Id;
            //    //        dyeingOrderDoc.FileName = Path.GetFileName(postedFile.FileName);
            //    //        dyeingOrderDoc.ContentType = postedFile.ContentType;
            //    //        dyeingOrderDoc.FileSize = postedFile.ContentLength;
            //    //        dyeingOrderDoc.CreatedBy = timeAndActionPlan.CreatedBy;
            //    //        dyeingOrderDoc.CreateDate = DateTime.Now;
            //    //        using (Stream fs = postedFile.InputStream)
            //    //        {
            //    //            using (BinaryReader br = new BinaryReader(fs))
            //    //            {
            //    //                byte[] bytes = br.ReadBytes((Int32)fs.Length);
            //    //                dyeingOrderDoc.FileData = bytes;
            //    //            }
            //    //        }

            //    //        unitOfWork.GenericRepositories<TimeAndActionPlanDyeingOrderDocuments>().Insert(dyeingOrderDoc);
            //    //    }
            ////    }

            ////    unitOfWork.Save();

            ////    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Saved successfully.');", true);
            ////    Response.AddHeader("REFRESH", "2;URL=AddTimeAndActionPlan.aspx");
            ////}
            ////catch (Exception ex)
            ////{
            ////    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            ////}
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            //try
            //{

            //    //var timeAndActionPlan = unitOfWork.GenericRepositories<TimeAndActionPlans>().GetByID(TNAId);
                //timeAndActionPlan.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                //timeAndActionPlan.StyleId = int.Parse(ddlStyle.SelectedValue);
                //timeAndActionPlan.YearId = string.IsNullOrEmpty(ddlYear.SelectedValue) ? (int?)null : int.Parse(ddlYear.SelectedValue);
                //timeAndActionPlan.SeasonId = string.IsNullOrEmpty(ddlSeason.SelectedValue) ? (int?)null : int.Parse(ddlSeason.SelectedValue);
                //timeAndActionPlan.BookedCapacityQuantity = string.IsNullOrEmpty(tbxBookedCapacityQty.Text) ? (int?)null : int.Parse(tbxBookedCapacityQty.Text);
                //timeAndActionPlan.OrderId = string.IsNullOrEmpty(ddlOrder.SelectedValue) ? (int?)null : int.Parse(ddlOrder.SelectedValue);
                //timeAndActionPlan.OrderQuantity = string.IsNullOrEmpty(tbxOrderQuantity.Text) ? (int?)null : int.Parse(tbxOrderQuantity.Text);
                //timeAndActionPlan.OrderSheetReceivedDate = string.IsNullOrEmpty(tbxOrderSheetReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxOrderSheetReceivedDate.Text);
                //timeAndActionPlan.InitialShipmentDate = string.IsNullOrEmpty(tbxInitialShipmentDate.Text) ? (DateTime?)null : DateTime.Parse(tbxInitialShipmentDate.Text);
                //timeAndActionPlan.ExtendedShipmentDate = string.IsNullOrEmpty(tbxExtendedShipmentDate.Text) ? (DateTime?)null : DateTime.Parse(tbxExtendedShipmentDate.Text);
                //timeAndActionPlan.FOB = tbxFOB.Text;
                //timeAndActionPlan.TotalFOB = tbxTotalFOB.Text;
                //timeAndActionPlan.ActualExFactoryDate = string.IsNullOrEmpty(tbxActualExFactory.Text) ? (DateTime?)null : DateTime.Parse(tbxActualExFactory.Text);
                //timeAndActionPlan.PlusShipQty = string.IsNullOrEmpty(tbxPlusShipQty.Text) ? (int?)null : int.Parse(tbxPlusShipQty.Text);
                //timeAndActionPlan.LessShipQty = string.IsNullOrEmpty(tbxLessShipQty.Text) ? (int?)null : int.Parse(tbxLessShipQty.Text);
                //timeAndActionPlan.MasterLCOrContactReceivedDate = string.IsNullOrEmpty(tbxMasterLcOrContactReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxMasterLcOrContactReceivedDate.Text);
                //timeAndActionPlan.MasterLCValue = string.IsNullOrEmpty(tbxMasterLCValue.Text) ? (decimal?)null : decimal.Parse(tbxMasterLCValue.Text);
                //timeAndActionPlan.ShipmentValue = string.IsNullOrEmpty(tbxShipmentValue.Text) ? (decimal?)null : decimal.Parse(tbxShipmentValue.Text);
                //timeAndActionPlan.FinishingUnitId = string.IsNullOrEmpty(ddlFinishingUnit.SelectedValue) ? (int?)null : int.Parse(ddlFinishingUnit.SelectedValue);
                //timeAndActionPlan.YarnComCountAndPly = tbxYarnComCountAndPly.Text;
                //timeAndActionPlan.SampleMCBrandId = string.IsNullOrEmpty(ddlSampleMCBrand.SelectedValue) ? (int?)null : int.Parse(ddlSampleMCBrand.SelectedValue);
                //timeAndActionPlan.SampleTiming = string.IsNullOrEmpty(tbxSampleTiming.Text) ? (int?)null : int.Parse(tbxSampleTiming.Text);
                //timeAndActionPlan.KnittingOutput = tbxKnittingOutput.Text;
                //timeAndActionPlan.KnittingMCGauge = string.IsNullOrEmpty(tbxKnittingMCGauge.Text) ? (int?)null : int.Parse(tbxKnittingMCGauge.Text);
                //timeAndActionPlan.LinkingMCGaugeId = string.IsNullOrEmpty(ddlLinkingMCGauge.SelectedValue) ? (int?)null : int.Parse(ddlLinkingMCGauge.SelectedValue);
                //timeAndActionPlan.LinkingTiming = string.IsNullOrEmpty(tbxLinkingTiming.Text) ? (int?)null : int.Parse(tbxLinkingTiming.Text);
                //timeAndActionPlan.LinkingOutput = tbxLinkingOutput.Text;
                //timeAndActionPlan.WeightPerDz = string.IsNullOrEmpty(tbxWeightPerDz.Text) ? (decimal?)null : decimal.Parse(tbxWeightPerDz.Text);
                //timeAndActionPlan.DyeingOrderPlaceDate = string.IsNullOrEmpty(tbxDyeingOrderPlaceDate.Text) ? (DateTime?)null : DateTime.Parse(tbxDyeingOrderPlaceDate.Text);
                //timeAndActionPlan.YarnSupplier = tbxYarnSupplier.Text;
                //timeAndActionPlan.PIReceivedDate = string.IsNullOrEmpty(tbxPIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPIReceivedDate.Text);
                //timeAndActionPlan.PINumber = tbxPINumber.Text;
                //timeAndActionPlan.PIValue = string.IsNullOrEmpty(tbxPIValue.Text) ? (decimal?)null : decimal.Parse(tbxPIValue.Text);
                //timeAndActionPlan.YarnETD = string.IsNullOrEmpty(tbxYarnETD.Text) ? (DateTime?)null : DateTime.Parse(tbxYarnETD.Text);
                //timeAndActionPlan.YarnETA = string.IsNullOrEmpty(tbxYarnETA.Text) ? (DateTime?)null : DateTime.Parse(tbxYarnETA.Text);
                //timeAndActionPlan.YarnInHouseDate = string.IsNullOrEmpty(tbxYarnInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxYarnInHouseDate.Text);
                //timeAndActionPlan.KnittingAccessoriesInHouseDate = string.IsNullOrEmpty(tbxKnittingAccessoriesInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxKnittingAccessoriesInHouseDate.Text);
                //timeAndActionPlan.LinkingAdditionalInHouseDate = string.IsNullOrEmpty(tbxLinkingAdditionalInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxLinkingAdditionalInHouseDate.Text);
                //timeAndActionPlan.MainSizeCareLabelBookingDate = string.IsNullOrEmpty(tbxMainSizeCareBookingDate.Text) ? (DateTime?)null : DateTime.Parse(tbxMainSizeCareBookingDate.Text);
                //timeAndActionPlan.MainSizeCareLabelPIReceivedDate = string.IsNullOrEmpty(tbxMainSizeCarePIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxMainSizeCarePIReceivedDate.Text);
                //timeAndActionPlan.MainSizeCareLabelPINumber = tbxMainSizeCarePINumber.Text;
                //timeAndActionPlan.MainSizeCareLabelPIValue = string.IsNullOrEmpty(tbxMainSizeCarePIValue.Text) ? (decimal?)null : decimal.Parse(tbxMainSizeCarePIValue.Text);
                //timeAndActionPlan.MainSizeCareLabelPISendToCommercialDate = string.IsNullOrEmpty(tbxMainSizeCarePISignAndSendToCom.Text) ? (DateTime?)null : DateTime.Parse(tbxMainSizeCarePISignAndSendToCom.Text);
                //timeAndActionPlan.MainSizeCareLabelLCOpenDate = string.IsNullOrEmpty(tbxMainSizeCareLCOpenDate.Text) ? (DateTime?)null : DateTime.Parse(tbxMainSizeCareLCOpenDate.Text);
                //timeAndActionPlan.MainSizeCareLabelGoodsInHouseDate = string.IsNullOrEmpty(tbxMainSizeCareGoodsInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxMainSizeCareGoodsInHouseDate.Text);
                //timeAndActionPlan.HangOrPriceTagBookingDate = string.IsNullOrEmpty(tbxHangOrPriceTagBookingDate.Text) ? (DateTime?)null : DateTime.Parse(tbxHangOrPriceTagBookingDate.Text);
                //timeAndActionPlan.HangOrPriceTagPIReceivedDate = string.IsNullOrEmpty(tbxHangOrPriceTagPIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxHangOrPriceTagPIReceivedDate.Text);
                //timeAndActionPlan.HangOrPriceTagPINumber = tbxHangOrPriceTagPINumber.Text;
                //timeAndActionPlan.HangOrPriceTagPIValue = string.IsNullOrEmpty(tbxHangOrPriceTagPIValue.Text) ? (decimal?)null : decimal.Parse(tbxHangOrPriceTagPIValue.Text);
                //timeAndActionPlan.HangOrPriceTagPISendToCommercialDate = string.IsNullOrEmpty(tbxHangOrPriceTagPISignAndSendToCom.Text) ? (DateTime?)null : DateTime.Parse(tbxHangOrPriceTagPISignAndSendToCom.Text);
                //timeAndActionPlan.HangOrPriceTagLCOpenDate = string.IsNullOrEmpty(tbxHangOrPriceTagLCOpenDate.Text) ? (DateTime?)null : DateTime.Parse(tbxHangOrPriceTagLCOpenDate.Text);
                //timeAndActionPlan.HangOrPriceTagGoodsInHouseDate = string.IsNullOrEmpty(tbxHangOrPriceTagGoodsInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxHangOrPriceTagGoodsInHouseDate.Text);
                //timeAndActionPlan.ButtonZipperBookingDate = string.IsNullOrEmpty(tbxButtonZipperBookingDate.Text) ? (DateTime?)null : DateTime.Parse(tbxButtonZipperBookingDate.Text);
                //timeAndActionPlan.ButtonZipperPIReceivedDate = string.IsNullOrEmpty(tbxButtonZipperPIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxButtonZipperPIReceivedDate.Text);
                //timeAndActionPlan.ButtonZipperPINumber = tbxButtonZipperPINumber.Text;
                //timeAndActionPlan.ButtonZipperPIValue = string.IsNullOrEmpty(tbxButtonZipperPIValue.Text) ? (decimal?)null : decimal.Parse(tbxButtonZipperPIValue.Text);
                //timeAndActionPlan.ButtonZipperPISendToCommercialDate = string.IsNullOrEmpty(tbxButtonZipperPISignAndSendToCom.Text) ? (DateTime?)null : DateTime.Parse(tbxButtonZipperPISignAndSendToCom.Text);
                //timeAndActionPlan.ButtonZipperLCOpenDate = string.IsNullOrEmpty(tbxButtonZipperLCOpenDate.Text) ? (DateTime?)null : DateTime.Parse(tbxButtonZipperLCOpenDate.Text);
                //timeAndActionPlan.ButtonZipperGoodsInHouseDate = string.IsNullOrEmpty(tbxButtonZipperGoodsInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxButtonZipperGoodsInHouseDate.Text);
                //timeAndActionPlan.BadgeFlowerFabricsBookingDate = string.IsNullOrEmpty(tbxBadgeFlowerFabricsBookingDate.Text) ? (DateTime?)null : DateTime.Parse(tbxBadgeFlowerFabricsBookingDate.Text);
                //timeAndActionPlan.BadgeFlowerFabricsPIReceivedDate = string.IsNullOrEmpty(tbxBadgeFlowerFabricsPIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxBadgeFlowerFabricsPIReceivedDate.Text);
                //timeAndActionPlan.BadgeFlowerFabricsPINumber = tbxBadgeFlowerFabricsPINumber.Text;
                //timeAndActionPlan.BadgeFlowerFabricsPIValue = string.IsNullOrEmpty(tbxBadgeFlowerFabricsPIValue.Text) ? (decimal?)null : decimal.Parse(tbxBadgeFlowerFabricsPIValue.Text);
                //timeAndActionPlan.BadgeFlowerFabricsPISendToCommercialDate = string.IsNullOrEmpty(tbxBadgeFlowerFabricsPISignAndSendToCom.Text) ? (DateTime?)null : DateTime.Parse(tbxBadgeFlowerFabricsPISignAndSendToCom.Text);
                //timeAndActionPlan.BadgeFlowerFabricsLCOpenDate = string.IsNullOrEmpty(tbxBadgeFlowerFabricsLCOpenDate.Text) ? (DateTime?)null : DateTime.Parse(tbxBadgeFlowerFabricsLCOpenDate.Text);
                //timeAndActionPlan.BadgeFlowerFabricsGoodsInHouseDate = string.IsNullOrEmpty(tbxBadgeFlowerFabricsGoodsInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxBadgeFlowerFabricsGoodsInHouseDate.Text);
                //timeAndActionPlan.PrintEmbroideryBookingDate = string.IsNullOrEmpty(tbxPrintEmbroideryBookingDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPrintEmbroideryBookingDate.Text);
                //timeAndActionPlan.PrintEmbroideryPIReceivedDate = string.IsNullOrEmpty(tbxPrintEmbroideryPIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPrintEmbroideryPIReceivedDate.Text);
                //timeAndActionPlan.PrintEmbroideryPINumber = tbxPrintEmbroideryPINumber.Text;
                //timeAndActionPlan.PrintEmbroideryPIValue = string.IsNullOrEmpty(tbxPrintEmbroideryPIValue.Text) ? (decimal?)null : decimal.Parse(tbxPrintEmbroideryPIValue.Text);
                //timeAndActionPlan.PrintEmbroideryPISendToCommercialDate = string.IsNullOrEmpty(tbxPrintEmbroideryPISignAndSendToCom.Text) ? (DateTime?)null : DateTime.Parse(tbxPrintEmbroideryPISignAndSendToCom.Text);
                //timeAndActionPlan.PrintEmbroideryLCOpenDate = string.IsNullOrEmpty(tbxPrintEmbroideryLCOpenDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPrintEmbroideryLCOpenDate.Text);
                //timeAndActionPlan.PrintEmbroideryGoodsInHouseDate = string.IsNullOrEmpty(tbxPrintEmbroideryGoodsInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPrintEmbroideryGoodsInHouseDate.Text);
                //timeAndActionPlan.SewingShoulderTapBookingDate = string.IsNullOrEmpty(tbxSewingShoulderTapBookingDate.Text) ? (DateTime?)null : DateTime.Parse(tbxSewingShoulderTapBookingDate.Text);
                //timeAndActionPlan.SewingShoulderTapPIReceivedDate = string.IsNullOrEmpty(tbxSewingShoulderTapPIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxSewingShoulderTapPIReceivedDate.Text);
                //timeAndActionPlan.SewingShoulderTapPINumber = tbxSewingShoulderTapPINumber.Text;
                //timeAndActionPlan.SewingShoulderTapPIValue = string.IsNullOrEmpty(tbxSewingShoulderTapPIValue.Text) ? (decimal?)null : decimal.Parse(tbxSewingShoulderTapPIValue.Text);
                //timeAndActionPlan.SewingShoulderTapPISendToCommercialDate = string.IsNullOrEmpty(tbxSewingShoulderTapPISignAndSendToCom.Text) ? (DateTime?)null : DateTime.Parse(tbxSewingShoulderTapPISignAndSendToCom.Text);
                //timeAndActionPlan.SewingShoulderTapLCOpenDate = string.IsNullOrEmpty(tbxSewingShoulderTapLCOpenDate.Text) ? (DateTime?)null : DateTime.Parse(tbxSewingShoulderTapLCOpenDate.Text);
                //timeAndActionPlan.SewingShoulderTapGoodsInHouseDate = string.IsNullOrEmpty(tbxSewingShoulderTapGoodsInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxSewingShoulderTapGoodsInHouseDate.Text);
                //timeAndActionPlan.PolyBookingDate = string.IsNullOrEmpty(tbxPolyBookingDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPolyBookingDate.Text);
                //timeAndActionPlan.PolyPIReceivedDate = string.IsNullOrEmpty(tbxPolyPIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPolyPIReceivedDate.Text);
                //timeAndActionPlan.PolyPINumber = tbxPolyPINumber.Text;
                //timeAndActionPlan.PolyPIValue = string.IsNullOrEmpty(tbxPolyPIValue.Text) ? (decimal?)null : decimal.Parse(tbxPolyPIValue.Text);
                //timeAndActionPlan.PolyPISendToCommercialDate = string.IsNullOrEmpty(tbxPolyPISignAndSendToCom.Text) ? (DateTime?)null : DateTime.Parse(tbxPolyPISignAndSendToCom.Text);
                //timeAndActionPlan.PolyLCOpenDate = string.IsNullOrEmpty(tbxPolyLCOpenDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPolyLCOpenDate.Text);
                //timeAndActionPlan.PolyGoodsInHouseDate = string.IsNullOrEmpty(tbxPolyGoodsInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxPolyGoodsInHouseDate.Text);
                //timeAndActionPlan.CartoonBookingDate = string.IsNullOrEmpty(tbxCartoonBookingDate.Text) ? (DateTime?)null : DateTime.Parse(tbxCartoonBookingDate.Text);
                //timeAndActionPlan.CartoonPIReceivedDate = string.IsNullOrEmpty(tbxCartoonPIReceivedDate.Text) ? (DateTime?)null : DateTime.Parse(tbxCartoonPIReceivedDate.Text);
                //timeAndActionPlan.CartoonPINumber = tbxCartoonPINumber.Text;
                //timeAndActionPlan.CartoonPIValue = string.IsNullOrEmpty(tbxCartoonPIValue.Text) ? (decimal?)null : decimal.Parse(tbxCartoonPIValue.Text);
                //timeAndActionPlan.CartoonPISendToCommercialDate = string.IsNullOrEmpty(tbxCartoonPISignAndSendToCom.Text) ? (DateTime?)null : DateTime.Parse(tbxCartoonPISignAndSendToCom.Text);
                //timeAndActionPlan.CartoonLCOpenDate = string.IsNullOrEmpty(tbxCartoonLCOpenDate.Text) ? (DateTime?)null : DateTime.Parse(tbxCartoonLCOpenDate.Text);
                //timeAndActionPlan.CartoonGoodsInHouseDate = string.IsNullOrEmpty(tbxCartoonGoodsInHouseDate.Text) ? (DateTime?)null : DateTime.Parse(tbxCartoonGoodsInHouseDate.Text);
                //timeAndActionPlan.UpdatedBy = CommonMethods.SessionInfo.UserName;
                //timeAndActionPlan.UpdateDate = DateTime.Now;
                //unitOfWork.GenericRepositories<TimeAndActionPlans>().Update(timeAndActionPlan);

                //foreach (HttpPostedFile postedFile in FileUpload1.PostedFiles)
                //{
                //    TimeAndActionPlanSamplePictures picture = new TimeAndActionPlanSamplePictures();

                //    if (postedFile.ContentLength > 0)
                //    {
                //        picture.TimeAndActionPlanId = timeAndActionPlan.Id;
                //        picture.FileName = Path.GetFileName(postedFile.FileName);
                //        picture.ContentType = postedFile.ContentType;
                //        picture.FileSize = postedFile.ContentLength;
                //        picture.CreatedBy = timeAndActionPlan.CreatedBy;
                //        picture.CreateDate = DateTime.Now;
                //        using (Stream fs = postedFile.InputStream)
                //        {
                //            using (BinaryReader br = new BinaryReader(fs))
                //            {
                //                byte[] bytes = br.ReadBytes((Int32)fs.Length);
                //                picture.FileData = bytes;
                //            }
                //        }

                //        unitOfWork.GenericRepositories<TimeAndActionPlanSamplePictures>().Insert(picture);
                //    }
                //}

                //foreach (HttpPostedFile postedFile in FileUpload2.PostedFiles)
                //{
                //    TimeAndActionPlanDyeingOrderDocuments dyeingOrderDoc = new TimeAndActionPlanDyeingOrderDocuments();

                //    if (postedFile.ContentLength > 0)
                //    {
                //        dyeingOrderDoc.TimeAndActionPlanId = timeAndActionPlan.Id;
                //        dyeingOrderDoc.FileName = Path.GetFileName(postedFile.FileName);
                //        dyeingOrderDoc.ContentType = postedFile.ContentType;
                //        dyeingOrderDoc.FileSize = postedFile.ContentLength;
                //        dyeingOrderDoc.CreatedBy = timeAndActionPlan.CreatedBy;
                //        dyeingOrderDoc.CreateDate = DateTime.Now;
                //        using (Stream fs = postedFile.InputStream)
                //        {
                //            using (BinaryReader br = new BinaryReader(fs))
                //            {
                //                byte[] bytes = br.ReadBytes((Int32)fs.Length);
                //                dyeingOrderDoc.FileData = bytes;
                //            }
                //        }

                //        unitOfWork.GenericRepositories<TimeAndActionPlanDyeingOrderDocuments>().Insert(dyeingOrderDoc);
                //    }
                //}

                //unitOfWork.Save();

            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Updated successfully.');", true);
            //    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewTimeAndActionPlans.aspx');", true);
            //}
            //catch (Exception ex)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            //}
        }

        protected void ddlOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlOrder.SelectedValue != "")
            //{
            //    var orderId = int.Parse(ddlOrder.SelectedValue);
            //    var orderQty = unitOfWork.GenericRepositories<OrderColors>().Get(x => x.OrderId == orderId).Sum(x => x.OrderQuantity);
            //    tbxOrderQuantity.Text = orderQty + "";

            //    var TNAExist = unitOfWork.GenericRepositories<TimeAndActionPlans>().Get(x => x.OrderId == orderId).FirstOrDefault();
            //    if (TNAExist != null)
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('Time & action plan already exist for this order.');", true);
            //    }
            //}
            //else
            //{
            //    tbxOrderQuantity.Text = "";
            //}
        }
    }
}