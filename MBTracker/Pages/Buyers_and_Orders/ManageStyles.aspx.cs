﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.Code_Folder.Buyers_and_Orders;
using Repositories;
using MBTracker.EF;

namespace MBTracker.Pages.Buyers_and_Orders
{
    public partial class ManageStyles : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnableDisableDeleteButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteStyle(BuyerStyleId);
                }
            }
        }

        private void EnableDisableDeleteButton()
        {
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn("Admin");
        }

        int BuyerStyleId
        {
            set { ViewState["styleId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["styleId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue != "")
            {

                BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            }
            else
            {
                pnlStyles.Visible = false;
            }
        }

        private void BindData(int buyerId)
        {

            DataTable dt = new DataTable();

            dt = buyerManager.LoadStylesByBuyer(buyerId);
            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
                pnlStyles.Visible = true;
            }
            else
            {
                pnlStyles.Visible = false;
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {


            int styleId = buyerManager.InsertStyle(tbxStyleName.Text, Convert.ToInt32(ddlBuyers.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now);

            if (styleId > 0)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Style name exists.')", true);
            }


            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearStyleName();


        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int returnValue = buyerManager.UpdateBuyerStyle(BuyerStyleId, tbxStyleName.Text, Convert.ToInt32(ddlBuyers.SelectedValue), CommonMethods.SessionInfo.UserName, DateTime.Now);
            if (returnValue > 0)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Style update was failed.')", true);
            }


            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearStyleName();

            styleActionTitle.Text = "Add a New Style:";
            btnSave.Visible = true;
            btnUpdate.Visible = false;

        }


        private void ClearStyleName()
        {
            tbxStyleName.Text = string.Empty;

        }


        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)

        {

            if (e.CommandName == "Edit")
            {

                int buyerStyleId = Convert.ToInt32(e.CommandArgument.ToString());
                PopulateStyles(buyerStyleId);


            }
            else if (e.CommandName == "Delete")
            {
                BuyerStyleId = Convert.ToInt32(e.CommandArgument.ToString());

                var styleCannotBeDeleted = unitOfWork.GenericRepositories<viewStylesCanNotBeDeleted>().GetByID(BuyerStyleId);
                if (styleCannotBeDeleted == null)
                {
                    var title = "Warning";
                    var msg = "Are you sure you want to delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                }
                else
                {
                    var title = "Warning";
                    var msg = "Sorry, you can not delete it.<br />There was some dependencies of this style.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                }
            }
        }

        private void PopulateStyles(int buyerStyleId)
        {
            DataTable dt = buyerManager.GetStyleById(buyerStyleId);

            if (dt.Rows.Count > 0)
            {
                BuyerStyleId = Convert.ToInt32(dt.Rows[0]["Id"]);
                tbxStyleName.Text = dt.Rows[0]["StyleName"].ToString();
                ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, Convert.ToInt32(dt.Rows[0]["BuyerId"]));

                styleActionTitle.Text = "Update Style:";
                btnSave.Visible = false;
                btnUpdate.Visible = true;
            }


        }


        private void DeleteStyle(int buyerStyleId)
        {

            int returnValue = buyerManager.DeleteBuyerStyle(buyerStyleId, CommonMethods.SessionInfo.UserName, DateTime.Now);

            if (returnValue > 0)
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.success('Deleted successfully.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Style deletion was failed.');", true);
            }

            BindData(Convert.ToInt32(ddlBuyers.SelectedValue));
            ClearStyleName();


            styleActionTitle.Text = "Add a New Style:";
            btnSave.Visible = true;
            btnUpdate.Visible = false;

        }
    }
}