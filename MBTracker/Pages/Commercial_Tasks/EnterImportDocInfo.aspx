﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="EnterImportDocInfo.aspx.cs" Inherits="MBTracker.Pages.Commercial_Tasks.EnterImportDocInfo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <asp:Panel ID="pnlNonEdit1" runat="server">

        <div class="row-fluid">

            <div class="span6">
                <div class="widget">
                    <div class="widget-header">
                        <div class="title">
                            <span class="fs1" aria-hidden="true" data-icon=""></span>
                            <asp:Label runat="server" ID="actionTitle" Text="Enter Import Document Information:"></asp:Label>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="form-horizontal">
                            <div class="pull-right" style="text-align: center; font-size: 12px; line-height: 20px; padding-bottom: 5px">
                                <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                            </div>
                            <div class="control-group">
                                <div class="col-md-12 col-sm-12">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                </div>
                            </div>

                            <div class="control-group" style="padding-bottom: 30px">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="lblBuyer" runat="server" Text="Select Beneficiary:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlBeneficiary" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlBeneficiary_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlBeneficiary"><span style="font-weight: 700; color: #CC0000">Please select a beneficiary.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <br />
    </asp:Panel>


    <div class="row-fluid">

        <div id="divPIOtherInfo2" runat="server" visible="false">

            <div class="span12">
                <div class="widget">

                    <div class="widget-body">
                        <div class="row" style="padding-left: 15px; padding-right: 15px;">
                            <div class="control-group">
                                <div class="controls controls-row" style="text-align: left; line-height: 40px">
                                    <span style="font-weight: 700; padding-left: 30px">Import Document Information:</span>
                                </div>
                            </div>

                            <asp:Panel ID="pnlNonEdit2" runat="server">
                                <div class="col-md-6">
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblBeneficiaryName" runat="server" Text="LC Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlLCNumber" runat="server" CssClass="form-control" Width="70%">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlLCNumber">
			                                        <span style="font-weight: 700; color: #CC0000">Select LC.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="Label1" runat="server" Text="Doc Receive Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxDocReceiveDate" runat="server" CssClass="form-control" Width="70%" TextMode="Date"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxDocReceiveDate">
			                                        <span style="font-weight: 700; color: #CC0000">Enter doc receive date.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="Label3" runat="server" Text="Chalan/BL Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxChalanrOrBLNumber" runat="server" placeholder="Enter chalan/BL number" CssClass="form-control" Width="70%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxChalanrOrBLNumber">
			                                        <span style="font-weight: 700; color: #CC0000">Enter chalan or BL number.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="Label8" runat="server" Text="Chalan/BL Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxChalanOrBLDate" runat="server" CssClass="form-control" Width="70%" TextMode="Date"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxChalanOrBLDate">
			                                        <span style="font-weight: 700; color: #CC0000">Enter chalan/BL date.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </asp:Panel>


                            <div class="col-md-6">
                                <div class="form-horizontal">

                                    <asp:Panel ID="pnlNonEdit3" runat="server">
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="Label9" runat="server" Text="Payment Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxPaymentDate" runat="server" CssClass="form-control" Width="70%" TextMode="Date"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxPaymentDate">
			                                        <span style="font-weight: 700; color: #CC0000">Enter payment date.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblBeneficiaryPhone" runat="server" Text="Document Value:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxDocumentValue" runat="server" TextMode="Number" min="1" placeholder="Enter document value" CssClass="form-control" Width="70%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxDocumentValue">
			                                        <span style="font-weight: 700; color: #CC0000">Enter doc value.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblBeneficiaryBank" runat="server" Text="Documetn Quantity:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxDocumentQty" runat="server" TextMode="Number" min="1" placeholder="Enter document quantity" CssClass="form-control" Width="70%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxDocumentQty">
			                                        <span style="font-weight: 700; color: #CC0000">Enter doc quantity.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                         <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="Label4" runat="server" Text="Payment Status:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlPaymentStatus" runat="server" CssClass="form-control" Width="70%">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlPaymentStatus">
			                                        <span style="font-weight: 700; color: #CC0000">Select payment status.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                       <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="Label2" runat="server" Text="Invoice Information:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:Repeater ID="rptEntryInfo" runat="server">
                                                        <HeaderTemplate>
                                                            <table id="entryTable" class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr class="GridViewScrollHeader">
                                                                        <th style="text-align: center; width: 10%;">
                                                                            <asp:Label ID="lblColor" runat="server" Text="Invoice Number"></asp:Label></th>
                                                                        <th style="text-align: center; width: 10%;">
                                                                            <asp:Label ID="Label7" runat="server" Text="Invoice Date"></asp:Label></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr class="GridViewScrollItem">

                                                                <td>
                                                                    <asp:TextBox ID="tbxInvoiceNumber" Width="100%" Text="" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox TextMode="Date" ID="tbxInvoicedate" Width="100%" Text="" runat="server"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody>
                                </table>
                                                             <div class="pull-right" style="margin-right: 0px; padding-top: 10px">
                                                                 <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="71px" Text="Add Row" OnClick="btnAddRow_Click" />
                                                             </div>
                                                            <div class="clearfix"></div>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" ValidationGroup="save" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
                <asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" ValidationGroup="save" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>



            </div>



        </div>
    </div>
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
