﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Commercial_Tasks;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Commercial_Tasks
{
    public partial class EnterPIInformation : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        CommercialManager commercialManager = new CommercialManager();

        UnitOfWork unitOfWork = new UnitOfWork();

        DataTable dtPIItemDetails;

        int initialLoadForUpdate = 0;
        int totalGoods = 0;
        decimal grandTotalAmount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                if (Request.QueryString["PIId"] != null)
                {
                    PIId = int.Parse(Tools.UrlDecode(Request.QueryString["PIId"]));
                    initialLoadForUpdate = PIId;
                    actionTitle.Text = "Udpate PI Information";
                    lnkbtnSaveEntries.Visible = false;
                    lnkbtnUpdateEntries.Visible = true;

                    LoadPIInfo();
                }
                else
                {
                    CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                }
            }

        }

        int PIId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["costingSheetId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["costingSheetId"] = value; }
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblNoCostingSheetFound.Visible = false;
            divPIOtherInfo.Visible = false;
            divCostingSheet.Visible = false;

            if (ddlBuyers.SelectedValue != "")
            {

                DataTable dt = buyerManager.LoadStylesByBuyer(int.Parse(ddlBuyers.SelectedValue));

                if (dt.Rows.Count > 0)
                {

                    cblStyles.DataValueField = "Id";
                    cblStyles.DataTextField = "StyleName";
                    cblStyles.DataSource = dt;
                    cblStyles.DataBind();

                    divStyles.Visible = true;
                    lblNoStyleFound.Visible = false;
                }

            }
            else
            {
                lblNoStyleFound.Visible = true;
                cblStyles.Items.Clear();
                divStyles.Visible = false;

                rptCostings.DataSource = null;
                rptCostings.DataBind();

                divCostingSheet.Visible = false;

            }
        }

        protected void btnLoadCostingSheets_Click(object sender, EventArgs e)
        {
            StringBuilder sbStyleIds = new StringBuilder();
            lblNoCostingSheetFound.Visible = false;



            if (cblStyles.SelectedIndex == -1)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select an style.')", true);
            }
            else
            {
                divPIOtherInfo.Visible = true;
                CommonMethods.LoadYarnAndAccessoriesSupplierDropdown(ddlYarnAndAccssSuppliers, 1, 0);
                try
                {

                    for (int i = 0; i < cblStyles.Items.Count; i++)
                    {
                        if (cblStyles.Items[i].Selected)
                        {
                            sbStyleIds.Append(",");
                            sbStyleIds.Append(cblStyles.Items[i].Value).ToString();
                        }
                    }


                    string sql = $"Exec usp_GetOrderCostingInfoByStyleIds '{sbStyleIds}'";

                    var dt = unitOfWork.GetDataTableFromSql(sql);

                    if (dt.Rows.Count > 0)
                    {
                        divCostingSheet.Visible = true;
                        lblNoCostingSheetFound.Visible = false;
                        rptCostings.DataSource = dt;
                        rptCostings.DataBind();

                       

                    }
                    else
                    {
                        divCostingSheet.Visible = false;
                        lblNoCostingSheetFound.Visible = true;

                    }

                    if (int.Parse(ddlBuyers.SelectedValue) == 1)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(1000)", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);
                    }
                   

                    SetInitialRowCount();
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }


        }

        protected void CalculatePrices(object sender, EventArgs e)
        {
            TextBox tbxSender = (TextBox)sender;

            Label lblTotalAmount = (Label)((RepeaterItem)tbxSender.NamingContainer).FindControl("lblTotalAmount");
            TextBox tbxQty = (TextBox)((RepeaterItem)tbxSender.NamingContainer).FindControl("tbxQty");
            TextBox tbxUnitPrice = (TextBox)((RepeaterItem)tbxSender.NamingContainer).FindControl("tbxUnitPrice");


            if (tbxQty.Text != "" && tbxUnitPrice.Text != "")
            {
                //double totalAmount = double.Parse(tbxQty.Text) * double.Parse(tbxUnitPrice.Text);
                decimal quantity = Convert.ToDecimal(tbxQty.Text);
                decimal unitPrice = Convert.ToDecimal(tbxUnitPrice.Text);

                decimal totalAmount = quantity * unitPrice;

                lblTotalAmount.Text = Math.Round(totalAmount, 2).ToString();
            }

            SetCurrentRowCount();


            if (int.Parse(ddlBuyers.SelectedValue) == 1)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(1000)", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);
            }
        }

        private void SetCurrentRowCount()
        {
            DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
            if (dtCurrentTable.Rows.Count > 0)
            {
                for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                {

                    TextBox tbxDescription = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxDescription");
                    TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                    TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                    TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");
                    Label lblTotalAmount = (Label)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("lblTotalAmount");


                    dtCurrentTable.Rows[rowIndex]["Description"] = tbxDescription.Text;
                    dtCurrentTable.Rows[rowIndex]["HSCode"] = tbxHSCode.Text;
                    dtCurrentTable.Rows[rowIndex]["Quantity"] = tbxQty.Text;

                    dtCurrentTable.Rows[rowIndex]["UnitPrice"] = tbxUnitPrice.Text;
                    dtCurrentTable.Rows[rowIndex]["TotalAmount"] = lblTotalAmount.Text;

                }


                this.ViewState["CurrentTable"] = dtCurrentTable;

                this.rptPIItemEntryInfo.DataSource = dtCurrentTable;
                this.rptPIItemEntryInfo.DataBind();

            }


        }

        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("Description", typeof(string)));
            dt.Columns.Add(new DataColumn("HSCode", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("UnitPrice", typeof(string)));
            dt.Columns.Add(new DataColumn("TotalAmount", typeof(string)));

            dr = dt.NewRow();

            dr["Description"] = string.Empty;
            dr["HSCode"] = string.Empty;
            dr["Quantity"] = string.Empty;
            dr["UnitPrice"] = string.Empty;
            dr["TotalAmount"] = string.Empty;


            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptPIItemEntryInfo.DataSource = dt;
            rptPIItemEntryInfo.DataBind();
        }

        protected void rptPIItemEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {


                var itemDescription = DataBinder.Eval(e.Item.DataItem, "Description").ToString();
                var hsCode = DataBinder.Eval(e.Item.DataItem, "HSCode").ToString();
                var qty = DataBinder.Eval(e.Item.DataItem, "Quantity").ToString();
                var unitPrice = DataBinder.Eval(e.Item.DataItem, "UnitPrice").ToString();
                var totalAmount = DataBinder.Eval(e.Item.DataItem, "TotalAmount").ToString();

                //if (initialLoadForUpdate > 0)
                //{

                var tbxDescription = (TextBox)e.Item.FindControl("tbxDescription");
                var tbxHSCode = (TextBox)e.Item.FindControl("tbxHSCode");
                var tbxQty = (TextBox)e.Item.FindControl("tbxQty");
                var tbxUnitPrice = (TextBox)e.Item.FindControl("tbxUnitPrice");
                var lblTotalAmount = (Label)e.Item.FindControl("lblTotalAmount");

                tbxDescription.Text = itemDescription.ToString();
                tbxHSCode.Text = hsCode.ToString();
                tbxQty.Text = qty.ToString();
                tbxUnitPrice.Text = unitPrice.ToString();
                lblTotalAmount.Text = totalAmount.ToString();

                //}

                if (qty != "")
                {
                    totalGoods += int.Parse(qty);
                }

                if (totalAmount != "")
                {
                    grandTotalAmount += decimal.Parse(totalAmount);
                }

            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                //if (initialLoadForUpdate > 0)
                //{
                Label lblTotalQuantity = (Label)e.Item.FindControl("lblTotalQuantity");
                Label lblGrandTotalAmount = (Label)e.Item.FindControl("lblGrandTotalAmount");


                lblTotalQuantity.Text = totalGoods.ToString();
                lblGrandTotalAmount.Text = grandTotalAmount.ToString();
                //}
            }

        }


        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {


                        TextBox tbxDescription = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxDescription");
                        TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                        TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                        TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");
                        Label lblTotalAmount = (Label)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("lblTotalAmount");


                        dtCurrentTable.Rows[rowIndex]["Description"] = tbxDescription.Text;
                        dtCurrentTable.Rows[rowIndex]["HSCode"] = tbxHSCode.Text;
                        dtCurrentTable.Rows[rowIndex]["Quantity"] = tbxQty.Text;

                        dtCurrentTable.Rows[rowIndex]["UnitPrice"] = tbxUnitPrice.Text;
                        dtCurrentTable.Rows[rowIndex]["TotalAmount"] = lblTotalAmount.Text;


                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();

                            drCurrentRow["Description"] = string.Empty;
                            drCurrentRow["HSCode"] = string.Empty;
                            drCurrentRow["Quantity"] = string.Empty;
                            drCurrentRow["UnitPrice"] = string.Empty;
                            drCurrentRow["TotalAmount"] = string.Empty;

                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptPIItemEntryInfo.DataSource = dtCurrentTable;
                    this.rptPIItemEntryInfo.DataBind();

                }
            }
            this.SetPreviousData();
        }




        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        TextBox tbxDescription = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxDescription");
                        TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                        TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                        TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");
                        Label lblTotalAmount = (Label)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("lblTotalAmount");

                        tbxDescription.Text = dt.Rows[i]["Description"].ToString();
                        tbxHSCode.Text = dt.Rows[i]["HSCode"].ToString();
                        tbxQty.Text = dt.Rows[i]["Quantity"].ToString();
                        tbxUnitPrice.Text = dt.Rows[i]["UnitPrice"].ToString();
                        lblTotalAmount.Text = dt.Rows[i]["TotalAmount"].ToString();

                        rowIndex++;
                    }
                }
            }
        }


        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (cblStyles.SelectedIndex == -1)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select the style(s).')", true);
            }
            else if (String.IsNullOrEmpty(tbxPINumber.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter IP number.')", true);
            }
            else if (String.IsNullOrEmpty(tbxPIDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter PI date.')", true);
            }
            else if (!CheckItems())
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter item(s) info correctly.')", true);
            }
            else if (ddlPartialShipment.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select partial shipment allowed.')", true);
            }
            else if (ddlYarnAndAccssSuppliers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter beneficiary name.')", true);
            }
            else if (String.IsNullOrEmpty(tbxBeneficiaryPhone.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter beneficiary phone.')", true);
            }
            else if (String.IsNullOrEmpty(tbxBeneficiaryBankInfo.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter beneficiary bank info.')", true);
            }
            else
            {
                try
                {

                    var pIInfo = new PIInfo()
                    {
                        BuyerId = int.Parse(ddlBuyers.SelectedValue),
                        PINumber = tbxPINumber.Text.ToString(),
                        PIDate = DateTime.Parse(tbxPIDate.Text),
                        PaymentMode = tbxPaymentMode.Text.ToString(),
                        ShipmentInfo = tbxShipment.Text.ToString(),
                        PartialShipment = int.Parse(ddlPartialShipment.SelectedValue),
                        TolaranceInfo = tbxTolerance.Text,
                        BeneficiaryName = int.Parse(ddlYarnAndAccssSuppliers.SelectedValue),
                        BeneficiaryPhone = tbxBeneficiaryPhone.Text,
                        BeneficiaryBankInfo = tbxBeneficiaryBankInfo.Text,
                        OtherInfo = tbxOtherInfo.Text,

                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };

                    unitOfWork.GenericRepositories<PIInfo>().Insert(pIInfo);
                    unitOfWork.Save();


                    int piStyleSavingResult = SavePIStyles(pIInfo.Id);

                    int piCostingSheetSavingResult = SavePICostingSheets(pIInfo.Id);



                    List<PIItems> listPIItems = new List<PIItems>();

                    for (int rowIndex = 0; rowIndex < rptPIItemEntryInfo.Items.Count; rowIndex++)
                    {

                        TextBox tbxDescription = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxDescription");
                        TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                        TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                        TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");

                        Label lblTotalAmount = (Label)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("lblTotalAmount");



                        if (!string.IsNullOrEmpty(tbxDescription.Text) && !string.IsNullOrEmpty(tbxHSCode.Text) && !string.IsNullOrEmpty(tbxQty.Text) && !string.IsNullOrEmpty(tbxUnitPrice.Text))
                        {
                            var pIITem = new PIItems()
                            {
                                PIId = pIInfo.Id,
                                DescriptionOfGoods = tbxDescription.Text,
                                HSCode = tbxHSCode.Text,
                                Quantity = int.Parse(tbxQty.Text),
                                UnitPrice = decimal.Parse(tbxUnitPrice.Text),
                                Amount = decimal.Parse(lblTotalAmount.Text),

                                CreateDate = DateTime.Now,
                                CreatedBy = CommonMethods.SessionInfo.UserName
                            };

                            listPIItems.Add(pIITem);
                        }

                    }

                    foreach (var item in listPIItems)
                    {
                        unitOfWork.GenericRepositories<PIItems>().Insert(item);
                    }

                    unitOfWork.Save();

                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('EnterPIInformation.aspx');", true);
                    //ClearFields();

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }

        }

        private bool CheckItems()
        {
            bool hasItem = false;

            for (int rowIndex = 0; rowIndex < rptPIItemEntryInfo.Items.Count; rowIndex++)
            {

                TextBox tbxDescription = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxDescription");
                TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");

                //Label lblTotalAmount = (Label)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("lblTotalAmount");

                if (!string.IsNullOrEmpty(tbxDescription.Text) && !string.IsNullOrEmpty(tbxHSCode.Text) && !string.IsNullOrEmpty(tbxQty.Text) && !string.IsNullOrEmpty(tbxUnitPrice.Text))
                {
                    hasItem = true;
                    break;
                }

            }

            return hasItem;
        }



        private void ClearFields()
        {
            ddlBuyers.SelectedIndex = 0;

            cblStyles.Items.Clear();
            divStyles.Visible = false;

            rptCostings.DataSource = null;
            rptCostings.DataBind();

            divCostingSheet.Visible = false;
            divPIOtherInfo.Visible = false;
        }



        public int SavePIStyles(int piId)
        {
            DataTable dt = PIStyle();


            for (int i = 0; i < cblStyles.Items.Count; i++)
            {
                if (cblStyles.Items[i].Selected)
                {
                    dt.Rows.Add(piId, cblStyles.Items[i].Value, CommonMethods.SessionInfo.UserName, DateTime.Now);
                }
            }


            return commercialManager.SavePIStyles(dt);
        }

        public DataTable PIStyle()
        {
            DataTable dt = new DataTable("PIStyles");
            dt.Columns.Add("PIId", typeof(int));
            dt.Columns.Add("StyleId", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }



        public int SavePICostingSheets(int piId)
        {
            DataTable dt = PICosting();


            for (int rowIndex = 0; rowIndex < rptCostings.Items.Count; rowIndex++)
            {
                CheckBox cbxCostingSheet = (CheckBox)this.rptCostings.Items[rowIndex].FindControl("cbxCostingSheet");
                Label lblCostingInfoId = (Label)this.rptCostings.Items[rowIndex].FindControl("lblCostingInfoId");

                if (cbxCostingSheet.Checked)
                {
                    dt.Rows.Add(piId, int.Parse(lblCostingInfoId.Text), CommonMethods.SessionInfo.UserName, DateTime.Now);
                }
            }

            return commercialManager.SavePICostingSheets(dt);
        }

        public DataTable PICosting()
        {
            DataTable dt = new DataTable("PICostingSheets");
            dt.Columns.Add("PIId", typeof(int));
            dt.Columns.Add("CostingSheetId", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }



        private void LoadPIInfo()
        {

            try
            {
                string sql = $"Exec usp_GetPIInfoDetailsById '{PIId}'";

                var dtPIInfo = unitOfWork.GetDataTableFromSql(sql);

                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["BuyerId"].ToString()) ? "0" : dtPIInfo.Rows[0]["BuyerId"].ToString())));

                DataTable dt = buyerManager.LoadStylesByBuyer(int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["BuyerId"].ToString()) ? "0" : dtPIInfo.Rows[0]["BuyerId"].ToString())));

                if (dt.Rows.Count > 0)
                {
                    cblStyles.DataValueField = "Id";
                    cblStyles.DataTextField = "StyleName";
                    cblStyles.DataSource = dt;
                    cblStyles.DataBind();

                    divStyles.Visible = true;
                    lblNoStyleFound.Visible = false;
                }

                var selectedStyles = unitOfWork.GenericRepositories<PIStyles>().Get(x => x.PIId == PIId).ToList();

                foreach (ListItem item in cblStyles.Items)
                {
                    item.Selected = selectedStyles.Exists(x => x.StyleId == Int32.Parse(item.Value));
                }

                StringBuilder sbStyleIds = new StringBuilder();
                lblNoCostingSheetFound.Visible = false;

                for (int i = 0; i < cblStyles.Items.Count; i++)
                {
                    if (cblStyles.Items[i].Selected)
                    {
                        sbStyleIds.Append(",");
                        sbStyleIds.Append(cblStyles.Items[i].Value).ToString();
                    }
                }

                string sql2 = $"Exec usp_GetOrderCostingInfoByStyleIds '{sbStyleIds}'";

                var dt2 = unitOfWork.GetDataTableFromSql(sql2);

                if (dt2.Rows.Count > 0)
                {
                    divCostingSheet.Visible = true;
                    lblNoCostingSheetFound.Visible = false;
                    rptCostings.DataSource = dt2;
                    rptCostings.DataBind();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);
                }
                else
                {
                    divCostingSheet.Visible = false;
                    lblNoCostingSheetFound.Visible = true;
                }

                var selectedCostingSheets = unitOfWork.GenericRepositories<PICostingSheets>().Get(x => x.PIId == PIId).ToList();

                for (int rowIndex = 0; rowIndex < rptCostings.Items.Count; rowIndex++)
                {
                    CheckBox cbxCostingSheet = (CheckBox)this.rptCostings.Items[rowIndex].FindControl("cbxCostingSheet");
                    Label lblCostingInfoId = (Label)this.rptCostings.Items[rowIndex].FindControl("lblCostingInfoId");

                    cbxCostingSheet.Checked = selectedCostingSheets.Exists(x => x.CostingSheetId == Int32.Parse(lblCostingInfoId.Text));
                }

                divPIOtherInfo.Visible = true;
                tbxPINumber.Text = dtPIInfo.Rows[0]["PINumber"].ToString();
                tbxPIDate.Text = DateTime.Parse(dtPIInfo.Rows[0]["PIDate"].ToString()).ToString("yyyy-MM-dd");

                string sql3 = $"Exec usp_GetPIItems '{PIId}'";
                dtPIItemDetails = unitOfWork.GetDataTableFromSql(sql3);

                if (dtPIItemDetails.Rows.Count > 0)
                {
                    rptPIItemEntryInfo.Visible = true;

                    rptPIItemEntryInfo.DataSource = dtPIItemDetails;
                    rptPIItemEntryInfo.DataBind();

                    CreateDataTableForViewState();
                }
                else
                {
                    SetInitialRowCount();

                }

                tbxPaymentMode.Text = dtPIInfo.Rows[0]["PaymentMode"].ToString();
                tbxShipment.Text = dtPIInfo.Rows[0]["ShipmentInfo"].ToString();
                ddlPartialShipment.SelectedIndex = CommonMethods.MatchDropDownItem(ddlPartialShipment, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["PartialShipmentId"].ToString()) ? "0" : dtPIInfo.Rows[0]["PartialShipmentId"].ToString())));
                tbxTolerance.Text = dtPIInfo.Rows[0]["TolaranceInfo"].ToString();

                //tbxBeneficiaryName.Text = dtPIInfo.Rows[0]["BeneficiaryName"].ToString();

                CommonMethods.LoadYarnAndAccessoriesSupplierDropdown(ddlYarnAndAccssSuppliers, 1, 0);
                ddlYarnAndAccssSuppliers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlYarnAndAccssSuppliers, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["BeneficiaryNameId"].ToString()) ? "0" : dtPIInfo.Rows[0]["BeneficiaryNameId"].ToString())));

                tbxBeneficiaryPhone.Text = dtPIInfo.Rows[0]["BeneficiaryPhone"].ToString();
                tbxBeneficiaryBankInfo.Text = dtPIInfo.Rows[0]["BeneficiaryBankInfo"].ToString();
                tbxOtherInfo.Text = dtPIInfo.Rows[0]["OtherInfo"].ToString();

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }


        private void CreateDataTableForViewState()
        {

            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("Description", typeof(string)));
            dt.Columns.Add(new DataColumn("HSCode", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("UnitPrice", typeof(string)));
            dt.Columns.Add(new DataColumn("TotalAmount", typeof(string)));

            //dr = dt.NewRow();

            //dr["Description"] = string.Empty;
            //dr["HSCode"] = string.Empty;
            //dr["Quantity"] = string.Empty;
            //dr["UnitPrice"] = string.Empty;
            //dr["TotalAmount"] = string.Empty;





            for (int rowIndex = 0; rowIndex < dtPIItemDetails.Rows.Count; rowIndex++)
            {
                dr = dt.NewRow();

                dr["Description"] = dtPIItemDetails.Rows[rowIndex]["Description"];
                dr["HSCode"] = dtPIItemDetails.Rows[rowIndex]["HSCode"];
                dr["Quantity"] = dtPIItemDetails.Rows[rowIndex]["Quantity"];
                dr["UnitPrice"] = dtPIItemDetails.Rows[rowIndex]["UnitPrice"];
                dr["TotalAmount"] = dtPIItemDetails.Rows[rowIndex]["TotalAmount"];


                dt.Rows.Add(dr);
            }


            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

        }


        private bool HasLC()
        {
            bool hasLC = false;

            string sql = $"Exec usp_GetLCInfoByPIId '{PIId}'";
            DataTable dt = unitOfWork.GetDataTableFromSql(sql);

            if (dt.Rows.Count > 0)
            {
                hasLC = true;
            }

                return hasLC;

        }

        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {

            if (HasLC())
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cannot update PI as it was used for LC.')", true);
            }
            else if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (cblStyles.SelectedIndex == -1)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select the style(s).')", true);
            }
            else if (String.IsNullOrEmpty(tbxPINumber.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter IP number.')", true);
            }
            else if (String.IsNullOrEmpty(tbxPIDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter PI date.')", true);
            }
            else if (!CheckItems())
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter item(s) info correctly.')", true);
            }

            else if (ddlPartialShipment.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select partial shipment allowed.')", true);
            }
            else if (ddlYarnAndAccssSuppliers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter beneficiary name.')", true);
            }
            else if (String.IsNullOrEmpty(tbxBeneficiaryPhone.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter beneficiary phone.')", true);
            }
            else if (String.IsNullOrEmpty(tbxBeneficiaryBankInfo.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter beneficiary bank info.')", true);
            }
            else
            {
                try
                {

                    var piInfo = unitOfWork.GenericRepositories<PIInfo>().GetByID(PIId);


                    piInfo.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                    piInfo.PINumber = tbxPINumber.Text.ToString();
                    piInfo.PIDate = DateTime.Parse(tbxPIDate.Text);
                    piInfo.PaymentMode = tbxPaymentMode.Text.ToString();
                    piInfo.ShipmentInfo = tbxShipment.Text.ToString();
                    piInfo.PartialShipment = int.Parse(ddlPartialShipment.SelectedValue);
                    piInfo.TolaranceInfo = tbxTolerance.Text;
                    piInfo.BeneficiaryName = int.Parse(ddlYarnAndAccssSuppliers.SelectedValue);
                    piInfo.BeneficiaryPhone = tbxBeneficiaryPhone.Text;
                    piInfo.BeneficiaryBankInfo = tbxBeneficiaryBankInfo.Text;
                    piInfo.OtherInfo = tbxOtherInfo.Text;

                    piInfo.UpdateDate = DateTime.Now;
                    piInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;

                    unitOfWork.GenericRepositories<PIInfo>().Update(piInfo);
                    unitOfWork.Save();


                    UpdatePIStyles(PIId);
                    UpdatePICostingSheet(PIId);
                    UpdatePIItems(PIId);

                    unitOfWork.Save();


                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewEditPIInformation.aspx');", true);

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }


        }

        protected void UpdatePIStyles(int piId)
        {
            var piStyles = unitOfWork.GenericRepositories<PIStyles>().Get(x => x.PIId == piId);

            foreach (var item in piStyles)
            {
                unitOfWork.GenericRepositories<PIStyles>().Delete(item);
            }
            for (int i = 0; i < cblStyles.Items.Count; i++)
            {
                if (cblStyles.Items[i].Selected)
                {
                    var piStyle = new PIStyles()
                    {
                        PIId = piId,
                        StyleId = Convert.ToInt32(cblStyles.Items[i].Value),
                        CreatedBy = CommonMethods.SessionInfo.UserName,
                        CreateDate = DateTime.Now
                    };
                    unitOfWork.GenericRepositories<PIStyles>().Insert(piStyle);
                }
            }
        }


        private void UpdatePICostingSheet(int piId)
        {

            var piCostingSheets = unitOfWork.GenericRepositories<PICostingSheets>().Get(x => x.PIId == piId);
            foreach (var piCostingItem in piCostingSheets)
            {
                unitOfWork.GenericRepositories<PICostingSheets>().Delete(piCostingItem);
            }


            List<PICostingSheets> listPICostingSheets = new List<PICostingSheets>();


            for (int rowIndex = 0; rowIndex < rptCostings.Items.Count; rowIndex++)
            {
                CheckBox cbxCostingSheet = (CheckBox)this.rptCostings.Items[rowIndex].FindControl("cbxCostingSheet");
                Label lblCostingInfoId = (Label)this.rptCostings.Items[rowIndex].FindControl("lblCostingInfoId");

                if (cbxCostingSheet.Checked)
                {
                    var piCostingSheet = new PICostingSheets()
                    {
                        PIId = piId,
                        CostingSheetId = int.Parse(lblCostingInfoId.Text),
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };

                    listPICostingSheets.Add(piCostingSheet);
                }

            }

            foreach (var item in listPICostingSheets)
            {
                unitOfWork.GenericRepositories<PICostingSheets>().Insert(item);
            }

        }

        private void UpdatePIItems(int piId)
        {

            var piItems = unitOfWork.GenericRepositories<PIItems>().Get(x => x.PIId == piId);

            foreach (var piItem in piItems)
            {
                unitOfWork.GenericRepositories<PIItems>().Delete(piItem);
            }

            List<PIItems> listPIItems = new List<PIItems>();

            for (int rowIndex = 0; rowIndex < rptPIItemEntryInfo.Items.Count; rowIndex++)
            {

                TextBox tbxDescription = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxDescription");
                TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");

                Label lblTotalAmount = (Label)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("lblTotalAmount");


                if (!string.IsNullOrEmpty(tbxDescription.Text) && !string.IsNullOrEmpty(tbxHSCode.Text) && !string.IsNullOrEmpty(tbxQty.Text) && !string.IsNullOrEmpty(tbxUnitPrice.Text))
                {
                    var pIITem = new PIItems()
                    {
                        PIId = piId,
                        DescriptionOfGoods = tbxDescription.Text,
                        HSCode = tbxHSCode.Text,
                        Quantity = int.Parse(tbxQty.Text),
                        UnitPrice = decimal.Parse(tbxUnitPrice.Text),
                        Amount = decimal.Parse(lblTotalAmount.Text),

                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };

                    listPIItems.Add(pIITem);
                }

            }

            foreach (var item in listPIItems)
            {
                unitOfWork.GenericRepositories<PIItems>().Insert(item);
            }

        }


    }
}

