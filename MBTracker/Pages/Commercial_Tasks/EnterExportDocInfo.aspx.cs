﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Commercial_Tasks;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Commercial_Tasks
{
    public partial class EnterExportDocInfo : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        CommercialManager commercialManager = new CommercialManager();

        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Request.QueryString["DocId"] != null)
                {
                    DocId = int.Parse(Tools.UrlDecode(Request.QueryString["DocId"]));
                    actionTitle.Text = "Update Export Document Information";
                    lnkbtnSaveEntries.Visible = false;
                    lnkbtnUpdateEntries.Visible = true;

                    LoadDocInfo();
                }
                else
                {
                    CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                    //CommonMethods.LoadDropdown(ddlAdvisingBank, "ApplicantBanks", 1, 0);
                    //CommonMethods.LoadDropdown(ddlAccountHolder, "AccountHolders", 1, 0);
                    //CommonMethods.LoadDropdown(ddlStatus, "ExportLCStatus", 1, 0);
                }
            }
        }


        int DocId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["DocId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["DocId"] = value; }
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            divPIOtherInfo2.Visible = false;
            if (ddlBuyers.SelectedValue != "")
            {
                divPIOtherInfo2.Visible = true;
                SetInitialRowCount();
                BindLCDropdown(int.Parse(ddlBuyers.SelectedValue));
                LoadCollectionStatus();
            }
        }

        protected void LoadCollectionStatus()
        {
            CommonMethods.LoadDropdown(ddlCollectionStatus, "DocCollectionStatus", 1, 0);
        }


        protected void BindLCDropdown(int buyerId)
        {
            var lcInfo = unitOfWork.GenericRepositories<ExportLCInfo>().Get(x => x.BuyerId == buyerId).ToList();
            ddlLCNumber.DataSource = lcInfo;
            ddlLCNumber.DataValueField = "LCId";
            ddlLCNumber.DataTextField = "LCNumber";
            ddlLCNumber.DataBind();
            ddlLCNumber.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlLCNumber.SelectedIndex = 0;
        }


        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("InvoiceNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("InvoiceDate", typeof(string)));

            dr = dt.NewRow();

            dr["InvoiceNumber"] = string.Empty;
            dr["InvoiceDate"] = string.Empty;

            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;

            rptEntryInfo.DataSource = dt;
            rptEntryInfo.DataBind();
        }



        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {
                var edocInfo = new ExportDocuments();
                edocInfo.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                edocInfo.LCId = int.Parse(ddlLCNumber.SelectedValue);
                edocInfo.DocumentNumber = tbxDocumentNumber.Text;
                edocInfo.DocSubmitDate = DateTime.Parse(tbxDocSubmitDate.Text);
                edocInfo.DocumentValue = decimal.Parse(tbxDocumentValue.Text);
                edocInfo.DocumentQty = decimal.Parse(tbxDocumentQty.Text);
                if (!string.IsNullOrEmpty(tbxPaymentDate.Text))
                {
                    edocInfo.PaymentDate = DateTime.Parse(tbxPaymentDate.Text);
                }
                if (!string.IsNullOrEmpty(tbxDeductValue.Text))
                {
                    edocInfo.DeductValue = decimal.Parse(tbxDeductValue.Text);
                }                
                if (!string.IsNullOrEmpty(tbxMaturityDate.Text))
                {
                    edocInfo.MaturityDate = DateTime.Parse(tbxMaturityDate.Text);
                }
                if (!string.IsNullOrEmpty(tbxRealizedValue.Text))
                {
                    edocInfo.RealizedValue = decimal.Parse(tbxRealizedValue.Text);
                }                
                if (!string.IsNullOrEmpty(tbxRealizedDate.Text))
                {
                    edocInfo.RealizedDate = DateTime.Parse(tbxRealizedDate.Text);
                }
                edocInfo.CollectionStatusId = int.Parse(ddlCollectionStatus.Text);
                edocInfo.CreatedBy = CommonMethods.SessionInfo.UserName;
                edocInfo.CreateDate = DateTime.Now;
                edocInfo.BillOfExchangeNumber = tbxBillOfExchangeNumber.Text;

                List<ExportDocumnetInvoices> lstInvoices = new List<ExportDocumnetInvoices>();

                for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                {
                    var tbxInvoiceNumber = (TextBox)rptEntryInfo.Items[i].FindControl("tbxInvoiceNumber");
                    var tbxInvoicedate = (TextBox)rptEntryInfo.Items[i].FindControl("tbxInvoicedate");
                    if (tbxInvoiceNumber.Text != "" && tbxInvoicedate.Text != "")
                    {
                        var inv = new ExportDocumnetInvoices();
                        inv.ExportDocumentId = edocInfo.ExportDocumentId;
                        inv.InvoiceNumber = tbxInvoiceNumber.Text;
                        inv.InvoiceDate = DateTime.Parse(tbxInvoicedate.Text);
                        inv.CreateDate = DateTime.Now;
                        inv.CreatedBy = CommonMethods.SessionInfo.UserName;
                        lstInvoices.Add(inv);
                    }
                }

                if(lstInvoices.Count() > 0)
                {
                    edocInfo.ExportDocumnetInvoices = lstInvoices;
                    unitOfWork.GenericRepositories<ExportDocuments>().Insert(edocInfo);
                    unitOfWork.Save();

                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('EnterExportDocInfo.aspx');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter at least one invoice data.')", true);
                }                
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private void LoadDocInfo()
        {
            try
            {
                divPIOtherInfo2.Visible = true;

                var docInfo = unitOfWork.GenericRepositories<ExportDocuments>().GetByID(DocId);
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                BindLCDropdown(docInfo.BuyerId);
                LoadCollectionStatus();

                ddlBuyers.SelectedValue = docInfo.BuyerId + "";
                ddlLCNumber.SelectedValue = docInfo.LCId+"";

                tbxDocumentNumber.Text = docInfo.DocumentNumber;
                tbxDocSubmitDate.Text = docInfo.DocSubmitDate.ToString("yyyy-MM-dd");
                tbxDocumentValue.Text = docInfo.DocumentValue + "";
                tbxDocumentQty.Text = docInfo.DocumentQty + "";
                tbxPaymentDate.Text = docInfo.PaymentDate?.ToString("yyyy-MM-dd");
                tbxDeductValue.Text = docInfo.DeductValue + "";
                tbxMaturityDate.Text = docInfo.MaturityDate?.ToString("yyyy-MM-dd");
                tbxRealizedValue.Text = docInfo.RealizedValue + "";
                tbxRealizedDate.Text = docInfo.RealizedDate?.ToString("yyyy-MM-dd");
                ddlCollectionStatus.SelectedValue = docInfo.CollectionStatusId + "";
                tbxBillOfExchangeNumber.Text = docInfo.BillOfExchangeNumber;

                var dt = unitOfWork.GetDataTableFromSql($"SELECT CAST(InvoiceNumber as nvarchar(150)) as InvoiceNumber,CAST(InvoiceDate as nvarchar(150)) as InvoiceDate FROM ExportDocumnetInvoices WHERE ExportDocumentId = '{DocId}'");
                rptEntryInfo.DataSource = dt;
                rptEntryInfo.DataBind();
                ViewState["CurrentTable"] = dt;
                SetPreviousData();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }



        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {
            try
            {
                var edocInfo = unitOfWork.GenericRepositories<ExportDocuments>().GetByID(DocId);
                var invoices = unitOfWork.GenericRepositories<ExportDocumnetInvoices>().Get(x => x.ExportDocumentId == DocId).ToList();
                foreach (var item in invoices)
                {
                    unitOfWork.GenericRepositories<ExportDocumnetInvoices>().Delete(item);
                }
                edocInfo.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                edocInfo.LCId = int.Parse(ddlLCNumber.SelectedValue);
                edocInfo.DocumentNumber = tbxDocumentNumber.Text;
                edocInfo.DocSubmitDate = DateTime.Parse(tbxDocSubmitDate.Text);
                edocInfo.DocumentValue = decimal.Parse(tbxDocumentValue.Text);
                edocInfo.DocumentQty = decimal.Parse(tbxDocumentQty.Text);
                if (!string.IsNullOrEmpty(tbxPaymentDate.Text))
                {
                    edocInfo.PaymentDate = DateTime.Parse(tbxPaymentDate.Text);
                }
                if (!string.IsNullOrEmpty(tbxDeductValue.Text))
                {
                    edocInfo.DeductValue = decimal.Parse(tbxDeductValue.Text);
                }
                if (!string.IsNullOrEmpty(tbxMaturityDate.Text))
                {
                    edocInfo.MaturityDate = DateTime.Parse(tbxMaturityDate.Text);
                }
                if (!string.IsNullOrEmpty(tbxRealizedValue.Text))
                {
                    edocInfo.RealizedValue = decimal.Parse(tbxRealizedValue.Text);
                }
                if (!string.IsNullOrEmpty(tbxRealizedDate.Text))
                {
                    edocInfo.RealizedDate = DateTime.Parse(tbxRealizedDate.Text);
                }
                edocInfo.CollectionStatusId = int.Parse(ddlCollectionStatus.Text);
                edocInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                edocInfo.UpdateDate = DateTime.Now;
                edocInfo.BillOfExchangeNumber = tbxBillOfExchangeNumber.Text;

                List<ExportDocumnetInvoices> lstInvoices = new List<ExportDocumnetInvoices>();

                for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                {
                    var tbxInvoiceNumber = (TextBox)rptEntryInfo.Items[i].FindControl("tbxInvoiceNumber");
                    var tbxInvoicedate = (TextBox)rptEntryInfo.Items[i].FindControl("tbxInvoicedate");
                    if (tbxInvoiceNumber.Text != "" && tbxInvoicedate.Text != "")
                    {
                        var inv = new ExportDocumnetInvoices();
                        inv.ExportDocumentId = edocInfo.ExportDocumentId;
                        inv.InvoiceNumber = tbxInvoiceNumber.Text;
                        inv.InvoiceDate = DateTime.Parse(tbxInvoicedate.Text);
                        inv.CreateDate = DateTime.Now;
                        inv.CreatedBy = CommonMethods.SessionInfo.UserName;
                        lstInvoices.Add(inv);
                    }
                }

                if (lstInvoices.Count() > 0)
                {
                    edocInfo.ExportDocumnetInvoices = lstInvoices;
                    unitOfWork.GenericRepositories<ExportDocuments>().Update(edocInfo);
                    unitOfWork.Save();

                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewEditExportDocInfo.aspx');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter at least one invoice data.')", true);
                }               
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
        }

        private void AddNewRowToGrid()
        {


            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];

                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {
                        TextBox tbxInvoiceNumber = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxInvoiceNumber");
                        TextBox tbxInvoicedate = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxInvoicedate");

                        dtCurrentTable.Rows[rowIndex]["InvoiceNumber"] = tbxInvoiceNumber.Text;
                        dtCurrentTable.Rows[rowIndex]["InvoiceDate"] = tbxInvoicedate.Text;

                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["InvoiceNumber"] = string.Empty;
                            drCurrentRow["InvoiceDate"] = string.Empty;
                        }
                    }

                    dtCurrentTable.Rows.InsertAt(drCurrentRow, dtCurrentTable.Rows.Count);

                    this.ViewState["CurrentTable"] = dtCurrentTable; ;
                    this.rptEntryInfo.DataSource = dtCurrentTable;
                    this.rptEntryInfo.DataBind();
                }
            }
            SetPreviousData();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        TextBox tbxInvoiceNumber = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxInvoiceNumber");
                        TextBox tbxInvoicedate = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxInvoicedate");

                        tbxInvoiceNumber.Text = dt.Rows[i]["InvoiceNumber"].ToString();
                        var stdate = dt.Rows[i]["InvoiceDate"].ToString();
                        if (!string.IsNullOrEmpty(stdate))
                        {
                            tbxInvoicedate.Text = Convert.ToDateTime(stdate).ToString("yyyy-MM-dd");
                        }

                        rowIndex++;
                    }
                }
            }
        }

    }
}


