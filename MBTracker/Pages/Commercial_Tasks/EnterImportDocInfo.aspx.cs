﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Commercial_Tasks;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Commercial_Tasks
{
    public partial class EnterImportDocInfo : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        CommercialManager commercialManager = new CommercialManager();

        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Request.QueryString["DocId"] != null)
                {
                    DocId = int.Parse(Tools.UrlDecode(Request.QueryString["DocId"]));
                    actionTitle.Text = "Update Import Document Information";
                    lnkbtnSaveEntries.Visible = false;
                    lnkbtnUpdateEntries.Visible = true;

                    LoadDocInfo();
                }
                else
                {
                    CommonMethods.LoadDropdown(ddlBeneficiary, "YarnAndAccessoriesSuppliers", 1, 0);
                    CommonMethods.LoadDropdown(ddlPaymentStatus, "PaymentStatus", 1, 0);
                }
            }
        }


        int DocId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["DocId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["DocId"] = value; }
        }


        protected void ddlBeneficiary_SelectedIndexChanged(object sender, EventArgs e)
        {
            divPIOtherInfo2.Visible = false;
            if (ddlBeneficiary.SelectedValue != "")
            {
                divPIOtherInfo2.Visible = true;
                SetInitialRowCount();
                BindLCDropdown(int.Parse(ddlBeneficiary.SelectedValue));
            }
        }


        protected void BindLCDropdown(int beneficiaryId)
        {
            var lcInfo = unitOfWork.GenericRepositories<LCInfoBackToBack>().Get(x => x.Id == beneficiaryId).ToList();
            ddlLCNumber.DataSource = lcInfo;
            ddlLCNumber.DataValueField = "Id";
            ddlLCNumber.DataTextField = "LCNumber";
            ddlLCNumber.DataBind();
            ddlLCNumber.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlLCNumber.SelectedIndex = 0;
        }


        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("InvoiceNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("InvoiceDate", typeof(string)));

            dr = dt.NewRow();

            dr["InvoiceNumber"] = string.Empty;
            dr["InvoiceDate"] = string.Empty;

            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;

            rptEntryInfo.DataSource = dt;
            rptEntryInfo.DataBind();
        }



        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {
                var idocInfo = new ImportDocuments();
                idocInfo.BeneficiaryId = int.Parse(ddlBeneficiary.SelectedValue);
                idocInfo.LCId = int.Parse(ddlLCNumber.SelectedValue);
                idocInfo.DocReceiveDate = DateTime.Parse(tbxDocReceiveDate.Text);
                idocInfo.ChalanOrBLDate = DateTime.Parse(tbxChalanOrBLDate.Text);
                idocInfo.ChalanOrBLNumber = tbxChalanrOrBLNumber.Text;
                idocInfo.DocumentValue = decimal.Parse(tbxDocumentValue.Text);
                idocInfo.DocumentQty = decimal.Parse(tbxDocumentQty.Text);
                idocInfo.PaymentStatusId = int.Parse(ddlPaymentStatus.SelectedValue);
                if (!string.IsNullOrEmpty(tbxPaymentDate.Text))
                {
                    idocInfo.PaymentDate = DateTime.Parse(tbxPaymentDate.Text);
                }               
                
                idocInfo.CreatedBy = CommonMethods.SessionInfo.UserName;
                idocInfo.CreateDate = DateTime.Now;

                List<ImportDocumnetInvoices> lstInvoices = new List<ImportDocumnetInvoices>();

                for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                {
                    var tbxInvoiceNumber = (TextBox)rptEntryInfo.Items[i].FindControl("tbxInvoiceNumber");
                    var tbxInvoicedate = (TextBox)rptEntryInfo.Items[i].FindControl("tbxInvoicedate");
                    if (tbxInvoiceNumber.Text != "" && tbxInvoicedate.Text != "")
                    {
                        var inv = new ImportDocumnetInvoices();
                        inv.ImportDocumentId = idocInfo.ImportDocumentId;
                        inv.InvoiceNumber = tbxInvoiceNumber.Text;
                        inv.InvoiceDate = DateTime.Parse(tbxInvoicedate.Text);
                        inv.CreateDate = DateTime.Now;
                        inv.CreatedBy = CommonMethods.SessionInfo.UserName;
                        lstInvoices.Add(inv);
                    }
                }

                if(lstInvoices.Count() > 0)
                {
                    idocInfo.ImportDocumnetInvoices = lstInvoices;
                    unitOfWork.GenericRepositories<ImportDocuments>().Insert(idocInfo);
                    unitOfWork.Save();

                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('EnterImportDocInfo.aspx');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter at least one invoice data.')", true);
                }                
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private void LoadDocInfo()
        {
            try
            {
                divPIOtherInfo2.Visible = true;
                var docInfo = unitOfWork.GenericRepositories<ImportDocuments>().GetByID(DocId);
                CommonMethods.LoadDropdown(ddlBeneficiary, "YarnAndAccessoriesSuppliers", 1, 0);
                CommonMethods.LoadDropdown(ddlPaymentStatus, "PaymentStatus", 1, 0);
                BindLCDropdown(docInfo.BeneficiaryId);

                ddlBeneficiary.SelectedValue = docInfo.BeneficiaryId + "";
                ddlPaymentStatus.SelectedValue = docInfo.PaymentStatusId + "";
                ddlLCNumber.SelectedValue = docInfo.LCId+"";
                tbxDocReceiveDate.Text = docInfo.DocReceiveDate.ToString("yyyy-MM-dd");
                tbxChalanrOrBLNumber.Text = docInfo.ChalanOrBLNumber;
                tbxChalanOrBLDate.Text = docInfo.ChalanOrBLDate?.ToString("yyyy-MM-dd");
                tbxDocumentQty.Text = docInfo.DocumentQty + "";
                tbxPaymentDate.Text = docInfo.PaymentDate?.ToString("yyyy-MM-dd");
                tbxDocumentValue.Text = docInfo.DocumentValue + "";

                var dt = unitOfWork.GetDataTableFromSql($"SELECT CAST(InvoiceNumber as nvarchar(150)) as InvoiceNumber,CAST(InvoiceDate as nvarchar(150)) as InvoiceDate FROM ImportDocumnetInvoices WHERE ImportDocumentId = '{DocId}'");
                rptEntryInfo.DataSource = dt;
                rptEntryInfo.DataBind();
                ViewState["CurrentTable"] = dt;
                SetPreviousData();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }



        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {
            try
            {
                var idocInfo = unitOfWork.GenericRepositories<ImportDocuments>().GetByID(DocId);
                var invoices = unitOfWork.GenericRepositories<ImportDocumnetInvoices>().Get(x => x.ImportDocumentId == DocId).ToList();
                foreach (var item in invoices)
                {
                    unitOfWork.GenericRepositories<ImportDocumnetInvoices>().Delete(item);
                }

                idocInfo.BeneficiaryId = int.Parse(ddlBeneficiary.SelectedValue);
                idocInfo.LCId = int.Parse(ddlLCNumber.SelectedValue);
                idocInfo.DocReceiveDate = DateTime.Parse(tbxDocReceiveDate.Text);
                idocInfo.ChalanOrBLDate = DateTime.Parse(tbxChalanOrBLDate.Text);
                idocInfo.ChalanOrBLNumber = tbxChalanrOrBLNumber.Text;
                idocInfo.DocumentValue = decimal.Parse(tbxDocumentValue.Text);
                idocInfo.DocumentQty = decimal.Parse(tbxDocumentQty.Text);
                idocInfo.PaymentStatusId = int.Parse(ddlPaymentStatus.SelectedValue);
                if (!string.IsNullOrEmpty(tbxPaymentDate.Text))
                {
                    idocInfo.PaymentDate = DateTime.Parse(tbxPaymentDate.Text);
                }

                idocInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                idocInfo.UpdateDate = DateTime.Now;

                List<ImportDocumnetInvoices> lstInvoices = new List<ImportDocumnetInvoices>();

                for (int i = 0; i < rptEntryInfo.Items.Count; i++)
                {
                    var tbxInvoiceNumber = (TextBox)rptEntryInfo.Items[i].FindControl("tbxInvoiceNumber");
                    var tbxInvoicedate = (TextBox)rptEntryInfo.Items[i].FindControl("tbxInvoicedate");
                    if (tbxInvoiceNumber.Text != "" && tbxInvoicedate.Text != "")
                    {
                        var inv = new ImportDocumnetInvoices();
                        inv.ImportDocumentId = idocInfo.ImportDocumentId;
                        inv.InvoiceNumber = tbxInvoiceNumber.Text;
                        inv.InvoiceDate = DateTime.Parse(tbxInvoicedate.Text);
                        inv.CreateDate = DateTime.Now;
                        inv.CreatedBy = CommonMethods.SessionInfo.UserName;
                        lstInvoices.Add(inv);
                    }
                }

                if (lstInvoices.Count() > 0)
                {
                    idocInfo.ImportDocumnetInvoices = lstInvoices;
                    unitOfWork.GenericRepositories<ImportDocuments>().Update(idocInfo);
                    unitOfWork.Save();

                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewEditImportDocInfo.aspx');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please enter at least one invoice data.')", true);
                }               
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
        }

        private void AddNewRowToGrid()
        {


            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];

                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {
                        TextBox tbxInvoiceNumber = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxInvoiceNumber");
                        TextBox tbxInvoicedate = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxInvoicedate");

                        dtCurrentTable.Rows[rowIndex]["InvoiceNumber"] = tbxInvoiceNumber.Text;
                        dtCurrentTable.Rows[rowIndex]["InvoiceDate"] = tbxInvoicedate.Text;

                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["InvoiceNumber"] = string.Empty;
                            drCurrentRow["InvoiceDate"] = string.Empty;
                        }
                    }

                    dtCurrentTable.Rows.InsertAt(drCurrentRow, dtCurrentTable.Rows.Count);

                    this.ViewState["CurrentTable"] = dtCurrentTable; ;
                    this.rptEntryInfo.DataSource = dtCurrentTable;
                    this.rptEntryInfo.DataBind();
                }
            }
            SetPreviousData();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        TextBox tbxInvoiceNumber = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxInvoiceNumber");
                        TextBox tbxInvoicedate = (TextBox)this.rptEntryInfo.Items[rowIndex].FindControl("tbxInvoicedate");

                        tbxInvoiceNumber.Text = dt.Rows[i]["InvoiceNumber"].ToString();
                        var stdate = dt.Rows[i]["InvoiceDate"].ToString();
                        if (!string.IsNullOrEmpty(stdate))
                        {
                            tbxInvoicedate.Text = Convert.ToDateTime(stdate).ToString("yyyy-MM-dd");
                        }

                        rowIndex++;
                    }
                }
            }
        }

    }
}


