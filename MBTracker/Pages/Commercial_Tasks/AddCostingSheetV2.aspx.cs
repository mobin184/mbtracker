﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Commercial_Tasks;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Commercial_Tasks
{
    public partial class AddCostingSheetV2 : System.Web.UI.Page
    {

        CommercialManager commercialManager = new CommercialManager();

        UnitOfWork unitOfWork = new UnitOfWork();
        DataTable constingInfoDetails;
        int initialLoadForUpdate = 0;

        decimal totalPricePerDzn = 0;
        decimal grandTotalValue = 0;


        protected void Page_Load(object sender, EventArgs e)
        {



            if (!IsPostBack)
            {

                if (EnableDisableStatusDropdown())
                {
                    ddlStatus.Enabled = true;
                }
                else
                {
                    ddlStatus.Enabled = false;
                }


                if (Request.QueryString["costingSheetId"] != null)
                {
                    CostingInfoId = int.Parse(Tools.UrlDecode(Request.QueryString["costingSheetId"]));
                    initialLoadForUpdate = CostingInfoId;
                    actionTitle.Text = "Udpate Costing Information";
                    lnkbtnSaveEntries.Visible = false;
                    lnkbtnUpdateEntries.Visible = true;

                    LoadCostingInfo();
                }
                else
                {
                    CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                   
                }
            }
        }


        private bool EnableDisableStatusDropdown()
        {


            var isExist = false;

            var pageName = "CostingStatusChange";
            var controlType = 3;
            var buttonNumberOnpage = 1;

            string sql = $"Exec usp_GetRoleIdsForEditDeleteButton '{pageName}', {controlType}, {buttonNumberOnpage}";

            UnitOfWork unitOfWork = new UnitOfWork();
            var dt = unitOfWork.GetDataTableFromSql(sql);
            var currentUserRoleId = CommonMethods.SessionInfo.RoleId;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (int.Parse(dt.Rows[i][0].ToString()) == currentUserRoleId)
                {
                    isExist = true;
                    break;
                }
            }

            return isExist;
        }


        int CostingInfoId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["costingSheetId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["costingSheetId"] = value; }
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            divCostingSheet.Visible = false;

            DataTable dt = new DataTable();
            ddlSeason.DataSource = dt;
            ddlSeason.DataBind();

            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
                CommonMethods.LoadDropdownById(ddlSeason, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerSeasons", 1, 0);

                ddlOrders.Items.Clear();
            }
            else
            {
                ddlStyles.Items.Clear();
                ddlOrders.Items.Clear();

            }
        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {

            DataTable dt = new DataTable();


            //dt.Columns.Add(new DataColumn("ItemTypeId", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));
            dt.Columns.Add(new DataColumn("OrderQty", typeof(string)));
            dt.Columns.Add(new DataColumn("ConsumptionPerDzn", typeof(string)));
            dt.Columns.Add(new DataColumn("Wastage", typeof(string)));

            dt.Columns.Add(new DataColumn("BookingQtyPerDzn", typeof(string)));
            dt.Columns.Add(new DataColumn("ConfirmedPrice", typeof(string)));
            dt.Columns.Add(new DataColumn("PricePerDzn", typeof(string)));
            dt.Columns.Add(new DataColumn("SupplierId", typeof(string)));
            dt.Columns.Add(new DataColumn("TotalValue", typeof(string)));




            //for (int i = 0; i < cblCostingItems.Items.Count; i++)
            for (int i = 0; i < 1; i++)
            {
                // if (cblCostingItems.Items[i].Selected)
                if (1 == 1)
                {


                    DataRow dr = null;
                    dr = dt.NewRow();

                    //dr["ItemTypeId"] = string.Empty;
                    //dr["ItemId"] = cblCostingItems.Items[i].Value.ToString();
                    dr["ItemId"] = string.Empty;
                    dr["OrderQty"] = tbxOrderQty.Text.ToString();
                    dr["ConsumptionPerDzn"] = string.Empty;
                    dr["Wastage"] = string.Empty;
                    dr["BookingQtyPerDzn"] = string.Empty;
                    dr["ConfirmedPrice"] = string.Empty;
                    dr["PricePerDzn"] = string.Empty;
                    dr["SupplierId"] = string.Empty;
                    dr["TotalValue"] = string.Empty;


                    dt.Rows.Add(dr);
                }
            }

            if (dt.Rows.Count > 0)
            {
                divCostingSheet.Visible = true;
            }
            else
            {
                divCostingSheet.Visible = false;
            }

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptCostingEntryInfo.DataSource = dt;
            rptCostingEntryInfo.DataBind();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(3000)", true);


            if (ddlBuyers.SelectedValue != "" && ddlStyles.SelectedValue != "")
            {
                BindSeasonByBuyer(Convert.ToInt32(ddlBuyers.SelectedValue), Convert.ToInt32(ddlStyles.SelectedValue));
            }
            else
            {
                ddlSeason.Items.Clear();
            }

            //divCostingItems.Visible = false;

            //if (ddlStyles.SelectedValue != "")
            //{
            //    CommonMethods.LoadOrderDropdownByStyle(ddlOrders, Convert.ToInt32(ddlStyles.SelectedValue), 1, 0);
            //    CommonMethods.LoadCostingItemsCheckBoxList(cblCostingItems);


            //    //SetInitialRowCount();
            //    //divCostingSheet.Visible = true;

            //    divCostingItems.Visible = true;


            //}
            //else
            //{
            //    ddlOrders.Items.Clear();

            //}


        }

        private void BindSeasonByBuyer(int buyerId, int StyleId)
        {
            //CommonMethods.LoadDropdown(ddlBuyersSeason, "BuyerSeasons WHERE BuyerId = " + buyerId, 1, 0);
            CommonMethods.LoadDropdownSeasonByBuyerAndStyleId(ddlSeason, buyerId, StyleId, 1, 0);
            ddlSeason.Enabled = false;
        }


        protected void btnSetupCostingSheet_Click(object sender, EventArgs e)
        {

            DataTable dt = new DataTable();


            //dt.Columns.Add(new DataColumn("ItemTypeId", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));
            dt.Columns.Add(new DataColumn("OrderQty", typeof(string)));
            dt.Columns.Add(new DataColumn("ConsumptionPerDzn", typeof(string)));
            dt.Columns.Add(new DataColumn("Wastage", typeof(string)));

            dt.Columns.Add(new DataColumn("BookingQtyPerDzn", typeof(string)));
            dt.Columns.Add(new DataColumn("ConfirmedPrice", typeof(string)));
            dt.Columns.Add(new DataColumn("PricePerDzn", typeof(string)));
            dt.Columns.Add(new DataColumn("SupplierId", typeof(string)));
            dt.Columns.Add(new DataColumn("TotalValue", typeof(string)));




            for (int i = 0; i < cblCostingItems.Items.Count; i++)
            {
                if (cblCostingItems.Items[i].Selected)
                {


                    DataRow dr = null;
                    dr = dt.NewRow();

                    //dr["ItemTypeId"] = string.Empty;
                    dr["ItemId"] = cblCostingItems.Items[i].Value.ToString();
                    dr["OrderQty"] = tbxOrderQty.Text.ToString();
                    dr["ConsumptionPerDzn"] = string.Empty;
                    dr["Wastage"] = string.Empty;
                    dr["BookingQtyPerDzn"] = string.Empty;
                    dr["ConfirmedPrice"] = string.Empty;
                    dr["PricePerDzn"] = string.Empty;
                    dr["SupplierId"] = string.Empty;
                    dr["TotalValue"] = string.Empty;


                    dt.Rows.Add(dr);
                }
            }

            if (dt.Rows.Count > 0)
            {
                divCostingSheet.Visible = true;
            }
            else
            {
                divCostingSheet.Visible = false;
            }

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptCostingEntryInfo.DataSource = dt;
            rptCostingEntryInfo.DataBind();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(3000)", true);
        }

        protected void rptCostingEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                if (initialLoadForUpdate > 0)
                {
                    //var ddlItemType = (DropDownList)e.Item.FindControl("ddlItemType");
                    var ddlItems = (DropDownList)e.Item.FindControl("ddlItems");
                    var ddlSupplier = (DropDownList)e.Item.FindControl("ddlSupplier");


                    var tbxOrderQty = (TextBox)e.Item.FindControl("tbxOrderQty");
                    var tbxConsumptionPerDzn = (TextBox)e.Item.FindControl("tbxConsumptionPerDzn");
                    var tbxWastage = (TextBox)e.Item.FindControl("tbxWastage");
                    var tbxBookingQtyPerDzn = (TextBox)e.Item.FindControl("tbxBookingQtyPerDzn");
                    var tbxConfirmedPrice = (TextBox)e.Item.FindControl("tbxConfirmedPrice");
                    var lblPricePerDznValue = (Label)e.Item.FindControl("lblPricePerDznValue");
                    var lblTotalValueValue = (Label)e.Item.FindControl("lblTotalValueValue");



                    //var costingItemTypeId = DataBinder.Eval(e.Item.DataItem, "CostingItemTypeId").ToString();
                    var itemSupplierId = DataBinder.Eval(e.Item.DataItem, "ItemSupplierId").ToString();


                    var itemId = DataBinder.Eval(e.Item.DataItem, "ItemId").ToString();

                    var orderQty = DataBinder.Eval(e.Item.DataItem, "OrderQty").ToString();
                    var consumptionPerDzn = DataBinder.Eval(e.Item.DataItem, "ConsumptionPerDzn").ToString();
                    var wastagePercentage = DataBinder.Eval(e.Item.DataItem, "WastagePercentage").ToString();
                    var bookingQtyPerDzn = DataBinder.Eval(e.Item.DataItem, "BookingQtyPerDzn").ToString();
                    var confirmedPrice = DataBinder.Eval(e.Item.DataItem, "ConfirmedPrice").ToString();
                    var pricePerDzn = DataBinder.Eval(e.Item.DataItem, "PricePerDzn").ToString();
                    var totalValue = DataBinder.Eval(e.Item.DataItem, "TotalValue").ToString();


                    //LoadRawMaterialItemTypDropdown(ddlItemType);
                    LoadSuppliers(ddlSupplier);
                    //ddlItemType.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItemType, int.Parse(costingItemTypeId));
                    ddlSupplier.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSupplier, int.Parse(itemSupplierId));


                    //CommonMethods.LoadYarnAndAccessoriesItemDropdownByType(ddlItems, int.Parse(ddlItemType.SelectedValue), 1, 0);
                    CommonMethods.LoadYarnAndAccessoriesItemsDropdown(ddlItems, 1, 0);
                    ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse(string.IsNullOrEmpty(itemId) ? "0" : itemId));



                    tbxOrderQty.Text = orderQty.ToString();
                    tbxConsumptionPerDzn.Text = consumptionPerDzn.ToString();
                    tbxWastage.Text = wastagePercentage.ToString();
                    tbxBookingQtyPerDzn.Text = bookingQtyPerDzn.ToString();

                    tbxConfirmedPrice.Text = confirmedPrice.ToString();
                    lblPricePerDznValue.Text = pricePerDzn.ToString();
                    lblTotalValueValue.Text = totalValue.ToString();

                    if (pricePerDzn != "")
                    {
                        totalPricePerDzn += decimal.Parse(pricePerDzn);
                    }

                    if (totalValue != "")
                    {
                        grandTotalValue += decimal.Parse(totalValue);
                    }

                }
                else
                {

                    //var ddlItemType = (DropDownList)e.Item.FindControl("ddlItemType");
                    //LoadRawMaterialItemTypDropdown(ddlItemType);

                    var ddlItems = (DropDownList)e.Item.FindControl("ddlItems");
                    CommonMethods.LoadYarnAndAccessoriesItemsDropdown(ddlItems, 1, 0);

                    var itemId = DataBinder.Eval(e.Item.DataItem, "ItemId").ToString();
                    ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse(string.IsNullOrEmpty(itemId) ? "0" : itemId));


                    var tbxOrderQty = (TextBox)e.Item.FindControl("tbxOrderQty");
                    var orderQty = DataBinder.Eval(e.Item.DataItem, "OrderQty").ToString();

                    tbxOrderQty.Text = orderQty;

                    var ddlSupplier = (DropDownList)e.Item.FindControl("ddlSupplier");
                    LoadSuppliers(ddlSupplier);

                    var pricePerDznAddingTime = DataBinder.Eval(e.Item.DataItem, "PricePerDzn").ToString();
                    var totalValueAdditingTime = DataBinder.Eval(e.Item.DataItem, "TotalValue").ToString();

                    if (pricePerDznAddingTime != "")
                    {
                        totalPricePerDzn += decimal.Parse(pricePerDznAddingTime);
                    }

                    if (totalValueAdditingTime != "")
                    {
                        grandTotalValue += decimal.Parse(totalValueAdditingTime);
                    }

                }



            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                //if (initialLoadForUpdate > 0)
                //{
                Label lblTotalPricePerDzn = (Label)e.Item.FindControl("lblTotalPricePerDzn");
                Label lblGrandTotalValue = (Label)e.Item.FindControl("lblGrandTotalValue");


                lblTotalPricePerDzn.Text = totalPricePerDzn.ToString();
                lblGrandTotalValue.Text = grandTotalValue.ToString();

                //}
            }

        }


        protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbxOrderQty.Text = "";
            ddlSeason.SelectedIndex = 0;
            tbxOrderQty.Enabled = true;
            ddlSeason.Enabled = true;

            if (ddlOrders.SelectedValue != "")
            {
                DataTable dtBasicInfo = unitOfWork.GetDataTableFromSql($"Exec usp_GetOrderInfoByOrderId {int.Parse(ddlOrders.SelectedValue)}");

                if (dtBasicInfo.Rows.Count > 0)
                {

                    if (!string.IsNullOrEmpty(dtBasicInfo.Rows[0]["OrderQuantity"].ToString()))
                    {
                        tbxOrderQty.Text = dtBasicInfo.Rows[0].Field<int>("OrderQuantity").ToString();
                        tbxOrderQty.Enabled = false;
                    }

                    ddlSeason.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSeason, int.Parse((string.IsNullOrEmpty(dtBasicInfo.Rows[0]["OrderSeasonId"].ToString()) ? "0" : dtBasicInfo.Rows[0]["OrderSeasonId"].ToString())));

                    if (ddlSeason.SelectedIndex != 0)
                    {
                        ddlSeason.Enabled = false;
                    }
               
                }
            }

        }

        private void SetCurrentRowCount()
        {

            DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];

            if (dtCurrentTable.Rows.Count > 0)
            {
                for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                {


                    //DropDownList ddlItemType = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlItemType");
                    DropDownList ddlSupplier = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlSupplier");

                    DropDownList ddlItems = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlItems");


                    TextBox tbxOrderQty = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxOrderQty");
                    TextBox tbxConsumptionPerDzn = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxConsumptionPerDzn");


                    TextBox tbxWastage = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxWastage");
                    TextBox tbxBookingQtyPerDzn = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxBookingQtyPerDzn");
                    TextBox tbxConfirmedPrice = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxConfirmedPrice");

                    Label lblPricePerDznValue = (Label)this.rptCostingEntryInfo.Items[rowIndex].FindControl("lblPricePerDznValue");
                    Label lblTotalValueValue = (Label)this.rptCostingEntryInfo.Items[rowIndex].FindControl("lblTotalValueValue");


                    //dtCurrentTable.Rows[rowIndex]["ItemTypeId"] = (ddlItemType.SelectedValue == "" ? "0" : ddlItemType.SelectedValue);

                    dtCurrentTable.Rows[rowIndex]["ItemId"] = (ddlItems.SelectedValue == "" ? "0" : ddlItems.SelectedValue);


                    dtCurrentTable.Rows[rowIndex]["SupplierId"] = (ddlSupplier.SelectedValue == "" ? "0" : ddlSupplier.SelectedValue);


                    dtCurrentTable.Rows[rowIndex]["OrderQty"] = tbxOrderQty.Text;
                    dtCurrentTable.Rows[rowIndex]["ConsumptionPerDzn"] = tbxConsumptionPerDzn.Text;

                    dtCurrentTable.Rows[rowIndex]["Wastage"] = tbxWastage.Text;
                    dtCurrentTable.Rows[rowIndex]["BookingQtyPerDzn"] = tbxBookingQtyPerDzn.Text;
                    dtCurrentTable.Rows[rowIndex]["ConfirmedPrice"] = tbxConfirmedPrice.Text;

                    dtCurrentTable.Rows[rowIndex]["PricePerDzn"] = lblPricePerDznValue.Text;
                    dtCurrentTable.Rows[rowIndex]["TotalValue"] = lblTotalValueValue.Text;


                }


                this.ViewState["CurrentTable"] = dtCurrentTable;

                this.rptCostingEntryInfo.DataSource = dtCurrentTable;
                this.rptCostingEntryInfo.DataBind();
            }


            this.SetPreviousData();

        }

        //private void SetInitialRowCount()
        //{
        //    DataTable dt = new DataTable();
        //    DataRow dr = null;
        //    dt.Columns.Add(new DataColumn("ItemTypeId", typeof(string)));
        //    dt.Columns.Add(new DataColumn("ItemId", typeof(string)));
        //    dt.Columns.Add(new DataColumn("OrderQty", typeof(string)));
        //    dt.Columns.Add(new DataColumn("ConsumptionPerDzn", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Wastage", typeof(string)));

        //    dt.Columns.Add(new DataColumn("BookingQtyPerDzn", typeof(string)));
        //    dt.Columns.Add(new DataColumn("ConfirmedPrice", typeof(string)));
        //    dt.Columns.Add(new DataColumn("PricePerDzn", typeof(string)));
        //    dt.Columns.Add(new DataColumn("SupplierId", typeof(string)));
        //    dt.Columns.Add(new DataColumn("TotalValue", typeof(string)));


        //    dr = dt.NewRow();

        //    dr["ItemTypeId"] = string.Empty;
        //    dr["ItemId"] = string.Empty;
        //    dr["OrderQty"] = string.Empty;
        //    dr["ConsumptionPerDzn"] = string.Empty;
        //    dr["Wastage"] = string.Empty;
        //    dr["BookingQtyPerDzn"] = string.Empty;
        //    dr["ConfirmedPrice"] = string.Empty;
        //    dr["PricePerDzn"] = string.Empty;
        //    dr["SupplierId"] = string.Empty;
        //    dr["TotalValue"] = string.Empty;


        //    dt.Rows.Add(dr);

        //    //Store the DataTable in ViewState
        //    ViewState["CurrentTable"] = dt;

        //    rptCostingEntryInfo.DataSource = dt;
        //    rptCostingEntryInfo.DataBind();
        //}



        //private void LoadRawMaterialItemTypDropdown(DropDownList ddl)
        //{
        //    CommonMethods.LoadYarnAndAccessoriesItemTypeDropdown(ddl, 1, 0);
        //}

        private void LoadSuppliers(DropDownList ddl)
        {
            CommonMethods.LoadYarnAndAccessoriesSupplierDropdown(ddl, 1, 0);
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {


                        //DropDownList ddlItemType = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlItemType");
                        DropDownList ddlSupplier = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlSupplier");


                        DropDownList ddlItems = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlItems");

                        TextBox tbxOrderQty = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxOrderQty");
                        TextBox tbxConsumptionPerDzn = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxConsumptionPerDzn");


                        TextBox tbxWastage = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxWastage");
                        TextBox tbxBookingQtyPerDzn = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxBookingQtyPerDzn");
                        TextBox tbxConfirmedPrice = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxConfirmedPrice");

                        Label lblPricePerDznValue = (Label)this.rptCostingEntryInfo.Items[rowIndex].FindControl("lblPricePerDznValue");
                        Label lblTotalValueValue = (Label)this.rptCostingEntryInfo.Items[rowIndex].FindControl("lblTotalValueValue");


                       // dtCurrentTable.Rows[rowIndex]["ItemTypeId"] = (ddlItemType.SelectedValue == "" ? "0" : ddlItemType.SelectedValue);
                        dtCurrentTable.Rows[rowIndex]["ItemId"] = (ddlItems.SelectedValue == "" ? "0" : ddlItems.SelectedValue);

                        dtCurrentTable.Rows[rowIndex]["SupplierId"] = (ddlSupplier.SelectedValue == "" ? "0" : ddlSupplier.SelectedValue);

                        dtCurrentTable.Rows[rowIndex]["OrderQty"] = tbxOrderQty.Text;
                        dtCurrentTable.Rows[rowIndex]["ConsumptionPerDzn"] = tbxConsumptionPerDzn.Text;

                        dtCurrentTable.Rows[rowIndex]["Wastage"] = tbxWastage.Text;
                        dtCurrentTable.Rows[rowIndex]["BookingQtyPerDzn"] = tbxBookingQtyPerDzn.Text;
                        dtCurrentTable.Rows[rowIndex]["ConfirmedPrice"] = tbxConfirmedPrice.Text;

                        dtCurrentTable.Rows[rowIndex]["PricePerDzn"] = lblPricePerDznValue.Text;
                        dtCurrentTable.Rows[rowIndex]["TotalValue"] = lblTotalValueValue.Text;



                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();

                            //drCurrentRow["ItemTypeId"] = string.Empty;
                            drCurrentRow["ItemId"] = string.Empty;
                            drCurrentRow["OrderQty"] = tbxOrderQty.Text;
                            drCurrentRow["ConsumptionPerDzn"] = string.Empty;
                            drCurrentRow["Wastage"] = string.Empty;

                            drCurrentRow["BookingQtyPerDzn"] = string.Empty;
                            drCurrentRow["ConfirmedPrice"] = string.Empty;
                            drCurrentRow["PricePerDzn"] = string.Empty;
                            drCurrentRow["SupplierId"] = string.Empty;
                            drCurrentRow["TotalValue"] = string.Empty;


                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptCostingEntryInfo.DataSource = dtCurrentTable;
                    this.rptCostingEntryInfo.DataBind();
                }
            }
            this.SetPreviousData();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        //DropDownList ddlItemType = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlItemType");
                        DropDownList ddlSupplier = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlSupplier");
                        DropDownList ddlItems = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlItems");


                        //TextBox tbxDescription = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxDescription");
                        TextBox tbxOrderQty = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxOrderQty");
                        TextBox tbxConsumptionPerDzn = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxConsumptionPerDzn");


                        TextBox tbxWastage = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxWastage");
                        TextBox tbxBookingQtyPerDzn = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxBookingQtyPerDzn");
                        TextBox tbxConfirmedPrice = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxConfirmedPrice");

                        Label lblPricePerDznValue = (Label)this.rptCostingEntryInfo.Items[rowIndex].FindControl("lblPricePerDznValue");
                        Label lblTotalValueValue = (Label)this.rptCostingEntryInfo.Items[rowIndex].FindControl("lblTotalValueValue");


                       // this.LoadRawMaterialItemTypDropdown(ddlItemType);

                        this.LoadSuppliers(ddlSupplier);



                        //ddlItemType.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItemType, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ItemTypeId"].ToString()) ? "0" : dt.Rows[i]["ItemTypeId"].ToString())));

                        //if (ddlItemType.SelectedValue != "")
                        //{
                        //    CommonMethods.LoadYarnAndAccessoriesItemDropdownByType(ddlItems, int.Parse(ddlItemType.SelectedValue), 1, 0);
                        //    ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ItemId"].ToString()) ? "0" : dt.Rows[i]["ItemId"].ToString())));
                        //}


                     
                        CommonMethods.LoadYarnAndAccessoriesItemsDropdown(ddlItems, 1, 0);
                        ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ItemId"].ToString()) ? "0" : dt.Rows[i]["ItemId"].ToString())));



                        ddlSupplier.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSupplier, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["SupplierId"].ToString()) ? "0" : dt.Rows[i]["SupplierId"].ToString())));


                        tbxOrderQty.Text = dt.Rows[i]["OrderQty"].ToString();
                        tbxConsumptionPerDzn.Text = dt.Rows[i]["ConsumptionPerDzn"].ToString();

                        tbxWastage.Text = dt.Rows[i]["Wastage"].ToString();
                        tbxBookingQtyPerDzn.Text = dt.Rows[i]["BookingQtyPerDzn"].ToString();
                        tbxConfirmedPrice.Text = dt.Rows[i]["ConfirmedPrice"].ToString();

                        lblPricePerDznValue.Text = dt.Rows[i]["PricePerDzn"].ToString();
                        lblTotalValueValue.Text = dt.Rows[i]["TotalValue"].ToString();


                        rowIndex++;
                    }
                }
            }
        }

        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }

            if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select an style.')", true);
            }
            if (ddlSeason.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select the season.')", true);
            }
            else if (String.IsNullOrEmpty(tbxOrderQty.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter order quantity.')", true);
            }
            else if (String.IsNullOrEmpty(tbxFOBPrice.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter FOB price.')", true);
            }
            else if (String.IsNullOrEmpty(tbxItem.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter product description.')", true);
            }
            else if (String.IsNullOrEmpty(tbxDeliveryDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter delivery date.')", true);
            }
            else if (String.IsNullOrEmpty(tbxCommissionOrCharge.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter commission or charge information.')", true);
            }
            else if (!CheckItems())
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter item(s) info correctly.')", true);
            }
            else
            {
                try
                {

                    List<OrderCostingInfoDetails> listOrderCostingInfoDetails = new List<OrderCostingInfoDetails>();

                    int orderId = 0;
                    if (ddlOrders.SelectedValue != "")
                    {
                        orderId = int.Parse(ddlOrders.SelectedValue);
                    }

                    var oci = new OrderCostingInfo()
                    {
                        BuyerId = int.Parse(ddlBuyers.SelectedValue),
                        StyleId = int.Parse(ddlStyles.SelectedValue),
                        OrderId = orderId,
                        OrderQty = int.Parse(tbxOrderQty.Text),
                        OrderSeasonId = int.Parse(ddlSeason.SelectedValue),
                        LCNumber = tbxLCNo.Text.ToString(),

                        LCValue = tbxLCValue.Text,
                        FOBPrice = tbxFOBPrice.Text,
                        ItemDescription = tbxItem.Text,
                        GarmentsWeight = tbxGarmentsWt.Text,
                        DeliveryDate = DateTime.Parse(tbxDeliveryDate.Text),
                        CommissionOrCharge = tbxCommissionOrCharge.Text,
                        Status = int.Parse(ddlStatus.SelectedValue),
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };

                    unitOfWork.GenericRepositories<OrderCostingInfo>().Insert(oci);
                    unitOfWork.Save();

                    for (int rowIndex = 0; rowIndex < rptCostingEntryInfo.Items.Count; rowIndex++)
                    {


                       // DropDownList ddlItemType = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlItemType");
                        DropDownList ddlSupplier = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlSupplier");

                        DropDownList ddlItems = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlItems");

                        TextBox tbxOrderQty = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxOrderQty");

                        TextBox tbxConsumptionPerDzn = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxConsumptionPerDzn");
                        TextBox tbxWastage = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxWastage");


                        TextBox tbxBookingQtyPerDzn = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxBookingQtyPerDzn");
                        TextBox tbxConfirmedPrice = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxConfirmedPrice");

                        Label lblPricePerDznValue = (Label)this.rptCostingEntryInfo.Items[rowIndex].FindControl("lblPricePerDznValue");
                        Label lblTotalValueValue = (Label)this.rptCostingEntryInfo.Items[rowIndex].FindControl("lblTotalValueValue");

                        if (ddlSupplier.SelectedIndex != 0 && ddlItems.SelectedIndex != 0 && !string.IsNullOrEmpty(tbxOrderQty.Text) && !string.IsNullOrEmpty(tbxConsumptionPerDzn.Text) && !string.IsNullOrEmpty(tbxWastage.Text) && !string.IsNullOrEmpty(tbxBookingQtyPerDzn.Text) && !string.IsNullOrEmpty(tbxConfirmedPrice.Text))
                        {
                            var ocid = new OrderCostingInfoDetails()
                            {
                                OrderCostingInfoId = oci.Id,
                                ItemId = int.Parse(ddlItems.SelectedValue),
                                OrderQty = int.Parse(tbxOrderQty.Text),
                                ConsumptionPerDzn = decimal.Parse(tbxConsumptionPerDzn.Text),
                                WastagePercentage = decimal.Parse(tbxWastage.Text),
                                BookingQtyPerDzn = decimal.Parse(tbxBookingQtyPerDzn.Text),

                                ConfirmedPrice = decimal.Parse(tbxConfirmedPrice.Text),
                                PricePerDzn = decimal.Parse(lblPricePerDznValue.Text),
                                ItemSupplierId = int.Parse(ddlSupplier.SelectedValue),
                                TotalValue = decimal.Parse(lblTotalValueValue.Text),

                                CreateDate = DateTime.Now,
                                CreatedBy = CommonMethods.SessionInfo.UserName
                            };
                            listOrderCostingInfoDetails.Add(ocid);
                        }

                    }


                    foreach (var item in listOrderCostingInfoDetails)
                    {
                        unitOfWork.GenericRepositories<OrderCostingInfoDetails>().Insert(item);
                    }

                    unitOfWork.Save();


                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ReloadScript", "reloadPage();", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);



                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }

        }

        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }

            if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select an style.')", true);
            }
            if (ddlSeason.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select the season.')", true);
            }
            else if (String.IsNullOrEmpty(tbxOrderQty.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter order quantity.')", true);
            }
            else if (String.IsNullOrEmpty(tbxFOBPrice.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter FOB price.')", true);
            }
            else if (String.IsNullOrEmpty(tbxItem.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter product description.')", true);
            }
            else if (String.IsNullOrEmpty(tbxDeliveryDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter delivery date.')", true);
            }
            else if (String.IsNullOrEmpty(tbxCommissionOrCharge.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter commission or charge information.')", true);
            }
            else if (!CheckItems())
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter item(s) info correctly.')", true);
            }
            else
            {
                try
                {

                    commercialManager.CopyCostingSheet(CostingInfoId, CommonMethods.SessionInfo.UserName);


                    int orderId = 0;
                    if (ddlOrders.SelectedValue != "")
                    {
                        orderId = int.Parse(ddlOrders.SelectedValue);
                    }

                    var costingInfo = unitOfWork.GenericRepositories<OrderCostingInfo>().GetByID(CostingInfoId);

                    costingInfo.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                    costingInfo.StyleId = int.Parse(ddlStyles.SelectedValue);
                    costingInfo.OrderId = orderId;
                    costingInfo.OrderQty = int.Parse(tbxOrderQty.Text);
                    costingInfo.OrderSeasonId = int.Parse(ddlSeason.SelectedValue);
                    costingInfo.LCNumber = tbxLCNo.Text.ToString();


                    costingInfo.LCValue = tbxLCValue.Text.ToString();
                    costingInfo.FOBPrice = tbxFOBPrice.Text.ToString();
                    costingInfo.ItemDescription = tbxItem.Text;
                    costingInfo.GarmentsWeight = tbxGarmentsWt.Text.ToString();
                    costingInfo.DeliveryDate = DateTime.Parse(tbxDeliveryDate.Text);
                    costingInfo.CommissionOrCharge = tbxCommissionOrCharge.Text;
                    costingInfo.Status = int.Parse(ddlStatus.SelectedValue);

                    costingInfo.UpdateDate = DateTime.Now;
                    costingInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;

                    unitOfWork.GenericRepositories<OrderCostingInfo>().Update(costingInfo);
                    unitOfWork.Save();



                    var costingItemDetails = unitOfWork.GenericRepositories<OrderCostingInfoDetails>().Get(x => x.OrderCostingInfoId == CostingInfoId);
                    foreach (var item in costingItemDetails)
                    {
                        unitOfWork.GenericRepositories<OrderCostingInfoDetails>().Delete(item);
                    }


                    List<OrderCostingInfoDetails> listOrderCostingInfoDetails = new List<OrderCostingInfoDetails>();

                    for (int rowIndex = 0; rowIndex < rptCostingEntryInfo.Items.Count; rowIndex++)
                    {

                        //DropDownList ddlItemType = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlItemType");
                        DropDownList ddlSupplier = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlSupplier");
                        DropDownList ddlItems = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlItems");


                        //TextBox tbxDescription = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxDescription");

                        TextBox tbxOrderQty = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxOrderQty");

                        TextBox tbxConsumptionPerDzn = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxConsumptionPerDzn");
                        TextBox tbxWastage = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxWastage");


                        TextBox tbxBookingQtyPerDzn = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxBookingQtyPerDzn");
                        TextBox tbxConfirmedPrice = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxConfirmedPrice");

                        Label lblPricePerDznValue = (Label)this.rptCostingEntryInfo.Items[rowIndex].FindControl("lblPricePerDznValue");
                        Label lblTotalValueValue = (Label)this.rptCostingEntryInfo.Items[rowIndex].FindControl("lblTotalValueValue");


                        if (ddlSupplier.SelectedIndex != 0 && ddlItems.SelectedIndex != 0 && !string.IsNullOrEmpty(tbxOrderQty.Text) && !string.IsNullOrEmpty(tbxConsumptionPerDzn.Text) && !string.IsNullOrEmpty(tbxWastage.Text) && !string.IsNullOrEmpty(tbxBookingQtyPerDzn.Text) && !string.IsNullOrEmpty(tbxConfirmedPrice.Text))
                        {

                            var ocid = new OrderCostingInfoDetails()
                            {
                                OrderCostingInfoId = CostingInfoId,
                                ItemId = int.Parse(ddlItems.SelectedValue),
                                OrderQty = int.Parse(tbxOrderQty.Text),
                                ConsumptionPerDzn = decimal.Parse(tbxConsumptionPerDzn.Text),
                                WastagePercentage = decimal.Parse(tbxWastage.Text),
                                BookingQtyPerDzn = decimal.Parse(tbxBookingQtyPerDzn.Text),

                                ConfirmedPrice = decimal.Parse(tbxConfirmedPrice.Text),
                                PricePerDzn = decimal.Parse(lblPricePerDznValue.Text),
                                ItemSupplierId = int.Parse(ddlSupplier.SelectedValue),
                                TotalValue = decimal.Parse(lblTotalValueValue.Text),

                                CreateDate = DateTime.Now,
                                CreatedBy = CommonMethods.SessionInfo.UserName
                            };
                            listOrderCostingInfoDetails.Add(ocid);
                        }

                    }


                    foreach (var item in listOrderCostingInfoDetails)
                    {
                        unitOfWork.GenericRepositories<OrderCostingInfoDetails>().Insert(item);
                    }

                    unitOfWork.Save();





                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewEditCostingSheets.aspx');", true);

                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "reloadPage();", true);
                    //Response.Redirect("ViewKnittingMachineSpareReceives.aspx");
                    //SetInitialRowCount();
                    //Server.Transfer("ViewKnittingMachineSpareReceives.aspx");
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }


        }

        protected void CalculatePrices(object sender, EventArgs e)
        {
            TextBox tbxSender = (TextBox)sender;

            Label lblPricePerDznValue = (Label)((RepeaterItem)tbxSender.NamingContainer).FindControl("lblPricePerDznValue");
            Label lblTotalValueValue = (Label)((RepeaterItem)tbxSender.NamingContainer).FindControl("lblTotalValueValue");

            TextBox tbxOrderQty = (TextBox)((RepeaterItem)tbxSender.NamingContainer).FindControl("tbxOrderQty");
            TextBox tbxBookingQtyPerDzn = (TextBox)((RepeaterItem)tbxSender.NamingContainer).FindControl("tbxBookingQtyPerDzn");
            TextBox tbxConfirmedPrice = (TextBox)((RepeaterItem)tbxSender.NamingContainer).FindControl("tbxConfirmedPrice");

            TextBox tbxConsumptionPerDzn = (TextBox)((RepeaterItem)tbxSender.NamingContainer).FindControl("tbxConsumptionPerDzn");
            TextBox tbxWastage = (TextBox)((RepeaterItem)tbxSender.NamingContainer).FindControl("tbxWastage");

            if (tbxConsumptionPerDzn.Text != "" && tbxWastage.Text != "")
            {
                double bookingPerDzn = (double.Parse(tbxConsumptionPerDzn.Text) * (1 + double.Parse(tbxWastage.Text) / 100));
                tbxBookingQtyPerDzn.Text = Math.Round(bookingPerDzn, 2).ToString();
            }

            if (tbxBookingQtyPerDzn.Text != "" && tbxConfirmedPrice.Text != "")
            {
                double perDznValue = (double.Parse(tbxBookingQtyPerDzn.Text) * double.Parse(tbxConfirmedPrice.Text));
                lblPricePerDznValue.Text = Math.Round(perDznValue, 2).ToString();
            }

            if (tbxOrderQty.Text != "" && tbxBookingQtyPerDzn.Text != "" && tbxConfirmedPrice.Text != "")
            {
                double totalRequiremnt = ((double.Parse(tbxOrderQty.Text) / 12) * (double.Parse(tbxBookingQtyPerDzn.Text))) * double.Parse(tbxConfirmedPrice.Text);
                lblTotalValueValue.Text = Math.Round(totalRequiremnt, 2).ToString();
            }

            SetCurrentRowCount();
        }

        private void LoadCostingInfo()
        {
            string sql = $"Exec usp_GetOrderCostingInfoById '{CostingInfoId}'";

            var dtCostingInfo = unitOfWork.GetDataTableFromSql(sql);

            CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, int.Parse((string.IsNullOrEmpty(dtCostingInfo.Rows[0]["BuyerId"].ToString()) ? "0" : dtCostingInfo.Rows[0]["BuyerId"].ToString())));

            CommonMethods.LoadDropdownById(ddlStyles, Convert.ToInt32(dtCostingInfo.Rows[0]["BuyerId"].ToString()), "BuyerStyles", 1, 0);
            ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, int.Parse((string.IsNullOrEmpty(dtCostingInfo.Rows[0]["StyleId"].ToString()) ? "0" : dtCostingInfo.Rows[0]["StyleId"].ToString())));

            CommonMethods.LoadOrderDropdownByStyle(ddlOrders, Convert.ToInt32(dtCostingInfo.Rows[0]["StyleId"].ToString()), 1, 0);
            ddlOrders.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrders, int.Parse((string.IsNullOrEmpty(dtCostingInfo.Rows[0]["OrderId"].ToString()) ? "0" : dtCostingInfo.Rows[0]["OrderId"].ToString())));

            ddlStatus.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStatus, int.Parse((string.IsNullOrEmpty(dtCostingInfo.Rows[0]["Status"].ToString()) ? "0" : dtCostingInfo.Rows[0]["Status"].ToString())));

            CommonMethods.LoadDropdownById(ddlSeason, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerSeasons", 1, 0);
            ddlSeason.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSeason, int.Parse((string.IsNullOrEmpty(dtCostingInfo.Rows[0]["OrderSeasonId"].ToString()) ? "0" : dtCostingInfo.Rows[0]["OrderSeasonId"].ToString())));

            if (EnableDisableStatusDropdown())
            {
                pnlNonEdit.Enabled = true;
            }
            else
            {
                pnlNonEdit.Enabled = false;
            }

            if (int.Parse(dtCostingInfo.Rows[0]["Status"].ToString()) == 1)
            {
                pnlNonEdit.Enabled = false;
            }


            tbxOrderQty.Text = dtCostingInfo.Rows[0]["OrderQty"].ToString();

            if (ddlOrders.SelectedValue != "")
            {
                tbxOrderQty.Enabled = false;
                ddlSeason.Enabled = false;
            }

            tbxLCNo.Text = dtCostingInfo.Rows[0]["LCNumber"].ToString();
            tbxLCValue.Text = dtCostingInfo.Rows[0]["LCValue"].ToString();

            tbxFOBPrice.Text = dtCostingInfo.Rows[0]["FOBPrice"].ToString();

            tbxItem.Text = dtCostingInfo.Rows[0]["ItemDescription"].ToString();

            tbxGarmentsWt.Text = dtCostingInfo.Rows[0]["GarmentsWeight"].ToString();


            tbxDeliveryDate.Text = DateTime.Parse(dtCostingInfo.Rows[0]["DeliveryDate"].ToString()).ToString("yyyy-MM-dd");

            tbxCommissionOrCharge.Text = dtCostingInfo.Rows[0]["CommissionOrCharge"].ToString();

            

            string sql2 = $"Exec usp_GetOrderCostingInfoDetails '{CostingInfoId}'";
            constingInfoDetails = unitOfWork.GetDataTableFromSql(sql2);

            if (constingInfoDetails.Rows.Count > 0)
            {
                rptCostingEntryInfo.DataSource = constingInfoDetails;
                rptCostingEntryInfo.DataBind();
                divCostingSheet.Visible = true;
            }
            else
            {
                divCostingSheet.Visible = false;
            }

            CreateDataTableForViewState();

        }

        private void CreateDataTableForViewState()
        {

            DataTable dt = new DataTable();
            DataRow dr = null;
            //dt.Columns.Add(new DataColumn("ItemTypeId", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));
            dt.Columns.Add(new DataColumn("OrderQty", typeof(string)));
            dt.Columns.Add(new DataColumn("ConsumptionPerDzn", typeof(string)));
            dt.Columns.Add(new DataColumn("Wastage", typeof(string)));

            dt.Columns.Add(new DataColumn("BookingQtyPerDzn", typeof(string)));
            dt.Columns.Add(new DataColumn("ConfirmedPrice", typeof(string)));
            dt.Columns.Add(new DataColumn("PricePerDzn", typeof(string)));
            dt.Columns.Add(new DataColumn("SupplierId", typeof(string)));
            dt.Columns.Add(new DataColumn("TotalValue", typeof(string)));




            for (int rowIndex = 0; rowIndex < constingInfoDetails.Rows.Count; rowIndex++)
            {
                dr = dt.NewRow();

                //dr["ItemTypeId"] = constingInfoDetails.Rows[rowIndex]["CostingItemTypeId"];
                dr["ItemId"] = constingInfoDetails.Rows[rowIndex]["ItemId"];
                dr["OrderQty"] = constingInfoDetails.Rows[rowIndex]["OrderQty"];
                dr["ConsumptionPerDzn"] = constingInfoDetails.Rows[rowIndex]["ConsumptionPerDzn"];
                dr["Wastage"] = constingInfoDetails.Rows[rowIndex]["ConsumptionPerDzn"];
                dr["BookingQtyPerDzn"] = constingInfoDetails.Rows[rowIndex]["BookingQtyPerDzn"];
                dr["ConfirmedPrice"] = constingInfoDetails.Rows[rowIndex]["ConfirmedPrice"];
                dr["PricePerDzn"] = constingInfoDetails.Rows[rowIndex]["PricePerDzn"];
                dr["SupplierId"] = constingInfoDetails.Rows[rowIndex]["ItemSupplierId"];
                dr["TotalValue"] = constingInfoDetails.Rows[rowIndex]["TotalValue"];

                dt.Rows.Add(dr);
            }


            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;


        }

        //protected void ddlItemTypes_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //    DropDownList ddlItemTypes = (DropDownList)sender;
        //    DropDownList ddlItems = (DropDownList)((RepeaterItem)ddlItemTypes.NamingContainer).FindControl("ddlItems");
        //    if (ddlItemTypes.SelectedValue != "")
        //    {
        //        ddlItems.Items.Clear();
        //        CommonMethods.LoadYarnAndAccessoriesItemDropdownByType(ddlItems, int.Parse(ddlItemTypes.SelectedValue), 1, 0);
        //    }
        //    else
        //    {
        //        ddlItems.Items.Clear();
        //    }

        //}



        private bool CheckItems()
        {
            bool hasItem = false;

            for (int rowIndex = 0; rowIndex < rptCostingEntryInfo.Items.Count; rowIndex++)
            {

                DropDownList ddlSupplier = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlSupplier");

                DropDownList ddlItems = (DropDownList)this.rptCostingEntryInfo.Items[rowIndex].FindControl("ddlItems");

                TextBox tbxOrderQty = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxOrderQty");

                TextBox tbxConsumptionPerDzn = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxConsumptionPerDzn");
                TextBox tbxWastage = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxWastage");


                TextBox tbxBookingQtyPerDzn = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxBookingQtyPerDzn");
                TextBox tbxConfirmedPrice = (TextBox)this.rptCostingEntryInfo.Items[rowIndex].FindControl("tbxConfirmedPrice");

                Label lblPricePerDznValue = (Label)this.rptCostingEntryInfo.Items[rowIndex].FindControl("lblPricePerDznValue");
                Label lblTotalValueValue = (Label)this.rptCostingEntryInfo.Items[rowIndex].FindControl("lblTotalValueValue");

                if (ddlSupplier.SelectedIndex != 0 && ddlItems.SelectedIndex != 0 && !string.IsNullOrEmpty(tbxOrderQty.Text) && !string.IsNullOrEmpty(tbxConsumptionPerDzn.Text) && !string.IsNullOrEmpty(tbxWastage.Text) && !string.IsNullOrEmpty(tbxBookingQtyPerDzn.Text) && !string.IsNullOrEmpty(tbxConfirmedPrice.Text))
                {
                    hasItem = true;
                    break;
                }

            }

            return hasItem;
        }


    }
}