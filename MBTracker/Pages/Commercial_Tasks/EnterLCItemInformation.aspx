﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="EnterLCItemInformation.aspx.cs" Inherits="MBTracker.Pages.Commercial_Tasks.EnterLCItemInformation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        input[type="radio"], input[type=checkbox] {
            margin: -2px 0 0;
        }


        .cblClass label {
            padding-left: 2px !important;
            padding-right: 14px !important;
            line-height: 20px !important;
        }
    </style>

    <div class="row-fluid">

        <div class="span9">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="actionTitle" Text="Enter L/C Information:"></asp:Label>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="widget-body">
                        <div class="control-group">
                            <div class="col-md-12">
                                <div class="pull-right" style="text-align: center; font-size: 12px; line-height: 20px; padding-bottom: 5px">
                                    <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                                </div>
                                <div class="control-group">
                                    <div class="col-md-12 col-sm-12">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="lblApplicantLCs" runat="server" Text="LC Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlApplicantLCs" runat="server" CssClass="form-control" Style="min-width: 125px" Width="80%"></asp:DropDownList>
                                    </div>
                                </div>
                                 
                                <%--<div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="lblPINumber" runat="server" Text="L/C Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxLCNumber" runat="server" placeholder="Enter L/C Number" CssClass="form-control" Width="80%"></asp:TextBox>
                                    </div>
                                </div>--%>

                               <%-- <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="Label1" runat="server" Text="L/C Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxLCDate" runat="server" placeholder="Enter L/C date" CssClass="form-control" Width="80%" TextMode="Date"></asp:TextBox>
                                    </div>
                                </div>--%>

                            </div>
                            <div class="col-md-6">
                                
                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="Label1" runat="server" Text="PI Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlApplicantPINotExistInLC" runat="server" CssClass="form-control" Style="min-width: 125px" Width="80%"></asp:DropDownList>
                                    </div>
                                </div>

                                 <%--<div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="Label17" runat="server" Text="PI Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxPINumber" runat="server" placeholder="Enter PI Number" CssClass="form-control" Width="80%"></asp:TextBox>
                                    </div>
                                </div>--%>

                               
                                <!--<div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="Label18" runat="server" Text="Export LC:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlExportLC" runat="server" Display="Dynamic" Width="80%" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>-->
                            </div>
                        </div>

                        <div class="control-group">
                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">

                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="View PI Info" CssClass="btn btn-info pull-center" OnClick="btnSearch_Click" />

                        </label>
                        <div class="controls controls-row">
                        </div>
                    </div>

                    </div>

                    


                </div>
            </div>

        </div>

    </div>

    <br />
    <br />

    <div class="row-fluid">


        <div class="span12" id="divPIAndOtherInfo" runat="server" visible="false">
            <div class="widget">

                <div class="widget-body">

                    <div class="form-horizontal">


                        <div class="control-group">

                            <div class="controls controls-row" style="text-align: center; padding-top: 10px">
                                <asp:Label runat="server" ID="lblLabelMessage" Text="Available PIs for beneficiary you selected:" Visible="false" Font-Bold="true"></asp:Label>
                                
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label3" runat="server" Text="Select PI"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:Repeater ID="rptPISummary" runat="server" OnItemDataBound="rptPISummary_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="Label16" runat="server" Text="Select"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label2" runat="server" Text="PI Number"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label7" runat="server" Text="PI Date"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label10" runat="server" Text="Beneficiary"></asp:Label></th>
                                                    
                                                    <th>
                                                        <asp:Label ID="Label6" runat="server" Text=""></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="vertical-align: middle">
                                                <asp:CheckBox ID="cbxPI" runat="server" AutoPostBack="true"/>
                                                <asp:Label ID="lblPIId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PIId")%>' Visible="false"></asp:Label>
                                            </td>
                                            <td style="vertical-align: middle">
                                                <asp:Label ID="lblPINumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PINumber")%>'></asp:Label></td>
                                            <td style="vertical-align: middle">
                                                <asp:Label ID="lblPIDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "PIDate"))%>'></asp:Label></td>
                                            <td style="vertical-align: middle">
                                                <asp:Label ID="lblBeneficiaryName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BeneficiaryName")%>'></asp:Label></td>
                                            
                                            <td style="vertical-align: middle">

                                                <asp:Repeater ID="rptPIItemInfo" runat="server" OnItemDataBound="rptPIItemInfo_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="data-table22" class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>
                                                                        <asp:Label ID="lblStyles" runat="server" Text="Style"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblOrders" runat="server" Text="PO"></asp:Label></th>
                                                                   
                                                                    <th>
                                                                        <asp:Label ID="lblItems" runat="server" Text="Item Description"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblQty" runat="server" Text="Qty(Lbs)"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblAmount" runat="server" Text="Amount(US$)"></asp:Label></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblStylesValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "StyleName")%>'></asp:Label>
                                                            </td>
                                                            <td style="width: 15%">
                                                                <asp:Label ID="lblPOValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderNumber")%>'></asp:Label>
                                                            </td>
                                                            
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblItemValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ItemName")%>'></asp:Label>
                                                            </td>
                                                            <td style="width: 10%; padding-top: 18px">
                                                                <asp:Label ID="lblQtyValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity")%>'></asp:Label>
                                                            </td>
                                                            <td style="width: 10%; padding-top: 18px">
                                                                <asp:Label ID="lblUnitPriceValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UnitPrice")%>'></asp:Label>
                                                            </td>
                                                            <td style="width: 10%; padding-top: 18px">
                                                                <asp:Label ID="lblTotalAmount" Width="100%" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TotalAmount")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>


                                                        <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                            <asp:Label ID="lblTotal" runat="server" Text="Total:" Font-Bold="true"></asp:Label></td>
                                                        
                                                        <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                        <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                        
                                                        <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                            <asp:Label ID="lblTotalQuantity" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                                                        <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                        <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                            <asp:Label ID="lblGrandTotalAmount" runat="server" Text="" Font-Bold="true"></asp:Label></td>


                                                        </tbody>
                                 </table>
                                           
                                                    </FooterTemplate>
                                                </asp:Repeater>

                                                <div style="text-align: center">
                                                    <asp:Label ID="lblNoItemFound" runat="server" Visible="false" Text="No Item found." BackColor="#ffff00"></asp:Label>
                                                </div>

                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                         </table>
                            <div class="clearfix">
                                    </FooterTemplate>
                                </asp:Repeater>

                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label4" runat="server" Text=""></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                            <div class="controls controls-row" style="text-align: right; padding-top: 1px">
                                <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>

                            </div>
                        </div>

                    </div>

                </div>


            </div>

        </div>



        <!--LC Wise PI List start ----->

        <div class="row-fluid" runat="server">
        <div class="span12">

            <div class="span9">
                <div class="widget" id="dataDiv" runat="server" visible="false">
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">

                            <div class="control-group">
                                <label for="inputOrder" class="control-label" style="padding-bottom: 12px">
                                    <asp:Label runat="server" ID="Label5" Text="LC Information:" Font-Bold="true" Visible="false"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rptLCSummary" runat="server">
                                        <HeaderTemplate>
                                            <table id="data-table1" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="Label2" runat="server" Text="L/C Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label7" runat="server" Text="L/C Date"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label4" runat="server" Text="L/C Value"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label6" runat="server" Text="PI Number(s)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Style(s)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label9" runat="server" Text="Applicant Bank"></asp:Label></th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: left">
                                                    <asp:Label ID="lblLCNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCNumber")%>'></asp:Label>
                                                    <asp:Label ID="lblStatusValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "StatusName")%>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblLCTypeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCType")%>' Visible="false"></asp:Label>
                                                     <asp:Label ID="lblLCPurposeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCPurpose")%>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblCommercialConceredValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CommercialConceredName")%>' Visible="false"></asp:Label>

                                                    <asp:Label ID="lblShipmentMode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShipmentModeInfo")%>' Visible="false"></asp:Label>


                                                    <asp:Label ID="lblUDDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "UDDate"))%>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblUDStatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "[UDStatus]")%>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblETDDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "ETDDate"))%>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblETADate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "ETADate"))%>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblMaturityDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "MaturityDate"))%>' Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblLCDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "LCDate"))%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblLCAmount" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCAmount")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblLCPIs" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCPIs")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblLCStyles" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCStyles")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblAdvisingBank" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BankName")%>'></asp:Label>
                                                    <asp:Label ID="lblOtherInfo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OtherInfo")%>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblExportLCNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ExportLCNumber")%>' Visible="false"></asp:Label>

                                                </td>
                                                <%--<td>
                                                    <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" Visible='<%#Eval("CanEdit")%>' CommandArgument='<%#Eval("LCId")%>' class="btn btn-success btn-mini hidden-phone" Text="Edit"></asp:LinkButton>

                                                    <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="ViewDetails" CommandArgument='<%#Eval("LCId")%>' class="btn btn-success btn-mini hidden-phone" Text="View Details"></asp:LinkButton>
                                                </td>--%>

                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                         </table>
                            <div class="clearfix">
                                        </FooterTemplate>
                                    </asp:Repeater>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="span6">
            </div>
        </div>
    </div>

        <!---LC Wise PI List End ----->


        <div style="text-align: left">
            <asp:Label ID="lblNoPIFound" runat="server" Visible="false" Text="No PI found." BackColor="#ffff00"></asp:Label>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
