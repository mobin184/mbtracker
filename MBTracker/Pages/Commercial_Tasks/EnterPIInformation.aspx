﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="EnterPIInformation.aspx.cs" Inherits="MBTracker.Pages.Commercial_Tasks.EnterPIInformation" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <style>
        input[type="radio"], input[type=checkbox] {
            margin: -2px 0 0;
        }


        .cblClass label {
            /*display: block;*/
            padding-left: 2px !important;
            padding-right: 14px !important;
        }
    </style>

    <div class="row-fluid">

        <div class="span9">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="actionTitle" Text="Enter PI Information:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="pull-right" style="text-align: center; font-size: 12px; line-height: 20px; padding-bottom: 5px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="40%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group" id="divStyles" runat="server" visible="false">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblStyle" runat="server" Text="Select Style(s):"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <div class="form-inline cblClass">
                                    <asp:CheckBoxList ID="cblStyles" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="4" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                    <asp:Label ID="lblNoStyleFound" runat="server" Visible="false" Text="No Style found." BackColor="#ffff00"></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="Load Costing Sheets" CssClass="btn btn-info pull-left" OnClick="btnLoadCostingSheets_Click" />

                                </div>
                            </div>
                        </div>
                        <br />
                        <br />

                        <div class="control-group" id="divCostingSheet" runat="server" visible="false">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblOrder" runat="server" Text="Costing Sheet(s):"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                            <div class="controls controls-row">
                                <asp:Repeater ID="rptCostings" runat="server">
                                    <HeaderTemplate>
                                        <table id="data-table1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="Label16" runat="server" Text="Select"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label2" runat="server" Text="Order Number"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label7" runat="server" Text="Order Season"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label5" runat="server" Text="Order Qty"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label6" runat="server" Text="Delivery Date"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label4" runat="server" Text="LC Number"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label1" runat="server" Text="LC Value"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label3" runat="server" Text="FOB Price"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label9" runat="server" Text="Garments Weight"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="cbxCostingSheet" runat="server" />
                                                <asp:Label ID="lblCostingInfoId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CostingInfoId")%>' Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblOrderNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderNumber")%>'></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblOrderSeason" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderSeason")%>'></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblOrderQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderQty")%>'></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblDeliveryDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "DeliveryDate"))%>'></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblLCNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCNumber")%>'></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblLCValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCValue")%>'></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblFOBPrice" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FOBPrice")%>'></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblGarmentsWt" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "GarmentsWeight")%>'></asp:Label></td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                         </table>
                            <div class="clearfix">
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>

                        <div style="text-align: center">
                            <asp:Label ID="lblNoCostingSheetFound" runat="server" Visible="false" Text="No Costing Sheet found." BackColor="#ffff00"></asp:Label>
                        </div>

                        <br />
                        <br />
                        <div id="divPIOtherInfo" runat="server" visible="false">

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblPINumber" runat="server" Text="PI Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxPINumber" runat="server" placeholder="Enter PI Number" CssClass="form-control" Width="40%"></asp:TextBox>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="Label1" runat="server" Text="PI Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxPIDate" runat="server" placeholder="Enter PI date" CssClass="form-control" Width="40%" TextMode="Date"></asp:TextBox>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="controls controls-row" style="text-align: right">
                                    <span style="font-weight: 700; color: #CC0000">*</span>Row will not be saved if all the fields don't have values.
                                </div>
                            </div>


                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblPIEntry" runat="server" Text="Item Info:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row" style="overflow-x: auto">
                                    <asp:Repeater ID="rptPIItemEntryInfo" runat="server" OnItemDataBound="rptPIItemEntryInfo_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblDescription" runat="server" Text="Description of Goods"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblHSCode" runat="server" Text="HS Code"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblQty" runat="server" Text="Quantity(Lbs)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price(US$/Lbs)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblAmount" runat="server" Text="Amount(US$)"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="width: 40%">
                                                    <asp:TextBox ID="tbxDescription" Width="100%" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                                <td style="width: 14%; padding-top: 18px">
                                                    <asp:TextBox ID="tbxHSCode" Width="100%" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 14%; padding-top: 18px">
                                                    <asp:TextBox ID="tbxQty" Width="100%" runat="server" TextMode="Number" AutoPostBack="true" OnTextChanged="CalculatePrices"></asp:TextBox>
                                                </td>
                                                <td style="width: 18%; padding-top: 18px">
                                                    <asp:TextBox ID="tbxUnitPrice" Width="100%" runat="server" TextMode="Number" AutoPostBack="true" OnTextChanged="CalculatePrices"></asp:TextBox>
                                                </td>
                                                <td style="width: 14%; padding-top: 18px">
                                                    <asp:Label ID="lblTotalAmount" Width="100%" runat="server" Text=""></asp:Label>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>

                                            <td style="font-weight:500; text-align:left" >Total:</td>
                                             <td></td>
                                             <td> <asp:Label ID="lblTotalQuantity" runat="server" Text=""></asp:Label></td>
                                             <td></td>
                                             <td> <asp:Label ID="lblGrandTotalAmount" runat="server" Text=""></asp:Label></td>

                                            </tbody>
                                 </table>
                                            <div class="pull-right" style="margin-right: 0px">
                                                <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="81px" Text="Add Row" OnClick="btnAddRow_Click" />
                                            </div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>

                            <br />
                            <br />

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblPaymentMode" runat="server" Text="Payment Mode:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxPaymentMode" runat="server" placeholder="Enter payment mode info" CssClass="form-control" Width="40%" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblShipment" runat="server" Text="Shipment Info:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxShipment" runat="server" placeholder="Enter shipment info" CssClass="form-control" Width="40%"></asp:TextBox>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblPartialShipment" runat="server" Text="Partial Shipment:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlPartialShipment" runat="server" CssClass="form-control" Width="40%">
                                        <asp:ListItem Text="Yes" Value="1" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblTolarance" runat="server" Text="Tolerance Info:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxTolerance" runat="server" placeholder="Enter tolerance info" CssClass="form-control" Width="40%"></asp:TextBox>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblBeneficiaryName" runat="server" Text="Beneficiary:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                   <%-- <asp:TextBox ID="tbxBeneficiaryName" runat="server" placeholder="Enter beneficiary name" CssClass="form-control" Width="40%"></asp:TextBox>--%>
                                     <asp:DropDownList ID="ddlYarnAndAccssSuppliers" runat="server" Display="Dynamic" Width="40%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblBeneficiaryPhone" runat="server" Text="Beneficiary Phone:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxBeneficiaryPhone" runat="server" placeholder="Enter beneficiary phone" CssClass="form-control" Width="40%"></asp:TextBox>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblBeneficiaryBank" runat="server" Text="Beneficiary Bank:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxBeneficiaryBankInfo" runat="server" placeholder="Enter beneficiary bank info" CssClass="form-control" Width="40%" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblOthers" runat="server" Text="Other Info:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxOtherInfo" runat="server" placeholder="Enter other info" CssClass="form-control" Width="40%" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>

                            <br />
                            <br />
                            <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>

                        </div>

                        <div class="controls controls-row">
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
