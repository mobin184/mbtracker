﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.DAL;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Commercial_Tasks
{
    public partial class ViewPIInfoforAdmin : System.Web.UI.Page
    {
        DatabaseManager dm = new DatabaseManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        decimal totalGoods = 0;
        decimal grandTotalAmount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                EnableDisableEditButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                CommonMethods.PIStatusDropdownOnlyOpenandapprove(ddlStatus, 1, 0);

            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    
                }
            }


        }

        private void EnableDisableEditButton()
        {
            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Merchandiser,QCE");
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewEditPIInformationV2", 1, 1);
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewEditPIInformationV2", 2, 1);
        }

        int PIId
        {
            set { ViewState["PIId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["PIId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            dataDiv.Visible = false;
            pnlDetails.Visible = false;

            ddlStyles.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {


            pnlDetails.Visible = false;
            dataDiv.Visible = false;


            try
            {

                string sql = "";
                DataTable dt;

                if (tbxPINumber.Text == "" && ddlBuyers.SelectedValue == "" && ddlStyles.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter search criteria.')", true);
                }
                else if (tbxPINumber.Text != "")
                {
                    sql = $"Exec usp_GetPIInfoByPINumberForAdmin '{tbxPINumber.Text}'";
                    dt = unitOfWork.GetDataTableFromSql(sql);

                    if (dt.Rows.Count > 0)
                    {
                        rptPISummary.Visible = true;
                        dataDiv.Visible = true;

                        dt = dt.CheckEditDeletePermission(CommonMethods.SessionInfo.RoleId, "ViewPIInfoforAdmin");
                        rptPISummary.DataSource = dt;
                        rptPISummary.DataBind();
                        lblNoDataFound.Visible = false;
                        lblLabelMessage.Visible = true;

                        CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                        ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, int.Parse((string.IsNullOrEmpty(dt.Rows[0]["BuyerId"].ToString()) ? "0" : dt.Rows[0]["BuyerId"].ToString())));

                        CommonMethods.LoadDropdownById(ddlStyles, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
                        ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, int.Parse((string.IsNullOrEmpty(dt.Rows[0]["StyleId"].ToString()) ? "0" : dt.Rows[0]["StyleId"].ToString())));

                    }
                    else
                    {
                        rptPISummary.Visible = false;
                        dataDiv.Visible = false;

                        lblNoDataFound.Visible = true;
                        lblLabelMessage.Visible = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }


        }


        protected void rptPISummary_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "ViewDetails")
            {
                pnlDetails.Visible = true;


                Label lblPINumber = (Label)e.Item.FindControl("lblPINumber");
                Label lblPIDate = (Label)e.Item.FindControl("lblPIDate");
                Label lblBeneficiary = (Label)e.Item.FindControl("lblBeneficiary");
                Label lblItemBuyer = (Label)e.Item.FindControl("lblItemBuyer");

                Label lblMerchandiser = (Label)e.Item.FindControl("lblMerchandiser");
                Label lblMerchandiserLeader = (Label)e.Item.FindControl("lblMerchandiserLeader");

                Label lblOrderSeason = (Label)e.Item.FindControl("lblOrderSeason");
                Label lblSeasonYear = (Label)e.Item.FindControl("lblSeasonYear");


                lblPINumberValue.Text = lblPINumber.Text;
                lblPIDateValue.Text = lblPIDate.Text;
                lblOrderSeasonValue.Text = lblOrderSeason.Text;
                lblSeasonYearValue.Text = lblSeasonYear.Text;


                //lblBeneficiaryValue.Text = lblBeneficiary.Text;
                //lblItemBuyerValue.Text = lblItemBuyer.Text;


                string PIId = e.CommandArgument.ToString();

                string sql = $"Exec usp_GetPIInfoDetailsById '{PIId}'";

                var dt = unitOfWork.GetDataTableFromSql(sql);

                if (dt.Rows.Count > 0)
                {
                    lblPaymentModeValue.Text = dt.Rows[0]["PaymentMode"].ToString();
                    lblShipmentInfoValue.Text = dt.Rows[0]["ShipmentInfo"].ToString();
                    lblPartialShipmentValue.Text = dt.Rows[0]["PartialShipment"].ToString();
                    lblTolaranceValue.Text = dt.Rows[0]["TolaranceInfo"].ToString();
                    lblBeneficiaryNameValue.Text = dt.Rows[0]["BeneficiaryName"].ToString();

                    lblItemOrderDate.Text = dt.Rows[0]["ItemOrderMonthYear"].ToString();


                    lblBeneficiaryPhoneValue.Text = dt.Rows[0]["BeneficiaryPhone"].ToString();
                    lblBeneficialyBankInfo.Text = dt.Rows[0]["BeneficiaryBankInfo"].ToString();
                    lblOthersValue.Text = dt.Rows[0]["Remarks"].ToString();

                    lblStatus.Text = dt.Rows[0]["PIStatus"].ToString();

                }



                string sql2 = $"Exec usp_GetPIItemsByPIId '{PIId}'";
                var dt2 = unitOfWork.GetDataTableFromSql(sql2);

                if (dt2.Rows.Count > 0)
                {
                    rptPIItemEntryInfo.Visible = true;

                    rptPIItemEntryInfo.DataSource = dt2;
                    rptPIItemEntryInfo.DataBind();

                }
                else
                {
                    rptPIItemEntryInfo.Visible = false;

                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);

            }
            else if (e.CommandName == "Edit")
            {
                string PIId = e.CommandArgument.ToString();
                Response.Redirect("EnterPIInformationV2.aspx?PIId=" + Tools.UrlEncode(PIId));
            }
            else if (e.CommandName == "Delete")
            {
                //string PIId = e.CommandArgument.ToString();
                PIId = Convert.ToInt32(e.CommandArgument.ToString());
                //var PICannotBeDeleted = unitOfWork.GenericRepositories<viewSeasonsCanNotBeDeleted>().GetByID(PIId);

                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);

            }

        }

        protected void rptPIItemEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                var qty = DataBinder.Eval(e.Item.DataItem, "Quantity").ToString();
                var totalAmount = DataBinder.Eval(e.Item.DataItem, "TotalAmount").ToString();

                totalGoods += decimal.Parse(qty);
                grandTotalAmount += decimal.Parse(totalAmount);


            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {

                Label lblTotalQuantity = (Label)e.Item.FindControl("lblTotalQuantity");
                Label lblGrandTotalAmount = (Label)e.Item.FindControl("lblGrandTotalAmount");


                lblTotalQuantity.Text = totalGoods.ToString();
                lblGrandTotalAmount.Text = grandTotalAmount.ToString();

            }

        }


    }
}