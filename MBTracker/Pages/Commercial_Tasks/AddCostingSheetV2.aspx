﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddCostingSheetV2.aspx.cs" Inherits="MBTracker.Pages.Commercial_Tasks.AddCostingSheetV2" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <style>
        td > label {
            line-height: 13px
        }

        /*.table-bordered th, .table-bordered td {
            border-left: 1px solid #e0e0e0 !important;
            border-right: 1px solid #e0e0e0 !important;
            border-top: 1px solid #e0e0e0 !important;
            border-bottom: 1px solid #e0e0e0 !important;
        }*/


        .cblClass label {
            /*display: block;*/
            padding-left: 2px !important;
            padding-right: 14px !important;
        }
    </style>

    <asp:Panel ID="pnlNonEdit" runat="server">

    <div class="row-fluid">

        <div class="span9" id="divSearchCriteria" runat="server" >
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="actionTitle" Text="Enter Costing Information:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="pull-right" style="text-align: center; font-size: 12px; line-height: 20px; padding-bottom: 5px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="control-group">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="lblOrder" runat="server" Text="Select Order:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="true" Display="Dynamic" Enabled="false" Width="100%" OnSelectedIndexChanged="ddlOrders_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlOrders"><span style="font-weight: 700; color: #CC0000">Please select an order.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblOrderQty" runat="server" Text="Order Quantity:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxOrderQty" runat="server" placeholder="Enter order quantity" CssClass="form-control" Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="tbxOrderQty"><span style="font-weight: 700; color: #CC0000">Enter order quantity.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblOrderSeason" runat="server" Text="Order Season:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    
                                    <asp:DropDownList ID="ddlSeason" runat="server" Display="Dynamic" CssClass="form-control"></asp:DropDownList>
                                    <%--<asp:TextBox ID="tbxOrderSeason" runat="server" placeholder="Enter order season" CssClass="form-control" Width="100%"></asp:TextBox>--%>

                                   <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator9" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="tbxOrderSeason"><span style="font-weight: 700; color: #CC0000">Enter order season.</span></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblFOBPrice" runat="server" Text="FOB Price:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxFOBPrice" runat="server" placeholder="Enter FOB Price" CssClass="form-control" Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="tbxFOBPrice"><span style="font-weight: 700; color: #CC0000">Enter FOB Price.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>

                             

                        </div>
                        <div class="col-md-6">

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="Label1" runat="server" Text="Ex Factory Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxDeliveryDate" runat="server" placeholder="Enter Ex Factory Date" CssClass="form-control" Width="100%" TextMode="Date"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="tbxDeliveryDate"><span style="font-weight: 700; color: #CC0000">Enter delivery date.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="Label4" runat="server" Text="Contact L/C No.:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxLCNo" runat="server" placeholder="Enter L/C No." CssClass="form-control" Width="100%"></asp:TextBox>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="Label2" runat="server" Text="L/C Value:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxLCValue" runat="server" placeholder="Enter L/C Value" CssClass="form-control" Width="100%"></asp:TextBox>
                                </div>
                            </div>



                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="Label3" runat="server" Text="Product Description:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxItem" runat="server" placeholder="Product Description" CssClass="form-control" Width="100%"></asp:TextBox>
                                </div>
                            </div>


                            <div class="control-group" style="display:none;">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="Label6" runat="server" Text="Garments Weight:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxGarmentsWt" runat="server" placeholder="Enter garments weight" CssClass="form-control" Width="100%"></asp:TextBox>
                                </div>
                            </div>


                             <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="Label5" runat="server" Text="Commission/Charge(%):"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxCommissionOrCharge" runat="server" placeholder="Commission or Charge" CssClass="form-control" Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="tbxFOBPrice"><span style="font-weight: 700; color: #CC0000">Enter commission charge.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="control-group">
                                        <label for="inputEmail3" class="control-label">
                                            <asp:Label ID="lblStatus" runat="server" Text="Status:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control" Width="100%">
                                                <asp:ListItem Text="Open" Value="0" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Approved" Value="1"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                        </div>

                        <div class="controls controls-row">
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="row-fluid">
        <div class="span12" id="divCostingItems" runat="server" visible="false">

            <div class="widget">
                <div class="widget-body">
                    <div class="row-fluid" runat="server">

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label" style="padding-top: 1px; text-align: left">
                                <asp:Label ID="lblListOfCostingItems" runat="server" Text="Select Costing Items:" Font-Bold="true"></asp:Label></label>
                        </div>

                        <div class="form-inline cblClass">
                            <asp:CheckBoxList ID="cblCostingItems" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="3" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                        </div>

                        <div style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 15px">

                            <div class="controls controls-row" style="padding-top: 20px">
                                <asp:Button ID="btnSetupCostingSheet" runat="server" class="btn btn-add btn-small btnStyle pull-left"  Text="Setup Costing Sheet" OnClick="btnSetupCostingSheet_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>




    <div class="row-fluid">
        <div class="span12" id="divCostingSheet" runat="server" visible="false">

            <div class="widget">
                <div class="widget-body">
                    <div class="row-fluid" runat="server">

                        <div style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 15px">
                            <div class="pull-right" style="padding-bottom: 10px">
                                <span style="font-weight: 700;"></span><span style="font-weight: 700; color: #CC0000">*</span>Row will not be saved if all the fields don't have values.
                            </div>
                        </div>

                        <div id="dt_example" class="example_alt_pagination">

                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblKnittingEntry" runat="server" Text="Items & Costing Information:" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                    <asp:Repeater ID="rptCostingEntryInfo" runat="server" OnItemDataBound="rptCostingEntryInfo_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>

<%--                                                        <th>
                                                            <asp:Label ID="lblItemType" runat="server" Text="Item<br/> Type"></asp:Label></th>--%>
                                                        <th>
                                                            <asp:Label ID="lblDescription" runat="server" Text="Item<br/>Description"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblOrderQty" runat="server" Text="Order Qty<br/> (in Pcs)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblConsumption" runat="server" Text="Consumption<br/>(Per Dzn)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblWastage" runat="server" Text="Wastage<br/>(%)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblBookingQty" runat="server" Text="Booking Qty<br/>(Per Dzn)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblConfirmedPrice" runat="server" Text="Confirmed <br/> Price($)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblPricePerDzn" runat="server" Text="Price<br/>(Per Dzn)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblSupplier" runat="server" Text="Item<br/>Supplier"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblTotalValue" runat="server" Text="Total Value<br/>($)"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                               <%-- <td style="width: 10%">
                                                    <asp:DropDownList ID="ddlItemType" runat="server" CssClass="form-control" Style="min-width: 125px" AutoPostBack="true" OnSelectedIndexChanged="ddlItemTypes_SelectedIndexChanged"></asp:DropDownList>
                                                    <%--<asp:Label ID="lblCostingInfoDetailId" runat="server" Text="" Visible="false"></asp:Label>
                                                </td>--%>
                                                <td style="width: 20%">
                                                    <%--<asp:TextBox ID="tbxDescription" Width="100%" runat="server" TextMode="MultiLine"></asp:TextBox>--%>
                                                    <asp:DropDownList ID="ddlItems" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                </td>
                                                <td style="width: 10%">
                                                    <asp:TextBox ID="tbxOrderQty" Width="100%" runat="server" AutoPostBack="true" OnTextChanged="CalculatePrices" TextMode="Number"></asp:TextBox>
                                                </td>
                                                <td style="width: 10%">
                                                    <asp:TextBox ID="tbxConsumptionPerDzn" Width="100%" runat="server" AutoPostBack="true" OnTextChanged="CalculatePrices" TextMode="Number"></asp:TextBox>
                                                </td>
                                                <td style="width: 8%">
                                                    <asp:TextBox ID="tbxWastage" Width="100%" runat="server" AutoPostBack="true" OnTextChanged="CalculatePrices" TextMode="Number"></asp:TextBox>
                                                </td>
                                                <td style="width: 10%">
                                                    <asp:TextBox ID="tbxBookingQtyPerDzn" Width="100%" runat="server" AutoPostBack="true" OnTextChanged="CalculatePrices" TextMode="Number"></asp:TextBox>
                                                </td>
                                                <td style="width: 10%">
                                                    <asp:TextBox ID="tbxConfirmedPrice" Width="100%" runat="server" AutoPostBack="true" OnTextChanged="CalculatePrices" TextMode="Number"></asp:TextBox>
                                                </td>
                                                <td style="width: 8%">
                                                    <asp:Label ID="lblPricePerDznValue" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td style="width: 16%">
                                                    <asp:DropDownList ID="ddlSupplier" runat="server" CssClass="form-control" Style="min-width: 125px"></asp:DropDownList>
                                                </td>
                                                <td style="width: 8%">
                                                    <asp:Label ID="lblTotalValueValue" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>

                                            <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                <asp:Label ID="lblTotal" runat="server" Text="Total:" Font-Bold="true"></asp:Label></td>

                                           <%-- <td style="font-weight: 500; text-align: left; background-color: lavender"></td>--%>
                                            <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                            <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                            <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                            <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                            <td style="font-weight: 500; text-align: left; background-color: lavender"></td>

                                            <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                <asp:Label ID="lblTotalPricePerDzn" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                                            <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                            <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                <asp:Label ID="lblGrandTotalValue" runat="server" Text="" Font-Bold="true"></asp:Label></td>


                                            </tbody>
                                 </table>
                                            <div class="pull-right" style="margin-right: 0px">
                                                <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="81px" Text="Add Row" OnClick="btnAddRow_Click" />
                                            </div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>


                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>

            <br />
            <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
            <asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>


        </div>

    </div>

    </asp:Panel>


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
