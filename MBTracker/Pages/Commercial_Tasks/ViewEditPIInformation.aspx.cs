﻿using MBTracker.Code_Folder;
using Repositories;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace MBTracker.Pages.Commercial_Tasks
{
    public partial class ViewEditPIInformation : System.Web.UI.Page
    {

        UnitOfWork unitOfWork = new UnitOfWork();
        int totalGoods = 0;
        decimal grandTotalAmount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);

            }

        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);

                //ddlOrders.Items.Clear();
            }
            else
            {
                ddlStyles.Items.Clear();
                //ddlOrders.Items.Clear();

            }
        }

       


        protected void btnSearch_Click(object sender, EventArgs e)
        {
           

            if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select an style.')", true);
            }
            else
            {

                try
                {

                    int styleId = 0;
                    styleId = int.Parse(ddlStyles.SelectedValue);

                    lblLabelMessage.Text = "PI info:";


                    string sql = $"Exec usp_GetPIInfoByStyleIds '{styleId}'";
                    var dt = unitOfWork.GetDataTableFromSql(sql);

                    if (dt.Rows.Count > 0)
                    {
                        rptPISummary.Visible = true;
                        dataDiv.Visible = true;

                        rptPISummary.DataSource = dt;
                        rptPISummary.DataBind();
                        lblNoDataFound.Visible = false;
                        lblLabelMessage.Visible = true;
                    }
                    else
                    {
                        rptPISummary.Visible = false;
                        dataDiv.Visible = false;

                        lblNoDataFound.Visible = true;
                        lblLabelMessage.Visible = false;
                    }

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }

            }
        }


        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "ViewDetails")
            {
                pnlDetails.Visible = true;


                Label lblPINumber = (Label)e.Item.FindControl("lblPINumber");
                Label lblPIDate = (Label)e.Item.FindControl("lblPIDate");
                Label lblPIStyles = (Label)e.Item.FindControl("lblPIStyles");
                Label lblPIOrders = (Label)e.Item.FindControl("lblPIOrders");

                lblPINumberValue.Text = lblPINumber.Text;
                lblPIDateValue.Text = lblPIDate.Text;
                lblStylesInPIValue.Text = lblPIStyles.Text;
                lblPOsInPIValue.Text = lblPIOrders.Text;


                string PIId = e.CommandArgument.ToString();

                string sql = $"Exec usp_GetPIInfoDetailsById '{PIId}'";

                var dt = unitOfWork.GetDataTableFromSql(sql);

                if (dt.Rows.Count > 0)
                {
                    lblPaymentModeValue.Text = dt.Rows[0]["PaymentMode"].ToString();
                    lblShipmentInfoValue.Text = dt.Rows[0]["ShipmentInfo"].ToString();
                    lblPartialShipmentValue.Text = dt.Rows[0]["PartialShipment"].ToString();
                    lblTolaranceValue.Text = dt.Rows[0]["TolaranceInfo"].ToString();
                    lblBeneficiaryNameValue.Text = dt.Rows[0]["BeneficiaryName"].ToString();

                    lblBeneficiaryPhoneValue.Text = dt.Rows[0]["BeneficiaryPhone"].ToString();
                    lblBeneficialyBankInfo.Text = dt.Rows[0]["BeneficiaryBankInfo"].ToString();
                    lblOthersValue.Text = dt.Rows[0]["OtherInfo"].ToString();
                }



                string sql2 = $"Exec usp_GetPIItems '{PIId}'";
                var dt2 = unitOfWork.GetDataTableFromSql(sql2);

                if (dt2.Rows.Count > 0)
                {
                    rptPIItemEntryInfo.Visible = true;

                    rptPIItemEntryInfo.DataSource = dt2;
                    rptPIItemEntryInfo.DataBind();

                }
                else
                {
                    rptPIItemEntryInfo.Visible = false;

                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);

            }
            else if (e.CommandName == "Edit")
            {
                string PIId = e.CommandArgument.ToString();
                Response.Redirect("EnterPIInformation.aspx?PIId=" + Tools.UrlEncode(PIId));
            }

        }

        protected void rptPIItemEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                    var qty = DataBinder.Eval(e.Item.DataItem, "Quantity").ToString();
                    var totalAmount = DataBinder.Eval(e.Item.DataItem, "TotalAmount").ToString();

                    totalGoods += int.Parse(qty);
                    grandTotalAmount += decimal.Parse(totalAmount);
                

            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {

                    Label lblTotalQuantity = (Label)e.Item.FindControl("lblTotalQuantity");
                    Label lblGrandTotalAmount = (Label)e.Item.FindControl("lblGrandTotalAmount");


                    lblTotalQuantity.Text = totalGoods.ToString();
                    lblGrandTotalAmount.Text = grandTotalAmount.ToString();
       
            }

        }

    }
}