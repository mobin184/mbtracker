﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.DAL;
using MBTracker.EF;
using Repositories;
using System;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Commercial_Tasks
{
    public partial class ViewEditLCInformation : System.Web.UI.Page
    {
        DatabaseManager dm = new DatabaseManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        decimal totalGoods = 0;
        decimal grandTotalAmount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                EnableDisableEditButton();
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);

            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteLCInfo(LCId);
                }
            }

        }

        private void EnableDisableEditButton()
        {
            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Merchandiser,QCE");
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewEditLCInformation", 1, 1);
            ViewState["deleteEnabled"] = CommonMethods.CurrentUserRollIn2("ViewEditLCInformation", 3, 1);
        }

        private bool EnableDisableLCPurpose()
        {


            var isExist = false;

            var pageName = "LCPurposeSection";
            var controlType = 4;
            var buttonNumberOnpage = 1;

            string sql = $"Exec usp_GetRoleIdsForEditDeleteButton '{pageName}', {controlType}, {buttonNumberOnpage}";

            UnitOfWork unitOfWork = new UnitOfWork();
            var dt = unitOfWork.GetDataTableFromSql(sql);
            var currentUserRoleId = CommonMethods.SessionInfo.RoleId;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (int.Parse(dt.Rows[i][0].ToString()) == currentUserRoleId)
                {
                    isExist = true;
                    break;
                }
            }

            return isExist;
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            pnlDetails.Visible = false;
            dataDiv.Visible = false;

            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);

            }
            else
            {
                ddlStyles.Items.Clear();
                ddlPIs.Items.Clear();

            }
        }


        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlDetails.Visible = false;
            dataDiv.Visible = false;

            if (ddlStyles.SelectedValue != "")
            {
                CommonMethods.LoadPIDropdownByStyle(ddlPIs, Convert.ToInt32(ddlStyles.SelectedValue), 1, 0);
            }
            else
            {
                ddlPIs.Items.Clear();

            }
        }

        int LCId
        {
            set { ViewState["LCId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["LCId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }
        private void DeleteLCInfo(int LCId)
        {

            int returnValue = DeleteLCInfoById(LCId, CommonMethods.SessionInfo.UserName, DateTime.Now);

            if (returnValue > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('LC deletion was failed.')", true);
            }
            ClientScript.RegisterClientScriptBlock(this.GetType(), "refresh", "setTimeout('window.location.href=window.location.href', 3000);", true);


        }

        public int DeleteLCInfoById(int LCId, string actionTaker, DateTime currentTime)
        {
            var obj = unitOfWork.GenericRepositories<LCInfoBackToBack>().GetByID(LCId);
            dm.AddParameteres("@LCId", LCId);
            DataTable dt = dm.ExecuteQuery("usp_Delete_LCInfoBackToBackAndLCPIs");
            //for audit log
            unitOfWork.SaveAuditLog("Deleted", LCId + "", "LCInfoBackToBack", obj);
            unitOfWork.Save();
            return Convert.ToInt32(dt.Rows[0][0]);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {



            pnlDetails.Visible = false;
            dataDiv.Visible = false;

            try
            {

                string sql = "";
                DataTable dt;

                if (tbxLCNumber.Text == "" && ddlBuyers.SelectedValue == "" && ddlStyles.SelectedValue == "" && ddlPIs.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter Search Criteria.')", true);
                }
                else if (tbxLCNumber.Text != "")
                {
                    sql = $"Exec usp_GetLCInfoByLCNumber '{tbxLCNumber.Text}'";
                    dt = unitOfWork.GetDataTableFromSql(sql);
                    if (dt.Rows.Count > 0)
                    {
                        rptLCSummary.Visible = true;
                        dataDiv.Visible = true;
                        dt = dt.CheckEditDeletePermission(CommonMethods.SessionInfo.RoleId, "ViewEditLCInformation");
                        rptLCSummary.DataSource = dt;
                        rptLCSummary.DataBind();
                        lblNoDataFound.Visible = false;
                        lblLabelMessage.Visible = true;
                    }
                    else
                    {
                        rptLCSummary.Visible = false;
                        dataDiv.Visible = false;

                        lblNoDataFound.Visible = true;
                        lblLabelMessage.Visible = false;
                    }
                }
                //else if (ddlPIs.SelectedValue == "")
                //{
                //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter search criteria.')", true);

                //}
                else
                {

                    int buyerId = 0;
                    if (ddlBuyers.SelectedValue != "")
                    {
                        buyerId = int.Parse(ddlBuyers.SelectedValue);
                    }

                    int styleId = 0;
                    if (ddlStyles.SelectedValue != "")
                    {
                        styleId = int.Parse(ddlStyles.SelectedValue);
                    }

                    int piId = 0;
                    if (ddlPIs.SelectedValue != "")
                    {
                        piId = int.Parse(ddlPIs.SelectedValue);
                    }


                    sql = $"Exec usp_GetLCInfoByBuyerOrStyleOrPIId {buyerId},{styleId},{piId} ";


                    dt = unitOfWork.GetDataTableFromSql(sql);
                    if (dt.Rows.Count > 0)
                    {
                        rptLCSummary.Visible = true;
                        dataDiv.Visible = true;
                        dt = dt.CheckEditDeletePermission(CommonMethods.SessionInfo.RoleId, "ViewEditLCInformation");
                        rptLCSummary.DataSource = dt;
                        rptLCSummary.DataBind();
                        lblNoDataFound.Visible = false;
                        lblLabelMessage.Visible = true;
                    }
                    else
                    {
                        rptLCSummary.Visible = false;
                        dataDiv.Visible = false;

                        lblNoDataFound.Visible = true;
                        lblLabelMessage.Visible = false;
                    }
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }


        }


        protected void rptLCSummary_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "ViewDetails")
            {


                if (EnableDisableLCPurpose())
                {
                    divLCPurpose.Visible = true;
                }
                else
                {
                    divLCPurpose.Visible = false;
                }


                pnlDetails.Visible = true;


                Label lblLCNumber = (Label)e.Item.FindControl("lblLCNumber");
                Label lblLCDate = (Label)e.Item.FindControl("lblLCDate");
                Label lblLCAmount = (Label)e.Item.FindControl("lblLCAmount");

                Label lblStatusValue = (Label)e.Item.FindControl("lblStatusValue");
                Label lblLCTypeValue = (Label)e.Item.FindControl("lblLCTypeValue");

                Label lblLCPurposeValue = (Label)e.Item.FindControl("lblLCPurposeValue");



                Label lblCommercialConceredValue = (Label)e.Item.FindControl("lblCommercialConceredValue");


                Label lblBuyer = (Label)e.Item.FindControl("lblBuyer");
                Label lblLCStyles = (Label)e.Item.FindControl("lblLCStyles");
                Label lblAdvisingBank = (Label)e.Item.FindControl("lblAdvisingBank");
                Label lblOtherInfo = (Label)e.Item.FindControl("lblOtherInfo");



                Label lblShipmentMode = (Label)e.Item.FindControl("lblShipmentMode");
                Label lblUDDate = (Label)e.Item.FindControl("lblUDDate");


                Label lblUDStatus = (Label)e.Item.FindControl("lblUDStatus");


                Label lblETDDate = (Label)e.Item.FindControl("lblETDDate");
                Label lblETADate = (Label)e.Item.FindControl("lblETADate");
                Label lblMaturityDate = (Label)e.Item.FindControl("lblMaturityDate");
                Label lblExportLCNumber = (Label)e.Item.FindControl("lblExportLCNumber");



                elcNumber.Text = lblExportLCNumber.Text;
                lblLCNumberValue.Text = lblLCNumber.Text;
                lblLCDateValue.Text = lblLCDate.Text;
                lblLCAmountValue.Text = lblLCAmount.Text;
                lblAdvisingBankValue.Text = lblAdvisingBank.Text;

                lblOtherInfoValue.Text = lblOtherInfo.Text;
                lblStatus.Text = lblStatusValue.Text;

                lblLCType.Text = lblLCTypeValue.Text;
                lblLCPurpose.Text = lblLCPurposeValue.Text;

                lblCommercialConcered.Text = lblCommercialConceredValue.Text;

                lblUDDateValue.Text = lblUDDate.Text;
                lblUDStatusValue.Text = lblUDStatus.Text;

                lblETDDateValue.Text = lblETDDate.Text;
                lblETADateValue.Text = lblETADate.Text;
                lblMaturityDateValue.Text = lblMaturityDate.Text;

                lblShipmentModeValue.Text = lblShipmentMode.Text;


                string LCId = e.CommandArgument.ToString();

                string sql = $"Exec usp_GetPIInfoByLCIds '{LCId}'";
                var dt = unitOfWork.GetDataTableFromSql(sql);

                if (dt.Rows.Count > 0)
                {
                    rptPISummary.Visible = true;
                    //divPIAndOtherInfo.Visible = true;

                    rptPISummary.DataSource = dt;
                    rptPISummary.DataBind();
                    //lblNoPIFound.Visible = false;
                    lblLabelMessage.Visible = true;
                }
                else
                {
                    rptPISummary.Visible = false;
                    //divPIAndOtherInfo.Visible = false;

                    //lblNoPIFound.Visible = true;
                    lblLabelMessage.Visible = false;
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);

            }
            else if (e.CommandName == "Edit")
            {
                string LCId = e.CommandArgument.ToString();
                Response.Redirect("EnterLCInformationV2.aspx?LCId=" + Tools.UrlEncode(LCId));
            }
            else if (e.CommandName == "Delete")
            {
                LCId = Convert.ToInt32(e.CommandArgument.ToString());
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);

            }

        }


        protected void rptPISummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                totalGoods = 0;
                grandTotalAmount = 0;

                string PIId = DataBinder.Eval(e.Item.DataItem, "PIId").ToString();

                Repeater rptPIItemInfo = (Repeater)e.Item.FindControl("rptPIItemInfo");
                Label lblNoItemFound = (Label)e.Item.FindControl("lblNoItemFound");

                string sql2 = $"Exec usp_GetPIItemsByPIId '{PIId}'";
                var dt2 = unitOfWork.GetDataTableFromSql(sql2);

                if (dt2.Rows.Count > 0)
                {
                    lblNoItemFound.Visible = false;

                    rptPIItemInfo.DataSource = dt2;
                    rptPIItemInfo.DataBind();

                }
                else
                {
                    lblNoItemFound.Visible = true;

                }

            }
        }


        protected void rptPIItemInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                var qty = DataBinder.Eval(e.Item.DataItem, "Quantity").ToString();
                var totalAmount = DataBinder.Eval(e.Item.DataItem, "TotalAmount").ToString();

                totalGoods += decimal.Parse(qty);
                grandTotalAmount += decimal.Parse(totalAmount);


            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {

                Label lblTotalQuantity = (Label)e.Item.FindControl("lblTotalQuantity");
                Label lblGrandTotalAmount = (Label)e.Item.FindControl("lblGrandTotalAmount");


                lblTotalQuantity.Text = totalGoods.ToString();
                lblGrandTotalAmount.Text = grandTotalAmount.ToString();

            }

        }


    }
}