﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewEditPIInformation.aspx.cs" Inherits="MBTracker.Pages.Commercial_Tasks.ViewEditPIInformation" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <%--    <style>
        .table th, .table td {
            padding: 4px;
            line-height: 10px;
            text-align: center;
            vertical-align: top;
            border-top: 1px solid #e0e0e0;
        }
    </style>--%>

    <div class="row-fluid">
        <div class="span6">
            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="View PI Information:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">

                    <div class="row-fluid" runat="server" visible="true" id="FilterDiv">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="col-md-10" style="padding-left: 0px">

                            <div class="col-md-9" style="padding-left: 0px">
                                <div class="form-horizontal">

                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%"  CssClass="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">
                                            <asp:Label ID="Label8" runat="server" Text=""></asp:Label></label>
                                        <div class="controls controls-row">
                                            <br />
                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="View PI Info" CssClass="btn btn-info pull-left" OnClick="btnSearch_Click" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>


    <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>

    <div class="row-fluid" runat="server">
        <div class="span12">

            <div class="span9">
                <div class="widget" id="dataDiv" runat="server" visible="false">
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">

                            <div class="control-group">
                                <label for="inputOrder" class="control-label" style="padding-bottom: 10px">
                                    <asp:Label runat="server" ID="lblLabelMessage" Text="" Font-Bold="true" Visible="false"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rptPISummary" runat="server" OnItemCommand="rpt_ItemCommand">
                                        <HeaderTemplate>
                                            <table id="data-table1" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="Label2" runat="server" Text="PI Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label7" runat="server" Text="PI Date"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Style(s) in PI"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label6" runat="server" Text="PO(s) in PI"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label16" runat="server" Text="Actions"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblPINumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PINumber")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblPIDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "PIDate"))%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblPIStyles" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PIStyles")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblPIOrders" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CostingOrders")%>'></asp:Label></td>
                                                <td>
                                                    <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("PIId")%>' class="btn btn-success btn-mini hidden-phone" Text="Edit"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="ViewDetails" CommandArgument='<%#Eval("PIId")%>' class="btn btn-success btn-mini hidden-phone" Text="View Details"></asp:LinkButton>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                         </table>
                            <div class="clearfix">
                                        </FooterTemplate>
                                    </asp:Repeater>

                                </div>
                            </div>
                            
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="span6">
            </div>
        </div>
    </div>
    <asp:Label ID="Label10" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>


    <div class="row-fluid" runat="server" id="pnlDetails" visible="false">

        <div class="span9">
            <div class="widget">
                <div class="widget-body">
                    <div class="row-fluid" runat="server">
                        <div class="form-horizontal">


                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblPINumber" runat="server" Text="PI Number:"></asp:Label></label>
                                <div class="controls controls-row" style="padding-top: 12px">
                                    <asp:Label ID="lblPINumberValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblPIDate2" runat="server" Text="PI Date:"></asp:Label></label>
                                <div class="controls controls-row" style="padding-top: 12px">
                                    <asp:Label ID="lblPIDateValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblStylesInPI" runat="server" Text="Style(s) in PI:"></asp:Label></label>
                                <div class="controls controls-row" style="padding-top: 12px">
                                    <asp:Label ID="lblStylesInPIValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblPOsInPI" runat="server" Text="PO(s) in PI:"></asp:Label></label>
                                <div class="controls controls-row" style="padding-top: 12px">
                                    <asp:Label ID="lblPOsInPIValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>


                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblPIEntry" runat="server" Text="Item Info:"></asp:Label></label>
                                <div class="controls controls-row" style="overflow-x: auto">
                                    <asp:Repeater ID="rptPIItemEntryInfo" runat="server" OnItemDataBound="rptPIItemEntryInfo_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblDescription" runat="server" Text="Description of Goods"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblHSCode" runat="server" Text="HS Code"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblQty" runat="server" Text="Quantity(Lbs)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price(US$/Lbs)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblAmount" runat="server" Text="Amount(US$)"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="width: 40%">
                                                    <asp:Label ID="lblDescriptionOfGoods" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Description")%>'></asp:Label>
                                                </td>
                                                <td style="width: 14%; padding-top: 18px">
                                                    <asp:Label ID="lblHSCodeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "HSCode")%>'></asp:Label>
                                                </td>
                                                <td style="width: 14%; padding-top: 18px">
                                                    <asp:Label ID="lblQtyValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity")%>'></asp:Label>
                                                </td>
                                                <td style="width: 18%; padding-top: 18px">
                                                    <asp:Label ID="lblUnitPriceValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UnitPrice")%>'></asp:Label>
                                                </td>
                                                <td style="width: 14%; padding-top: 18px">
                                                    <asp:Label ID="lblTotalAmount" Width="100%" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TotalAmount")%>'></asp:Label>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>

                                             <td style="font-weight:500; text-align:left" >Total:</td>
                                             <td></td>
                                             <td> <asp:Label ID="lblTotalQuantity" runat="server" Text=""></asp:Label></td>
                                             <td></td>
                                             <td> <asp:Label ID="lblGrandTotalAmount" runat="server" Text=""></asp:Label></td>

                                            </tbody>
                                 </table>
                                           
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblPaymentMode" runat="server" Text="Payment Mode:"></asp:Label></label>
                                <div class="controls controls-row" style="padding-top: 12px">
                                    <asp:Label ID="lblPaymentModeValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblShipment" runat="server" Text="Shipment Info:"></asp:Label></label>
                                <div class="controls controls-row" style="padding-top: 12px">
                                    <asp:Label ID="lblShipmentInfoValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblPartialShipment" runat="server" Text="Partial Shipment:"></asp:Label></label>
                                <div class="controls controls-row" style="padding-top: 12px">
                                    <asp:Label ID="lblPartialShipmentValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblTolarance" runat="server" Text="Tolerance Info:"></asp:Label></label>
                                <div class="controls controls-row" style="padding-top: 12px">
                                    <asp:Label ID="lblTolaranceValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblBeneficiaryName" runat="server" Text="Beneficiary:"></asp:Label></label>
                                <div class="controls controls-row" style="padding-top: 12px">
                                    <asp:Label ID="lblBeneficiaryNameValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblBeneficiaryPhone" runat="server" Text="Beneficiary Phone:"></asp:Label></label>
                                <div class="controls controls-row" style="padding-top: 12px">
                                    <asp:Label ID="lblBeneficiaryPhoneValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblBeneficiaryBank" runat="server" Text="Beneficiary Bank:"></asp:Label></label>
                                <div class="controls controls-row" style="padding-top: 12px">
                                    <asp:Label ID="lblBeneficialyBankInfo" runat="server" Text=""></asp:Label>
                                </div>
                            </div>


                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblOthers" runat="server" Text="Other Info:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                <div class="controls controls-row" style="padding-top: 12px">
                                    <asp:Label ID="lblOthersValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>

        </div>

    </div>


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
