﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Commercial_Tasks
{
    public partial class ViewEditImportDocInfo : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadDropdown(ddlBeneficiary, "YarnAndAccessoriesSuppliers", 1, 0);
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteDocInfo(DocId);
                }
            }
        }
   

        int DocId
        {
            set { ViewState["DocId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["DocId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBeneficiary_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataDiv.Visible = false;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            DocId = int.Parse(e.CommandArgument.ToString());
            if (DocId != 0)
            {
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
            }
            else
            {
                var title = "Error";
                var msg = "Sorry, you can not delete it.<br />Something went wrong.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            }
        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            var id = int.Parse(e.CommandArgument.ToString());
            Response.Redirect("EnterImportDocInfo.aspx?DocId=" + Tools.UrlEncode(id + ""));
        }


        private void DeleteDocInfo(int id)
        {
            var invInfo = unitOfWork.GenericRepositories<ImportDocumnetInvoices>().Get(x => x.ImportDocumentId == id).ToList();
            foreach (var item in invInfo)
            {
                unitOfWork.GenericRepositories<ImportDocumnetInvoices>().Delete(item);
            }
            var Info = unitOfWork.GenericRepositories<ImportDocuments>().GetByID(id);

            unitOfWork.GenericRepositories<ImportDocuments>().Delete(Info);
            unitOfWork.Save();
            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
            LoadData();
        }

        protected void LoadData()
        {
            dataDiv.Visible = false;
            pnlDetails.Visible = false;
            try
            {

                if (tbxLCNumber.Text == "" && ddlBeneficiary.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter search criteria.')", true);
                    return;
                }

                var lcNumber = "";
                if (!string.IsNullOrEmpty(tbxLCNumber.Text))
                {
                    lcNumber = tbxLCNumber.Text;
                }

                var beneficiaryId = 0;
                if (ddlBeneficiary.SelectedValue != "")
                {
                    beneficiaryId = int.Parse(ddlBeneficiary.SelectedValue);
                }

                var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetImportDocInfo '{beneficiaryId}','{lcNumber}'");

                if (dt.Rows.Count > 0)
                {
                    rptPISummary.Visible = true;
                    dataDiv.Visible = true;

                    dt = dt.CheckEditDeletePermission(CommonMethods.SessionInfo.RoleId, "ViewEditImportDocInfo");
                    rptPISummary.DataSource = dt;
                    rptPISummary.DataBind();
                    lblNoDataFound.Visible = false;
                    lblLabelMessage.Visible = true;
                }
                else
                {
                    rptPISummary.Visible = false;
                    dataDiv.Visible = false;

                    lblNoDataFound.Visible = true;
                    lblLabelMessage.Visible = false;
                    
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void lnkbtnViewDetails_Command(object sender, CommandEventArgs e)
        {
            var id = int.Parse(e.CommandArgument.ToString());
            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetImportDocInfoById '{id}'");
            var dtDetails = unitOfWork.GetDataTableFromSql($"SELECT * FROM ImportDocumnetInvoices WHERE ImportDocumentId='{id}'");
            if (dt.Rows.Count > 0)
            {
                pnlDetails.Visible = true;
                lblDetailsNotFound.Visible = false;
                rptInvoices.DataSource = null;
                if (dtDetails.Rows.Count > 0)
                {
                    rptInvoices.DataSource = dtDetails;
                    rptInvoices.DataBind();
                }

                lblBeneficiary.Text = dt.Rows[0]["BeneficiaryName"].ToString();
                lblLCNumber.Text = dt.Rows[0]["LCNumber"].ToString();
                lblChalanOrBLNumber.Text = dt.Rows[0]["ChalanOrBLNumber"].ToString();
                lblDocReceiveDate.Text = Convert.ToDateTime(dt.Rows[0]["DocReceiveDate"].ToString()).ToString("dd-MMM-yyyy");
                lblChalanOrBLDate.Text = Convert.ToDateTime(dt.Rows[0]["ChalanOrBLDate"].ToString()).ToString("dd-MMM-yyyy");
                lblPaymentDate.Text = Convert.ToDateTime(dt.Rows[0]["PaymentDate"].ToString()).ToString("dd-MMM-yyyy");
                lblDocumentValue.Text = dt.Rows[0]["DocumentValue"].ToString();
                lblDocumentQty.Text = dt.Rows[0]["DocumentQty"].ToString();
                lblPaymentStatus.Text = dt.Rows[0]["PaymentStatusName"].ToString();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown()", true);
            }
            else
            {
                pnlDetails.Visible = false;
                lblDetailsNotFound.Visible = true;
            }
        }
    }
}