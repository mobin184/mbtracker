﻿using MBTracker.Code_Folder;
using Repositories;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Commercial_Tasks
{
    public partial class ViewEditCostingSheets : System.Web.UI.Page
    {

        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                EnableDisableEditButton();

                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);

            }
        }

        private void EnableDisableEditButton()
        {
            //ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn("Admin,Top Mgmt,Merchandiser,QCE");
            ViewState["editEnabled"] = CommonMethods.CurrentUserRollIn2("ViewEditCostingSheets", 1, 1);
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataDiv.Visible = false;
            pnlDetails.Visible = false;
            Div1.Visible = false;
            pnlSummary.Visible = false;
            btnPrint.Visible = false;

            if (ddlBuyers.SelectedValue != "")
            {
                CommonMethods.LoadDropdownById(ddlStyles, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);

                ddlOrders.Items.Clear();
            }
            else
            {
                ddlStyles.Items.Clear();
                ddlOrders.Items.Clear();

            }
        }



        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataDiv.Visible = false;
            pnlDetails.Visible = false;
            Div1.Visible = false;
            pnlSummary.Visible = false;
            btnPrint.Visible = false;

            if (ddlStyles.SelectedValue != "")
            {
                CommonMethods.LoadOrderDropdownByStyle(ddlOrders, Convert.ToInt32(ddlStyles.SelectedValue), 1, 0);
            }
            else
            {
                ddlOrders.Items.Clear();

            }


        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            dataDiv.Visible = false;
            pnlDetails.Visible = false;
            Div1.Visible = false;
            pnlSummary.Visible = false;
            btnPrint.Visible = false;

            if (ddlStyles.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select an style.')", true);
            }
            //else if (ddlOrders.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select an order.')", true);
            //}
            else
            {

                try
                {

                    int styleId = 0;
                    int orderId = 0;

                    styleId = int.Parse(ddlStyles.SelectedValue);

                    if (ddlOrders.SelectedValue != "")
                    {
                        orderId = int.Parse(ddlOrders.SelectedValue);
                    }


                    lblLabelMessage.Text = "Costing info";

                    //var userId = CommonMethods.SessionInfo.UserId;

                    string sql = $"Exec usp_GetOrderCostingInfo '{styleId}','{orderId}'";

                    var dt = unitOfWork.GetDataTableFromSql(sql);

                    if (dt.Rows.Count > 0)
                    {
                        rpt.Visible = true;
                        dataDiv.Visible = true;

                        rpt.DataSource = dt;
                        rpt.DataBind();
                        lblNoDataFound.Visible = false;
                        lblLabelMessage.Visible = true;
                    }
                    else
                    {
                        rpt.Visible = false;
                        dataDiv.Visible = false;

                        lblNoDataFound.Visible = true;
                        lblLabelMessage.Visible = false;
                    }

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }

            }


        }




        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "ViewDetails")
            {
                pnlDetails.Visible = true;
                Div1.Visible = true;
                pnlSummary.Visible = true;
                btnPrint.Visible = true;


                Label lblOrderNumber = (Label)e.Item.FindControl("lblOrderNumber");
                Label lblOrderSeason = (Label)e.Item.FindControl("lblOrderSeason");
                Label lblOrderQty = (Label)e.Item.FindControl("lblOrderQty");
                Label lblDeliveryDate = (Label)e.Item.FindControl("lblDeliveryDate");
                Label lblLCNumber = (Label)e.Item.FindControl("lblLCNumber");
                Label lblLCValue = (Label)e.Item.FindControl("lblLCValue");
                Label lblFOBPrice = (Label)e.Item.FindControl("lblFOBPrice");
                Label lblItemDescription = (Label)e.Item.FindControl("lblItemDescription");
                Label lblGarmentsWt = (Label)e.Item.FindControl("lblGarmentsWt");
                Label lblCommissionOrCharge = (Label)e.Item.FindControl("lblCommissionOrCharge");



                lblOrderNumberValue.Text = lblOrderNumber.Text;
                lblOrderSeasonValue.Text = lblOrderSeason.Text;
                lblOrderQtyValue.Text = lblOrderQty.Text;
                lblDeliveryDateValue.Text = lblDeliveryDate.Text;
                lblLCNumberValue.Text = lblLCNumber.Text;
                lblLCValueValue.Text = lblLCValue.Text;
                lblFOBPriceValue.Text = lblFOBPrice.Text;
                lblGarmentsWeightValue.Text = lblGarmentsWt.Text;

                lblStyleValue.Text = ddlStyles.SelectedItem.Text;
                lblItemValue.Text = lblItemDescription.Text;
                lblBuyerValue.Text = ddlBuyers.SelectedItem.Text;

                lblCommissionCharge.Text = lblCommissionOrCharge.Text;


                string costingInfoId = e.CommandArgument.ToString();

                string sql = $"Exec usp_GetOrderCostingInfoDetails '{costingInfoId}'";
                var dt = unitOfWork.GetDataTableFromSql(sql);

                if (dt.Rows.Count > 0)
                {
                    rptCostingItemInfo.Visible = true;

                    rptCostingItemInfo.DataSource = dt;
                    rptCostingItemInfo.DataBind();

                }
                else
                {
                    rptCostingItemInfo.Visible = false;

                }

                string sql2 = $"Exec usp_GetOrderCostingInfoSummary '{costingInfoId}'";
                var dt2 = unitOfWork.GetDataTableFromSql(sql2);

                decimal fobPerDzn = (decimal.Parse(dt2.Rows[0]["FOBPrice"].ToString()) * 12);
                lblFobPerDzn.Text = fobPerDzn.ToString();

                decimal commisionOrCharge = decimal.Parse(dt2.Rows[0]["CommissionOrCharge"].ToString());
                lblCommissionOrChargeValue.Text = (fobPerDzn * commisionOrCharge / 100).ToString();

                decimal yarnCostPerDzn = decimal.Parse(dt2.Rows[0]["YarnCostPerDzn"].ToString());
                lblYarnCostPerDzn.Text = yarnCostPerDzn.ToString();

                decimal accCostPerDzn = decimal.Parse(dt2.Rows[0]["AccCostPerDzn"].ToString());
                lblAccCostPerDzn.Text = accCostPerDzn.ToString();

                lblCMPerDzn.Text = (fobPerDzn - (yarnCostPerDzn + accCostPerDzn + (fobPerDzn * commisionOrCharge / 100))).ToString();



                ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);

            }
            else if (e.CommandName == "Edit")
            {
                string costingSheetId = e.CommandArgument.ToString();
                Response.Redirect("AddCostingSheetV2.aspx?costingSheetId=" + Tools.UrlEncode(costingSheetId));
            }

        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {

            divSearchCriteria.Visible = false;
            dataDiv.Visible = false;
            pnlDetails.Visible = true;
            pnlSummary.Visible = true;
            lblCostingSheetTitle.Visible = true;
            btnPrint.Visible = false;
            //divSearchCriteria.Style.Add()
        }


        }
}