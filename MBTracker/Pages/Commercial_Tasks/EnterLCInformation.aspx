﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="EnterLCInformation.aspx.cs" Inherits="MBTracker.Pages.Commercial_Tasks.EnterLCInformation"  Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <style>
        input[type="radio"], input[type=checkbox] {
            margin: -2px 0 0;
        }


        .cblClass label {
            /*display: block;*/
            padding-left: 2px !important;
            padding-right: 14px !important;
            line-height:20px !important;
        }
    </style>

    <div class="row-fluid">

        <div class="span9">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="actionTitle" Text="Enter L/C Information:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="pull-right" style="text-align: center; font-size: 12px; line-height: 20px; padding-bottom: 5px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="lblPINumber" runat="server" Text="L/C Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxLCNumber" runat="server" placeholder="Enter L/C Number" CssClass="form-control" Width="40%"></asp:TextBox>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label1" runat="server" Text="L/C Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxLCDate" runat="server" placeholder="Enter L/C date" CssClass="form-control" Width="40%" TextMode="Date"></asp:TextBox>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="lblAdvisingBank" runat="server" Text="Applicant Bank:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlApplicantBanks" runat="server" CssClass="form-control" Style="min-width: 125px" Width="40%"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="40%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>



                        <div class="control-group" id="divStyles" runat="server" visible="false">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblStyle" runat="server" Text="Select Style(s):"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <div class="form-inline cblClass">
                                    <asp:CheckBoxList ID="cblStyles" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="4" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                    <asp:Label ID="lblNoStyleFound" runat="server" Visible="false" Text="No Style found." BackColor="#ffff00"></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="Get Proforma Invoices (PI)" CssClass="btn btn-info pull-left" OnClick="btnLoadPIs_Click" />

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <br />
    <br />

    <div class="row-fluid">




        <div class="span12" id="divPIAndOtherInfo" runat="server" visible="false">
            <div class="widget">

                <div class="widget-body">

                    <div class="form-horizontal">


                        <div class="control-group">

                            <div class="controls controls-row" style="text-align: center; padding-top: 10px">
                                <asp:Label runat="server" ID="lblLabelMessage" Text="Available PIs for styles you selected:" Visible="false" Font-Bold="true"></asp:Label>
                                <span style="font-weight: 700; color: #CC0000">
                                    <br />
                                    (You cannot select PIs from different beneficiaries)</span>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label3" runat="server" Text="Select PI"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:Repeater ID="rptPISummary" runat="server" OnItemDataBound="rptPISummary_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="Label16" runat="server" Text="Select"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label2" runat="server" Text="PI Number"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label7" runat="server" Text="PI Date"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label10" runat="server" Text="Beneficiary"></asp:Label></th>
                                                    <%-- <th>
                                                        <asp:Label ID="Label5" runat="server" Text="Style(s) in PI"></asp:Label></th>--%>
                                                    <th>
                                                        <asp:Label ID="Label6" runat="server" Text=""></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="vertical-align: middle">
                                                <asp:CheckBox ID="cbxPI" runat="server" AutoPostBack="true" OnCheckedChanged="lnkbtnCalculateLCTotal_Click" />
                                                <asp:Label ID="lblPIId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PIId")%>' Visible="false"></asp:Label>
                                            </td>
                                            <td style="vertical-align: middle">
                                                <asp:Label ID="lblPINumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PINumber")%>'></asp:Label></td>
                                            <td style="vertical-align: middle">
                                                <asp:Label ID="lblPIDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "PIDate"))%>'></asp:Label></td>
                                            <td style="vertical-align: middle">
                                                <asp:Label ID="lblBeneficiaryName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BeneficiaryName")%>'></asp:Label></td>
                                            <%-- <td style="vertical-align: middle">
                                                <asp:Label ID="lblPIStyles" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PIStyles")%>'></asp:Label></td>--%>
                                            <td style="vertical-align: middle">

                                                <asp:Repeater ID="rptPIItemInfo" runat="server" OnItemDataBound="rptPIItemInfo_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="data-table" class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>
                                                                        <asp:Label ID="lblStyles" runat="server" Text="Style"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblOrders" runat="server" Text="PO"></asp:Label></th>
                                                                   <%-- <th>
                                                                        <asp:Label ID="lblItemTypes" runat="server" Text="Item Type"></asp:Label></th>--%>
                                                                    <th>
                                                                        <asp:Label ID="lblItems" runat="server" Text="Item Description"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblQty" runat="server" Text="Qty(Lbs)"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblAmount" runat="server" Text="Amount(US$)"></asp:Label></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblStylesValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "StyleName")%>'></asp:Label>
                                                            </td>
                                                            <td style="width: 15%">
                                                                <asp:Label ID="lblPOValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderNumber")%>'></asp:Label>
                                                            </td>
                                                           <%-- <td style="width: 15%">
                                                                <asp:Label ID="lblItemTypeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RawMaterialItemTypeName")%>'></asp:Label>
                                                            </td>--%>
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblItemValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ItemName")%>'></asp:Label>
                                                            </td>
                                                            <td style="width: 10%; padding-top: 18px">
                                                                <asp:Label ID="lblQtyValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity")%>'></asp:Label>
                                                            </td>
                                                            <td style="width: 10%; padding-top: 18px">
                                                                <asp:Label ID="lblUnitPriceValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UnitPrice")%>'></asp:Label>
                                                            </td>
                                                            <td style="width: 10%; padding-top: 18px">
                                                                <asp:Label ID="lblTotalAmount" Width="100%" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TotalAmount")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>


                                                        <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                            <asp:Label ID="lblTotal" runat="server" Text="Total:" Font-Bold="true"></asp:Label></td>
                                                        <%-- <td style="font-weight: 500; text-align: left; background-color: lavender"></td> --%>
                                                        <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                        <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                      <%--  <td style="font-weight: 500; text-align: left; background-color: lavender"></td>--%>
                                                        <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                            <asp:Label ID="lblTotalQuantity" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                                                        <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                        <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                            <asp:Label ID="lblGrandTotalAmount" runat="server" Text="" Font-Bold="true"></asp:Label></td>


                                                        </tbody>
                                 </table>
                                           
                                                    </FooterTemplate>
                                                </asp:Repeater>

                                                <div style="text-align: center">
                                                    <asp:Label ID="lblNoItemFound" runat="server" Visible="false" Text="No Item found." BackColor="#ffff00"></asp:Label>
                                                </div>

                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                         </table>
                            <div class="clearfix">
                                    </FooterTemplate>
                                </asp:Repeater>

                            </div>
                        </div>





                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="lblLCAmountMessage" runat="server" Text="LC Amount($):"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                            <div class="controls controls-row" style="text-align: left; padding-top: 10px">
                                <asp:Label runat="server" ID="lblLCAmount" Text=""></asp:Label>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="lblNominatedOrNot" runat="server" Text="Shipment Mode:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlShipmentMode" runat="server" Display="Dynamic" Width="20%" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlShipmentMode_SelectedIndexChanged">
                                    <asp:ListItem Text="---Select---" Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Sea" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Air" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                                     <asp:ListItem Text="EPZ" Value="4"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label8" runat="server" Text="UD Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxUDDate" runat="server" CssClass="form-control" Width="20%" TextMode="Date"></asp:TextBox>
                            </div>
                        </div>


                         <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label12" runat="server" Text="UD Status:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlUDStatus" runat="server" Display="Dynamic" Width="20%" CssClass="form-control">
                                    <asp:ListItem Text="Not Completed" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Completed" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label9" runat="server" Text="ETD Date:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxETDDate" runat="server" CssClass="form-control" Width="20%" TextMode="Date"></asp:TextBox>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label11" runat="server" Text="ETA Date:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxETADate" runat="server" CssClass="form-control" Width="20%" TextMode="Date"></asp:TextBox>
                            </div>
                        </div>

                         <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label13" runat="server" Text="Maturity Date:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxMaturityDate" runat="server" CssClass="form-control" Width="20%" TextMode="Date"></asp:TextBox>
                            </div>
                        </div>

                         <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label14" runat="server" Text="LC Type:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlLCType" runat="server" CssClass="form-control" Width="20%">

                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label5" runat="server" Text="LC Status:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control" Width="20%">
                                </asp:DropDownList>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="Commercial Concerned:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlCommercialConcerened" runat="server" CssClass="form-control" Width="20%">
                                </asp:DropDownList>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="lblOtherInfo" runat="server" Text="Other Info:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxOtherInfo" runat="server" placeholder="Enter Other Info" CssClass="form-control" Width="40%" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>



                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label4" runat="server" Text=""></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                            <div class="controls controls-row" style="text-align: right; padding-top: 1px">
                                <%--<asp:Button ID="btnCalculsteTotal" runat="server" Text="Calculate Total:" class="btn btn-add btn-small btnStyle" OnClick="lnkbtnCalculateLCTotal_Click" />--%>
                                <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>

                            </div>
                        </div>

                    </div>

                </div>


            </div>

        </div>


        <div style="text-align: left">
            <asp:Label ID="lblNoPIFound" runat="server" Visible="false" Text="No PI found." BackColor="#ffff00"></asp:Label>
        </div>

    </div>



</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
