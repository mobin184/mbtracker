﻿using MBTracker.Code_Folder;
using MBTracker.EF;
using Repositories;
using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Commercial_Tasks
{
    public partial class ViewEditExportLCInfo : System.Web.UI.Page
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteLCInfo(LCId);
                }
            }
        }
   

        int LCId
        {
            set { ViewState["LCId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["LCId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataDiv.Visible = false;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {
            LCId = int.Parse(e.CommandArgument.ToString());
            if (LCId != 0)
            {
                var title = "Warning";
                var msg = "Are you sure you want to delete?";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
            }
            else
            {
                var title = "Error";
                var msg = "Sorry, you can not delete it.<br />Something went wrong.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
            }
        }

        protected void lnkbtnEdit_Command(object sender, CommandEventArgs e)
        {
            var lcId = int.Parse(e.CommandArgument.ToString());
            Response.Redirect("EnterExportLCInformation.aspx?LCId=" + Tools.UrlEncode(lcId + ""));
        }


        private void DeleteLCInfo(int lcId)
        {
            var lcInfo = unitOfWork.GenericRepositories<ExportLCInfo>().GetByID(lcId);  
            var lcDetails = unitOfWork.GenericRepositories<ExportLCDetails>().Get(x=>x.LCId == lcId).ToList();
            foreach (var item in lcDetails)
            {
                unitOfWork.GenericRepositories<ExportLCDetails>().Delete(item);
            }
            unitOfWork.GenericRepositories<ExportLCInfo>().Delete(lcInfo);
            unitOfWork.Save();
            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Deleted successfully.')", true);
            LoadData();
        }

        protected void LoadData()
        {
            dataDiv.Visible = false;
            try
            {

                if (tbxLCNumber.Text == "" && ddlBuyers.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter search criteria.')", true);
                    return;
                }

                var lcNumber = "";
                if (!string.IsNullOrEmpty(tbxLCNumber.Text))
                {
                    lcNumber = tbxLCNumber.Text;
                }

                var buyerId = 0;
                if (ddlBuyers.SelectedValue != "")
                {
                    buyerId = int.Parse(ddlBuyers.SelectedValue);
                }

                var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetExportLCInfo '{buyerId}','{lcNumber}'");

                if (dt.Rows.Count > 0)
                {
                    rptPISummary.Visible = true;
                    dataDiv.Visible = true;

                    dt = dt.CheckEditDeletePermission(CommonMethods.SessionInfo.RoleId, "ViewEditExportLCInfo");
                    rptPISummary.DataSource = dt;
                    rptPISummary.DataBind();
                    lblNoDataFound.Visible = false;
                    lblLabelMessage.Visible = true;
                }
                else
                {
                    rptPISummary.Visible = false;
                    dataDiv.Visible = false;

                    lblNoDataFound.Visible = true;
                    lblLabelMessage.Visible = false;
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }
    }
}