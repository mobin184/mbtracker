﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Commercial_Tasks;
using MBTracker.EF;
using Newtonsoft.Json;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Commercial_Tasks
{
    public partial class EnterExportLCInformation : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        CommercialManager commercialManager = new CommercialManager();

        UnitOfWork unitOfWork = new UnitOfWork();
        List<string> selectedStyles = new List<string>();
        List<string> selectedPOs = new List<string>();
        List<string> selectedSCs = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Request.QueryString["LCId"] != null)
                {
                    LCId = int.Parse(Tools.UrlDecode(Request.QueryString["LCId"]));
                    actionTitle.Text = "Update Export LC/Sales Contract Information";
                    lnkbtnSaveEntries.Visible = false;
                    lnkbtnUpdateEntries.Visible = true;

                    CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                    CommonMethods.LoadDropdown(ddlAdvisingBank, "ApplicantBanks", 1, 0);
                    CommonMethods.LoadDropdown(ddlTenor, "Tenor", 1, 0);
                    CommonMethods.LoadDropdown(ddlStatus, "ExportLCStatus", 1, 0);
                    CommonMethods.LoadCommercialConceredDropDown(ddlCommercialConcern, 1, 0);
                    LoadLCInfo();
                }
                else
                {
                    CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                    CommonMethods.LoadDropdown(ddlAdvisingBank, "ApplicantBanks", 1, 0);
                    CommonMethods.LoadDropdown(ddlTenor, "Tenor", 1, 0);
                    //CommonMethods.LoadDropdown(ddlAccountHolder, "AccountHolders", 1, 0);
                    CommonMethods.LoadDropdown(ddlStatus, "ExportLCStatus", 1, 0);
                    CommonMethods.LoadCommercialConceredDropDown(ddlCommercialConcern, 1, 0);
                }
            }
            //else
            //{
            //    string controlName = this.Request.Params.Get("__EVENTTARGET");
            //    if (controlName.Contains("ddlSalesContract"))
            //    {
            //        var ids = JsonConvert.DeserializeObject<List<int>>(Request["__EVENTARGUMENT"]);
            //        ddlSalesContract_SelectedIndexChanged(ids);
            //    }
                
            //}
        }


        int LCId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["LCId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["LCId"] = value; }
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {
            divPIOtherInfo2.Visible = false;
            cblSalesContract.Items.Clear();
            divPOs.Visible = false;
            cblPO.Items.Clear();
            if (ddlBuyers.SelectedValue != "")
            {
                divPIOtherInfo2.Visible = true;
                LoadStylesByBuyerIds(ddlBuyers.SelectedValue);
                LoadSalesContract(int.Parse(ddlBuyers.SelectedValue));
                LoadLCForAmandment(int.Parse(ddlBuyers.SelectedValue));
                LoadSCForAmandment(int.Parse(ddlBuyers.SelectedValue));
            }
        }


        protected void LoadSalesContract(int buyerId)
        {
            var sc = unitOfWork.GenericRepositories<ExportLCInfo>().Get(x => x.BuyerId == buyerId && x.LCOrSalesContract == 2 && x.LCId != LCId).GroupBy(x => x.LCNumber).Select(y => y.OrderByDescending(x => x.LCId).First()).ToList(); /// here 2 is for Sales Contract

            cblSalesContract.DataSource = sc;
            cblSalesContract.DataValueField = "LCId";
            cblSalesContract.DataTextField = "LCNumber";
            cblSalesContract.DataBind();
            //ddlSalesContract.Items.Insert(0, new ListItem("", string.Empty));
            //ddlSalesContract.SelectedValue = "";
        }


        protected void LoadLCForAmandment(int buyerId)
        {
            //var lc = unitOfWork.GetDataTableFromSql($"SELECT * FROM ExportLCInfo WHERE BuyerId = '{buyerId}'")
            var lc = unitOfWork.GenericRepositories<ExportLCInfo>().Get(x => x.BuyerId == buyerId && x.LCOrSalesContract == 1 && x.LCId != LCId).GroupBy(x => x.LCNumber).Select(y => y.OrderByDescending(x => x.LCId).First()).ToList(); /// here 1 is for LC
            ddlLC.DataSource = lc;
            ddlLC.DataValueField = "LCId";
            ddlLC.DataTextField = "LCNumber";
            ddlLC.DataBind();
            ddlLC.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlLC.SelectedValue = "";
        }

        protected void LoadSCForAmandment(int buyerId)
        {
            //var lc = unitOfWork.GetDataTableFromSql($"SELECT * FROM ExportLCInfo WHERE BuyerId = '{buyerId}'")
            var sc = unitOfWork.GenericRepositories<ExportLCInfo>().Get(x => x.BuyerId == buyerId && x.LCOrSalesContract == 2 && x.LCId != LCId).GroupBy(x => x.LCNumber).Select(y => y.OrderByDescending(x => x.LCId).First()).ToList(); /// here 2 is for Sales Contract
            ddlSc.DataSource = sc;
            ddlSc.DataValueField = "LCId";
            ddlSc.DataTextField = "LCNumber";
            ddlSc.DataBind();
            ddlSc.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddlSc.SelectedValue = "";
        }
        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {
            try
            {
                selectedSCs.Clear();
                selectedPOs.Clear();

                for (int i = 0; i < cblPO.Items.Count; i++)
                {
                    if (cblPO.Items[i].Selected)
                    {
                        selectedPOs.Add(cblPO.Items[i].Value);
                    }
                }

                for (int j = 0; j < cblSalesContract.Items.Count; j++)
                {
                    if (cblSalesContract.Items[j].Selected)
                    {
                        selectedSCs.Add(cblSalesContract.Items[j].Value);
                    }
                }

                //if (selectedPOs.Count > 0)
                //{
                var exportLcInfo = new ExportLCInfo();
                exportLcInfo.LCOrSalesContract = int.Parse(ddlLCOrSalesContract.SelectedValue);
                exportLcInfo.LCForSalesContract = 0;
                if (ddlLCForSalesContract.SelectedValue != "" && exportLcInfo.LCOrSalesContract == 1)
                {
                    exportLcInfo.LCForSalesContract = int.Parse(ddlLCForSalesContract.SelectedValue);
                }

                exportLcInfo.LcAmandment = 0;
                exportLcInfo.AmandmentNumber = "";
                if (ddlLCAmandment.SelectedValue != "" && exportLcInfo.LCForSalesContract == 0)
                {
                    exportLcInfo.LcAmandment = int.Parse(ddlLCAmandment.SelectedValue);
                    exportLcInfo.AmandmentNumber = tbxAmandmentNumber.Text;
                }


                exportLcInfo.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                exportLcInfo.LCNumber = tbxLCNumber.Text;
                exportLcInfo.LCDate = DateTime.Parse(tbxLCDate.Text);
                exportLcInfo.IssuingBankAndBranch = tbxIssuingBankAndBranch.Text;
                exportLcInfo.AdvisingBankId = int.Parse(ddlAdvisingBank.SelectedValue);
                exportLcInfo.Tenor = int.Parse(ddlTenor.SelectedValue);
                exportLcInfo.LCAmount = Decimal.Parse(tbxLCAmount.Text);
                if (!string.IsNullOrEmpty(tbxLCQuantity.Text))
                {
                    exportLcInfo.LCQuantity = Decimal.Parse(tbxLCQuantity.Text);
                }
                exportLcInfo.LCCommission = Decimal.Parse(tbxLCCommission.Text);
                exportLcInfo.CommercialConcernId = int.Parse(ddlCommercialConcern.SelectedValue);
                exportLcInfo.Status = int.Parse(ddlStatus.SelectedValue);
                exportLcInfo.LastShipmentDate = DateTime.Parse(tbxLastShipmentDate.Text);
                exportLcInfo.ExpiaryDate = DateTime.Parse(tbxExpiaryDate.Text);
                exportLcInfo.CreatedBy = CommonMethods.SessionInfo.UserName;
                exportLcInfo.CreateDate = DateTime.Now;
                exportLcInfo.ParentLCId = null;
                exportLcInfo.SalesContractId = null;
                if (ddlLC.SelectedValue != "" && exportLcInfo.LcAmandment == 1)
                {
                    exportLcInfo.ParentLCId = int.Parse(ddlLC.SelectedValue);
                }
                else if(ddlSc.SelectedValue != "" && exportLcInfo.LcAmandment == 1)
                {
                    exportLcInfo.ParentLCId = int.Parse(ddlSc.SelectedValue);
                }
                if (selectedSCs.Count() > 0 && exportLcInfo.LCForSalesContract == 1)
                {
                    //exportLcInfo.SalesContractId = int.Parse(ddlSalesContract.SelectedValue);
                    exportLcInfo.SalesContractId = string.Join(",", selectedSCs);

                }
                List<ExportLCDetails> lstDetails = new List<ExportLCDetails>();
                foreach (var item in selectedPOs)
                {
                    var detail = new ExportLCDetails()
                    {
                        LCId = exportLcInfo.LCId,
                        OrderId = int.Parse(item),
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };
                    lstDetails.Add(detail);
                }
                exportLcInfo.ExportLCDetails = lstDetails;
                unitOfWork.GenericRepositories<ExportLCInfo>().Insert(exportLcInfo);
                unitOfWork.Save();

                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('EnterExportLCInformation.aspx');", true);
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select PO')", true);
                //}
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        private void LoadLCInfo()
        {
            try
            {
                divPIOtherInfo2.Visible = true;

                var exportLcInfo = unitOfWork.GenericRepositories<ExportLCInfo>().GetByID(LCId);

                LoadSalesContract(exportLcInfo.BuyerId);
                LoadLCForAmandment(exportLcInfo.BuyerId);
                LoadSCForAmandment(exportLcInfo.BuyerId);
                LoadStylesByBuyerIds(exportLcInfo.BuyerId + "");
                //LoadSalesContract(exportLcInfo.BuyerId);

                ddlLCOrSalesContract.SelectedValue = exportLcInfo.LCOrSalesContract + "";
                ddlLCForSalesContract.SelectedValue = exportLcInfo.LCForSalesContract + "";
                ddlLCAmandment.SelectedValue = exportLcInfo.LcAmandment + "";
                tbxAmandmentNumber.Text = exportLcInfo.AmandmentNumber;
                ddlLC.SelectedValue = exportLcInfo.ParentLCId + "";
                ddlSc.SelectedValue = exportLcInfo.ParentLCId + "";
                //ddlSalesContract.SelectedValue = exportLcInfo.SalesContractId + "";
                ddlBuyers.SelectedValue = exportLcInfo.BuyerId + "";
                tbxLCNumber.Text = exportLcInfo.LCNumber;
                tbxLCDate.Text = exportLcInfo.LCDate.ToString("yyyy-MM-dd");
                tbxIssuingBankAndBranch.Text = exportLcInfo.IssuingBankAndBranch;
                ddlAdvisingBank.SelectedValue = exportLcInfo.AdvisingBankId + "";
                ddlTenor.SelectedValue = exportLcInfo.Tenor + "";
                tbxLCQuantity.Text = exportLcInfo.LCQuantity + "";
                tbxLCAmount.Text = exportLcInfo.LCAmount + "";
                tbxLCCommission.Text = exportLcInfo.LCCommission + "";
                ddlCommercialConcern.SelectedValue = exportLcInfo.CommercialConcernId + "";
                ddlStatus.SelectedValue = exportLcInfo.Status + "";
                tbxLastShipmentDate.Text = exportLcInfo.LastShipmentDate?.ToString("yyyy-MM-dd");
                tbxExpiaryDate.Text = exportLcInfo.ExpiaryDate?.ToString("yyyy-MM-dd");

                // var lcInfo = unitOfWork.GenericRepositories<ExportLCInfo>().GetByID(int.Parse(ddlLC.SelectedValue));
                var poIds = exportLcInfo.ExportLCDetails.Select(x => x.OrderId).ToList();
                var styleIds = unitOfWork.GenericRepositories<Orders>().Get(x => poIds.Contains(x.Id)).Select(x => x.StyleId).Distinct().ToList();
                var selectedSc = exportLcInfo.SalesContractId?.Split(',').Select(int.Parse).ToList();

                LoadPOByStyleIdsForLCAmandment(string.Join(",", styleIds));
                foreach (ListItem li in cblStyles.Items)
                {
                    if (styleIds.Contains(int.Parse(li.Value)))
                    {
                        li.Selected = true;
                    }
                }
                foreach (ListItem li in cblPO.Items)
                {
                    if (poIds.Contains(int.Parse(li.Value)))
                    {
                        li.Selected = true;
                    }
                }
                if(selectedSc != null)
                {
                    foreach (ListItem li in cblSalesContract.Items)
                    {
                        if (selectedSc.Contains(int.Parse(li.Value)))
                        {
                            li.Selected = true;
                        }
                    }
                }
                

                LCDiv.Visible = false;
                SalesContractDiv.Visible = false;
                AmandmentDiv.Visible = false;
                LCEntryDiv.Visible = false;
                LCAmandmentDiv.Visible = false;

                if (exportLcInfo.LCOrSalesContract == 1)
                {
                    LCDiv.Visible = true;
                    if (exportLcInfo.LCForSalesContract == 1)
                    {
                        SalesContractDiv.Visible = true;
                        LCEntryDiv.Visible = true;
                    }
                    else
                    {
                        AmandmentDiv.Visible = true;
                        if (exportLcInfo.LcAmandment == 1)
                        {
                            LCAmandmentDiv.Visible = true;
                        }
                        else
                        {
                            LCEntryDiv.Visible = true;
                        }
                    }
                }

                var checkChildLC = unitOfWork.GenericRepositories<ExportLCInfo>().IsExist(x => x.ParentLCId == LCId || x.SalesContractId.Contains(LCId+""));
                if (checkChildLC)
                {
                    ddlLCOrSalesContract.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }



        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {

            try
            {

                selectedSCs.Clear();
                selectedPOs.Clear();
                for (int i = 0; i < cblPO.Items.Count; i++)
                {
                    if (cblPO.Items[i].Selected)
                    {
                        selectedPOs.Add(cblPO.Items[i].Value);
                    }
                }
                for (int i = 0; i < cblSalesContract.Items.Count; i++)
                {
                    if (cblSalesContract.Items[i].Selected)
                    {
                        selectedSCs.Add(cblSalesContract.Items[i].Value);
                    }
                }
                //if (selectedPOs.Count > 0)
                //{
                var exportLcInfo = unitOfWork.GenericRepositories<ExportLCInfo>().GetByID(LCId);
                var oldDetails = unitOfWork.GenericRepositories<ExportLCDetails>().Get(x => x.LCId == LCId).ToList();
                foreach (var item in oldDetails)
                {
                    unitOfWork.GenericRepositories<ExportLCDetails>().Delete(item);
                }
                exportLcInfo.LCOrSalesContract = int.Parse(ddlLCOrSalesContract.SelectedValue);
                exportLcInfo.LCForSalesContract = 0;
                if (ddlLCForSalesContract.SelectedValue != "" && exportLcInfo.LCOrSalesContract == 1)
                {
                    exportLcInfo.LCForSalesContract = int.Parse(ddlLCForSalesContract.SelectedValue);
                }

                exportLcInfo.LcAmandment = 0;
                exportLcInfo.AmandmentNumber = "";
                if (ddlLCAmandment.SelectedValue != "" && exportLcInfo.LCOrSalesContract == 1 && exportLcInfo.LCForSalesContract == 0)
                {
                    exportLcInfo.LcAmandment = int.Parse(ddlLCAmandment.SelectedValue);
                    exportLcInfo.AmandmentNumber = tbxAmandmentNumber.Text;
                }


                exportLcInfo.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                exportLcInfo.LCNumber = tbxLCNumber.Text;
                exportLcInfo.LCDate = DateTime.Parse(tbxLCDate.Text);
                exportLcInfo.IssuingBankAndBranch = tbxIssuingBankAndBranch.Text;
                exportLcInfo.AdvisingBankId = int.Parse(ddlAdvisingBank.SelectedValue);
                exportLcInfo.Tenor = int.Parse(ddlTenor.SelectedValue);
                exportLcInfo.LCAmount = Decimal.Parse(tbxLCAmount.Text);
                if (!string.IsNullOrEmpty(tbxLCQuantity.Text))
                {
                    exportLcInfo.LCQuantity = Decimal.Parse(tbxLCQuantity.Text);
                }
                exportLcInfo.LCCommission = Decimal.Parse(tbxLCCommission.Text);
                exportLcInfo.CommercialConcernId = int.Parse(ddlCommercialConcern.SelectedValue);
                exportLcInfo.Status = int.Parse(ddlStatus.SelectedValue);
                exportLcInfo.LastShipmentDate = DateTime.Parse(tbxLastShipmentDate.Text);
                exportLcInfo.ExpiaryDate = DateTime.Parse(tbxExpiaryDate.Text);
                exportLcInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                exportLcInfo.UpdateDate = DateTime.Now;
                exportLcInfo.ParentLCId = null;
                exportLcInfo.SalesContractId = null;
                if (ddlLC.SelectedValue != "" && exportLcInfo.LcAmandment == 1)
                {
                    exportLcInfo.ParentLCId = int.Parse(ddlLC.SelectedValue);
                }
                if (selectedSCs.Count() > 0 && exportLcInfo.LCForSalesContract == 1)
                {
                    exportLcInfo.SalesContractId = string.Join(",", selectedSCs);
                }
                //if (ddlSalesContract.SelectedValue != "" && exportLcInfo.LCForSalesContract == 1)
                //{
                //    exportLcInfo.SalesContractId = int.Parse(ddlSalesContract.SelectedValue);
                //}
                List<ExportLCDetails> lstDetails = new List<ExportLCDetails>();
                foreach (var item in selectedPOs)
                {
                    var detail = new ExportLCDetails()
                    {
                        LCId = exportLcInfo.LCId,
                        OrderId = int.Parse(item),
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };
                    lstDetails.Add(detail);
                }
                exportLcInfo.ExportLCDetails = lstDetails;
                unitOfWork.GenericRepositories<ExportLCInfo>().Update(exportLcInfo);
                unitOfWork.Save();

                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewEditExportLCInfo.aspx');", true);

                //var exportLcInfo = unitOfWork.GenericRepositories<ExportLCInfo>().GetByID(LCId);
                //exportLcInfo.BuyerId = int.Parse(ddlBuyers.SelectedValue);
                //exportLcInfo.LCNumber = tbxLCNumber.Text;
                //exportLcInfo.LCDate = DateTime.Parse(tbxLCDate.Text);
                //exportLcInfo.IssuingBankAndBranch = tbxIssuingBankAndBranch.Text;
                //exportLcInfo.AdvisingBankId = int.Parse(ddlAdvisingBank.SelectedValue);
                //exportLcInfo.Tenor = tbxTenor.Text;
                //if (!string.IsNullOrEmpty(tbxLCQuantity.Text))
                //{
                //    exportLcInfo.LCQuantity = Decimal.Parse(tbxLCQuantity.Text);
                //}
                //else
                //{
                //    exportLcInfo.LCQuantity = 0;
                //}
                //exportLcInfo.LCAmount = Decimal.Parse(tbxLCAmount.Text);
                //exportLcInfo.LCCommission = Decimal.Parse(tbxLCCommission.Text);
                //exportLcInfo.CommercialConcernId = int.Parse(ddlCommercialConcern.SelectedValue);
                //exportLcInfo.Status = int.Parse(ddlStatus.SelectedValue);
                //exportLcInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                //exportLcInfo.UpdateDate = DateTime.Now;
                //unitOfWork.GenericRepositories<ExportLCInfo>().Update(exportLcInfo);
                //unitOfWork.Save();

                //ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                //ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewEditExportLCInfo.aspx');", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }

        protected void btnCheckAllStyles_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in cblStyles.Items)
            {
                li.Selected = true;
            }
        }

        protected void btnUnCheckAllStyles_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in cblStyles.Items)
            {
                li.Selected = false;
            }
        }

        protected void btnShowPO_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < cblStyles.Items.Count; i++)
            {
                if (cblStyles.Items[i].Selected)
                {
                    selectedStyles.Add(cblStyles.Items[i].Value);
                }
            }
            string styles = "";
            if (selectedStyles.Count != 0)
            {
                styles = string.Join(",", selectedStyles);
                LoadPOByStyleIds(styles);
                // btnSubmit.Visible = true;
            }
            else
            {
                //styleDiv.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Please select style.')", true);
            }
        }

        private void LoadPOByStyleIds(string styleIds)
        {
            divPOs.Visible = true;
            cblPO.Items.Clear();
            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetPOByStyles '{styleIds}'");
            if (dt.Rows.Count > 0)
            {
                cblPO.DataValueField = "Id";
                cblPO.DataTextField = "OrderNumber";
                cblPO.DataSource = dt;
                cblPO.DataBind();
                btnCheckAllPO.Visible = true;
                btnUnCheckAllPO.Visible = true;

                lblColorsNotFound.Visible = false;
            }
            else
            {

                btnCheckAllPO.Visible = false;
                btnUnCheckAllPO.Visible = false;
                lblColorsNotFound.Visible = true;
            }
        }

        private void LoadPOByStyleIdsForLCAmandment(string styleIds)
        {
            divPOs.Visible = true;
            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetPOByStylesForLCAmandment '{styleIds}'");
            if (dt.Rows.Count > 0)
            {
                cblPO.DataValueField = "Id";
                cblPO.DataTextField = "OrderNumber";
                cblPO.DataSource = dt;
                cblPO.DataBind();
                btnCheckAllPO.Visible = true;
                btnUnCheckAllPO.Visible = true;

                lblColorsNotFound.Visible = false;
            }
            else
            {
                btnCheckAllPO.Visible = false;
                btnUnCheckAllPO.Visible = false;
                lblColorsNotFound.Visible = true;
            }
        }

        private void LoadStylesByBuyerIds(string buyerIds)
        {
            var dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetStylesByBuyers '{buyerIds}'");
            if (dt.Rows.Count > 0)
            {
                cblStyles.DataValueField = "Id";
                cblStyles.DataTextField = "StyleName";
                cblStyles.DataSource = dt;
                cblStyles.DataBind();
                //styleDiv.Visible = true;
                lblNoStyle.Visible = false;
            }
            else
            {
                lblNoStyle.Visible = true;
            }
        }



        protected void btnCheckAllPO_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in cblPO.Items)
            {
                li.Selected = true;
            }
        }

        protected void btnUnCheckAllPO_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in cblPO.Items)
            {
                li.Selected = false;
            }
        }

        protected void ddlLCOrSalesContract_SelectedIndexChanged(object sender, EventArgs e)
        {
            LCDiv.Visible = false;
            AmandmentDiv.Visible = false;
            SalesContractDiv.Visible = false;
            LCAmandmentDiv.Visible = false;
            LCEntryDiv.Visible = false;
            ddlLCForSalesContract.SelectedIndex = 0;
            ddlLCAmandment.SelectedIndex = 0;
            //ddlSalesContract.SelectedIndex = 0;
            //cblSalesContract.Items.Clear();

            if (ddlLCOrSalesContract.SelectedValue != "")
            {
                if (int.Parse(ddlLCOrSalesContract.SelectedValue) == 1)
                {
                    LCDiv.Visible = true;
                    LCEntryDiv.Visible = true;
                }
                else
                {
                    //SalesContractDiv.Visible = true;
                    //LCEntryDiv.Visible = true;
                    AmandmentDiv.Visible = true;
                }
            }

        }

        protected void ddlLCForSalesContract_SelectedIndexChanged(object sender, EventArgs e)
        {
            AmandmentDiv.Visible = false;
            SalesContractDiv.Visible = false;
            LCAmandmentDiv.Visible = false;
            LCEntryDiv.Visible = false;
            ddlLCAmandment.SelectedIndex = 0;
            //ddlSalesContract.SelectedIndex = 0;

            if (ddlLCForSalesContract.SelectedValue != "")
            {
                if (int.Parse(ddlLCForSalesContract.SelectedValue) == 1)
                {
                    SalesContractDiv.Visible = true;
                    LCEntryDiv.Visible = true;
                }
                else
                {
                    AmandmentDiv.Visible = true;
                }
            }
        }

        protected void ddlLCAmandment_SelectedIndexChanged(object sender, EventArgs e)
        {
            LCEntryDiv.Visible = false;
            LCAmandmentDiv.Visible = false;
            var lcorsc = int.Parse(ddlLCOrSalesContract.SelectedValue);
            if (ddlLCAmandment.SelectedValue != "")
            {
                if (int.Parse(ddlLCAmandment.SelectedValue) == 1)
                {
                    if(lcorsc == 1)
                    {
                        lcAmandment.Visible = true;
                        scAmandment.Visible = false;
                    }
                    else
                    {
                        lcAmandment.Visible = false;
                        scAmandment.Visible = true;
                    }
                    LCAmandmentDiv.Visible = true;
                }
                else
                {
                    LCEntryDiv.Visible = true;
                }
            }
        }

        protected void ddlLC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearFields();
            if (ddlLC.SelectedValue != "")
            {
                var lcInfo = unitOfWork.GenericRepositories<ExportLCInfo>().GetByID(int.Parse(ddlLC.SelectedValue));
                var poIds = lcInfo.ExportLCDetails.Select(x => x.OrderId).ToList();
                var styleIds = unitOfWork.GenericRepositories<Orders>().Get(x => poIds.Contains(x.Id)).Select(x => x.StyleId).Distinct().ToList();
                cblPO.Items.Clear();
                LoadPOByStyleIdsForLCAmandment(string.Join(",", styleIds));
                foreach (ListItem li in cblStyles.Items)
                {
                    if (styleIds.Contains(int.Parse(li.Value)))
                    {
                        li.Selected = true;
                    }
                }
                foreach (ListItem li in cblPO.Items)
                {
                    if (poIds.Contains(int.Parse(li.Value)))
                    {
                        li.Selected = true;
                    }
                }

                tbxLCDate.Text = lcInfo.LCDate.ToString("yyyy-MM-dd");
                tbxLCQuantity.Text = lcInfo.LCQuantity + "";
                tbxLCAmount.Text = lcInfo.LCAmount + "";
                tbxLCCommission.Text = lcInfo.LCCommission + "";
                tbxIssuingBankAndBranch.Text = lcInfo.IssuingBankAndBranch;
                ddlAdvisingBank.SelectedValue = lcInfo.AdvisingBankId + "";
                tbxLastShipmentDate.Text = lcInfo.LastShipmentDate?.ToString("yyyy-MM-dd");
                tbxExpiaryDate.Text = lcInfo.ExpiaryDate?.ToString("yyyy-MM-dd");
                ddlCommercialConcern.SelectedValue = lcInfo.CommercialConcernId + "";
                ddlStatus.SelectedValue = lcInfo.Status + "";
                ddlTenor.SelectedValue = lcInfo.Tenor + "";
                tbxLCNumber.Text = ddlLC.SelectedItem.Text;
            }
            else
            {
                ClearFields();
            }
        }

        protected void ClearFields()
        {
            divPOs.Visible = false;
            foreach (ListItem li in cblStyles.Items)
            {
                li.Selected = false;
            }

            foreach (ListItem li in cblPO.Items)
            {
                li.Selected = false;
            }
           

           

            tbxLCDate.Text = "";
            tbxLCQuantity.Text = "";
            tbxLCAmount.Text = "";
            tbxLCCommission.Text = "";
            tbxIssuingBankAndBranch.Text = "";
            ddlAdvisingBank.SelectedValue = "";
            tbxLastShipmentDate.Text = "";
            tbxExpiaryDate.Text = "";
            ddlCommercialConcern.SelectedValue = "";
            ddlStatus.SelectedValue = "";
            ddlTenor.SelectedValue = "";
            tbxLCNumber.Text = "";
        }

        protected void ddlSalesContract_SelectedIndexChanged(object sender, EventArgs e)
        {
            var lcScNumber = tbxLCNumber.Text;
            ClearFields();

            for (int j = 0; j < cblSalesContract.Items.Count; j++)
            {
                if (cblSalesContract.Items[j].Selected)
                {
                    selectedSCs.Add(cblSalesContract.Items[j].Value);
                }
            }

            if (selectedSCs.Count() > 0)
            {
                var lcInfo = unitOfWork.GenericRepositories<ExportLCInfo>().GetByID(int.Parse(selectedSCs.FirstOrDefault()));
                var poIds = lcInfo.ExportLCDetails.Select(x => x.OrderId).ToList();
                var styleIds = unitOfWork.GenericRepositories<Orders>().Get(x => poIds.Contains(x.Id)).Select(x => x.StyleId).Distinct().ToList();
                cblPO.Items.Clear();
                LoadPOByStyleIdsForLCAmandment(string.Join(",", styleIds));
                foreach (ListItem li in cblStyles.Items)
                {
                    if (styleIds.Contains(int.Parse(li.Value)))
                    {
                        li.Selected = true;
                    }
                }
                foreach (ListItem li in cblPO.Items)
                {
                    if (poIds.Contains(int.Parse(li.Value)))
                    {
                        li.Selected = true;
                    }
                }

                tbxLCDate.Text = lcInfo.LCDate.ToString("yyyy-MM-dd");
                tbxLCQuantity.Text = lcInfo.LCQuantity + "";
                tbxLCAmount.Text = lcInfo.LCAmount + "";
                tbxLCCommission.Text = lcInfo.LCCommission + "";
                tbxIssuingBankAndBranch.Text = lcInfo.IssuingBankAndBranch;
                ddlAdvisingBank.SelectedValue = lcInfo.AdvisingBankId + "";
                tbxLastShipmentDate.Text = lcInfo.LastShipmentDate?.ToString("yyyy-MM-dd");
                tbxExpiaryDate.Text = lcInfo.ExpiaryDate?.ToString("yyyy-MM-dd");
                ddlCommercialConcern.SelectedValue = lcInfo.CommercialConcernId + "";
                ddlStatus.SelectedValue = lcInfo.Status + "";
                ddlTenor.SelectedValue = lcInfo.Tenor + "";
                tbxLCNumber.Text = lcScNumber;
                // tbxLCNumber.Text = ddlSalesContract.SelectedItem.Text;
            }
            else
            {
                ClearFields();
            }
        }

        protected void ddlSc_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearFields();
            if (ddlSc.SelectedValue != "")
            {
                var lcInfo = unitOfWork.GenericRepositories<ExportLCInfo>().GetByID(int.Parse(ddlSc.SelectedValue));
                var poIds = lcInfo.ExportLCDetails.Select(x => x.OrderId).ToList();
                var styleIds = unitOfWork.GenericRepositories<Orders>().Get(x => poIds.Contains(x.Id)).Select(x => x.StyleId).Distinct().ToList();
                cblPO.Items.Clear();
                LoadPOByStyleIdsForLCAmandment(string.Join(",", styleIds));
                foreach (ListItem li in cblStyles.Items)
                {
                    if (styleIds.Contains(int.Parse(li.Value)))
                    {
                        li.Selected = true;
                    }
                }
                foreach (ListItem li in cblPO.Items)
                {
                    if (poIds.Contains(int.Parse(li.Value)))
                    {
                        li.Selected = true;
                    }
                }

                tbxLCDate.Text = lcInfo.LCDate.ToString("yyyy-MM-dd");
                tbxLCQuantity.Text = lcInfo.LCQuantity + "";
                tbxLCAmount.Text = lcInfo.LCAmount + "";
                tbxLCCommission.Text = lcInfo.LCCommission + "";
                tbxIssuingBankAndBranch.Text = lcInfo.IssuingBankAndBranch;
                ddlAdvisingBank.SelectedValue = lcInfo.AdvisingBankId + "";
                tbxLastShipmentDate.Text = lcInfo.LastShipmentDate?.ToString("yyyy-MM-dd");
                tbxExpiaryDate.Text = lcInfo.ExpiaryDate?.ToString("yyyy-MM-dd");
                ddlCommercialConcern.SelectedValue = lcInfo.CommercialConcernId + "";
                ddlStatus.SelectedValue = lcInfo.Status + "";
                ddlTenor.SelectedValue = lcInfo.Tenor + "";
                tbxLCNumber.Text = ddlSc.SelectedItem.Text;
            }
            else
            {
                ClearFields();
            }
        }
    }
}


