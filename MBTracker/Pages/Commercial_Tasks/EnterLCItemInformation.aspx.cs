﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Commercial_Tasks;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Commercial_Tasks
{
    public partial class EnterLCItemInformation : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        CommercialManager commercialManager = new CommercialManager();

        UnitOfWork unitOfWork = new UnitOfWork();
        decimal totalGoods = 0;
        decimal grandTotalAmount = 0;
        int initialLoadForUpdate = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (Request.QueryString["LCId"] != null)
                {
                    LCId = int.Parse(Tools.UrlDecode(Request.QueryString["LCId"]));
                    initialLoadForUpdate = LCId;
                    actionTitle.Text = "Udpate LC Information";
                    lnkbtnSaveEntries.Visible = true;
                    lnkbtnUpdateEntries.Visible = false;



                    CommonMethods.LoadAllLCDropdown(ddlApplicantLCs, 1, 0);
                    CommonMethods.LoadAllPIDropdownNotExistInLC(ddlApplicantPINotExistInLC, 1, 0);

                    LoadPiInfoByLC(LCId);
                }
                else
                {

                    //CommonMethods.LoadApplicantBanksDropdown(ddlApplicantBanks, 1, 0);
                    //CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                    //CommonMethods.LoadYarnAndAccessoriesSupplierDropdown(ddlYarnAndAccssSuppliers, 1, 0);
                    CommonMethods.LoadAllLCDropdown(ddlApplicantLCs, 1, 0);
                    CommonMethods.LoadAllPIDropdownNotExistInLC(ddlApplicantPINotExistInLC, 1, 0);

                }
            }

        }

        private bool EnableDisableLCPurpose()
        {


            var isExist = false;

            var pageName = "LCPurposeSection";
            var controlType = 4;
            var buttonNumberOnpage = 1;

            string sql = $"Exec usp_GetRoleIdsForEditDeleteButton '{pageName}', {controlType}, {buttonNumberOnpage}";

            UnitOfWork unitOfWork = new UnitOfWork();
            var dt = unitOfWork.GetDataTableFromSql(sql);
            var currentUserRoleId = CommonMethods.SessionInfo.RoleId;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (int.Parse(dt.Rows[i][0].ToString()) == currentUserRoleId)
                {
                    isExist = true;
                    break;
                }
            }

            return isExist;
        }

        int LCId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["lcId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["lcId"] = value; }
        }

        bool flag
        {
            get
            {
                try
                {
                    return Convert.ToBoolean(ViewState["flag"]);
                }
                catch
                {
                    return false;
                }
            }
            set { ViewState["flag"] = value; }
        }


        //protected void ddlYarnAndAccssSuppliers_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //if (((DropDownList)sender).ID == "ddlBuyers")
        //{
        //    var buyerId = int.Parse(ddlBuyers.SelectedValue == "" ? "0" : ddlBuyers.SelectedValue);
        //    LoadExportLC(buyerId);
        //}

        //if (ddlYarnAndAccssSuppliers.SelectedValue == "")
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a Beneficiary.')", true);
        //}

        //else if (ddlBuyers.SelectedValue == "")
        //{
        //    if (flag)
        //    {
        //        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a Buyer.')", true);
        //    }
        //    rptPISummary.Visible = false;
        //    divPIAndOtherInfo.Visible = false;
        //}
        //else
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (ddlApplicantPINotExistInLC.SelectedValue != "")
            {
                try
                {
                    flag = true;
                    //var buyerId = ddlBuyers.SelectedValue;
                   
                    string sql = $"Exec usp_GetPIInfoByBeneficiaryIdNotAlreadyInLCV3 '{int.Parse(ddlApplicantPINotExistInLC.SelectedValue.ToString())}'";

                    var dt = unitOfWork.GetDataTableFromSql(sql);

                    if (dt.Rows.Count > 0)
                    {
                        rptPISummary.Visible = true;
                        divPIAndOtherInfo.Visible = true;

                        rptPISummary.DataSource = dt;
                        rptPISummary.DataBind();
                        lblNoPIFound.Visible = false;
                        lblLabelMessage.Visible = true;

                       // lblLCAmount.Text = CalculateTotal().ToString();

                      
                    }
                    else
                    {
                        rptPISummary.Visible = false;
                        divPIAndOtherInfo.Visible = false;

                        lblNoPIFound.Visible = true;
                        lblLabelMessage.Visible = false;
                    }

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);
            }

        }

        protected void LoadExportLC(int buyerId)
        {
            if (buyerId != 0)
            {
                var eLcInfo = unitOfWork.GenericRepositories<ExportLCInfo>().Get(x => x.Status != 1 && x.BuyerId == buyerId).ToList();
                ddlExportLC.DataSource = eLcInfo;
                ddlExportLC.DataTextField = "LCNumber";
                ddlExportLC.DataValueField = "LCId";
                ddlExportLC.DataBind();
                ddlExportLC.Items.Insert(0, new ListItem("---Select---", string.Empty));
                ddlExportLC.SelectedIndex = 0;
            }
            else
            {
                ddlExportLC.Items.Clear();
            }
        }

        protected void rptPISummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                totalGoods = 0;
                grandTotalAmount = 0;

                string PIId = DataBinder.Eval(e.Item.DataItem, "PIId").ToString();

                Repeater rptPIItemInfo = (Repeater)e.Item.FindControl("rptPIItemInfo");
                Label lblNoItemFound = (Label)e.Item.FindControl("lblNoItemFound");

                string sql2 = $"Exec usp_GetPIItemsByPIId '{PIId}'";
                var dt2 = unitOfWork.GetDataTableFromSql(sql2);

                if (dt2.Rows.Count > 0)
                {
                    lblNoItemFound.Visible = false;

                    rptPIItemInfo.DataSource = dt2;
                    rptPIItemInfo.DataBind();

                }
                else
                {
                    lblNoItemFound.Visible = true;

                }

            }
        }

        protected void rptPIItemInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                var qty = DataBinder.Eval(e.Item.DataItem, "Quantity").ToString();
                var totalAmount = DataBinder.Eval(e.Item.DataItem, "TotalAmount").ToString();

                totalGoods += decimal.Parse(qty);
                grandTotalAmount += decimal.Parse(totalAmount);


            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {

                Label lblTotalQuantity = (Label)e.Item.FindControl("lblTotalQuantity");
                Label lblGrandTotalAmount = (Label)e.Item.FindControl("lblGrandTotalAmount");


                lblTotalQuantity.Text = totalGoods.ToString();
                lblGrandTotalAmount.Text = grandTotalAmount.ToString();

            }

        }

        private void LoadPiInfoByLC(int LCId)
        {

            //pnlDetails.Visible = false;
            dataDiv.Visible = false;

            try
            {

                string sql = "";
                DataTable dt;
                


              if (LCId != 0)
                {
                    sql = $"Exec usp_GetLCInfoByLCId '{LCId}'";
                    dt = unitOfWork.GetDataTableFromSql(sql);
                    if (dt.Rows.Count > 0)
                    {
                        rptLCSummary.Visible = true;
                        dataDiv.Visible = true;
                        //dt = dt.CheckEditDeletePermission(CommonMethods.SessionInfo.RoleId, "ViewEditLCInformation");
                        dt = dt.CheckEditDeletePermission(CommonMethods.SessionInfo.RoleId, "EnterLCItemInformation");
                        rptLCSummary.DataSource = dt;
                        rptLCSummary.DataBind();
                        //lblNoDataFound.Visible = false;
                        lblLabelMessage.Visible = true;
                    }
                    else
                    {
                        rptLCSummary.Visible = false;
                        dataDiv.Visible = false;

                        //lblNoDataFound.Visible = true;
                        lblLabelMessage.Visible = false;
                    }
                }
               
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }


        }

        //protected void lnkbtnCalculateLCTotal_Click(object sender, EventArgs e)
        //{

        //    lblLCAmount.Text = CalculateTotal().ToString();

        //}

        private decimal CalculateTotal()
        {
            decimal totalLCAmount = 0;

            for (int i = 0; i < this.rptPISummary.Items.Count; i++)
            {

                CheckBox cbxPI = (CheckBox)this.rptPISummary.Items[i].FindControl("cbxPI");
                if (cbxPI.Checked)
                {
                    Repeater rptPIItemInfo = (Repeater)this.rptPISummary.Items[i].FindControl("rptPIItemInfo");

                    for (int j = 0; j < rptPIItemInfo.Items.Count; j++)
                    {
                        Label lblTotalAmount = (Label)rptPIItemInfo.Items[j].FindControl("lblTotalAmount");
                        if (lblTotalAmount.Text != "")
                        {
                            totalLCAmount += decimal.Parse(lblTotalAmount.Text);
                        }
                    }

                }

            }


            return totalLCAmount;

        }


        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {
            if (CheckPISelected() == 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select at least one PI.')", true);
            }
            else if (CheckPISelected() == 2)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cannot select PIs from different beneficiaries.')", true);
            }
            else
            {
                try
                {
                   
                    List<LCPIs> listLCPIItems = new List<LCPIs>();

                    for (int rowIndex = 0; rowIndex < rptPISummary.Items.Count; rowIndex++)
                    {

                        CheckBox cbxPI = (CheckBox)this.rptPISummary.Items[rowIndex].FindControl("cbxPI");

                        if (cbxPI.Checked)
                        {
                            Label rptPIItemInfo = (Label)this.rptPISummary.Items[rowIndex].FindControl("lblPIId");

                            var lcPI = new LCPIs()
                            {
                                LCId = int.Parse(ddlApplicantLCs.SelectedValue.ToString()),
                                PIId = int.Parse(rptPIItemInfo.Text),
                                CreateDate = DateTime.Now,
                                CreatedBy = CommonMethods.SessionInfo.UserName
                            };

                            listLCPIItems.Add(lcPI);
                        }

                    }

                    foreach (var item in listLCPIItems)
                    {
                        unitOfWork.GenericRepositories<LCPIs>().Insert(item);
                    }

                    unitOfWork.Save();

                    
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                    // ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('EnterLCItemInformation.aspx');", true);
                    Response.Redirect("EnterLCItemInformation.aspx?LCId=" + Tools.UrlEncode(ddlApplicantLCs.SelectedValue.ToString()));

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }

        }

        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {

           
            //else if (ddlYarnAndAccssSuppliers.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a beneficiary.')", true);
            //}
            //else if (ddlApplicantBanks.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select advising bank.')", true);
            //}
            //else if (ddlBuyers.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            //}
            if (CheckPISelected() == 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select at least one PI.')", true);
            }
            else if (CheckPISelected() == 2)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('You cannot select PIs from different beneficiaries.')", true);
            }
           
            else
            {
                try
                {

                    var lcInfo = unitOfWork.GenericRepositories<LCInfoBackToBack>().GetByID(LCId);

                   
                    lcInfo.LCAmount = CalculateTotal();
                    //lcInfo.BeneficiaryId = int.Parse(ddlYarnAndAccssSuppliers.SelectedValue);

                    //lcInfo.ApplicantBankId = int.Parse(ddlApplicantBanks.SelectedValue);
                  

                 
                   

                    int LCPurpose = 0;
                   

                    lcInfo.LCPurpose = LCPurpose;
                    if (ddlExportLC.SelectedValue != "")
                    {
                        lcInfo.ExportLCId = int.Parse(ddlExportLC.SelectedValue);
                    }

                   
                    lcInfo.UpdateDate = DateTime.Now;
                    lcInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;

                    unitOfWork.GenericRepositories<LCInfoBackToBack>().Update(lcInfo);
                    unitOfWork.Save();


                   
                    UpdateLCPIs(LCId);

                    unitOfWork.Save();

                   
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewEditLCInformation.aspx');", true);

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }


        }


        private void UpdateLCPIs(int lcId)
        {

            var lcPIs = unitOfWork.GenericRepositories<LCPIs>().Get(x => x.LCId == lcId);

            foreach (var lcPIItem in lcPIs)
            {
                unitOfWork.GenericRepositories<LCPIs>().Delete(lcPIItem);
            }


            List<LCPIs> listLCPIs = new List<LCPIs>();


            for (int rowIndex = 0; rowIndex < rptPISummary.Items.Count; rowIndex++)
            {
                CheckBox cbxPI = (CheckBox)this.rptPISummary.Items[rowIndex].FindControl("cbxPI");
                Label lblPIId = (Label)this.rptPISummary.Items[rowIndex].FindControl("lblPIId");

                if (cbxPI.Checked)
                {
                    var lcPI = new LCPIs()
                    {
                        LCId = lcId,
                        PIId = int.Parse(lblPIId.Text),
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };

                    listLCPIs.Add(lcPI);
                }

            }

            foreach (var item in listLCPIs)
            {
                unitOfWork.GenericRepositories<LCPIs>().Insert(item);
            }

        }

        private int CheckPISelected()
        {
            int hasChekced = 0;

            for (int rowIndex = 0; rowIndex < rptPISummary.Items.Count; rowIndex++)
            {

                CheckBox cbxPI1 = (CheckBox)this.rptPISummary.Items[rowIndex].FindControl("cbxPI");
                Label lblBeneficiaryName1 = (Label)rptPISummary.Items[rowIndex].FindControl("lblBeneficiaryName");
                Label lblPIId1 = (Label)rptPISummary.Items[rowIndex].FindControl("lblPIId");

                if (cbxPI1.Checked)
                {
                    hasChekced = 1;

                    for (int rowIndex2 = 0; rowIndex2 < rptPISummary.Items.Count; rowIndex2++)
                    {
                        CheckBox cbxPI2 = (CheckBox)this.rptPISummary.Items[rowIndex2].FindControl("cbxPI");
                        Label lblBeneficiaryName2 = (Label)rptPISummary.Items[rowIndex2].FindControl("lblBeneficiaryName");
                        Label lblPIId2 = (Label)rptPISummary.Items[rowIndex2].FindControl("lblPIId");


                        if (cbxPI2.Checked && lblBeneficiaryName1.Text != lblBeneficiaryName2.Text && lblPIId1.Text != lblPIId2.Text)
                        {
                            hasChekced = 2;
                            break;
                        }
                    }

                }
            }

            return hasChekced;
        }

        public DataTable LCStyle()
        {
            DataTable dt = new DataTable("LCStyles");
            dt.Columns.Add("LCId", typeof(int));
            dt.Columns.Add("StyleId", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }

        //private void LoadLCInfo()
        //{

        //    try
        //    {
        //        string sql = $"Exec usp_GetLCInfoDetailsById '{LCId}'";

        //        var dtLCInfo = unitOfWork.GetDataTableFromSql(sql);

        //        tbxLCNumber.Text = dtLCInfo.Rows[0]["LCNumber"].ToString();
        //        tbxLCDate.Text = DateTime.Parse(dtLCInfo.Rows[0]["LCDate"].ToString()).ToString("yyyy-MM-dd");


        //        tbxUDDate.Text = DateTime.Parse(dtLCInfo.Rows[0]["UDDate"].ToString()).ToString("yyyy-MM-dd");

        //        if (!string.IsNullOrEmpty(dtLCInfo.Rows[0]["ETDDate"].ToString()))
        //        {
        //            tbxETDDate.Text = DateTime.Parse(dtLCInfo.Rows[0]["ETDDate"].ToString()).ToString("yyyy-MM-dd");
        //        }

        //        if (!string.IsNullOrEmpty(dtLCInfo.Rows[0]["ETADate"].ToString()))
        //        {
        //            tbxETADate.Text = DateTime.Parse(dtLCInfo.Rows[0]["ETADate"].ToString()).ToString("yyyy-MM-dd");
        //        }

        //        if (!string.IsNullOrEmpty(dtLCInfo.Rows[0]["MaturityDate"].ToString()))
        //        {
        //            tbxMaturityDate.Text = DateTime.Parse(dtLCInfo.Rows[0]["MaturityDate"].ToString()).ToString("yyyy-MM-dd");
        //        }



        //       // CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
        //       // ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, int.Parse((string.IsNullOrEmpty(dtLCInfo.Rows[0]["BuyerId"].ToString()) ? "0" : dtLCInfo.Rows[0]["BuyerId"].ToString())));

        //        int buyerId = 0;
        //        buyerId = int.Parse(dtLCInfo.Rows[0]["BuyerId"].ToString() == "" ? "0" : dtLCInfo.Rows[0]["BuyerId"].ToString());
        //        var elcId = dtLCInfo.Rows[0]["ExportLCId"].ToString();
        //        if (!string.IsNullOrEmpty(elcId))
        //        {
        //            LoadExportLC(buyerId);
        //            ddlExportLC.SelectedValue = elcId;
        //        }

        //       // CommonMethods.LoadYarnAndAccessoriesSupplierDropdown(ddlYarnAndAccssSuppliers, 1, 0);
        //       // ddlYarnAndAccssSuppliers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlYarnAndAccssSuppliers, int.Parse((string.IsNullOrEmpty(dtLCInfo.Rows[0]["BeneficiaryId"].ToString()) ? "0" : dtLCInfo.Rows[0]["BeneficiaryId"].ToString())));


        //        //CommonMethods.LoadApplicantBanksDropdown(ddlApplicantBanks, 1, 0);
        //        //ddlApplicantBanks.SelectedIndex = CommonMethods.MatchDropDownItem(ddlApplicantBanks, int.Parse((string.IsNullOrEmpty(dtLCInfo.Rows[0]["ApplicantBankId"].ToString()) ? "0" : dtLCInfo.Rows[0]["ApplicantBankId"].ToString())));


        //        CommonMethods.LoadLCBToBStatusDropdown(ddlStatus, 1, 0);
        //        ddlStatus.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStatus, int.Parse((string.IsNullOrEmpty(dtLCInfo.Rows[0]["LCStatusId"].ToString()) ? "0" : dtLCInfo.Rows[0]["LCStatusId"].ToString())));



        //        CommonMethods.LoadLCTypeDropdown(ddlLCType, 1, 0);
        //        ddlLCType.SelectedIndex = CommonMethods.MatchDropDownItem(ddlLCType, int.Parse((string.IsNullOrEmpty(dtLCInfo.Rows[0]["LCTypeId"].ToString()) ? "0" : dtLCInfo.Rows[0]["LCTypeId"].ToString())));




        //        CommonMethods.LoadLCPurposeDropdown(ddlLCPurpose, 1, 0);
        //        ddlLCPurpose.SelectedIndex = CommonMethods.MatchDropDownItem(ddlLCPurpose, int.Parse((string.IsNullOrEmpty(dtLCInfo.Rows[0]["LCPurposeId"].ToString()) ? "0" : dtLCInfo.Rows[0]["LCPurposeId"].ToString())));



        //        CommonMethods.LoadCommercialConceredDropDown(ddlCommercialConcerened, 1, 0);
        //        ddlCommercialConcerened.SelectedIndex = CommonMethods.MatchDropDownItem(ddlCommercialConcerened, int.Parse((string.IsNullOrEmpty(dtLCInfo.Rows[0]["CommercialConceredId"].ToString()) ? "0" : dtLCInfo.Rows[0]["CommercialConceredId"].ToString())));

        //        ddlShipmentMode.SelectedIndex = CommonMethods.MatchDropDownItem(ddlShipmentMode, int.Parse((string.IsNullOrEmpty(dtLCInfo.Rows[0]["ShipmentModeId"].ToString()) ? "0" : dtLCInfo.Rows[0]["ShipmentModeId"].ToString())));

        //        ddlUDStatus.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUDStatus, int.Parse((string.IsNullOrEmpty(dtLCInfo.Rows[0]["UDStatus"].ToString()) ? "0" : dtLCInfo.Rows[0]["UDStatus"].ToString())));



        //        string sql2 = $"Exec usp_GetPIInfoByBeneficiaryIdAlreadyNotInOtherLC '{int.Parse(ddlYarnAndAccssSuppliers.SelectedValue)}','{LCId}','{buyerId}'";
        //        var dt2 = unitOfWork.GetDataTableFromSql(sql2);

        //        if (dt2.Rows.Count > 0)
        //        {
        //            rptPISummary.Visible = true;
        //            divPIAndOtherInfo.Visible = true;

        //            rptPISummary.DataSource = dt2;
        //            rptPISummary.DataBind();
        //            lblNoPIFound.Visible = false;
        //            lblLabelMessage.Visible = true;
        //        }
        //        else
        //        {
        //            rptPISummary.Visible = false;
        //            divPIAndOtherInfo.Visible = false;

        //            lblNoPIFound.Visible = true;
        //            lblLabelMessage.Visible = false;
        //        }

        //        lblLCAmount.Text = dtLCInfo.Rows[0]["LCAmount"].ToString();
        //        tbxOtherInfo.Text = dtLCInfo.Rows[0]["OtherInfo"].ToString();


        //        var selectedPIs = unitOfWork.GenericRepositories<LCPIs>().Get(x => x.LCId == LCId).ToList();


        //        for (int rowIndex = 0; rowIndex < rptPISummary.Items.Count; rowIndex++)
        //        {
        //            CheckBox cbxPI = (CheckBox)this.rptPISummary.Items[rowIndex].FindControl("cbxPI");
        //            Label lblPIId = (Label)this.rptPISummary.Items[rowIndex].FindControl("lblPIId");

        //            cbxPI.Checked = selectedPIs.Exists(x => x.PIId == Int32.Parse(lblPIId.Text));
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
        //    }
        //}

       


    }
}