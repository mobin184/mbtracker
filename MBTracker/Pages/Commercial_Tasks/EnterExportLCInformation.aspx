﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="EnterExportLCInformation.aspx.cs" Inherits="MBTracker.Pages.Commercial_Tasks.EnterExportLCInformation" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <asp:Panel ID="pnlNonEdit1" runat="server">
        <style>
            .form-horizontal .control-label {
                float: left;
                width: 175px;
                padding-top: 5px;
                text-align: right;
            }


            #cpMain_cblStyles td {
                /*display: inline-flex;*/
                margin-right: 20px;
                padding: 0px 5px;
            }

                #cpMain_cblStyles td > label {
                    display: inline !important;
                    line-height: 14px;
                    margin-left: 5px;
                    font-size: 11px;
                }

            #cpMain_cblPO td {
                /*display: inline-flex;*/
                margin-right: 20px;
                padding: 0px 5px;
            }

                #cpMain_cblPO td > label {
                    display: inline !important;
                    line-height: 14px;
                    margin-left: 5px;
                    font-size: 11px;
                }

            #cpMain_cblSalesContract td {
                /*display: inline-flex;*/
                margin-right: 20px;
                padding: 0px 5px;
            }

                #cpMain_cblSalesContract td > label {
                    display: inline !important;
                    line-height: 14px;
                    margin-left: 5px;
                    font-size: 11px;
                }
        </style>
        <script>


            <%--function YourChangeFun(ddl) {
                $('#<%= ddlSalesContract.ClientID %>') JSON.stringify($('#<%= ddlSalesContract.ClientID %>').val()));
            }--%>

            function myFunction(tableName, searchbox) {
                var input, filter, table, tr, td, i, txtValue;
                input = document.getElementById(searchbox);
                filter = input.value.toUpperCase();
                table = document.getElementById(tableName);
                //tr = table.getElementsByTagName("tr");
                /* for (i = 0; i < tr.length; i++) {*/
                //td = tr[i].getElementsByTagName("td")[0];
                td = table.getElementsByTagName("td");
                //console.log(td);
                for (i = 0; i < td.length; i++) {
                    var cell = td[i];
                    //console.log(cell);
                    if (cell) {
                        txtValue = cell.textContent || cell.innerText;
                        txtValue = txtValue.trim();

                        if (txtValue.toUpperCase().indexOf(filter) > -1) {
                            cell.style.display = "";
                            //console.log(txtValue, filter, '');
                        } else {
                            cell.style.display = "none";
                            //console.log(txtValue, filter, 'none');
                        }
                    }
                }
            }
        </script>

        <div class="row-fluid">

            <div class="span6">
                <div class="widget">
                    <div class="widget-header">
                        <div class="title">
                            <span class="fs1" aria-hidden="true" data-icon=""></span>
                            <asp:Label runat="server" ID="actionTitle" Text="Enter Export LC/Sales Contract Information:"></asp:Label>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="form-horizontal">
                            <div class="pull-right" style="text-align: center; font-size: 12px; line-height: 20px; padding-bottom: 5px">
                                <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                            </div>
                            <div class="control-group">
                                <div class="col-md-12 col-sm-12">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                </div>
                            </div>

                            <div class="control-group" style="padding-bottom: 30px">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <br />
    </asp:Panel>


    <div class="row-fluid">

        <div id="divPIOtherInfo2" runat="server" visible="false">

            <div class="span12">
                <div class="widget">

                    <div class="widget-body">
                        <div class="row" style="padding-left: 15px; padding-right: 15px;">
                            <div class="col-md-12" style="padding-bottom: 30px;">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                        <div class="controls controls-row" style="padding-top: 10px; display: grid">
                                            <input type="text" id="tbxSearchStyle" class="col-md-2" onkeyup="myFunction('cpMain_cblStyles','tbxSearchStyle')" placeholder="search..." />
                                            <asp:CheckBoxList ID="cblStyles" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="4" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                            <asp:Label ID="lblNoStyle" runat="server" Visible="false" Text="<br/>No data found." BackColor="#ffff00"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="controls controls-row">
                                        <asp:Button ID="btnCheckAllStyles" runat="server" Text="Select All Styles" OnClick="btnCheckAllStyles_Click" />
                                        <asp:Button ID="btnUnCheckAllStyles" runat="server" Text="Unselect All Styles" OnClick="btnUnCheckAllStyles_Click" />
                                        <asp:Button ID="btnShowPO" Style="min-height: 35px; border: 1px solid black;" runat="server" class="btn btn-info" Text="Show PO" OnClick="btnShowPO_Click" />
                                    </div>
                                    <div id="divPOs" runat="server" visible="false">
                                        <div class="control-group" style="margin-top: 25px">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="Label6" runat="server" Text="Select PO:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                            <div class="controls controls-row" style="padding-top: 10px; display: grid">
                                                <input type="text" id="tbxSearchPO" class="col-md-2" onkeyup="myFunction('cpMain_cblPO','tbxSearchPO')" placeholder="search..." />
                                                <asp:CheckBoxList ID="cblPO" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="5" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                                <asp:Label ID="lblColorsNotFound" runat="server" Visible="false" Text="<br/>No data found." BackColor="#ffff00"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="controls controls-row">
                                            <asp:Button ID="btnCheckAllPO" runat="server" Text="Select All PO" OnClick="btnCheckAllPO_Click" />
                                            <asp:Button ID="btnUnCheckAllPO" runat="server" Text="Unselect All PO" OnClick="btnUnCheckAllPO_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="controls controls-row" style="text-align: left; line-height: 40px">
                                    <span style="font-weight: 700; padding-left: 30px">LC/Sales Contract Information:</span>
                                </div>
                            </div>

                            <asp:Panel ID="pnlNonEdit2" runat="server">
                                <div class="col-md-6">
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="Label7" runat="server" Text="LC or Sales Contract? "></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlLCOrSalesContract" runat="server" CssClass="form-control" Width="70%" AutoPostBack="true" OnSelectedIndexChanged="ddlLCOrSalesContract_SelectedIndexChanged">
                                                    <asp:ListItem Text="---Select---" Value="" />
                                                    <asp:ListItem Text="LC" Value="1" />
                                                    <asp:ListItem Text="Sales Contract" Value="2" />
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlLCOrSalesContract">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Select type.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div runat="server" id="LCDiv" visible="false">
                                            <div class="control-group">
                                                <label for="inputEmail3" class="control-label">
                                                    <asp:Label ID="Label8" runat="server" Text="Is LC for Sales Contract? "></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                <div class="controls controls-row">
                                                    <asp:DropDownList ID="ddlLCForSalesContract" runat="server" CssClass="form-control" Width="70%" AutoPostBack="true" OnSelectedIndexChanged="ddlLCForSalesContract_SelectedIndexChanged">
                                                        <asp:ListItem Text="---Select---" Value="" />
                                                        <asp:ListItem Text="Yes" Value="1" />
                                                        <asp:ListItem Text="No" Value="0" />
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="ddlLCForSalesContract">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Select is LC for sales contract.</span>
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div runat="server" id="SalesContractDiv" visible="false">
                                            <div class="control-group">
                                                <label for="inputEmail3" class="control-label">
                                                    <asp:Label ID="Label10" runat="server" Text="Select Sales Contract"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                <div class="controls controls-row" style="display:grid">
                                                    <%--     <asp:DropDownList ID="ddlSalesContract" SelectionMode="Multiple" runat="server" CssClass="form-control" Width="70%" onchange="YourChangeFun(this)" multiple="multiple"  >
                                                    </asp:DropDownList>--%>
                                                    <input type="text" id="tbxSearchSC" style="margin-bottom:5px;" class="col-md-6" onkeyup="myFunction('cpMain_cblSalesContract','tbxSearchSC')" placeholder="search..." />
                                                    <asp:CheckBoxList ID="cblSalesContract" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="2" RepeatDirection="Vertical" RepeatLayout="Table" AutoPostBack="true" OnSelectedIndexChanged="ddlSalesContract_SelectedIndexChanged" TextAlign="Right"></asp:CheckBoxList>
                                                    <%--<asp:DropDownList ID="ddlSalesContract" runat="server" CssClass="form-control ignore-select2" Width="70%" SelectionMode="Multiple"></asp:DropDownList>--%>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator16" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="cblSalesContract">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Select is sales contract.</span>
                                                    </asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div runat="server" id="AmandmentDiv" visible="false">
                                            <div class="control-group">
                                                <label for="inputEmail3" class="control-label">
                                                    <asp:Label ID="Label9" runat="server" Text="Is LC/SC Amandment? "></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                <div class="controls controls-row">
                                                    <asp:DropDownList ID="ddlLCAmandment" runat="server" CssClass="form-control" Width="70%" AutoPostBack="true" OnSelectedIndexChanged="ddlLCAmandment_SelectedIndexChanged">
                                                        <asp:ListItem Text="---Select---" Value="" />
                                                        <asp:ListItem Text="Yes" Value="1" />
                                                        <asp:ListItem Text="No" Value="0" />
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="ddlLCAmandment">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Select is LC/Sales contrcact amandment.</span>
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div runat="server" id="LCEntryDiv" visible="true">
                                            <div class="control-group">
                                                <label for="inputEmail3" class="control-label">
                                                    <asp:Label ID="lblBeneficiaryName" runat="server" Text="LC/Sales Contract Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                <div class="controls controls-row">
                                                    <asp:TextBox ID="tbxLCNumber" runat="server" placeholder="Enter LC/Sales Contract number" CssClass="form-control" Width="70%"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbxLCNumber">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Enter LC/Sales Contract number.</span>
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div runat="server" id="LCAmandmentDiv" visible="false">
                                            <div class="control-group" id="lcAmandment" runat="server" visible="false">
                                                <label for="inputEmail3" class="control-label">
                                                    <asp:Label ID="Label11" runat="server" Text="Select LC Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                <div class="controls controls-row">
                                                    <asp:DropDownList ID="ddlLC" runat="server" CssClass="form-control" Width="70%" AutoPostBack="true" OnSelectedIndexChanged="ddlLC_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="ddlLC">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Select LC number.</span>
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="control-group" id="scAmandment" runat="server" visible="false">
                                                <label for="inputEmail3" class="control-label">
                                                    <asp:Label ID="Label13" runat="server" Text="Select Sales Contract:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                <div class="controls controls-row">
                                                    <asp:DropDownList ID="ddlSc" runat="server" CssClass="form-control" Width="70%" AutoPostBack="true" OnSelectedIndexChanged="ddlSc_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="ddlSC">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Select Sales Contract.</span>
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label for="inputEmail3" class="control-label">
                                                    <asp:Label ID="Label12" runat="server" Text="Amandment Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                <div class="controls controls-row">
                                                    <asp:TextBox ID="tbxAmandmentNumber" runat="server" CssClass="form-control" Width="70%"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="tbxAmandmentNumber">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Enter amandment number.</span>
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="Label1" runat="server" Text="LC/Sales Contract Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxLCDate" runat="server" CssClass="form-control" Width="70%" TextMode="Date"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxLCDate">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Enter LC/Sales Contract date.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="Label3" runat="server" Text="LC/Sales Contract Quantity:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxLCQuantity" runat="server" TextMode="Number" min="0" placeholder="Enter LC/Sales Contract Quantity" CssClass="form-control" Width="70%"></asp:TextBox>
                                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator11" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxLCQuantity">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Enter LC/Sales Contract quantity.</span>
                                                </asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblBeneficiaryPhone" runat="server" Text="LC/Sales Contract Amount:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxLCAmount" runat="server" TextMode="Number" min="0" placeholder="Enter LC/Sales Contract Amount" CssClass="form-control" Width="70%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxLCAmount">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Enter LC/Sales Contract amount.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblBeneficiaryBank" runat="server" Text="LC/Sales Contract Commission:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxLCCommission" runat="server" TextMode="Number" min="0" placeholder="Enter LC commission" CssClass="form-control" Width="70%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxLCCommission">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Enter LC/Sales Contract commission.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblShipment" runat="server" Text="Tenor:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlTenor" runat="server" CssClass="form-control" Width="70%">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlAdvisingBank">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Select lien bank.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <%--<div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblShipment" runat="server" Text="Tenor:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxTenor" runat="server" TextMode="Number" min="1" placeholder="Enter tenor" CssClass="form-control" Width="70%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxTenor">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Enter LC/Sales Contract tenor.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>--%>
                                    </div>
                                </div>
                            </asp:Panel>

                            <div class="col-md-6">
                                <div class="form-horizontal">

                                    <asp:Panel ID="pnlNonEdit3" runat="server">
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblItemOrderDate" runat="server" Text="Issuing Bank & Branch:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxIssuingBankAndBranch" runat="server" placeholder="Issuing Bank & Branch" CssClass="form-control" Width="70%" Style="height: 80px" TextMode="MultiLine"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxIssuingBankAndBranch">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Enter issuing bank & branch.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblPaymentMode" runat="server" Text="Lien Bank:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlAdvisingBank" runat="server" CssClass="form-control" Width="70%">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlAdvisingBank">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Select lien bank.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="Label4" runat="server" Text="Last Shipment Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxLastShipmentDate" TextMode="Date" runat="server" CssClass="form-control" Width="70%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxLastShipmentDate">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Select last shipment date.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="Label5" runat="server" Text="Expiary Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxExpiaryDate" TextMode="Date" runat="server" CssClass="form-control" Width="70%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxExpiaryDate">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Select expiary date.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblMerchandiser" runat="server" Text="Commercial Concern:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlCommercialConcern" runat="server" CssClass="form-control" Width="70%">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlCommercialConcern">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Select commercial concern.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="Label2" runat="server" Text="LC/Sales Contract Status:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control" Width="70%">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlStatus">
			                                        <span style="font-weight: 700; color: #CC0000;display:flex">Select LC/Sales Contract status.</span>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>


                                    </asp:Panel>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" ValidationGroup="save" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
                <asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" ValidationGroup="save" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>



            </div>

        </div>
    </div>
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
