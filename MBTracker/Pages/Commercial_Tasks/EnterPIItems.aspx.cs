﻿using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Commercial_Tasks;
using MBTracker.Code_Folder.DAL;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBTracker.Pages.Commercial_Tasks
{
    public partial class EnterPIItems : System.Web.UI.Page
    {
        BuyerManager buyerManager = new BuyerManager();
        CommercialManager commercialManager = new CommercialManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        DataTable dtPIItemDetails;
        int initialLoadForUpdate = 0;
        decimal totalGoods = 0;
        decimal grandTotalAmount = 0;
        DatabaseManager dm = new DatabaseManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (Request.QueryString["PIId"] != null)
                {
                 var currentUserRoleId = CommonMethods.SessionInfo.RoleId;
                  if (Request.QueryString["PIId"] != null )
                    {
                        PIId = int.Parse(Tools.UrlDecode(Request.QueryString["PIId"]));
                        initialLoadForUpdate = PIId;
                       
                    }

                }
                else
                {
                    initialLoadForUpdate = PIId;
                    LoadOpenAndRevisePINotInLcpisDropdown(ddlPINumbers, 1, 0);
                    //CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                }
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteOperation();
                }
            }
        }

        public static void LoadOpenAndRevisePINotInLcpisDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new EnterPIItems().GetAllLoadOpenAndRevisePINotInLcpis();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }

        public DataTable GetAllLoadOpenAndRevisePINotInLcpis()
        {
            return dm.ExecuteQuery("usp_GetAllPIFORADDITEM");
        }
        int PIId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["PIId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["PIId"] = value; }
        }

        int IsRevised
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["revised"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["revised"] = value; }
        }


        protected void ddlPINumbers_SelectedIndexChanged(object sender, EventArgs e)
        {

            divPIOtherInfo.Visible = false;
            //ddlBuyers.Enabled = false;
            tbxPIDate.Enabled = false;
            ddlSeason.Enabled = false;
            ddlYears.Enabled = false;
            if (ddlPINumbers.SelectedValue != "")
            {
               
                divPIOtherInfo.Visible = true;
                
                LoadPIInfo(int.Parse(ddlPINumbers.SelectedValue));
                //SetInitialRowCount();
            }
        }

        protected void CalculatePrices(object sender, EventArgs e)
        {
            TextBox tbxSender = (TextBox)sender;

            Label lblTotalAmount = (Label)((RepeaterItem)tbxSender.NamingContainer).FindControl("lblTotalAmount");
            TextBox tbxQty = (TextBox)((RepeaterItem)tbxSender.NamingContainer).FindControl("tbxQty");
            TextBox tbxUnitPrice = (TextBox)((RepeaterItem)tbxSender.NamingContainer).FindControl("tbxUnitPrice");

            if (tbxQty.Text != "" && tbxUnitPrice.Text != "")
            {
                //double totalAmount = double.Parse(tbxQty.Text) * double.Parse(tbxUnitPrice.Text);
                decimal quantity = Convert.ToDecimal(tbxQty.Text);
                decimal unitPrice = Convert.ToDecimal(tbxUnitPrice.Text);

                decimal totalAmount = quantity * unitPrice;

                lblTotalAmount.Text = Math.Round(totalAmount, 4).ToString();
            }

            //SetCurrentRowCount();
            CalculateGrandTotal();

        }

        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("StyleId", typeof(string)));
            dt.Columns.Add(new DataColumn("OrderId", typeof(string)));
            //dt.Columns.Add(new DataColumn("ItemTypeId", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));
            dt.Columns.Add(new DataColumn("HSCode", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("UnitId", typeof(string)));
            dt.Columns.Add(new DataColumn("UnitPrice", typeof(string)));
            dt.Columns.Add(new DataColumn("TotalAmount", typeof(string)));

            dr = dt.NewRow();

            dr["StyleId"] = string.Empty;
            dr["OrderId"] = string.Empty;
            //dr["ItemTypeId"] = string.Empty;
            dr["ItemId"] = string.Empty;
            dr["HSCode"] = string.Empty;
            dr["Quantity"] = string.Empty;
            dr["UnitId"] = string.Empty;
            dr["UnitPrice"] = string.Empty;
            dr["TotalAmount"] = string.Empty;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptPIItemEntryInfo.DataSource = dt;
            rptPIItemEntryInfo.DataBind();

            CalculateGrandTotal();
        }

        protected void rptPIItemEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {


                var hsCode = DataBinder.Eval(e.Item.DataItem, "HSCode").ToString();
                var qty = DataBinder.Eval(e.Item.DataItem, "Quantity").ToString();
                //var unit = DataBinder.Eval(e.Item.DataItem, "Unit").ToString();
                var unitPrice = DataBinder.Eval(e.Item.DataItem, "UnitPrice").ToString();
                var totalAmount = DataBinder.Eval(e.Item.DataItem, "TotalAmount").ToString();

                var ddlStyles = (DropDownList)e.Item.FindControl("ddlStyles");
                var ddlItems = (DropDownList)e.Item.FindControl("ddlItems");
                var ddlUnit = (DropDownList)e.Item.FindControl("ddlUnit");

                var tbxHSCode = (TextBox)e.Item.FindControl("tbxHSCode");
                var tbxQty = (TextBox)e.Item.FindControl("tbxQty");
                var tbxUnitPrice = (TextBox)e.Item.FindControl("tbxUnitPrice");
                var lblTotalAmount = (Label)e.Item.FindControl("lblTotalAmount");
                //Will need add again
                CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
                CommonMethods.LoadYarnAndAccessoriesItemsDropdown(ddlItems, 1, 0);
                CommonMethods.LoadDropdown(ddlUnit, "Units WHERE IsActive = 1", 1, 0);

                tbxHSCode.Text = hsCode.ToString();
                tbxQty.Text = qty.ToString();
                tbxUnitPrice.Text = unitPrice.ToString();
                lblTotalAmount.Text = totalAmount.ToString();

                if (initialLoadForUpdate > 0)
                {

                    var styleId = DataBinder.Eval(e.Item.DataItem, "StyleId").ToString();
                    var orderId = DataBinder.Eval(e.Item.DataItem, "OrderId").ToString();
                    //var itemTypeId = DataBinder.Eval(e.Item.DataItem, "ItemTypeId").ToString();
                    var itemId = DataBinder.Eval(e.Item.DataItem, "ItemId").ToString();
                    var unitId = DataBinder.Eval(e.Item.DataItem, "UnitId").ToString();

                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, int.Parse(string.IsNullOrEmpty(styleId) ? "0" : styleId));
                    ddlUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUnit, int.Parse(string.IsNullOrEmpty(unitId) ? "0" : unitId));

                    var ddlOrders = (DropDownList)e.Item.FindControl("ddlOrders");
                    CommonMethods.LoadOrderDropdownByStyle(ddlOrders, int.Parse(ddlStyles.SelectedValue), 1, 0);
                    ddlOrders.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrders, int.Parse(string.IsNullOrEmpty(orderId) ? "0" : orderId));

                    int selectedStyleId = 0;
                    if (ddlStyles.SelectedValue != "")
                    {
                        selectedStyleId = int.Parse(ddlStyles.SelectedValue);
                    }

                    int selectedOrderId = 0;

                    if (ddlOrders.SelectedValue != "")
                    {
                        selectedOrderId = int.Parse(ddlOrders.SelectedValue);
                    }


                    ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse(string.IsNullOrEmpty(itemId) ? "0" : itemId));

                }



                if (qty != "")
                {
                    totalGoods += decimal.Parse(qty);
                }

                if (totalAmount != "")
                {
                    grandTotalAmount += decimal.Parse(totalAmount);
                }

            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {

                Label lblTotalQuantity = (Label)e.Item.FindControl("lblTotalQuantity");
                Label lblGrandTotalAmount = (Label)e.Item.FindControl("lblGrandTotalAmount");

                lblTotalQuantity.Text = totalGoods.ToString();
                lblGrandTotalAmount.Text = grandTotalAmount.ToString();

            }


        }


        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {
                        DropDownList ddlStyles = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                        DropDownList ddlOrders = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlOrders");
                        // DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                        DropDownList ddlItems = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItems");
                        DropDownList ddlUnit = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlUnit");

                        TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                        TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                        TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");
                        Label lblTotalAmount = (Label)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("lblTotalAmount");

                        dtCurrentTable.Rows[rowIndex]["StyleId"] = ddlStyles.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["OrderId"] = ddlOrders.SelectedValue;
                        //dtCurrentTable.Rows[rowIndex]["ItemTypeId"] = ddlItemTypes.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["ItemId"] = ddlItems.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["UnitId"] = ddlUnit.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["HSCode"] = tbxHSCode.Text;
                        dtCurrentTable.Rows[rowIndex]["Quantity"] = tbxQty.Text;
                        dtCurrentTable.Rows[rowIndex]["UnitPrice"] = tbxUnitPrice.Text;
                        dtCurrentTable.Rows[rowIndex]["TotalAmount"] = lblTotalAmount.Text;


                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["StyleId"] = string.Empty;
                            drCurrentRow["OrderId"] = string.Empty;
                            //drCurrentRow["ItemTypeId"] = string.Empty;
                            drCurrentRow["ItemId"] = string.Empty;
                            drCurrentRow["UnitId"] = string.Empty;
                            drCurrentRow["HSCode"] = string.Empty;
                            drCurrentRow["Quantity"] = string.Empty;
                            drCurrentRow["UnitPrice"] = string.Empty;
                            drCurrentRow["TotalAmount"] = string.Empty;

                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptPIItemEntryInfo.DataSource = dtCurrentTable;
                    this.rptPIItemEntryInfo.DataBind();

                }
            }
            this.SetPreviousData();
            CalculateGrandTotal();
        }


        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DropDownList ddlStyles = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                        DropDownList ddlOrders = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlOrders");
                        // DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                        DropDownList ddlItems = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItems");
                        DropDownList ddlUnit = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlUnit");

                        TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                        TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                        TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");
                        Label lblTotalAmount = (Label)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("lblTotalAmount");

                        CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
                        CommonMethods.LoadDropdown(ddlUnit, "Units WHERE IsActive = 1", 1, 0);
                        ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["StyleId"].ToString()) ? "0" : dt.Rows[i]["StyleId"].ToString())));
                        ddlUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUnit, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["UnitId"].ToString()) ? "0" : dt.Rows[i]["UnitId"].ToString())));

                        // CommonMethods.LoadYarnAndAccessoriesItemTypeDropdown(ddlItemTypes, 1, 0);
                        // ddlItemTypes.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItemTypes, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ItemTypeId"].ToString()) ? "0" : dt.Rows[i]["ItemTypeId"].ToString())));


                        int selectedStyleId = 0;
                        if (ddlStyles.SelectedValue != "")
                        {
                            selectedStyleId = int.Parse(ddlStyles.SelectedValue);
                        }

                        CommonMethods.LoadOrderDropdownByStyle(ddlOrders, selectedStyleId, 1, 0);
                        ddlOrders.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrders, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["OrderId"].ToString()) ? "0" : dt.Rows[i]["OrderId"].ToString())));


                        int selectedOrderId = 0;

                        if (ddlOrders.SelectedValue != "")
                        {
                            selectedOrderId = int.Parse(ddlOrders.SelectedValue);
                        }

                        //CommonMethods.LoadYarnAndAccessoriesItemDropdownByOrderOrStyle(ddlItems, selectedStyleId, selectedOrderId, 1, 0);
                        CommonMethods.LoadYarnAndAccessoriesItemsDropdown(ddlItems, 1, 0);
                        ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ItemId"].ToString()) ? "0" : dt.Rows[i]["ItemId"].ToString())));

                        tbxHSCode.Text = dt.Rows[i]["HSCode"].ToString();
                        tbxQty.Text = dt.Rows[i]["Quantity"].ToString();
                        tbxUnitPrice.Text = dt.Rows[i]["UnitPrice"].ToString();
                        lblTotalAmount.Text = dt.Rows[i]["TotalAmount"].ToString();

                        rowIndex++;
                    }
                }
            }
        }


        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {
            if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (!CheckItems())
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter item(s) info correctly.')", true);
            }
           
            else
            {
                try
                {

                    var pIInfo = new PIInfo()
                    {
                        BuyerId = int.Parse(ddlBuyers.SelectedValue),
                        Id = int.Parse(ddlPINumbers.SelectedValue),
                        PIDate = DateTime.Parse(tbxPIDate.Text),
                        OrderSeasonId = int.Parse(ddlSeason.SelectedValue),
                        SeasonYearId = int.Parse(ddlYears.SelectedValue),
                       
                        IsActive = 1,
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };

                    var isExist = unitOfWork.GenericRepositories<PIInfo>().Get(x => x.Id == pIInfo.Id && x.BuyerId == pIInfo.BuyerId && x.OrderSeasonId == pIInfo.OrderSeasonId).Any();
                    if (isExist)
                    {

                        List<PIItems> listPIItems = new List<PIItems>();

                        for (int rowIndex = 0; rowIndex < rptPIItemEntryInfo.Items.Count; rowIndex++)
                        {
                            DropDownList ddlStyles = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                            DropDownList ddlOrders = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlOrders");
                            DropDownList ddlItems = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItems");
                            DropDownList ddlUnit = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlUnit");

                            int selectedOrderId = 0;
                            if (ddlOrders.SelectedValue != "")
                            {
                                selectedOrderId = int.Parse(ddlOrders.SelectedValue);
                            }

                            TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                            TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                            TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");

                            Label lblTotalAmount = (Label)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("lblTotalAmount");

                            if (ddlStyles.SelectedValue != "" && ddlItems.SelectedValue != "" && !string.IsNullOrEmpty(tbxQty.Text) && !string.IsNullOrEmpty(tbxUnitPrice.Text))
                            {
                                Decimal QntityInKg = 0;
                                if (int.Parse(ddlUnit.SelectedValue.ToString()) == 1)
                                {
                                    QntityInKg = (decimal)0.453592 * decimal.Parse(tbxQty.Text);
                                }

                                var pIITem = new PIItems()
                                {
                                    PIId = pIInfo.Id,
                                    StyleId = int.Parse(ddlStyles.SelectedValue),
                                    OrderId = selectedOrderId,
                                    ItemId = int.Parse(ddlItems.SelectedValue),
                                    HSCode = tbxHSCode.Text,
                                    UnitId = int.Parse(ddlUnit.SelectedValue == "" ? "17" : ddlUnit.SelectedValue),
                                    Quantity = decimal.Parse(tbxQty.Text),
                                    UnitPrice = decimal.Parse(tbxUnitPrice.Text),
                                    ItemTotalAmount = decimal.Parse(lblTotalAmount.Text),
                                    QuantityInKg = QntityInKg,
                                    CreateDate = DateTime.Now,
                                    CreatedBy = CommonMethods.SessionInfo.UserName
                                };

                                listPIItems.Add(pIITem);
                            }

                        }

                        foreach (var item in listPIItems)
                        {
                            unitOfWork.GenericRepositories<PIItems>().Insert(item);
                        }

                        unitOfWork.Save();

                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('EnterPIItems.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('PI Item's can not save.')", true);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }

        }

        private bool CheckItems()
        {
            bool hasItem = false;

            for (int rowIndex = 0; rowIndex < rptPIItemEntryInfo.Items.Count; rowIndex++)
            {

                DropDownList ddlStyles = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                DropDownList ddlItems = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItems");
                DropDownList ddlUnit = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlUnit");

                TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");


                try
                {
                    decimal quantity = Convert.ToDecimal(tbxQty.Text);
                    decimal unitPrice = Convert.ToDecimal(tbxUnitPrice.Text);

                    if (quantity > decimal.Parse("0.00") && unitPrice > decimal.Parse("0.00") && ddlStyles.SelectedValue != "" && ddlItems.SelectedValue != "" && ddlUnit.SelectedValue != "") //&& ddlUnit.SelectedValue != ""
                    {
                        hasItem = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    hasItem = false;
                    break;
                }
            }

            return hasItem;
        }



        public int SavePIStyles(int piId)
        {
            DataTable dt = PIStyle();


           

            return commercialManager.SavePIStyles(dt);
        }

        public DataTable PIStyle()
        {
            DataTable dt = new DataTable("PIStyles");
            dt.Columns.Add("PIId", typeof(int));
            dt.Columns.Add("StyleId", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }

        private void LoadPIInfo(int PIId)
        {

            try
            {
                string sql = $"Exec usp_GetPIInfoDetailsById '{PIId}'";

                var dtPIInfo = unitOfWork.GetDataTableFromSql(sql);
                //var status = int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["StatusId"].ToString()) ? "0" : dtPIInfo.Rows[0]["StatusId"].ToString()));

                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["BuyerId"].ToString()) ? "0" : dtPIInfo.Rows[0]["BuyerId"].ToString())));

                divPIOtherInfo.Visible = true;
               
                //tbxPINumber.Text = dtPIInfo.Rows[0]["PINumber"].ToString();
                tbxPIDate.Text = DateTime.Parse(dtPIInfo.Rows[0]["PIDate"].ToString()).ToString("yyyy-MM-dd");

                CommonMethods.LoadDropdownById(ddlSeason, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerSeasons", 1, 0);
                ddlSeason.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSeason, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["OrderSeasonId"].ToString()) ? "0" : dtPIInfo.Rows[0]["OrderSeasonId"].ToString())));

                CommonMethods.LoadDropdown(ddlYears, "Years WHERE IsActive = 1", 1, 0);
                ddlYears.SelectedIndex = CommonMethods.MatchDropDownItem(ddlYears, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["SeasonYearId"].ToString()) ? "0" : dtPIInfo.Rows[0]["SeasonYearId"].ToString())));


                    SetInitialRowCount();

               
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }



        private void CreateDataTableForViewState()
        {

            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("StyleId", typeof(string)));
            dt.Columns.Add(new DataColumn("OrderId", typeof(string)));
            //dt.Columns.Add(new DataColumn("ItemTypeId", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));

            dt.Columns.Add(new DataColumn("HSCode", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("UnitId", typeof(string)));
            dt.Columns.Add(new DataColumn("UnitPrice", typeof(string)));
            dt.Columns.Add(new DataColumn("TotalAmount", typeof(string)));



            for (int rowIndex = 0; rowIndex < dtPIItemDetails.Rows.Count; rowIndex++)
            {
                dr = dt.NewRow();

                dr["StyleId"] = dtPIItemDetails.Rows[rowIndex]["StyleId"];
                dr["OrderId"] = dtPIItemDetails.Rows[rowIndex]["OrderId"];
                //dr["ItemTypeId"] = dtPIItemDetails.Rows[rowIndex]["ItemTypeId"];
                dr["ItemId"] = dtPIItemDetails.Rows[rowIndex]["ItemId"];
                dr["HSCode"] = dtPIItemDetails.Rows[rowIndex]["HSCode"];
                dr["UnitId"] = dtPIItemDetails.Rows[rowIndex]["UnitId"];
                dr["Quantity"] = dtPIItemDetails.Rows[rowIndex]["Quantity"];
                dr["UnitPrice"] = dtPIItemDetails.Rows[rowIndex]["UnitPrice"];
                dr["TotalAmount"] = dtPIItemDetails.Rows[rowIndex]["TotalAmount"];


                dt.Rows.Add(dr);
            }


            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

        }



        private void UpdatePIItems(int piId)
        {

            var piItems = unitOfWork.GenericRepositories<PIItems>().Get(x => x.PIId == piId);

            foreach (var piItem in piItems)
            {
                unitOfWork.GenericRepositories<PIItems>().Delete(piItem);
            }

            List<PIItems> listPIItems = new List<PIItems>();

            for (int rowIndex = 0; rowIndex < rptPIItemEntryInfo.Items.Count; rowIndex++)
            {

                DropDownList ddlStyles = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                DropDownList ddlOrders = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlOrders");
                //DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                DropDownList ddlItems = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItems");
                DropDownList ddlUnit = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlUnit");

                int selectedOrderId = 0;
                if (ddlOrders.SelectedValue != "")
                {
                    selectedOrderId = int.Parse(ddlOrders.SelectedValue);
                }

                TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");

                Label lblTotalAmount = (Label)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("lblTotalAmount");


                if (ddlStyles.SelectedValue != "" && ddlItems.SelectedValue != "" && !string.IsNullOrEmpty(tbxQty.Text) && !string.IsNullOrEmpty(tbxUnitPrice.Text)) //&& ddlUnit.SelectedValue != ""
                {
                    Decimal QntityInKg = 0;
                    if (int.Parse(ddlUnit.SelectedValue.ToString()) == 1)
                    {
                        QntityInKg = (decimal)0.453592 * decimal.Parse(tbxQty.Text);
                    }

                    var pIITem = new PIItems()
                    {
                        PIId = piId,
                        StyleId = int.Parse(ddlStyles.SelectedValue),
                        OrderId = selectedOrderId,
                        // ItemTypeId = int.Parse(ddlItemTypes.SelectedValue),
                        ItemId = int.Parse(ddlItems.SelectedValue),
                        UnitId = int.Parse(ddlUnit.SelectedValue == "" ? "17" : ddlUnit.SelectedValue),
                        HSCode = tbxHSCode.Text,
                        Quantity = decimal.Parse(tbxQty.Text),
                        UnitPrice = decimal.Parse(tbxUnitPrice.Text),
                        ItemTotalAmount = decimal.Parse(lblTotalAmount.Text),
                        QuantityInKg = QntityInKg,
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };


                    listPIItems.Add(pIITem);
                }

            }

            foreach (var item in listPIItems)
            {
                unitOfWork.GenericRepositories<PIItems>().Insert(item);
            }

        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {

            DropDownList ddlStyles = (DropDownList)sender;
            DropDownList ddlOrders = (DropDownList)((RepeaterItem)ddlStyles.NamingContainer).FindControl("ddlOrders");

            //DropDownList ddlItemTypes = (DropDownList)((RepeaterItem)ddlStyles.NamingContainer).FindControl("ddlItemTypes");
            DropDownList ddlItems = (DropDownList)((RepeaterItem)ddlStyles.NamingContainer).FindControl("ddlItems");


            if (ddlStyles.SelectedValue != "")
            {

                int orderId = 0;

                if (ddlOrders.SelectedValue != "")
                {
                    orderId = int.Parse(ddlOrders.SelectedValue);
                }


                //ddlItems.Items.Clear();


                CommonMethods.LoadOrderDropdownByStyle(ddlOrders, int.Parse(ddlStyles.SelectedValue), 1, 0);

            }
            else
            {
                ddlOrders.Items.Clear();
            }

        }


       

        protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        {

            DropDownList ddlOrders = (DropDownList)sender;
            DropDownList ddlStyles = (DropDownList)((RepeaterItem)ddlOrders.NamingContainer).FindControl("ddlStyles");

            //DropDownList ddlItemTypes = (DropDownList)((RepeaterItem)ddlOrders.NamingContainer).FindControl("ddlItemTypes");
            DropDownList ddlItems = (DropDownList)((RepeaterItem)ddlOrders.NamingContainer).FindControl("ddlItems");


            //if (ddlOrders.SelectedValue != "")
            //{

            int styleId = 0;

            if (ddlStyles.SelectedValue != "")
            {
                styleId = int.Parse(ddlStyles.SelectedValue);
            }


            int orderId = 0;

            if (ddlOrders.SelectedValue != "")
            {
                orderId = int.Parse(ddlOrders.SelectedValue);
            }

          
            ddlItems.Items.Clear();

            CommonMethods.LoadYarnAndAccessoriesItemDropdownByOrderOrStyle(ddlItems, styleId, orderId, 1, 0);
           

        }



        private void CalculateGrandTotal()
        {
            decimal grandQtyTotal = 0;
            decimal grandDollarTotal = 0;

            for (int i = 0; i < this.rptPIItemEntryInfo.Items.Count; i++)
            {


                TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[i].FindControl("tbxQty");
                TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[i].FindControl("tbxUnitPrice");

                if (tbxQty.Text != "" && tbxUnitPrice.Text != "")
                {

                    decimal quantity = Convert.ToDecimal(tbxQty.Text);
                    decimal unitPrice = Convert.ToDecimal(tbxUnitPrice.Text);

                    decimal totalAmount = quantity * unitPrice;

                    grandQtyTotal = grandQtyTotal + quantity;
                    grandDollarTotal = grandDollarTotal + totalAmount;



                }

            }

            Label lblTotalQuantity = (Label)rptPIItemEntryInfo.Controls[rptPIItemEntryInfo.Controls.Count - 1].Controls[0].FindControl("lblTotalQuantity");
            Label lblGrandTotalAmount = (Label)rptPIItemEntryInfo.Controls[rptPIItemEntryInfo.Controls.Count - 1].Controls[0].FindControl("lblGrandTotalAmount");

            lblTotalQuantity.Text = Math.Round(grandQtyTotal, 2).ToString();
            lblGrandTotalAmount.Text = Math.Round(grandDollarTotal, 2).ToString();

        }


        protected void btnCopy_Click(object sender, CommandEventArgs e)
        {
            int rowNumberWillCopy = Convert.ToInt32(e.CommandArgument.ToString());

            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    var StyleId = "";
                    var OrderId = "";
                    var ItemTypeId = "";
                    var ItemId = "";
                    var UnitId = "";
                    var HSCode = "";
                    var Quantity = "";
                    var UnitPrice = "";
                    var TotalAmount = "";

                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {


                        DropDownList ddlStyles = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                        DropDownList ddlOrders = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlOrders");
                        // DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                        DropDownList ddlItems = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItems");
                        DropDownList ddlUnit = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlUnit");



                        TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                        TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                        TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");
                        Label lblTotalAmount = (Label)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("lblTotalAmount");

                        dtCurrentTable.Rows[rowIndex]["StyleId"] = ddlStyles.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["OrderId"] = ddlOrders.SelectedValue;
                        //dtCurrentTable.Rows[rowIndex]["ItemTypeId"] = ddlItemTypes.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["ItemId"] = ddlItems.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["UnitId"] = ddlUnit.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["HSCode"] = tbxHSCode.Text;
                        dtCurrentTable.Rows[rowIndex]["Quantity"] = tbxQty.Text;
                        dtCurrentTable.Rows[rowIndex]["UnitPrice"] = tbxUnitPrice.Text;
                        dtCurrentTable.Rows[rowIndex]["TotalAmount"] = lblTotalAmount.Text;

                        if (rowIndex == rowNumberWillCopy)
                        {
                            StyleId = ddlStyles.SelectedValue;
                            OrderId = ddlOrders.SelectedValue;
                            ItemId = ddlItems.SelectedValue;
                            UnitId = ddlUnit.SelectedValue;
                            HSCode = tbxHSCode.Text;
                            Quantity = tbxQty.Text;
                            UnitPrice = tbxUnitPrice.Text;
                            TotalAmount = lblTotalAmount.Text;
                        }


                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["StyleId"] = StyleId;
                            drCurrentRow["OrderId"] = OrderId;
                            drCurrentRow["ItemId"] = ItemId;
                            drCurrentRow["UnitId"] = UnitId;
                            drCurrentRow["HSCode"] = HSCode;
                            drCurrentRow["Quantity"] = Quantity;
                            drCurrentRow["UnitPrice"] = UnitPrice;
                            drCurrentRow["TotalAmount"] = TotalAmount;
                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptPIItemEntryInfo.DataSource = dtCurrentTable;
                    this.rptPIItemEntryInfo.DataBind();

                }
            }
            this.SetPreviousData();
            CalculateGrandTotal();
        }

        int RowId
        {
            set { ViewState["rowId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["rowId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void btnRemove_Command(object sender, CommandEventArgs e)
        {
            try
            {
                RowId = Convert.ToInt32(e.CommandArgument.ToString());
                //var IsDeletable = bool.Parse(unitOfWork.GetSingleStringValue($"EXEC usp_CheckBookingIsDeleteable {BookingId}"));
                var IsDeletable = true;
                if (IsDeletable)
                {
                    var title = "Warning";
                    var msg = "Are you sure you want to Delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                }
                else
                {
                    var title = "Warning";
                    var msg = "Sorry, you can not delete it.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }

        }


        private void DeleteOperation()
        {
            //int rowWillRemove = Convert.ToInt32(e.CommandArgument.ToString());

            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {

                    dtCurrentTable.Rows.RemoveAt(RowId);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptPIItemEntryInfo.DataSource = dtCurrentTable;
                    this.rptPIItemEntryInfo.DataBind();

                }
            }
            this.SetPreviousData();
            CalculateGrandTotal();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Row deleted successfully.');", true);
        }
    }
}