﻿using AjaxControlToolkit;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Commercial_Tasks;
using MBTracker.EF;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Pages.Commercial_Tasks
{
    public partial class EnterPIInformationV2 : System.Web.UI.Page
    {

        BuyerManager buyerManager = new BuyerManager();
        CommercialManager commercialManager = new CommercialManager();

        UnitOfWork unitOfWork = new UnitOfWork();

        DataTable dtPIItemDetails;

        int initialLoadForUpdate = 0;
        decimal totalGoods = 0;
        decimal grandTotalAmount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                if (EnableDisableStatusDropdown())
                {
                    ddlStatus.Enabled = true;
                }
                else
                {
                    ddlStatus.Enabled = false;
                }

                if (Request.QueryString["PIId"] != null)
                {
                    var CommandName = "";
                    var tt = Request.QueryString["PIId"];
                    CommandName = Tools.UrlDecode(Request.QueryString["CommandName"].ToString());
                    var currentUserRoleId = CommonMethods.SessionInfo.RoleId;
                    if (Request.QueryString["PIId"] != null && CommandName == "Copy")
                    {
                       
                        PIId = int.Parse(Tools.UrlDecode(Request.QueryString["PIId"]));
                        initialLoadForUpdate = PIId;
                        actionTitle.Text = "Udpate PI Information";
                        lnkbtnSaveEntries.Visible = true;
                        lnkbtnUpdateEntries.Visible = false;
                        ddlStatus.Enabled = true;
                        LoadPIInfoForCopy();

                    }
                    else if(Request.QueryString["PIId"] != null && CommandName == "Edit")
                    {
                        PIId = int.Parse(Tools.UrlDecode(Request.QueryString["PIId"]));
                        initialLoadForUpdate = PIId;
                        actionTitle.Text = "Udpate PI Information";
                        lnkbtnSaveEntries.Visible = false;
                        lnkbtnUpdateEntries.Visible = true;
                        tbxPINumber.Enabled = false;

                        if (currentUserRoleId == 20 || currentUserRoleId == 1) { 
                        DivApprovedDateByMdSir.Visible = true;
                        }
                        else
                        {
                            DivApprovedDateByMdSir.Visible = false;
                        }
                        LoadPIInfo();
                    }
                    else if (Request.QueryString["PIId"] != null && CommandName == "StatusChange")
                    {
                        PIId = int.Parse(Tools.UrlDecode(Request.QueryString["PIId"]));
                        initialLoadForUpdate = PIId;
                        actionTitle.Text = "Udpate PI Information";
                        lnkbtnSaveEntries.Visible = false;
                        lnkbtnUpdateEntries.Visible = false;
                        lnkbtnStatusChange.Visible = true;
                        tbxPINumber.Enabled = false;
                        divPIOtherInfo.Visible = false;
                        divPIOtherInfo2.Visible = false;
                        ddlStatus.Enabled = true;


                        LoadPIInfo();
                        if (currentUserRoleId == 20 || currentUserRoleId == 1)
                        {
                            DivApprovedDateByMdSir.Visible = true;
                            tbxApprovedDateByMdSir.Enabled = true;
                        }
                        else
                        {
                            DivApprovedDateByMdSir.Visible = false;
                        }

                    }

                }
                else
                {
                    DivApprovedDateByMdSir.Visible = false;
                    CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                   // CommonMethods.LoadDropdown(ddlTenor, "Tenor", 1, 0);
                }
            }
            else
            {
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "confirmDelete")
                {
                    DeleteOperation();
                }
            }

        }

        private bool EnableDisableStatusDropdown()
        {

            var isExist = false;

            var pageName = "PIStatusChange";
            var controlType = 3;
            var buttonNumberOnpage = 1;

            string sql = $"Exec usp_GetRoleIdsForEditDeleteButton '{pageName}', {controlType}, {buttonNumberOnpage}";

            UnitOfWork unitOfWork = new UnitOfWork();
            var dt = unitOfWork.GetDataTableFromSql(sql);
            var currentUserRoleId = CommonMethods.SessionInfo.RoleId;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (int.Parse(dt.Rows[i][0].ToString()) == currentUserRoleId)
                {
                    isExist = true;
                    break;
                }
            }

            return isExist;
        }


        private bool EnableDisableEditInfo()
        {
            var isExist = false;

            var pageName = "EditPIInfo";
            var controlType = 4;
            var buttonNumberOnpage = 1;

            string sql = $"Exec usp_GetRoleIdsForEditDeleteButton '{pageName}', {controlType}, {buttonNumberOnpage}";

            UnitOfWork unitOfWork = new UnitOfWork();
            var dt = unitOfWork.GetDataTableFromSql(sql);
            var currentUserRoleId = CommonMethods.SessionInfo.RoleId;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (int.Parse(dt.Rows[i][0].ToString()) == currentUserRoleId)
                {
                    isExist = true;
                    break;
                }
            }

            return isExist;
        }


        int PIId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["PIId"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["PIId"] = value; }
        }

        int IsRevised
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["revised"]);
                }
                catch
                {
                    return 0;
                }
            }
            set { ViewState["revised"] = value; }
        }


        protected void ddlBuyers_SelectedIndexChanged(object sender, EventArgs e)
        {

            divPIOtherInfo.Visible = false;
            divPIOtherInfo2.Visible = false;


            if (ddlBuyers.SelectedValue != "")
            {
                SetInitialRowCount();
                divPIOtherInfo.Visible = true;
                divPIOtherInfo2.Visible = true;
                CommonMethods.LoadYarnAndAccessoriesSupplierDropdown(ddlYarnAndAccssSuppliers, 1, 0);
                CommonMethods.LoadYarnAndAccessoriesBuyerDropdown(ddlItemBuyers, 1, 0);
                CommonMethods.LoadDropdown(ddlTenor, "Tenor", 1, 0);
                CommonMethods.LoadMerchandiserDropDown(ddlMerchandiser, 1, 0);
                CommonMethods.LoadMerchandiserLeaderDropDown(ddlMerchandisingLeader, 1, 0);
                CommonMethods.PIStatusDropdown(ddlStatus, 1, 0);
                CommonMethods.LoadDropdownById(ddlSeason, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerSeasons", 1, 0);
                CommonMethods.LoadDropdown(ddlYears, "Years WHERE IsActive = 1", 1, 0);
                

            }
        }


        protected void CalculatePrices(object sender, EventArgs e)
        {
            TextBox tbxSender = (TextBox)sender;

            Label lblTotalAmount = (Label)((RepeaterItem)tbxSender.NamingContainer).FindControl("lblTotalAmount");
            TextBox tbxQty = (TextBox)((RepeaterItem)tbxSender.NamingContainer).FindControl("tbxQty");
            TextBox tbxUnitPrice = (TextBox)((RepeaterItem)tbxSender.NamingContainer).FindControl("tbxUnitPrice");


            if (tbxQty.Text != "" && tbxUnitPrice.Text != "")
            {
                //double totalAmount = double.Parse(tbxQty.Text) * double.Parse(tbxUnitPrice.Text);
                decimal quantity = Convert.ToDecimal(tbxQty.Text);
                decimal unitPrice = Convert.ToDecimal(tbxUnitPrice.Text);

                decimal totalAmount = quantity * unitPrice;

                lblTotalAmount.Text = Math.Round(totalAmount, 4).ToString();
            }

            //SetCurrentRowCount();
            CalculateGrandTotal();

            //if (int.Parse(ddlBuyers.SelectedValue) == 1)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(1000)", true);
            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "scrollDown(500)", true);
            //}
        }

        

        private void SetInitialRowCount()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("StyleId", typeof(string)));
            dt.Columns.Add(new DataColumn("OrderId", typeof(string)));
            //dt.Columns.Add(new DataColumn("ItemTypeId", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));

            dt.Columns.Add(new DataColumn("HSCode", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("UnitId", typeof(string)));
            dt.Columns.Add(new DataColumn("UnitPrice", typeof(string)));
            dt.Columns.Add(new DataColumn("TotalAmount", typeof(string)));

            dr = dt.NewRow();

            dr["StyleId"] = string.Empty;
            dr["OrderId"] = string.Empty;
            //dr["ItemTypeId"] = string.Empty;
            dr["ItemId"] = string.Empty;
            dr["HSCode"] = string.Empty;
            dr["Quantity"] = string.Empty;
            dr["UnitId"] = string.Empty;
            dr["UnitPrice"] = string.Empty;
            dr["TotalAmount"] = string.Empty;


            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            rptPIItemEntryInfo.DataSource = dt;
            rptPIItemEntryInfo.DataBind();

            CalculateGrandTotal();
        }

        protected void rptPIItemEntryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {


                var hsCode = DataBinder.Eval(e.Item.DataItem, "HSCode").ToString();
                var qty = DataBinder.Eval(e.Item.DataItem, "Quantity").ToString();
                //var unit = DataBinder.Eval(e.Item.DataItem, "Unit").ToString();
                var unitPrice = DataBinder.Eval(e.Item.DataItem, "UnitPrice").ToString();
                var totalAmount = DataBinder.Eval(e.Item.DataItem, "TotalAmount").ToString();


                var ddlStyles = (DropDownList)e.Item.FindControl("ddlStyles");
                var ddlItems = (DropDownList)e.Item.FindControl("ddlItems");
                var ddlUnit = (DropDownList)e.Item.FindControl("ddlUnit");


                var tbxHSCode = (TextBox)e.Item.FindControl("tbxHSCode");
                var tbxQty = (TextBox)e.Item.FindControl("tbxQty");
                var tbxUnitPrice = (TextBox)e.Item.FindControl("tbxUnitPrice");
                var lblTotalAmount = (Label)e.Item.FindControl("lblTotalAmount");


                CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
                CommonMethods.LoadYarnAndAccessoriesItemsDropdown(ddlItems, 1, 0);
                CommonMethods.LoadDropdown(ddlUnit, "Units WHERE IsActive = 1", 1, 0);

                tbxHSCode.Text = hsCode.ToString();
                tbxQty.Text = qty.ToString();
                tbxUnitPrice.Text = unitPrice.ToString();
                lblTotalAmount.Text = totalAmount.ToString();



                if (initialLoadForUpdate > 0)
                {

                    var styleId = DataBinder.Eval(e.Item.DataItem, "StyleId").ToString();
                    var orderId = DataBinder.Eval(e.Item.DataItem, "OrderId").ToString();
                    //var itemTypeId = DataBinder.Eval(e.Item.DataItem, "ItemTypeId").ToString();
                    var itemId = DataBinder.Eval(e.Item.DataItem, "ItemId").ToString();
                    var unitId = DataBinder.Eval(e.Item.DataItem, "UnitId").ToString();


                    ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, int.Parse(string.IsNullOrEmpty(styleId) ? "0" : styleId));
                    ddlUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUnit, int.Parse(string.IsNullOrEmpty(unitId) ? "0" : unitId));

                    var ddlOrders = (DropDownList)e.Item.FindControl("ddlOrders");
                    CommonMethods.LoadOrderDropdownByStyle(ddlOrders, int.Parse(ddlStyles.SelectedValue), 1, 0);
                    ddlOrders.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrders, int.Parse(string.IsNullOrEmpty(orderId) ? "0" : orderId));



                    int selectedStyleId = 0;
                    if (ddlStyles.SelectedValue != "")
                    {
                        selectedStyleId = int.Parse(ddlStyles.SelectedValue);
                    }

                    int selectedOrderId = 0;

                    if (ddlOrders.SelectedValue != "")
                    {
                        selectedOrderId = int.Parse(ddlOrders.SelectedValue);
                    }


                    ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse(string.IsNullOrEmpty(itemId) ? "0" : itemId));

                }



                if (qty != "")
                {
                    totalGoods += decimal.Parse(qty);
                }

                if (totalAmount != "")
                {
                    grandTotalAmount += decimal.Parse(totalAmount);
                }

            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {

                Label lblTotalQuantity = (Label)e.Item.FindControl("lblTotalQuantity");
                Label lblGrandTotalAmount = (Label)e.Item.FindControl("lblGrandTotalAmount");


                lblTotalQuantity.Text = totalGoods.ToString();
                lblGrandTotalAmount.Text = grandTotalAmount.ToString();

            }


        }


        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {


                        DropDownList ddlStyles = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                        DropDownList ddlOrders = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlOrders");
                        // DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                        DropDownList ddlItems = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItems");
                        DropDownList ddlUnit = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlUnit");



                        TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                        TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                        TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");
                        Label lblTotalAmount = (Label)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("lblTotalAmount");

                        dtCurrentTable.Rows[rowIndex]["StyleId"] = ddlStyles.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["OrderId"] = ddlOrders.SelectedValue;
                        //dtCurrentTable.Rows[rowIndex]["ItemTypeId"] = ddlItemTypes.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["ItemId"] = ddlItems.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["UnitId"] = ddlUnit.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["HSCode"] = tbxHSCode.Text;
                        dtCurrentTable.Rows[rowIndex]["Quantity"] = tbxQty.Text;
                        dtCurrentTable.Rows[rowIndex]["UnitPrice"] = tbxUnitPrice.Text;
                        dtCurrentTable.Rows[rowIndex]["TotalAmount"] = lblTotalAmount.Text;


                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["StyleId"] = string.Empty;
                            drCurrentRow["OrderId"] = string.Empty;
                            //drCurrentRow["ItemTypeId"] = string.Empty;
                            drCurrentRow["ItemId"] = string.Empty;
                            drCurrentRow["UnitId"] = string.Empty;
                            drCurrentRow["HSCode"] = string.Empty;
                            drCurrentRow["Quantity"] = string.Empty;
                            drCurrentRow["UnitPrice"] = string.Empty;
                            drCurrentRow["TotalAmount"] = string.Empty;

                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptPIItemEntryInfo.DataSource = dtCurrentTable;
                    this.rptPIItemEntryInfo.DataBind();

                }
            }
            this.SetPreviousData();
            CalculateGrandTotal();
        }


        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (this.ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)this.ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {


                        DropDownList ddlStyles = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                        DropDownList ddlOrders = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlOrders");
                        // DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                        DropDownList ddlItems = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItems");
                        DropDownList ddlUnit = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlUnit");



                        TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                        TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                        TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");
                        Label lblTotalAmount = (Label)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("lblTotalAmount");


                        CommonMethods.LoadDropdownById(ddlStyles, int.Parse(ddlBuyers.SelectedValue), "BuyerStyles", 1, 0);
                        CommonMethods.LoadDropdown(ddlUnit, "Units WHERE IsActive = 1", 1, 0);
                        ddlStyles.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStyles, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["StyleId"].ToString()) ? "0" : dt.Rows[i]["StyleId"].ToString())));
                        ddlUnit.SelectedIndex = CommonMethods.MatchDropDownItem(ddlUnit, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["UnitId"].ToString()) ? "0" : dt.Rows[i]["UnitId"].ToString())));

                        // CommonMethods.LoadYarnAndAccessoriesItemTypeDropdown(ddlItemTypes, 1, 0);
                        // ddlItemTypes.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItemTypes, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ItemTypeId"].ToString()) ? "0" : dt.Rows[i]["ItemTypeId"].ToString())));


                        int selectedStyleId = 0;
                        if (ddlStyles.SelectedValue != "")
                        {
                            selectedStyleId = int.Parse(ddlStyles.SelectedValue);
                        }

                        CommonMethods.LoadOrderDropdownByStyle(ddlOrders, selectedStyleId, 1, 0);
                        ddlOrders.SelectedIndex = CommonMethods.MatchDropDownItem(ddlOrders, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["OrderId"].ToString()) ? "0" : dt.Rows[i]["OrderId"].ToString())));


                        int selectedOrderId = 0;

                        if (ddlOrders.SelectedValue != "")
                        {
                            selectedOrderId = int.Parse(ddlOrders.SelectedValue);
                        }


                        //CommonMethods.LoadYarnAndAccessoriesItemDropdownByOrderOrStyle(ddlItems, selectedStyleId, selectedOrderId, 1, 0);
                        CommonMethods.LoadYarnAndAccessoriesItemsDropdown(ddlItems, 1, 0);
                        ddlItems.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItems, int.Parse((string.IsNullOrEmpty(dt.Rows[i]["ItemId"].ToString()) ? "0" : dt.Rows[i]["ItemId"].ToString())));


                        tbxHSCode.Text = dt.Rows[i]["HSCode"].ToString();
                        tbxQty.Text = dt.Rows[i]["Quantity"].ToString();
                        tbxUnitPrice.Text = dt.Rows[i]["UnitPrice"].ToString();
                        lblTotalAmount.Text = dt.Rows[i]["TotalAmount"].ToString();

                        rowIndex++;
                    }
                }
            }
        }


        protected void lnkbtnSaveEntries_Click(object sender, EventArgs e)
        {
            string ApprovedDateByMdSirdefault = "2100-01-01";

            if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (String.IsNullOrEmpty(tbxPINumber.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter IP number.')", true);
            }
            else if (String.IsNullOrEmpty(tbxPIDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter PI date.')", true);
            }
            else if (ddlSeason.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select the season.')", true);
            }
            else if (ddlYears.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select the year.')", true);
            }
            else if (!CheckItems())
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter item(s) info correctly.')", true);
            }
            else if (ddlYarnAndAccssSuppliers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select beneficiary.')", true);
            }
            else if (ddlItemBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select item buyer.')", true);
            }
            else if (String.IsNullOrEmpty(tbxItemOrderDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter item order date.')", true);
            }
            else if (ddlMerchandiser.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a merchandiser.')", true);
            }
            else if (ddlTenor.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a Payment Mode.')", true);
            }
            else if (ddlMerchandisingLeader.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select merchandiser leader.')", true);
            }
            else
            {
                try
                {

                    var pIInfo = new PIInfo()
                    {
                        BuyerId = int.Parse(ddlBuyers.SelectedValue),
                        PINumber = tbxPINumber.Text.ToString(),
                        PIDate = DateTime.Parse(tbxPIDate.Text),
                        OrderSeasonId = int.Parse(ddlSeason.SelectedValue),
                        SeasonYearId = int.Parse(ddlYears.SelectedValue),
                        BeneficiaryId = int.Parse(ddlYarnAndAccssSuppliers.SelectedValue),
                        NominatedOrNot = int.Parse(ddlNominateOrNot.SelectedValue),
                        BeneficiaryPhone = tbxBeneficiaryPhone.Text,
                        BeneficiaryBankInfo = tbxBeneficiaryBankInfo.Text,
                        ShipmentInfo = tbxShipment.Text.ToString(),
                        PartialShipment = int.Parse(ddlPartialShipment.SelectedValue),
                        PIType = int.Parse(ddlPIType.SelectedValue),
                        ItemBuyerId = int.Parse(ddlItemBuyers.SelectedValue),
                        ItemOrderDate = DateTime.Parse(tbxItemOrderDate.Text),
                        ItemOrderMonth = (DateTime.Parse(tbxItemOrderDate.Text)).ToString("MMMM"),
                        ItemOrderYear = (DateTime.Parse(tbxItemOrderDate.Text)).Year,
                        Tenor = int.Parse(ddlTenor.SelectedValue),
                        PaymentMode = tbxPaymentMode.Text.ToString(),
                        MerchandiserId = int.Parse(ddlMerchandiser.SelectedValue),
                        MerchandiserLeaderId = int.Parse(ddlMerchandisingLeader.SelectedValue),
                        TolaranceInfo = tbxTolerance.Text,
                        Remarks = tbxOtherInfo.Text,
                        Status = int.Parse(ddlStatus.SelectedValue),
                        ApprovedDateByMdSir = DateTime.Parse(ApprovedDateByMdSirdefault),
                        IsActive = 1,
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };

                    var isExist = unitOfWork.GenericRepositories<PIInfo>().Get(x => x.PINumber == pIInfo.PINumber.Trim() && x.BuyerId == pIInfo.BuyerId && x.OrderSeasonId == pIInfo.OrderSeasonId && x.Status != 0).Any();
                    if (!isExist)
                    {

                        unitOfWork.GenericRepositories<PIInfo>().Insert(pIInfo);
                        unitOfWork.Save();


                        List<PIItems> listPIItems = new List<PIItems>();

                        for (int rowIndex = 0; rowIndex < rptPIItemEntryInfo.Items.Count; rowIndex++)
                        {

                            //TextBox tbxDescription = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxDescription");

                            DropDownList ddlStyles = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                            DropDownList ddlOrders = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlOrders");
                            //DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                            DropDownList ddlItems = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItems");
                            DropDownList ddlUnit = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlUnit");

                            int selectedOrderId = 0;
                            if (ddlOrders.SelectedValue != "")
                            {
                                selectedOrderId = int.Parse(ddlOrders.SelectedValue);
                            }





                            TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                            TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                            TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");

                            Label lblTotalAmount = (Label)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("lblTotalAmount");
                            
                            if (ddlStyles.SelectedValue != "" && ddlItems.SelectedValue != "" && !string.IsNullOrEmpty(tbxQty.Text) && !string.IsNullOrEmpty(tbxUnitPrice.Text))
                            {
                                Decimal QntityInKg = 0;
                                if (int.Parse(ddlUnit.SelectedValue.ToString()) == 1)
                                {
                                     QntityInKg = (decimal)0.453592 * decimal.Parse(tbxQty.Text);
                                }
                                
                                var pIITem = new PIItems()
                                {
                                    PIId = pIInfo.Id,
                                    StyleId = int.Parse(ddlStyles.SelectedValue),
                                    OrderId = selectedOrderId,
                                    //ItemTypeId = int.Parse(ddlItemTypes.SelectedValue),
                                    ItemId = int.Parse(ddlItems.SelectedValue),
                                    HSCode = tbxHSCode.Text,
                                    UnitId = int.Parse(ddlUnit.SelectedValue == "" ? "17" : ddlUnit.SelectedValue),
                                    Quantity = decimal.Parse(tbxQty.Text),
                                    UnitPrice = decimal.Parse(tbxUnitPrice.Text),
                                    ItemTotalAmount = decimal.Parse(lblTotalAmount.Text),
                                    QuantityInKg = QntityInKg,
                                    CreateDate = DateTime.Now,
                                    CreatedBy = CommonMethods.SessionInfo.UserName
                                };

                                listPIItems.Add(pIITem);
                            }

                        }

                        foreach (var item in listPIItems)
                        {
                            unitOfWork.GenericRepositories<PIItems>().Insert(item);
                        }

                        unitOfWork.Save();

                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Saved successfully.')", true);
                        ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('EnterPIInformationV2.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('PI already exist.')", true);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }

        }

        private bool CheckItems()
        {
            bool hasItem = false;

            for (int rowIndex = 0; rowIndex < rptPIItemEntryInfo.Items.Count; rowIndex++)
            {

                DropDownList ddlStyles = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                DropDownList ddlItems = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItems");
                DropDownList ddlUnit = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlUnit");

                TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");


                try
                {
                    decimal quantity = Convert.ToDecimal(tbxQty.Text);
                    decimal unitPrice = Convert.ToDecimal(tbxUnitPrice.Text);

                    if (quantity > decimal.Parse("0.00") && unitPrice > decimal.Parse("0.00") && ddlStyles.SelectedValue != "" && ddlItems.SelectedValue != "" && ddlUnit.SelectedValue != "") //&& ddlUnit.SelectedValue != ""
                    {
                        hasItem = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    hasItem = false;
                    break;
                }
            }

            return hasItem;
        }



        public int SavePIStyles(int piId)
        {
            DataTable dt = PIStyle();


            //for (int i = 0; i < cblStyles.Items.Count; i++)
            //{
            //    if (cblStyles.Items[i].Selected)
            //    {
            //        dt.Rows.Add(piId, cblStyles.Items[i].Value, CommonMethods.SessionInfo.UserName, DateTime.Now);
            //    }
            //}


            return commercialManager.SavePIStyles(dt);
        }

        public DataTable PIStyle()
        {
            DataTable dt = new DataTable("PIStyles");
            dt.Columns.Add("PIId", typeof(int));
            dt.Columns.Add("StyleId", typeof(int));
            dt.Columns.Add("CdBy", typeof(string));
            dt.Columns.Add("CDate", typeof(DateTime));
            return dt;
        }

        private void LoadPIInfo()
        {

            try
            {
                string sql = $"Exec usp_GetPIInfoDetailsById '{PIId}'";

                var dtPIInfo = unitOfWork.GetDataTableFromSql(sql);
                var status = int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["StatusId"].ToString()) ? "0" : dtPIInfo.Rows[0]["StatusId"].ToString()));

                if (status == 5)
                {
                    IsRevised = 1;
                    tbxPINumber.Enabled = true;
                }

                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["BuyerId"].ToString()) ? "0" : dtPIInfo.Rows[0]["BuyerId"].ToString())));


                divPIOtherInfo.Visible = true;
                divPIOtherInfo2.Visible = true;

                tbxPINumber.Text = dtPIInfo.Rows[0]["PINumber"].ToString();
                tbxPIDate.Text = DateTime.Parse(dtPIInfo.Rows[0]["PIDate"].ToString()).ToString("yyyy-MM-dd");

                CommonMethods.LoadDropdownById(ddlSeason, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerSeasons", 1, 0);
                ddlSeason.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSeason, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["OrderSeasonId"].ToString()) ? "0" : dtPIInfo.Rows[0]["OrderSeasonId"].ToString())));

                CommonMethods.LoadDropdown(ddlTenor, "Tenor", 1, 0);
                ddlTenor.SelectedIndex = CommonMethods.MatchDropDownItem(ddlTenor, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["Tenor"].ToString()) ? "0" : dtPIInfo.Rows[0]["Tenor"].ToString())));

                CommonMethods.LoadDropdown(ddlYears, "Years WHERE IsActive = 1", 1, 0);
                ddlYears.SelectedIndex = CommonMethods.MatchDropDownItem(ddlYears, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["SeasonYearId"].ToString()) ? "0" : dtPIInfo.Rows[0]["SeasonYearId"].ToString())));



                string sql3 = $"Exec usp_GetPIItemsByPIId '{PIId}'";
                dtPIItemDetails = unitOfWork.GetDataTableFromSql(sql3);

                if (dtPIItemDetails.Rows.Count > 0)
                {
                    rptPIItemEntryInfo.Visible = true;

                    rptPIItemEntryInfo.DataSource = dtPIItemDetails;
                    rptPIItemEntryInfo.DataBind();

                    CreateDataTableForViewState();
                }
                else
                {
                    SetInitialRowCount();

                }


                CommonMethods.LoadYarnAndAccessoriesSupplierDropdown(ddlYarnAndAccssSuppliers, 1, 0);
                ddlYarnAndAccssSuppliers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlYarnAndAccssSuppliers, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["BeneficiaryNameId"].ToString()) ? "0" : dtPIInfo.Rows[0]["BeneficiaryNameId"].ToString())));

                ddlNominateOrNot.SelectedIndex = CommonMethods.MatchDropDownItem(ddlNominateOrNot, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["NominatedOrNot"].ToString()) ? "0" : dtPIInfo.Rows[0]["NominatedOrNot"].ToString())));

                tbxBeneficiaryPhone.Text = dtPIInfo.Rows[0]["BeneficiaryPhone"].ToString();
                tbxBeneficiaryBankInfo.Text = dtPIInfo.Rows[0]["BeneficiaryBankInfo"].ToString();

                tbxShipment.Text = dtPIInfo.Rows[0]["ShipmentInfo"].ToString();

                ddlPartialShipment.SelectedIndex = CommonMethods.MatchDropDownItem(ddlPartialShipment, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["PartialShipmentId"].ToString()) ? "0" : dtPIInfo.Rows[0]["PartialShipmentId"].ToString())));
                ddlPIType.SelectedIndex = CommonMethods.MatchDropDownItem(ddlPIType, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["PITypeId"].ToString()) ? "0" : dtPIInfo.Rows[0]["PITypeId"].ToString())));

                CommonMethods.LoadYarnAndAccessoriesBuyerDropdown(ddlItemBuyers, 1, 0);
                ddlItemBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItemBuyers, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["ItemBuyerId"].ToString()) ? "0" : dtPIInfo.Rows[0]["ItemBuyerId"].ToString())));

                tbxApprovedDateByMdSir.Text = DateTime.Parse(dtPIInfo.Rows[0]["ApprovedDateByMdSir"].ToString()).ToString("yyyy-MM-dd");

                tbxItemOrderDate.Text = DateTime.Parse(dtPIInfo.Rows[0]["ItemOrderDate"].ToString()).ToString("yyyy-MM-dd");

                tbxPaymentMode.Text = dtPIInfo.Rows[0]["PaymentMode"].ToString();

                CommonMethods.LoadMerchandiserDropDown(ddlMerchandiser, 1, 0);
                ddlMerchandiser.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMerchandiser, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["MerchandiserId"].ToString()) ? "0" : dtPIInfo.Rows[0]["MerchandiserId"].ToString())));

                CommonMethods.LoadMerchandiserLeaderDropDown(ddlMerchandisingLeader, 1, 0);
                ddlMerchandisingLeader.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMerchandisingLeader, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["MerchandiserLeaderId"].ToString()) ? "0" : dtPIInfo.Rows[0]["MerchandiserLeaderId"].ToString())));

                tbxTolerance.Text = dtPIInfo.Rows[0]["TolaranceInfo"].ToString();
                tbxOtherInfo.Text = dtPIInfo.Rows[0]["Remarks"].ToString();

                CommonMethods.PIStatusDropdown(ddlStatus, 1, 0);
                ddlStatus.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStatus, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["StatusId"].ToString()) ? "0" : dtPIInfo.Rows[0]["StatusId"].ToString())));

                if (EnableDisableEditInfo() && (int.Parse(dtPIInfo.Rows[0]["StatusId"].ToString()) == 5 || int.Parse(dtPIInfo.Rows[0]["StatusId"].ToString()) == 1))
                {
                    pnlNonEdit1.Enabled = true;
                    pnlNonEdit2.Enabled = true;
                    pnlNonEdit3.Enabled = true;
                }
                else
                {
                    pnlNonEdit1.Enabled = false;
                    pnlNonEdit2.Enabled = false;
                    pnlNonEdit3.Enabled = false;
                }

                if (int.Parse(dtPIInfo.Rows[0]["LCId"].ToString()) != 0 && int.Parse(dtPIInfo.Rows[0]["StatusId"].ToString()) != 5)
                {
                    pnlNonEdit1.Enabled = false;
                    pnlNonEdit2.Enabled = false;
                    pnlNonEdit3.Enabled = false;
                }

                if (int.Parse(dtPIInfo.Rows[0]["StatusId"].ToString()) == 2)
                {
                    pnlNonEdit1.Enabled = false;
                    pnlNonEdit2.Enabled = false;
                    pnlNonEdit3.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }



        private void LoadPIInfoForCopy()
        {

            try
            {
                string sql = $"Exec usp_GetPIInfoDetailsById '{PIId}'";

                var dtPIInfo = unitOfWork.GetDataTableFromSql(sql);
                var status = int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["StatusId"].ToString()) ? "0" : dtPIInfo.Rows[0]["StatusId"].ToString()));

                

                CommonMethods.LoadBuyerDropdownForUser(ddlBuyers, 1, 0);
                ddlBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlBuyers, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["BuyerId"].ToString()) ? "0" : dtPIInfo.Rows[0]["BuyerId"].ToString())));


                divPIOtherInfo.Visible = true;
                divPIOtherInfo2.Visible = true;

                tbxPINumber.Text = dtPIInfo.Rows[0]["PINumber"].ToString();
                tbxPIDate.Text = DateTime.Parse(dtPIInfo.Rows[0]["PIDate"].ToString()).ToString("yyyy-MM-dd");

                CommonMethods.LoadDropdownById(ddlSeason, Convert.ToInt32(ddlBuyers.SelectedValue), "BuyerSeasons", 1, 0);
                ddlSeason.SelectedIndex = CommonMethods.MatchDropDownItem(ddlSeason, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["OrderSeasonId"].ToString()) ? "0" : dtPIInfo.Rows[0]["OrderSeasonId"].ToString())));

                CommonMethods.LoadDropdown(ddlTenor, "Tenor", 1, 0);
                ddlTenor.SelectedIndex = CommonMethods.MatchDropDownItem(ddlTenor, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["Tenor"].ToString()) ? "0" : dtPIInfo.Rows[0]["Tenor"].ToString())));

                CommonMethods.LoadDropdown(ddlYears, "Years WHERE IsActive = 1", 1, 0);
                ddlYears.SelectedIndex = CommonMethods.MatchDropDownItem(ddlYears, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["SeasonYearId"].ToString()) ? "0" : dtPIInfo.Rows[0]["SeasonYearId"].ToString())));



                string sql3 = $"Exec usp_GetPIItemsByPIId '{PIId}'";
                dtPIItemDetails = unitOfWork.GetDataTableFromSql(sql3);

                if (dtPIItemDetails.Rows.Count > 0)
                {
                    rptPIItemEntryInfo.Visible = true;

                    rptPIItemEntryInfo.DataSource = dtPIItemDetails;
                    rptPIItemEntryInfo.DataBind();

                    CreateDataTableForViewState();
                }
                else
                {
                    SetInitialRowCount();

                }


                CommonMethods.LoadYarnAndAccessoriesSupplierDropdown(ddlYarnAndAccssSuppliers, 1, 0);
                ddlYarnAndAccssSuppliers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlYarnAndAccssSuppliers, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["BeneficiaryNameId"].ToString()) ? "0" : dtPIInfo.Rows[0]["BeneficiaryNameId"].ToString())));

                ddlNominateOrNot.SelectedIndex = CommonMethods.MatchDropDownItem(ddlNominateOrNot, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["NominatedOrNot"].ToString()) ? "0" : dtPIInfo.Rows[0]["NominatedOrNot"].ToString())));

                tbxBeneficiaryPhone.Text = dtPIInfo.Rows[0]["BeneficiaryPhone"].ToString();
                tbxBeneficiaryBankInfo.Text = dtPIInfo.Rows[0]["BeneficiaryBankInfo"].ToString();

                tbxShipment.Text = dtPIInfo.Rows[0]["ShipmentInfo"].ToString();

                ddlPartialShipment.SelectedIndex = CommonMethods.MatchDropDownItem(ddlPartialShipment, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["PartialShipmentId"].ToString()) ? "0" : dtPIInfo.Rows[0]["PartialShipmentId"].ToString())));
                ddlPIType.SelectedIndex = CommonMethods.MatchDropDownItem(ddlPIType, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["PITypeId"].ToString()) ? "0" : dtPIInfo.Rows[0]["PITypeId"].ToString())));

                CommonMethods.LoadYarnAndAccessoriesBuyerDropdown(ddlItemBuyers, 1, 0);
                ddlItemBuyers.SelectedIndex = CommonMethods.MatchDropDownItem(ddlItemBuyers, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["ItemBuyerId"].ToString()) ? "0" : dtPIInfo.Rows[0]["ItemBuyerId"].ToString())));

                tbxItemOrderDate.Text = DateTime.Parse(dtPIInfo.Rows[0]["ItemOrderDate"].ToString()).ToString("yyyy-MM-dd");

                tbxPaymentMode.Text = dtPIInfo.Rows[0]["PaymentMode"].ToString();

                CommonMethods.LoadMerchandiserDropDown(ddlMerchandiser, 1, 0);
                ddlMerchandiser.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMerchandiser, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["MerchandiserId"].ToString()) ? "0" : dtPIInfo.Rows[0]["MerchandiserId"].ToString())));

                CommonMethods.LoadMerchandiserLeaderDropDown(ddlMerchandisingLeader, 1, 0);
                ddlMerchandisingLeader.SelectedIndex = CommonMethods.MatchDropDownItem(ddlMerchandisingLeader, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["MerchandiserLeaderId"].ToString()) ? "0" : dtPIInfo.Rows[0]["MerchandiserLeaderId"].ToString())));

                tbxTolerance.Text = dtPIInfo.Rows[0]["TolaranceInfo"].ToString();
                tbxOtherInfo.Text = dtPIInfo.Rows[0]["Remarks"].ToString();

                CommonMethods.PIStatusDropdown(ddlStatus, 1, 0);
                ddlStatus.SelectedIndex = CommonMethods.MatchDropDownItem(ddlStatus, int.Parse((string.IsNullOrEmpty(dtPIInfo.Rows[0]["StatusId"].ToString()) ? "0" : dtPIInfo.Rows[0]["StatusId"].ToString())));

                

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
            }
        }



        private void CreateDataTableForViewState()
        {

            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("StyleId", typeof(string)));
            dt.Columns.Add(new DataColumn("OrderId", typeof(string)));
            //dt.Columns.Add(new DataColumn("ItemTypeId", typeof(string)));
            dt.Columns.Add(new DataColumn("ItemId", typeof(string)));

            dt.Columns.Add(new DataColumn("HSCode", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
            dt.Columns.Add(new DataColumn("UnitId", typeof(string)));
            dt.Columns.Add(new DataColumn("UnitPrice", typeof(string)));
            dt.Columns.Add(new DataColumn("TotalAmount", typeof(string)));



            for (int rowIndex = 0; rowIndex < dtPIItemDetails.Rows.Count; rowIndex++)
            {
                dr = dt.NewRow();

                dr["StyleId"] = dtPIItemDetails.Rows[rowIndex]["StyleId"];
                dr["OrderId"] = dtPIItemDetails.Rows[rowIndex]["OrderId"];
                //dr["ItemTypeId"] = dtPIItemDetails.Rows[rowIndex]["ItemTypeId"];
                dr["ItemId"] = dtPIItemDetails.Rows[rowIndex]["ItemId"];
                dr["HSCode"] = dtPIItemDetails.Rows[rowIndex]["HSCode"];
                dr["UnitId"] = dtPIItemDetails.Rows[rowIndex]["UnitId"];
                dr["Quantity"] = dtPIItemDetails.Rows[rowIndex]["Quantity"];
                dr["UnitPrice"] = dtPIItemDetails.Rows[rowIndex]["UnitPrice"];
                dr["TotalAmount"] = dtPIItemDetails.Rows[rowIndex]["TotalAmount"];


                dt.Rows.Add(dr);
            }


            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

        }


        private bool HasLC()
        {
            bool hasLC = false;

            string sql = $"Exec usp_GetLCInfoByPIId '{PIId}'";
            DataTable dt = unitOfWork.GetDataTableFromSql(sql);

            if (dt.Rows.Count > 0)
            {
                hasLC = true;
            }

            return hasLC;

        }

        protected void lnkbtnUpdateEntries_Click(object sender, EventArgs e)
        {

            if (ddlBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a buyer.')", true);
            }
            else if (String.IsNullOrEmpty(tbxPINumber.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter IP number.')", true);
            }
            else if (String.IsNullOrEmpty(tbxPIDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter PI date.')", true);
            }
            if (ddlSeason.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select the season.')", true);
            }
            else if (ddlYears.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select the year.')", true);
            }
            else if (!CheckItems())
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter item(s) info correctly.')", true);
            }
            else if (ddlYarnAndAccssSuppliers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select beneficiary.')", true);
            }
            else if (ddlItemBuyers.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select item buyer.')", true);
            }
            else if (String.IsNullOrEmpty(tbxItemOrderDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Enter item order date.')", true);
            }
            else if (ddlMerchandiser.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select a merchandiser.')", true);
            }
            else if (ddlMerchandisingLeader.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select merchandiser leader.')", true);
            }
            else if (ddlTenor.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('Select Payment Mode.')", true);
            }
            else
            {
                try
                {

                    var piInfo = unitOfWork.GenericRepositories<PIInfo>().GetByID(PIId);

                    var nPINumber = tbxPINumber.Text.ToString().Trim();
                    var buerId = int.Parse(ddlBuyers.SelectedValue);
                    var orderseasonId = int.Parse(ddlSeason.SelectedValue);
                    if (piInfo.PINumber.Trim() != nPINumber || piInfo.BuyerId != buerId || piInfo.OrderSeasonId != orderseasonId)
                    {
                        var isExist = unitOfWork.GenericRepositories<PIInfo>().Get(x => x.PINumber == nPINumber && x.BuyerId == buerId && x.OrderSeasonId == orderseasonId && x.Status != 0).Any();
                        if (isExist)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('PI already exist.')", true);
                            return;
                        }
                    }

                    piInfo.BuyerId = buerId;
                    piInfo.PINumber = nPINumber;
                    piInfo.PIDate = DateTime.Parse(tbxPIDate.Text);
                    piInfo.OrderSeasonId = orderseasonId;
                    piInfo.SeasonYearId = int.Parse(ddlYears.SelectedValue);
                    piInfo.BeneficiaryId = int.Parse(ddlYarnAndAccssSuppliers.SelectedValue);
                    piInfo.NominatedOrNot = int.Parse(ddlNominateOrNot.SelectedValue);
                    piInfo.BeneficiaryPhone = tbxBeneficiaryPhone.Text;
                    piInfo.BeneficiaryBankInfo = tbxBeneficiaryBankInfo.Text;
                    piInfo.ShipmentInfo = tbxShipment.Text.ToString();
                    piInfo.PartialShipment = int.Parse(ddlPartialShipment.SelectedValue);
                    piInfo.PIType = int.Parse(ddlPIType.SelectedValue);
                    piInfo.ItemBuyerId = int.Parse(ddlItemBuyers.SelectedValue);
                    piInfo.ApprovedDateByMdSir = DateTime.Parse(tbxApprovedDateByMdSir.Text);
                    piInfo.ItemOrderDate = DateTime.Parse(tbxItemOrderDate.Text);
                    piInfo.ItemOrderMonth = (DateTime.Parse(tbxItemOrderDate.Text)).ToString("MMMM");
                    piInfo.ItemOrderYear = (DateTime.Parse(tbxItemOrderDate.Text)).Year;
                    piInfo.Tenor = int.Parse(ddlTenor.SelectedValue);

                    piInfo.PaymentMode = tbxPaymentMode.Text.ToString();
                    piInfo.MerchandiserId = int.Parse(ddlMerchandiser.SelectedValue);
                    piInfo.MerchandiserLeaderId = int.Parse(ddlMerchandisingLeader.SelectedValue);
                    piInfo.TolaranceInfo = tbxTolerance.Text;
                    piInfo.Remarks = tbxOtherInfo.Text;
                    piInfo.Status = int.Parse(ddlStatus.SelectedValue);
                    piInfo.Revised = IsRevised;

                    piInfo.UpdateDate = DateTime.Now;
                    piInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;

                    unitOfWork.GenericRepositories<PIInfo>().Update(piInfo);
                    unitOfWork.Save();

                    unitOfWork.SaveAuditLog("Updated", piInfo.PINumber + "", "PIInfo", piInfo);

                    UpdatePIItems(PIId);

                    unitOfWork.Save();


                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewEditPIInformationv2.aspx');", true);

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            }


        }

        protected void lnkbtnStatusChange_Click(object sender, EventArgs e)
        {

                try
                {
                    var piInfo = unitOfWork.GenericRepositories<PIInfo>().GetByID(PIId);
                    piInfo.Status = int.Parse(ddlStatus.SelectedValue);
                    piInfo.ApprovedDateByMdSir = DateTime.Parse(tbxApprovedDateByMdSir.Text);

                    switch (piInfo.Status)
                    {
                        case 7:
                            piInfo.PIRecvedtfromFact = DateTime.Now;
                            break;
                        case 8:
                            piInfo.ForwardtoAuditdt = DateTime.Now;
                            break;
                        case 10:
                            piInfo.BankSubdt = DateTime.Now;
                            break;
                    }

                    piInfo.UpdateDate = DateTime.Now;
                    piInfo.UpdatedBy = CommonMethods.SessionInfo.UserName;
                    unitOfWork.GenericRepositories<PIInfo>().Update(piInfo);
                    unitOfWork.Save();

                    unitOfWork.SaveAuditLog("Updated", piInfo.PINumber + "", "PIInfo", piInfo);
                    unitOfWork.Save();

                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.success('Updated successfully.')", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect", "returnToPage('ViewEditPIInformationv2.aspx');", true);

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "')", true);
                }
            
        }

        private void UpdatePIItems(int piId)
        {

            var piItems = unitOfWork.GenericRepositories<PIItems>().Get(x => x.PIId == piId);

            foreach (var piItem in piItems)
            {
                unitOfWork.GenericRepositories<PIItems>().Delete(piItem);
            }

            List<PIItems> listPIItems = new List<PIItems>();

            for (int rowIndex = 0; rowIndex < rptPIItemEntryInfo.Items.Count; rowIndex++)
            {

                DropDownList ddlStyles = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                DropDownList ddlOrders = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlOrders");
                //DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                DropDownList ddlItems = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItems");
                DropDownList ddlUnit = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlUnit");

                int selectedOrderId = 0;
                if (ddlOrders.SelectedValue != "")
                {
                    selectedOrderId = int.Parse(ddlOrders.SelectedValue);
                }

                TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");

                Label lblTotalAmount = (Label)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("lblTotalAmount");


                if (ddlStyles.SelectedValue != "" && ddlItems.SelectedValue != "" && !string.IsNullOrEmpty(tbxQty.Text) && !string.IsNullOrEmpty(tbxUnitPrice.Text)) //&& ddlUnit.SelectedValue != ""
                {
                    Decimal QntityInKg = 0;
                    if (int.Parse(ddlUnit.SelectedValue.ToString()) == 1)
                    {
                        QntityInKg = (decimal)0.453592 * decimal.Parse(tbxQty.Text);
                    }

                    var pIITem = new PIItems()
                    {
                        PIId = piId,
                        StyleId = int.Parse(ddlStyles.SelectedValue),
                        OrderId = selectedOrderId,
                        // ItemTypeId = int.Parse(ddlItemTypes.SelectedValue),
                        ItemId = int.Parse(ddlItems.SelectedValue),
                        UnitId = int.Parse(ddlUnit.SelectedValue == "" ? "17" : ddlUnit.SelectedValue),
                        HSCode = tbxHSCode.Text,
                        Quantity = decimal.Parse(tbxQty.Text),
                        UnitPrice = decimal.Parse(tbxUnitPrice.Text),
                        ItemTotalAmount = decimal.Parse(lblTotalAmount.Text),
                        QuantityInKg = QntityInKg,
                        CreateDate = DateTime.Now,
                        CreatedBy = CommonMethods.SessionInfo.UserName
                    };


                    listPIItems.Add(pIITem);
                }

            }

            foreach (var item in listPIItems)
            {
                unitOfWork.GenericRepositories<PIItems>().Insert(item);
            }

        }

        protected void ddlStyles_SelectedIndexChanged(object sender, EventArgs e)
        {

            DropDownList ddlStyles = (DropDownList)sender;
            DropDownList ddlOrders = (DropDownList)((RepeaterItem)ddlStyles.NamingContainer).FindControl("ddlOrders");

            //DropDownList ddlItemTypes = (DropDownList)((RepeaterItem)ddlStyles.NamingContainer).FindControl("ddlItemTypes");
            DropDownList ddlItems = (DropDownList)((RepeaterItem)ddlStyles.NamingContainer).FindControl("ddlItems");


            if (ddlStyles.SelectedValue != "")
            {

                int orderId = 0;

                if (ddlOrders.SelectedValue != "")
                {
                    orderId = int.Parse(ddlOrders.SelectedValue);
                }


                //ddlItems.Items.Clear();


                CommonMethods.LoadOrderDropdownByStyle(ddlOrders, int.Parse(ddlStyles.SelectedValue), 1, 0);
                
            }
            else
            {
                ddlOrders.Items.Clear();
            }

        }


        //protected void ddlItemTypes_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //    DropDownList ddlItemTypes = (DropDownList)sender;
        //    DropDownList ddlItems = (DropDownList)((RepeaterItem)ddlItemTypes.NamingContainer).FindControl("ddlItems");

        //    DropDownList ddlStyles = (DropDownList)((RepeaterItem)ddlItemTypes.NamingContainer).FindControl("ddlStyles");
        //    DropDownList ddlOrders = (DropDownList)((RepeaterItem)ddlItemTypes.NamingContainer).FindControl("ddlOrders");


        //    if (ddlItemTypes.SelectedValue != "")
        //    {


        //        int styleId = 0;

        //        if (ddlStyles.SelectedValue != "")
        //        {
        //            styleId = int.Parse(ddlStyles.SelectedValue);
        //        }

        //        int orderId = 0;

        //        if (ddlOrders.SelectedValue != "")
        //        {
        //            orderId = int.Parse(ddlOrders.SelectedValue);
        //        }

        //        ddlItems.Items.Clear();

        //        CommonMethods.LoadYarnAndAccessoriesItemDropdownByOrderOrStyle(ddlItems, styleId, orderId, 1, 0);

        //    }
        //    else
        //    {
        //        ddlItems.Items.Clear();
        //    }

        //}

        protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
        {

            DropDownList ddlOrders = (DropDownList)sender;
            DropDownList ddlStyles = (DropDownList)((RepeaterItem)ddlOrders.NamingContainer).FindControl("ddlStyles");

            //DropDownList ddlItemTypes = (DropDownList)((RepeaterItem)ddlOrders.NamingContainer).FindControl("ddlItemTypes");
            DropDownList ddlItems = (DropDownList)((RepeaterItem)ddlOrders.NamingContainer).FindControl("ddlItems");


            //if (ddlOrders.SelectedValue != "")
            //{

            int styleId = 0;

            if (ddlStyles.SelectedValue != "")
            {
                styleId = int.Parse(ddlStyles.SelectedValue);
            }


            int orderId = 0;

            if (ddlOrders.SelectedValue != "")
            {
                orderId = int.Parse(ddlOrders.SelectedValue);
            }

            //int itemTypeId = 0;

            //    if (ddlItemTypes.SelectedValue != "")
            //    {
            //        itemTypeId = int.Parse(ddlItemTypes.SelectedValue);
            //    }

            ddlItems.Items.Clear();

            CommonMethods.LoadYarnAndAccessoriesItemDropdownByOrderOrStyle(ddlItems, styleId, orderId, 1, 0);
            //CommonMethods.LoadOrderDropdownByStyle(ddlOrders, int.Parse(ddlStyles.SelectedValue), 1, 0);
            //}
            //else
            //{
            //    ddlItems.Items.Clear();
            //}

        }



        private void CalculateGrandTotal()
        {
            decimal grandQtyTotal = 0;
            decimal grandDollarTotal = 0;

            for (int i = 0; i < this.rptPIItemEntryInfo.Items.Count; i++)
            {


                TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[i].FindControl("tbxQty");
                TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[i].FindControl("tbxUnitPrice");

                if (tbxQty.Text != "" && tbxUnitPrice.Text != "")
                {

                    decimal quantity = Convert.ToDecimal(tbxQty.Text);
                    decimal unitPrice = Convert.ToDecimal(tbxUnitPrice.Text);

                    decimal totalAmount = quantity * unitPrice;

                    grandQtyTotal = grandQtyTotal + quantity;
                    grandDollarTotal = grandDollarTotal + totalAmount;



                }

            }

            Label lblTotalQuantity = (Label)rptPIItemEntryInfo.Controls[rptPIItemEntryInfo.Controls.Count - 1].Controls[0].FindControl("lblTotalQuantity");
            Label lblGrandTotalAmount = (Label)rptPIItemEntryInfo.Controls[rptPIItemEntryInfo.Controls.Count - 1].Controls[0].FindControl("lblGrandTotalAmount");

            lblTotalQuantity.Text = Math.Round(grandQtyTotal, 2).ToString();
            lblGrandTotalAmount.Text = Math.Round(grandDollarTotal, 2).ToString();

        }


        protected void btnCopy_Click(object sender, CommandEventArgs e)
        {
            int rowNumberWillCopy = Convert.ToInt32(e.CommandArgument.ToString());

            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    var StyleId = "";
                    var OrderId = "";
                    var ItemTypeId = "";
                    var ItemId = "";
                    var UnitId = "";
                    var HSCode = "";
                    var Quantity = "";
                    var UnitPrice = "";
                    var TotalAmount = "";

                    for (int rowIndex = 0; rowIndex < dtCurrentTable.Rows.Count; rowIndex++)
                    {


                        DropDownList ddlStyles = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlStyles");
                        DropDownList ddlOrders = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlOrders");
                        // DropDownList ddlItemTypes = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItemTypes");
                        DropDownList ddlItems = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlItems");
                        DropDownList ddlUnit = (DropDownList)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("ddlUnit");



                        TextBox tbxHSCode = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxHSCode");
                        TextBox tbxQty = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxQty");
                        TextBox tbxUnitPrice = (TextBox)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("tbxUnitPrice");
                        Label lblTotalAmount = (Label)this.rptPIItemEntryInfo.Items[rowIndex].FindControl("lblTotalAmount");

                        dtCurrentTable.Rows[rowIndex]["StyleId"] = ddlStyles.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["OrderId"] = ddlOrders.SelectedValue;
                        //dtCurrentTable.Rows[rowIndex]["ItemTypeId"] = ddlItemTypes.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["ItemId"] = ddlItems.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["UnitId"] = ddlUnit.SelectedValue;
                        dtCurrentTable.Rows[rowIndex]["HSCode"] = tbxHSCode.Text;
                        dtCurrentTable.Rows[rowIndex]["Quantity"] = tbxQty.Text;
                        dtCurrentTable.Rows[rowIndex]["UnitPrice"] = tbxUnitPrice.Text;
                        dtCurrentTable.Rows[rowIndex]["TotalAmount"] = lblTotalAmount.Text;

                        if (rowIndex == rowNumberWillCopy)
                        {
                            StyleId = ddlStyles.SelectedValue;
                            OrderId = ddlOrders.SelectedValue;
                            ItemId = ddlItems.SelectedValue;
                            UnitId = ddlUnit.SelectedValue;
                            HSCode = tbxHSCode.Text;
                            Quantity = tbxQty.Text;
                            UnitPrice = tbxUnitPrice.Text;
                            TotalAmount = lblTotalAmount.Text;
                        }


                        if (rowIndex + 1 >= dtCurrentTable.Rows.Count)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["StyleId"] = StyleId;
                            drCurrentRow["OrderId"] = OrderId;
                            drCurrentRow["ItemId"] = ItemId;
                            drCurrentRow["UnitId"] = UnitId;
                            drCurrentRow["HSCode"] = HSCode;
                            drCurrentRow["Quantity"] = Quantity;
                            drCurrentRow["UnitPrice"] = UnitPrice;
                            drCurrentRow["TotalAmount"] = TotalAmount;
                        }
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptPIItemEntryInfo.DataSource = dtCurrentTable;
                    this.rptPIItemEntryInfo.DataBind();

                }
            }
            this.SetPreviousData();
            CalculateGrandTotal();
        }

        int RowId
        {
            set { ViewState["rowId"] = value; }
            get
            {
                try
                {
                    return Convert.ToInt32(ViewState["rowId"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        protected void btnRemove_Command(object sender, CommandEventArgs e)
        {
            try
            {
                RowId = Convert.ToInt32(e.CommandArgument.ToString());
                //var IsDeletable = bool.Parse(unitOfWork.GetSingleStringValue($"EXEC usp_CheckBookingIsDeleteable {BookingId}"));
                var IsDeletable = true;
                if (IsDeletable)
                {
                    var title = "Warning";
                    var msg = "Are you sure you want to Delete?";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','confirmDelete','350');", true);
                }
                else
                {
                    var title = "Warning";
                    var msg = "Sorry, you can not delete it.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "showConfirm('" + title + "','" + msg + "','','400','true');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.error('" + Tools.GetErrorMessage(ex.Message) + "');", true);
            }

        }


        private void DeleteOperation()
        {
            //int rowWillRemove = Convert.ToInt32(e.CommandArgument.ToString());

            if (this.ViewState["CurrentTable"] == null)
            {
                base.Response.Write("ViewState is null");
            }
            else
            {
                DataTable dtCurrentTable = (DataTable)this.ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {

                    dtCurrentTable.Rows.RemoveAt(RowId);
                    this.ViewState["CurrentTable"] = dtCurrentTable;

                    this.rptPIItemEntryInfo.DataSource = dtCurrentTable;
                    this.rptPIItemEntryInfo.DataBind();

                }
            }
            this.SetPreviousData();
            CalculateGrandTotal();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "toastr_message", "toastr.success('Row deleted successfully.');", true);
        }
    }
}


