﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="MBTracker.Pages.Error" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .span4 {
            width: 375px;
        }
    </style>
    <div class="span4"></div>
    <div class="span4">
        <h1 style="text-align: center"><font face="NewRoman" style="font-size: 30px">Ops, Something went wrong!</font></h1>
        <h6 style="text-align: center"><font face="NewRoman">Please contact system administrator.</font></h6>
    </div>

    <div class="span4"></div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
