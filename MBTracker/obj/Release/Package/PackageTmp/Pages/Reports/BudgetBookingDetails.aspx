﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="BudgetBookingDetails.aspx.cs" Inherits="MBTracker.Pages.Reports.BudgetBookingDetails" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
     <asp:UpdatePanel ID="orderUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div class="row-fluid">
                <div class="span9" id="rptCriteria" runat="server">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title">
                                <span class="fs1" aria-hidden="true" data-icon=""></span>
                                <asp:Label ID="Label10" runat="server" Text="Yearly Budget Booking Details:"></asp:Label>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="row-fluid" runat="server">
                                <div class="form-horizontal">

                                    <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px;">
                                        <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field
                                    </div>

                                    <div class="col-md-6">

                                        <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="lblBudgetYear" runat="server" Text="Select Year:"></asp:Label><span style="font-weight: 500; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlBudgetYear" runat="server" Display="Dynamic" Width="80%" CssClass="form-control"></asp:DropDownList>

                                            </div>
                                        </div>
                                        
                                        <%--<div class="control-group">
                                            <label for="inputMonth" class="control-label">
                                                <asp:Label ID="lblMonth" runat="server" Text="Select Month:"></asp:Label></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlMonths" runat="server" Display="Dynamic" Width="80%" CssClass="form-control"></asp:DropDownList>

                                            </div>
                                        </div>--%>

                                        <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="80%" CssClass="form-control"></asp:DropDownList>

                                            </div>
                                        </div>
                                        
                                    </div>

                                    <div class="col-md-6">

                                        <div class="control-group">
                                            <label for="inputBuyer" class="control-label"></label>
                                            <div class="controls controls-row">
                                            </div>
                                        </div>

                                         <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                              </label>
                                        </div>

                                        <div class="pull-center" style="text-align: center; font-size: 96px; line-height: 30px;">
                                            <asp:Button ID="btnSubmit" runat="server" ValidationGroup="save" class="btn btn-info pull-right" Text="Show Report" OnClick="btnSubmit_Click" />
                                        </div>

                                    </div>

                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="span12" style="margin-left: 0px;">
                <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="<br/>No data found." BackColor="#ffff00"></asp:Label>
            </div>
            <div class="row-fluid" runat="server" id="divReport" visible="true">
                <div class="span12">
                    <div class="widget" style="border: 0px;">
                        <div class="widget-body" style="border: 0px; padding: 0px;">
                            <div style="margin-left: 0%">
                                <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%" ShowToolBar="false" SizeToReportContent="false" Style="overflow-x: auto; min-height: 410px" Height="500px">
                                </rsweb:ReportViewer>
                                <div class="span12" style="margin-top: 20px; margin-left: 0px;">
                                    <asp:Button ID="btnExportToEXCEL" runat="server" Visible="false" class="btn btn-info pull-left" Text="Export to Excel" OnClick="btnExportToExcel_Click" />
                                    <asp:Button ID="btnExportToPDF" runat="server" Visible="false" class="btn btn-info pull-left hidden" Text="Export to Pdf" OnClick="btnExportToPdf_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToEXCEL" />
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
