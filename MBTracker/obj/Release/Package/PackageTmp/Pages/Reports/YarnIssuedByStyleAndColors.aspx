﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="YarnIssuedByStyleAndColors.aspx.cs" Inherits="MBTracker.Pages.Reports.YarnIssuedByStyleAndColors" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <asp:UpdatePanel ID="orderUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <style>
                .widget-body {
                    margin-top: -1px;
                }

                .table th, .table td {
                    text-align: center;
                }

                #cpMain_cblProductionUnits td {
                    display: inline-flex;
                    margin-right: 10px;
                }

                    #cpMain_cblProductionUnits td > label {
                        display: inline !important;
                        line-height: 19px;
                        margin-left: 5px;
                    }
            </style>
            <div class="row-fluid" runat="server" id="rptCriteria">
                <div class="span12">
                    <div class="span9">
                        <div class="widget">
                            <div class="widget-header">
                                <div class="title">
                                    <span class="fs1" aria-hidden="true" data-icon=""></span>
                                    <asp:Label runat="server" ID="lblMachinePlanning" Text="Yarn Issue Details Report:"></asp:Label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="widget-body" style="min-height: 280px">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                                            <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000"></span>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                        </div>

                                        <div class="col-md-6">
                                            <div class="control-group">
                                                <label for="inputBuyer" class="control-label">
                                                    <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                                <div class="controls controls-row">
                                                    <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="90%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label for="inputStyle" class="control-label">
                                                    <asp:Label ID="lblStyle" runat="server" Text="Select Style (Optional):"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                                <div class="controls controls-row">
                                                    <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                                        ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label for="inputBuyer" class="control-label">
                                                    <asp:Label ID="Label1" runat="server" Text="Select Color (Optional):"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                                <div class="controls controls-row">
                                                    <asp:DropDownList ID="ddlColors" runat="server" AutoPostBack="true" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="control-group">
                                                <label for="inputBuyer" class="control-label">
                                                    <asp:Label ID="Label5" runat="server" Text="From Date (Optional):"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                                <div class="controls controls-row">
                                                    <asp:TextBox ID="tbxFromDate" runat="server" placeholder="Enter from date" Width="65%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label for="inputBuyer" class="control-label">
                                                    <asp:Label ID="Label6" runat="server" Text="To Date (Optional):"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                                <div class="controls controls-row">
                                                    <asp:TextBox ID="tbxToDate" runat="server" placeholder="Enter to date" Width="65%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label for="inputStyle" class="control-label">
                                                    <asp:Label ID="Label3" runat="server" Text="Knitting Unit (Optional):"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                                <div class="controls controls-row">
                                                    <%--<asp:DropDownList ID="ddlKnittingUnits" runat="server" AutoPostBack="true" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>--%>
                                                    <asp:CheckBoxList ID="cblProductionUnits" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="3" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="display: flex; justify-content: center; padding-top: 15px;">
                                            <br />
                                            <asp:Button ID="btnViewIssues" runat="server" class="btn btn-success btn-midium pull-left btnStyle" Text="View Yarn Issues" OnClick="btnViewIssues_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />

            <div class="span12" style="margin-left: 0px;">
                <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="<br/>No data found." BackColor="#ffff00"></asp:Label>
            </div>
            <div class="row-fluid" runat="server" id="divReport" visible="true">
                <div class="span12">
                    <div class="widget" style="border: 0px;">
                        <div class="widget-body" style="border: 0px; padding: 0px;">
                            <div style="margin-left: 0%">
                                <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%" ShowToolBar="false" SizeToReportContent="false" Style="overflow-x: auto; min-height: 210px" Height="500px">
                                </rsweb:ReportViewer>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left: 0px;">
                    <asp:Button ID="btnExportToEXCEL" runat="server" class="btn btn-info pull-left" Visible="false" Text="Export to Excel" OnClick="btnExportToExcel_Click" />
                    <asp:Button ID="btnExportToPDF" runat="server" class="btn btn-info pull-left hidden" Visible="false" Text="PDF" OnClick="btnExportToPdf_Click" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToEXCEL" />
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
