﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="FinishingStatus.aspx.cs" Inherits="MBTracker.Pages.Reports.FinishingStatus" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <div class="row-fluid">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <asp:Label ID="lblReportPageHeading" runat="server" Text="Finishing Status Report"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label1" runat="server" Text="&nbsp;"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                            <div class="controls controls-row">
                                <br />
                                <asp:Button ID="btnSubmit" runat="server" class="btn btn-info pull-left"   Text="Show Report" OnClick="btnSubmit_Click" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <br />
    <br />

    <div class="row-fluid" runat="server" id="divReport" visible="false">
        <div class="span12">
            <div class="widget">
                <div class="widget-body">
                    <div style="margin-left: 0%">
                        <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%" ShowToolBar="false" SizeToReportContent="true" Style="overflow-x: auto; min-height: 210px">
                        </rsweb:ReportViewer>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
