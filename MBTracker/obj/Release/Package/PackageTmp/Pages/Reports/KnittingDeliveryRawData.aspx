﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="KnittingDeliveryRawData.aspx.cs" Inherits="MBTracker.Pages.Reports.KnittingDeliveryRawData" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <asp:UpdatePanel ID="orderUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div class="row-fluid">
                <div class="span6">
                    <div class="widget">

                        <div class="widget-header">
                            <div class="title">
                                <span class="fs1" aria-hidden="true" data-icon=""></span>
                                <asp:Label ID="lblReportPageHeading" runat="server" Text="Knitting Delivery Raw Data:"></asp:Label>
                            </div>
                        </div>

                        <div class="widget-body">

                            <div class="row-fluid" runat="server" visible="true" id="FilterDiv">
                                <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                                    <%--<span style="font-weight: 700; padding-right: 400px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field--%>
                                </div>
                                <div class="control-group">
                                    <div class="col-md-12 col-sm-12">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                    </div>
                                </div>
                                <%--                                <div class="col-md-10" style="padding-left: 0px">

                                    <div class="col-md-9" style="padding-left: 0px">--%>
                                <div class="form-horizontal">

                                    <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px;">
                                        <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field
                                    </div>
                                    <div class="col-md-12">
                                        <div class="control-group">
                                            <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: right">
                                                <asp:Label ID="lblFilterDate" runat="server" Text="Delivery Date:"></asp:Label>
                                                <span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;</span>
                                            </label>
                                            <div class="controls controls-row" style="margin-left: 150px">
                                                <asp:TextBox ID="tbxFromDate" runat="server" placeholder="Enter date" Width="60%" style="float:left" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxFromDate"><span style="font-weight: 700; float:left; width:100%; color: #CC0000">Please select delivery date.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                       <%-- <div class="control-group">
                                            <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: right">
                                                <asp:Label ID="Label2" runat="server" Text="To Date:"></asp:Label>
                                                <span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;</span>
                                            </label>
                                            <div class="controls controls-row" style="margin-left: 150px">
                                                <asp:TextBox ID="tbxToDate" runat="server" placeholder="Enter date" Width="65%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxToDate"><span style="font-weight: 700;float:left; color: #CC0000">Please select to date.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>--%>
                                        <div class="control-group">
                                        <br />
                                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">
                                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                        </label>
                                        <div class="contents controls-row">
                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="Show Report" CssClass="btn btn-info pull-left" OnClick="btn_showReport" />
                                        </div>
                                    </div>
                                    </div>
                                    <%--<div class="col-md-6">
                                        <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="65%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlStyles" runat="server" Display="Dynamic" Width="65%" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>--%>
                                    
                                </div>
                                <%--  </div>
                                </div>--%>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>

            <br />
            <br />

            <div class="row-fluid" runat="server" id="divReport" visible="true">
                <div class="span12">
                    <div class="widget" style="border: none">
                        <div class="widget-body" style="border: none">
                            <div style="margin-left: 0%">
                                <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%" ShowToolBar="false" Style="overflow-x: auto; min-height: 210px">
                                </rsweb:ReportViewer>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left: 0px;">
                    <asp:Button ID="btnExportToEXCEL" runat="server" class="btn btn-info pull-left" Visible="false" Text="Export to Excel" OnClick="btnExportToExcel_Click" />
                    <asp:Button ID="btnExportToPDF" runat="server" class="btn btn-info pull-left hidden" Visible="false" Text="PDF" OnClick="btnExportToPdf_Click" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToEXCEL" />
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
