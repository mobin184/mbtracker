﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="MonthlyProductionPlan.aspx.cs" Inherits="MBTracker.Pages.Reports.MonthlyProductionPlan" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <div class="row-fluid">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <asp:Label ID="lblReportPageHeading" runat="server" Text="Monthly Production Plans - Report"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 10px">
                                    <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                                </div>
                        <div class="control-group">
                                <div class="col-md-12 col-sm-12">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                </div>
                            </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Production Month:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlProductionMonths" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlProductionMonths"><span style="font-weight: 700; color: #CC0000">Please select a month.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label1" runat="server" Text="&nbsp;"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                            <div class="controls controls-row">
                                <br />
                                <asp:Button ID="btnSubmit" runat="server" ValidationGroup="save" class="btn btn-info pull-left"   Text="Show Report" OnClick="btnSubmit_Click" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <br />

    <div class="span12" style="margin-left:0px;">
        <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="<br/>No production plan found." BackColor="#ffff00"></asp:Label>
    </div>
    <div class="row-fluid" runat="server" id="divReport" visible="true">
        <div class="span12">            
            <div class="widget" style="border:0px;">
                <div class="widget-body" style="border:0px;padding:0px;">
                    <div style="margin-left: 0%">
                        <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%" ShowToolBar="false" SizeToReportContent="true" Style="overflow-x: auto; min-height: 210px">
                        </rsweb:ReportViewer>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
