﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ShipmentSummaryReport.aspx.cs" Inherits="MBTracker.Pages.Reports.ShipmentSummaryReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <asp:UpdatePanel ID="orderUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <style>
                .form-horizontal .control-label {
                    float: left;
                    width: 160px;
                    padding-top: 0px;
                    text-align: right;
                }
            </style>
            <div class="row-fluid">
                <div class="span9" id="divCriteria" runat="server">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title">
                                <asp:Label ID="lblReportPageHeading" runat="server" Text="Shipment Summary Report:"></asp:Label>
                            </div>
                        </div>
                        <div class="widget-body" style="min-height: 230px;">
                            <div class="row">
                                <div class="form-horizontal">
                                    <div class="col-md-12">
                                        <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 10px">
                                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                                        </div>
                                        <div class="control-group">
                                            <div class="col-md-12 col-sm-12">
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000; float:left">Please select a buyer.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="lblStyle" runat="server" Text="Select Style (optional):"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select a style.</span></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                        <div class="control-group hidden">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="lblOrder" runat="server" Text="Select Order (optional):"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control" OnSelectedIndexChanged="ddlOrders_SelectedIndexChanged"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="Label1" runat="server" Text="Select Color (optional):"></asp:Label></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlColors" runat="server" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="Label5" runat="server" Text="Enter From Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxFromDate" Style="z-index: 99; position: relative;"
                                                    runat="server" placeholder="Enter from date" Width="70%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxFromDate"><span style="font-weight: 700; color: #CC0000; float:left;width:100%">Enter from date.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                         <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="Label6" runat="server" Text="Enter To Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                             <div class="controls controls-row">
                                            <asp:TextBox ID="tbxToDate" runat="server" placeholder="Enter to date" Width="70%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxToDate"><span style="font-weight: 700; color: #CC0000; float:left;width:100%">Enter to date.</span></asp:RequiredFieldValidator>
                                        </div>
                                        </div>
                                        <div class="control-group hidden">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="Label2" runat="server" Text="Order Year (optional):"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlOrderYear" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="control-group hidden">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="Label3" runat="server" Text="Order Season (optional):"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlOrderSeason" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="control-group hidden">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="Label4" runat="server" Text="Department (optional):"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="display: inline-flex; justify-content: center; margin-top: 15px">
                                        <asp:Button ID="btnSubmit" runat="server" ValidationGroup="save" class="btn btn-info pull-left" Text="Show Report" OnClick="btnSubmit_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div class="span12" style="margin-left: 0px;">
                <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="<br/>No data found." BackColor="#ffff00"></asp:Label>
            </div>
            <div class="row-fluid" runat="server" id="divReport" visible="true">
                <div class="span12">
                    <div class="widget" style="border: 0px;">
                        <div class="widget-body" style="border: 0px; padding: 0px;">
                            <div style="margin-left: 0%">
                                <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%" ShowToolBar="false" SizeToReportContent="false" Style="overflow-x: auto; min-height: 210px" Height="500px">
                                </rsweb:ReportViewer>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left: 0px;">
                    <asp:Button ID="btnExportToEXCEL" runat="server" class="btn btn-info pull-left" Visible="false" Text="Export to Excel" OnClick="btnExportToExcel_Click" />
                    <asp:Button ID="btnExportToPDF" runat="server" class="btn btn-info pull-left hidden" Visible="false" Text="PDF" OnClick="btnExportToPdf_Click" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToEXCEL" />
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
