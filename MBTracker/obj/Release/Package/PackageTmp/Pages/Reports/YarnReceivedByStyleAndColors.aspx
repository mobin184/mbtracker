﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="YarnReceivedByStyleAndColors.aspx.cs" Inherits="MBTracker.Pages.Reports.YarnReceivedByStyleAndColors" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">


     <asp:UpdatePanel ID="orderUpdatePanel" UpdateMode="Conditional" runat="server">
         <ContentTemplate>
              <style>
                .widget-body {
                    margin-top: -1px;
                }

                .table th, .table td {
                    text-align: center;
                }
            </style>


                         <div class="row-fluid" runat="server" id="filterDiv"> 
                <div class="span12">
                    <div class="span6">
                        <div class="widget">
                            <div class="widget-header">
                                <div class="title">
                                    <span class="fs1" aria-hidden="true" data-icon=""></span>
                                    <asp:Label runat="server" ID="lblMachinePlanning" Text="Yarn Receive History:"></asp:Label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="widget-body">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <div class="col-md-12 col-sm-12">
                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>


                                    <div class="control-group">
                                        <label for="inputStyle" class="control-label">
                                            <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" CssClass="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="Label2" runat="server" Text="&nbsp;"></asp:Label></label>
                                        <div class="controls controls-row">
                                            <br />
                                            <asp:Button ID="btnViewReceived" ValidationGroup="save" runat="server" class="btn btn-success btn-midium pull-left btnStyle" Text="View Yarn Received" OnClick="btnViewReceived_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <br />


             
            <div class="span12" style="margin-left: 0px;">
                <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="<br/>No data found." BackColor="#ffff00"></asp:Label>
            </div>

              <div class="row-fluid" runat="server" id="divReport" Visible="true">
                <div class="span12">
                    <div class="widget" style="border: 0px;">
                        <div class="widget-body" style="border: 0px; padding: 0px;">
                            <div style="margin-left: 0%">
                                <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%" ShowToolBar="false" SizeToReportContent="true" Style="overflow-x: auto; min-height: 210px">
                                </rsweb:ReportViewer>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left:0px;">
                    <asp:Button ID="btnExportToEXCEL" runat="server" class="btn btn-info pull-left" Visible="false" Text="Export to Excel" OnClick="btnExportToExcel_Click" />
                    <asp:Button ID="btnExportToPDF" runat="server" class="btn btn-info pull-left hidden" Visible="false" Text="PDF" OnClick="btnExportToPdf_Click" />
                </div>
            </div>



        </ContentTemplate>

          <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToEXCEL" />
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
        </Triggers>


     </asp:UpdatePanel>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
