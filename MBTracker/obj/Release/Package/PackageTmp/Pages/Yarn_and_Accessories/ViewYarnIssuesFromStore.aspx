﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewYarnIssuesFromStore.aspx.cs" Inherits="MBTracker.Pages.Yarn_and_Accessories.ViewYarnIssuesFromStore" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .widget-body {
            margin-top: -1px;
        }

        .table th, .table td {
            text-align: center;
        }
    </style>
    <div class="row-fluid">
        <div class="span12">
            <div class="span6">
                <div class="widget">

                    <div class="widget-header">
                        <div class="title">
                            <span class="fs1" aria-hidden="true" data-icon=""></span>
                            <asp:Label runat="server" ID="lblMachinePlanning" Text="View & Edit Issued Yarn from Store:"></asp:Label>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="widget-body">
                        <div class="form-horizontal">

                            <div class="control-group">
                                <div class="col-md-12 col-sm-12">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>


                            <div class="control-group">
                                <label for="inputStyle" class="control-label">
                                    <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>

                           <div class="control-group">
                            <label for="inputStyle" class="control-label">
                                <asp:Label ID="Label7" runat="server" Text="Date Issued (Optional):"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxDateIssued" runat="server" Width="60%" TextMode="Date"></asp:TextBox>
                            </div>
                        </div>

                            <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="&nbsp;"></asp:Label></label>
                            <div class="controls controls-row">
                                <br />
                                <asp:Button ID="btnViewIssues" ValidationGroup="save" runat="server"  class="btn btn-success btn-midium pull-left btnStyle" Text="View Yarn Issues" OnClick="btnViewIssues_Click" />
                            </div>
                         </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row-fluid">        
        <div class="span9">
            <asp:Label ID="lblNoIssueFound" runat="server" Visible="false" Text="<br/><br/>No data found." BackColor="#ffff00"></asp:Label>
            <asp:Panel ID="pnlSummaryView" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-body">
                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label4" runat="server" Text="Issued Yarn Summary:" Font-Bold="true"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:Repeater ID="rptYarnIssueInfo" runat="server">
                                    <HeaderTemplate>
                                        <table id="data-table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lable1" runat="server" Text="Issue Date"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lable2" runat="server" Text="Production Units"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lable3" runat="server" Text="Colors"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lable4" runat="server" Text="Issued Quantity"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label5" runat="server" Text="Actions"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td class="hidden"><%#Eval("IssueNumber") %>></td>
                                            <td><%#String.Format("{0:dd-MMM-yyyy}",Eval("Date")) %></td>
                                            <td><%#Eval("ProductionUnits") %></td>
                                            <td><%#Eval("Colors") %></td>
                                            <td style="text-align: center"><%#Eval("Quantity") %></td>
                                            <td style="text-align: center">
                                                <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete"  Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("IssueNumber") %>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkbtnEdit" Visible='<%# ViewState["editEnabled"]%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("IssueNumber") %>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="Edit" CommandArgument='<%#Eval("IssueNumber") %>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnView_Command" Text="View Details"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                            </table>
                                    </FooterTemplate>
                                </asp:Repeater>                                
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div class="span3">
            <div class="widget-body">
                <asp:Panel ID="pnlPlanningInfo" runat="server" Visible="false">
                    <div class="widget">
                        <div class="widget-body" style="overflow-x: auto">
                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="Label5" runat="server" Text="Knitting Information:" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rptPlanInfo" runat="server">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblOrderQty" runat="server" Text="Order<br/>Quantity"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblMachineBrand" runat="server" Text="Machine<br/>Brand"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblMachineGauge" runat="server" Text="Machine<br/>Gauge"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblKnittingTime" runat="server" Text="Knitting<br/>Time"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center" width="15%"><%#Eval("OrderQuantity") %> </td>
                                                <td style="text-align: center" width="35%">
                                                    <asp:Label ID="lblMCBrandName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BrandName")%>'></asp:Label><asp:Label ID="lblMCBrandId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "MachineBrandId")%>'></asp:Label>
                                                    <asp:DropDownList ID="ddlMachineBrands" runat="server" Display="Dynamic" CssClass="form-control" Visible="false"></asp:DropDownList>
                                                </td>
                                                <td style="text-align: center" width="15%">
                                                    <asp:Label ID="lblMCGauge" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingMachineGauge")%>'></asp:Label>
                                                    <asp:TextBox ID="tbxKnittingMCGauge" runat="server" CssClass="form-control" placeholder="MC Gauge" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingMachineGauge")%>'></asp:TextBox>
                                                </td>
                                                <td style="text-align: center" width="15%">
                                                    <asp:Label ID="lblKnittingTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingTime")%>'></asp:Label>
                                                    <asp:TextBox ID="tbxKnittingTime" runat="server" placeholder="Knitting time" CssClass="form-control" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingTime")%>'></asp:TextBox>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <asp:Label ID="lblNoPlanningInfo" runat="server" Visible="false" Text="<br/><br/>No planning info found." BackColor="#ffff00"></asp:Label>
                                </div>
                            </div>

                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="span12" style="margin-left: 0px;">
            <div id="dt_example" class="example_alt_pagination">
                <asp:Panel ID="pnlAssignMachinesToOrder" runat="server" Visible="false">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="control-group">
                                <label for="inputBuyer" class="control-label"></label>
                                <div class="controls controls-row">
                                    <asp:Label ID="Label6" runat="server" Text="Issued Yarn Information:" Font-Bold="true" Font-Underline="true" Font-Size="Larger"></asp:Label>
                                </div>
                            </div>

                            <div style="text-align: left; font-size: 12px; line-height: 20px; padding-bottom: 5px">
                                <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            </div>

                            <div class="control-group" runat="server" id="divKnittingPart">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="Label1" runat="server" Text="Knitting Part:"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Label ID="lblKnittingPart" Width="300px" CssClass="form-control" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="control-group" runat="server" id="divYarnDes">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="lblYarnDescription" runat="server" Text="Yarn Description:"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Label ID="lblYarnDecription" Width="300px" CssClass="form-control" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="Label3" runat="server" Font-Bold="true" Style="font-size: 12px" Font-Size="Larger" Text="Yarn Issued:"></asp:Label></label>
                                <div class="controls controls-row" style="overflow-x:auto">
                                    <asp:Repeater ID="rpt" runat="server" OnItemDataBound="rpt_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>

                                                        <th>
                                                            <asp:Label ID="Label2" runat="server" Text="Unit<br/>Name"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Yarn<br/> Issued"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label4" runat="server" Text="Knitting<br/>Start Date"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label12" runat="server" Text="Yarn Quantity By Size"></asp:Label></th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="width: 80px"><%#Eval("UnitName") %><asp:Label ID="lblUnitId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "UnitId")%>'></asp:Label>
                                                </td>
                                                <td style="width: 120px"><%#Eval("IssueQuantity") %> </td>
                                                <td style="width: 80px"><%#String.Format("{0:dd-MMM-yyyy}",Eval("ProdStartDate"))%><asp:Label ID="lblMachinAssignedId" runat="server" Visible="false" Text='<%#Eval("MCAssignedId")%>'></asp:Label>
                                                    <td>
                                                        <asp:Repeater ID="rptOrderColors" runat="server" OnItemDataBound="rptOrderColors_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="data-table" class="table table-bordered table-hover">
                                                                    <tbody>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>'></asp:Label><asp:Label ID="lblOrderColorDescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ColorDescription")%>' Width="100px"></asp:Label></th></td>
                                                                    <td>
                                                                        <asp:GridView ID="gvSizeQuantity" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity_RowCreated">
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tbody>
                                                        </table>
                                                        <div class="clearfix">
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                 </table>
                                    <div class="clearfix">
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <asp:Label runat="server" ID="lblMachineNotAssigned" Visible="false" Font-Bold="true" BackColor="#ffff00">Machine was not assigned for this order!</asp:Label>
                                </div>
                            </div>


                        </div>

                    </div>


                </asp:Panel>

            </div>

            <div class="clearfix"></div>

        </div>



    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
