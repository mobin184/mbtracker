﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" CodeBehind="ViewDyeingOrders.aspx.cs" Inherits="MBTracker.Pages.Yarn_and_Accessories.ViewDyeingOrders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <div class="row-fluid" runat="server" id="pnlEntry">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="View/Edit Dyeing Orders:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body" style="min-height: 250px">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="span-12" style="text-align: right; padding-right: 20px; margin-top:-10px; padding-bottom:20px;">
                            Please select buyer with style or dyeing order date.
                        </div>
                        <div class="span8">
                            <div class="control-group">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputStyle" class="control-label">
                                    <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputStyle" class="control-label">
                                    <asp:Label ID="Label4" runat="server" Text="Dyeing Order Date:"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxDODate" runat="server" Width="100%" TextMode="Date"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="span-12" style="display:flex; justify-content:center; padding-top:20px;">
                            <br />
                            <asp:Button ID="btnSearch" ValidationGroup="save" runat="server" class="btn btn-success btn-midium pull-left btnStyle" Text="View Entries" OnClick="btnSearch_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <asp:Label ID="lblSummryNotFound" runat="server" Text="No data found." Visible="false" BackColor="#ffff00"></asp:Label>
        <div class="span12" runat="server" id="pnlSummary" visible="false">
            <div class="widget">
                <div class="widget-body">
                    <div class="control-group">
                        <label for="inputBuyer" class="control-label">
                            <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Dyeing Order Information:"></asp:Label>
                        </label>
                    </div>
                    <div class="controls controls-row">
                        <div id="dt_example" class="example_alt_pagination">
                            <asp:Repeater ID="rptSummary" Visible="false" runat="server">
                                <HeaderTemplate>
                                    <table id="data-table" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>

                                                <th>
                                                    <asp:Label ID="Label5" runat="server" Text="Buyer"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label15" runat="server" Text="Style"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label6" runat="server" Text="Merchandiser"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="lbl2" runat="server" Text="Dyeing Order #"></asp:Label>
                                                </th>
                                                 <th>
                                                    <asp:Label ID="Label7" runat="server" Text="PI Number"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label8" runat="server" Text="Dyeing Order Date"></asp:Label>
                                                </th>
                                                 <th>
                                                    <asp:Label ID="Label9" runat="server" Text="Quantity (lbs)"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label3" runat="server" Text="Take  Action"></asp:Label>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <%#Eval("BuyerName") %>
                                        </td>
                                        <td>
                                            <%#Eval("StyleName") %>
                                        </td>
                                        <td>
                                            <%#Eval("MerchandiserName") %>
                                        </td>
                                        <td>
                                            <%#Eval("DyeingOrderNumber") %>
                                        </td>
                                        <td>
                                            <%#Eval("PINumber") %>
                                        </td>
                                        <td>
                                            <%#Eval("DyeingOrderDate") %>
                                        </td>
                                        <td>
                                            <%#Eval("DyeingOrderQuantity") %>
                                        </td>
  
                                        <td>
                                            <%--<asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("DyeingOrderId") %>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" Visible='<%# ViewState["editEnabled"]%>' CommandName="Edit" CommandArgument='<%#Eval("DyeingOrderId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>--%>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#Eval("CanDelete")%>' CommandArgument='<%#Eval("DyeingOrderId") %>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" Visible='<%# Eval("CanEdit")%>' CommandName="Edit" CommandArgument='<%#Eval("DyeingOrderId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnDetails" runat="server" CommandName="ViewDetails" CommandArgument='<%#Eval("DyeingOrderId") %>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnDetails_Command" Text="View Details"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                                </table>                                                  
                                </FooterTemplate>
                            </asp:Repeater>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="row-fluid">
        <asp:Label ID="lblNoDetails" runat="server" Text="No details found." Visible="false" BackColor="#ffff00"></asp:Label>
        <div class="span12" runat="server" id="pnlDetails" visible="false">
            <div class="widget">
                <div class="widget-body">
                    <div class="control-group">
                        <label for="inputBuyer" class="control-label">
                            <asp:Label ID="lblDetails" runat="server" Font-Bold="true" Text="Dyeing Order Details Information:"></asp:Label>
                        </label>
                    </div>
                    <div class="controls controls-row">
                        <div id="dt_example" class="example_alt_pagination">
                            <asp:Repeater ID="rptDetails" Visible="false" runat="server">
                                <HeaderTemplate>
                                    <table id="data-table1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>

                                               <%-- <th>
                                                    <asp:Label ID="Label5" runat="server" Text="Buyer"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label6" runat="server" Text="Style"></asp:Label>
                                                </th>--%>
                                                <th>
                                                    <asp:Label ID="Label8" runat="server" Text="Color"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="lbl2" runat="server" Text="Item Description"></asp:Label>
                                                </th>
                                                 <th>
                                                    <asp:Label ID="Label10" runat="server" Text="Lab Dip Ref"></asp:Label>
                                                </th>
                                                 <th>
                                                    <asp:Label ID="Label11" runat="server" Text="Shade"></asp:Label>
                                                </th>
                                                 <th>
                                                    <asp:Label ID="Label12" runat="server" Text="LOT Number"></asp:Label>
                                                </th>
                                                 <th>
                                                    <asp:Label ID="Label9" runat="server" Text="Quantity (lbs)"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label2" runat="server" Text="Remarks"></asp:Label>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>

                                       <%-- <td>
                                            <%#Eval("BuyerName") %>
                                        </td>
                                        <td>
                                            <%#Eval("StyleName") %>
                                        </td>--%>
                                        <td>
                                            <%#Eval("ColorName") %>
                                        </td>
                                        <td>
                                            <%#Eval("ItemName") %>
                                        </td>
                                        <td>
                                            <%#Eval("LabDipRef") %>
                                        </td>
                                        <td>
                                            <%#Eval("Shade") %>
                                        </td>
                                        <td>
                                            <%#Eval("LOTNumber") %>
                                        </td>
                                        <td>
                                            <%#Eval("Quantity") %>
                                        </td>
                                        <td>
                                            <%#Eval("Remarks") %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                                </table>                                                  
                                </FooterTemplate>
                            </asp:Repeater>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>

