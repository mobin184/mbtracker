﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewYarnBookings.aspx.cs" Inherits="MBTracker.Pages.Yarn_and_Accessories.ViewYarnBookings" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <div class="row-fluid" runat="server" id="pnlEntry">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="View/Edit Yarn Booking:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="form-horizontal">
                        <%-- <div style="text-align: center; font-size: 12px; line-height: 10px; padding-bottom: 10px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>--%>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputStyle" class="control-label">
                                <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="&nbsp;"></asp:Label></label>
                            <div class="controls controls-row">
                                <br />
                                <asp:Button ID="btnViewBookings" ValidationGroup="save" runat="server"  class="btn btn-success btn-midium pull-left btnStyle" Text="View Yarn Bookings" OnClick="btnViewBookings_Click" />
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <asp:Label ID="lblSummryNotFound" runat="server" Text="Yarn was not booked yet." Visible="false" BackColor="#ffff00"></asp:Label>
        <div class="span6" runat="server" id="pnlSummary" visible="false">
            <div class="widget">
                <div class="widget-body">
                   <%-- <div class="form-horizontal">--%>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Booking Information:"></asp:Label>
                            </label>
                        </div>
                        <div class="controls controls-row">
                            <div id="dt_example" class="example_alt_pagination">
                                <asp:Repeater ID="rptSummary" Visible="false" runat="server">
                                    <HeaderTemplate>
                                        <table id="data-table1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lbl1" runat="server" Text="Booking Date"></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lbl2" runat="server" Text="Booking Quantity"></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="Label3" runat="server" Text="Take  Actions"></asp:Label>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl1" Visible="true" runat="server" Text='<%#string.Format("{0:dd-MMM-yyyy hh:mm tt}",Eval("BookingDate"))%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbl2" Visible="true" runat="server" Text='<%#Eval("BookingQunatity")%>'></asp:Label>
                                            </td>
                                            <td>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete"  Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("BookingId") %>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkbtnEdit" runat="server" Visible='<%# ViewState["editEnabled"]%>' CommandName="Edit" CommandArgument='<%#Eval("BookingId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="View" CommandArgument='<%#Eval("BookingId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnView_Command" Text="View Details"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                                </table>                                                  
                                    </FooterTemplate>
                                </asp:Repeater>
                                
                            </div>
                        </div>

                  <%--  </div>--%>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid" id="pnlYarnBooking" runat="server" visible="false">
        <div class="span9">
            <div class="widget">
                <div class="widget-body">
                    <asp:Panel runat="server">
                        <%--<div class="form-horizontal">--%>
                            <div class="control-group">
                                <label for="inputDeliveryCountry" class="control-label" style="text-align: left; line-height:20px">
                                    <asp:Label ID="lblColorAndSizeTitle" Font-Bold="true" runat="server" Text="Yarn Booking Info:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                            </div>
                            <div class="controls controls-row">
                                <div id="dt_example" class="example_alt_pagination">
                                    <asp:Repeater ID="rptColors" Visible="false" OnItemDataBound="rptColors_ItemDataBound" runat="server">
                                        <HeaderTemplate>
                                            <table id="data-table1" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblColorInfo" runat="server" Text="Color"></asp:Label>
                                                        </th>
                                                        <th>
                                                            <asp:Label ID="lblBookingInfo" runat="server" Text="Booking Info"></asp:Label>
                                                        </th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblBuyerColorId" Visible="false" runat="server" Text='<%#Eval("BuyerColorId")%>'></asp:Label>
                                                    <asp:Label ID="lblColorDescription" Visible="true" runat="server" Text='<%#Eval("ColorDescription")%>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Repeater ID="rpt" runat="server">
                                                        <HeaderTemplate>
                                                            <table id="data-table2" class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 100px">
                                                                            <asp:Label ID="lbl1" runat="server" Text="Description"></asp:Label></th>
                                                                        <th style="width: 100px">
                                                                            <asp:Label ID="lbl2" runat="server" Text="Booking Qty"></asp:Label></th>
                                                                        <th style="width: 100px">
                                                                            <asp:Label ID="lbl3" runat="server" Text="Supplier"></asp:Label></th>
                                                                        <th style="width: 100px">
                                                                            <asp:Label ID="lbl4" runat="server" Text="Cons/Dzn"></asp:Label></th>
                                                                        <th style="width: 100px">
                                                                            <asp:Label ID="lbl5" runat="server" Text="Status"></asp:Label></th>

                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblYarnDesc" runat="server" Text='<%#Eval("YarnDescription")%>'></asp:Label></td>
                                                                </td>
                                                            <td>
                                                                <asp:Label ID="lblBookingQty" runat="server" Text='<%#Eval("BookingQuantity")%>'></asp:Label></td>
                                                                <td>
                                                                    <asp:Label ID="lblSupplierName" runat="server" Text='<%#Eval("Supplier")%>'></asp:Label></td>
                                                                <td>
                                                                    <asp:Label ID="lblConsumptionPerDzn" runat="server" Text='<%#Eval("ConsumptionPerDzn")%>'></asp:Label>
                                                                    <td>
                                                                        <asp:Label ID="lblYarnStatus" runat="server" Text='<%#Eval("ProcurementStatusName")%>'></asp:Label>
                                                                    </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody>
                                                </table>
                                                  
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                                </table>                                                  
                                        </FooterTemplate>
                                    </asp:Repeater>

                                    <asp:Label ID="lblNoColorFound" runat="server" Text="This style does not have any color."  Visible="false" BackColor="#ffff00"></asp:Label>
                                </div>
                            </div>
                            <%--   </div>--%>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
