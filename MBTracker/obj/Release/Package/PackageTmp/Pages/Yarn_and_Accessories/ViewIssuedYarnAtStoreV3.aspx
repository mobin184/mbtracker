﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" MaintainScrollPositionOnPostback="true" CodeBehind="ViewIssuedYarnAtStoreV3.aspx.cs" Inherits="MBTracker.Pages.Yarn_and_Accessories.ViewIssuedYarnAtStoreV3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th {
            padding: 4px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 5px 3px;
            white-space: nowrap;
        }
    </style>
    <div class="row-fluid">
        <div class="span8">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="View Issued Yarn at Store:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row" style="padding-left: 15px; padding-right: 15px;">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                            <%-- <span style="font-weight: 700; padding-right: 400px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field--%>
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="search" />
                            </div>
                        </div>
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px;">
                            <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000"></span>Please enter issue date or select buyer with style.
                        </div>
                        <div class="col-md-6" style="padding-right: 0px">
                            <div class="form-horizontal">
                                <div class="control-group" style="padding-top: 20px;">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label1" runat="server" Text="Yarn Issued Date:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxIssuedDate" AutoPostBack="true" TextMode="Date" Width="100%" Text="" runat="server"></asp:TextBox>
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="search"
                                            ControlToValidate="tbxIssuedDate"><span style="font-weight: 700; color: #CC0000">Please select delivery date.</span></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label8" runat="server" Text="Select Store:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlStore" runat="server" AutoPostBack="true" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label12" runat="server" Text="Select Knitting Unit:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlKnittingUnits" runat="server" AutoPostBack="true" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <br />
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label11" runat="server" Text=""></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:Button runat="server" CssClass="btn btn-info" ID="btnViewEntries" Text="View Entries" ValidationGroup="search" OnClick="btnViewEntries_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="padding-top: 20px;">
                            <div class="control-group">
                                <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                    <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer (optional):"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                <div class="controls controls-row" style="margin-left: 140px">
                                    <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                            ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputStyle" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                    <asp:Label ID="lblStyle" runat="server" Text="Select Style (optional):"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                <div class="controls controls-row" style="margin-left: 140px">
                                    <asp:DropDownList ID="ddlStyles" runat="server" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid" runat="server" visible="false" id="divSummary">
        <div class="span12">
            <div class="widget" id="dataDiv" runat="server" visible="false">
                <div class="widget-body" style="overflow-x: auto">
                    <div class="control-group">
                        <label for="inputBuyer" class="control-label">
                            <asp:Label ID="lblHeaderMessage" runat="server" Text="Issued Yarn Information:" Font-Bold="true"></asp:Label>
                            <asp:Label Style="float: right" Text="" runat="server" ID="lblAvailableBalance"></asp:Label></label>
                        <div class="controls controls-row">
                            <div id="dt_example" class="example_alt_pagination">
                                <div class="control-group">
                                    <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                        <asp:Repeater ID="rptSummary" runat="server" OnItemDataBound="rptSummary_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table-2" class="table table-bordered table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th style="min-width: 120px">
                                                                <asp:Label ID="lblColor" runat="server" Text="Issue Date"></asp:Label></th>
                                                            <th style="min-width: 100px">
                                                                <asp:Label ID="Label3" runat="server" Text="Buyer Name"></asp:Label></th>
                                                            <th style="min-width: 150px">
                                                                <asp:Label ID="lblStyleName" runat="server" Text="Style Name"></asp:Label></th>
                                                            <th style="min-width: 160px">
                                                                <asp:Label ID="Label15" runat="server" Text="Color Name"></asp:Label></th>
                                                            <th style="min-width: 200px">
                                                                <asp:Label ID="Label16" runat="server" Text="Item Name"></asp:Label></th>
                                                            <th style="min-width: 100px">
                                                                <asp:Label ID="Label9" runat="server" Text="LOT Number"></asp:Label></th>
                                                            <th style="min-width: 140px">
                                                                <asp:Label ID="Label4" runat="server" Text="From Store"></asp:Label></th>
                                                            <th style="min-width: 100px">
                                                                <asp:Label ID="Label6" runat="server" Text="Knitting Unit"></asp:Label></th>
                                                            <th style="min-width: 150px">
                                                                <asp:Label ID="Label7" runat="server" Text="Issued Quantity (lbs)"></asp:Label>
                                                            </th>
                                                            <th style="min-width: 120px">
                                                                <asp:Label ID="Label10" runat="server" Text="Actions"></asp:Label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblIssueDate"><%# string.Format("{0:dd-MMM-yyyy}",Eval("IssueDate")) %></asp:Label>
                                                        <asp:TextBox Visible="false" runat="server" ID="rptIssueDate" TextMode="Date" value='<%# string.Format("{0:yyyy-MM-dd}",Eval("IssueDate")) %>'></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblBuyer"><%#Eval("BuyerName") %></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblStyles"><%#Eval("StyleName") %></asp:Label>
                                                        <asp:DropDownList ID="rptStyles" runat="server" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="rptStyle_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblColors"><%#Eval("ColorDescription") %></asp:Label>
                                                        <asp:DropDownList ID="rptColors" runat="server" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblItems"><%#Eval("ItemName") %></asp:Label>
                                                        <asp:DropDownList ID="rptItems" runat="server" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblLOTNumber"><%#Eval("LotNumber") %></asp:Label>
                                                        <asp:DropDownList ID="rptLOTNumber" runat="server" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblStores"><%#Eval("StoreName") %></asp:Label>
                                                        <asp:DropDownList ID="rptStores" runat="server" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblUnits"><%#Eval("UnitName") %></asp:Label>
                                                        <asp:DropDownList ID="rptUnits" runat="server" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="IssuedQty"><%#Eval("IssuedQuantityLbs") %></asp:Label>
                                                        <asp:TextBox Visible="false" TextMode="Number" min="0" runat="server" ID="UpdatedIssuedQty" value='<%#Eval("IssuedQuantityLbs")%>'></asp:TextBox>
                                                    </td>
                                                    <td>
                                                       <%-- <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("IssueId") %>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkbtnEdit" Visible='<%# ViewState["editEnabled"]%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("IssueId") %>' OnCommand="lnkbtnEdit_Command" class="btn btn-success btn-mini hidden-phone" Text="Edit"></asp:LinkButton>--%>
                                                        <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#Eval("CanDelete")%>' CommandArgument='<%#Eval("IssueId") %>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkbtnEdit" Visible='<%# Eval("CanEdit")%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("IssueId") %>' OnCommand="lnkbtnEdit_Command" class="btn btn-success btn-mini hidden-phone" Text="Edit"></asp:LinkButton>
                                                        
                                                        <asp:LinkButton ID="lnkbtnUpdated" Visible="false" runat="server" CommandName="Updated" CommandArgument='<%#Eval("IssueId") %>' OnCommand="lnkbtnUpdated_Command" class="btn btn-warning btn-mini hidden-phone" Text="Update"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkbtnCancel" Visible="false" runat="server" CommandName="Updated" CommandArgument='<%#Eval("IssueId") %>' OnCommand="lnkbtnCancel_Command" class="btn btn-warning btn-mini hidden-phone" Text="Cancel"></asp:LinkButton>
                                                        <%--<asp:LinkButton ID="lnkbtnView" runat="server" CommandName="View" CommandArgument='<%#Eval("IssueId") %>' OnCommand="lnkbtnView_Command" class="btn btn-success btn-mini hidden-phone" Text="View Details"></asp:LinkButton>--%>
                                                    </td>
                                                </tr>
                                                <asp:PlaceHolder Visible="false" runat="server" ID="trAvailableBalance">
                                                    <tr>
                                                        <td colspan="10">
                                                            <asp:Label Style="text-align:center; font-weight:bold; color:forestgreen" Text="Available Balance" runat="server" ID="rptAvailableBalance"></asp:Label>
                                                        </td>
                                                    </tr>                                                    
                                                </asp:PlaceHolder>                                                
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                                                </table>
                                                                    <div class="clearfix"></div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <asp:Label ID="lblGrandTotal" runat="server" Text="" Font-Bold="true"></asp:Label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Label ID="lblEntryNotFound" runat="server" Text="No data found." Visible="false" BackColor="#ffff00"></asp:Label>

    <div class="row-fluid" runat="server" id="divColors" visible="false">
        <div class="span12">
            <div class="widget">
                <div class="widget-body">
                    <div class="row-fluid">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblEntry" runat="server" Text="" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                    <asp:Repeater ID="rptDetailInfo" OnItemDataBound="rptDetailInfo_ItemDataBound" runat="server">
                                        <HeaderTemplate>
                                            <table id="detailedView">
                                                <thead>
                                                    <tr class="GridViewScrollHeader">
                                                        <th>
                                                            <asp:Label ID="lblBuyer" runat="server" Text="Buyer Name"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblStyle" runat="server" Text="Style Name"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblColor" runat="server" Text="Color Name"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label2" runat="server" Text="Lot Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label13" runat="server" Text="Con. Per Dozen"></asp:Label></th>
                                                        <th style="text-align: center">
                                                            <asp:Label ID="Label5" runat="server" Text="Size & Received Quantity"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                           <%-- <tr class="GridViewScrollItem">
                                                <td style="text-align: left; padding-top: 28px"><%#Eval("BuyerName") %></td>
                                                <td style="text-align: left"><%#Eval("StyleName") %></td>
                                                <td style="text-align: left"><%#Eval("ColorDescription") %>
                                                    <asp:Label ID="lblBuyerColorId" runat="server" Text='<%#Eval("BuyerColorId") %>' Visible="false"></asp:Label></td>
                                                <asp:Label ID="lblStyleId" runat="server" Text='<%#Eval("StyleId") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblBuyerId" runat="server" Text='<%#Eval("BuyerId") %>' Visible="false"></asp:Label>
                                                <td style="text-align: left; padding-top: 28px">
                                                    <asp:Label ID="lblLotNumber" runat="server" Text='<%#Eval("LotNumber") %>'></asp:Label></td>
                                                <td style="text-align: left; padding-top: 28px">
                                                    <asp:Label ID="Label14" runat="server" Text='<%#Eval("ConPerDozen") %>'></asp:Label></td>
                                                <td style="text-align: center">
                                                    <asp:GridView ID="gvSizeQuantity1" RowStyle-Height="30" HeaderStyle-BackColor="#cccccc" CssClass="GridViewClass" OnRowCreated="gvSizeQuantity1_RowCreated" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                                    </asp:GridView>
                                                    <asp:Label ID="lblNoSizeFound" runat="server" Text="No size found." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                                                </td>
                                            </tr>--%>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                </table>
                            <div class="clearfix"></div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <asp:Label ID="lblNoDataFound" runat="server" Text="There is no color for this style." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>

