﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" CodeBehind="IssueYarnAtStoreV2.aspx.cs" Inherits="MBTracker.Pages.Yarn_and_Accessories.IssueYarnAtStoreV2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <div class="row-fluid">
        <div class="span8">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="Issue Yarn at Store:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row" style="padding-left: 5px; padding-right: 5px;">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                            <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="form-horizontal">
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label2" runat="server" Text="Issue Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:TextBox ID="tbxIssueDate" AutoPostBack="true" TextMode="Date" Width="90%" Text="" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxIssueDate">
                                            <span style="font-weight: 700; color: #CC0000">Please select issue date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label3" runat="server" Text="From Store:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlStore" runat="server" Display="Dynamic" AutoPostBack="true" OnSelectedIndexChanged="ddlStore_SelectedIndexChanged" Width="90%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlStore">
                                            <span style="font-weight: 700; color: #CC0000">Please select a store.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label1" runat="server" Text="Knitting Unit:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlUserProductionUnit" runat="server" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlUserProductionUnit">
                                            <span style="font-weight: 700; color: #CC0000">Please select a knitting unit.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                            ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid" runat="server" id="divColors" visible="false">
        <div class="span12">
            <div class="widget" style="overflow-x: auto">
                <div class="widget-body">
                    <div class="row-fluid">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblEntry" runat="server" Text="" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                    <asp:Repeater ID="rptEntryInfo" OnItemDataBound="rptEntryInfo_ItemDataBound" runat="server">
                                        <HeaderTemplate>
                                            <table id="entryTable" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr class="GridViewScrollHeader">
                                                        <th style="text-align: center; width: 10%">
                                                            <asp:Label ID="lblColor" runat="server" Text="Color Names"></asp:Label></th>
                                                        <th style="text-align: center; width: 10%">
                                                            <asp:Label ID="Label7" runat="server" Text="Yarn Composition"></asp:Label></th>
                                                        <th style="text-align: center; width: 10%">
                                                            <asp:Label ID="lblLotNumberMsg" runat="server" Text="Lot Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label4" runat="server" Text="Available Balance (lbs)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Consumption/Dzn"></asp:Label></th>
                                                        <th style="text-align: center">
                                                            <asp:Label ID="lblSizeAndQuantity" runat="server" Text="Size & Quantity"></asp:Label></th>
                                                         <th>
                                                            <asp:Label ID="Label8" runat="server" Text="Remarks"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label6" runat="server" Text="Color Total (lbs)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblColorTotal" runat="server" Text="Color Total (pcs)"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr class="GridViewScrollItem">

                                                <td>
                                                    <asp:DropDownList ID="ddlColors" runat="server" CssClass="custom-select2" AutoPostBack="true" OnSelectedIndexChanged="SelectedIndexChanged" Width="100%" Style="min-width: 200px" Display="Dynamic"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlItems" runat="server" CssClass="custom-select2" AutoPostBack="true" OnSelectedIndexChanged="SelectedIndexChanged" Width="100%" Style="min-width: 200px" Display="Dynamic"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlLotNumber" runat="server" CssClass="custom-select2" AutoPostBack="true" OnSelectedIndexChanged="SelectedIndexChanged" Width="100%" Style="min-width: 200px" Display="Dynamic"></asp:DropDownList>
                                                </td>
                                                 <td>
                                                    <asp:Label ID="lblAvailableBalance" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="tbxConPerDozen" TextMode="number" step="0.01" Width="100%" Text="" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:GridView ID="gvSizeQuantity1" RowStyle-Height="30" Style="margin-left: 5px" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity1_RowCreated">
                                                    </asp:GridView>
                                                    <asp:Label ID="lblNoSizeFound" runat="server" Text="No size found." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                                                </td>
                                               <td>
                                                    <asp:TextBox TextMode="MultiLine" Rows="3" ID="tbxRemarks" style="min-width:250px" Width="100%" Text="" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblColorTotalLBS" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblColorTotalValue" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                </table>
                                                             <div class="pull-right" style="margin-right: 0px; padding-top: 10px">
                                                                 <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="71px" Text="Add Row" OnClick="btnAddRow_Click" />
                                                             </div>
                                            <div class="clearfix"></div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <label for="inputOrder" class="control-label" style="text-align: right; padding-top: 0px">
                <asp:Label ID="lblStyleTotal" runat="server" Text="" Font-Bold="true"></asp:Label>
                <asp:Label ID="lblStyleTotalLbs" runat="server" Text="" Font-Bold="true"></asp:Label>
                <br />
                <asp:LinkButton ID="lnkbtnCalculateStyleTotal" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="Calculate Total" OnClick="lnkbtnCalculateColorTotal_Click"></asp:LinkButton>
            </label>

            <asp:Button ID="btnSaveEntries" runat="server" ValidationGroup="save" class="btn btn-success btn-medium pull-left hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSaveEntries_Click"></asp:Button>&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnUpdateEntries" Visible="false" ValidationGroup="save" runat="server" class="btn btn-success btn-medium pull-left hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdateEntries_Click"></asp:Button>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
