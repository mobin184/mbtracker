﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewReceivedYarnAtUnit.aspx.cs" Inherits="MBTracker.Pages.Yarn_and_Accessories.ViewReceivedYarnAtUnit" %>


<%@ Register Src="~/User_Controls/Header.ascx" TagPrefix="uc1" TagName="Header" %>
<%@ Register Src="~/User_Controls/MainMenu.ascx" TagPrefix="uc1" TagName="MainMenu" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>MB Tracker</title>
    <link href="../../css/web.css" rel="stylesheet" />
    <script type="text/javascript" src="../../js/gridviewscroll.js"></script>
    <script type="text/javascript">
        var gridViewScroll = null;
        window.onload = function () {
            gridViewScroll = new GridViewScroll({
                elementID: "detailedView",
                width: 1425,
                height: 500,
                freezeColumn: true,
                freezeFooter: false,
                freezeColumnCssClass: "GridViewScrollItemFreeze",
                freezeFooterCssClass: "GridViewScrollFooterFreeze",
                freezeHeaderRowCount: 1,
                freezeColumnCount: 4,
                onscroll: function (scrollTop, scrollLeft) {
                    console.log(scrollTop + " - " + scrollLeft);
                }
            });
            gridViewScroll.enhance();
        }
        function getScrollPosition() {
            var position = gridViewScroll.scrollPosition;
            alert("scrollTop: " + position.scrollTop + ", scrollLeft: " + position.scrollLeft);
        }
        function setScrollPosition() {
            var scrollPosition = { scrollTop: 50, scrollLeft: 50 };

            gridViewScroll.scrollPosition = scrollPosition;
        }


    </script>

    <script src="../../js/jquery.dataTables.js"></script>

    <script src="../../js/html5-trunk.js"></script>
    <link href="~/css/bootstrap.min.css" rel="stylesheet" />
    <link type="text/css" href="~/icomoon/style.css" rel="stylesheet" />
    <%--<link type="text/css" href="/css/nvd-charts.css" rel="stylesheet" />--%>
    <link type="text/css" href="~/css/main.css" rel="stylesheet" />
    <%--   <link type="text/css" href='/css/fullcalendar/fullcalendar.css' rel='stylesheet' />
    <link type="text/css" href='/css/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />--%>

    <link type="text/css" href="~/css/custom.css" rel="stylesheet" />
    <link type="text/css" href="~/content/toastr.css" rel="stylesheet" />
    <script src="../../js/jquery.min.js"></script>
    <script src="../../js/bootstrap.js"></script>
    <script src="../../Scripts/toastr.js"></script>
    <%--  <link href="../../FancyBox/jquery.fancybox.css" rel="stylesheet" />--%>
    <link href="../../css/font-awesome.min.css" rel="stylesheet" />

    <%--<link href="../../css/all.css" rel="stylesheet" />--%>
    <link href="../../css/sweetalert.css" rel="stylesheet" />
    <script src="../../Scripts/sweetalert.min.js"></script>
    <link href="../../css/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.min.js"></script>
    <script src="../../js/jquery.blockUI.js"></script>
    <script src="../../Scripts/jquery.signalR-2.2.2.js"></script>
    <%--<script src="/SignalR/hubs"></script>--%>

    <link href="../../content/css/select2.css" rel="stylesheet" />
    <script src="../../Scripts/select2.js"></script>


    <script type="text/javascript">

        $(function () {
            //$('.custom-select2').select2();
            $('select').select2();
            //$('select').select2({
            //    sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)),
            //});
        })

        $(document).ready(function () {

            $("#main-nav ul li ul li").hover(function () {
                $(this).children("ul").toggleClass('test-SubMenu-open', 1, "easeOutSine");
            }, function () {
                $(this).children("ul").toggleClass('test-SubMenu-open', 1, "easeOutSine");
            });
        });

        try {

            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

        } catch (e) {

        }

        function beginRequest(sender, args) {

            $.blockUI({
                //message: '<img src="../../images/loader.gif" />',
                //message: '<div class="spinner"></div>',
                message: '<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>',
                //message: '<div class="lds-ripple"><div></div><div></div></div>',
                css: {
                    border: 'none',
                    backgroundColor: 'transparent'
                },
                overlayCSS: { backgroundColor: '#0d0d0d' }
            });
        }

        function endRequest(sender, args) {
            $.unblockUI();
        }

        function blockUI() {
            $.blockUI({
                message: '<img src="../../images/loader.gif" />',
                css: {
                    border: 'none',
                    backgroundColor: 'transparent'
                },
                overlayCSS: { backgroundColor: '#0d0d0d' }
            });
        }

        function unblockUI() {
            $.unblockUI();
        }

        function scrollDown(position) {
            console.log(position);
            var n = $(document).height();
            if (position != "" && position != 'undefined' && position != null) {
                n = position;
            }
            $('html, body').animate({ scrollTop: n }, 'slow');
        };

        function reloadPage() {
            setTimeout(function () { location.reload(); }, 1000);
        }

        function resetPage() {
            debugger
            $('#form1')[0].reset();
        }


        function returnToPage(url) {
            setTimeout(function () {
                $(location).attr('href', url);
            }, 2000);
        }

        function showConfirm(title, msg, purpose, width, onlyClose) {
            var dialogWidth = "auto";
            var dialogButtons = {
                Yes: function () {
                    $(this).dialog("close");
                    __doPostBack("Confirm", purpose);
                },
                No: function () {
                    $(this).dialog("close");
                }
            };

            if (width != null && width != 'undefined') {
                dialogWidth = width;
            }
            if (onlyClose == 'true') {
                dialogButtons = {
                    Close: function () {
                        $(this).dialog("close");
                    }
                }
            }

            $('<div></div>').appendTo('body')
                .html('<div><h6>' + msg + '</h6></div>')
                .dialog({
                    modal: true,
                    title: title,
                    zIndex: 10000,
                    autoOpen: true,
                    width: dialogWidth,
                    resizable: false,
                    buttons: dialogButtons,
                    close: function (event, ui) {
                        $(this).remove();
                    }
                });
        };


        function showConfirmSA(title, msg, purpose) {
            swal({
                title: title,
                text: msg,
                html: true,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        __doPostBack("Confirm", "ConfirmBtnClicked", purpose);
                    }
                });
        };
    </script>

</head>



<body style="margin: 0px; width: 100%; height: 100%;">
    <form id="form1" runat="server">


        <header id="header">
            <uc1:Header runat="server" ID="Header" />
        </header>

        <div class="container" id="menu" style="margin: 0px;">
            <div id="main-nav" class="hidden-phone hidden-tablet">
                <uc1:MainMenu runat="server" ID="MainMenu" />
                <div class="clearfix"></div>
            </div>
        </div>




        <div class="container-fluid">
            <div class="dashboard-wrapper">
                <div class="main-container" id="body">
                    <div class="page-header">
                        <div class="clearfix"></div>
                    </div>
                    <div>
                    </div>

                    <%-- Start Main Content --%>

                    <div class="row-fluid">
                        <div class="span8">
                            <div class="widget">
                                <div class="widget-header">
                                    <div class="title">
                                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                                        <asp:Label runat="server" ID="lblTitle" Text="View Received Yarn at Knitting Unit:"></asp:Label>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="row" style="padding-left: 15px; padding-right: 15px;">
                                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                                            <%-- <span style="font-weight: 700; padding-right: 400px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field--%>
                                        </div>
                                        <div class="control-group">
                                            <div class="col-md-12 col-sm-12">
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="search" />
                                            </div>
                                        </div>
                                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px;">
                                            <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field
                                        </div>
                                        <div class="col-md-6" style="padding-right: 0px">
                                            <div class="form-horizontal">
                                                <div class="control-group">
                                                    <label for="inputBuyer" class="control-label" style="padding-top: 20px;">

                                                        <asp:Label ID="Label12" runat="server" Text="Select Knitting Unit:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                    <div class="controls controls-row" style="padding-top: 20px;">
                                                        <asp:DropDownList ID="ddlKnittingUnits" runat="server" AutoPostBack="true" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="search"
                                                            ControlToValidate="ddlKnittingUnits"><span style="font-weight: 700; color: #CC0000">Please select knitting unit.</span></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="inputBuyer" class="control-label">
                                                        <asp:Label ID="Label1" runat="server" Text="Yarn Received Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                    <div class="controls controls-row">
                                                        <asp:TextBox ID="tbxReceivedDate" AutoPostBack="true" TextMode="Date" Width="100%" Text="" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="search"
                                                            ControlToValidate="tbxReceivedDate"><span style="font-weight: 700; color: #CC0000">Please select delivery date.</span></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <br />
                                                    <label for="inputBuyer" class="control-label">
                                                        <asp:Label ID="Label11" runat="server" Text=""></asp:Label></label>
                                                    <div class="controls controls-row">
                                                        <asp:Button runat="server" CssClass="btn btn-info" ID="btnViewEntries" Text="View Entries" ValidationGroup="search" OnClick="btnViewEntries_Click" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6" style="padding-right: 0px">
                                            <div class="form-horizontal">
                                                <div class="control-group">
                                                    <label for="inputBuyer" class="control-label" style="padding-top: 20px;">
                                                        <asp:Label ID="Label13" runat="server" Text="Buyer (optional):"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                                    <div class="controls controls-row" style="padding-top: 20px;">
                                                        <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="control-group" style="margin-bottom:0px;">
                                                    <label for="inputBuyer" class="control-label">
                                                        <asp:Label ID="Label14" runat="server" Text="Style (optional):"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                                    <div class="control-group">
                                                        <div class="controls controls-row">
                                                            <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid" runat="server" visible="false" id="divSummary">
                        <div class="span12">
                            <div class="widget" id="dataDiv" runat="server" visible="false">
                                <div class="widget-body" style="overflow-x: auto">
                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="lblHeaderMessage" runat="server" Text="" Font-Bold="true"></asp:Label></label>
                                        <div class="controls controls-row">
                                            <div id="dt_example" class="example_alt_pagination">
                                                <div class="control-group">
                                                    <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                                        <asp:Repeater ID="rptSummary" runat="server" OnItemDataBound="rptSummary_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="data-table" class="table table-bordered table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>
                                                                                <asp:Label ID="lblColor" runat="server" Text="Receive Date"></asp:Label></th>
                                                                            <th>
                                                                                <asp:Label ID="Label3" runat="server" Text="Buyer Name"></asp:Label></th>
                                                                            <th>
                                                                                <asp:Label ID="lblStyleName" runat="server" Text="Style Name"></asp:Label></th>
                                                                            <th>
                                                                                <asp:Label ID="Label4" runat="server" Text="Knitting Part"></asp:Label></th>
                                                                            <th>
                                                                                <asp:Label ID="Label6" runat="server" Text="Yarn Description"></asp:Label></th>
                                                                            <th>
                                                                                <asp:Label ID="Label7" runat="server" Text="GG"></asp:Label></th>
                                                                            <th>
                                                                                <asp:Label ID="Label9" runat="server" Text="Received Quantity"></asp:Label></th>
                                                                            <th>
                                                                                <asp:Label ID="Label10" runat="server" Text="Actions"></asp:Label></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# string.Format("{0:dd-MMM-yyyy}",Eval("ReceivedDate")) %></td>
                                                                    <td><%#Eval("BuyerName") %></td>
                                                                    <td><%#Eval("StyleName") %></td>
                                                                    <td><%#Eval("KnittingPart") %></td>
                                                                    <td><%#Eval("YarnDescription") %></td>
                                                                    <td><%#Eval("GG") %></td>
                                                                    <td><%#Eval("ReceivedQty") %></td>
                                                                    <td>
                                                                        <%--<asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                                                        <asp:LinkButton ID="lnkbtnEdit" Visible='<%# ViewState["editEnabled"]%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' OnCommand="lnkbtnEdit_Command" class="btn btn-success btn-mini hidden-phone" Text="Edit"></asp:LinkButton>--%>
                                                                        <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#Eval("CanDelete")%>' CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                                                        <asp:LinkButton ID="lnkbtnEdit" Visible='<%# Eval("CanEdit")%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' OnCommand="lnkbtnEdit_Command" class="btn btn-success btn-mini hidden-phone" Text="Edit"></asp:LinkButton>
                                                                        
                                                                        <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="View" CommandArgument='<%#Eval("Id") %>' OnCommand="lnkbtnView_Command" class="btn btn-success btn-mini hidden-phone" Text="View Details"></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tbody>
                                                                </table>
                                                                    <div class="clearfix"></div>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                        <asp:Label ID="lblGrandTotal" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:Label ID="lblEntryNotFound" runat="server" Text="No data found." Visible="false" BackColor="#ffff00"></asp:Label>

                    <div class="row-fluid" runat="server" id="divColors" visible="false">
                        <div class="span12">
                            <div class="widget">
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div id="dt_example" class="example_alt_pagination">
                                            <div class="control-group">
                                                <label for="inputOrder" class="control-label">
                                                    <asp:Label ID="lblEntry" runat="server" Text="" Font-Bold="true"></asp:Label></label>
                                                <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                                    <asp:Repeater ID="rptDetailInfo" OnItemDataBound="rptDetailInfo_ItemDataBound" runat="server">
                                                        <HeaderTemplate>
                                                            <table id="detailedView">
                                                                <thead>
                                                                    <tr class="GridViewScrollHeader">
                                                                        <th>
                                                                            <asp:Label ID="lblBuyer" runat="server" Text="Buyer Name"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="lblStyle" runat="server" Text="Style Name"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="lblColor" runat="server" Text="Color Name"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="Label2" runat="server" Text="Lot Number"></asp:Label></th>
                                                                        <th style="text-align: center">
                                                                            <asp:Label ID="Label5" runat="server" Text="Size & Received Quantity"></asp:Label></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr class="GridViewScrollItem">
                                                                <td style="text-align: left; padding-top: 28px"><%#Eval("BuyerName") %></td>
                                                                <td style="text-align: left"><%#Eval("StyleName") %></td>
                                                                <td style="text-align: left"><%#Eval("ColorDescription") %>
                                                                    <asp:Label ID="lblBuyerColorId" runat="server" Text='<%#Eval("BuyerColorId") %>' Visible="false"></asp:Label></td>
                                                                <asp:Label ID="lblStyleId" runat="server" Text='<%#Eval("StyleId") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblBuyerId" runat="server" Text='<%#Eval("BuyerId") %>' Visible="false"></asp:Label>
                                                                <td style="text-align: left; padding-top: 28px">
                                                                    <asp:Label ID="lblLotNumber" runat="server" Text='<%#Eval("LotNumber") %>'></asp:Label></td>
                                                                <td style="text-align: center">
                                                                    <asp:GridView ID="gvSizeQuantity1" RowStyle-Height="30" HeaderStyle-BackColor="#cccccc" CssClass="GridViewClass" OnRowCreated="gvSizeQuantity1_RowCreated" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                                                    </asp:GridView>
                                                                    <asp:Label ID="lblNoSizeFound" runat="server" Text="No size found." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody>
                                </table>
                            <div class="clearfix"></div>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                    <asp:Label ID="lblNoDataFound" runat="server" Text="There is no color for this style." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <%-- End Main Content --%>
                </div>
            </div>
        </div>

        <div class="col-md-12" id="footer">
            <div class="row" style="text-align: center; color: #f79825">
                <b>Copyright © 2020 <a style="text-decoration: none; color: #f79825;" href="http://www.masihata.com/">Masihata Group</a> </b>
            </div>
        </div>

    </form>
</body>
</html>
