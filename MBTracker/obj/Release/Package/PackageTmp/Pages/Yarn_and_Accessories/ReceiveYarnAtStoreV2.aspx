﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" MaintainScrollPositionOnPostback="true" CodeBehind="ReceiveYarnAtStoreV2.aspx.cs" Inherits="MBTracker.Pages.Yarn_and_Accessories.ReceiveYarnAtStoreV2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        input[type="radio"], input[type="checkbox"] {
            margin: -3px 0 0;
        }

        label {
            display: inline;
            padding-left: 3px !important;
        }

        td, th {
            padding: 5px;
        }

        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 7px;
        }

        .table {
            width: 100%;
            margin-bottom: 5px;
        }
    </style>
    <div class="row-fluid">
        <div class="span8">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="Receive Yarn at Store:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body" style="min-height: 175px;">
                    <div class="row" style="padding-left: 5px; padding-right: 5px;">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                            <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="form-horizontal">
                            <%--<div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 10px">
                                <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                            </div>--%>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label1" runat="server" Text="Receive Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:TextBox ID="tbxReceiveDate" AutoPostBack="true" TextMode="Date" Width="90%" Text="" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxReceiveDate"><span style="font-weight: 700; color: #CC0000">Please select receive date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label14" runat="server" Text="Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlBuyers" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyer_SelectedIndexChanged" runat="server" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlPurchaseType"><span style="font-weight: 700; color: #CC0000">Please select a purchase type.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="lblStyles" runat="server" Text="Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlStyles" AutoPostBack="true" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" runat="server" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlPurchaseType"><span style="font-weight: 700; color: #CC0000">Please select a purchase type.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="lblStore" runat="server" Text="Select Store:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlStore" runat="server" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlPurchaseType"><span style="font-weight: 700; color: #CC0000">Please select a store.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group" runat="server">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="lblMerchandiser" runat="server" Text="Select Merchandiser:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlMerchandiser" runat="server" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlPurchaseType"><span style="font-weight: 700; color: #CC0000">Please select merchandiser.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label2" runat="server" Text="Purchase Type:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlPurchaseType" AutoPostBack="true" runat="server" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlPurchaseType"><span style="font-weight: 700; color: #CC0000">Please select a purchase type.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                               

                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="lblBuyer" runat="server" Text="Select Supplier:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlYarnAndAccssSuppliers" AutoPostBack="true" OnSelectedIndexChanged="ddlYarnAndAccssSuppliers_SelectedIndexChanged" runat="server" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlPurchaseType"><span style="font-weight: 700; color: #CC0000">Please select a supplier.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="lblPiNumber" runat="server" Text="Select PI Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlPiNumber" AutoPostBack="true" OnSelectedIndexChanged="ddlPiNumber_SelectedIndexChanged"  runat="server" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlPurchaseType"><span style="font-weight: 700; color: #CC0000">Select PI Number.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                

                                <div class="control-group" runat="server">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label11" runat="server" Text="Invoice Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:TextBox ID="tbxInvoiceNumber" runat="server" placeholder="Enter invoice number" CssClass="form-control" Width="90%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxInvoiceNumber"><span style="font-weight: 700; color: #CC0000">Please enter invoice number.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group" runat="server">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label13" runat="server" Text="Chalan Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:TextBox ID="tbxChalanNo" runat="server" placeholder="Enter chalan number" CssClass="form-control" Width="90%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxChalanNo"><span style="font-weight: 700; color: #CC0000">Please enter chalan number.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group" runat="server" id="lcDiv1" visible="true">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label4" runat="server" Text="Enter L/C Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:TextBox ID="tbxLCNumber" runat="server" placeholder="Enter L/C number" CssClass="form-control" Width="90%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxLCNumber"><span style="font-weight: 700; color: #CC0000">Please enter L/C number.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>


                                <%--<div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="lblDyeingOrderNumber" runat="server" Text="Select Dyeing Order Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlDyeingOrder" AutoPostBack="true" runat="server" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlDyeingOrder"><span style="font-weight: 700; color: #CC0000">Select PI Number.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>--%>

                                <!--<div class="control-group" runat="server" id="dyeingOrderDiv" visible="true">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label3" runat="server" Text="Dyeing Order #:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                         <asp:TextBox ID="tbxDyeingOrderNumber" runat="server" placeholder="Enter dyeing order number" CssClass="form-control" Width="90%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator56" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxDyeingOrderNumber"><span style="font-weight: 700; color: #CC0000">Please enter dyeing order number.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>-->
                                <div class="control-group hidden" runat="server" id="lcDiv" visible="true">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label45" runat="server" Text="Select L/C:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlLCNumber" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLCNumber_SelectedIndexChanged" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator55" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlLCNumber"><span style="font-weight: 700; color: #CC0000">Please select a LC.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group" runat="server" id="dyeingOrderDiv1" visible="true">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label33" runat="server" Text="Dyeing Order #:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlDyeingOrder" runat="server" AutoPostBack="true" Display="Dynamic" Width="90%" OnSelectedIndexChanged="ddlDyeingOrder_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator44" Style="float: left" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlDyeingOrder"><span style="font-weight: 700; color: #CC0000">Please enter dyeing order number.</span></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                            </div>





                            <%--<div class="control-group">
                            <label for="inputStyle" class="control-label">
                                <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="divItemInfo" runat="server" visible="false">

            <div class="span12">

                <div class="widget">

                    <div class="widget-body">
                        <div class="form-horizontal">

                            <div class="control-group">
                                <div class="controls controls-row" style="text-align: right">
                                    <span style="font-weight: 700; color: #CC0000">*</span>Row will not be saved if all the fields don't have values.
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblItemRecEntry" runat="server" Text="Item(s):"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row" style="overflow-x: auto">
                                    <asp:Repeater ID="rptItemRecEntryInfo" runat="server" OnItemDataBound="rptItemRecEntryInfo_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table1" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <%--<th>
                                                            <asp:Label ID="lblStyles" runat="server" Text="Style"></asp:Label></th>--%>
                                                        <th>
                                                            <asp:Label ID="lblColors" runat="server" Text="Color"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblItems" runat="server" Text="Yarn Composition"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Item Category"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblLOTNumber" runat="server" Text="LOT Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblQty" runat="server" Text="Qty (lbs)"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <%-- <td style="width: 18%; padding-top: 18px">
                                                    <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                                </td>--%>
                                                <td style="width: 15%; padding-top: 18px">
                                                    <asp:DropDownList ID="ddlColors" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                </td>
                                                <td style="width: 21%; padding-top: 18px">
                                                    <asp:DropDownList ID="ddlItems" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                </td>

                                                <td style="width: 21%; padding-top: 18px">
                                                    <asp:DropDownList ID="ddlItemCategory" runat="server" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                </td>

                                               <%-- <td style="width: 21%; padding-top: 18px">
                                                    <asp:DropDownList ID="ddlItemCategory" runat="server" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                </td>--%>

                                                <%-- <td style="width: 21%; padding-top: 18px">
                                                    <asp:dropdownlist runat="server" id="ddlItemCategory">
                                                     <asp:listitem text="Cone" value="1"></asp:listitem>
                                                     <asp:listitem text="Hangs" value="2"></asp:listitem>
                                                </asp:dropdownlist>
                                                </td>--%>

                                                <td style="width: 8%; padding-top: 18px">
                                                    <asp:TextBox ID="tbxLotNumber" Width="100%" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 8%; padding-top: 18px">
                                                    <asp:TextBox ID="tbxQty" Width="100%" runat="server" TextMode="Number" step=""></asp:TextBox>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                 </table>
                                            <div class="pull-right" style="margin-right: 0px">
                                                <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="81px" Text="Add Row" OnClick="btnAddRow_Click" />
                                            </div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="span12" style="margin-left: 0px; margin-bottom: 30px">
                <asp:LinkButton ID="lnkbtnSaveEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-left hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
                <asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-left hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>
                <div class="clearfix"></div>
            </div>
        </div>

        <%--   <div class="span9" style="margin-left: 0px">
            <asp:Label ID="lblNoColorFound" runat="server" Text="This style does not have any colors!" Visible="false" BackColor="#ffff00"></asp:Label>
            <div class="widget" runat="server" id="pnlYarnReceive" visible="false" style="border: 0px;">
                <div class="widget-body" style="overflow-x: auto">
                    <asp:Panel runat="server">
                        <div class="control-group">
                            <label class="control-label" style="text-align: left">
                                <asp:Label ID="lbl1" Font-Bold="true" runat="server" Text="Yarn Receive Info:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                        </div>
                        <div class=" controls-row">
                            <div id="dt_example" class="example_alt_pagination">
                                <asp:Repeater ID="rpt" Visible="false" OnItemDataBound="rpt_ItemDataBound" runat="server">
                                    <HeaderTemplate>
                                        <table id="data-table1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="width: 475px">
                                                        <asp:Label ID="lblColorInof" runat="server" Text=""></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblBookingInfo" runat="server" Text="Receive Info"></asp:Label>
                                                    </th>
                                                    <th style="width: 50px">
                                                        <asp:Label ID="lblDeleteRow" runat="server" Text="Action"></asp:Label>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblStyleId" runat="server" Visible="false" Text='<%#Eval("StyleId") %>'></asp:Label>
                                                <div class="form-horizontal">
                                                    <div class="control-group">
                                                        <label for="inputBuyer" class="control-label">
                                                            <asp:Label ID="lbl22" runat="server" Text="Yarn Composition:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                        <div class="controls controls-row">
                                                            <asp:TextBox ID="tbxYarnComposition" Width="250" Text="" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label for="inputBuyer" class="control-label">
                                                            <asp:Label ID="Label1" runat="server" Text="LC Number:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                                        <div class="controls controls-row">
                                                            <asp:TextBox ID="tbxLCNumber" Width="250" Text="" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <asp:Repeater ID="rptColors" runat="server" OnItemDataBound="rptColors_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="data-table2" class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th style="">
                                                                        <asp:Label ID="lbl11" runat="server" Text="Color"></asp:Label>
                                                                    </th>
                                                                    <th style="">
                                                                        <asp:Label ID="lbl12" runat="server" Text="Received Qty"></asp:Label>
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblColorName" runat="server" Text='<%#Eval("ColorDescription") %>'></asp:Label>
                                                                <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%#Eval("BuyerColorId") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbxReceivedQty" CssClass="dummyClass" TextMode="Number" Height="30" Width="80" runat="server"></asp:TextBox>
                                                            </td>

                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                                </table>
                                                  
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Container.ItemIndex %>' class="btn btn-danger btn-mini pull-right " OnCommand="lnkbtnDelete_Command" Text="Delete Row"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                        </table> 
                                        <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-mini pull-right btnStyle" Style="margin-right: 8px" Text="&nbsp;&nbsp;Add Row&nbsp;&nbsp;" OnClick="btnAddRow_Click" />
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </asp:Panel>
                </div>
                <div class="control-group">
                    <div class="controls-row" style="padding-top: 40px;">
                        <asp:Button ID="btnSave" runat="server" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSave_Click" />
                        <asp:Button ID="btnUpdate" Visible="false" runat="server" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdate_Click" />
                    </div>
                </div>
            </div>
        </div>--%>
    </div>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>

