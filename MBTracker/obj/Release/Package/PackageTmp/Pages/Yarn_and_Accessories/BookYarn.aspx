﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="BookYarn.aspx.cs" Inherits="MBTracker.Pages.Yarn_and_Accessories.BookYarn" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        input[type="radio"], input[type="checkbox"] {
            margin: -3px 0 0;
        }

        label {
            display: inline;
            padding-left: 3px !important;
        }

        td, th {
            padding: 5px;
        }

        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 7px;
        }

        .table {
            width: 100%;
            margin-bottom: 5px;
        }
    </style>
    <div class="row-fluid">
        <div class="span6">
    
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="Yarn Booking:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 10px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputStyle" class="control-label">
                                <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
    
              </div>
        <div class="span9" style="margin-left: 0px">
            <div class="widget" runat="server" id="pnlYarnBooking" visible="false">
                <div class="widget-body" style="overflow-x:auto">
                    <asp:Panel runat="server">
                        <%--                            <span style="font-weight: 700; color: #CC0000">&nbsp;</span>--%>
                        <div class="control-group">
                            <label for="inputDeliveryCountry" class="control-label" style="text-align: left">
                                <asp:Label ID="lblColorAndSize" Font-Bold="true" runat="server" Text="Yarn Booking Info:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                        </div>
                        <div class=" controls-row">
                            <div id="dt_example" class="example_alt_pagination">
                                <asp:Repeater ID="rptColors" Visible="false" OnItemDataBound="rptColors_ItemDataBound" runat="server">
                                    <HeaderTemplate>
                                        <table id="data-table1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lblColorInof" runat="server" Text="Color"></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblBookingInfo" runat="server" Text="Booking Info"></asp:Label>
                                                    </th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblBuyerColorId" Visible="false" runat="server" Text='<%#Eval("BuyerColorId")%>'></asp:Label>
                                                <asp:Label ID="lblColorDescription" Visible="true" runat="server" Text='<%#Eval("ColorDescription")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Repeater ID="rpt" runat="server" OnItemDataBound="rpt_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="data-table2" class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 100px">
                                                                        <asp:Label ID="lblYarnDesc" runat="server" Text="Description"></asp:Label></th>
                                                                    <th style="width: 100px">
                                                                        <asp:Label ID="lblBookingQty" runat="server" Text="Booking Qty"></asp:Label></th>
                                                                    <th style="width: 100px">
                                                                        <asp:Label ID="lblSupplierName" runat="server" Text="Supplier"></asp:Label></th>
                                                                    <%-- <th style="width:100px">
                                                                    <asp:Label ID="lblSupplierAddress" runat="server" Text="Supplier Address"></asp:Label></th>--%>
                                                                    <th style="width: 100px">
                                                                        <asp:Label ID="lblConsumptionPerDzn" runat="server" Text="Cons/Dzn"></asp:Label></th>
                                                                    <th style="width: 100px">
                                                                        <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label></th>
                                                                    <th style="width: 50px">
                                                                        <asp:Label ID="lblDeleteRow" runat="server" Text="Action"></asp:Label></th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="tbxYarnDesc" CssClass="dummyClass" Height="30" runat="server" Width="150"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbxBookingQty" CssClass="dummyClass" TextMode="Number" Height="30" Width="80" runat="server"></asp:TextBox></td>
                                                            <td>
                                                                <asp:TextBox ID="tbxSupplierName" CssClass="dummyClass" Height="30" runat="server" Width="150"></asp:TextBox></td>
                                                            <%--<td><asp:TextBox ID="tbxSupplierAddress" CssClass="dummyClass" Height="30" runat="server" Width="100" TextMode="MultiLine"></asp:TextBox></td>--%>
                                                            <td>
                                                                <asp:TextBox ID="tbxConsumptionPerDzn" TextMode="Number" step="0.01" CssClass="dummyClass" Height="30" runat="server" Width="80"></asp:TextBox></td>

                                                            <td>
                                                                <asp:DropDownList ID="ddlStatus" runat="server" Width="170"></asp:DropDownList>
                                                            </td>

                                                            <td>
                                                                <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Container.ItemIndex %>' class="btn btn-danger btn-mini pull-right " OnCommand="btnDelete_Command" Text="Delete Row"></asp:LinkButton>
                                                            </td>

                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                                </table>
                                                  <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-mini pull-right btnStyle" Style="margin-right: 8px" Text="&nbsp;&nbsp;Add Row&nbsp;&nbsp;" OnClick="btnAddRow_Click" />
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                                </table>                                                  
                                    </FooterTemplate>
                                </asp:Repeater>

                                <asp:Label ID="lblNoColorFound" runat="server" Text="This style does not have any colors!" Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                            </div>
                        </div>



                        <div class="control-group">
                            <div class="controls-row" style="padding-top:40px;">
                                <asp:Button ID="btnSaveYarnBooking" runat="server" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSaveYarnBooking_Click" />
                                <asp:Button ID="btnUpdateYarnBooking" Visible="false" runat="server" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdateYarnBooking_Click" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </asp:Panel>
                </div>
            </div>
        </div>
        <div class="span3" id="divTotalOrderQuantity" runat="server" visible="false">            
        </div>
        <div class="span3" runat="server" id="pnlSummary" visible="false">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="Label1" Text="Yarn Booking by Date:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class=" controls-row">
                        <div id="dt_example" class="example_alt_pagination">
                            <asp:Repeater ID="rptSummary" Visible="false" runat="server">
                                <HeaderTemplate>
                                    <table id="data-table1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <asp:Label ID="lbl1" runat="server" Text="Booking Date"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="lbl2" runat="server" Text="Booking Quantity"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label3" runat="server" Text="Action"></asp:Label>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl1" Visible="true" runat="server" Text='<%#string.Format("{0:dd-MMM-yyyy}",Eval("BookingDate"))%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl2" Visible="true" runat="server" Text='<%#Eval("BookingQunatity")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnView" runat="server" OnClientClick='<%#string.Format("window.open(\"ViewYarnBookings.aspx?bookingId={0}\",\"_blank\");",Eval("BookingId"))%>' class="btn btn-success btn-mini hidden-phone" Text="View Details"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                                </table>                                                  
                                </FooterTemplate>
                            </asp:Repeater>

                            <asp:Label ID="lblSummryNotFound" runat="server" Text="Yarn was not booked yet!" Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="span3" runat="server" id="DivColorSummary" visible="false">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="Label2" Text="Yarn Booking by Color:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class=" controls-row">
                        <div id="dt_example" class="example_alt_pagination">
                            <asp:Repeater ID="rptColorWiseSummary" Visible="false" runat="server">
                                <HeaderTemplate>
                                    <table id="data-table1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <asp:Label ID="lbl1" runat="server" Text="Color Name"></asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="lbl2" runat="server" Text="Booking Quantity"></asp:Label>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl1" Visible="true" runat="server" Text='<%#Eval("ColorDescription")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl2" Visible="true" runat="server" Text='<%#Eval("BookingQunatity")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                                </table>                                                  
                                </FooterTemplate>
                            </asp:Repeater>

                            <asp:Label ID="lblColorWizeDataNotFound" runat="server" Text="Yarn was not booked yet!" Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
