﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddNewBooking.aspx.cs" Inherits="MBTracker.Pages.Target_and_Bookings.AddNewBooking" Async="true" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .btn-info {
            margin: 1px 0px 1px 3px;
        }

        /*.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 1px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }*/
    </style>
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="userActionTitle" Text="Add a New Booking"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 30px; padding-bottom: 0px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>

                        <div class="col-md-4">
                            <div class="control-group">
                                <label for="inputBookingDate" class="control-label">
                                    <asp:Label ID="lblBookingDate" runat="server" Text="Booking Date:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxBookingDate" runat="server" placeholder="Enter Booking Date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="tbxBookingDate">
			                        <span style="font-weight: 700; color: #CC0000">Enter Booking Date.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputBuyerName" class="control-label">
                                    <asp:Label ID="lblBuyerName" runat="server" Text="Buyer Name:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlBuyerName" runat="server" placeholder="Select Buyer Name" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlBuyerName">
			                        <span style="font-weight: 700; color: #CC0000">Please select Buyer Name.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputStyleName" class="control-label">
                                    <asp:Label ID="lblStyleName" runat="server" Text="Style Name:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlStyleName" runat="server" placeholder="Select Style Name" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlStyleName">
			                        <span style="font-weight: 700; color: #CC0000">Please select a Style.</span>
                                    </asp:RequiredFieldValidator>

                                </div>
                            </div>
                            <%-- <div class="control-group">
                                <label for="inputBookingQuantity" class="control-label">
                                    <asp:label id="lblBookingQuantity" runat="server" text="Booking Quantity:"></asp:label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:textbox id="tbxBookingQuantity" runat="server" placeholder="Enter Booking Quantity" width="100%" textmode="Number" cssclass="form-control"></asp:textbox>
                                    <asp:requiredfieldvalidator id="RequiredFieldValidator4" display="Dynamic" runat="server" validationgroup="save" controltovalidate="tbxBookingQuantity">
			                        <span style="font-weight: 700; color: #CC0000">Enter Booking Quantity.</span>
                                    </asp:requiredfieldvalidator>
                                </div>
                            </div>--%>
                            <div class="control-group">
                                <label for="inputYarnComposition" class="control-label">
                                    <asp:Label ID="lblYarnComposition" runat="server" Text="Yarn Com. Count & Ply:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxYarnComposition" runat="server" placeholder="Enter Yarn Composition" Width="100%" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputBookingForYear" class="control-label">
                                    <asp:Label ID="lblBookingForYear" runat="server" Text="Booking for Year:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlBookingForYear" runat="server" placeholder="Select Year (Season)" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlBookingForYear_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlBookingForYear">
			                        <span style="font-weight: 700; color: #CC0000">Please select Booking For Year.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputBuyer’sSeason" class="control-label">
                                    <asp:Label ID="Label3" runat="server" Text="Buyer’s Season:"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlBuyersSeason" runat="server" placeholder="Select Buyer’s Season" Width="100%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputShipmentMonth" class="control-label">
                                    <asp:Label ID="lblShipmentMonth" runat="server" Text="Shipment Month:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlShipmentMonth" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChangeProductionMonthInfo" placeholder="Select Shipment Month" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlShipmentMonth">
			                        <span style="font-weight: 700; color: #CC0000">Please select Shipment Month.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputShipmentYear" class="control-label">
                                    <asp:Label ID="lblShipmentYear" runat="server" Text="Shipment Year:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlShipmentYear" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChangeProductionMonthInfo" placeholder="Select Shipment Year" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlShipmentYear">
			                        <span style="font-weight: 700; color: #CC0000">Please select Shipment Year.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputMachineBrand" class="control-label">
                                    <asp:Label ID="lblMachineBrand" runat="server" Text="Machine Brand:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlMachineBrand" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChangeProductionMonthInfo" placeholder="Select Machine Brand" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlMachineBrand">
			                        <span style="font-weight: 700; color: #CC0000">Please select Machine Brand.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputKnittingMachineGauge" class="control-label">
                                    <asp:Label ID="lblKnittingMachineGauge" runat="server" Text="Knitting Machine Gauge:"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxKnittingMachineGauge" runat="server" placeholder="Enter Knitting Machine Gauge" Width="100%" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label for="inputLinkingMachineGaugeTwo" class="control-label">
                                    <asp:Label ID="Label5" runat="server" Text="Linking Machine Gauge (2):"></asp:Label>
                                 <%--   <span style="font-weight: 700; color: #CC0000">*</span>--%>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlLinkingMachineGaugeTwo" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChangeProductionMonthInfo" placeholder="Select Linking Machine Gauge" Width="100%" CssClass="form-control"></asp:DropDownList>
                                   <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator39" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlLinkingMachineGaugeTwo">
			                        <span style="font-weight: 700; color: #CC0000">Please select linking machine gauge two.</span>
                                    </asp:RequiredFieldValidator>--%>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="control-group">
                                <label for="inputLinkingMachineGauge" class="control-label">
                                    <asp:Label ID="lblLinkingMachineGauge" runat="server" Text="Linking Machine Gauge:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <%-- <asp:TextBox ID="tbxLinkingMachineGauge" runat="server" placeholder="Enter Linking Machine Gauge" Width="100%" CssClass="form-control"></asp:TextBox>--%>
                                    <asp:DropDownList ID="ddlLinkingMachineGauge" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChangeProductionMonthInfo" placeholder="Select Linking Machine Gauge" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlLinkingMachineGauge">
			                        <span style="font-weight: 700; color: #CC0000">Please select linking machine gauge.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label for="inputFinishingUnit" class="control-label">
                                    <asp:Label ID="lblFinishingUnit" runat="server" Text="Finishing Unit:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlFinishingUnit" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChangeProductionMonthInfo" placeholder="Select Finishing Unit" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="ddlFinishingUnit">
			                        <span style="font-weight: 700; color: #CC0000">Please select Finishing Unit.</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <%-- <div class="control-group">
                                <label for="inputKnittingStartDate" class="control-label">
                                    <asp:label id="lblProductionMonth" runat="server" text="Production Month:"></asp:label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:dropdownlist id="ddlProductionMonth" runat="server" placeholder="Select production month" width="100%" cssclass="form-control"></asp:dropdownlist>
                                    <asp:requiredfieldvalidator id="RequiredFieldValidator8" display="Dynamic" runat="server" validationgroup="save" controltovalidate="ddlProductionMonth">
			                        <span style="font-weight: 700; color: #CC0000">Please select production month.</span>
                                    </asp:requiredfieldvalidator>
                                </div>
                            </div>--%>
                            <div class="control-group">
                                <label for="inputKnittingTime" class="control-label">
                                    <asp:Label ID="Label2" runat="server" Text="Knitting Time (Minutes):"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxKnittingTime" runat="server" AutoPostBack="true" OnTextChanged="ChangeProductionMonthInfo" placeholder="Enter Knitting Time (Minutes)" Width="100%" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="tbxKnittingTime">
			                        <span style="font-weight: 700; color: #CC0000">Enter Knitting Time (Minutes).</span>
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <%--<div class="control-group">
                                <label for="inputKnittingStartDate" class="control-label">
                                    <asp:label id="lblKnittingStartDate" runat="server" text="Knitting Start Date:"></asp:label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:textbox id="tbxKnittingStartDate" runat="server" placeholder="Enter Knitting Start Date" width="100%" textmode="Date" cssclass="form-control"></asp:textbox>
                                    <asp:requiredfieldvalidator id="RequiredFieldValidator8" display="Dynamic" runat="server" validationgroup="save" controltovalidate="tbxKnittingStartDate">
			                        <span style="font-weight: 700; color: #CC0000">Enter Knitting Start Date.</span>
                                    </asp:requiredfieldvalidator>
                                </div>
                            </div>--%>
                            <div class="control-group">
                                <label for="inputLinkingPcsperDay" class="control-label">
                                    <asp:Label ID="lblLinkingPcsperDay" runat="server" Text="Linking Pcs per Day:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxLinkingPcsperDay" runat="server" AutoPostBack="true" OnTextChanged="ChangeProductionMonthInfo" placeholder="Enter Linking Pcs per Day" Width="100%" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" Display="Dynamic" runat="server" ValidationGroup="save" ControlToValidate="tbxLinkingPcsperDay">
			                        <span style="font-weight: 700; color: #CC0000">Enter Linking PCs per day.</span>
                                    </asp:RequiredFieldValidator>

                                </div>
                            </div>
                            <div class="control-group">
                                <label for="" class="control-label">
                                    <asp:Label ID="Label4" runat="server" Text="Yarn In Hanks/Cone:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlYarnInHanksOrCone" runat="server" placeholder="" Width="100%" CssClass="form-control">
                                        <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Hanks" Value="Hanks"></asp:ListItem>
                                        <asp:ListItem Text="Cone" Value="Cone"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group" style="display:none;">
                                <label for="" class="control-label">
                                    <asp:Label ID="Label6" runat="server" Text="Yarn Local/Imported:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlYarnLocalOrImported" runat="server" placeholder="" Width="100%" CssClass="form-control">
                                        <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Local" Value="Local"></asp:ListItem>
                                        <asp:ListItem Text="Imported" Value="Imported"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="" class="control-label">
                                    <asp:Label ID="Label7" runat="server" Text="Weight Per Dzn:"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxWeightPerDz" runat="server" placeholder="Enter Weight per Dzn" Width="100%" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="" class="control-label">
                                    <asp:Label ID="Label8" runat="server" Text="Auto Linking:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlAutoLinking" runat="server" placeholder="" Width="100%" CssClass="form-control">
                                        <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                        <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="" class="control-label">
                                    <asp:Label ID="Label9" runat="server" Text="Trimming Output (10h):"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxTrimmingOutput" TextMode="Number" runat="server" placeholder="Enter Trimming Output" Width="100%" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="" class="control-label">
                                    <asp:Label ID="Label10" runat="server" Text="Mending Output (10h):"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxMendingOutput" TextMode="Number" runat="server" placeholder="Enter Mending Output" Width="100%" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="" class="control-label">
                                    <asp:Label ID="Label11" runat="server" Text="Ironing Output (10h):"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxIroningOutput" TextMode="Number" runat="server" placeholder="Enter Ironing Output" Width="100%" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group" style="display:none;">
                                <label for="" class="control-label">
                                    <asp:Label ID="Label12" runat="server" Text="Additional Work:"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxAdditionalWork" runat="server" placeholder="Enter Addtional Work" Width="100%" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <%--<div class="control-group">
                                <label for="inputLinkingStartDate" class="control-label">
                                    <asp:label id="lblLinkingStartDate" runat="server" text="Linking Start Date:"></asp:label>
                                     <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:textbox id="tbxLinkingStartDate" runat="server" placeholder="Enter Linking Start Date" width="100%" textmode="Date" cssclass="form-control"></asp:textbox>
                                    <asp:requiredfieldvalidator id="RequiredFieldValidator10" display="Dynamic" runat="server" validationgroup="save" controltovalidate="tbxLinkingStartDate">
			                        <span style="font-weight: 700; color: #CC0000">Enter Linking Start Date.</span>
                                    </asp:requiredfieldvalidator>
                                </div>
                            </div>--%>
                            <%--<div class="control-group">
                                <label for="inputSampleApprovalDate" class="control-label">
                                    <asp:Label ID="lblSampleApprovalDate" runat="server" Text="Sample Approval Date:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxSampleApprovalDate" runat="server" placeholder="Enter Sample Approval Date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputYarnInHouseDate" class="control-label">
                                    <asp:Label ID="Label4" runat="server" Text="Yarn In-House Date:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxYarnInHouseDate" runat="server" placeholder="Enter Yarn In-House Date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>--%>
                            <%--<div class="control-group hidden">
                                <label for="inputPPApprovalDate" class="control-label">
                                    <asp:Label ID="lblPPApprovalDate" runat="server" Text="PP Approval Date:"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxPPApprovalDate" runat="server" placeholder="Enter PP Approval Date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>--%>
                        </div>
                        <div class="col-md-4" runat="server" id="InfoTextDiv" style="height: 350px; text-align: center; line-height: 350px">
                            <span style="padding: 10px; display: inline-block; background-color: #ffff00ab; vertical-align: middle; line-height: 26px; font-size: 14px; font-weight: 600;">Enter required information
                                <br />
                                to get the production months.</span>
                        </div>
                        <div class="col-md-4" id="divProductionMonth" runat="server" visible="false">
                            <div class="control-group" style="margin-bottom: 8px;">
                                <label for="" class="control-label" style="width: 100%; font-size: 13px; text-align: left; margin-left: 58px; font-weight: bold;">
                                    <asp:Label ID="Label1" Style="text-decoration: underline;" runat="server" Text="Months Wise Booking Quantity:"></asp:Label>
                                </label>
                            </div>
                            <div class="control-group" style="margin-left: 58px;">
                                <asp:Repeater ID="rptBookingMonths" runat="server" OnItemDataBound="rptBookingMonths_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="width: 33%; font-weight: 600">
                                                        <asp:Label ID="lblStage" runat="server" Text="Production <br />Month"></asp:Label></th>
                                                    <th style="width: 33%; font-weight: 600">
                                                        <asp:Label ID="lblMCBrand" runat="server" Text="Available  <br />Capacity"></asp:Label></th>
                                                    <th style="width: 34%; font-weight: 600">
                                                        <asp:Label ID="lblMCGauge" runat="server" Text="Booking  <br />Quantity"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="vertical-align: middle">
                                                <asp:Label ID="lblProductionMonthId" runat="server" Visible="false" Text='<%# Eval("ProductionMonthId")%>'></asp:Label>
                                                <%-- <asp:Label ID="lblMonthAndYearName" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "Year")%>' CssClass="text-center"></asp:Label>--%>
                                                <asp:Label ID="lblMonthName" runat="server" Text='<%# Eval("MonthName")%>'></asp:Label>

                                            </td>
                                            <td style="vertical-align: middle">
                                                <asp:Label ID="lblAvailableCapacity" runat="server" Text='<%# Eval("AvailableBookingCapacity")%>' Width="60px" CssClass="text-center"></asp:Label>
                                            </td>
                                            <td style="padding: 3px;">
                                                <asp:TextBox ID="tbxBookingQuantity" Enabled='<%# Convert.ToInt32(Eval("AvailableBookingCapacity")) > 0 ? true:false %>' runat="server" placeholder="" Width="100%" TextMode="Number" Style="height: 35px; line-height: 25px;" CssClass="form-control"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="control-group">
                                <br />
                                <div class="controls-row">
                                    <asp:Button ID="btnSave" runat="server" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSave_Click" CssClass="btn btn-info pull-right" ValidationGroup="save" />
                                    <asp:Button ID="btnUpdate" runat="server" Visible="False" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdate_Click" CssClass="btn btn-info pull-right" ValidationGroup="save" />
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <%--<div class="form-actions no-margin" style="padding-left: 0px; padding-bottom: 10px; padding-right: 0px; margin-left: 28%">
                            <div class="col-md-11" style="margin-left: -20px;">
                                <asp:Button ID="btnSave" runat="server" Text="Submit" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnSave_Click"  />
                                <asp:Button ID="btnUpdate" runat="server" Visible="False" Text="Update" OnClick="btnUpdate_Click" CssClass="btn btn-info pull-right" ValidationGroup="save" />
                            </div>

                        </div>--%>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-4 col-md-offset-4">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
