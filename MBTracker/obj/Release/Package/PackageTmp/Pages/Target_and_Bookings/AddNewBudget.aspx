﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddNewBudget.aspx.cs" Inherits="MBTracker.Pages.Target_and_Bookings.AddNewBudget" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        td > label {
            line-height: 13px
        }

        .cblClass label {
            padding-left: 2px !important;
            padding-right: 14px !important;
        }

        input[type="radio"], input[type="checkbox"] {
            margin: -2px 5px 0px 0;
        }

    </style>

    <div class="row-fluid">

        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <!--<span class="fs1" aria-hidden="true" data-icon=""></span>-->
                        <asp:Label runat="server" ID="styleActionTitle" Text="Add a New Budget:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 20px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save"/>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                       
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label6" runat="server" Text="Budget Year:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBudgetYear" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBudgetYear"><span style="font-weight: 700; color: #CC0000">Please select a year.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputMonths" class="control-label">
                                <asp:Label ID="Label1" runat="server" Text="Budget Month:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlMonths" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlMonths"><span style="font-weight: 700; color: #CC0000">Please select a Month.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                         <div class="control-group">
                            <label for="inputGaugeType" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="Gauge Type:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlGaugeType" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlGaugeType"><span style="font-weight: 700; color: #CC0000">Please select Gauge Type.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="lblBudgetQuantity" runat="server" Text="Budget Quantity"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxQuantity" runat="server" TextMode="Number" placeholder="Enter Budget Quantity" CssClass="form-control" Width="250"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxQuantity"><span style="font-weight: 700; color: #CC0000">Enter Quantity</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputMerchandisingLeader" class="control-label">
                                <asp:Label ID="lblMerchandisingLeader" runat="server" Text="Merchandising Leader:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlMerchandisingLeader" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlMerchandisingLeader"><span style="font-weight: 700; color: #CC0000">Please select Merchandising Leader.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 40px">
                        </div>



                        <div class="clearfix"></div>


                       

                       <%-- <div class="clearfix"></div>


                        <div class="clearfix"></div>--%>
                        <div class="control-group">
                            <br />
                            <div class="controls-row">
                                <asp:Button ID="btnSave" runat="server" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnSave_Click" />
                                <asp:Button ID="btnSearch" runat="server" Text="&nbsp;&nbsp;&nbsp;Show&nbsp;&nbsp;&nbsp;" Class="btn btn-success btn-midium pull-left btnBudget" OnClick="btnSearch_Click" />
                               <%-- <asp:Button ID="btnView" runat="server" Text="&nbsp;&nbsp;&nbsp;View Details&nbsp;&nbsp;&nbsp;" Class="btn btn-success btn-midium mx-auto btnBudget" OnClick="btnViewDetails_Click" />--%>
                                <asp:Button ID="btnUpdate" runat="server" Visible="False" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdate_Click" CssClass="btn btn-info pull-right" ValidationGroup="save" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="span6">
            <asp:Panel ID="pnlBudget" runat="server" Visible="true">
                <div class="widget">
                    <div class="widget-header">
                        <div class="title">
                            <span class="fs1" aria-hidden="true" data-icon=""></span>Budgets:
                        </div>
                    </div>
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">
                            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
                            <asp:Repeater ID="rpt" runat="server" OnItemCommand="rpt_ItemCommand">
                                <HeaderTemplate>
                                    
                                        <thead>
                                            <tr>
                                                <th>
                                                    <asp:Label ID="Label2" runat="server" Text="Buyer"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label5" runat="server" Text="Year"></asp:Label></th>
                                                 <th>
                                                    <asp:Label ID="Label7" runat="server" Text="Month"></asp:Label></th>
                                                 <th>
                                                    <asp:Label ID="Label8" runat="server" Text="Gauge Type"></asp:Label></th>
                                                 <th>
                                                    <asp:Label ID="Label4" runat="server" Text="Quantity"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label3" runat="server" Text="Actions"></asp:Label></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%#Eval("BuyerName")%> </td>
                                        <td><%#Eval("Year") %></td>
                                        <td><%#Eval("MonthName")%></td>
                                        <td><%#Eval("GaugeName") %></td>
                                        <td><%#Eval("Quantity") %></td>
                                        <td  style="white-space:nowrap">
                                        
                                        <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>
                                          <%--  <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="Completed"></asp:LinkButton>   --%>
                                     
                                         </td>
                                        

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                 </FooterTemplate>
                                </asp:Repeater>
                         </table>
                            <div class="clearfix">
                                
                            
                        </div>
                    </div>
                    <!--<div class="clearfix"></div>-->
                </div>
                    </div>

            </asp:Panel>


        </div>


       <div class="clearfix"></div>

    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
