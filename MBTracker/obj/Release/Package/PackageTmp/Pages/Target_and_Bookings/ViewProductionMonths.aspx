﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewProductionMonths.aspx.cs" Inherits="MBTracker.Pages.Target_and_Bookings.ViewProductionMonths" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <div class="row-fluid">
        <div runat="server">
            <div class="span12">
                <div class="widget">
                    <div class="widget-header">
                        <div class="title">
                            <span class="fs1" aria-hidden="true" data-icon=""></span>Production Months
                        </div>
                    </div>
                    <div class="widget-body" style="width: 100%; overflow-x: auto">
                        <div id="dt_example" class="example_alt_pagination">
                            <asp:Repeater ID="rpt" runat="server">
                                <HeaderTemplate>
                                    <table id="data-table2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                 <th class="hidden">
                                                    <asp:Label ID="Label12" Visible="false" runat="server" Text="Id"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label1" runat="server" Text="Production </br>Year"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label2" runat="server" Text="Production </br>Month"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label3" runat="server" Text="Month </br>Start Date"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label4" runat="server" Text="Month </br>End Date"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label9" runat="server" Text="Weekend</br>Day"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label10" runat="server" Text="No of </br>Weekend"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label5" runat="server" Text="Other </br>Holidays"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label6" runat="server" Text="Other </br>Holidays Count"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label7" runat="server" Text="Special </br>Working Days"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label8" runat="server" Text="Sepecial Working <br /> Days Count"></asp:Label></th>     
                                                <th>
                                                    <asp:Label ID="Label11" runat="server" Text="Total <br />Working  Days"></asp:Label></th>   
                                                <th>
                                                    <asp:Label ID="Label16" runat="server" Text="Take  </br>Action"></asp:Label></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="hidden"><%#Eval("Id") %>></td>
                                        <td><%#Eval("ProductionYear") %> </td>
                                        <td><%#Eval("ProductionMonth") %></td>
                                        <td><%#String.Format("{0:dd-MMM-yyyy}",Eval("StartDate")) %></td>
                                        <td><%#String.Format("{0:dd-MMM-yyyy}",Eval("EndDate")) %></td>
                                        <td><%#Eval("Weekend") %></td>
                                        <td><%#Eval("NoOfWeekends") %></td>
                                        <td><%#Eval("OtherHolidays") %></td>
                                        <td><%#Eval("NoOfOtherHolidays") %></td>
                                        <td><%#Eval("ExceptionalWorkingDays") %></td>
                                        <td><%#Eval("NoOfExceptionalWorkingDays") %></td>   
                                        <td><%#Eval("NoOfWorkingDays") %></td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnEdit" Visible='<%# ViewState["editEnabled"]%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" OnCommand="btnEdit_Command" Text="Edit"></asp:LinkButton>
                                            <%-- <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-small hidden-phone" OnCommand="btnDelete_Command" Text="Delete"></asp:LinkButton>--%>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                         </table>
                            <div class="clearfix">
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
        <script src="../../js/jquery.dataTables.js"></script>
</asp:Content>
