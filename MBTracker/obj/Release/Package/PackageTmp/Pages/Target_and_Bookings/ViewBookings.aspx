﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewBookings.aspx.cs" Inherits="MBTracker.Pages.Target_and_Bookings.ViewBookings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .form-horizontal .control-label {
            width: 110px;
        }

        .form-horizontal .controls {
            margin-left: 115px;
        }

        #dt_example td, #dt_example th {
            vertical-align: middle;
            white-space: nowrap;
        }

        .bold {
            font-weight: bold;
        }

        .form-control-without-border {
            font-size: 12px;
            display: block;
            width: 100%;
            height: 34px;
            padding: 10px 12px;
            color: #555;
            background-color: #fff;
            background-image: none;
        }

        .form-horizontal .control-label {
            line-height: 12px !important;
        }

        .form-horizontal .controls {
            line-height: 3px !important;
        }

        .form-horizontal .control-group {
            margin-bottom: 0px !important;
        }

        .form-control-without-border {
            height: 25px !important;
        }

       /* #detailsDiv > .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 2px !important;
        }*/
    </style>
    <div class="row-fluid" runat="server" visible="true" id="FilterDiv">
        <div class="col-md-9" style="padding-left: 0px; margin-bottom: -50px">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>Filter Criteria
                    </div>
                </div>
                <div class="widget-body" style="min-height: 150px">
                    <div class="col-md-5">
                        <div class="form-horizontal">
                            <div class="control-group">
                                <label for="inputBuyerName" class="control-label">
                                    <asp:Label ID="lblBuyerName" runat="server" Text="Buyer Name:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlBuyerName" runat="server" placeholder="Select Buyer Name" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputStyleName" class="control-label">
                                    <asp:Label ID="lblStyleName" runat="server" Text="Style Name:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlStyleName" runat="server" placeholder="Select Style Name" Width="100%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputFinishingUnit" class="control-label">
                                    <asp:Label ID="lbl9" runat="server" Text="Finishing Unit:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlFinishingUnit" runat="server" placeholder="Select Finishing Unit" Width="100%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-horizontal">
                            <div class="control-group">
                                <label for="inputShipmentMonth" class="control-label">
                                    <asp:Label ID="lbl1" runat="server" Text="Shipment Month:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlShipmentMonth" runat="server" placeholder="Select Shipment Month" Width="100%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputShipmentYear" class="control-label">
                                    <asp:Label ID="lbl2" runat="server" Text="Shipment Year:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlShipmentYear" runat="server" placeholder="Select Shipment Year" Width="100%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputMachineBrand" class="control-label">
                                    <asp:Label ID="lbl3" runat="server" Text="Machine Brand:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlMachineBrand" runat="server" placeholder="Select Machine Brand" Width="100%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" style="padding-left: 0px">
                        <div class="form-horizontal" style="width: 80%; float: left">
                            <asp:Button ID="btnReset" runat="server" Width="100%" Text="Reset Filter" CssClass="btn btn-danger pull-right" OnClick="btnResetFilter_Click" />
                            <asp:Button ID="btnSearch" Width="100%" runat="server" Style="margin: 0px; margin-top: 61px" Text="Search" CssClass="btn btn-info pull-right" OnClick="btnSearch_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row-fluid" runat="server" id="showFilterButtonDiv">
        <div class="col-md-12" style="padding-right: 0px; height:25px;">
            <%--<asp:Button ID="btnShowFilter" runat="server"  Text="Show Search Filter" CssClass="btn btn-info pull-right" OnClick="btnShowFilter_Click" />
            <asp:Button ID="btnHideFilter" runat="server" Visible="false" Text="Hide Search Filter" CssClass="btn btn-info pull-right" OnClick="btnHineFilter_Click" />--%>
        </div>
    </div>
    <div class="row-fluid" runat="server" id="bookingListDiv">
        <div runat="server">
            <div class="span12">
                <div class="widget">
                    <div class="widget-header">
                        <div class="title">
                            <span class="fs1" aria-hidden="true" data-icon=""></span>Bookings
                        </div>
                    </div>
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination" style="overflow-x: auto">
                            <asp:Repeater ID="rpt" runat="server">
                                <HeaderTemplate>
                                    <table id="data-table" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="hidden">Id</th>
                                                <th>
                                                    <asp:Label ID="Label18" runat="server" Text="Booking <br/>Number"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label2" runat="server" Text="Buyer <br/>Name"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label3" runat="server" Text="Style <br/>Name"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label22" runat="server" Text="Quantity"></asp:Label></th>
                                                
                                                <th>
                                                    <asp:Label ID="Label5" runat="server" Text="Yarn <br/>InHouse Date"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label6" runat="server" Text="PP Approval Date"></asp:Label></th>
                                                
                                                <th>
                                                    <asp:Label ID="Label9" runat="server" Text="Acc. In-House Date"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label17" runat="server" Text="Knitting <br/>Machine Brand"></asp:Label></th>
                                                
                                                <th>
                                                    <asp:Label ID="Label19" runat="server" Text="Ex-Factory Date"></asp:Label></th>
                                                
                                                <th>
                                                    <asp:Label ID="Label10" runat="server" Text="Lead Time"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label20" runat="server" Text="Finishing Unit"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label4" runat="server" Text="Status"></asp:Label></th>
                                                
                                                <th>
                                                    <asp:Label ID="Label16" runat="server" Text="Take <br/>Actions"></asp:Label></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="hidden"><%#Eval("Id") %>></td>
                                       <td><%#Eval("BookingNumber") %></td>
                                        <td><%#Eval("Buyer") %></td>
                                        <td><%#Eval("Style") %></td>
                                        <td><%#Eval("Quantity") %></td>
                                        <td><%#Eval("YarnInHouseDate", "{0:yyyy-MM-dd}") %></td>
                                        <td><%#Eval("PPApprovalDate", "{0:yyyy-MM-dd}") %></td>
                                        <td><%#Eval("PackingAccesInhouseDate", "{0:yyyy-MM-dd}") %></td>
                                        <td><%#Eval("MachineBrand") %></td>
                                        <td><%#Eval("ExFactoryDate", "{0:yyyy-MM-dd}") %></td>

                                        <td><%#Eval("LeadTime") %></td>
                                       
                                        <td><%#Eval("FinishingUnit") %></td>
                                        <td><%#Eval("StatusName") %></td>
                                       
                                        <td>
                                            <asp:LinkButton ID="lnkbtnDelete" Visible='<%#ViewState["deleteEnabled"]%>' runat="server" CommandName="Delete" CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-small hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnEdit" Visible='<%#ViewState["editEnabled"]%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" OnCommand="btnEdit_Command" Text="Edit"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="View" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" OnCommand="lnkbtnView_Command" Text="View Details"></asp:LinkButton>
                                            
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                         </table>
                            <div class="clearfix">
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

    <label for="inputStyle" class="control-label" style="font-weight: bold; padding-top: 100px; margin-left: 200px; text-align: left">
        <asp:Label ID="lblNotAuthorizedToView" Font-Bold="true" Visible="false" Font-Size="Large" runat="server" Text="You are not authorized to view this information."></asp:Label>
    </label>


    <div class="row-fluid" runat="server" id="detailsDiv" visible="false">
        <div class="span12">
            <div class="widget" style="border: none">
                <label for="" class="control-label">
                    <asp:Label ID="lblBookingDetails" Font-Bold="true" Font-Size="Large" runat="server" Text="Booking Details:"></asp:Label>
                </label>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="col-md-4">
                            <div class="control-group">
                                <label for="inputBookingDate" class="control-label" style="width: 188px;">
                                    <asp:Label ID="lblBookingDate1" runat="server" Font-Bold="true" Text="Booking Date:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblBookingDate" runat="server" CssClass="form-control-without-border" Width="100%"></asp:Label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputBuyerName" class="control-label" style="width: 188px;">
                                    <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Buyer Name:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblBuyer" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputStyleName" class="control-label" style="width: 188px;">
                                    <asp:Label ID="Label7" runat="server" Font-Bold="true" Text="Style Name:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblStyle" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>
                            <%--<div class="control-group">
                                <label for="inputBookingQuantity" class="control-label" style="width: 188px;">
                                    <asp:Label ID="lblBookingQuantity1" Font-Bold="true" runat="server" Text="Booking Quantity:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblBookingQuantity" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>--%>
                            <div class="control-group">
                                <label for="inputYarnComposition" class="control-label" style="width: 188px;">
                                    <asp:Label ID="lblYarnComposition1" Font-Bold="true" runat="server" Text="Yarn Composition:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblYarnComposition" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputBeneficiaryName" class="control-label" style="width: 188px;">
                                    <asp:Label ID="lblBeneficiaryName1" Font-Bold="true" runat="server" Text="Yarn Supplier Name:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblBeneficiaryName" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputBookingForYear" class="control-label" style="width: 188px;">
                                    <asp:Label ID="lblBookingForYear1" Font-Bold="true" runat="server" Text="Booking for Year:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblBookingForYear" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputBuyer’sSeason" class="control-label" style="width: 188px;">
                                    <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="Buyer’s Season:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblBuyerSeason" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>
                            <%--<div class="control-group">
                                <label for="inputShipmentMonth" class="control-label" style="width: 188px;">
                                    <asp:Label ID="Label8" runat="server" Font-Bold="true" Text="Shipment Month:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblShipmentMonth" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>--%>
                            <%--<div class="control-group">
                                <label for="inputShipmentYear" class="control-label" style="width: 188px;">
                                    <asp:Label ID="Label12" runat="server" Font-Bold="true" Text="Shipment Year:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblShipmentYear" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>--%>

                            <div class="control-group">
                                <label for="inputShipmentMode" class="control-label" style="width: 188px;">
                                    <asp:Label ID="lblShipmentMode1" Font-Bold="true" runat="server" Text="Shipment Mode:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblShipmentMode" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputMachineBrand" class="control-label" style="width: 188px;">
                                    <asp:Label ID="Label13" runat="server" Font-Bold="true" Text="Machine Brand (Option A):"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblMachineBrand" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputMachineBrandOptionB1" class="control-label" style="width: 188px;">
                                    <asp:Label ID="Label8" runat="server" Font-Bold="true" Text="MC Flexibility/MC Mapping:(Option B-1):"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblMachineBrandOptionB1" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputMachineBrandOptionB2" class="control-label" style="width: 188px;">
                                    <asp:Label ID="Label11" runat="server" Font-Bold="true" Text="MC Flexibility/MC Mapping:(Option B-2):"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblMachineBrandOptionB2" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputMachineBrandOptionc" class="control-label" style="width: 188px;">
                                    <asp:Label ID="Label12" runat="server" Font-Bold="true" Text="MC Flexibility/MC Mapping:(Option C):"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblMachineBrandOptionC" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-4">

                            <div class="control-group">
                                <label for="inputBookingNumber" class="control-label" style="width: 188px;">
                                    <asp:Label ID="lblBookingNumber1" Font-Bold="true" runat="server" Text="Booking Number:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblBookingNumber" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputKnittingMachineGauge" class="control-label" style="width: 188px;">
                                    <asp:Label ID="lblKnittingMachineGauge1" Font-Bold="true" runat="server" Text="Knit. MC Gauge:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblKnittingMachineGauge" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputLinkingMachineGauge" class="control-label" style="width: 188px;">
                                    <asp:Label ID="lblLinkingMachineGauge1" Font-Bold="true" runat="server" Text="Link. MC Gauge:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblLinkingMachineGauge" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>
                            <%--<div class="control-group">
                                <label for="inputLinkingMachineGauge" class="control-label" style="width: 188px;">
                                    <asp:Label ID="Label11" Font-Bold="true" runat="server" Text="Link. MC Gauge Two:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblLinkingMachineGaugeTwo" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>--%>
                            <div class="control-group">
                                <label for="inputFinishingUnit" class="control-label" style="width: 188px;">
                                    <asp:Label ID="Label14" runat="server" Font-Bold="true" Text="Finishing Unit:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblFinishingUnit" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputKnittingTime" class="control-label" style="width: 188px;">
                                    <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="Knit. Time (Min):"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblKnittingTime" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>
                           
                            <div class="control-group">
                                <label for="inputLinkingPcsperDay" class="control-label" style="width: 188px;">
                                    <asp:Label ID="Label15" Font-Bold="true" runat="server" Text="Yarn In Hanks/Cone:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblYarnInHanksOrCone" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>
                           
                           <%-- <div class="control-group">
                                <label for="inputLinkingPcsperDay" class="control-label" style="width: 188px;">
                                    <asp:Label ID="Label22" Font-Bold="true" runat="server" Text="Weight Per Dzn:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblWeightPerDz" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>--%>
                            <div class="control-group">
                                <label for="inputLinkingPcsperDay" class="control-label" style="width: 188px;">
                                    <asp:Label ID="Label24" Font-Bold="true" runat="server" Text="Auto Linking:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblAutoLinking" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputAdditionalWork" class="control-label" style="width: 188px;">
                                    <asp:Label ID="Label21" Font-Bold="true" runat="server" Text="Additional Work:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Label ID="lblAdditionalWork" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                </div>
                            </div>

                            

                        </div>

                        <div class="col-md-4">
                            
                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="" class="control-label" style="width: 188px">
                                        <asp:Label ID="Label38" Font-Bold="true" runat="server" Text="Remarks:"></asp:Label>
                                    </label>
                                    <div style="height: 320px; overflow-y: auto;">
                                        <asp:Label ID="lblRemarks" runat="server" TextMode="MultiLine" Rows="6" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                    </div>

                                    <%--<div class="controls controls-row" style="margin-left: 188px;">
                                        <asp:Label ID="lblRemarks" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                    </div>--%>
                                </div>
                            </div>
                          
                          
                            <%--<div class="control-group">
                                <label for="inputKnittingStartDate" class="control-label" style="width: 188px;">
                                    <asp:Label ID="lblKnittingStartDate1" Font-Bold="true" runat="server" Text="Booked Quantity:"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="margin-left: 188px;">
                                    <asp:Repeater ID="rptBookingMonths" runat="server">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover" style="margin-top: 20px; margin-left: 10px;">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 33%">
                                                            <asp:Label ID="lblStage" runat="server" Text="Production Month"></asp:Label></th>
                                                        <th style="width: 34%">
                                                            <asp:Label ID="lblMCGauge" runat="server" Text="Booked Quantity"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="vertical-align: middle">
                                                    <%# Eval("MonthName")%>
                                                </td>
                                                <td style="padding: 5px;">
                                                    <%# Eval("BookingQuantity")%>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>--%>
                        </div>

                        <div style="width: 100%; float: left">
                            <%--<div class="col-md-4">
                                <div class="control-group">
                                    <label for="" class="control-label" style="width: 188px">
                                        <asp:Label ID="Label21" Font-Bold="true" runat="server" Text="Delivery Date:"></asp:Label>
                                    </label>
                                    <div class="controls controls-row" style="margin-left: 188px;">
                                        <asp:Label ID="lblDeliveryDate" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                    </div>
                                </div>
                            </div>--%>
                            
                            <div class="col-md-12">
                                <%--<div class="col-md-4" style="padding-left: 0px">
                                    <div class="control-group">
                                        <label for="" class="control-label" style="width: 188px">
                                            <asp:Label ID="Label28" Font-Bold="true" runat="server" Text="PP Approval Date:"></asp:Label>
                                        </label>
                                        <div class="controls controls-row" style="margin-left: 188px;">
                                            <asp:Label ID="lblPPApprovalDate" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                        </div>
                                    </div>
                                </div>--%>
                               
                               
                            </div>

                            <%--<div class="col-md-4">
                                <div class="control-group">
                                    <label for="" class="control-label" style="width: 188px">
                                        <asp:Label ID="Label29" Font-Bold="true" runat="server" Text="Care Label In-house Date:"></asp:Label>
                                    </label>
                                    <div class="controls controls-row" style="margin-left: 188px;">
                                        <asp:Label ID="lblmsclabelInshouseDate" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                    </div>
                                </div>
                            </div>--%>
                            
                          
                            <%--<div class="col-md-4">
                                <div class="control-group">
                                    <label for="" class="control-label" style="width: 188px">
                                        <asp:Label ID="Label31" Font-Bold="true" runat="server" Text="Badge,Fabrics In-house Date:"></asp:Label>
                                    </label>
                                    <div class="controls controls-row" style="margin-left: 188px;">
                                        <asp:Label ID="lblbffInHouseDate" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                    </div>
                                </div>
                            </div>--%>
                          
                           
                            <%--<div class="col-md-4">
                                <div class="control-group">
                                    <label for="" class="control-label" style="width: 188px">
                                        <asp:Label ID="Label34" Font-Bold="true" runat="server" Text="Poly In-house Date:"></asp:Label>
                                    </label>
                                    <div class="controls controls-row" style="margin-left: 188px;">
                                        <asp:Label ID="lblPloyInhouseDate" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                    </div>
                                </div>
                            </div>--%>
                          
                     
                            <%--<div class="col-md-4">
                                <div class="control-group">
                                    <label for="" class="control-label" style="width: 188px">
                                        <asp:Label ID="Label37" Font-Bold="true" runat="server" Text="Other Deadline Date:"></asp:Label>
                                    </label>
                                    <div class="controls controls-row" style="margin-left: 188px;">
                                        <asp:Label ID="lblOtherDeadlineDate" runat="server" Width="100%" CssClass="form-control-without-border"></asp:Label>
                                    </div>
                                </div>
                            </div>--%>
                            
                        </div>
                        <%--//OCWisePo Section Start--%>

                        <!--OC AND PO Part Start--->
                            <div class="col-md-12" id="divOCANDPO" runat="server">
                                <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                        <asp:Label ID="lblPIEntry" runat="server" Text="Booking Data:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    </div> <br/>
                            <div class="control-group">
                                    
                                    <div class="controls" style="overflow-x: auto; margin-left:0px;">
                                        <asp:Repeater ID="rptOCPOEntryInfo" runat="server" OnItemDataBound="rptOCPOEntryInfo_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table22" class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th><asp:Label ID="lblSL" runat="server" Text="SL No"></asp:Label></th>
                                                           
                                                            <th>
                                                                <asp:Label ID="lblQty" runat="server" Text="Booking Qty"></asp:Label></th>
                                                            
                                                            <th>
                                                                <asp:Label ID="lblDeliveryDate" runat="server" Text="Ex Factory Date"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblYarnInhouseDate" runat="server" Text="Yarn In-house Date"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblPPApprovalDate" runat="server" Text="PP Approval Date"></asp:Label></th>
                                                            <%--<th>
                                                                <asp:Label ID="lblSampleApprovalDate" runat="server" Text="Sample Approval Date"></asp:Label></th>--%>
                                                            <th>
                                                                <asp:Label ID="lblKnittingStartDeadlineDate" runat="server" Text="Knitting Start Deadline Date"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblPackingAccesInhouseDate" runat="server" Text="Packing Accessories Inhouse Date"></asp:Label></th>
                                                            
                                                             <%--<th>
                                                                <asp:Label ID="Action" runat="server" Text="Action"></asp:Label></th>--%>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 4%; text-align:center;vertical-align:middle; font-weight:bold"><%# (Container.ItemIndex+1) %></td>
                                                  
                                                    <td style="width: 12%; padding-top: 18px">
                                                        <asp:Label ID="lblQtyValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity")%>'></asp:Label>
                                                    </td>
                                                   
                                                    <td style="width: 12%; padding-top: 18px">
                                                        <asp:Label ID="lblDeliveryDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryDate")%>'></asp:Label>
                                                    </td>
                                                   <td style="width: 12%; padding-top: 18px">
                                                       <asp:Label ID="lblYarnInhouseDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "YarnInhouseDate")%>'></asp:Label>
                                                    </td>
                                                    <td style="width: 12%; padding-top: 18px">
                                                         <asp:Label ID="lblPPApprovalDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PPApprovalDate")%>'></asp:Label>
                                                    </td>
                                                    <%--<td style="width: 12%; padding-top: 18px">
                                                        <asp:Label ID="lblSampleApprovalDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SampleApprovalDate")%>'></asp:Label>
                                                    </td>--%>
                                                    <td style="width: 12%; padding-top: 18px">
                                                        <asp:Label ID="lblKnittingStartDeadlineDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingStartDeadlineDate")%>'></asp:Label>
                                                    </td>
                                                    <td style="width: 12%; padding-top: 18px">
                                                        <asp:Label ID="lblPackingAccesInhouseDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PackingAccesInhouseDate")%>'></asp:Label>
                                                    </td>
                                                    
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <td style="font-weight: 500; text-align: center; background-color: lavender"></td>
                                                
                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                
                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                               
                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                               
                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                 <%--<td style="font-weight: 500; text-align: left; background-color: lavender"></td>--%>
                                                 <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                 <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                
                                                </tbody>
                                            </table>
                                           

                                           
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                </div>
                            <!--OC AND PO Part END---->

                        <%--//OCWisePO Section End--%>

                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
    <script src="../../js/jquery.dataTables.js"></script>
</asp:Content>
