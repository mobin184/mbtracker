﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ItemWiseLcInfo.aspx.cs" Inherits="MBTracker.Pages.Mega.ItemWiseLcInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <div class="row-fluid" runat="server" id="divColors" visible="true">
        <div class="span12">
            <div class="widget" style="overflow-x: auto">
                <div class="widget-body">
                    <div class="row-fluid">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblEntry" runat="server" Text="" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                    <asp:Repeater ID="rptEntryInfo" OnItemDataBound="rptEntryInfo_ItemDataBound" runat="server">
                                        <HeaderTemplate>
                                            <table id="entryTable" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr class="GridViewScrollHeader">
                                                        
                                                        <th style="text-align: center; width: 10%; min-width:350px">
                                                            <asp:Label ID="Label7" runat="server" Text="Yarn Composition"></asp:Label></th>
                                                        <th style="text-align: center; width: 10%; min-width:250px">
                                                            <asp:Label ID="Label3" runat="server" Text="Yarn Category"></asp:Label></th>
                                                        <th style="text-align: center; width: 10%; min-width:350px">
                                                            <asp:Label ID="Label2" runat="server" Text="LC Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label4" runat="server" Text="Available Balance (lbs)"></asp:Label></th>
                                                        
                                                        <th style="">
                                                            <asp:Label ID="Label8" runat="server" Text="Quantity"></asp:Label></th>
                                                        <th style="">
                                                            <asp:Label ID="Label1" runat="server" Text="Remarks"></asp:Label></th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr class="GridViewScrollItem">

                                                
                                                <td>
                                                    <asp:DropDownList ID="ddlItems" runat="server" CssClass="custom-select2" AutoPostBack="true" Width="100%"  Style="min-width: 200px" Display="Dynamic"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlCategory" runat="server" CssClass="custom-select2" AutoPostBack="true" Width="100%" Style="min-width: 200px" Display="Dynamic"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlLcNumber" runat="server" CssClass="custom-select2" AutoPostBack="true" Width="100%" OnSelectedIndexChanged="SelectedIndexChanged" Style="min-width: 200px" Display="Dynamic"></asp:DropDownList>
                                                </td>
                                                
                                                <td>
                                                    <asp:Label ID="lblAvailableBalance" runat="server"></asp:Label>
                                                </td>
                                                
                                               <td style="">
                                                    <asp:TextBox TextMode="Number" Rows="3" ID="tbxQuantity" Style="min-width: 250px" Width="100%" Text="" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="">
                                                    <asp:TextBox TextMode="MultiLine" Rows="3" ID="tbxRemarks" Style="min-width: 250px" Width="100%" Text="" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                </table>
                                                             <%--<div class="pull-right" style="margin-right: 0px; padding-top: 10px">
                                                                 <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="71px" Text="Add Row" OnClick="btnAddRow_Click" />
                                                             </div>--%>
                                            <div class="clearfix"></div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <label for="inputOrder" class="control-label" style="text-align: right; padding-top: 0px">
                <asp:Label ID="lblStyleTotal" runat="server" Text="" Font-Bold="true"></asp:Label>
                <asp:Label ID="lblStyleTotalLbs" runat="server" Text="" Font-Bold="true"></asp:Label>
                <br />
                <%--<asp:LinkButton ID="lnkbtnCalculateStyleTotal" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="Calculate Total" OnClick="lnkbtnCalculateColorTotal_Click"></asp:LinkButton>--%>
            </label>

           <asp:Button ID="btnSaveEntries" runat="server" ValidationGroup="save" class="btn btn-success btn-medium pull-left hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSaveEntries_Click"></asp:Button>&nbsp;&nbsp;&nbsp;
             <%--<asp:Button ID="btnUpdateEntries" Visible="false" ValidationGroup="save" runat="server" class="btn btn-success btn-medium pull-left hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdateEntries_Click"></asp:Button>--%>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
