﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="YarnBookingInfo.aspx.cs" Inherits="MBTracker.Pages.Mega.YarnBookingInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <div class="row-fluid">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="styleActionTitle" Text="Add a New Booking:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 20px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblItems" runat="server" Text="Select Item:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlItems" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlItems"><span style="font-weight: 700; color: #CC0000">Please select a Yarn.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                         <div class="control-group">
                            <label for="inputCategory" class="control-label">
                                <asp:Label ID="lblCategory" runat="server" Text="Select Category:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlYarn_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlCategory"><span style="font-weight: 700; color: #CC0000">Please select a Category.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="lbl1" runat="server" Text="Booking Quantity"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxBookingQuantity" TextMode="Number" runat="server" placeholder="Enter Booking Quantity" CssClass="form-control" Width="250"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxBookingQuantity"><span style="font-weight: 700; color: #CC0000">Enter Booking Quantity</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                         <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label7" runat="server" Text="Booking By"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxBookingBy" runat="server" placeholder="Enter Booking By" type="text" CssClass="form-control" Width="250"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label1" runat="server" Text="Remarks"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxRemarks" TextMode="MultiLine" runat="server" placeholder="Enter Remarks" CssClass="form-control" Width="250"></asp:TextBox>
                                
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="control-group">
                            <br />
                            <div class="controls-row">
                                <asp:Button ID="btnSave" runat="server" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnSave_Click" />
                                <%--<asp:Button ID="btnUpdate" runat="server" Visible="False" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdate_Click" CssClass="btn btn-info pull-right" ValidationGroup="save" />--%>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="span6">
            <asp:Panel ID="pnlColors" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-header">
                        <div class="title">
                            <span class="fs1" aria-hidden="true" data-icon=""></span>Bookings:
                        </div>
                    </div>
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">
                            <asp:Repeater ID="rpt" runat="server" OnItemCommand="rpt_ItemCommand">
                                <HeaderTemplate>
                                    <table id="data-table" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <asp:Label ID="Label2" runat="server" Text="Yarn Name"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label8" runat="server" Text="Yarn Category"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label5" runat="server" Text="Booking Quantity"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label4" runat="server" Text="Booking By"></asp:Label></th>
                                                 <th>
                                                    <asp:Label ID="Label6" runat="server" Text="Booking Date"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label3" runat="server" Text="Actions"></asp:Label></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%#Eval("YarnName") %> </td>
                                        <td><%#Eval("CategoryName") %> </td>
                                        <td><%#Eval("BookingQuantity") %></td>
                                        <td><%#Eval("CreatedBy") %></td>
                                        <td><%# Convert.ToDateTime(Eval("BookingDate")).ToString("yyyy-MM-dd") %></td>
                                        <td>
                                            <%--<asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>--%>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-small hidden-phone" Text="Delete"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                         </table>
                            <div class="clearfix">
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
    <script src="../../js/jquery.dataTables.js"></script>
</asp:Content>
