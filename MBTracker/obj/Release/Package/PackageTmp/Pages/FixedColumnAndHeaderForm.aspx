﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FixedColumnAndHeaderForm.aspx.cs" Inherits="MBTracker.Pages.FixedColumnAndHeaderForm" %>


<%@ Register Src="~/User_Controls/Header.ascx" TagPrefix="uc1" TagName="Header" %>
<%@ Register Src="~/User_Controls/MainMenu.ascx" TagPrefix="uc1" TagName="MainMenu" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>MB Tracker</title>
    <link href="../../css/web.css" rel="stylesheet" />
    <script type="text/javascript" src="../../js/gridviewscroll.js"></script>
    <script type="text/javascript">
        var gridViewScroll = null;
        window.onload = function () {
            gridViewScroll = new GridViewScroll({
                elementID: "entryTable",
                width: 1440,
                height: 500,
                freezeColumn: true,
                freezeFooter: false,
                freezeColumnCssClass: "GridViewScrollItemFreeze",
                freezeFooterCssClass: "GridViewScrollFooterFreeze",
                freezeHeaderRowCount: 1,
                freezeColumnCount: 2,
                onscroll: function (scrollTop, scrollLeft) {
                    console.log(scrollTop + " - " + scrollLeft);
                }
            });
            gridViewScroll.enhance();
        }
        function getScrollPosition() {
            var position = gridViewScroll.scrollPosition;
            alert("scrollTop: " + position.scrollTop + ", scrollLeft: " + position.scrollLeft);
        }
        function setScrollPosition() {
            var scrollPosition = { scrollTop: 50, scrollLeft: 50 };

            gridViewScroll.scrollPosition = scrollPosition;
        }


    </script>

    <script src="../../js/jquery.dataTables.js"></script>

    <script src="../../js/html5-trunk.js"></script>
    <link href="~/css/bootstrap.min.css" rel="stylesheet" />
    <link type="text/css" href="~/icomoon/style.css" rel="stylesheet" />
    <%--<link type="text/css" href="/css/nvd-charts.css" rel="stylesheet" />--%>
    <link type="text/css" href="~/css/main.css" rel="stylesheet" />
    <%--   <link type="text/css" href='/css/fullcalendar/fullcalendar.css' rel='stylesheet' />
    <link type="text/css" href='/css/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />--%>

    <link type="text/css" href="~/css/custom.css" rel="stylesheet" />
    <link type="text/css" href="~/content/toastr.css" rel="stylesheet" />
    <script src="../../js/jquery.min.js"></script>
    <script src="../../js/bootstrap.js"></script>
    <script src="../../Scripts/toastr.js"></script>
    <%--  <link href="../../FancyBox/jquery.fancybox.css" rel="stylesheet" />--%>
    <link href="../../css/font-awesome.min.css" rel="stylesheet" />

    <%--<link href="../../css/all.css" rel="stylesheet" />--%>
    <link href="../../css/sweetalert.css" rel="stylesheet" />
    <script src="../../Scripts/sweetalert.min.js"></script>
    <link href="../../css/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.min.js"></script>
    <script src="../../js/jquery.blockUI.js"></script>
    <script src="../../Scripts/jquery.signalR-2.2.2.js"></script>
    <script src="/SignalR/hubs"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#main-nav ul li ul li").hover(function () {
                $(this).children("ul").toggleClass('test-SubMenu-open', 1, "easeOutSine");
            }, function () {
                $(this).children("ul").toggleClass('test-SubMenu-open', 1, "easeOutSine");
            });
        });

        try {

            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

        } catch (e) {

        }

        function beginRequest(sender, args) {

            $.blockUI({
                //message: '<img src="../../images/loader.gif" />',
                //message: '<div class="spinner"></div>',
                message: '<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>',
                //message: '<div class="lds-ripple"><div></div><div></div></div>',
                css: {
                    border: 'none',
                    backgroundColor: 'transparent'
                },
                overlayCSS: { backgroundColor: '#0d0d0d' }
            });
        }

        function endRequest(sender, args) {
            $.unblockUI();
        }

        function blockUI() {
            $.blockUI({
                message: '<img src="../../images/loader.gif" />',
                css: {
                    border: 'none',
                    backgroundColor: 'transparent'
                },
                overlayCSS: { backgroundColor: '#0d0d0d' }
            });
        }

        function unblockUI() {
            $.unblockUI();
        }

        function scrollDown(position) {
            console.log(position);
            var n = $(document).height();
            if (position != "" && position != 'undefined' && position != null) {
                n = position;
            }
            $('html, body').animate({ scrollTop: n }, 'slow');
        };

        function reloadPage() {
            setTimeout(function () { location.reload(); }, 1000);
        }

        function resetPage() {
            debugger
            $('#form1')[0].reset();
        }


        function returnToPage(url) {
            setTimeout(function () {
                $(location).attr('href', url);
            }, 2000);
        }

        function showConfirm(title, msg, purpose, width, onlyClose) {
            var dialogWidth = "auto";
            var dialogButtons = {
                Yes: function () {
                    $(this).dialog("close");
                    __doPostBack("Confirm", purpose);
                },
                No: function () {
                    $(this).dialog("close");
                }
            };

            if (width != null && width != 'undefined') {
                dialogWidth = width;
            }
            if (onlyClose == 'true') {
                dialogButtons = {
                    Close: function () {
                        $(this).dialog("close");
                    }
                }
            }

            $('<div></div>').appendTo('body')
                .html('<div><h6>' + msg + '</h6></div>')
                .dialog({
                    modal: true,
                    title: title,
                    zIndex: 10000,
                    autoOpen: true,
                    width: dialogWidth,
                    resizable: false,
                    buttons: dialogButtons,
                    close: function (event, ui) {
                        $(this).remove();
                    }
                });
        };


        function showConfirmSA(title, msg, purpose) {
            swal({
                title: title,
                text: msg,
                html: true,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        __doPostBack("Confirm", "ConfirmBtnClicked", purpose);
                    }
                });
        };
    </script>

</head>


<body style="margin: 0px; width: 100%; height: 100%;">
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <header id="header">
            <uc1:Header runat="server" ID="Header" />
        </header>

        <div class="container" id="menu" style="margin: 0px;">
            <div id="main-nav" class="hidden-phone hidden-tablet">
                <uc1:MainMenu runat="server" ID="MainMenu" />
                <div class="clearfix"></div>
            </div>
        </div>

                    <%-- Start Main Content --%>





                    <%-- End Main Content --%>


         <div class="col-md-12" id="footer">
            <div class="row" style="text-align: center; color: #f79825">
                <b>Copyright © 2020 <a style="text-decoration: none; color: #f79825;" href="http://www.masihata.com/">Masihata Group</a> </b>
            </div>
        </div>

    </form>
</body>
</html>

