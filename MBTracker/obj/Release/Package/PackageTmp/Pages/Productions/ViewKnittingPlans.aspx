﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="ViewKnittingPlans.aspx.cs" Inherits="MBTracker.Pages.Productions.ViewKnittingPlans" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <div class="row-fluid" id="divBuyerAndStyle" runat="server">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblMachinePlanning" Text="View Knitting Plans:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 5px">
                           <%-- <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field--%>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputStyle" class="control-label">
                                <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <%--<div class="control-group">
                            <label for="inputOrder" class="control-label">
                                <asp:Label ID="lblOrder" runat="server" Text="Select Order:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlOrders_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlOrders"><span style="font-weight: 700; color: #CC0000">Please select an order.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>--%>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="&nbsp;"></asp:Label></label>
                            <div class="controls controls-row">
                                <br />
                                <asp:Button ID="btnViewKnittingPlan" ValidationGroup="save" runat="server"  class="btn btn-success btn-midium pull-left btnStyle" Text="View Knitting Plan" OnClick="btnViewKnittingPlan_Click" />
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    
      <label for="inputStyle" class="control-label" style="font-weight:bold; padding-top:100px; margin-left:200px; text-align:left">
        <asp:Label ID="lblNotAuthorizedToView" Font-Bold="true" Visible="false" Font-Size="Large" runat="server" Text="You are not authorized to view this information."></asp:Label>
     </label>


    <div class="row-fluid">
        
        <div class="span9">
            <asp:Label ID="lblNoMCAssignedMessage" runat="server" Visible="false" Text="<br/><br/>Knitting plan was not found." BackColor="#ffff00"></asp:Label>
            <asp:Panel ID="pnlAssignMachinesToOrder" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="Label3" runat="server" Text="Existing Plans:" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rpt" runat="server" OnItemCommand="rpt_ItemCommand">
                                        <HeaderTemplate>
                                            <table id="data-table-1" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="Label2" runat="server" Text="Unit"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label5" runat="server" Text="MC Brand"></asp:Label></th>
                                                         <th>
                                                            <asp:Label ID="Label1" runat="server" Text="Issued Qty"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label12" runat="server" Text="# of MC"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label10" runat="server" Text="Start Date"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label11" runat="server" Text="End Date"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label3" runat="server" Text="Actions"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%#Eval("UnitName") %> </td>
                                                <td><%#Eval("MachineBrandName") %> </td>
                                                <td><%#Eval("IssueQuantity") %> </td>
                                                <td><%#Eval("NumberOfMachines") %></td>
                                                <td><%# String.Format("{0:dd-MMM-yyyy}", Eval("ProdStartDate"))%></td>
                                                <td><%# String.Format("{0:dd-MMM-yyyy}", Eval("ProdEndDate"))%></td>

                                                <td>
                                                   <%-- <asp:LinkButton ID="lbkbtnEdit" runat="server" CommandName="Edit" Visible='<%# ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("MCAssignedId") %>' class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%# ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("MCAssignedId") %>' class="btn btn-danger btn-small hidden-phone" Text="Delete"></asp:LinkButton>
                                                    --%>

                                                     <asp:LinkButton ID="lbkbtnEdit" runat="server" CommandName="Edit" Visible='<%#Eval("CanEdit")%>' CommandArgument='<%#Eval("MCAssignedId") %>' class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#Eval("CanDelete")%>' CommandArgument='<%#Eval("MCAssignedId") %>' class="btn btn-danger btn-small hidden-phone" Text="Delete"></asp:LinkButton>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                         </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div class="span3">
            <asp:Panel ID="pnlPlanningInfo" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-body">
                        <div style="width: 100%" id="divTotalOrderQuantity" runat="server" visible="false">
                            </div>
                            <br />
                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label5" runat="server" Text="Planning Information:" Font-Bold="true"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:Repeater ID="rptPlanInfo" runat="server">
                                    <HeaderTemplate>
                                        <table id="data-table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="hidden">
                                                        <asp:Label ID="lblOrderQty" runat="server" Text="Order<br/>Quantity"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblMachineBrand" runat="server" Text="Machine<br/>Brand"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblMachineGauge" runat="server" Text="Machine<br/>Gauge"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblKnittingTime" runat="server" Text="Knitting<br/>Time"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td class="hidden" style="text-align: center" width="15%"><%#Eval("OrderQuantity") %> </td>
                                            <td style="text-align: center" width="35%">
                                                <asp:Label ID="lblMCBrandName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BrandName")%>'></asp:Label><asp:Label ID="lblMCBrandId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "MachineBrandId")%>'></asp:Label>
                                                <asp:DropDownList ID="ddlMachineBrands" runat="server" Display="Dynamic" CssClass="form-control" Visible="false"></asp:DropDownList>
                                            </td>
                                            <td style="text-align: center" width="15%">
                                                <asp:Label ID="lblMCGauge" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingMachineGauge")%>'></asp:Label>
                                                <asp:TextBox ID="tbxKnittingMCGauge" runat="server" CssClass="form-control" placeholder="MC Gauge" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingMachineGauge")%>'></asp:TextBox>
                                            </td>
                                            <td style="text-align: center" width="15%">
                                                <asp:Label ID="lblKnittingTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingTime")%>'></asp:Label>
                                                <asp:TextBox ID="tbxKnittingTime" runat="server" placeholder="Knitting time" CssClass="form-control" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingTime")%>'></asp:TextBox>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                            </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Label ID="lblNoPlanningInfo" runat="server" Visible="false" Text="<br/><br/>No planning info found." BackColor="#ffff00"></asp:Label>
                            </div>
                        </div>

                       

                        <asp:Panel ID="pnlShipmentInfo" runat="server" Visible="false">
                            <div class="control-group" style="line-height: 10px">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblShipmentDates" runat="server" Text="Shipment Summary:" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rptShipmentSummary" runat="server" OnItemDataBound="rptShipmentSummary_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblCol1" runat="server" Text="Shipment<br/>Dates"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblCol3" runat="server" Text="Shipment<br/> Quantity"></asp:Label></th>
                                                        <th class="hidden">
                                                            <asp:Label ID="lblCol4" runat="server" Text="Machines<br/> Required"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center; vertical-align: middle">
                                                    <asp:Label ID="lblShipmentDates" runat="server" Text=' <%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "ShipmentDate"))%>'></asp:Label></td>
                                                <td style="text-align: center; vertical-align: middle">
                                                    <asp:Label ID="lblShipmentQuantity" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShipmentQuantity")%>'></asp:Label>
                                                </td>
                                                <td class="hidden" style="text-align: center; vertical-align: middle">
                                                    <asp:Label ID="lblMCRequired" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                            </table>
                                            <div class="clearfix">
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <asp:Label ID="lblNoShippingInfo" runat="server" Visible="false" Text="<br/><br/>No shipping info found." BackColor="#ffff00"></asp:Label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>




<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
