﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewEditRejections.aspx.cs" Inherits="MBTracker.Pages.Productions.ViewEditRejections" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid" id="divBuyerAndStyle" runat="server">
        <div class="span6">

            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="View Rejections"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 10px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblOrder" runat="server" Text="Select Order:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="&nbsp;"></asp:Label></label>
                            <div class="controls controls-row">
                                <br />
                                <asp:Button ID="btnViewRejection" runat="server" class="btn btn-success btn-midium pull-left btnStyle" Text="View Rejections" OnClick="btnViewRejection_Click" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-10 ">
        <asp:Label ID="lblNoRejectionDataFound" runat="server" Text="No data found." Visible="false" BackColor="#ffff00"></asp:Label>
    </div>



    <div class="row-fluid">
        <div class="span12">
            <asp:Panel ID="pnlColorAndRejectionQuantity" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-body" style="overflow-x: auto">
                        <div class="form-horizontal">
                            <div style="text-align: left; font-size: 20px; line-height: 30px; padding-bottom: 15px">Rejection Details:</div>

                            <div id="dt_example2" class="example_alt_pagination">
                                <asp:Repeater ID="rptRejections" runat="server" OnItemDataBound="rptRejections_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lblCol1" runat="server" Text="Rejection Floor"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblCol2" runat="server" Text="Rejection Time"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label1" runat="server" Text="Reported By"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblCol3" runat="server" Text="Color, Size, Quantity & Notes"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblAction" runat="server" Text="Action(s)"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>

                                    <ItemTemplate>
                                        <tr>
                                            <td style="text-align: center; vertical-align: middle; width: 12%">
                                                <asp:Label ID="lblFinishingFloor" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "FinishingFloorName")%>'></asp:Label><asp:Label ID="lblRejectionId" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "RejectionId")%>' Visible="false"></asp:Label></td>
                                            <td style="text-align: center; vertical-align: middle; width: 12%">
                                                <asp:Label ID="lblRejectionDate" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "DateReported")%>'></asp:Label></td>
                                            <td style="text-align: center; vertical-align: middle; width: 12%">
                                                <asp:Label ID="Label3" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ReportedBy")%>'></asp:Label></td>
                                            <td style="vertical-align: middle; width: 52%">
                                                <asp:Repeater ID="rptRejectionColors" runat="server" OnItemDataBound="rptRejectionColors_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="data-table" class="table table-bordered table-hover">

                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="text-align: left; vertical-align: middle; width: 20%">
                                                                <asp:Label ID="lblColor" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ColorDescription")%>'></asp:Label>
                                                                <asp:Label ID="lblRejectionId2" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ProductRejectionsId")%>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblRejectionColorId" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>' Visible="false"></asp:Label>
                                                            </td>
                                                            <td style="text-align: center; width: 50%">
                                                                <asp:GridView ID="gvSizeAndQty" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeAndQty_RowCreated" Width="90%"></asp:GridView>
                                                            </td>

                                                            <td style="text-align: left; width: 30%">
                                                                <asp:Label ID="lblNotes" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Notes")%>'></asp:Label>
                                                            </td>

                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                                                    </table>
                                                                    <div class="clearfix">
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                             <td style="text-align: center; vertical-align: middle; width: 12%">
                                                 <asp:LinkButton ID="lnkbtnEdit" Visible='<%# ViewState["editEnabled"]%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("RejectionId") %>' class="btn btn-success btn-samll pull-center btnStyle" OnCommand="lnkbtnEdit_Command" Text="&nbsp;&nbsp;Edit&nbsp;&nbsp;"></asp:LinkButton>
                                             </td>

                                        </tr>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        </tbody>
                                                    </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </asp:Panel>
        </div>
    </div>


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
