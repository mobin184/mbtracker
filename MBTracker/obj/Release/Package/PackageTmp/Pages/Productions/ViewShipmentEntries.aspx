﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewShipmentEntries.aspx.cs" Inherits="MBTracker.Pages.Productions.ViewShipmentEntries" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table th, .table td {
            text-align: center;
        }
    </style>
    <div class="row-fluid">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="actionTitle" Text="View Shipment Entries:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row">
                        <div class="form-horizontal">
                            <div class="col-md-5 pull-right" style="text-align: right; font-size: 12px; line-height: 20px">
                                <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                            </div>
                            <div class="control-group">
                                <div class="col-md-12 col-sm-12">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblOrder" runat="server" Text="Select Order:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="true" Display="Dynamic" Width="250"  CssClass="form-control" OnSelectedIndexChanged="ddlOrders_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlOrders"><span style="font-weight: 700; color: #CC0000">Please select an order.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <br />
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label11" runat="server" Text=""></asp:Label></label>
                                    <div class="controls controls-row">
                                      <asp:Button runat="server" CssClass="btn btn-info" ID="btnViewEntries" Text="View Entries" ValidationGroup="save" OnClick="btnViewEntries_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="span12" style="margin-left: 0px">
            <asp:Label ID="lblNoDataFound" runat="server" Text="No data found!" Visible="false" BackColor="#ffff00"></asp:Label>
            <div class="widget"  runat="server" id="divDetailsInfo" visible="false" >
                <div class="widget-body">
                    <div id="dt_example2" class="example_alt_pagination">
                        <asp:Repeater ID="rptInfo" runat="server">
                            <HeaderTemplate>
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="lbl1" runat="server" Text="Buyer Name"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label2" runat="server" Text="Style Name"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label5" runat="server" Text="Order Number"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label6" runat="server" Text="Finishing Floor"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label1" runat="server" Text="Shipment Date"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label3" runat="server" Text="Shipped Quantity"></asp:Label></th>
                                            <th >
                                                <asp:Label ID="Label4" runat="server" Text="Take Actions"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%#Eval("BuyerName")%></td>
                                    <td><%#Eval("StyleName")%></td>                                    
                                    <td><%#Eval("OrderNumber")%></td>
                                    <td><%#Eval("FloorName")%></td>
                                    <td><%#string.Format("{0:dd-MMM-yyyy}",Eval("ShipmentDate"))%></td>
                                    <td><%#Eval("TotalShippedQty")%></td>
                                    <td>
                                        <asp:LinkButton ID="lnkbtnEdit" runat="server" Visible='<%# Eval("CanEdit")%>' CommandName="Edit" CommandArgument='<%#Eval("Id") %>' OnCommand="lnkbtnEdit_Command" class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#Eval("CanDelete")%>' CommandArgument='<%#Eval("Id") %>' OnCommand="lnkbtnDelete_Command" class="btn btn-danger btn-small hidden-phone" Text="Delete"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="View" CommandArgument='<%#Eval("Id") %>' OnCommand="lnkbtnView_Command" class="btn btn-success btn-small hidden-phone" Text="View Details"></asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                            </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="span12" id="pnlColorDeliveryCountryAndQuantity" runat="server" visible="false" style="margin-left: 0px;">
            <div class="widget">
                <div class="widget-body">
                    <div class="controls controls-row">
                        <asp:Label ID="lblColorOrDeliveryCountryNotFound" runat="server" Text="" Visible="false" BackColor="#ffff00"></asp:Label>
                    </div>
                    <asp:Panel runat="server">
                        <div class="control-group">
                            <label for="inputDeliveryCountry" class="control-label">
                                <asp:Label ID="lblColorAndSize" runat="server" Text="Packing Information:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                        </div>
                        <div class="controls controls-row">
                            <div id="dt_example2" class="example_alt_pagination">
                                <asp:Repeater ID="rptColorAndDeliveryCountries" runat="server" OnItemDataBound="rptColorAndDeliveryCountries_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="width: 200px">
                                                        <asp:Label ID="lblColorDesc" runat="server" Text="Color"></asp:Label></th>
                                                    <th style="width: 350px">
                                                        <asp:Label ID="lblColorDescription" runat="server" Text="Packing Country, Size & Quantity"></asp:Label></th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="vertical-align: middle">
                                                <asp:Label ID="lblColorDesc" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ColorDescription")%>'></asp:Label><asp:Label ID="lblColorId" Visible="false" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Repeater ID="rptDeliveryCountryAndQty" runat="server" OnItemDataBound="rptDeliveryCountryAndQty_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="data-table" class="table table-bordered table-hover">
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="vertical-align: middle">
                                                                <asp:Label ID="lblDeliveryCountryId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryCountryId")%>'></asp:Label><asp:Label ID="lblDeliveryCountryName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryCountryCode")%>'></asp:Label></th></td>
                                                            <td>
                                                                <asp:GridView ID="gvSizeAndQty" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeAndQty_RowCreated"></asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                                                </table>
                                                                <div class="clearfix">
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                <asp:Label ID="lblBuyerColorId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                            </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
