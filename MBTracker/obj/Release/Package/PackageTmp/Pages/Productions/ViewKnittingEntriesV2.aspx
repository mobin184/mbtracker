﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewKnittingEntriesV2.aspx.cs" Inherits="MBTracker.Pages.Productions.ViewKnittingEntriesV2" %>



<%@ Register Src="~/User_Controls/Header.ascx" TagPrefix="uc1" TagName="Header" %>
<%@ Register Src="~/User_Controls/MainMenu.ascx" TagPrefix="uc1" TagName="MainMenu" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>MB Tracker</title>
    <link href="../../css/web.css" rel="stylesheet" />
    <script type="text/javascript" src="../../js/gridviewscroll.js"></script>
    <script type="text/javascript">
        var gridViewScroll = null;
        window.onload = function () {
            gridViewScroll = new GridViewScroll({
                elementID: "data-table4",
                width: 1425,
                height: 500,
                freezeColumn: true,
                freezeFooter: false,
                freezeColumnCssClass: "GridViewScrollItemFreeze",
                freezeFooterCssClass: "GridViewScrollFooterFreeze",
                freezeHeaderRowCount: 1,
                freezeColumnCount: 3,
                onscroll: function (scrollTop, scrollLeft) {
                    console.log(scrollTop + " - " + scrollLeft);
                }
            });
            gridViewScroll.enhance();
        }
        function getScrollPosition() {
            var position = gridViewScroll.scrollPosition;
            alert("scrollTop: " + position.scrollTop + ", scrollLeft: " + position.scrollLeft);
        }
        function setScrollPosition() {
            var scrollPosition = { scrollTop: 50, scrollLeft: 50 };

            gridViewScroll.scrollPosition = scrollPosition;
        }
    </script>

    <script src="../../js/html5-trunk.js"></script>
    <link href="~/css/bootstrap.min.css" rel="stylesheet" />
    <link type="text/css" href="~/icomoon/style.css" rel="stylesheet" />
    <%--<link type="text/css" href="/css/nvd-charts.css" rel="stylesheet" />--%>
    <link type="text/css" href="~/css/main.css" rel="stylesheet" />
    <%--   <link type="text/css" href='/css/fullcalendar/fullcalendar.css' rel='stylesheet' />
    <link type="text/css" href='/css/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />--%>

    <link type="text/css" href="~/css/custom.css" rel="stylesheet" />
    <link type="text/css" href="~/content/toastr.css" rel="stylesheet" />
    <script src="../../js/jquery.min.js"></script>
    <script src="../../js/bootstrap.js"></script>
    <script src="../../Scripts/toastr.js"></script>
    <%--  <link href="../../FancyBox/jquery.fancybox.css" rel="stylesheet" />--%>
    <link href="../../css/font-awesome.min.css" rel="stylesheet" />

    <%--<link href="../../css/all.css" rel="stylesheet" />--%>
    <link href="../../css/sweetalert.css" rel="stylesheet" />
    <script src="../../Scripts/sweetalert.min.js"></script>
    <link href="../../css/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.min.js"></script>
    <script src="../../js/jquery.blockUI.js"></script>
    <script src="../../Scripts/jquery.signalR-2.2.2.js"></script>
    <script src="/SignalR/hubs"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#main-nav ul li ul li").hover(function () {
                $(this).children("ul").toggleClass('test-SubMenu-open', 1, "easeOutSine");
            }, function () {
                $(this).children("ul").toggleClass('test-SubMenu-open', 1, "easeOutSine");
            });
        });

        try {

            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

        } catch (e) {

        }

        function beginRequest(sender, args) {

            $.blockUI({
                //message: '<img src="../../images/loader.gif" />',
                //message: '<div class="spinner"></div>',
                message: '<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>',
                //message: '<div class="lds-ripple"><div></div><div></div></div>',
                css: {
                    border: 'none',
                    backgroundColor: 'transparent'
                },
                overlayCSS: { backgroundColor: '#0d0d0d' }
            });
        }

        function endRequest(sender, args) {
            $.unblockUI();
        }

        function blockUI() {
            $.blockUI({
                message: '<img src="../../images/loader.gif" />',
                css: {
                    border: 'none',
                    backgroundColor: 'transparent'
                },
                overlayCSS: { backgroundColor: '#0d0d0d' }
            });
        }

        function unblockUI() {
            $.unblockUI();
        }

        function scrollDown(position) {
            console.log(position);
            var n = $(document).height();
            if (position != "" && position != 'undefined' && position != null) {
                n = position;
            }
            $('html, body').animate({ scrollTop: n }, 'slow');
        };

        function reloadPage() {
            setTimeout(function () { location.reload(); }, 1000);
        }

        function resetPage() {
            debugger
            $('#form1')[0].reset();
        }


        function returnToPage(url) {
            setTimeout(function () {
                $(location).attr('href', url);
            }, 2000);
        }

        function showConfirm(title, msg, purpose, width, onlyClose) {
            var dialogWidth = "auto";
            var dialogButtons = {
                Yes: function () {
                    $(this).dialog("close");
                    __doPostBack("Confirm", purpose);
                },
                No: function () {
                    $(this).dialog("close");
                }
            };

            if (width != null && width != 'undefined') {
                dialogWidth = width;
            }
            if (onlyClose == 'true') {
                dialogButtons = {
                    Close: function () {
                        $(this).dialog("close");
                    }
                }
            }

            $('<div></div>').appendTo('body')
                .html('<div><h6>' + msg + '</h6></div>')
                .dialog({
                    modal: true,
                    title: title,
                    zIndex: 10000,
                    autoOpen: true,
                    width: dialogWidth,
                    resizable: false,
                    buttons: dialogButtons,
                    close: function (event, ui) {
                        $(this).remove();
                    }
                });
        };


        function showConfirmSA(title, msg, purpose) {
            swal({
                title: title,
                text: msg,
                html: true,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        __doPostBack("Confirm", "ConfirmBtnClicked", purpose);
                    }
                });
        };




    </script>

</head>



<body style="margin: 0px; width: 100%; height: 100%;">
    <form id="form1" runat="server">


        <style>
            .table th, .table td {
                padding: 4px;
                line-height: 10px;
                text-align: center;
                vertical-align: top;
                border-top: 1px solid #e0e0e0;
            }
        </style>

        <header id="header">
            <uc1:Header runat="server" ID="Header" />
        </header>

        <div class="container" id="menu" style="margin: 0px;">
            <div id="main-nav" class="hidden-phone hidden-tablet">
                <uc1:MainMenu runat="server" ID="MainMenu" />
                <div class="clearfix"></div>
            </div>
        </div>




        <div class="container-fluid">
            <div class="dashboard-wrapper">
                <div class="main-container" id="body">
                    <div class="page-header">
                        <div class="clearfix"></div>
                    </div>
                    <div>
                    </div>

                    <%-- Main Content Start --%>



                    <div class="row-fluid">
                        <div class="span6">
                            <div class="widget">

                                <div class="widget-header">
                                    <div class="title">
                                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                                        <asp:Label runat="server" ID="lblTitle" Text="View Daily Knittings:"></asp:Label>
                                    </div>
                                </div>

                                <div class="widget-body">

                                    <div class="row-fluid" runat="server" visible="true" id="FilterDiv">
                                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                                            <%-- <span style="font-weight: 700; padding-right: 400px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field--%>
                                        </div>
                                        <div class="control-group">
                                            <div class="col-md-12 col-sm-12">
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                            </div>
                                        </div>
                                        <div class="col-md-10" style="padding-left: 0px">

                                            <div class="col-md-9" style="padding-left: 0px">
                                                <div class="form-horizontal">
                                                    <div class="control-group">
                                                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">
                                                            <asp:Label ID="lblFilterDate" runat="server" Text="Select Knitting Date:"></asp:Label>
                                                            <span style="font-weight: 700; color: #CC0000">*</span>
                                                        </label>
                                                        <div class="controls controls-row" style="margin-left: 150px">
                                                            <asp:TextBox ID="tbxFilterDate" runat="server" placeholder="Enter date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                                ControlToValidate="tbxFilterDate"><span style="font-weight: 700; color: #CC0000">Please select a knitting date.</span></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>

                                                    <div class="control-group">
                                                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">
                                                            <asp:Label ID="Label8" runat="server" Text=""></asp:Label></label>
                                                        <div class="controls controls-row" style="margin-left: 150px">
                                                            <br />
                                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="View Knittings" CssClass="btn btn-info pull-left" OnClick="btnSearch_Click" />
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row-fluid" runat="server">
                        <div class="span8">
                            <div class="widget" id="dataDiv" runat="server" visible="false">
                                <div class="widget-body">
                                    <div id="dt_example" class="example_alt_pagination">
                                        <div class="control-group">
                                            <label for="inputOrder" class="control-label">
                                                <asp:Label runat="server" ID="lblLabelMessage" Text="Daily Knittings:" Font-Bold="true" Visible="false"></asp:Label></label>
                                            <div class="controls controls-row">
                                                <asp:Repeater ID="rptUnits" runat="server" OnItemDataBound="rptUnits_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="data-table1" class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th class="hidden">
                                                                        <asp:Label ID="lblUnitId" runat="server" Text="UnitId"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="Label22" runat="server" Text="Units"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="Label7" runat="server" Text="Shifts & Quantity"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblDailyUnitTotal" runat="server" Text="Total"></asp:Label></th>
                                                                     <th>
                                                                        <asp:Label ID="lblActions" runat="server" Text="Actions"></asp:Label></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                    </HeaderTemplate>


                                                    <ItemTemplate>
                                                        <tr>
                                                            <td class="hidden">
                                                                <asp:Label ID="lblUnitIdValue" runat="server"></asp:Label></td>
                                                            <td><%#Eval("UnitName") %></td>

                                                            <td style="padding: 0px; line-height: 1; width:40%">

                                                                <asp:Repeater ID="rpt" runat="server" OnItemDataBound="rpt_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="data-table1" class="table table-bordered table-hover" style="margin-bottom: 0px">

                                                                            <tbody>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td style="width: 30%"><%#Eval("ShiftName") %></td>
                                                                            <td style="width: 30%"><%#Eval("Production") %></td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </tbody>
                                                         </table>
                                                            <div class="clearfix">
                                                                    </FooterTemplate>
                                                                </asp:Repeater>

                                                            </td>
                                                            <td style="width:15%">
                                                                <asp:Label ID="lblDailyUnitTotalValue" runat="server" Text=""></asp:Label>

                                                            </td>

                                                            <td>
                                                                <asp:Repeater ID="rptActions" runat="server">
                                                                    <HeaderTemplate>
                                                                        <table id="data-table1" class="table table-bordered table-hover" style="margin-bottom: 0px">

                                                                            <tbody>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td class="hidden"><%#Eval("KnittingUnitId") %></td>

                                                                            <td>
                                                                                <%--<asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("KnittingUnitId")+","+ Eval("KnittingDate")+","+ Eval("ShiftId")%>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkbtnEdit" Visible='<%# ViewState["editEnabled"]%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("KnittingUnitId")+","+ Eval("KnittingDate")+","+ Eval("ShiftId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="View" CommandArgument='<%#Eval("KnittingUnitId")+","+ Eval("KnittingDate")+","+ Eval("ShiftId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnView_Command" Text="View Details"></asp:LinkButton>--%>

                                                                                <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#Eval("CanDelete")%>' CommandArgument='<%#Eval("KnittingUnitId")+","+ Eval("KnittingDate")+","+ Eval("ShiftId")%>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkbtnEdit" Visible='<%# Eval("CanEdit")%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("KnittingUnitId")+","+ Eval("KnittingDate")+","+ Eval("ShiftId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="View" CommandArgument='<%#Eval("KnittingUnitId")+","+ Eval("KnittingDate")+","+ Eval("ShiftId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnView_Command" Text="View Details"></asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </tbody>
                                                         </table>
                                                            <div class="clearfix">
                                                                    </FooterTemplate>
                                                                </asp:Repeater>

                                                            </td>

                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                                         </table>
                                                            <div class="clearfix">
                                                    </FooterTemplate>
                                                </asp:Repeater>

                                                <div style="text-align:center">
                                                    <asp:Label ID="lblNightTotal" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    <asp:Label ID="lblDayTotal" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    <asp:Label ID="lblGrandTotal" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>

                    <div class="row-fluid" runat="server" id="pnlSummary" visible="false">
                        <div class="span12" style="margin-top: 10px">

                            <div class="col-md-6" style="padding-left: 0px">
                                <div id="dt_example" class="example_alt_pagination">

                                    <div class="widget">
                                        <div class="widget-body">

                                            <div class="control-group">
                                                <label for="inputOrder" class="control-label">
                                                    <asp:Label ID="lblSummary" Visible="false" runat="server" Text="" Font-Bold="true"> Unit & Production Date Info:</asp:Label></label>
                                                <div class="controls controls-row">
                                                    <asp:Repeater ID="rptGeneralInfo" runat="server" OnItemDataBound="rptGeneralInfo_ItemDataBound" OnItemCreated="rptGeneralInfo_ItemCreated">
                                                        <HeaderTemplate>
                                                            <table id="data-table" class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <asp:Label ID="lblProductionUnit" Enabled="false" runat="server" Text="Production Unit"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="lblProductionDate" runat="server" Text="Production Date"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="Label5" runat="server" Text="Shift"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="lblUser" runat="server" Text="User Name"></asp:Label></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlUserUnits" runat="server" Width="100" AutoPostBack="true"></asp:DropDownList></td>

                                                                <td><%#Eval("ProductionEntryDate") %><asp:TextBox ID="tbxDeliveryDate" Visible="false" Text='<%#Eval("ProductionEntryDate") %>' runat="server"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlShift" runat="server" Width="100"></asp:DropDownList></td>
                                                                <td><%#Eval("UserName") %> </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody>
                                 </table>
                                    <div class="clearfix">
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="row-fluid" runat="server" id="pnlDetails" visible="false">
                        <div class="span12">

                            <div class="widget">
                                <div class="widget-body">

                                    <div id="dt_example" class="example_alt_pagination">
                                        <div class="control-group">
                                            <label for="inputOrder" class="control-label">
                                                <asp:Label ID="lblDetails" runat="server" Visible="false" Text="Detailed Information:" Font-Bold="true"></asp:Label></label>
                                            <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                                <asp:Repeater ID="rptEntryInfo" runat="server" OnItemDataBound="rptEntryInfo_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="data-table4">
                                                            <thead>
                                                                <tr class="GridViewScrollHeader">
                                                                    <th style="text-align: center">
                                                                        <asp:Label ID="lblBuyer" runat="server" Text="Buyer<br/> Names"></asp:Label></th>
                                                                    <th style="text-align: center">
                                                                        <asp:Label ID="lblStyle" runat="server" Text="Styles<br/> Names"></asp:Label></th>
                                                                    <th style="text-align: center">
                                                                        <asp:Label ID="lblColor" runat="server" Text="Color<br/> Names"></asp:Label></th>
                                                                    <th class="hidden">
                                                                        <asp:Label ID="lblTotalIssue" runat="server" Text="Yarn<br/> Issued"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblMCBrand" runat="server" Text="Machine<br/> Brand"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblMCGauge" runat="server" Text="Machine<br/> Gauge"></asp:Label></th>
                                                                    <th class="hidden">
                                                                        <asp:Label ID="Label1" runat="server" Text="Jacquard<br/> Portion"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="Label2" runat="server" Text="Number of <br/>MC Used"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="Label3" runat="server" Text="Finishing<br/> Unit"></asp:Label></th>
                                                                    <th style="text-align: center">
                                                                        <asp:Label ID="Label4" runat="server" Text="Actual Knittings by Size<br/> &nbsp;"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblColorTotal" runat="server" Text="Color Total"></asp:Label></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr class="GridViewScrollItem">
                                                            <td style="text-align: left; padding-top: 20px">
                                                                <asp:Label runat="server" Text='<%#Eval("BuyerName") %>'></asp:Label>
                                                                <asp:Label ID="lblStyleId" runat="server" Text='<%#Eval("StyleId") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblBuyerId" runat="server" Text='<%#Eval("BuyerId") %>' Visible="false"></asp:Label>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:Label runat="server" Text='<%#Eval("StyleName") %>'></asp:Label></td>
                                                            <td style="text-align: left"><%#Eval("ColorDescription") %>
                                                                <asp:Label ID="lblBuyerColorId" runat="server" Text='<%#Eval("BuyerColorId") %>' Visible="false"></asp:Label></td>

                                                            <td class="hidden">
                                                                <asp:Label ID="lblUnitId" runat="server" Text='<%#Eval("UnitId") %>' Visible="false"></asp:Label>
                                                            </td>
                                                            <td><%#Eval("BrandName") %>
                                                                <asp:Label ID="lblMachineBrandId" runat="server" Text='<%#Eval("MachineBrandId") %>' Visible="false"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblKnittingMCGauge" runat="server" Text='<%#Eval("KnittingMachineGauge") %>'></asp:Label>
                                                                <asp:Label ID="lblTarget" runat="server" Text='<%#Eval("Target") %>' Visible="false"></asp:Label></td>
                                                            
                                                            <td class="hidden">
                                                                <asp:DropDownList ID="ddlJacquardPortion" Enabled="false" runat="server" Width="100px">
                                                                    <asp:ListItem Text="Complete" Value="Complete"></asp:ListItem>
                                                                    <asp:ListItem Text="No Work" Value="Nowork"></asp:ListItem>
                                                                    <asp:ListItem Text="Technical" Value="Technical"></asp:ListItem>
                                                                    <asp:ListItem Text="Sample" Value="Sample"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>

                                                            <td>
                                                                <asp:TextBox ID="tbxMCUsed" Enabled="false" runat="server" Text='<%#Eval("NumberOfMachines") %>'></asp:TextBox></td>
                                                             <td>
                                                                <asp:Label ID="lblasdfasd" runat="server" Text='<%#Eval("FinishingUnitName") %>'></asp:Label>
                                                                <asp:Label ID="lblFinishingUnitId" runat="server" Text='<%#Eval("FinishingUnitId") %>' Visible="false"></asp:Label>
                                                             </td>
                                                            <td>
                                                                <asp:GridView ID="gvSizeQuantity2" RowStyle-Height="30" Style="margin-left: 5px" RowStyle-HorizontalAlign="Left" runat="server" HeaderStyle-BackColor="WhiteSmoke" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity_RowCreated2">
                                                                </asp:GridView>

                                                            </td>
                                                            <td style="text-align: center">
                                                                <asp:Label ID="lblColorTotalValue" runat="server" Text=""></asp:Label></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                 </table>
                                    <div class="clearfix"></div>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                <asp:Label ID="lblNoKnittingGoingOn" runat="server" Text="Production was not planned in this unit today." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                        </div>
                    </div>



                    <%-- Main Content End --%>
                </div>
            </div>
        </div>

        <div class="col-md-12" id="footer">
            <div class="row" style="text-align: center; color: #f79825">
                <b>Copyright © 2020 <a style="text-decoration: none; color: #f79825;" href="http://www.masihata.com/">Masihata Group</a> </b>
            </div>
        </div>

    </form>
</body>
</html>

