﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewEditWashProductionInfo.aspx.cs" Inherits="MBTracker.Pages.Productions.ViewEditWashProductionInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
     <style>
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th {
            padding: 4px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 5px 3px;
            white-space: nowrap;
        }
    </style>
    <div class="row-fluid">
        <div class="span8">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="View Wash Production Info:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row" style="padding-left: 15px; padding-right: 15px;">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                            
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="search" />
                            </div>
                        </div>
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px;">
                            <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000"></span>.
                        </div>
                        <div class="col-md-6" style="padding-right: 0px">
                            <div class="form-horizontal">
                                <div class="control-group" style="padding-top: 20px;">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label1" runat="server" Text="Wash Production Date:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxWashProductionDate" AutoPostBack="true" TextMode="Date" Width="100%" Text="" runat="server"></asp:TextBox>
                                        
                                    </div>
                                </div>
                                <div class="control-group">
                                <label for="inputBuyer" class="control-label" style="width: 140px; padding-top: 5px;">
                                    <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                <div class="controls controls-row" >
                                    <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    
                                </div>
                            </div>
                              

                                <div class="control-group">
                                    <br />
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label11" runat="server" Text=""></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:Button runat="server" CssClass="btn btn-info" ID="btnViewEntries" Text="View Entries" ValidationGroup="search" OnClick="btnViewEntries_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="padding-top: 20px;">
                            
                            <div class="control-group">
                                <label for="inputStyle" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                    <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                <div class="controls controls-row" style="margin-left: 140px">
                                    <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>

                             <div class="control-group">
                                <label for="inputColor" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                    <asp:Label ID="lblColors" runat="server" Text="Select Color:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                <div class="controls controls-row" style="margin-left: 140px">
                                    <asp:DropDownList ID="ddlColors" runat="server" Display="Dynamic" Width="90%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid" runat="server" visible="false" id="divSummary">
        <div class="span12">
            <div class="widget" id="dataDiv" runat="server" visible="false">
                <div class="widget-body" style="overflow-x: auto">
                    <div class="control-group">
                        <label for="inputBuyer" class="control-label">
                            <asp:Label ID="lblHeaderMessage" runat="server" Text="Wash Production Info:" Font-Bold="true"></asp:Label>
                            <%--<asp:Label Style="float: right" Text="" runat="server" ID="lblAvailableBalance"></asp:Label>--%></label>
                        <div class="controls controls-row">
                            <div id="dt_example" class="example_alt_pagination">
                                <div class="control-group">
                                    <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                        <asp:Repeater ID="rptSummary" runat="server" OnItemDataBound="rptSummary_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table-2" class="table table-bordered table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th style="min-width: 120px">
                                                                <asp:Label ID="lblColor" runat="server" Text="Wash Production Date"></asp:Label></th>
                                                            <th style="min-width: 100px">
                                                                <asp:Label ID="Label3" runat="server" Text="Buyer Name"></asp:Label></th>
                                                            <th style="min-width: 150px">
                                                                <asp:Label ID="lblStyleName" runat="server" Text="Style Name"></asp:Label></th>
                                                            <th style="min-width: 140px">
                                                                <asp:Label ID="Label8" runat="server" Text="Finishing Unit Name"></asp:Label></th>
                                                            <th style="min-width: 140px">
                                                                <asp:Label ID="Label12" runat="server" Text="Linking Floor"></asp:Label></th>
                                                            <th style="min-width: 160px">
                                                                <asp:Label ID="Label15" runat="server" Text="Color Name"></asp:Label></th>
                                                            <th style="min-width: 150px">
                                                                <asp:Label ID="lblTrimmingMendingQnty" runat="server" Text="Wash Quantity"></asp:Label>
                                                            </th>
                                                            <th style="min-width: 160px">
                                                                <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label></th>
                                                            <th style="min-width: 120px">
                                                                <asp:Label ID="Label10" runat="server" Text="Actions"></asp:Label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblWashProductionDate"><%# string.Format("{0:dd-MMM-yyyy}",Eval("WashProductionDate")) %></asp:Label>
                                                        <asp:TextBox Visible="false" runat="server" ID="rptWashProductionDate" TextMode="Date" value='<%# string.Format("{0:yyyy-MM-dd}",Eval("WashProductionDate")) %>'></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblBuyer"><%#Eval("BuyerName") %></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblStyles"><%#Eval("StyleName") %></asp:Label>
                                                        <asp:DropDownList ID="rptStyles" runat="server" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="rptStyle_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </td>
                                                   
                                                    <td>
                                                        <asp:Label runat="server" ID="lblFinishingUnit"><%#Eval("UnitName") %></asp:Label>
                                                        <asp:DropDownList ID="rptFinishingUnit" runat="server" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblLinkingFloor"><%#Eval("FloorName") %></asp:Label>
                                                        <asp:DropDownList ID="rptLinkingFloor" runat="server" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblColors"><%#Eval("ColorDescription") %></asp:Label>
                                                        <asp:DropDownList ID="rptColors" runat="server" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </td>
                                                    
                                                   
                                                    <td>
                                                        <asp:Label runat="server" ID="DeliveredQty"><%#Eval("Quantity") %></asp:Label>
                                                        <asp:TextBox Visible="false" TextMode="Number" min="0" runat="server" ID="UpdatedDeliveredQty" value='<%#Eval("Quantity")%>'></asp:TextBox>
                                                    </td>
                                                     <td>
                                                        <asp:Label runat="server" ID="Remarks"><%#Eval("Remarks") %></asp:Label>
                                                        <asp:TextBox Visible="false" runat="server" ID="UpdatedRemarks" value='<%#Eval("Remarks")%>'></asp:TextBox>
                                                    </td>
                                                    <%--<td>
                                                        <asp:Label runat="server" ID="lblRemarks"><%#Eval("Remarks") %></asp:Label>
                                                        <asp:DropDownList ID="rptRemarks" runat="server" AutoPostBack="true" Visible="false" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </td>--%>

                                                    <td>
                                                       
                                                        <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#Eval("CanDelete")%>' CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkbtnEdit" Visible='<%# Eval("CanEdit")%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' OnCommand="lnkbtnEdit_Command" class="btn btn-success btn-mini hidden-phone" Text="Edit"></asp:LinkButton>
                                                        
                                                        <asp:LinkButton ID="lnkbtnUpdated" Visible="false" runat="server" CommandName="Updated" CommandArgument='<%#Eval("Id") %>' OnCommand="lnkbtnUpdated_Command" class="btn btn-warning btn-mini hidden-phone" Text="Update"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkbtnCancel" Visible="false" runat="server" CommandName="Updated" CommandArgument='<%#Eval("Id") %>' OnCommand="lnkbtnCancel_Command" class="btn btn-warning btn-mini hidden-phone" Text="Cancel"></asp:LinkButton>
                                                       
                                                    </td>
                                                </tr>
                                                <asp:PlaceHolder Visible="false" runat="server" ID="trAvailableBalance">
                                                    <tr>
                                                        <td colspan="10">
                                                           <%-- <asp:Label Style="text-align:center; font-weight:bold; color:forestgreen" Text="Available Balance" runat="server" ID="rptAvailableBalance"></asp:Label>--%>
                                                        </td>
                                                    </tr>                                                    
                                                </asp:PlaceHolder>                                                
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                                                </table>
                                                                    <div class="clearfix"></div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <asp:Label ID="lblGrandTotal" runat="server" Text="" Font-Bold="true"></asp:Label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Label ID="lblEntryNotFound" runat="server" Text="No data found." Visible="false" BackColor="#ffff00"></asp:Label>

    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
