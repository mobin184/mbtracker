﻿<%@ Page Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="DeliverKnittedPartsToFinishingUnitV2.aspx.cs" Inherits="MBTracker.Pages.Productions.DeliverKnittedPartsToFinishingUnitV2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table th {
            text-align: center;
        }

        .table th, .table td {
            padding: 5px;
            text-align: center;
            vertical-align: top;
            border-top: 1px solid #e0e0e0;
        }

        .available-balance {
            width: 100%;
            float: left;
            font-size: 10px;
            font-weight: 600;
            color: green;
        }
    </style>
    <script type="text/javascript">
        function Validate(e) {
            var isDataEntered = false;
            var mcSelected = true;
            var ggSelected = true;
            var required = false;

            if ($('#<%=tbxDelivryDate.ClientID %>').val() == "") {
                $('#<%=tbxDelivryDate.ClientID %>').focus();
                $('#<%=tbxDelivryDate.ClientID %>')[0].setCustomValidity('This field is required');
                $('#<%=tbxDelivryDate.ClientID %>')[0].reportValidity();
                return false;
            }
            else {
                $('#<%=tbxDelivryDate.ClientID %>')[0].setCustomValidity('');
            }

            if ($('#<%=ddlUserProductionUnit.ClientID %>').val() == "") {
                $('#<%=ddlUserProductionUnit.ClientID %>').focus();
                $('#<%=ddlUserProductionUnit.ClientID %>')[0].setCustomValidity('This field is required');
                $('#<%=ddlUserProductionUnit.ClientID %>')[0].reportValidity();
                return false;
            }
            else {
                $('#<%=ddlUserProductionUnit.ClientID %>')[0].setCustomValidity('');
            }

            if ($('#<%=ddlBuyers.ClientID %>').val() == "") {
                $('#<%=ddlBuyers.ClientID %>').focus();
                $('#<%=ddlBuyers.ClientID %>')[0].setCustomValidity('This field is required');
                $('#<%=ddlBuyers.ClientID %>')[0].reportValidity();
                return false;
            }
            else {
                $('#<%=ddlBuyers.ClientID %>')[0].setCustomValidity('');
            }

            if ($('#<%=ddlStyles.ClientID %>').val() == "") {
                $('#<%=ddlStyles.ClientID %>').focus();
                $('#<%=ddlStyles.ClientID %>')[0].setCustomValidity('This field is required');
                $('#<%=ddlStyles.ClientID %>')[0].reportValidity();
                return false;
            }
            else {
                $('#<%=ddlStyles.ClientID %>')[0].setCustomValidity('');
            }

            if ($('#<%=ddlFinishingUnit.ClientID %>').val() == "") {
                $('#<%=ddlFinishingUnit.ClientID %>').focus();
                $('#<%=ddlFinishingUnit.ClientID %>')[0].setCustomValidity('This field is required');
                $('#<%=ddlFinishingUnit.ClientID %>')[0].reportValidity();
                return false;
            }
            else {
                $('#<%=ddlFinishingUnit.ClientID %>')[0].setCustomValidity('');
            }

            if ($('#<%=tbxChalanNumber.ClientID %>').val() == "") {
                $('#<%=tbxChalanNumber.ClientID %>').focus();
                $('#<%=tbxChalanNumber.ClientID %>')[0].setCustomValidity('This field is required');
                $('#<%=tbxChalanNumber.ClientID %>')[0].reportValidity();
                return false;
            }
            else {
                $('#<%=tbxChalanNumber.ClientID %>')[0].setCustomValidity('');
            }
            //var isValid = true;
            $(".sizeQty").each(function () {
                //console.log($(this).closest("tr"));
                var ddlMCBrand = $(this).parents("table:first").parents("tr:first").find(".ddlMCBrand");
                var ddlGG = $(this).parents("table:first").parents("tr:first").find(".ddlGG");
                var maxValue = parseInt($(this).attr("max"));
                //console.log(maxValue);
                //console.log(e);
                var txt = $(this).val();
                //console.log(txt);
                if (txt != "" && txt != 0 && maxValue < txt) {
                    $(this).focus();
                    $(this)[0].setCustomValidity('Value must be smaller than or equal to ' + maxValue);
                    $(this)[0].reportValidity();
                    required = true;
                    return false;
                }
                else {
                    $(this)[0].setCustomValidity('');
                    if (txt > 0) {
                        isDataEntered = true;
                        if (ddlMCBrand.val() == "") {
                            mcSelected = false;
                        }
                        if (ddlGG.val() == "") {
                            ggSelected = false;
                        }
                    }

                }
            });


            if (!isDataEntered && !required) {
                toastr.error('Please enter size quantity.');
                return isDataEntered;
            }
            else if (!mcSelected) {
                toastr.error('Please select machine brand.');
                return mcSelected;
            }
            else if (!ggSelected) {
                toastr.error('Please select garmetns gauge.');
                return ggSelected;
            }
            return true;
        }
    </script>
    <div class="row-fluid">
        <div class="span9">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="Deliver Knitted Parts:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row" style="padding-left: 5px; padding-right: 5px;">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                            <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label2" runat="server" Text="Delivery Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:TextBox ID="tbxDelivryDate" AutoPostBack="true" TextMode="Date" Width="100%" Text="" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxDelivryDate"><span style="font-weight: 700; color: #CC0000">Please select receive date.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label1" runat="server" Text="Knitting Unit:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlUserProductionUnit" runat="server" Display="Dynamic" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlUserProductionUnit_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlUserProductionUnit"><span style="font-weight: 700; color: #CC0000">Please select a knitting unit.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                        <%--<asp:TextBox ID="tbxSearchCriteriaForStyle" runat="server" onkeyup="FilterItems(this.value)" Width="29%" PlaceHoder="Type to Search style(s)"></asp:TextBox>--%>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select a style.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label3" runat="server" Text="Finishing Unit:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:DropDownList ID="ddlFinishingUnit" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlFinishingUnit"><span style="font-weight: 700; color: #CC0000">Please select a finishing unit.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label4" runat="server" Text="Chalan No:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:TextBox ID="tbxChalanNumber" runat="server" placeholder="Enter chalan number" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxChalanNumber"><span style="font-weight: 700; color: #CC0000">Please enter chalan number.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label6" runat="server" Text="Gate Pass No:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:TextBox ID="tbxGatePassNo" runat="server" placeholder="Enter gate pass number" CssClass="form-control"></asp:TextBox>
                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxGatePassNo"><span style="font-weight: 700; color: #CC0000">Enter gate pass no.</span></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label" style="float: left; width: 140px; padding-top: 5px; text-align: right">
                                        <asp:Label ID="Label5" runat="server" Text="Vehicle No:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;&nbsp;&nbsp;</span></label>
                                    <div class="controls controls-row" style="margin-left: 140px">
                                        <asp:TextBox ID="tbxVehicleNo" runat="server" placeholder="Enter vehicle number" CssClass="form-control"></asp:TextBox>
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="tbxVehicleNo"><span style="font-weight: 700; color: #CC0000">Enter vehicle no.</span></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>




    <div class="row-fluid">
        <asp:Label ID="lblNoDataFound" runat="server" Text="There is no color for this style." Visible="false" BackColor="#ffff00"></asp:Label>

    </div>

    <div class="span6" style="text-align: center">
        <asp:Label ID="lblSaveMessage" runat="server" Text="Data was saved successfully." Visible="false" ForeColor="#006666"></asp:Label>
    </div>

    <div class="row-fluid" runat="server" id="divColors" visible="false">
        <div class="span12">
            <div class="widget">
                <div class="widget-body" style="overflow-x: auto;">
                    <div class="row-fluid">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblEntry" runat="server" Text="" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                    <asp:Repeater ID="rptEntryInfo" OnItemDataBound="rptEntryInfo_ItemDataBound" runat="server">
                                        <HeaderTemplate>
                                            <table id="entryTable" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr class="GridViewScrollHeader">
                                                        <%--  <th>
                                                            <asp:Label ID="lblBuyer" runat="server" Text="Buyer<br/> Names"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblStyle" runat="server" Text="Styles<br/> Names"></asp:Label></th>--%>

                                                        <th style="text-align: center">
                                                            <asp:Label ID="lblColor" runat="server" Text="Color Names"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblStyle" runat="server" Text="Machine Brand"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label7" runat="server" Text="GG"></asp:Label></th>
                                                        <th style="text-align: center">
                                                            <asp:Label ID="lblSizeAndQuantity" runat="server" Text="Size & Quantity"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblColorTotal" runat="server" Text="Color Total"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr class="GridViewScrollItem">
                                                <td style="text-align: center"><%#Eval("ColorDescription") %>

                                                    <asp:Label ID="lblBuyerColorId" runat="server" Text='<%#Eval("BuyerColorId") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblColorAvailableBalance" CssClass="available-balance" Style="margin-top: 7px" runat="server"></asp:Label></td>
                                                <asp:Label ID="lblStyleId" runat="server" Text='<%#Eval("StyleId") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblBuyerId" runat="server" Text='<%#Eval("BuyerId") %>' Visible="false"></asp:Label>
                                                <td>
                                                    <asp:DropDownList ID="ddlMachineBrand" runat="server" AutoPostBack="true" Width="100%" OnSelectedIndexChanged="ddlMachineBrand_SelectedIndexChanged" CssClass="form-control ddlMCBrand"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlGG" runat="server" Width="100%" Style="min-width: 50px" CssClass="form-control ddlGG"></asp:DropDownList>
                                                </td>
                                                <td>
                                                  <%--  <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                                        <ContentTemplate>--%>
                                                            <asp:GridView ID="gvSizeQuantity1" EnableViewState="true" RowStyle-Height="30" Style="margin-left: 5px" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity1_RowCreated">
                                                            </asp:GridView>
                                                       <%-- </ContentTemplate>
                                                    </asp:UpdatePanel>--%>
                                                    <asp:Label ID="lblNoSizeFound" runat="server" Text="No size found." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>

                                                </td>
                                                <td>
                                                    <asp:Label ID="lblColorTotalValue" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                </table>
                            <div class="clearfix"></div>
                                        </FooterTemplate>
                                    </asp:Repeater>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <label for="inputOrder" class="control-label" style="text-align: right; padding-top: 0px">
                <asp:Label ID="lblStyleTotal" runat="server" Text="" Font-Bold="true"></asp:Label>
                <br />
                <asp:LinkButton ID="lnkbtnCalculateStyleTotal" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="Calculate Total" OnClick="lnkbtnCalculateColorTotal_Click"></asp:LinkButton>
            </label>

            <asp:Button ID="btnSaveEntries" runat="server" ValidationGroup="save" class="btn btn-success btn-medium pull-left hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClientClick="return Validate(event); return false;" UseSubmitBehavior="true" OnClick="btnSaveEntries_Click"></asp:Button>&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnUpdateEntries" Visible="false" ValidationGroup="save" runat="server" class="btn btn-success btn-medium pull-left hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClientClick="return Validate(event); return false;" UseSubmitBehavior="true" OnClick="btnUpdateEntries_Click"></asp:Button>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>



