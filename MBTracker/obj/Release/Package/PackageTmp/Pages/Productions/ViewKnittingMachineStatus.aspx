﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewKnittingMachineStatus.aspx.cs" Inherits="MBTracker.Pages.Productions.ViewKnittingMachineStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table th, .table td {
            padding: 10px;
            line-height: 20px;
            text-align: center;
            vertical-align: top;
            border-top: 1px solid #e0e0e0;
        }

        .LineHeight20 {
            line-height: 20px !important;
        }
    </style>
    <div class="row-fluid">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblProductionEntry" Text="View Knitting Machine Status:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row-fluid" runat="server" visible="true" id="FilterDiv">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                            <%--<span style="font-weight: 700; padding-right: 400px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field--%>
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="span12">
                            <div class="col-md-12" style="padding-left: 0px;">
                                <div class="col-md-7" style="padding-left: 0px;">
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">
                                                <asp:Label ID="lblFilterDate" runat="server" Text="Select Knitting Date:"></asp:Label>
                                                <span style="font-weight: 700; color: #CC0000">*</span>
                                            </label>
                                            <div class="controls controls-row" style="margin-left: 150px; width: 200px">
                                                <asp:TextBox ID="tbxFilterDate" runat="server" placeholder="Enter date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxFilterDate"><span style="font-weight: 700; color: #CC0000">Please select a knitting date.</span></asp:RequiredFieldValidator>

                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <br />
                                            <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">
                                                <asp:Label ID="Label10" runat="server" Text=""></asp:Label>
                                            </label>
                                            <div class="controls controls-row" style="margin-left: 150px; width: 200px">
                                                <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="View Status" CssClass="btn btn-info pull-left" OnClick="btnSearch_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>



            </div>
        </div>
    </div>

    <div class="row-fluid" runat="server" id="dataDiv">
        <div class="span12" style="">
            <div class="widget" id="dataDisplayRepeater" runat="server" visible="false">
                <div class="widget-body" style="overflow-x: auto">
                    <div id="dt_example" class="example_alt_pagination">
                        <label for="inputOrder" class="control-label">
                            <asp:Label ID="lblSummary" runat="server" Visible="false" Text="Knitting machine status:" Font-Bold="true"></asp:Label></label>
                        <asp:Repeater ID="rpt" runat="server" OnItemDataBound="rpt_ItemDataBound">
                            <HeaderTemplate>
                                <table id="data-table1" class="table table-bordered table-hover">
                                    <tr>
                                        <th>
                                            <asp:Label ID="Label1" runat="server" Text="Production <br/>Date"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label2" runat="server" Text="Knitting <br/>Unit"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label3" runat="server" Text="Production <br/>Shift"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label4" runat="server" Text="No of Running <br/>Machine"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label5" runat="server" Text="User for <br/>Sample"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label14" runat="server" Text="User for <br/>Size Set"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label6" runat="server" Text="Long Time <br/>Out of Order"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label7" runat="server" Text="Temporary <br/> Out of Order"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label8" runat="server" Text="Idle <br/>Machine"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label11" runat="server" Text="Machine Used<br/>For Setup"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label9" runat="server" Text="Used For <br/>Other Works"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label13" runat="server" Text="Total MCs <br/> for Unit"></asp:Label></th>
                                        <th>
                                            <asp:Label ID="Label16" runat="server" Text="Take <br/>Actions"></asp:Label></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%#String.Format("{0:dd-MMM-yyyy}",Eval("ProductionDate")) %> </td>
                                    <td><%#Eval("UnitName") %></td>
                                    <td><%#Eval("ShiftName") %></td>
                                    <td><%#Eval("totalNoOfRunningMachine") %></td>
                                    <td><%#Eval("totalUsedForSample") %></td>
                                    <td><%#Eval("totalUsedForSizeSet") %></td>
                                    <td><%#Eval("totalLongTimeStop") %></td>
                                    <td><%#Eval("totalTemporaryStop") %></td>
                                    <td><%#Eval("totalIdleMachine") %></td>
                                    <td><%#Eval("totalUsedForSetup") %></td>
                                    <td><%#Eval("totalUsedForOtherWorks") %></td>
                                    <td><%#Eval("unitTotal") %></td>
                                    <td>
                                        <%--<asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("KnittingUnitId")+","+ Eval("ProductionDate")+","+Eval("ShiftId")%>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnEdit" Visible='<%# ViewState["editEnabled"]%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("KnittingUnitId")+","+ Eval("ProductionDate")+","+Eval("ShiftId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>--%>

                                         <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#Eval("CanDelete")%>' CommandArgument='<%#Eval("KnittingUnitId")+","+ Eval("ProductionDate")+","+Eval("ShiftId")%>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnEdit" Visible='<%# Eval("CanEdit")%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("KnittingUnitId")+","+ Eval("ProductionDate")+","+Eval("ShiftId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="View" CommandArgument='<%#Eval("KnittingUnitId")+","+ Eval("ProductionDate")+","+Eval("ShiftId")+","+Eval("UnitName")+","+Eval("ShiftName")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnView_Command" Text="View Details"></asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>

                                <td style="font-size:large;font-weight:500">Total:</td>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:Label ID="lblTotalRunningMCs" runat="server" Text="2500"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblTotalUsedForSample" runat="server" Text="2500"></asp:Label></td>
                                 <td>
                                    <asp:Label ID="lblTotalUsedForSizeSet" runat="server" Text="2500"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblLongTimeStop" runat="server" Text="2500"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblTempStop" runat="server" Text="2500"></asp:Label></td>
                                 <td>
                                    <asp:Label ID="lblIdleMachines" runat="server" Text="2500"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblUsedForSetUp" runat="server" Text="2500"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblUsedForOtherWorks" runat="server" Text="2500"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblTotalMachinesForUnits" runat="server" Text="2500"></asp:Label></td>
                               
                                <td></td>

                                </tbody>
                                 </table>
                                    <div class="clearfix">
                            </FooterTemplate>
                        </asp:Repeater>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>


    <div class="row-fluid" runat="server" visible="false" id="pnlDetails">
        <div class="span12" style="">
            <div class="widget">
                <div class="widget-body">

                    <div id="dt_example" class="example_alt_pagination">
                        <div class="control-group">
                            <label for="inputOrder" class="control-label LineHeight20">
                                <asp:Label ID="lblKnittingMachineStatusView" runat="server" Visible="false" Text="Detailed Information:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:Repeater ID="rptEntryInfo" runat="server" OnItemDataBound="rptEntryInfo_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lblMachineBrand" runat="server" Text="Machine <br/> Brand"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblRunningMachine" runat="server" Text="Number Of<br/>Running Machine"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblUsedForSample" runat="server" Text="Used For<br/> Sample"></asp:Label></th>
                                                      <th>
                                                        <asp:Label ID="lblUsedForSizeSet" runat="server" Text="Used For<br/> Size Set"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblLongTimeStoped" runat="server" Text="Long Time<br/> Out of Order"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblTemporaryStoped" runat="server" Text="Temporary<br/> Out of Order"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblIdleMachine1" runat="server" Text="Idle<br/> Machine"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label12" runat="server" Text="Machine Used<br/>For Setup"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblUsedForOtherWork" runat="server" Text="Used For<br/> Other Works"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblPurposeOfOtherWorks" runat="server" Text="Purpose of <br/> Other Works"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblTotalMC" runat="server" Text="Total <br/> Machines"></asp:Label></th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("MachineBrand") %>
                                                <asp:Label ID="lblDailyKnittingMachineStatusId" Text="" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblMachineBrandId" runat="server" Text='<%#Eval("MachineBrandId") %>' Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblNoOfRunningMachine" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblUsedForSample" runat="server"></asp:Label>
                                            </td>
                                             <td>
                                                <asp:Label ID="lblUsedForSizeSet" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblLongTimeStoped" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTemporaryStoped" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblIdleMachine" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMachineUsedForSetup" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblUsedForOtherWorks" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPurposeOfOtherWorksValue" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTotalMachines" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                 </table>
                                    <div class="clearfix">
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Label ID="lblNoDataFoundForView" runat="server" Text="No machine brand found in this unit." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
