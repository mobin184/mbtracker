﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewDeliveredLinkedItemsForFinishing.aspx.cs" Inherits="MBTracker.Pages.Productions.ViewDeliveredLinkedItemsForFinishing" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table th {
            text-align: center;
        }
    </style>
    <div class="row-fluid">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="View Delivered Linked Items:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row" style="padding-left: 15px; padding-right: 15px;">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                            <%--<span style="font-weight: 700; padding-right: 400px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field--%>
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="search" />
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label1" runat="server" Text="Select Delivered Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxDeliveryDate" AutoPostBack="true" TextMode="Date" Width="100%" Text="" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="search"
                                            ControlToValidate="tbxDeliveryDate"><span style="font-weight: 700; color: #CC0000">Please select date delivered.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <br />
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label11" runat="server" Text=""></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:Button runat="server" CssClass="btn btn-info" ID="btnViewEntries" Text="View Entries" ValidationGroup="search" OnClick="btnViewEntries_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row-fluid">
        <asp:Label ID="lblEntryNotFound" runat="server" Text="knitting delivery entries not found!"  Visible="false" BackColor="#ffff00"></asp:Label>
        <div class="span12" runat="server" visible="false" id="divSummary">
            <div class="widget">
                <div class="widget-body">
                    <div class="control-group">
                        <label for="inputBuyer" class="control-label">
                            <asp:Label ID="Label2" runat="server" Text="Knitting Delivery Entries:" Font-Bold="true"></asp:Label></label>
                        <div class="controls controls-row">
                            <div id="dt_example" class="example_alt_pagination">
                                <div class="control-group">
                                    <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                        <asp:Repeater ID="rptSummary" runat="server">
                                            <HeaderTemplate>
                                                <table id="data-table" class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <%--<th>
                                                                <asp:Label ID="lbl56" runat="server" Text="Finishing<br/> Unit"></asp:Label></th>--%>
                                                            <th>
                                                                <asp:Label ID="Label4" runat="server" Text="Finishing<br/> Unit Floor"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="Label3" runat="server" Text="Buyer<br/> Name"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblStyle" runat="server" Text="Styles<br/> Name"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblColor" runat="server" Text="Delivery<br/> Date"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="Label9" runat="server" Text="Delivery <br/> Quantity"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="Label6" runat="server" Text="Entry Date <br/> Time"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="Label7" runat="server" Text="Received <br/> Status"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="Label10" runat="server" Text="Take <br/> Actions"></asp:Label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%#Eval("FloorName") %></td>
                                                    <td><%#Eval("BuyerName") %></td>
                                                    <td><%#Eval("StyleName") %></td>
                                                    <td><%# string.Format("{0:dd-MMM-yyyy}",Eval("DeliveryDate")) %></td>
                                                    <td><%#Eval("DeliveryQty") %></td>
                                                    <td><%# string.Format("{0:dd-MMM-yyyy hh:mm tt}",Eval("EntryDate")) %></td>
                                                    <td><%#Eval("IsReceived").ToString() == "1" ? "<span class='label label-success'>&nbsp;&nbsp;&nbsp;Received&nbsp;&nbsp;&nbsp;</span>": "<span class='label label-danger'>Not Received</span>" %></td>
                                                    <td>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" Visible='<%#Eval("IsReceived").ToString() == "1" && Convert.ToBoolean(ViewState["editEnabled"])==true ? true: false %>' disabled class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkbtnEdit" runat="server" Visible='<%#Eval("IsReceived").ToString() == "0" && Convert.ToBoolean(ViewState["editEnabled"])==true ? true: false %>' CommandName="Edit" CommandArgument='<%#Eval("Id") %>' OnCommand="lnkbtnEdit_Command" class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="View" CommandArgument='<%#Eval("Id")+","+Eval("IsReceived")%>' OnCommand="lnkbtnView_Command" class="btn btn-success btn-small hidden-phone" Text="View Details"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                </table>
                            <div class="clearfix"></div>
                                            </FooterTemplate>
                                        </asp:Repeater>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row-fluid" runat="server" id="divColors" visible="false">
        <div class="span12">
            <div class="widget">
                <div class="widget-body">
                    <div class="row-fluid">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblEntry" runat="server" Text="" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                    <asp:Repeater ID="rptEntryInfo" OnItemDataBound="rptEntryInfo_ItemDataBound" runat="server">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblBuyer" runat="server" Text="Buyer Name"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblStyle" runat="server" Text="Style Name"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblColor" runat="server" Text="Color Name"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Size & Received Quantity"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%#Eval("BuyerName") %></td>
                                                <td><%#Eval("StyleName") %></td>
                                                <td><%#Eval("ColorDescription") %>
                                                    <asp:Label ID="lblBuyerColorId" runat="server" Text='<%#Eval("BuyerColorId") %>' Visible="false"></asp:Label></td>
                                                <asp:Label ID="lblStyleId" runat="server" Text='<%#Eval("StyleId") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblBuyerId" runat="server" Text='<%#Eval("BuyerId") %>' Visible="false"></asp:Label>
                                                <td>
                                                    <asp:GridView ID="gvSizeQuantity1" RowStyle-Height="30" CssClass="GridViewClass" OnRowCreated="gvSizeQuantity1_RowCreated" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px">
                                                    </asp:GridView>
                                                    <asp:Label ID="lblNoSizeFound" runat="server" Text="No size found." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                </table>
                            <div class="clearfix"></div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <asp:Label ID="lblNoDataFound" runat="server" Text="There is no color for this style." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-2 pull-right">
                <asp:Button runat="server" CssClass="btn btn-info" ID="btnConfirm" Text="Confirm Delivery" OnClick="btnConfirm_Click" />
            </div>
        </div>
    </div>



</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
