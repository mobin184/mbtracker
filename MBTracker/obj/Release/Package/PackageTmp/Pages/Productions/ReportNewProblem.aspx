﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ReportNewProblem.aspx.cs" Inherits="MBTracker.Pages.Productions.ReportNewProblem" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

        <asp:UpdatePanel ID="ConSheetUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>


    <style>
        .table th {
            text-align: center;
        }
    </style>
    <div class="row-fluid">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="Report a New Problem:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row" style="padding-left: 15px; padding-right: 15px;">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                        </div>
                        <div class="control-group">
                        </div>
                        <div class="col-md-9">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label">
                                        <asp:Label ID="Label2" runat="server" Text="When/Who Reporting:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlReportedStage" runat="server" Display="Dynamic" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlReportedStage_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlReportedStage"><span style="font-weight: 700; color: #CC0000">Please select a stage.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="row-fluid" runat="server" id="div1" visible="true">
        <div class="span6">
            <div class="widget">
                <div class="widget-body">

                    <div class="row" style="padding-left: 15px; padding-right: 15px;">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                        </div>
                        <div class="control-group">
                             <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="bodyLinking" />
                        </div>
                        <div class="col-md-12">
                            <div class="form-horizontal">

                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" Display="Dynamic" Width="65%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="bodyLinking"
                                            ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputStyle" class="control-label">
                                        <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlStyles" runat="server" Display="Dynamic" Width="65%" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="bodyLinking"
                                            ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select a style.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="" class="control-label">
                                        <asp:Label ID="Label4" runat="server" Text="Problem Description:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxProblemDescription" TextMode="MultiLine" Style="min-height: 170px" Width="100%" ClientIDMode="Static" runat="server" placeholder="Enter problem description" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            
                                <div class="control-group">
                                <label for="inputBuyer" class="control-label" style="line-height:52px">
                                    <asp:Label ID="Label10" runat="server" Text="Upload files (if any):" CssClass="text-left"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Label ID="Label18" runat="server" Text="&nbsp;"></asp:Label><br />

                                    <asp:FileUpload ID="FileUpload1" onchange="javascript:updateFileList()" runat="server" AllowMultiple="true" />

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="FileUpload1"><span style="font-weight: 700; color: #CC0000">Select file(s) to upload.</span></asp:RequiredFieldValidator>
                                    <br />
                                    <asp:Label ID="lblShowFileNames" runat="server" Text="" Width="600px"></asp:Label>
                                </div>
                            </div>
                            
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-horizontal">                                
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



    <div class="row-fluid" runat="server" id="buttonsDiv" visible="true">
        <div class="span6">
            <asp:Button ID="btnSaveEntries" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSaveEntries_Click"></asp:Button>
            <asp:Button ID="btnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdateEntries_Click"></asp:Button>
            <br />
        </div>
    </div>
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSaveEntries" />
            <asp:PostBackTrigger ControlID="btnUpdateEntries" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
    <script>
        updateFileList = function () {
            debugger

            var input = document.getElementById('cpMain_FileUpload1');
            var output = document.getElementById('cpMain_lblShowFileNames');

            output.innerHTML = '<br />Selected Files:<br />';
            output.innerHTML += '<ul>';

            for (var i = 0; i < input.files.length; ++i) {
                output.innerHTML += '<li>' + input.files.item(i).name + '</li>';
            }
            output.innerHTML += '</ul>';

        }
    </script>
</asp:Content>
