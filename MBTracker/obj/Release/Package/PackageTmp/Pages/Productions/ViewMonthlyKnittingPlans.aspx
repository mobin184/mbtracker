﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="ViewMonthlyKnittingPlans.aspx.cs" Inherits="MBTracker.Pages.Productions.ViewMonthlyKnittingPlans" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table th, .table td {
            padding: 4px;
            line-height: 10px;
            text-align: center;
            vertical-align: top;
            border-top: 1px solid #e0e0e0;
        }
    </style>

    <div class="row-fluid">
        <div class="span8">
            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="View Monthly Knitting Plan:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row-fluid" runat="server">
                        <div class="form-horizontal">
                            <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px;">
                                <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field
                            </div>
                            <div class="control-group">
                                <div class="col-md-12 col-sm-12">
                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="save" />
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblBuyer" runat="server" Text="Select Year:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control" Style="min-width: 200px; margin-top: 5px;" Display="Dynamic" Width="100%"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlYear"><span style="font-weight: 700; color: #CC0000">Please select a year.</span></asp:RequiredFieldValidator>


                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblStyle" runat="server" Text="Select Month:"></asp:Label><span style="font-weight: 700; color: #CC0000">*&nbsp;</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlMonth" runat="server" CssClass="form-control" Style="min-width: 200px; margin-top: 10px;" Display="Dynamic" Width="100%"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                            ControlToValidate="ddlMonth"><span style="font-weight: 700; color: #CC0000">Please select a month.</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label3" runat="server" Text="Select Buyer (optional):"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label4" runat="server" Text="Select Style (optional):"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlStyles" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="display: flex; justify-content: center; padding-top: 20px;">
                                <br />
                                <asp:Button ID="btnSubmit" runat="server" ValidationGroup="save" class="btn btn-info pull-right" Text="View Entries" OnClick="btnSearch_Click" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <%--<div class="widget-body">
                    <div class="row-fluid" runat="server" visible="true" id="FilterDiv">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                            
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="col-md-10" style="padding-left: 0px">

                            <div class="col-md-9" style="padding-left: 0px">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">
                                            <asp:Label ID="lblFilterDate" runat="server" Text="Select Year:"></asp:Label>
                                            <span style="font-weight: 700; color: #CC0000">*</span>
                                        </label>
                                        <div class="controls controls-row" style="margin-left: 150px">
                                           
                                        </div>
                                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left; padding-top: 10px">
                                            <asp:Label ID="Label2" runat="server" Text="Select Month:"></asp:Label>
                                            <span style="font-weight: 700; color: #CC0000">*</span>
                                        </label>
                                        <div class="controls controls-row" style="margin-left: 150px">
                                           
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">
                                            <asp:Label ID="Label8" runat="server" Text=""></asp:Label></label>
                                        <div class="controls controls-row" style="margin-left: 150px">
                                            <br />
                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="View Knitting Plans" CssClass="btn btn-info pull-left" OnClick="btnSearch_Click" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>

                    </div>
                </div>--%>
            </div>
        </div>
    </div>


<%--    <div class="row-fluid" runat="server">
        <div class="span6">
            <div class="widget" id="dataDiv" runat="server" visible="false">
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <div class="control-group">
                            <label for="inputOrder" class="control-label">
                                <asp:Label runat="server" ID="lblLabelMessage" Text="Knitting Plans:" Font-Bold="true" Visible="false"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:Repeater ID="rptUnits" runat="server">
                                    <HeaderTemplate>
                                        <table id="data-table1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>

                                                    <th>
                                                        <asp:Label ID="Label22" runat="server" Text="Month Name"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label7" runat="server" Text="Year"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblDailyUnitTotal" runat="server" Text="Total"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblActions" runat="server" Text="Actions"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("MonthName") %></td>
                                            <td><%#Eval("Year") %></td>
                                            <td><%#Eval("Target") %></td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#Eval("CanDelete")%>' CommandArgument='<%#Eval("Month")+","+ Eval("Year")%>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkbtnEdit" Visible='<%# Eval("CanEdit")%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("Month")+","+ Eval("Year")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="View" CommandArgument='<%#Eval("Month")+","+ Eval("Year")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnView_Command" Text="View Details"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                                         </table>
                                                            <div class="clearfix">
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>--%>

    <div class="row-fluid" runat="server" id="pnlDetails" visible="false">
        <div class="span12">

            <div class="widget">
                <div class="widget-body">

                    <div id="dt_example" class="example_alt_pagination">
                        <div class="control-group">
                            <label for="inputOrder" class="control-label">
                                <asp:Label ID="lblDetails" runat="server" Visible="false" Text="Detailed Information:" Font-Bold="true"></asp:Label></label>
                            <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                <asp:Repeater ID="rptDetails" runat="server" OnItemDataBound="rptDetails_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>

                                                    <th>
                                                        <asp:Label ID="Label7" runat="server" Text="Buyer"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label8" runat="server" Text="Style"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label10" runat="server" Text="Machine Brand"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label22" runat="server" Text="Unit Name"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label9" runat="server" Text="Month"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label1" runat="server" Text="GG"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label11" runat="server" Text="Sample Timing"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label12" runat="server" Text="Yarn ETA"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label13" runat="server" Text="PP Appr. Date"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label14" runat="server" Text="TOD"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label15" runat="server" Text="Total"></asp:Label></th>
                                                     <th>
                                                        <asp:Label ID="lblActions" runat="server" Text="Actions"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("BuyerName") %></td>
                                            <td><%#Eval("StyleName") %></td>
                                            <td><%#Eval("MachineBrand") %></td>
                                            <td><%#Eval("UnitName") %></td>
                                            <td><%#Eval("MonthName") %></td>
                                            <td><%#Eval("KnittingMCGauge") %></td>
                                            <td><%#Eval("KnittingTime") %></td>
                                            <td><%#Eval("YarnETAFormated") %></td>
                                            <td><%#Eval("PPApprDateFormated") %></td>
                                            <td><%#Eval("TODFormated") %></td>
                                            <td><%#Eval("Target") %></td>
                                             <td>
                                                <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#Eval("CanDelete")%>' CommandArgument='<%#Eval("KnittingPlanId")%>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkbtnEdit" Visible='<%# Eval("CanEdit")%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("KnittingPlanId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                                         </table>
                                                            <div class="clearfix">
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Label ID="lblGrandTotal" style="float:right; margin-right:100px" runat="server" Text="" Font-Bold="true"></asp:Label>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    </div>
    <asp:Label ID="lblNoDetails" runat="server" Text="No data found." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>


</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
