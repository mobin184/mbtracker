﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="AddFinishingPlan.aspx.cs" Inherits="MBTracker.Pages.Productions.AddFinishingPlan" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 3px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }
    </style>



    <div class="row-fluid">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblMachinePlanning" Text="Add a Finishing Plan:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 15px">
                            <span style="font-weight: 700; padding-right: 400px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputStyle" class="control-label">
                                <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <%--<div class="control-group">
                            <label for="inputOrder" class="control-label">
                                <asp:Label ID="lblOrder" runat="server" Text="Select Order:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlOrders_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlOrders"><span style="font-weight: 700; color: #CC0000">Please select an order.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span9">
            <asp:Label ID="lblNoPlanningInfo" runat="server" Visible="false" Text="<br/><br/>No shipment info found." BackColor="#ffff00"></asp:Label>
            <asp:Panel ID="pnlAssignMachinesToOrder" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="Label2" runat="server" Text="Add a New Plan:" Font-Bold="true"></asp:Label></label>

                                <div class="controls controls-row">
                                    <asp:Repeater ID="rptIssueQuantityAndMachinesPerUnit" OnItemDataBound="rptIssueQuantityAndMachinesPerUnit_ItemDataBound" runat="server">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblProductionUnitLabel" runat="server" Text="Finishing<br/> Units"></asp:Label></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center; vertical-align: middle"><asp:Label runat="server" ID="lblUnitName" Text='<%#Eval("UnitName") %>'></asp:Label><asp:Label ID="lblUnitId" runat="server" Visible="false" Text='<%#Eval("Id") %>'></asp:Label></td>
                                                <td>
                                                    <asp:Repeater ID="rptUnitMachines" runat="server" OnItemDataBound="rptUnitMachines_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="data-table" class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <asp:Label ID="lblProductionUnitLabel" runat="server" Text="Linking <br/>MC Gauges"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="lblTotalMachinesLabel" runat="server" Text="Issue<br/> Quantity"></asp:Label></th>
                                                                         <th>
                                                                            <asp:Label ID="Label5" runat="server" Text="Linking PCs<br/> Per Day"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="Label6" runat="server" Text="Number of<br/> Machines"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="Label7" runat="server" Text="Start<br/> Date"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="Label8" runat="server" Text="End<br/> Date"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="Label9" runat="server" Text="Production<br/> Quantity"></asp:Label></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td style="text-align: center; vertical-align: middle">
                                                                    <%#Eval("GaugeNumber") %>
                                                                    <asp:Label ID="lblMCGaugeId" runat="server" Visible="false" Text='<%#Eval("Id") %>'></asp:Label>                                                                   
                                                                </td>
                                                                <td style="text-align: center; vertical-align: middle">
                                                                    <asp:TextBox ID="tbxIssueQty" runat="server" TextMode="Number" placeholder="" Width="60px"></asp:TextBox></td>
                                                                <td style="text-align: center; vertical-align: middle">
                                                                    <asp:TextBox ID="tbxLinkPerDay" runat="server" TextMode="Number" placeholder="" AutoPostBack="true" OnTextChanged="tbxNumMC_TextChanged" Width="60px"></asp:TextBox></td>
                                                                <td style="text-align: center; vertical-align: middle">
                                                                    <asp:TextBox ID="tbxNumMC" runat="server" placeholder="" TextMode="Number" Width="60px" AutoPostBack="true" OnTextChanged="tbxNumMC_TextChanged"></asp:TextBox></td>
                                                                <td style="text-align: center; vertical-align: middle">
                                                                    <asp:TextBox ID="tbxFromDate" runat="server" placeholder="Start date" TextMode="Date" Width="150px" AutoPostBack="true" OnTextChanged="tbxFromDate_TextChanged"></asp:TextBox></td>
                                                                <td style="text-align: center; vertical-align: middle">
                                                                    <asp:TextBox ID="tbxToDate" runat="server" placeholder="End date" TextMode="Date" Width="150px" AutoPostBack="true" OnTextChanged="tbxToDate_TextChanged"></asp:TextBox></td>
                                                                <td style="text-align: center; vertical-align: middle">
                                                                    <asp:Label ID="lblProductionQty" runat="server" Text="" Width="80px"></asp:Label></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody>
                                        </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Label ID="lblTotalProductions" runat="server" Text="Total Production: 0 Pcs" Font-Bold="true"></asp:Label>
                                    <asp:Button ID="btnUpdateMachinesForOrder" runat="server" Visible="false" class="btn btn-success btn-small pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdateMachinesForOrder_Click" />
                                    <asp:Button ID="btnSaveMachinesForOrder" runat="server" class="btn btn-success btn-small pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="btnSaveMachinesForOrder_Click" />
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="Label4" runat="server" Text=""></asp:Label></label>
                                <div class="controls controls-row">
                                </div>
                            </div>


                            <div style="text-align: left; font-size: 12px; line-height: 30px; padding-bottom: 5px">
                                <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            </div>

                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="Label3" runat="server" Text="View Machine Availability :" BackColor="#ffff00" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlMonthYears" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control" OnSelectedIndexChanged="ddlMonthYears_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlMonthYears"><span style="font-weight: 700; color: #CC0000">Please select production month.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>


                        </div>

                    </div>

                </div>

            </asp:Panel>
        </div>
        <div class="span3">
            <asp:Panel ID="pnlPlanningInfo" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-body">
                        <div class="control-group">
                            <div style="width: 100%" id="divTotalOrderQuantity" runat="server" visible="false">
                            </div>
                            <br />
                            
                        </div>
                        <asp:Panel ID="pnlShipmentInfo" runat="server" Visible="false">

                            <div class="control-group" style="line-height: 10px">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="lblShipmentDates" runat="server" Text="Shipment Summary:" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rptShipmentSummary" runat="server" OnItemDataBound="rptShipmentSummary_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblCol1" runat="server" Text="Shipment<br/>Dates"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblCol3" runat="server" Text="Shipment<br/> Quantity"></asp:Label></th>
                                                        <th class="hidden">
                                                            <asp:Label ID="lblCol4" runat="server" Text="Machines<br/> Required"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center; vertical-align: middle">
                                                    <asp:Label ID="lblShipmentDates" runat="server" Text=' <%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "ShipmentDate"))%>'> </asp:Label></td>
                                                <td style="text-align: center; vertical-align: middle">
                                                    <asp:Label ID="lblShipmentQuantity" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShipmentQuantity")%>'></asp:Label>
                                                </td>
                                                <td class="hidden" style="text-align: center; vertical-align: middle">
                                                    <asp:Label ID="lblMCRequired" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                            </table>
                                            <div class="clearfix">
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <asp:Label ID="lblNoShippingInfo" runat="server" Visible="false" Text="<br/><br/>No shipping info found." BackColor="#ffff00"></asp:Label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span9">
            <asp:Panel ID="pnlMachinesAvailable" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="Label10" runat="server" Text=""></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rptMonthlyDates" runat="server" OnItemDataBound="rptMonthlyDates_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblCol1" runat="server" Text="Dates"></asp:Label></th>
                                                        <th style="text-align: center">
                                                            <asp:Label ID="lblAvailableMachineMessage" runat="server" Text="Availability of Machines"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center; vertical-align: central">

                                                    <asp:Label ID="lblProducitonDate" runat="server" Text='<%#Eval("Date") %>'></asp:Label></th>
                                                </td>
                                                <td>
                                                    <div class="row-fluid">
                                                        <div class="widget-body" style="border: none">
                                                            <div class="form-horizontal no-margin">
                                                                <asp:Repeater ID="rptMachinesAvailable" OnItemDataBound="rptMachinesAvailable_ItemDataBound" runat="server">
                                                                    <HeaderTemplate>
                                                                        <table id="data-table" class="table table-bordered table-hover">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>
                                                                                        <asp:Label ID="lblProductionUnitLabel" runat="server" Text="Finishing <br/> Units"></asp:Label></th>
                                                                                    <th></th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td style="text-align: center" width="13%">
                                                                                <%#Eval("UnitName") %><asp:Label ID="lblUnitId" runat="server" Visible="false" Text='<%#Eval("Id") %>'></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Repeater ID="rptUnitMachinesAvailable" runat="server">
                                                                                    <HeaderTemplate>
                                                                                        <table id="data-table" class="table table-bordered table-hover">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>
                                                                                                        <asp:Label ID="lbl111" runat="server" Text="Machine <br/> Gauge"></asp:Label></th>

                                                                                                    <th>
                                                                                                        <asp:Label ID="lblTotalMachinesLabel" runat="server" Text="Total <br/> Machines"></asp:Label></th>
                                                                                                    <th>
                                                                                                        <asp:Label ID="lblAlreadyBookedMachinesLabel" runat="server" Text="Booked By <br/>  All Styles"></asp:Label></th>
                                                                                                    <th>
                                                                                                        <asp:Label ID="lblOrderBookedMachineLabel" runat="server" Text="Booked By <br/>  This Style"></asp:Label></th>
                                                                                                    <th>
                                                                                                        <asp:Label ID="lblAvailableMachinesLabel" runat="server" Text="Machines <br/> Available"></asp:Label></th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td style="text-align: center" width="13%"><%#Eval("GaugeNumber") %><asp:Label ID="lblMCGaugeId" runat="server" Visible="false" Text='<%#Eval("Id") %>'></asp:Label></td>

                                                                                            <td style="text-align: center" width="12%"><%#Eval("TotalMachines") %></td>
                                                                                            <td style="text-align: center" width="12%">
                                                                                                <asp:Label ID="lblAlreadyBookedMachines" runat="server" Text='<%#Eval("AlreadyBooked") %>'></asp:Label></td>
                                                                                            <td style="text-align: center" width="12%">
                                                                                                <asp:Label ID="lblBookedMachinesForOrder" runat="server" Text='<%#Eval("BookedMachinesForOrder") %>'></asp:Label></td>
                                                                                            <td style="text-align: center" width="12%">
                                                                                                <asp:Label ID="lblAvailableMachines" runat="server" Text='<%#Eval("MachinesAvailable") %>'></asp:Label></td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </tbody>
                                                                </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </tbody>
                                                                </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>

                                            </tr>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                         </table>
                            <div class="clearfix">
                                        </FooterTemplate>
                                    </asp:Repeater>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
    <div class="clearfix"></div>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
