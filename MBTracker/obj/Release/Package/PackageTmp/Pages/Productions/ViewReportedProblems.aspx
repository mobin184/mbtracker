﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewReportedProblems.aspx.cs" Inherits="MBTracker.Pages.Productions.ViewReportedProblems" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <div class="row-fluid" id="divBuyerAndStyle" runat="server">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblMachinePlanning" Text="Reported Problems:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="search" />
                            </div>
                        </div>
                        <div style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 15px">
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="search"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputStyle" class="control-label">
                                <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyles" runat="server" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="search"
                                    ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <br />
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label11" runat="server" Text=""></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:Button runat="server" CssClass="btn btn-info" ID="btnViewEntries" Text="View Entries" ValidationGroup="search" OnClick="btnViewEntries_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

          <label for="inputStyle" class="control-label" style="font-weight:bold; padding-top:100px; margin-left:200px; text-align:left">
        <asp:Label ID="lblNotAuthorizedToView" Font-Bold="true" Visible="false" Font-Size="Large" runat="server" Text="You are not authorized to view this information."></asp:Label>
     </label>


    <div class="row-fluid">
        <div class="span12">
            <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="<br/><br/>No data found." BackColor="#ffff00"></asp:Label>
            <asp:Panel ID="pnlDetails" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-body">
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="Reported Problems:" Font-Bold="true"></asp:Label></label>
                            <div class="controls controls-row">
                                <div id="dt_example" class="example_alt_pagination">
                                    <div class="control-group">
                                        <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                            <asp:Repeater ID="rptReportedProblems" runat="server" OnItemDataBound="rptReportedProblems_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="data-table" class="table table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                 <th>
                                                                    <asp:Label ID="Label7" runat="server" Text="Entry Date"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label4" runat="server" Text="When Reported"></asp:Label></th>
                                                                <%--<th>
                                                                    <asp:Label ID="Label3" runat="server" Text="Finishing Floor"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="lblStyle" runat="server" Text="Size Name"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="lblColor" runat="server" Text="LOT Number"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label9" runat="server" Text="TC Number"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label1" runat="server" Text="MC Number"></asp:Label></th>
                                                                <th>
                                                                    <asp:Label ID="Label5" runat="server" Text="Knit ID"></asp:Label></th>--%>
                                                                <th>
                                                                    <asp:Label ID="Label6" runat="server" Text="Problem Description"></asp:Label></th>
                                                               <th>
                                                                    <asp:Label ID="lblFiles" runat="server" Text="Problem Related File(s)"></asp:Label></th>
                                                               <%-- <th>
                                                                    <asp:Label ID="Label10" runat="server" Text="Action"></asp:Label></th>--%>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="width:10%"; text-align: left><%# string.Format("{0:dd-MMM-yyyy hh:mm tt}",Eval("CreateDate")) %><asp:Label ID="lblReportedProblemId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "Id")%>'></asp:Label></td>
                                                        <td style="width:10%"><%#Eval("StageName")%></td>
                                                       <%-- <td><%#Eval("FloorName")%></td>
                                                        <td><%#Eval("SizeName")%></td>
                                                        <td><%#Eval("LOTNumber")%></td>
                                                        <td><%#Eval("TCNumber")%></td>
                                                        <td><%#Eval("MCNumber")%></td>
                                                        <td><%#Eval("KnitId")%></td>--%>
                                                        <td style="width:40%; text-align: left"><%#Eval("ProblemDescription")%></td>
                                                       <td style="width:40%">
                                                <asp:Repeater ID="rptFiles" runat="server">
                                                    <HeaderTemplate>
                                                        <table id="data-table" class="table table-bordered table-hover" style="margin-bottom: 0px">
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="lblFileName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FileName")%>' Width="75%"></asp:Label></td>
                                                            <td style="text-align: center">
                                                                <asp:HyperLink runat="server" NavigateUrl='<%# Eval("Id", "../Buyers_and_Orders/GetFile.aspx?ID={0}&Type=ReportedProblem") %>' Text="Download"></asp:HyperLink></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                                            </table>
                                                            <div class="clearfix">
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                        </td>
                                                        <%--<td style="width:10%">
                                                            <asp:LinkButton ID="lnkbtnEdit" Visible='<%# ViewState["editEnabled"]%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' OnCommand="lnkbtnEdit_Command" class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton1" runat="server" Visible='<%#Convert.ToBoolean(ViewState["editEnabled"])==true ? false: true %>' disabled class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>
                                                            
                                                            <%--<asp:LinkButton ID="lnkbtnView" runat="server" CommandName="View" CommandArgument='<%#Eval("TransactionId")%>' OnCommand="lnkbtnView_Command" class="btn btn-success btn-small hidden-phone" Text="View Details"></asp:LinkButton>
                                                        </td>--%>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </tbody>
                                            </table>
                                                </FooterTemplate>
                                            </asp:Repeater>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
