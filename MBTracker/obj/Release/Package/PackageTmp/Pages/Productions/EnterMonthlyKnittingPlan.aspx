﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="EnterMonthlyKnittingPlan.aspx.cs" Inherits="MBTracker.Pages.Productions.EnterMonthlyKnittingPlan" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table th, .table td {
            padding: 4px;
            line-height: 10px;
            text-align: center;
            vertical-align: top;
            border-top: 1px solid #e0e0e0;
        }
    </style>

    <div class="row-fluid" runat="server">

        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblProductionEntry" Text="Enter Monthly Knitting Plans:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">
                                <asp:Label ID="lblFilterDate" runat="server" Text="Select Month:"></asp:Label>
                                <span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row" style="margin-left: 150px; width: 200px">
                                <asp:TextBox ID="tbxMonth" runat="server" placeholder="Enter date" Width="100%" TextMode="Month" CssClass="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxMonth"><span style="font-weight: 700; color: #CC0000">Please select a knitting date.</span></asp:RequiredFieldValidator>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>



    <div class="row-fluid">
        <div class="span12">

            <asp:Panel ID="pnlKnittingEntry" runat="server">
                <div class="widget" style="border: 0px;">
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label pull-right" style="font-size:11px;">
                                    <asp:Label ID="Label3" runat="server" Text="All fields are required except yarn ETA, PP Appr. Date, TOD. Row with missing information will not be saved." Font-Bold="true"></asp:Label></label>
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblKnittingEntry" runat="server" Text="Enter Knitting Information:" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                    <asp:Repeater ID="rptEntryInfo" runat="server" OnItemDataBound="rptEntryInfo_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table2" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblBuyer" runat="server" Text="Buyer<br/> Names"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></th>
                                                        <th>
                                                            <asp:Label ID="lblStyle" runat="server" Text="Styles<br/> Names"></asp:Label></th>
                                                         <th>
                                                            <asp:Label ID="lblMCBrand" runat="server" Text="Machine<br/> Brand"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label8" runat="server" Text="Finishing</br> Unit"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblMCGauge" runat="server" Text="Garments<br/> Gauge"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label6" runat="server" Text="Sample<br/> Timing"></asp:Label></th>
                                                        <th style="vertical-align:middle">
                                                            <asp:Label ID="Label7" runat="server" Text="Yarn ETA"></asp:Label></th>
                                                        <th style="vertical-align:middle">
                                                            <asp:Label ID="Label4" runat="server" Text="PP Appr. Date"></asp:Label></th>
                                                        <th style="vertical-align:middle">
                                                            <asp:Label ID="Label1" runat="server" Text="TOD"></asp:Label></th>                                                        
                                                        <th style="vertical-align:middle">
                                                            <asp:Label ID="Label2" runat="server" Text="Target"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="ddlBuyers" Style="margin-top: 20px; min-width: 200px;" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    <%--<asp:Label runat="server" Width="75" Text='<%#Eval("BuyerName") %>'></asp:Label>--%>
                                                    <asp:Label ID="lblStyleId" runat="server" Text='<%#Eval("StyleId") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblBuyerId" runat="server" Text='<%#Eval("BuyerId") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblMachineBrandId" runat="server" Text='<%#Eval("MachineBrandId") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblFinishingUnitId" runat="server" Text='<%#Eval("FinishingUnitId") %>' Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlStyles" Style="margin-top: 20px; min-width: 200px;" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlMachineBrand" runat="server" CssClass="form-control" Style="min-width: 200px; margin-top: 20px;" Display="Dynamic" Width="100%"></asp:DropDownList>

                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlFinishingUnit" runat="server" CssClass="form-control" Style="min-width: 200px; margin-top: 20px;" Display="Dynamic" Width="100%"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="tbxKnittingMCGauge" Style="" Width="60" min="1" TextMode="Number" runat="server" Text='<%#Eval("KnittingMachineGauge") %>'></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="tbxKnittingTime" Style="" min="1" TextMode="Number" Text='<%#Eval("KnittingTime") %>' runat="server" Width="60px"></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="tbxYarnETA" Style="" TextMode="Date" runat="server" Text='<%#Eval("ETA") %>' Width="130px"></asp:TextBox></td>
                                                </td>
                                                 <td>
                                                    <asp:TextBox ID="tbxPPApprDate" Style="" TextMode="Date" Text='<%#Eval("PPApprDate") %>' runat="server" Width="130px"></asp:TextBox></td>
                                                </td>
                                                 <td>
                                                    <asp:TextBox ID="tbxTOD" Style="" TextMode="Date" Text='<%#Eval("TOD") %>' runat="server" Width="130px"></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="tbxTarget" Style="" min="1" TextMode="Number" Text='<%#Eval("Target") %>' runat="server" Width="100px"></asp:TextBox></td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                 </table>
                                            <br />
                                            <div class="pull-right" style="margin-right: 0px">
                                                <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="81px" Text="Add Row" OnClick="btnAddRow_Click" />
                                            </div>
                                            <br />
                                            <br />

                                        </FooterTemplate>
                                    </asp:Repeater>


                                </div>

                            </div>
                            <br />

                        </div>

                    </div>
                </div>

                <label for="inputOrder" class="control-label" style="text-align: right; padding-top: 0px">
                    <asp:Label ID="lblShiftTotal" runat="server" Text="" Font-Bold="true"></asp:Label>
                </label>

                <%--<asp:LinkButton ID="lnkbtnCalculateSizeTotal" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="Calculate Total" OnClick="lnkbtnCalculateSizeTotal_Click"></asp:LinkButton>--%>

                <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-left hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
                <asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-left hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>

            </asp:Panel>
            <asp:Label ID="lblNoKnittingGoingOn" runat="server" Text="Production was not planned at this unit today." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>


        </div>
        <div class="clearfix"></div>
    </div>

</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
