﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="EnterKnittingEntryV2.aspx.cs" Inherits="MBTracker.Pages.Productions.EnterKnittingEntryV2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table th, .table td {
            padding: 4px;
            line-height: 10px;
            text-align: center;
            vertical-align: top;
            border-top: 1px solid #e0e0e0;
        }
        .available-balance {
            width: 100%;
            float: left;
            font-size: 10px;
            font-weight: 600;
            color: green;
        }
    </style>
    <script type="text/javascript">
        var retValue = false;

        function Validate(e) {
            var entered = false;
            var mcUsed = 0;
            $mcUsedTextBox = null;
            $(".sizeQty").each(function () {
                //console.log($(this).closest("tr"));
                $mcUsedTextBox = $(this).parents("table:first").parents("tr:first").find(".mcUsed");
                mcUsed = $mcUsedTextBox.val();
                console.log(mcUsed);
                //console.log(e);
                var txt = $(this).val();
                //console.log(txt);
                if (txt != '' && txt == 0) {
                    $(this).focus();
                    $(this)[0].setCustomValidity('Value must be greater than or equal to 1.');
                    $(this)[0].reportValidity();
                    return false;
                }
                else {
                    $(this)[0].setCustomValidity('');

                    if (txt != '' && txt > 0) {
                        entered = true;
                    }
                }
                
            });

            if (entered && mcUsed == 0) {
                $($mcUsedTextBox)[0].setCustomValidity('This field is required.');
                $($mcUsedTextBox)[0].reportValidity();
                return false;
            }
            else {
                $($mcUsedTextBox)[0].setCustomValidity('');
            }
            return true;
        }
    </script>
    <div class="row-fluid" runat="server">

        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblProductionEntry" Text="Enter Daily Knittings:"></asp:Label>
                        <%--<asp:validationsummary id="ErrorSummary" ValidationGroup="save" runat="server"/>--%>
                    </div>
                </div>
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <div class="control-group">
                            <label for="inputOrder" class="control-label">
                                <asp:Label ID="lblEntryInfo" runat="server" Text="" Font-Bold="true"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:Repeater ID="rptGeneralInfo" runat="server" OnItemDataBound="rptGeneralInfo_ItemDataBound" OnItemCreated="rptGeneralInfo_ItemCreated">
                                    <HeaderTemplate>
                                        <table id="data-table-1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lblProductionUnit" runat="server" Text="Production Unit"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblShift" runat="server" Text="Shift"></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblProductionDate" runat="server" Text="Production Date"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblUser" runat="server" Text="User Name"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlUserUnits" runat="server" Width="80%" AutoPostBack="true"></asp:DropDownList></td>
                                            <td>
                                                <asp:DropDownList ID="ddlShift" runat="server" Width="100"></asp:DropDownList>
                                                <asp:Label runat="server" ID="lblEntryDate" Visible="false" Text='<%#Eval("EntryDate")%>'></asp:Label>
                                            </td>
                                            <%--<td><%#Eval("ProductionEntryDate") %> </td>--%>
                                            <td>
                                                <asp:TextBox ID="tbxKnittingDate" AutoPostBack="true" OnTextChanged="tbxKnittingDate_TextChanged" TextMode="Date" Width="130px" Text="" runat="server"></asp:TextBox></td>
                                            <td><%#Eval("UserName") %> </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                 </table>
                                    <div class="clearfix">
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                            <asp:Label ID="lblNoUnitPermission" runat="server" Text="You don't have any production unit permission, Please contact with administration." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

    </div>



    <div class="row-fluid">
        <div class="span12">

            <asp:Panel ID="pnlKnittingEntry" runat="server">
                <div class="widget" style="border: 0px;">
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblKnittingEntry" runat="server" Text="Enter Knitting Information:" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                    <asp:Repeater ID="rptEntryInfo" runat="server" OnItemDataBound="rptEntryInfo_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table2" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblBuyer" runat="server" Text="Buyer<br/> Names"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblStyle" runat="server" Text="Styles<br/> Names"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblColor" runat="server" Text="Color<br/> Names"></asp:Label></th>
                                                        <%-- <th>
                                                            <asp:Label ID="lblTotalIssue" runat="server" Text="Yarn<br/> Issued"></asp:Label></th>--%>
                                                        <th>
                                                            <asp:Label ID="lblMCBrand" runat="server" Text="Machine<br/> Brand"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label4" runat="server" Text="Finishing<br/>Unit"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblMCGauge" runat="server" Text="Garments<br/> Gauge"></asp:Label></th>
                                                        <th class="hidden">
                                                            <asp:Label ID="Label1" runat="server" Text="Jacquard<br/> Portion"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label2" runat="server" Text="# of <br/> MC Used"></asp:Label></th>

                                                        <%-- <th>
                                                        <asp:Label ID="Label3" runat="server" Text="MC Used<br/> at Night"></asp:Label></th>--%>
                                                        <%--  <th>
                                                        <asp:Label ID="Label4" runat="server" Text="Day<br/> Production"></asp:Label></th>--%>
                                                        <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Knittings by Size <br/>&nbsp;"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label3" runat="server" Text="Total"></asp:Label></th>
                                                        <%--<th>
                                                        <asp:Label ID="Label6" runat="server" Text="Quantity<br/> Delivered"></asp:Label></th>--%>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="ddlBuyers" Style="margin-top: 20px; min-width: 200px;" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    <%--<asp:Label runat="server" Width="75" Text='<%#Eval("BuyerName") %>'></asp:Label>
                                                    <asp:Label ID="lblStyleId" runat="server" Text='<%#Eval("StyleId") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblBuyerId" runat="server" Text='<%#Eval("BuyerId") %>' Visible="false"></asp:Label>--%>
                                                </td>
                                                <td>
                                                    <%-- <asp:Label runat="server" Width="75" Text='<%#Eval("StyleName") %>'></asp:Label>--%>
                                                    <asp:DropDownList ID="ddlStyles" Style="margin-top: 20px; min-width: 200px;" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlColors" runat="server" CssClass="form-control" Style="min-width: 200px; margin-top: 20px;" AutoPostBack="true" OnSelectedIndexChanged="ddlColors_SelectedIndexChanged" Display="Dynamic" Width="100%"></asp:DropDownList>
                                                    <%-- <asp:Label ID="lblBuyerColorId" runat="server" Text='<%#Eval("BuyerColorId") %>' Visible="false"></asp:Label>--%>
                                                    <asp:Label ID="lblUnitId" runat="server" Text='<%#Eval("UnitId") %>' Visible="false"></asp:Label>
                                                </td>

                                                <td class="hidden">
                                                    <%-- <asp:GridView ID="gvSizeQuantity1" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity_RowCreated1">
                                                    </asp:GridView>--%>
                                                    
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlMachineBrand" runat="server" CssClass="form-control" Style="min-width: 200px; margin-top: 20px;" Display="Dynamic" Width="100%"></asp:DropDownList>
                                                    <asp:Label ID="lblMachineBrandId" runat="server" Text='<%#Eval("MachineBrandId") %>' Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlFinishingUnit" runat="server" CssClass="form-control" Style="min-width: 200px; margin-top: 20px;" Display="Dynamic" Width="100%"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="tbxKnittingMCGauge" Style="" Width="60" runat="server" Text='<%#Eval("KnittingMachineGauge") %>'></asp:TextBox>
                                                    <asp:Label ID="lblTarget" runat="server" Text='<%#Eval("Target") %>' Visible="false"></asp:Label></td>

                                                <td class="hidden">
                                                    <asp:DropDownList ID="ddlJacquardPortion" runat="server" Width="100px">
                                                        <asp:ListItem Text="Complete" Value="Complete"></asp:ListItem>
                                                        <asp:ListItem Text="No Work" Value="Nowork"></asp:ListItem>
                                                        <asp:ListItem Text="Technical" Value="Technical"></asp:ListItem>
                                                        <asp:ListItem Text="Sample" Value="Sample"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="tbxMCUsed" CssClass="mcUsed" Style="" min="1" TextMode="Number" runat="server" Width="60px"></asp:TextBox>
                                                     <asp:Label ID="lblMCUsed" runat="server" Text='<%#Eval("MachineBrandId") %>' Visible="false"></asp:Label>

                                                </td>
                                                <%--  <td>
                                                <asp:TextBox ID="tbxMCUsedAtNight" runat="server" Width="60px" Text='<%#Eval("NumberOfMachines") %>'></asp:TextBox></td>--%>

                                                <td>
                                                    <asp:GridView ID="gvSizeQuantity2" RowStyle-Height="30" Style="margin-left: 5px" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity_RowCreated2">
                                                    </asp:GridView>

                                                </td>

                                                <%--<td>
                                                <asp:GridView ID="gvSizeQuantity3" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity_RowCreated3">
                                                </asp:GridView>
                                            </td>--%>

                                                <%--<td>
                                                <asp:GridView ID="gvSizeQuantity4" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeQuantity_RowCreated4">
                                                </asp:GridView>
                                            </td>--%>

                                                <td>
                                                    <asp:Label ID="lblSizeTotal" runat="server"></asp:Label>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                 </table>
                                            <br />
                                            <div class="pull-right" style="margin-right: 0px">

                                                <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="81px" Text="Add Row" OnClick="btnAddRow_Click" />
                                            </div>
                                            <br />
                                            <br />

                                        </FooterTemplate>
                                    </asp:Repeater>
                                   <%-- <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Please select at least one record."
                                        ClientValidationFunction="Validate" ForeColor="Red"></asp:CustomValidator>--%>
                                    <br />

                                </div>

                            </div>
                            <br />

                        </div>

                    </div>
                </div>

                <label for="inputOrder" class="control-label" style="text-align: right; padding-top: 0px">
                    <asp:Label ID="lblShiftTotal" runat="server" Text="" Font-Bold="true"></asp:Label>
                </label>

                <asp:LinkButton ID="lnkbtnCalculateSizeTotal" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="Calculate Total" OnClick="lnkbtnCalculateSizeTotal_Click"></asp:LinkButton>

                <asp:Button ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-left hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClientClick="return Validate(event); return false;" OnClick="lnkbtnSaveEntries_Click"></asp:Button>
                <asp:Button ID="lnkbtnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-left hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClientClick="return Validate(event); return false;" OnClick="lnkbtnUpdateEntries_Click"></asp:Button>

            </asp:Panel>
            <asp:Label ID="lblNoKnittingGoingOn" runat="server" Text="Production was not planned at this unit today." Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>


        </div>
        <div class="clearfix"></div>
    </div>

</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
