﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewKnittingMachineSpareRequests.aspx.cs" Inherits="MBTracker.Pages.Knitting_Spares.ViewKnittingMachineSpareRequests" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid" id="divUnitSelection" runat="server">

        <div class="span6">

            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="View Knitting Spares Requests:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 5px">
                            <span style="font-weight: 700; padding-right: 450px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>

                        <div class="span7">

                            <div class="control-group">
                                <label for="inputBuyer" class="control-label" style="padding-top: 5px; padding-bottom: 10px">
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                    <asp:Label ID="lblKnittingUnitMessage" runat="server" Text="Knitting Unit:"></asp:Label></label>
                                <div class="controls controls-row" style="padding-top: 5px;">
                                    <asp:DropDownList ID="ddlKnittingUnits" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" CssClass="form-control" OnSelectedIndexChanged="ddlKnittingUnits_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="lblMachineBrand1" runat="server" Text="Machine Brand (Optional):"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlMachineBrands" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="inputBuyer" class="control-label">
                                    <%--  <span style="font-weight: 700; color: #CC0000">*</span>--%>
                                    <asp:Label ID="Label3" runat="server" Text="From Date (Default: 30 days):"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxFromDate" runat="server" placeholder="From date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox> 
                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="tbxFromDate"><span style="font-weight: 700; color: #CC0000">Please select From date.</span></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>

                        </div>


                        <div class="span4">

                            <div class="control-group">
                                <label for="inputBuyer" class="control-label" style="padding-top: 55px; padding-bottom: 10px">&nbsp;</label>

                                <div class="controls controls-row" style="padding-top: 55px;">
                                    <asp:Button ID="btnViewKnittingParts" runat="server" class="btn btn-success btn-midium pull-right btnStyle" Text="View Spare Requests" OnClick="btnViewKnittingParts_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>
        </div>

    </div>


    <div class="row-fluid">
        <div class="span12">
            <asp:Label ID="lblNoRequestFound" Style="width: 100%" runat="server" Visible="false" Text="No spare request found during this period." BackColor="#ffff00"></asp:Label>
            <asp:Panel ID="pnlViewKnittingSpareRequests" runat="server" Visible="false">

                <div class="widget">
                    <div class="widget-body">
                        <div class="form-horizontal">
                            <div class="control-group" style="overflow-x: auto">
                                <label for="inputStyle" class="control-label" style="font-weight: bold; text-align: left">
                                    <asp:Label ID="Label4" runat="server" Style="font-weight: bold; text-align: left" Font-Bold="false" Text=""></asp:Label></label>
                                <div class="controls-row">
                                    <asp:Repeater ID="rptKnittingSpareRequests" runat="server" OnItemCommand="rptKnittingSpareRequests_ItemCommand">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblRequestDate" runat="server" Text="Request Date"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblMachineBrand" runat="server" Text="Machine Brand"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblMachineName" runat="server" Text="Machine Name"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblSparesType" runat="server" Text="Spares Type"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblSpareName" runat="server" Text="Spare Name"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblRequestedQty" runat="server" Text="Quantity"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblReasonForRequest" runat="server" Text="Reason"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblActions" runat="server" Text="Actions"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>

                                                <td style="vertical-align: middle; text-align: left; width: 10%">
                                                    <asp:Label ID="lblDateRequestedValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DateRequested")%>'></asp:Label>
                                                </td>

                                                <td style="vertical-align: middle; text-align: center; width: 12%">
                                                    <asp:Label ID="lblMachineBrandId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "MachineBrandId")%>'></asp:Label>
                                                    <asp:Label ID="lblMachineBrandValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MachineBrandName")%>' Width="100%" ></asp:Label>
                                                    <asp:DropDownList ID="ddlMachineBrands" runat="server" CssClass="form-control" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlMachineBrands_SelectedIndexChanged" ></asp:DropDownList>
                                                </td>

                                                <td style="vertical-align: middle; text-align: center; width: 15%">
                                                    <asp:Label ID="lblMachineId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingMachineId")%>'></asp:Label>
                                                    <asp:Label ID="lblMachineNameValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingMachineName")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                    <asp:DropDownList ID="ddlKnittingMachines" runat="server" CssClass="form-control" Visible="false"></asp:DropDownList>
                                                </td>


                                                <td style="vertical-align: middle; text-align: left; width: 10%">
                                                    <asp:Label ID="lblSpareTypeId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingSpareTypeId")%>'></asp:Label>
                                                    <asp:Label ID="lblSpareTypeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PartTypeName")%>' Width="100%"></asp:Label>
                                                    <asp:DropDownList ID="ddlKnittingPartTypes" runat="server" CssClass="form-control" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlKnittingPartTypes_SelectedIndexChanged" Visible="false"></asp:DropDownList>
                                                </td>

                                                <td style="vertical-align: middle; text-align: left; width: 15%">
                                                    <asp:Label ID="lblSpareId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingSpareId")%>'></asp:Label>
                                                    <asp:Label ID="lblSpareNameValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingSpareName")%>' Width="100%"></asp:Label>
                                                    <asp:DropDownList ID="ddlSpares" runat="server" CssClass="form-control" Width="100%" AutoPostBack="true"  Visible="false"></asp:DropDownList>
                                                </td>

                                                <td style="vertical-align: middle; text-align: center; width: 5%">
                                                    <asp:Label ID="lblQuantity" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "QuantityRequested")%>' Width="100%" ></asp:Label>
                                                    <asp:TextBox ID="tbxQuantity" Width="100%" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "QuantityRequested")%>'></asp:TextBox>
                                                </td>

                                                <td style="vertical-align: middle; text-align: left; width: 8%">
                                                    <asp:Label ID="lblRequestReasonId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "ReasonForRequestId")%>'></asp:Label>
                                                    <asp:Label ID="lblRequestReason" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ReasonName")%>' Width="100%"></asp:Label>
                                                    <asp:DropDownList ID="ddlRequestReasons" runat="server" CssClass="form-control" Width="100%" AutoPostBack="true"  Visible="false"></asp:DropDownList>
                                                </td>
                                                <td style="vertical-align: middle; text-align: center; width: 15%">
                                                    <asp:Label ID="lblRemarksValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Remarks")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                    <asp:TextBox ID="tbxRemarks" Width="100%" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "Remarks")%>' TextMode="MultiLine"></asp:TextBox>
                                                </td>

                                                <td style="vertical-align: middle; text-align: center; width: 10%">
                                                    <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="&nbsp;&nbsp;Edit&nbsp;&nbsp;"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnUpdate" runat="server" Visible="false" CommandName="Update" CommandArgument='<%#Eval("Id") %>' class="btn btn-warning2 btn-mini" Text="Update"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnCancel" runat="server" Visible="false" CommandName="Cancel" CommandArgument='<%#Eval("Id") %>' class="btn btn-warning2 btn-mini" Text="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;&nbsp;"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </asp:Panel>


        </div>
    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
