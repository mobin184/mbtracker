﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewKnittingMachineNeedleUpdates.aspx.cs" Inherits="MBTracker.Pages.Knitting_Spares.ViewKnittingMachineNeedleUpdates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid" id="divBuyerAndStyle" runat="server">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="View Needle Updates:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <span style="font-weight: 700; color: #CC0000">*</span>
                                <asp:Label ID="Label1" runat="server" Text="Select Knitting Unit:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlKnittingUnits" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" CssClass="form-control" OnSelectedIndexChanged="ddlKnittingUnits_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <span style="font-weight: 700; color: #CC0000">*</span>
                                <asp:Label ID="lblMachineBrand" runat="server" Text="Select Machine Brand:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlMachineBrands" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" CssClass="form-control" OnSelectedIndexChanged="ddlMachineBrands_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <span style="font-weight: 700; color: #CC0000">*</span>
                                <asp:Label ID="Label3" runat="server" Text="Select Machine:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlKnittingMachines" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" CssClass="form-control" OnSelectedIndexChanged="ddlKnittingMachines_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </div>


                        <%--                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="&nbsp;"></asp:Label></label>
                            <div class="controls controls-row">
                                <br />
                                <asp:Button ID="btnViewKnittingMachines" runat="server" class="btn btn-success btn-midium pull-left btnStyle" Text="Show Machines" OnClick="btnViewKnittingMachines_Click" />
                            </div>
                        </div>--%>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="row-fluid">
        <div class="span12">
            <asp:Label ID="lblNoNeedleUpdateFound" Style="width: 100%" runat="server" Visible="false" Text="There was no needle update for the selected machine." BackColor="#ffff00"></asp:Label>
            <asp:Panel ID="pnlViewNeedleUpdates" runat="server" Visible="false">

                <div class="widget">
                    <div class="widget-body">
                        <div class="form-horizontal">
                            <div class="control-group" style="overflow-x: auto">
                                <label for="inputStyle" class="control-label" style="font-weight: bold; text-align: left">
                                    <asp:Label ID="Label2" runat="server" Style="font-weight: bold; text-align: left" Font-Bold="false" Text=""></asp:Label></label>
                                <div class="controls-row">
                                    <asp:Repeater ID="rptNeedleUpdates" runat="server">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblDateAndTime" runat="server" Text="Date & Time<br/>"></asp:Label></th>
                                                           <th>
                                                            <asp:Label ID="lblNumNeedleBeforeChange" runat="server" Text="Number Before"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblNumNeedleChanged" runat="server" Text="Number Changed"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblNeedleBrand" runat="server" Text="Needle Brand"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblNumNeedleAfterChange" runat="server" Text="Number After"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblCauseOfChange" runat="server" Text="Change Reason"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblOtherMachine" runat="server" Text="Other Machine"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblRemarks" runat="server" Text="Remarks<br/>"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblChangedBy" runat="server" Text="Changed By"></asp:Label></th>
                                                       <%-- <th>
                                                            <asp:Label ID="lblAction" runat="server" Text="Action"></asp:Label></th>--%>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>

                                                <td style="vertical-align: middle; text-align: left; width: 10%">
                                                    <asp:Label ID="lblDateAndTimeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DateAndTime")%>'></asp:Label>
                                                </td>
                                                <td style="vertical-align: middle; text-align: center; width: 10%">
                                                    <asp:Label ID="lblNumNeedleBeforeChangeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NumNeedlesBefore")%>'></asp:Label>
                                                </td>
                                                <td style="vertical-align: middle; text-align: center; width: 10%">
                                                    <asp:Label ID="lblNeedleChangedValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NumNeedlesChanged")%>'></asp:Label>
                                                </td>

                                                <td style="vertical-align: middle; text-align: center; width: 10%">
                                                    <asp:Label ID="lblBrandValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NeedleBrand")%>'></asp:Label>
                                                </td>

                                                <td style="vertical-align: middle; text-align: center; width: 10%">
                                                    <asp:Label ID="lblNumNeedleAfterChangeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NumNeedlesAfter")%>'></asp:Label>
                                                </td>
                                                <td style="vertical-align: middle; text-align: left; width: 10%">
                                                    <asp:Label ID="lblCauseOfChangeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "WhyChanged")%>'></asp:Label>
                                                </td>
                                                <td style="vertical-align: middle; text-align: left; width: 10%">
                                                    <asp:Label ID="lblOtherMachineValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OtherMachineName")%>'></asp:Label>
                                                </td>
                                                <td style="vertical-align: middle; text-align: left; width: 15%">
                                                    <asp:Label ID="lblRemarksValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Remarks")%>'></asp:Label>
                                                </td>
                                                <td style="vertical-align: middle; text-align: center; width: 10%">
                                                    <asp:Label ID="lblChangedBy" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NeedleChangedBy")%>'></asp:Label>
                                                </td>
                                                <%--<td style="vertical-align: middle; text-align: center; width: 15%">
                                                    <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnUpdate" runat="server" Visible="false" CommandName="Update" CommandArgument='<%#Eval("Id") %>' class="btn btn-warning2 btn-mini" Text="Update"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnCancel" runat="server" Visible="false" CommandName="Cancel" CommandArgument='<%#Eval("Id") %>' class="btn btn-warning2 btn-mini" Text="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;&nbsp;"></asp:LinkButton>
                                                </td>--%>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </asp:Panel>


        </div>
    </div>




</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
