﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewUnitWiseMachine.aspx.cs" Inherits="MBTracker.Pages.Knitting_Spares.ViewUnitWiseMachine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid" id="divBuyerAndStyle" runat="server">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="View Knitting Machines:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="form-horizontal">

                        <div style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 15px">
                            <div class="pull-right" style="padding-bottom: 10px">
                                <span style="font-weight: 700;"></span><span style="font-weight: 700; color: #CC0000">*</span>Selections are optional.
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputMachineBrands" class="control-label">
                                <asp:Label ID="lblMachineBrand" runat="server" Text="Select Machine Brand:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlMachineBrands" runat="server" Width="60%" DataValueField="Id" DataTextField="MachineBrand" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label1" runat="server" Text="Select Knitting Unit:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlKnittingUnits" runat="server" Width="60%" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="&nbsp;"></asp:Label></label>
                            <div class="controls controls-row">
                                <br />
                                <asp:Button ID="btnViewKnittingMachines" runat="server" class="btn btn-success btn-midium pull-left btnStyle" Text="View Production Machines" OnClick="btnViewKnittingMachines_Click" />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span9">
            <asp:Label ID="lblNoKnittingMachineFound" Style="width: 100%" runat="server" Visible="false" Text="Knitting machine was not found." BackColor="#ffff00"></asp:Label>
            <asp:Panel ID="pnlViewKnittingMachines" runat="server" Visible="false">

                <div class="widget">
                    <div class="widget-body">
                        <div class="form-horizontal">
                            <div class="control-group" style="overflow-x: auto">
                                <label for="inputStyle" class="control-label" style="font-weight: bold; text-align: left">
                                    <asp:Label ID="Label3" runat="server" Style="font-weight: bold; text-align: left" Font-Bold="false" Text="Knitting Machines:"></asp:Label></label>
                                <div class="controls-row">
                                    <asp:Repeater ID="rptKnittingMachines" runat="server" OnItemCommand="rptKnittingMachines_ItemCommand">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblMachineBrand" runat="server" Text="Machine Brand"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblKnittingUnit" runat="server" Text="Production Unit"></asp:Label></th>
                                                        
                                                        <th>
                                                            <asp:Label ID="lblNeedleCapacity" runat="server" Text="Number of Machine"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="LabelAction" runat="server" Text="Action"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="vertical-align: middle; width: 15%">
                                                    <asp:Label ID="lblMachineBrandValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MachineBrandName")%>' Width="100%" CssClass="text-left"></asp:Label></td>
                                                <td style="vertical-align: middle; width: 15%">
                                                    <asp:Label ID="lblKnittingUnitId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingUnitId")%>'></asp:Label>
                                                    <asp:Label ID="lblKnittingUnitValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KnittingUnit")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                    <asp:DropDownList ID="ddlKnittingUnits" runat="server" CssClass="form-control" Style="min-width: 125px" Visible="false"></asp:DropDownList>
                                                </td>
                                                
                                                <td style="vertical-align: middle; width: 10%">
                                                    <asp:Label ID="lblNeedleCapacity" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NumberOfMachines")%>' Width="100%" CssClass="text-right"></asp:Label>
                                                   <%--<asp:TextBox ID="TextBox1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NumberOfMachines")%>' Width="100%" CssClass="text-right"></asp:TextBox></td>--%>
                                              <asp:TextBox ID="tbxNumberOfMachines" Width="100%" runat="server" TextMode="Number" Visible="false"></asp:TextBox> 
                                                    </td>
                                                    
                                                
                                                <td style="vertical-align: middle; width: 20%">
                                                    <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="&nbsp;&nbsp;&nbsp;Edit&nbsp;&nbsp;&nbsp;"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnUpdate" runat="server" Visible="false" CommandName="Update" CommandArgument='<%#Eval("Id") %>' class="btn btn-warning2 btn-mini" Text="Update"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnCancel" runat="server" Visible="false" CommandName="Cancel" CommandArgument='<%#Eval("Id") %>' class="btn btn-warning2 btn-mini" Text="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;&nbsp;"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </asp:Panel>


        </div>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>

