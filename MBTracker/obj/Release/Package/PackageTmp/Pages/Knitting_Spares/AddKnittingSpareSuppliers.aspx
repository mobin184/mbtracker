﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddKnittingSpareSuppliers.aspx.cs" Inherits="MBTracker.Pages.Knitting_Parts.AddKnittingSpareSuppliers" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    
    <div class="row-fluid">
        <div class="span9">

            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblProductionEntry" Text="Add Knitting Spare Suppliers:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="row-fluid" runat="server">

                        <div style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 15px">
                            <div class="pull-right" style="padding-bottom: 10px">
                                <span style="font-weight: 700;"></span><span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            </div>
                        </div>

                        <div id="dt_example" class="example_alt_pagination">

                            <div class="control-group">
                                <label for="inputOrder" class="control-label" style="text-align:left">
                                    <asp:Label ID="lblKnittingEntry" runat="server" Text="Enter Supplier's information below." Font-Bold="true"></asp:Label><span style="font-weight: 700; color: #CC0000">Data will not be saved if supplier's name is empty.</span></label>
                                <div class="controls controls-row" style="width: 100%; overflow-x: auto">
                                    <asp:Repeater ID="rptKnittingMachineSuppliersEntryInfo" runat="server" OnItemDataBound="rptKnittingMachineSuppliersEntryInfo_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>

                                                        <th>
                                                            <asp:Label ID="lblKnittingSpareSupplierName" runat="server" Text="Name"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblKnittingSapreSupplierAddress" runat="server" Text="Address"></asp:Label></th>
                                                         <th>
                                                            <asp:Label ID="lblSupplierPhone" runat="server" Text="Phone Number"></asp:Label></th>
                                                         <th>
                                                            <asp:Label ID="lblEmail" runat="server" Text="Email Address"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblKnittingSpareSupplierAdditionalInfo" runat="server" Text=" Additional Info"></asp:Label></th>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbxKnittingSpareSupplierName" Width="100%" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="tbxKnittingSapreSupplierAddress" Width="100%" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </td>

                                                 <td>
                                                    <asp:TextBox ID="tbxSupplierPhone" Width="100%" runat="server" TextMode="Phone"></asp:TextBox>
                                                </td>

                                                 <td>
                                                    <asp:TextBox ID="tbxSupplierEmail" Width="100%" runat="server" TextMode="Email"></asp:TextBox>
                                                </td>


                                                <td>
                                                    <asp:TextBox ID="tbxKnittingSpareSupplierAdditionalInfo" Width="100%" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                 </table>
                                            <div class="pull-right" style="margin-right: 0px">
                                                <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="81px" Text="Add Row" OnClick="btnAddRow_Click" />
                                            </div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>

            </div>

            <br />
            <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
            <asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>
        </div>
    </div>


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
