﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewKnittingMachineSpareReceives.aspx.cs" Inherits="MBTracker.Pages.Knitting_Spares.ViewKnittingMachineSpareReceives" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table th, .table td {
            padding: 4px;
            line-height: 10px;
            text-align: center;
            vertical-align: top;
            border-top: 1px solid #e0e0e0;
        }
    </style>

    <div class="row-fluid">
        <div class="span6">
            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="View Knitting Spare Receives:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">

                    <div class="row-fluid" runat="server" visible="true" id="FilterDiv">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 10px; padding-bottom: 20px">
                            <%-- <span style="font-weight: 700; padding-right: 400px"></span><span style="font-weight: 700; color: #CC0000">*</span> Required Field--%>
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="col-md-10" style="padding-left: 0px">

                            <div class="col-md-9" style="padding-left: 0px">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">
                                            <asp:Label ID="lblFilterDate" runat="server" Text="Select Receive Date:"></asp:Label>
                                            <span style="font-weight: 700; color: #CC0000">*</span>
                                        </label>
                                        <div class="controls controls-row" style="margin-left: 150px">
                                            <asp:TextBox ID="tbxFilterDate" runat="server" placeholder="Enter date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="tbxFilterDate"><span style="font-weight: 700; color: #CC0000">Please select a receive date.</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">
                                            <asp:Label ID="Label8" runat="server" Text=""></asp:Label></label>
                                        <div class="controls controls-row" style="margin-left: 150px">
                                            <br />
                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="View Spare Receives" CssClass="btn btn-info pull-left" OnClick="btnSearch_Click" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="row-fluid" runat="server">
        <div class="span12">

            <div class="span6">
                <div class="widget" id="dataDiv" runat="server" visible="false">
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label" style="padding-bottom: 10px">
                                    <asp:Label runat="server" ID="lblLabelMessage" Text="Spare Received:" Font-Bold="true" Visible="false"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rpt" runat="server" OnItemCommand="rpt_ItemCommand">
                                        <HeaderTemplate>
                                            <table id="data-table1" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <%--                                                    <th class="hidden">
                                                        <asp:Label ID="Label3" runat="server" Text="SpareReceiveId"></asp:Label></th>--%>
                                                        <%-- <th>
                                                        <asp:Label ID="Label1" runat="server" Text="Date Received"></asp:Label></th>--%>
                                                        <th>
                                                            <asp:Label ID="Label2" runat="server" Text="Supplier"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label7" runat="server" Text="Req. Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Bill Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label6" runat="server" Text="Chalan Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label4" runat="server" Text="Other Ref."></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label16" runat="server" Text="Actions"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblSupplierName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SupplierName")%>'></asp:Label><asp:Label ID="lblReceiveDate" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ReceiveDate")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblRequsitionNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RequisitionNumber")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblInvoiceNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "InvoiceNumber")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblChalanNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ChalanNumber")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblOtherRefNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OtherRefNumber")%>'></asp:Label></td>

                                                <td>
                                                    <%--<asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete"  Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("KnittingUnitId")+","+ Eval("KnittingDate")+","+ Eval("ShiftId")%>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>--%>
                                                    <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("KnittingSpareReceiveId")%>' class="btn btn-success btn-mini hidden-phone" Text="Edit"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="ViewDetails" CommandArgument='<%#Eval("KnittingSpareReceiveId")%>' class="btn btn-success btn-mini hidden-phone" Text="View Details"></asp:LinkButton>

                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                         </table>
                            <div class="clearfix">
                                        </FooterTemplate>
                                    </asp:Repeater>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="span6">
            </div>
        </div>
    </div>
    <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>

    <div class="row-fluid" runat="server" id="pnlDetails" visible="false">

        <div class="span6">
            <div class="widget">
                <div class="widget-body">
                    <div class="row-fluid" runat="server">
                        <div class="form-horizontal">

                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblDateReceived" runat="server" Text="Date Received:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left">
                                            <asp:Label ID="lblDateReceivedValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="Label2" runat="server" Text="Supplier:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left">
                                            <asp:Label ID="lblSupplierValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblRequisitionNumber" runat="server" Text="Req. Number:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left">
                                            <asp:Label ID="lblReqNumberValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblInvoiceNumber" runat="server" Text="Invoice/Bill Number:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left">
                                            <asp:Label ID="lblInvoiceValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblChalanNumber" runat="server" Text="Chalan Number:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left">
                                            <asp:Label ID="lblChalanNumberValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="inputBuyer" class="control-label">
                                        <asp:Label ID="lblOtherRef" runat="server" Text="Other Reference(s):"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left">
                                            <asp:Label ID="lblOtherRefValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>
                            </div>


                            <%-- <asp:Repeater ID="rptKnittingSpareReceiveInfo" runat="server" Visible="false">
                                <HeaderTemplate>
                                    <table id="data-table" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>

                                                <th>
                                                    <asp:Label ID="lblSparesType" runat="server" Text="Spares Type"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="lblMachineBrand" runat="server" Text="Machine Brand"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="lblSpareName" runat="server" Text="Spare Name"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="lblSupplierCode" runat="server" Text="Supplier's Code"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="lblSpareDescription" runat="server" Text="Spare Description"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="lblQuantity" runat="server" Text="Quantity"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%#Eval("PartTypeName") %></td>
                                        <td><%#Eval("MachineBrandName") %></td>
                                        <td><%#Eval("SpareName") %></td>
                                        <td><%#Eval("SupplierCode") %></td>
                                        <td><%#Eval("SpareDescription") %></td>
                                        <td><%#Eval("SpareQuantity") %></td>
                                        <td><%#Eval("SpareUnitPrice") %></td>

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                 </table>
                                </FooterTemplate>
                            </asp:Repeater>--%>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="row-fluid" runat="server" id="Div1" visible="false">
        <div class="span9">

            <div class="widget">
                <div class="widget-body">

                    <div id="dt_example" class="example_alt_pagination">
                        <div class="control-group">
                            <label for="inputOrder" class="control-label">
                                <asp:Label ID="lblDetails" runat="server" Visible="false" Text="Detailed Information:" Font-Bold="true"></asp:Label></label>
                            <div class="controls controls-row" style="width: 100%; overflow-x: auto">

                                <asp:Repeater ID="rptKnittingSpareReceiveInfo" runat="server" Visible="false">
                                    <HeaderTemplate>
                                        <table id="data-table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>

                                                    <th>
                                                        <asp:Label ID="lblSparesType" runat="server" Text="Spares Type"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblMachineBrand" runat="server" Text="Machine Brand"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblSpareName" runat="server" Text="Spare Name"></asp:Label></th>
                                                   <%-- <th>
                                                        <asp:Label ID="lblInternalCode" runat="server" Text="Internal Code"></asp:Label></th>--%>
                                                    <th>
                                                        <asp:Label ID="lblSupplierCode" runat="server" Text="Supplier's Code"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblSpareDescription" runat="server" Text="Spare Description"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblQuantity" runat="server" Text="Quantity"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="text-align: left"><%#Eval("PartTypeName") %></td>
                                            <td><%#Eval("MachineBrandName") %></td>
                                            <td style="text-align: left"><%#Eval("SpareName") %></td>
                                          <%--  <td><%#Eval("SpareCode") %></td>--%>
                                            <td><%#Eval("SupplierCode") %></td>
                                            <td style="text-align: left"><%#Eval("SpareDescription") %></td>
                                            <td><%#Eval("Quantity") %></td>
                                            <td><%#Eval("UnitPrice") %></td>


                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                 </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    </div>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
