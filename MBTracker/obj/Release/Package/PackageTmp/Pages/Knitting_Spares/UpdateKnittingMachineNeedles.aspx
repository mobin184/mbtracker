﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="UpdateKnittingMachineNeedles.aspx.cs" Inherits="MBTracker.Pages.Knitting_Spares.UpdateKnittingMachineNeedles" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid" id="divBuyerAndStyle" runat="server">
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="sizeActionTitle" Text="Update Machine Needles:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <span style="font-weight: 700; color: #CC0000">*</span>
                                <asp:Label ID="Label1" runat="server" Text="Select Knitting Unit:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlKnittingUnits" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" CssClass="form-control" OnSelectedIndexChanged="ddlKnittingUnits_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <span style="font-weight: 700; color: #CC0000">*</span>
                                <asp:Label ID="lblMachineBrand" runat="server" Text="Select Machine Brand:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlMachineBrands" runat="server" Display="Dynamic" Width="60%" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="&nbsp;"></asp:Label></label>
                            <div class="controls controls-row">
                                <br />
                                <asp:Button ID="btnViewKnittingMachines" runat="server" class="btn btn-success btn-midium pull-left btnStyle" Text="Show Machines" OnClick="btnViewKnittingMachines_Click" />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <asp:Label ID="lblNoKnittingMachineFound" Style="width: 100%" runat="server" Visible="false" Text="Knitting machine was not found." BackColor="#ffff00"></asp:Label>
            <asp:Panel ID="pnlViewKnittingMachines" runat="server" Visible="false">

                <div class="widget">
                    <div class="widget-body">
                        <div class="form-horizontal">
                            <div class="control-group" style="overflow-x: auto">
                                <label for="inputStyle" class="control-label" style="font-weight: bold; text-align: left">
                                    <asp:Label ID="Label3" runat="server" Style="font-weight: bold; text-align: left" Font-Bold="false" Text="Needle Information:"></asp:Label></label>
                                <div class="controls-row">
                                    <asp:Repeater ID="rptKnittingMachines" runat="server" OnItemDataBound="rptKnittingMachines_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblMachineName" runat="server" Text="Machine Name"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblNeedleCapacity" runat="server" Text="Capacity"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblNumNeedlesBefore" runat="server" Text="Qty Before"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblNumNeedleChanged" runat="server" Text="Change Qty"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblSupplier" runat="server" Text="Needle Brand"></asp:Label></th>

                                                        <th>
                                                            <asp:Label ID="lblWhyChange" runat="server" Text="Why Change?"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblOtherMachine" runat="server" Text="Other Machine"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblNumNeedlesAfter" runat="server" Text="Qty After"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr style="vertical-align:middle">
                                                <td style="text-align: center; width: 13%;vertical-align:middle">
                                                    <asp:Label ID="lblMachineNameValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MachineCode")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                    <asp:Label ID="lblMachineId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Id")%>' Visible="false"></asp:Label>
                                                </td>
                                                <td style="text-align: center; width: 5%;vertical-align:middle">
                                                    <asp:Label ID="lblNeedleCapacityValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NeedleCapacity")%>' Width="100%" CssClass="text-center"></asp:Label></td>
                                                <td style="text-align: center; width: 6%;vertical-align:middle">
                                                    <asp:Label ID="lblNumNeedlesBeforeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NumNeedleBefore")%>' Width="100%" CssClass="text-center"></asp:Label></td>

                                                <td style="text-align: center; width: 7%;vertical-align:middle">
                                                    <asp:TextBox ID="tbxNumNeedlesChangeValue" Width="90%" runat="server" AutoPostBack="true" TextMode="Number" OnTextChanged="tbxNumNeedlesChangeValue_OnTextChanged"></asp:TextBox></td>

                                                <td style="text-align: center; width: 15%;vertical-align:middle">
                                                    <asp:DropDownList ID="ddlNeedleBrands" runat="server" CssClass="form-control" Style="min-width: 110px" Width="100%"></asp:DropDownList></td>

                                                <td style="text-align: center; width: 16%;vertical-align:middle">
                                                    <asp:DropDownList ID="ddlWhyChange" runat="server" CssClass="form-control" AutoPostBack="true" Style="min-width: 110px" Width="100%" OnSelectedIndexChanged="ddlWhyChange_SelectedIndexChanged"></asp:DropDownList></td>

                                                 <td style="text-align: center; width: 13%;vertical-align:middle">
                                                    <asp:DropDownList ID="ddlOtherMachines" runat="server" CssClass="form-control" Style="min-width: 125px" Enabled="false"></asp:DropDownList></td>
                                                <td style="text-align: center; width: 6%;vertical-align:middle">
                                                    <asp:Label ID="lblNumNeedleAfterValue" runat="server" Text="" Width="100%" CssClass="text-center"></asp:Label></td>
                                                <td style="text-align: center; width: 19%;vertical-align:middle">
                                                    <asp:TextBox ID="tbxRemarks" Width="100%" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
                <%--<asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>--%>
            </asp:Panel>


        </div>
    </div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
    <script src="../../js/jquery.dataTables.js"></script>
</asp:Content>
