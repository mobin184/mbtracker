﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddNewOrderV2.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.AddNewOrderV2" Async="true" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        input[type="radio"], input[type=checkbox] {
            margin: -2px 0 0;
        }


        .cblClass label {
            /*display: block;*/
            padding-left: 2px !important;
            padding-right: 14px !important;
        }
    </style>

    <asp:UpdatePanel ID="orderUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlBasicInfo" runat="server" >
            <div class="row-fluid">
                <div class="span9">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title">
                                <span class="fs1" aria-hidden="true" data-icon=""></span>
                                <asp:Label runat="server" ID="userActionTitle" Text="Add a New Order"></asp:Label>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div style="text-align: right; font-size: 12px; line-height: 30px; padding-bottom: 0px">
                                <span style="font-weight: 700; color: #CC0000">*</span>Required Field
                            </div>
                            <div class="control-group">
                                <div class="col-md-12 col-sm-12">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-horizontal">

                                        <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputStyle" class="control-label">
                                                <asp:Label ID="lblStyle" runat="server" Text="Select a Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlStyle" runat="server" Display="Dynamic" CssClass="form-control" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlStyle_SelectedIndexChanged"></asp:DropDownList>
                                                 <%--<asp:TextBox ID="tbxSearchCriteriaForStyle" runat="server" onkeyup="FilterItems(this.value)" Width="28%" PlaceHoder="Type to Search style(s)"></asp:TextBox>--%>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlStyle"><span style="font-weight: 700; color: #CC0000">Please select a style.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputBuyerDepartment" class="control-label">
                                                <asp:Label ID="Label9" runat="server" Text="Style's Department:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlBuyerDeprtments" runat="server" Display="Dynamic" CssClass="form-control"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlBuyerDeprtments"><span style="font-weight: 700; color: #CC0000">Please select a Department.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputOrderSeason" class="control-label">
                                                <asp:Label ID="Label4" runat="server" Text="Order Season:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                            </label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlSeason" runat="server" Display="Dynamic" CssClass="form-control"></asp:DropDownList>
                                                
                                            </div>
                                        </div>

                                         <div class="control-group">
                                            <label for="inputBookingNumber" class="control-label">
                                                <asp:Label ID="lblBookingNumber" runat="server" Text="Booking Number:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                            </label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlBookingNumber" runat="server" Display="Dynamic" CssClass="form-control"></asp:DropDownList>
                                                
                                            </div>
                                        </div>



                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <label for="inputOrderSeason" class="control-label">
                                                <asp:Label ID="Label8" runat="server" Text="Order Year:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                            </label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlOrderYear" runat="server" Display="Dynamic" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlOrderYear_SelectedIndexChanged"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputOrderNumber" class="control-label">
                                                <asp:Label ID="Label1" runat="server" Text="PO Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                            </label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxOrderNumber" runat="server" placeholder="Enter PO number:" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxOrderNumber"><span style="font-weight: 700; color: #CC0000">Enter PO Number.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputOrderDate" class="control-label">
                                                <asp:Label ID="Label2" runat="server" Text="Order Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                            </label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxOrderDate" runat="server" placeholder="Enter order date:" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxOrderDate"><span style="font-weight: 700; color: #CC0000">Enter Order Date.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="inputExFactoryDate" class="control-label">
                                                <asp:Label ID="ExFactoryDate" runat="server" Text="Ex Factory Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                            </label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxExFactoryDate" runat="server" placeholder="Enter EX Factory date:" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbxExFactoryDate"><span style="font-weight: 700; color: #CC0000">Enter Ex Factory Date.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group hidden">
                                            <label for="inputOrderItem" class="control-label">
                                                <asp:Label runat="server" CssClass="hidden" ID="Label5"></asp:Label>
                                                <asp:Label ID="Label7" runat="server" Text="Order Item:"></asp:Label>
                                            </label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxOrderItem" runat="server" placeholder="Enter order item" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputOrderStatus" class="control-label">
                                                <asp:Label ID="Label11" runat="server" Text="Order Status:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlOrderStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlOrderStatus"><span style="font-weight: 700; color: #CC0000">Select order status</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 0px">
                                            <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
                </asp:Panel>
            <div class="row-fluid">
                <div class="span12">

                    <asp:Panel ID="pnlColorAndSize" runat="server" Visible="false">
                        <div class="widget">
                            <div class="widget-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-horizontal">

                                            <div class="control-group" style="padding-top: 40px">
                                                <label for="inputDeliveryCountry" class="control-label">
                                                    <asp:Label ID="lblColorAndSize" runat="server" Visible="false" Text="Order Quantity by Color:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>

                                                <div class="controls controls-row">
                                                    <div id="dt_example" class="example_alt_pagination" style="overflow-x: auto">
                                                        <asp:Repeater ID="rptColor" runat="server" OnItemDataBound="rptColor_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="data-table2" class="table table-bordered table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>
                                                                                <asp:Label ID="lblColor" runat="server" Text="Color"></asp:Label></th>
                                                                            <th>
                                                                                <asp:Label ID="lblOrderQtyByColor" runat="server" Text="Order Quantity"></asp:Label></th>
                                                                            <th>
                                                                                <asp:Label ID="lblUnitCost" runat="server" Text="Unit Cost"></asp:Label></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td style="width: 40%">
                                                                        <asp:Label ID="lblColorValue" runat="server" Text='<%#Eval("ColorDescription") %>'></asp:Label>
                                                                         <asp:Label ID="lblColorId" runat="server" Text='<%#Eval("BuyerColorId") %>' Visible="false"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%">
                                                                        <asp:TextBox ID="tbxOrderQtyByColor" CssClass="dummyClass" Height="30" runat="server" TextMode="Number"></asp:TextBox></td>
                                                                     <td style="width: 30%">

                                                                    <%--    <asp:TextBox ID="tbxUnitCost" CssClass="dummyClass" Height="30" runat="server" TextMode="Number"></asp:TextBox>--%>
                                                                          <asp:TextBox ID="tbxUnitCost" runat="server" Text=""></asp:TextBox>

                                                                     </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tbody>
                                        </table>
                                                   


                                                            </FooterTemplate>
                                                        </asp:Repeater>

                                                    </div>
                                                </div>

                                            </div>

                                            <asp:Panel ID="pnlStyleSizes" runat="server" Visible="false">
                                                <div class="control-group">
                                                    <label for="inputBuyers" class="control-label">
                                                        <asp:Label ID="Label3" runat="server" Text="Select Order Sizes:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                                    </label>
                                                </div>

                                                <div class="controls controls-row">
                                                    <div class="form-inline cblClass">
                                                        <asp:CheckBoxList ID="cblStyleSizes" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="4" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                                     <asp:Label ID="lblNoSizeFound" runat="server" Visible="false" Text="No size found." BackColor="#ffff00"></asp:Label>
                                                    </div>
                                                </div>
                                                <div style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 0px">
                                                    <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                                </div>
                                                <div class="controls controls-row">
                                                    <asp:Button ID="btnCheckAllSizes" runat="server" Text="Select All Sizes" OnClick="btnCheckAllSizes_Click" />
                                                    <asp:Button ID="btnUnCheckAllSizes" runat="server" Text="Unselect All Sizes" OnClick="btnUnCheckAllSizes_Click" />
                                                </div>
                                                <div style="text-align: right; font-size: 12px; line-height: 30px; padding-bottom: 0px">
                                                    <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                                </div>
                                            </asp:Panel>


                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-horizontal">
                                            <div class="control-group" style="padding-top: 40px">
                                                <label for="inputDeliveryCountry" class="control-label">
                                                    <asp:Label ID="lblDeliveryCountry" runat="server" Visible="false">Country Of Final Destination:<span style="font-weight: 700; color: #CC0000">*</span></asp:Label>
                                                </label>
                                                <div class="controls controls-row" style="padding-top: 5px">
                                                    <div class="form-inline cblClass">
                                                        <asp:Label ID="lblNoCountryFound" runat="server" Visible="false" Text="No country found." BackColor="#ffff00"></asp:Label>
                                                        <asp:CheckBoxList ID="cblDeliveryCountries" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="5" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                                    </div>
                                                </div>
                                            </div>

                                            

                                            <div class="controls controls-row">
                                                <%--<asp:Label ID="Label14" runat="server" Visible="false"><span style="font-weight: 700; color: #CC0000">Please select a delivery country.</span></asp:Label>--%>
                                                <asp:Button ID="btnSelectAllCountries" runat="server" Visible="false" Text="Select All Countries" OnClick="btnSelectAllCountries_Click" />
                                                <asp:Button ID="btnUnselectAllCountries" runat="server" Visible="false" Text="Unselect All Countries" OnClick="btnUnselectAllCountries_Click" />
                                            </div>
                                            <br />
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label for="inputEmail3" class="control-label">
                                                        <asp:Label ID="Label33" runat="server" Text="Status:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                                    <div class="controls controls-row">
                                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control" Width="100%">
                                                        </asp:DropDownList>
                                                    </div>
                                                   </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                    </asp:Panel>


                </div>
            </div>


            <asp:Panel ID="pnlFileUploads" runat="server" Visible="false">
                <div class="span11">
                    <div class="widget-body">
                        <div class="form-horizontal">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="control-group" style="padding-top: 0px;">

                        <asp:Button ID="btnSaveOrder" runat="server" Style="margin-right: 60px" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Save Order&nbsp;&nbsp;&nbsp;" ValidationGroup="save" OnClick="btnSaveOrder_Click" />
                        <asp:Button ID="btnUpdateOrder" runat="server" Visible="false" Style="margin-right: 60px" class="btn btn-success btn-samll pull-right btnStyle" Text="&nbsp;&nbsp;&nbsp;Update Order&nbsp;&nbsp;&nbsp;" OnClick="btnUpdateOrder_Click" />
                    </div>
                </div>
            </asp:Panel>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSaveOrder" />
            <asp:PostBackTrigger ControlID="btnUpdateOrder" />
        </Triggers>
    </asp:UpdatePanel>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">

   <%-- <script type="text/javascript">

        var ddlText, ddlValue, ddl, lblMesg;

        function CacheItems() {


            ddlText = new Array();
            ddlValue = new Array();
            ddl = document.getElementById("<%=ddlStyle.ClientID %>");
            for (var i = 0; i < ddl.options.length; i++) {
                ddlText[ddlText.length] = ddl.options[i].text;
                ddlValue[ddlValue.length] = ddl.options[i].value;
            }

            //alert("The length of ddl: " + ddl.options.length);
        }


        function FilterItems(value) {
            ddl.options.length = 0;

            for (var i = 0; i < ddlText.length; i++) {

                if (ddlText[i].toLowerCase().indexOf(value) != -1) {

                    AddItem(ddlText[i], ddlValue[i]);
                }
            }


            function AddItem(text, value) {

                var opt = document.createElement("option");

                opt.text = text;
                opt.value = value;
                ddl.options.add(opt);

            }
        }

    </script>--%>

</asp:Content>
