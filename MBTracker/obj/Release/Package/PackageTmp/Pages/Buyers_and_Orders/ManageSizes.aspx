﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ManageSizes.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.ManageSizes" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid">
        <div class="span6">
            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span><asp:Label runat="server" ID="sizeActionTitle" Text="Add a New Size:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 20px;padding-bottom: 20px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                       
                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="lblSizeName" runat="server" Text="Size Name"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxSizeName" runat="server" placeholder="Enter Size Name" CssClass="form-control" Width="250"></asp:TextBox>
                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxSizeName"><span style="font-weight: 700; color: #CC0000">Enter size name</span></asp:RequiredFieldValidator> 
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label1" runat="server" Text="Size Type"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlSizeType" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlSizeType"><span style="font-weight: 700; color: #CC0000">Enter size type</span></asp:RequiredFieldValidator> 
                            </div>
                        </div>

                        <div class="control-group">
                                <label for="position sequence" class="control-label">
                                    <asp:Label ID="position_sequence_No" runat="server" Text="position sequence No:"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxposition_sequence_No" runat="server" placeholder="Order No" Width="250" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                        <div class="clearfix"></div>

                        <div class="control-group">
                                <br />
                                <div class="controls-row">
                                    <asp:button id="btnSave" runat="server" text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" cssclass="btn btn-info pull-right" validationgroup="save" onclick="btnSave_Click" />
                                    <asp:button id="btnUpdate" runat="server" visible="False" text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" onclick="btnUpdate_Click" cssclass="btn btn-info pull-right" validationgroup="save" />
                                </div>
                            </div>




                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="span6">
            <asp:Panel ID="pnlSizes" runat="server" Visible="false">

             <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>Sizes:
                    </div>
                </div>
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <asp:Repeater ID="rpt" runat="server" OnItemCommand="rpt_ItemCommand">
                            <HeaderTemplate>
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Label ID="Label2" runat="server" Text="Buyer Name"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label5" runat="server" Text="Size Name"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label4" runat="server" Text="Type"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label6" runat="server" Text="Order"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="Label3" runat="server" Text="Actions"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <%--<td class="hidden"><%#Eval("Id") %>></td>--%>
                                    <td><%#Eval("BuyerName") %> </td>
                                    <td><%#Eval("SizeName") %></td>
                                    <td><%#Eval("SizeType") %></td>
                                    <td><%#Eval("OrderBy") %></td>
                                     <td>
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" Visible='<%#ViewState["deleteEnabled"]%>' CommandName="Delete" CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-small hidden-phone" Text="Delete"></asp:LinkButton>
                                     </td>

                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                         </table>
                            <div class="clearfix">
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            </asp:Panel>


        </div>
    </div>

</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
    <script src="../../js/jquery.dataTables.js"></script>
</asp:Content>
