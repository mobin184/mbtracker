﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewEditBuyers.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.ViewEditBuyers" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
<div class="row-fluid">
       <%--   <div class="span2"></div>--%>
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>Buyers
                    </div>
                </div>
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
                            <asp:Repeater ID="rptBuyers" runat="server">
                                <HeaderTemplate>
                                    <thead>
                                        <tr>
                                            <th style="width: 35%">Buyer Name</th>
                                            <th style="width: 35%">Description</th>
                                            <th style="width: 30%" class="hidden-phone">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="gradeX">
                                        <td style="text-align:left"><%#Eval("BuyerName") %> </td>
                                        <td style="text-align:left"><%#Eval("BuyerInfo") %> </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("BuyerId") %>' class="btn btn-success btn-small hidden-phone" OnCommand="btnEdit_Command" Text="Edit"></asp:LinkButton>&nbsp;&nbsp;
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" Visible='<%#ViewState["deleteEnabled"]%>'  CommandName="Delete" CommandArgument='<%#Eval("BuyerId") %>' class="btn btn-danger btn-small hidden-phone" OnCommand="btnDelete_Command" Text="Delete"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

     </div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
     <script src="../../js/jquery.dataTables.js"></script>
</asp:Content>


