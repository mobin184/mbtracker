﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewTimeAndActionPlans.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.ViewTimeAndActionPlans" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <%-- <style>
        .form-horizontal .control-label {
            width: 110px;
        }

        .form-horizontal .controls {
            margin-left: 115px;
        }

        #dt_example td, #dt_example th {
            vertical-align: middle;
            white-space: nowrap;
        }

        .bold {
            font-weight: bold;
        }

        .form-control-without-border {
            font-size: 12px;
            display: block;
            width: 100%;
            height: 34px;
            padding: 10px 12px;
            color: #555;
            background-color: #fff;
            background-image: none;
        }
    </style>--%>
    <style>
        .controls-row {
            padding-top: 10px;
            padding-left: 0px;
            /*margin-left: 170px !important;*/
        }

        .control-group {
            margin-bottom: 0px;
        }
    </style>

    <div class="row-fluid" runat="server" visible="true" id="FilterDiv">
        <div class="col-md-6" style="padding-left: 0px; margin-bottom: -50px">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>View Time & Action Plans
                    </div>
                </div>
                <div class="widget-body" style="min-height: 200px">
                     <div class="control-group">
                                <div class="col-md-12 col-sm-12">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                </div>
                            </div>
                    <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 10px">
                                    <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                                </div>
                    <div class="col-md-8">
                        <div class="form-horizontal">
                            
                            <div class="control-group">
                                <label for="inputBuyerName" class="control-label">
                                    <asp:Label ID="lblBuyerName" runat="server" Text="Select Buyer:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlBuyerName" runat="server" placeholder="Select Buyer" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlBuyerName"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputStyleName" class="control-label">
                                    <asp:Label ID="lblStyleName" runat="server" Text="Select Style:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlStyleName" runat="server" placeholder="Select Style" Width="100%" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlStyleName_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlStyleName"><span style="font-weight: 700; color: #CC0000">Please select an style.</span></asp:RequiredFieldValidator>

                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputShipmentYear" class="control-label">
                                    <asp:Label ID="lbl2" runat="server" Text="Select Year:"></asp:Label>
                                    <span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlYear" runat="server" placeholder="Select Year" Width="100%" CssClass="form-control"></asp:DropDownList>
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                                    ControlToValidate="ddlYear"><span style="font-weight: 700; color: #CC0000">Please select year.</span></asp:RequiredFieldValidator>


                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4" style="padding-left: 10px;padding-top:10px;">
                        <div class="form-horizontal" style="width: 80%; float: left">
                            <asp:Button ID="btnReset" runat="server" Width="100%" Text="Reset Filter" CssClass="btn btn-danger pull-right hidden" OnClick="btnResetFilter_Click" />
                            <asp:Button ID="btnSearch" Width="100%" runat="server" Style="margin: 0px; margin-top: 90px" Text="View" CssClass="btn btn-info pull-right" OnClick="btnSearch_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

      <label for="inputStyle" class="control-label" style="font-weight:bold; padding-top:100px; margin-left:200px; text-align:left">
        <asp:Label ID="lblNotAuthorizedToView" Font-Bold="true" Visible="false" Font-Size="Large" runat="server" Text="You are not authorized to view this information."></asp:Label>
     </label>


    <div class="row-fluid hidden">
        <div class="col-md-12" style="padding-right: 0px">
            <asp:Button ID="btnShowFilter" runat="server" Text="Show Search Filter" CssClass="btn btn-info pull-right" OnClick="btnShowFilter_Click" />
            <asp:Button ID="btnHideFilter" runat="server" Visible="false" Text="Hide Search Filter" style="margin-top:20px;" CssClass="btn btn-info pull-right" OnClick="btnHineFilter_Click" />
        </div>
    </div>


    <div class="row-fluid" id="divTimeAndActionPlans" runat="server" visible="false" style="margin-top:35px;"> 
        <div runat="server">
            <div class="span12">
                <div class="widget">
                    <div class="widget-body" style="overflow-x: auto">
                        <div class="control-group">
                            <label for="inputDeliveryCountry" class="control-label" style="text-align: left">
                                <asp:Label ID="lblColorAndSize" Font-Bold="true" runat="server" Text="Time & Aciton Plans:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                        </div>

                        <div class=" controls-row">
                        <div id="dt_example" class="example_alt_pagination">


                            <asp:Repeater ID="rpt" runat="server">
                                <HeaderTemplate>
                                    <table id="data-table2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                    <th>
                                                        <asp:Label ID="lbl1" runat="server" Text="Delivery<br/>Quantity<br/>&nbsp;"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label3" runat="server" Text="Delivery<br/>Date<br/>&nbsp;"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label2" runat="server" Text="Yarn In-house<br/>Date<br/>&nbsp;"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label4" runat="server" Text="PP Approval<br/>Date<br/>&nbsp;"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label5" runat="server" Text="Sample<br/>Approval Date<br/>&nbsp;"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label6" runat="server" Text="Knitting Start<br/>Deadline Date<br/>&nbsp;"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label8" runat="server" Text="Main,Size & Care Label<br/>In-house Date"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label9" runat="server" Text="Hang,Price Tag<br/>In-house Date<br/>&nbsp;"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label10" runat="server" Text="Button,Zipper<br/>In-house Date<br/>&nbsp;"></asp:Label></th>
                                                     <th>
                                                        <asp:Label ID="Label13" runat="server" Text="Badge,Flower,Fabrics<br/>In-house Date<br/>&nbsp;"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label14" runat="server" Text="Print,Embroidery<br/>In-house Date<br/>&nbsp;"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label15" runat="server" Text="Sewing, Shoulder Tap<br/>In-house Date"></asp:Label></th>
                                                     <th>
                                                        <asp:Label ID="Label16" runat="server" Text="Poly<br/>In-house Date<br/>&nbsp;"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label18" runat="server" Text="Carton <br/>In-house Date<br/>&nbsp;"></asp:Label></th>
                                                     <th>
                                                        <asp:Label ID="Label7" runat="server" Text="Addi. Wrok Acc.<br/>In-house Date<br/>&nbsp;"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label11" runat="server" Text="Other <br/>Deadline Date<br/>&nbsp;"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label31" runat="server" Text="Latest Note<br/>for TNA<br/>&nbsp;"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblActions" runat="server" Text="Take<br/>Action(s)<br/>&nbsp;"></asp:Label></th>
                                                </tr>

                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="hidden"><%#Eval("Id") %>></td>
                                      <%--  <td><%#Eval("BuyerName") %></td>
                                        <td><%#Eval("StyleName") %></td>
                                        <td><%#Eval("Year") %></td>--%>

                                       

                                         <td ><asp:Label ID="lbl31" runat="server" Text='<%#Eval("Quantity") %>' Width="100px"></asp:Label></td>
                                         <td><asp:Label ID="Label1" runat="server" Text='<%#Eval("DeliveryDate") %>'  Width="100px"></asp:Label></td>
                                         <td><asp:Label ID="Label12" runat="server" Text='<%#Eval("YarnInHouseDate") %>'  Width="100px"></asp:Label></td>
                                         <td><asp:Label ID="Label17" runat="server" Text='<%#Eval("PPApprovalDate") %>'  Width="100px"></asp:Label></td>

                                         <td ><asp:Label ID="Label19" runat="server" Text='<%#Eval("SampleApprovalDate") %>' Width="100px"></asp:Label></td>
                                         <td><asp:Label ID="Label20" runat="server" Text='<%#Eval("KnittingStartDeadlineDate") %>'  Width="100px"></asp:Label></td>
                                         <td><asp:Label ID="Label21" runat="server" Text='<%#Eval("MainSizeAndCareLabelInHouseDate") %>'  Width="100px"></asp:Label></td>
                                         <td><asp:Label ID="Label22" runat="server" Text='<%#Eval("HangAndPriceTagInHouseDate") %>'  Width="100px"></asp:Label></td>

                                         <td ><asp:Label ID="Label23" runat="server" Text='<%#Eval("ButtonAndZipperInHouseDate") %>' Width="100px"></asp:Label></td>
                                         <td><asp:Label ID="Label24" runat="server" Text='<%#Eval("BadgeFlowersAndFabricInHouseDate") %>'  Width="100px"></asp:Label></td>
                                         <td><asp:Label ID="Label25" runat="server" Text='<%#Eval("PrintAndEmbroideryInHouseDate") %>'  Width="100px"></asp:Label></td>
                                         <td><asp:Label ID="Label26" runat="server" Text='<%#Eval("SewingShoulderTagInHouseDate") %>'  Width="100px"></asp:Label></td>

                                         <td ><asp:Label ID="Label27" runat="server" Text='<%#Eval("PolyInHouseDate") %>' Width="100px"></asp:Label></td>
                                         <td><asp:Label ID="Label28" runat="server" Text='<%#Eval("CartonInHouseDate") %>'  Width="100px"></asp:Label></td>


                                         <td ><asp:Label ID="Label29" runat="server" Text='<%#Eval("AdditionalWorkAccessoriesInHouseDate") %>' Width="100px"></asp:Label></td>
                                         <td><asp:Label ID="Label30" runat="server" Text='<%#Eval("OtherDeadlineDate") %>'  Width="100px"></asp:Label></td>


                                        <%--<td style="text-align:left"><asp:Label ID="Label32" runat="server" Text='<%#Eval("LatestNote") %>'  Width="100px"></asp:Label></td>--%>

                                        <td style="text-align:left"><asp:HyperLink runat="server" NavigateUrl='<%# Eval("Id", "AddTimeAndActionPlanNote.aspx?TNAId={0}") %>' Text='<%#Eval("LatestNote") %>' Width="150px"></asp:HyperLink></td>

                                        <td style="width:25%">
                                           <asp:LinkButton ID="lnkbtnEdit" Visible='<%#ViewState["editEnabled"]%>' runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' style="width:100px; margin-bottom:10px" class="btn btn-success btn-small hidden-phone" OnCommand="btnEdit_Command" Text="Edit" ></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete"  Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("Id") %>' style="width:100px;margin-bottom:10px" class="btn btn-danger btn-small hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete" ></asp:LinkButton>
                                            <asp:LinkButton ID="lkbtnAddNote" runat="server" CommandName="Delete"  CommandArgument='<%#Eval("Id") %>' class="btn btn-add btn-small hidden-phone" style="width:100px" OnCommand="lnkbtnAddNote_Command" Text="Add/View Note" ></asp:LinkButton>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                         </table>
                            <div class="clearfix">
                                </FooterTemplate>
                            </asp:Repeater>
                            
                            <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>
                        </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            </div>
        </div>
   

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
<%--    <script src="../../js/jquery.dataTables.js"></script>--%>
</asp:Content>
