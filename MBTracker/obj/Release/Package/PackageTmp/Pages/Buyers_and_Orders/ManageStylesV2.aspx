﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ManageStylesV2.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.ManageStylesV2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">


    <style>

       td > label
       {
           line-height:13px
       }


        .cblClass label
        {
            /*display: block;*/
            padding-left: 2px !important;
            padding-right: 14px !important;

        }

        input[type="radio"], input[type="checkbox"] {
            margin: -2px 5px 0px 0;
        }
    </style>


    <div class="row-fluid">


        <div class="span6">

            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="styleActionTitle" Text="Add a New Style:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 20px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="lblStyleName" runat="server" Text="Style Name"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxStyleName" runat="server" placeholder="Enter Style Name" CssClass="form-control" Width="250"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxStyleName"><span style="font-weight: 700; color: #CC0000">Enter style name</span></asp:RequiredFieldValidator>

                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 40px">
                      
                        </div>



                        <asp:Panel ID="pnlBuyerColors" runat="server" Visible="false">
                                     <div class="control-group">
                                        <label for="inputBuyers" class="control-label">
                                            <asp:Label ID="Label13" runat="server" Text="Style Colors:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                        </label>
                                    </div>
                             
                                    <div class="controls controls-row">
                                        <div class ="form-inline cblClass">
                                             <asp:CheckBoxList ID="cblBuyerColors" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="2" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                         </div>
                                     </div>
                                        <div style="text-align: right; font-size: 12px; line-height: 20px;padding-bottom: 0px">
                                           <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                        </div>
                                    <div class="controls controls-row">
                                        <asp:Button ID="btnCheckAllBuyerColors" runat="server" Text="Select All Colors" OnClick="btnCheckAllBuyerColors_Click" />
                                        <asp:Button ID="btnUnCheckAllBuyerColors" runat="server" Text="Unselect All Colors" OnClick="btnUnCheckAllBuyerColors_Click" />
                                    </div>
                                    <div style="text-align: right; font-size: 12px; line-height: 30px;padding-bottom: 0px">
                                        <span style="font-weight: 700; color: #CC0000">&nbsp;</span> 
                                    </div>
                             </asp:Panel>
                       
                        <div class="clearfix"></div>

                        
                        <div style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 40px">
                      
                        </div>


                       <asp:Panel ID="pnlBuyerSizes" runat="server" Visible="false">
                                     <div class="control-group">
                                        <label for="inputBuyers" class="control-label">
                                            <asp:Label ID="Label1" runat="server" Text="Style Sizes:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                        </label>
                                    </div>
                             
                                    <div class="controls controls-row">
                                        <div class ="form-inline cblClass">
                                             <asp:CheckBoxList ID="cblBuyerSizes" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="4" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                         </div>
                                     </div>
                                        <div style="text-align: right; font-size: 12px; line-height: 20px;padding-bottom: 0px">
                                           <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                        </div>
                                    <div class="controls controls-row">
                                        <asp:Button ID="btnCheckAllBuyerSizes" runat="server" Text="Select All Sizes" OnClick="btnCheckAllBuyerSizes_Click" />
                                        <asp:Button ID="btnUnCheckAllBuyerSizes" runat="server" Text="Unselect All Sizes" OnClick="btnUnCheckAllBuyerSizes_Click" />
                                    </div>
                                    <div style="text-align: right; font-size: 12px; line-height: 30px;padding-bottom: 0px">
                                        <span style="font-weight: 700; color: #CC0000">&nbsp;</span> 
                                    </div>
                             </asp:Panel>
                       
                        <div class="clearfix"></div>


                         <div class="control-group">
                                <br />
                                <div class="controls-row">
                                    <asp:button id="btnSave" runat="server" text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" cssclass="btn btn-info pull-right" validationgroup="save" onclick="btnSave_Click" />
                                    <asp:button id="btnUpdate" runat="server" visible="False" text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" onclick="btnUpdate_Click" cssclass="btn btn-info pull-right" validationgroup="save" />
                                </div>
                            </div>


                        <div class="clearfix"></div>
   

                    </div>
                </div>





            </div>

        </div>


        <div class="span6">

            <asp:Panel ID="pnlStyles" runat="server" Visible="false">

                <div class="widget">
                    <div class="widget-header">
                        <div class="title">
                            <span class="fs1" aria-hidden="true" data-icon=""></span>Styles:
                        </div>
                    </div>
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">
                            <asp:Repeater ID="rpt" runat="server" OnItemCommand="rpt_ItemCommand">
                                <HeaderTemplate>
                                    <table id="data-table" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <asp:Label ID="Label2" runat="server" Text="Buyer Name"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label5" runat="server" Text="Style Name"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label3" runat="server" Text="Actions"></asp:Label></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <%--<td class="hidden"><%#Eval("Id") %>></td>--%>
                                        <td><%#Eval("BuyerName") %> </td>
                                        <td><%#Eval("StyleName") %></td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="Completed"></asp:LinkButton>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                         </table>
                            <div class="clearfix">
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

            </asp:Panel>


        </div>
    </div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
