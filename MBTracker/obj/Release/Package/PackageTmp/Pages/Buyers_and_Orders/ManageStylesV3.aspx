﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ManageStylesV3.aspx.cs" Inherits="MBTracker.Pages.Buyers_and_Orders.ManageStylesV3" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">


    <style>
        td > label {
            line-height: 13px
        }


        .cblClass label {
            /*display: block;*/
            padding-left: 2px !important;
            padding-right: 14px !important;
        }

        input[type="radio"], input[type="checkbox"] {
            margin: -2px 5px 0px 0;
        }

     
      /*  .form-horizontal .control-label {
            float: left;
            width: 100px;
            padding-top: 5px;
            text-align: right;
        }
        .form-horizontal .controls {
            margin-left:120px;
        }*/
    </style>


    <div class="row-fluid">


        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <!--<span class="fs1" aria-hidden="true" data-icon=""></span>-->
                        <asp:Label runat="server" ID="styleActionTitle" Text="Add a New Style:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 20px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save"/>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="lblStyleName" runat="server" Text="Style Name"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxStyleName" runat="server" placeholder="Enter Style Name" CssClass="form-control" Width="250"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxStyleName"><span style="font-weight: 700; color: #CC0000">Enter style name</span></asp:RequiredFieldValidator>

                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label4" runat="server" Text="Style Season:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                          
                            
                           
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyleSeasons" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlStyleSeasons"><span style="font-weight: 700; color: #CC0000">Please select a season.</span></asp:RequiredFieldValidator>
                            <button  OnClick="javascript:window.open('/Pages/Buyers_and_Orders/ManageSeasons.aspx', '_blank');">Add</button>
                                <asp:Button  runat="server" Text="load"  OnClick="ddlBuyersStyleSeason_SelectedIndexChanged" />
                            
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputBuyer" class="control-label">
                                <asp:Label ID="Label6" runat="server" Text="Style Year:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStyleYear" runat="server" AutoPostBack="true" Display="Dynamic" Width="250" CssClass="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a year.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 40px">
                        </div>



                        <%--<asp:Panel ID="pnlBuyerColors" runat="server" Visible="false">
                            <div class="control-group">
                                <label for="inputBuyers" class="control-label">
                                    <asp:Label ID="Label13" runat="server" Text="Style Colors:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                            </div>

                            <div class="controls controls-row">
                                <div class="form-inline cblClass">
                                    <asp:CheckBoxList ID="cblBuyerColors" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="2" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                </div>
                                <button  OnClick="javascript:window.open('/Pages/Buyers_and_Orders/ManageColors.aspx', '_blank');">Add</button>
                                <asp:Button  runat="server" Text="load"  OnClick="ddlBuyersColors_SelectedIndexChanged" />
                            </div>

                            

                            <div style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 0px">
                                <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            </div>
                            <div class="controls controls-row">
                                <asp:Button ID="btnCheckAllBuyerColors" runat="server" Text="Select All Colors" OnClick="btnCheckAllBuyerColors_Click" />
                                <asp:Button ID="btnUnCheckAllBuyerColors" runat="server" Text="Unselect All Colors" OnClick="btnUnCheckAllBuyerColors_Click" />
                            </div>
                            <div style="text-align: right; font-size: 12px; line-height: 30px; padding-bottom: 0px">
                                <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            </div>
                        </asp:Panel>--%>

                        <div class="clearfix"></div>


                        <div style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 40px">
                        </div>


                        <asp:Panel ID="pnlBuyerSizes" runat="server" Visible="false">
                            <div class="control-group">
                                <label for="inputBuyers" class="control-label">
                                    <asp:Label ID="Label1" runat="server" Text="Style Sizes:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                            </div>

                            <div class="controls controls-row">
                                <div class="form-inline cblClass">
                                    <asp:CheckBoxList ID="cblBuyerSizes" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="4" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                </div>
                                <button OnClick="javascript:window.open('/Pages/Buyers_and_Orders/ManageSizes.aspx', '_blank');">Add</button>
                                <asp:Button  runat="server" Text="load"  OnClick="ddlBuyersSizes_SelectedIndexChanged" />
                            </div>
                            <div style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 0px">
                                <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            </div>
                            <div class="controls controls-row">
                                <asp:Button ID="btnCheckAllBuyerSizes" runat="server" Text="Select All Sizes" OnClick="btnCheckAllBuyerSizes_Click" />
                                <asp:Button ID="btnUnCheckAllBuyerSizes" runat="server" Text="Unselect All Sizes" OnClick="btnUnCheckAllBuyerSizes_Click" />
                            </div>
                            <div class="clearfix"></div>
                            <div style="text-align: right; font-size: 12px; line-height: 30px; padding-bottom: 0px">
                                <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            </div>
                        </asp:Panel>

                        <div class="clearfix"></div>


                        <%--<div class="control-group">
                            <br />
                            <div class="controls-row">
                                <asp:Button ID="btnSave" runat="server" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnSave_Click" />
                                <asp:Button ID="btnUpdate" runat="server" Visible="False" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdate_Click" CssClass="btn btn-info pull-right" ValidationGroup="save" />
                            </div>
                        </div>--%>


                        <div class="clearfix"></div>


                    </div>
                </div>
            </div>
        </div>


        <div class="span6">
            <asp:Panel ID="pnlStyles" runat="server" Visible="true">
                <div class="widget">
                    <div class="widget-header">
                        <div class="title">
                            <span class="fs1" aria-hidden="true" data-icon=""></span>Styles:
                        </div>
                    </div>
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">
                            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
                            <asp:Repeater ID="rpt" runat="server" OnItemCommand="rpt_ItemCommand">
                                <HeaderTemplate>
                                    
                                        <thead>
                                            <tr>
                                                <th>
                                                    <asp:Label ID="Label2" runat="server" Text="Buyer"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label5" runat="server" Text="Style"></asp:Label></th>
                                                 <th>
                                                    <asp:Label ID="Label7" runat="server" Text="Season"></asp:Label></th>
                                                 <th>
                                                    <asp:Label ID="Label8" runat="server" Text="Year"></asp:Label></th>
                                                 <th>
                                                    <asp:Label ID="Label9" runat="server" Text="Status"></asp:Label></th>
                                                <th>
                                                    <asp:Label ID="Label3" runat="server" Text="Actions"></asp:Label></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <%--<td class="hidden"><%#Eval("Id") %>></td>--%>
                                        <td><%#Eval("BuyerName")%> </td>
                                        <td><%#Eval("StyleName")%></td>
                                        <td><%#Eval("SeasonName") %></td>
                                        <td><%#Eval("StyleYear") %></td>
                                        <td style="white-space:nowrap">
                                             <asp:LinkButton ID="lnkbtnRunningStatus1" runat="server" Visible='<%#Eval("RunningStatus").ToString() == "false" ? true : false %>' CommandName="RunningStatus" CommandArgument='<%#Eval("Id") %>' class="btn btn-warning btn-small hidden-phone" Text="Not Run"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnRunningStatus" runat="server" Visible='<%#Eval("RunningStatus").ToString() == "true" ? true : false %>' CommandName="RunningStatus" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="Running"></asp:LinkButton>
                                        </td>
                                        <td  style="white-space:nowrap">
                                        <%--<% if(Session["UserRoleID"].ToString() == "1" || Session["UserRoleID"].ToString() == "19") {  %>--%>
                                           <%-- <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" Visible='<%#ViewState["editEnabled"]%>' CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>--%>
                                             <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="Completed"></asp:LinkButton>
                                        <%--<% } %>--%>
                                         </td>
                                        

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                 </FooterTemplate>
                                </asp:Repeater>
                         </table>
                            <div class="clearfix">
                                
                            
                        </div>
                    </div>
                    <!--<div class="clearfix"></div>-->
                </div>
                    </div>

            </asp:Panel>


        </div>
       

        <!--Color start-->
        <div class="span12" style="margin-left:0;">
             <div class="widget" style="padding-left: 15px; border: 2px solid #00897B;">
                 <div class="clearfix"></div>

                        <div style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 40px">
                        </div>

            <asp:Panel ID="pnlBuyerColors" runat="server" Visible="false">
                            <div class="control-group">
                                <label for="inputBuyers" class="control-label">
                                    <asp:Label ID="Label13" runat="server" Text="Style Colors:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                            </div>

                            <div class="controls controls-row">
                                <div class="form-inline cblClass">
                                    <asp:CheckBoxList ID="cblBuyerColors" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="8" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                </div>
                                <button OnClick="javascript:window.open('/Pages/Buyers_and_Orders/ManageColors.aspx', '_blank');">Add</button>
                                <asp:Button  runat="server" Text="load"  OnClick="ddlBuyersColors_SelectedIndexChanged" />
                            </div>

                            

                            <div style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 0px">
                                <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            </div>
                            <div class="controls controls-row">
                                <asp:Button ID="btnCheckAllBuyerColors" runat="server" Text="Select All Colors" OnClick="btnCheckAllBuyerColors_Click" />
                                <asp:Button ID="btnUnCheckAllBuyerColors" runat="server" Text="Unselect All Colors" OnClick="btnUnCheckAllBuyerColors_Click" />
                            </div>
                            <div style="text-align: right; font-size: 12px; line-height: 30px; padding-bottom: 0px">
                                <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            </div>
                        </asp:Panel>
                 <div class="clearfix"></div>
                 </div>


             <div class="control-group">
                            <br />
                            <div class="controls-row">
                                <asp:Button ID="btnSave" runat="server" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnSave_Click" />
                                <asp:Button ID="btnUpdate" runat="server" Visible="False" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdate_Click" CssClass="btn btn-info pull-right" ValidationGroup="save" />
                            </div>
                        </div>

        </div>

        <!--Color End-->

       <div class="clearfix"></div>

    </div>

     

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
