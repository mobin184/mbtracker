﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewRoles.aspx.cs" Inherits="MBTracker.Pages.Admin_and_Setup.ViewRoles" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
     <div class="row-fluid">
       <%--   <div class="span2"></div>--%>
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>Roles
                    </div>
                </div>
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
                            <asp:Repeater ID="rptrRole" runat="server">
                                <HeaderTemplate>
                                    <thead>
                                        <tr>
                                            <th style="width: 35%">Role Name</th>
                                            <th style="width: 35%">Description</th>
                                            <th style="width: 30%" class="hidden-phone">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="gradeX">
                                        <td style="text-align:left"><%#Eval("RoleName") %> </td>
                                        <td style="text-align:left"><%#Eval("Description") %> </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" OnCommand="btnEdit_Command" Text="Edit"></asp:LinkButton>&nbsp;&nbsp;
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-small hidden-phone" OnCommand="btnDelete_Command" Text="Delete"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
         <%-- <div class="span2"></div>--%>
     </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cpScript" runat="Server">
<%--    <script src="../../js/jquery.dataTables.js"></script>--%>
</asp:Content>




