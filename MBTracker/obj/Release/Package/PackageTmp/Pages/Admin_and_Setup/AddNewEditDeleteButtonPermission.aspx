﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddNewEditDeleteButtonPermission.aspx.cs" Inherits="MBTracker.Pages.Admin_and_Setup.AddNewEditDeleteButtonPermission" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <div class="row-fluid">
        <%--<div class="span2"></div>--%>
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="roleActionTitle" Text="Add a New Button"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal no-margin">
                        <div style="text-align: right; font-size: 12px; line-height: 50px; padding-bottom: 7px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                               RoleId<span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxRoleId" required="true" CssClass="span12 form-control" type="text" placeholder="Role Id"></asp:TextBox>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label">
                                Page Name<span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxPageName" required="true" CssClass="span12 form-control" type="text" placeholder="Page Name"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Button Type<span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxButtonType" required="true" CssClass="span12 form-control" type="text" placeholder="Button Type"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Button NumberOnPage<span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxButtonNumberOnPage" required="true" CssClass="span12 form-control" type="text" placeholder="Button Number On Page"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Enable Duration InDays<span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxEnableDurationInDays" required="true" CssClass="span12 form-control" type="text" placeholder="Enable Duration In Days"></asp:TextBox>
                            </div>
                        </div>

                        <div class="control-group">
                            <br />
                            <div class="controls-row">
                                <asp:Button ID="btnSubmit" runat="server" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnSave_Click" />
                                <asp:Button ID="btnUpdate" runat="server" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" Visible="false" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnUpdate_Click" />
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>


        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>Existing Buttons
                    </div>
                </div>
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
                            <asp:Repeater ID="rptButton" runat="server" OnItemCommand="rptButtonPermission_ItemCommand">
                                <HeaderTemplate>
                                    <thead>
                                        <tr>
                                            <th>Role ID</th>
                                            <th>Page Name</th>
                                            <th>Button Type</th>
                                            <th>Enable Days</th>
                                            <th class="hidden-phone">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="gradeX">
                                        <td style="text-align: left"><%#Eval("RoleId") %> </td>
                                        <td style="text-align: left"><%#Eval("PageName") %> </td>
                                        <td style="text-align: left"><%#Eval("ButtonType") %> </td>
                                        <td style="text-align: left"><%#Eval("EnableDurationInDays") %> </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("BuyerId") %>' class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>&nbsp;&nbsp;
                                            <%--<asp:LinkButton ID="lnkbtnDelete" runat="server" Visible='<%#ViewState["deleteEnabled"]%>' CommandName="Delete" CommandArgument='<%#Eval("BuyerId") %>' class="btn btn-danger btn-small hidden-phone" Text="Delete"></asp:LinkButton>--%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
