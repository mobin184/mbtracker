﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddNewItem.aspx.cs" Inherits="MBTracker.Pages.Admin_and_Setup.AddNewItem" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .dataTables_filter input{
            min-width:300px;
        }
        .active{
            color:green;
        }
        .inactive{
            color:red;
        }
        .yarn{
            color:blue;
        }
        .accessories{
            color:orangered;
        }
    </style>
    <div class="row-fluid">
        <%--<div class="span2"></div>--%>
        <div class="span5">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="roleActionTitle" Text="Add a New Item"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal no-margin">
                        <div style="text-align: right; font-size: 12px; line-height: 50px; padding-bottom: 7px">
                            <div class="col-md-12 col-sm-12">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                </div>
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Item Name <span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxItemName" required="true" CssClass="span12 form-control" type="text" ValidationGroup="save" placeholder="Item Name"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxItemName"><span style="font-weight: 700; color: #CC0000">Please enter item name.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Item Type <span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlItemType" runat="server" Display="Dynamic" CssClass="form-control">
                                    <asp:ListItem Text="---Select---" Value=""></asp:ListItem>                                    
                                    <asp:ListItem Text="Yarn" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Accessories" Value="1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlItemType"><span style="font-weight: 700; color: #CC0000">Please select a type.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">
                                Item Sub Type: <span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                        <asp:DropDownList ID="ddlItemSubType" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlItemSubType"><span style="font-weight: 700; color: #CC0000">Please select a Item Sub Type.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                         <div class="control-group">
                            <label class="control-label">
                                Unit Type: <span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                        <asp:DropDownList ID="ddlUnit" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlUnit"><span style="font-weight: 700; color: #CC0000">Please select a Unit.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label">
                                Item Status <span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlIsActive" runat="server" Display="Dynamic" CssClass="form-control">
                                    <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlIsActive"><span style="font-weight: 700; color: #CC0000">Please select a status.</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group">
                            <br />
                            <div class="controls-row">
                                <asp:Button ID="btnSubmit" runat="server" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnSave_Click" />
                                <asp:Button ID="btnUpdate" runat="server" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" Visible="false" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnUpdate_Click" />
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>


        <div class="span7">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>Existing Items
                    </div>
                </div>
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
                            <asp:Repeater ID="rptBuyers" runat="server" OnItemCommand="rptBuyer_ItemCommand">
                                <HeaderTemplate>
                                    <thead>
                                        <tr>
                                            <th style="width: 40%">Item Name</th>
                                            <th style="width: 20%">Item Type</th>
                                            <th style="width: 20%">Status</th>
                                            <th style="width: 20%" class="hidden-phone">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="gradeX">
                                        <td style="text-align: left"><%#Eval("ItemName") %> </td>
                                        <td style="text-align: center"><%#((int)Eval("ItemTypeId") == 1 ? "<span class='accessories'>Accessories</span>":"<span class='yarn'>Yarn</span>")  %> </td>
                                        <td style="text-align: center"><%#((int)Eval("IsActive") == 1 ? "<span class='active'>Active</span>":"<span class='inactive'>In-Active</span>")  %> </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>&nbsp;&nbsp;
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" Visible='<%#ViewState["deleteEnabled"]%>' CommandName="Delete" CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-small hidden-phone" Text="Delete"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
