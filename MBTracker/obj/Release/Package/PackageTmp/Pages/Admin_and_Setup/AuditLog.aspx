﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AuditLog.aspx.cs" Inherits="MBTracker.Pages.Admin_and_Setup.AuditLog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid">

        <div class="span9">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="actionTitle" Text="Enter Audit Log Information:"></asp:Label>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="widget-body">
                        <div class="control-group">
                            <div class="col-md-12">
                                <div class="pull-right" style="text-align: center; font-size: 12px; line-height: 20px; padding-bottom: 5px">
                                    <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                                </div>
                                <div class="control-group">
                                    <div class="col-md-12 col-sm-12">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                 <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="lblFromDate" runat="server" Text="From Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxFromDate" runat="server" placeholder="Enter date" Width="80%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxFromDate"><span style="font-weight: 700; color: #CC0000">Please enter From Date.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                   

                            </div>
                            <div class="col-md-6">
                               
                                <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="Label19" runat="server" Text="To Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxToDate" runat="server" placeholder="Enter date" Width="80%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxToDate"><span style="font-weight: 700; color: #CC0000">Please enter To Date Date.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                               
                               
                            </div>
                        </div>
                        <div class="control-group">
                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">

                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="View Audit log" CssClass="btn btn-info pull-center" OnClick="btnSearch_Click" />

                        </label>
                        <div class="controls controls-row">
                        </div>
                    </div>


                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class="row-fluid">
        <div class="span9">
            <asp:Label ID="lblNoSpareSupplierFound" Style="width: 100%" runat="server" Visible="false" Text="No Audit Log found." BackColor="#ffff00"></asp:Label>
            <asp:Panel ID="pnlViewSpareSuppliers" runat="server" Visible="false">

                <div class="widget">
                    <div class="widget-body">
                        <div class="form-horizontal">

                            <div style="text-align: left; font-size: 16px; font-weight: 800; line-height: 20px; text-underline-position: below; padding-bottom: 25px; padding-top: 10px">
                                List of Audit Logs:
                            </div>


                            <div class="control-group" style="overflow-x: auto">
                                <div class="controls-row">
                                    <asp:Repeater ID="rptKnittingSpareSuppliers" runat="server">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblFullName" runat="server" Text="Full Name"></asp:Label>
                                                        </th>
                                                        <th>
                                                            <asp:Label ID="lblEventDate" runat="server" Text="Event Date"></asp:Label>

                                                        </th>
                                                        <th>
                                                            <asp:Label ID="lblEventType" runat="server" Text="Event Type"></asp:Label>
                                                        </th>
                                                        <th>
                                                            <asp:Label ID="lblTableName" runat="server" Text="Table Name"></asp:Label>
                                                        </th>
                                                        
                                                        <th>
                                                            <asp:Label ID="LabelData" runat="server" Text="Data"></asp:Label>

                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="width:15%">
                                                    <asp:Label ID="lblFullName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FullName")%>' Width="100%" CssClass="text-left" Style="padding-top: 5px;"></asp:Label>
                                                    
                                                </td>
                                                <td style="width:20%">
                                                    <asp:Label ID="lblEventDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EventDate")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                    
                                                </td>
                                                <td style="width:14%">
                                                    <asp:Label ID="lblEventType" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EventType")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                    
                                                </td>
                                                <td style="width:18%">
                                                    <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TableName")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                    
                                                </td>
                                              <td style="width:33%">
                                                    <asp:Label ID="LabelData" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Data")%>' Width="100%" CssClass="text-left"></asp:Label>
                                                    
                                                </td>
                                                
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </asp:Panel>


        </div>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
