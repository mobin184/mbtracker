﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="AddNewMenue.aspx.cs" Inherits="MBTracker.Pages.Admin_and_Setup.AddNewMenue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <div class="row-fluid">
        
        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>  <asp:Label runat="server" ID="roleActionTitle" Text="Add a New Menu"></asp:Label>
                    </div>
                </div>
                <div class="widget-body" >
                    <div class="form-horizontal no-margin">
                        <div style="text-align: right; font-size: 12px; line-height: 50px;padding-bottom: 7px">
                            <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Menu Name<span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxTextEng" required="true" CssClass="span12 form-control" type="text" placeholder="Menu Name English"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                URL Name<span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxURL" required="true" CssClass="span12 form-control" type="text" placeholder="URL Name"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Parent Id<span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxParentId" required="true" CssClass="span12 form-control" type="text" placeholder="ParentId"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Order<span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox runat="server" ID="tbxOrder" required="true" CssClass="span12 form-control" type="text" placeholder="Order"></asp:TextBox>
                            </div>
                        </div>
                        
                         <div class="control-group">
                                <br />
                                <div class="controls-row">
                                    <asp:button id="btnSubmit" runat="server" text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" cssclass="btn btn-info pull-right" validationgroup="save" onclick="btnSave_Click" />
                                    <%--<asp:button id="btnUpdate" runat="server" text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" Visible="false" cssclass="btn btn-info pull-right" validationgroup="save" onclick="btnUpdate_Click" />--%>
                                </div>
                            </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="span6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>Existing Menu
                    </div>
                </div>
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
                            <asp:Repeater ID="rptMenu" runat="server" OnItemCommand="rptMenu_ItemCommand">
                                <HeaderTemplate>
                                    <thead>
                                        <tr>
                                            <th style="width: 35%">Text Eng</th>
                                            <th style="width: 35%">URL</th>
                                            <th style="width: 30%" class="hidden-phone">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="gradeX">
                                        <td style="text-align: left"><%#Eval("TextEng") %> </td>
                                        <td style="text-align: left"><%#Eval("URL") %> </td>
                                        <td>
                                            <%--<asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-small hidden-phone" Text="Edit"></asp:LinkButton>&nbsp;&nbsp;--%>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("Id") %>' class="btn btn-danger btn-small hidden-phone" Text="Delete"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
  

    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
