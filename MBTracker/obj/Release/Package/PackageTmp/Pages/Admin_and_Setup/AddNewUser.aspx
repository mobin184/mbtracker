﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="AddNewUser.aspx.cs" Inherits="MBTracker.Pages.Admin_and_Setup.AddNewUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <style>
        td > label {
            line-height: 13px
        }

        /*.table-bordered th, .table-bordered td {
            border-left: 1px solid #e0e0e0 !important;
            border-right: 1px solid #e0e0e0 !important;
            border-top: 1px solid #e0e0e0 !important;
            border-bottom: 1px solid #e0e0e0 !important;
        }*/


        .cblClass label {
            /*display: block;*/
            padding-left: 2px !important;
            padding-right: 14px !important;
        }

        input[type="radio"], input[type="checkbox"] {
            margin: -2px 5px 0px 0;
        }
    </style>


    <div class="row-fluid">
        <div class="span9">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="userActionTitle" Text="Add a New User"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="form-horizontal">
                        <div style="text-align: right; font-size: 12px; line-height: 30px; padding-bottom: 0px">
                            <span style="font-weight: 700; color: #CC0000">*</span>Required Field
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label runat="server" CssClass="hidden" ID="tbxUserId"></asp:Label>
                                <asp:Label ID="Label1" runat="server" Text="Full Name"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                            </label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxName" runat="server" placeholder="Enter your name" CssClass="form-control" Width="50%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxName"><span style="font-weight: 700; color: #CC0000">Enter your name</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label3" runat="server" Text="Mobile Number"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxMobile" TextMode="Phone" runat="server" placeholder="Enter your mobile number" CssClass="form-control" Width="50%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxMobile"><span style="font-weight: 700; color: #CC0000">Enter your mobile number</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label6" runat="server" Text="Email Address"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxEmail" TextMode="Phone" runat="server" Display="Dynamic" placeholder="Enter your email address" CssClass="form-control" Width="50%"></asp:TextBox>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label11" runat="server" Text="Department"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlDepartments" runat="server" CssClass="form-control" Width="50%"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlDepartments"><span style="font-weight: 700; color: #CC0000">Select Role Name</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label12" runat="server" Text="Designation"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlDesignations" runat="server" CssClass="form-control" Width="50%"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlDesignations"><span style="font-weight: 700; color: #CC0000">Select Designation Name</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputEmployeeID" class="control-label">
                                <asp:Label ID="Label9" runat="server" Text="Employee ID"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxEmployeeID" runat="server" placeholder="Enter employee ID" CssClass="form-control" Width="50%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxEmployeeID"><span style="font-weight: 700; color: #CC0000">Enter employee ID</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label4" runat="server" Text="User Role"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlRoles" runat="server" Display="Dynamic" CssClass="form-control custom-select2" AutoPostBack="true" OnSelectedIndexChanged="ddlRoles_SelectedIndexChanged" Width="50%"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="ddlRoles"><span style="font-weight: 700; color: #CC0000">Select Role Name</span></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <asp:Panel ID="pnlNotification" runat="server" Visible="true">
                            <div class="control-group">
                                <label for="inputBuyers" class="control-label">
                                    <asp:Label ID="Label13" runat="server" Text="Receive Emails For:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                </label>
                            </div>

                            <div class="controls controls-row">
                                <div class="form-inline cblClass">
                                    <asp:CheckBoxList ID="cblEvents" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="2" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                </div>
                            </div>
                            <div style="text-align: right; font-size: 12px; line-height: 20px; padding-bottom: 0px">
                                <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            </div>
                            <div class="controls controls-row">
                                <asp:Button ID="btnCheckAllNoticiationEvents" runat="server" Text="Select All Events" OnClick="btnCheckAllNoticiationEvents_Click" />
                                <asp:Button ID="btnUnCheckAllNoticiationEvents" runat="server" Text="Unselect All Events" OnClick="btnUnCheckAllNoticiationEvents_Click" />
                            </div>
                            <div style="text-align: right; font-size: 12px; line-height: 30px; padding-bottom: 0px">
                                <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="pnlBuyers" runat="server" Visible="true">
                            <div class="control-group">
                                <label for="inputBuyers" class="control-label">
                                    <asp:Label ID="Label2" runat="server" Text="Select Buyers:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span>
                                </label>
                            </div>

                            <div class="controls controls-row">
                                <div class="form-inline cblClass">
                                    <asp:CheckBoxList ID="cblBuyers" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="3" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                </div>
                            </div>
                            <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 0px">
                                <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            </div>
                            <div class="controls controls-row">
                                <asp:Label ID="lblNoBuyerSelected" runat="server" Visible="false"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:Label>
                                <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 0px">
                                    <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                                </div>

                                <asp:Button ID="btnSelectAllBuyers" runat="server" Text="Select All Buyers" OnClick="btnSelectAllBuyers_Click" />
                                <asp:Button ID="btnUnselectAllBuyers" runat="server" Text="Unselect All Buyers" OnClick="btnUnselectAllBuyers_Click" />
                            </div>


                            <div style="text-align: right; font-size: 12px; line-height: 30px; padding-bottom: 0px">
                                <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="pnlProductionUnits" runat="server" Visible="true">

                            <div class="control-group">
                                <label for="inputBuyers" class="control-label">
                                    <asp:Label ID="Label10" runat="server" Text="Select Production Units:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            </div>

                            <div class="controls controls-row">
                                <div class="form-inline cblClass">
                                    <asp:CheckBoxList ID="cblProductionUnits" runat="server" CellPadding="15" CellSpacing="15" RepeatColumns="2" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                </div>
                            </div>

                            <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 0px">
                                <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            </div>
                            <div class="controls controls-row">
                                <asp:Label ID="lblNoProdUnitSelected" runat="server" Visible="false"><span style="font-weight: 700; color: #CC0000">Please select a production unit.</span></asp:Label>
                            </div>
                            <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 0px">
                                <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            </div>


                            <div style="text-align: right; font-size: 12px; line-height: 10px; padding-bottom: 0px">

                                <span style="font-weight: 700; color: #CC0000">&nbsp;</span>
                            </div>

                        </asp:Panel>

                        <asp:Panel ID="pnlFinsihingUnits" runat="server" Visible="true">

                            <div class="control-group">
                                <label for="inputBuyers" class="control-label">
                                    <asp:Label ID="lblFinishingFloorMessage" runat="server" Text="Select Finishing Floors:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            </div>
                            <div class="controls controls-row" style="width: 70%">
                                <div class="form-inline cblClass">
                                    <asp:Repeater ID="rptFinishingUnits" runat="server" OnItemDataBound="rptFinishingUnits_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table1" class="table table-bordered table-hover">
                                                <thead>
                                                    <%-- <tr>
                                                                <th style="width: 120px">
                                                                    <asp:Label ID="lblUnitMessage" runat="server" Text="Units"></asp:Label></th>
                                                                <th style="width: 150px">
                                                                    <asp:Label ID="lblFinsingFloorMessage" runat="server" Text="Finishing Floor"></asp:Label></th>
                                                            </tr>--%>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="vertical-align: middle; width: 30%">
                                                    <asp:Label ID="lblFinishingUnitId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "FinishingUnitId")%>'></asp:Label><asp:Label ID="lblUnitName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FinishingUnitName")%>'></asp:Label></th></td>
                                                <td style="width: 70%">
                                                    <asp:CheckBoxList ID="cblFinishingUnitFloors" runat="server" RepeatColumns="3" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                                    </table>
                                        </FooterTemplate>
                                    </asp:Repeater>

                                </div>
                            </div>

                            <div class="controls controls-row">
                                <asp:Label ID="lblNoFinishingUnitSelected" runat="server" Visible="false"><span style="font-weight: 700; color: #CC0000">Please select a finishing unit floor.</span></asp:Label>
                            </div>
                        </asp:Panel>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label7" runat="server" Text="User Name"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxUserName" runat="server" placeholder="Enter a user name" CssClass="form-control" Width="50%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxUserName"><span style="font-weight: 700; color: #CC0000">Enter users name</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div id="DivPassword" runat="server" class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label8" runat="server" Text="Password"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxpassword" runat="server" placeholder="Enter password" CssClass="form-control" TextMode="Password" Width="50%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxpassword"><span style="font-weight: 700; color: #CC0000">Enter password</span></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div id="DivConPassword" runat="server" class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label5" runat="server" Text="Confirm Password"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxConfirmPass" runat="server" placeholder="Confirm password" CssClass="form-control" TextMode="Password" Width="50%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxConfirmPass"><span style="font-weight: 700; color: #CC0000">Confirm password</span></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="comparePassword" ControlToCompare="tbxpassword" ControlToValidate="tbxConfirmPass" runat="server" Display="Dynamic" ErrorMessage="Passwords do not match." ValidationGroup="save"></asp:CompareValidator>
                            </div>
                        </div>

                        <div class="clearfix"></div>



                        <div class="control-group">
                            <br />
                            <div class="controls-row">
                                <asp:Button ID="btnSave" runat="server" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" CssClass="btn btn-info pull-right" ValidationGroup="save" OnClick="btnSave_Click" />
                                <asp:Button ID="btnUpdate" runat="server" Visible="False" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="btnUpdate_Click" CssClass="btn btn-info pull-right" ValidationGroup="save" />
                            </div>
                        </div>


                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
