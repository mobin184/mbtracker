﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="EditDeletePermission.aspx.cs" Inherits="MBTracker.Pages.Admin_and_Setup.EditDeletePermission" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        input[type="radio"], input[type="checkbox"] {
            margin: -2px 5px 0px 0;
        }

        .form-horizontal .control-label {
            float: left;
            width: 30px;
            padding-top: 5px;
            text-align: right;
        }

        /* #cpMain_TreeView1 table {
            border: 1px solid #dee2e6;
            width: 100%;
            max-width: 100%;
            margin-bottom: 1rem;
            background-color: transparent;
        }
         #cpMain_TreeView1 table {
            border: 1px solid #dee2e6;
            width: 100%;
            max-width: 100%;
            margin-bottom: 1rem;
            background-color: transparent;
        }
         #cpMain_TreeView1 table td{
          border: 1px solid #dee2e6;
          padding: .75rem;
        }*/
    </style>
    <script>
        //Sys.Application.add_load(load);
    </script>
    <div class="row-fluid">
        <div class="span8">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>Setup Edit,Delete Permissions
                    </div>
                </div>
                <div class="widget-body">
                    <div class="form-horizontal no-margin">
                        <div class="form-horizontal">
                            <div class="control-group">
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                    <asp:Label ID="lblError" runat="server" Font-Bold="True" SkinID="message"></asp:Label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="Label5" runat="server" Text="Role"></asp:Label></label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlRole" DataTextField="RoleName" DataValueField="Id" runat="server" CssClass="form-control dropdown" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
 <%--                           <div class="control-group">
                                <label for="inputEmail3" class="control-label">
                                    <asp:Label ID="Label2" runat="server" Text="TaskList"></asp:Label></label>
                            </div>--%>
                            <div class="control-group">
                                <asp:Repeater ID="rptTasks" runat="server">
                                    <HeaderTemplate>
                                        <table id="data-table1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lblColorDesc1" runat="server" Text="Parent"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblColorDesc" runat="server" Text="Menu Item"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblColorDesc2" runat="server" Text="Edit Duration (Days)"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblColorDesc3" runat="server" Text="Delete Duration (Days)"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="text-align: left; vertical-align:middle">
                                                <asp:Label ID="lbl1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Path")%>'></asp:Label>
                                            </td>
                                            <td style="vertical-align: middle">
                                                <asp:Label ID="lblColorDesc" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "TextEng")%>'></asp:Label>
                                                <asp:Label ID="lblTaskId" Visible="false" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Id")%>'></asp:Label>
                                                <asp:Label ID="lblTaskParentId" Visible="false" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ParentId")%>'></asp:Label>
                                                <asp:Label ID="lblUrl" Visible="false" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "URL")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbxEditDuration" runat="server" TextMode="number" min="0" Style="min-height: 34px" Width="100%"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbxDeleteDuration" runat="server" TextMode="number" min="0" Style="min-height: 34px" Width="100%"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                            </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>


                            <div class="control-group">
                                <br />
                                <div class="controls-row">
                                    <%--<asp:Button ID="btnRefresh" runat="server" class="btn btn-info pull-right" Text="Refresh" OnClick="btnRefresh_Click" />--%>
                                    <asp:Button ID="btnSave" runat="server" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" CssClass="btn btn-info pull-right" ValidationGroup="save"
                                        OnClick="btnSave_Click" />
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--</ContentTemplate>--%>
    <%-- <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlRole" />
        </Triggers>--%>
    <%--</asp:UpdatePanel>--%>
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
