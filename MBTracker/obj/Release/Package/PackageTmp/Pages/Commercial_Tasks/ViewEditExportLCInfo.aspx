﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewEditExportLCInfo.aspx.cs" Inherits="MBTracker.Pages.Commercial_Tasks.ViewEditExportLCInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table-bordered thead:first-child tr:first-child th {
            vertical-align: middle;
        }

        .headcol {
            position: absolute;
            width: 5em;
            left: 0;
            top: auto;
            border-top-width: 1px;
            /*only relevant for first row*/
            margin-top: -1px;
            /*compensate for top border*/
        }

            .headcol:before {
                content: 'Row ';
            }
    </style>
    <div class="row-fluid">
        <div class="span6">
            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="View Export LC Information:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row-fluid" runat="server" visible="true" id="FilterDiv">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 12px; padding-bottom: 20px">
                        </div>
                        <div class="control-group">
                            <div class="col-md-6 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="col-md-11" style="padding-left: 0px">
                            <div class="col-md-12" style="padding-left: 0px">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="80%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="Label3" runat="server" Text="Or type LC number:"></asp:Label></label>
                                        <div class="controls controls-row">
                                            <asp:TextBox ID="tbxLCNumber" runat="server" Width="80%"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group" style="width: 70%">
                                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: right; float: right">
                                            <br />
                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="View LC Info" CssClass="btn btn-info pull-center" OnClick="btnSearch_Click" />
                                        </label>
                                        <div class="controls controls-row">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>

    <div class="row-fluid" runat="server">
        <div class="span12">

            <div class="span12">
                <div class="widget" id="dataDiv" runat="server" visible="false">
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">

                            <div class="control-group">
                                <label for="inputOrder" class="control-label" style="padding-bottom: 12px">
                                    <asp:Label runat="server" ID="lblLabelMessage" Text="Export LC/Sales Contract Information:" Font-Bold="true" Visible="false"></asp:Label>
                                </label>
                                <div class="controls controls-row" style="overflow-x: auto">
                                    <asp:Repeater ID="rptPISummary" runat="server">
                                        <HeaderTemplate>
                                            <table id="data-table1" class="table table-bordered table-hover" style="white-space: nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="Label2" runat="server" Text="LC/Sales Contract"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label15" runat="server" Text="Type"></asp:Label></th>
                                                        <th style="min-width: 100px">
                                                            <asp:Label ID="Label7" runat="server" Text="Date"></asp:Label></th>
                                                        <th style="min-width: 100px">
                                                            <asp:Label ID="Label11" runat="server" Text="Buyer"></asp:Label></th>
                                                        <th style="min-width: 150px">
                                                            <asp:Label ID="Label13" runat="server" Text="Issuing Bank & Branch"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label4" runat="server" Text="Tenor"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label12" runat="server" Text="Quantity"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label8" runat="server" Text="Amount"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label6" runat="server" Text="Commission"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Lien Bank"></asp:Label></th>
                                                        <th style="min-width: 150px">
                                                            <asp:Label ID="Label10" runat="server" Text="Commercial Concern"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label17" runat="server" Text="Amandment"></asp:Label></th>
                                                         <th>
                                                            <asp:Label ID="Label20" runat="server" Text="For Sales Contract"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label1" runat="server" Text="Status"></asp:Label></th>
                                                        <th style="min-width: 100px">
                                                            <asp:Label ID="Label16" runat="server" Text="Actions"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblPINumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCNumber")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="Label18" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Type")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblPIDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "LCDate"))%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblOrderSeason" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BuyerName")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblSeasonYear" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "IssuingBankAndBranch")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblBeneficiary" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Tenor")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="Label14" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCQuantity")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblItemBuyer" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCAmount")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblMerchandiser" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCCommission")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblMerchandiserLeader" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BankName")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblPIStatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CommercialConcern")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="Label19" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AmandmentNumber")%>'></asp:Label></td>
                                                 <td>
                                                    <asp:Label ID="Label21" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SalecContractNumber")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="Label9" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "StatusName")%>'></asp:Label></td>
                                                <td>
                                                    <%--<asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" Visible='<%# ViewState["editEnabled"]%>' CommandArgument='<%#Eval("PIId")%>' class="btn btn-success btn-mini hidden-phone" Text="Edit"></asp:LinkButton>--%>
                                                    <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#Eval("CanDelete")%>' CommandArgument='<%#Eval("LCId") %>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" Visible='<%# Eval("CanEdit")%>' CommandArgument='<%#Eval("LCId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>

                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                         </table>
                            <div class="clearfix">
                                        </FooterTemplate>
                                    </asp:Repeater>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="span6">
            </div>
        </div>
    </div>


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
