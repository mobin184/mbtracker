﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewEditLCInformation.aspx.cs" Inherits="MBTracker.Pages.Commercial_Tasks.ViewEditLCInformation" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid">
        <div class="span9">
            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="View LC Information:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">

                    <div class="row-fluid" runat="server" visible="true" id="FilterDiv">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 12px; padding-bottom: 20px">
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="col-md-12" style="padding-left: 0px">

                            <div class="col-md-6" style="padding-left: 0px">
                                <div class="form-horizontal">

                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="Label1" runat="server" Text="Select PI:"></asp:Label></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlPIs" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>




                                </div>
                            </div>


                            <div class="col-md-6">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="Label3" runat="server" Text="Or type L/C number:"></asp:Label></label>
                                        <div class="controls controls-row">
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                        </label>
                                        <div class="controls controls-row">
                                            <asp:TextBox ID="tbxLCNumber" runat="server" Width="100%"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">

                                            <br />
                                            <br />
                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="View LC Info" CssClass="btn btn-info pull-center" OnClick="btnSearch_Click" />

                                        </label>
                                        <div class="controls controls-row">
                                        </div>
                                    </div>

                                </div>
                            </div>




                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>


    <div class="row-fluid" runat="server">
        <div class="span12">

            <div class="span9">
                <div class="widget" id="dataDiv" runat="server" visible="false">
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">

                            <div class="control-group">
                                <label for="inputOrder" class="control-label" style="padding-bottom: 12px">
                                    <asp:Label runat="server" ID="lblLabelMessage" Text="LC Information:" Font-Bold="true" Visible="false"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rptLCSummary" runat="server" OnItemCommand="rptLCSummary_ItemCommand">
                                        <HeaderTemplate>
                                            <table id="data-table1" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="Label2" runat="server" Text="L/C Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label7" runat="server" Text="L/C Date"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label4" runat="server" Text="L/C Value"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label6" runat="server" Text="PI Number(s)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Style(s)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label9" runat="server" Text="Applicant Bank"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label16" runat="server" Text="Actions"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: left">
                                                    <asp:Label ID="lblLCNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCNumber")%>'></asp:Label>
                                                    <asp:Label ID="lblStatusValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "StatusName")%>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblLCTypeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCType")%>' Visible="false"></asp:Label>
                                                     <asp:Label ID="lblLCPurposeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCPurpose")%>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblCommercialConceredValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CommercialConceredName")%>' Visible="false"></asp:Label>

                                                    <asp:Label ID="lblShipmentMode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShipmentModeInfo")%>' Visible="false"></asp:Label>


                                                    <asp:Label ID="lblUDDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "UDDate"))%>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblUDStatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "[UDStatus]")%>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblETDDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "ETDDate"))%>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblETADate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "ETADate"))%>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblMaturityDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "MaturityDate"))%>' Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblLCDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "LCDate"))%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblLCAmount" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCAmount")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblLCPIs" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCPIs")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblLCStyles" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCStyles")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblAdvisingBank" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BankName")%>'></asp:Label>
                                                    <asp:Label ID="lblOtherInfo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OtherInfo")%>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblExportLCNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ExportLCNumber")%>' Visible="false"></asp:Label>

                                                </td>
                                                <td>
                                                    <%--<asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" Visible='<%# ViewState["editEnabled"]%>' CommandArgument='<%#Eval("LCId")%>' class="btn btn-success btn-mini hidden-phone" Text="Edit"></asp:LinkButton>--%>
                                                    <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" Visible='<%#Eval("CanEdit")%>' CommandArgument='<%#Eval("LCId")%>' class="btn btn-success btn-mini hidden-phone" Text="Edit"></asp:LinkButton>

                                                    <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="ViewDetails" CommandArgument='<%#Eval("LCId")%>' class="btn btn-success btn-mini hidden-phone" Text="View Details"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("LCId") %>' class="btn btn-danger btn-small hidden-phone" Text="Delete"></asp:LinkButton>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                         </table>
                            <div class="clearfix">
                                        </FooterTemplate>
                                    </asp:Repeater>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="span6">
            </div>
        </div>
    </div>



    <div class="row-fluid" runat="server" id="pnlDetails" visible="false">

        <div class="span9">
            <div class="widget">
                <div class="widget-body">
                    <div class="row-fluid" runat="server">
                        <div class="form-horizontal">

                            <div class="col-md-6">

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="lblLCNumber" runat="server" Text="L/C Number:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblLCNumberValue" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>


                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="Label12" runat="server" Text="LC Date:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblLCDateValue" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="Label19" runat="server" Text="LC Type:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblLCType" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>

                                 <div class="control-group" style="margin-bottom: 1px" id="divLCPurpose" runat="server">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="Label21" runat="server" Text="LC Purpose:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblLCPurpose" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>


                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="lblLCDate2" runat="server" Text="UD Date:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblUDDateValue" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="Label13" runat="server" Text="UD Status:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblUDStatusValue" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="Label15" runat="server" Text="ETD Date:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblETDDateValue" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="Label18" runat="server" Text="ETA Date:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblETADateValue" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>


                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="Label8" runat="server" Text="PI Information:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="Label11" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>


                            </div>

                            <div class="col-md-6">


                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="lblLCAmountMsg" runat="server" Text="L/C Value:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblLCAmountValue" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="lblAdvisingBank" runat="server" Text="Applicant Bank:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblAdvisingBankValue" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="Label14" runat="server" Text="Commercial Concerned:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblCommercialConcered" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="Label20" runat="server" Text="Shipment Mode:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblShipmentModeValue" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="lblLCStatusMessage" runat="server" Text="LC Status:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>




                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="Label17" runat="server" Text="Maturity Date:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblMaturityDateValue" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="lblOtherInfo" runat="server" Text="Other Info:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblOtherInfoValue" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                 <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                        <asp:Label ID="Label22" runat="server" Text="Export LC Number:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="elcNumber" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>


                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="Label10" runat="server" Text=""></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rptPISummary" runat="server" OnItemDataBound="rptPISummary_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table1" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr style="font-size: 10px; line-height: 1px;">

                                                        <th>
                                                            <asp:Label ID="Label2" runat="server" Text="PI Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label7" runat="server" Text="PI Date"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label10" runat="server" Text="Beneficiary"></asp:Label></th>
                                                        <%--<th>
                                                        <asp:Label ID="Label5" runat="server" Text="Style(s) in PI"></asp:Label></th>--%>
                                                        <th>
                                                            <asp:Label ID="Label6" runat="server" Text=""></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr style="font-size: 10px">
                                                <td style="vertical-align: middle">
                                                    <asp:Label ID="lblPINumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PINumber")%>'></asp:Label></td>
                                                <td style="vertical-align: middle">
                                                    <asp:Label ID="lblPIDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "PIDate"))%>'></asp:Label></td>
                                                <td style="vertical-align: middle">
                                                    <asp:Label ID="lblBeneficiaryName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BeneficiaryName")%>'></asp:Label></td>
                                                <%-- <td style="vertical-align: middle">
                                                <asp:Label ID="lblPIStyles" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PIStyles")%>'></asp:Label></td>--%>
                                                <td style="vertical-align: middle">

                                                    <asp:Repeater ID="rptPIItemInfo" runat="server" OnItemDataBound="rptPIItemInfo_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="data-table" class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <asp:Label ID="lblStyles" runat="server" Text="Style"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="lblOrders" runat="server" Text="PO"></asp:Label></th>
                                                                        <%-- <th>
                                                                            <asp:Label ID="lblItemTypes" runat="server" Text="Item Type"></asp:Label></th>--%>
                                                                        <th>
                                                                            <asp:Label ID="lblItems" runat="server" Text="Item Description"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="lblQty" runat="server" Text="Qty(Lbs)"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label></th>
                                                                        <th>
                                                                            <asp:Label ID="lblAmount" runat="server" Text="Amount(US$)"></asp:Label></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="lblStylesValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "StyleName")%>'></asp:Label>
                                                                </td>
                                                                <td style="width: 13%">
                                                                    <asp:Label ID="lblPOValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderNumber")%>'></asp:Label>
                                                                </td>
                                                                <%-- <td style="width: 15%">
                                                                    <asp:Label ID="lblItemTypeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RawMaterialItemTypeName")%>'></asp:Label>
                                                                </td>--%>
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="lblItemValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ItemName")%>'></asp:Label>
                                                                </td>
                                                                <td style="width: 10%; padding-top: 18px">
                                                                    <asp:Label ID="lblQtyValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity")%>'></asp:Label>
                                                                </td>
                                                                <td style="width: 12%; padding-top: 18px">
                                                                    <asp:Label ID="lblUnitPriceValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UnitPrice")%>'></asp:Label>
                                                                </td>
                                                                <td style="width: 10%; padding-top: 18px">
                                                                    <asp:Label ID="lblTotalAmount" Width="100%" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TotalAmount")%>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>


                                                            <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                                <asp:Label ID="lblTotal" runat="server" Text="Total:" Font-Bold="true"></asp:Label></td>
                                                            <%-- <td style="font-weight: 500; text-align: left; background-color: lavender"></td> --%>
                                                            <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                            <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                            <%--  <td style="font-weight: 500; text-align: left; background-color: lavender"></td>--%>
                                                            <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                                <asp:Label ID="lblTotalQuantity" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                                                            <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                            <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                                <asp:Label ID="lblGrandTotalAmount" runat="server" Text="" Font-Bold="true"></asp:Label></td>


                                                            </tbody>
                                 </table>
                                           
                                                        </FooterTemplate>
                                                    </asp:Repeater>

                                                    <div style="text-align: center">
                                                        <asp:Label ID="lblNoItemFound" runat="server" Visible="false" Text="No Item found." BackColor="#ffff00"></asp:Label>
                                                    </div>

                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                         </table>
                            <div class="clearfix">
                                        </FooterTemplate>
                                    </asp:Repeater>

                                </div>
                            </div>



                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>

        </div>

    </div>



</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
