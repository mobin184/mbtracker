﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewEditImportDocInfo.aspx.cs" Inherits="MBTracker.Pages.Commercial_Tasks.ViewEditImportDocInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <div class="row-fluid">
        <div class="span6">
            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="View Import Document Information:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row-fluid" runat="server" visible="true" id="FilterDiv">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 12px; padding-bottom: 20px">
                        </div>
                        <div class="control-group">
                            <div class="col-md-6 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="col-md-11" style="padding-left: 0px">
                            <div class="col-md-12" style="padding-left: 0px">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="lblBuyer" runat="server" Text="Select Beneficiary:"></asp:Label></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlBeneficiary" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlBeneficiary_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="Label3" runat="server" Text="Or type LC number:"></asp:Label></label>
                                        <div class="controls controls-row">
                                            <asp:TextBox ID="tbxLCNumber" runat="server" Width="100%"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: right; float: right">
                                            <br />
                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="View LC Info" CssClass="btn btn-info pull-center" OnClick="btnSearch_Click" />
                                        </label>
                                        <div class="controls controls-row">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>

    <div class="row-fluid" runat="server">
        <div class="span12">

            <div class="span12">
                <div class="widget" id="dataDiv" runat="server" visible="false">
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">

                            <div class="control-group">
                                <label for="inputOrder" class="control-label" style="padding-bottom: 12px">
                                    <asp:Label runat="server" ID="lblLabelMessage" Text="Import Document Information:" Font-Bold="true" Visible="false"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rptPISummary" runat="server">
                                        <HeaderTemplate>
                                            <table id="data-table1" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="Label11" runat="server" Text="Beneficiary"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label12" runat="server" Text="LC Number"></asp:Label></th>
                                                         <th>
                                                            <asp:Label ID="Label7" runat="server" Text="Doc Received Date"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label2" runat="server" Text="Chalan/BL Number"></asp:Label></th>
                                                           <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Chalan/BL Date"></asp:Label></th>                                                       
                                                        <th>
                                                            <asp:Label ID="Label13" runat="server" Text="Document Value"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label4" runat="server" Text="Document Qty"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label8" runat="server" Text="Payment Date"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label15" runat="server" Text="Payment Status"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label16" runat="server" Text="Actions"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblOrderSeason" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BeneficiaryName")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblPINumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCNumber")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="Label17" runat="server" Text='<%#  String.Format("{0:dd-MMM-yyyy}",DataBinder.Eval(Container.DataItem, "DocReceiveDate"))%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="Label6" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ChalanOrBLNumber")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblPIDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "ChalanOrBLDate"))%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblSeasonYear" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DocumentValue")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblBeneficiary" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DocumentQty")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblItemBuyer" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "PaymentDate"))%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="Label18" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentStatusName")%>'></asp:Label></td>
                                                <td>
                                                    <%--<asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" Visible='<%# ViewState["editEnabled"]%>' CommandArgument='<%#Eval("PIId")%>' class="btn btn-success btn-mini hidden-phone" Text="Edit"></asp:LinkButton>--%>
                                                    <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#Eval("CanDelete")%>' CommandArgument='<%#Eval("ImportDocumentId") %>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" Visible='<%# Eval("CanEdit")%>' CommandArgument='<%#Eval("ImportDocumentId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnEdit_Command" Text="Edit"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnViewDetails" runat="server" CommandName="ViewDetails" CommandArgument='<%#Eval("ImportDocumentId")%>' class="btn btn-success btn-mini hidden-phone" OnCommand="lnkbtnViewDetails_Command" Text="View Details"></asp:LinkButton>

                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                         </table>
                            <div class="clearfix">
                                        </FooterTemplate>
                                    </asp:Repeater>

                                </div>
                            </div>

                            <div class="clearfix"></div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="span12" style="margin-left: 0px;">
                <asp:Label ID="lblDetailsNotFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>
                <div class="row-fluid" runat="server" id="pnlDetails" visible="false">
                    <div class="span6">
                        <div class="widget">
                            <div class="widget-body">
                                <label for="inputOrder" class="control-label" style="padding-bottom: 12px">
                                    <asp:Label runat="server" ID="Label10" Text="Import document detail information:" Font-Bold="true"></asp:Label>
                                </label>
                                <div class="row-fluid" runat="server">
                                    <div class="form-horizontal">
                                         <%--<div class="control-group" style="margin-bottom: 1px">
                                            <label for="inputBuyer" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                                <asp:Label ID="lblOrderSeason" runat="server" Text="Doc Number:"></asp:Label></label>
                                            <div class="controls controls-row">
                                                <label for="inputBuyer" class="control-label" style="text-align: left; line-height: 12px; font-size: 12px; padding-top: 5px">
                                                    <asp:Label ID="lblDocNumber" runat="server" Text=""></asp:Label></label>
                                            </div>
                                        </div>--%>
                                        <div class="control-group" style="margin-bottom: 1px">
                                            <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                                <asp:Label ID="lblPINumber" runat="server" Text="Beneficiary:"></asp:Label></label>
                                            <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                                <asp:Label ID="lblBeneficiary" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                        <div class="control-group" style="margin-bottom: 1px">
                                            <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                                <asp:Label ID="lblPIDate2" runat="server" Text="LC Number:"></asp:Label></label>
                                            <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                                <asp:Label ID="lblLCNumber" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>    
                                        <div class="control-group" style="margin-bottom: 1px">
                                            <label for="inputBuyer" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                                <asp:Label ID="Label14" runat="server" Text="Doc Receive Date:"></asp:Label></label>
                                            <div class="controls controls-row">
                                                <label for="inputBuyer" class="control-label" style="text-align: left; line-height: 12px; font-size: 12px; padding-top: 5px">
                                                    <asp:Label ID="lblDocReceiveDate" runat="server" Text=""></asp:Label></label>
                                            </div>
                                        </div>
                                        <div class="control-group" style="margin-bottom: 1px">
                                            <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                                <asp:Label ID="Label9" runat="server" Text="Chalan/BL Number:"></asp:Label></label>
                                            <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                                <asp:Label ID="lblChalanOrBLNumber" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>    
                                        <div class="control-group" style="margin-bottom: 1px">
                                            <label for="inputBuyer" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                                <asp:Label ID="Label12" runat="server" Text="Chalan/BL Date:"></asp:Label></label>
                                            <div class="controls controls-row">
                                                <label for="inputBuyer" class="control-label" style="text-align: left; line-height: 12px; font-size: 12px; padding-top: 5px">
                                                    <asp:Label ID="lblChalanOrBLDate" runat="server" Text=""></asp:Label></label>
                                            </div>
                                        </div>
                                        <div class="control-group" style="margin-bottom: 1px">
                                            <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                                <asp:Label ID="Label1" runat="server" Text="Document Value:"></asp:Label></label>
                                            <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                                <asp:Label ID="lblDocumentValue" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                        <div class="control-group" style="margin-bottom: 1px">
                                            <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                                <asp:Label ID="Label20" runat="server" Text="Document Qty:"></asp:Label></label>
                                            <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                                <asp:Label ID="lblDocumentQty" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                        <div class="control-group" style="margin-bottom: 1px">
                                            <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                                <asp:Label ID="Label22" runat="server" Text="Payment Date:"></asp:Label></label>
                                            <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                                <asp:Label ID="lblPaymentDate" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                       <div class="control-group" style="margin-bottom: 1px">
                                            <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                                <asp:Label ID="Label19" runat="server" Text="Payment Status:"></asp:Label></label>
                                            <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                                <asp:Label ID="lblPaymentStatus" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="control-group">
                                            <label for="inputOrder" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                                <asp:Label ID="lblPIEntry" runat="server" Text="Invoice Info:"></asp:Label></label>
                                            <div class="controls controls-row" style="overflow-x: auto">
                                                <asp:Repeater ID="rptInvoices" runat="server">
                                                    <HeaderTemplate>
                                                        <table id="data-table" class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr style="font-size: 10px; line-height: 1px;">
                                                                    <th style="padding-top: 0px">
                                                                        <asp:Label ID="lblStyles" runat="server" Text="Invoice Number"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblOrders" runat="server" Text="Invoice Date"></asp:Label></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr style="font-size: 10px">
                                                            <td>
                                                                <asp:Label ID="lblStylesValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "InvoiceNumber")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblPOValue" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}",DataBinder.Eval(Container.DataItem, "InvoiceDate"))%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                 </table>                                           
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
