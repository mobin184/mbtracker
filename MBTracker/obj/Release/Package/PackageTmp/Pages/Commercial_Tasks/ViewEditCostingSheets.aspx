﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewEditCostingSheets.aspx.cs" Inherits="MBTracker.Pages.Commercial_Tasks.ViewEditCostingSheets" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .table th, .table td {
            padding: 4px;
            line-height: 10px;
            text-align: center;
            vertical-align: top;
            border-top: 1px solid #e0e0e0;
        }
    </style>

    <div class="row-fluid">
        <div class="span6" id="divSearchCriteria" runat="server" style="min-height: 0px;">
            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="View Costing Information:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">

                    <div class="row-fluid" runat="server" visible="true" id="FilterDiv">
                        <div class="pull-right" style="text-align: left; font-size: 18px; line-height: 10px; padding-bottom: 20px">
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="col-md-10" style="padding-left: 0px">

                            <div class="col-md-9" style="padding-left: 0px">
                                <div class="form-horizontal">

                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="ddlStyles"><span style="font-weight: 700; color: #CC0000">Please select the style.</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="control-group" style="display:none;">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="lblOrder" runat="server" Text="Select Order:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>

                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">
                                            <asp:Label ID="Label8" runat="server" Text=""></asp:Label></label>
                                        <div class="controls controls-row" style="margin-left: 150px">
                                            <br />
                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="View Costing Info" CssClass="btn btn-info pull-left" OnClick="btnSearch_Click" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="row-fluid" runat="server">
        <div class="span12">

            <div class="span9" style="min-height: 0px;">
                <div class="widget" id="dataDiv" runat="server" visible="false" style="margin-bottom: 2px">
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label" style="padding-bottom: 10px">
                                    <asp:Label runat="server" ID="lblLabelMessage" Text="Spare Received:" Font-Bold="true" Visible="false"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rpt" runat="server" OnItemCommand="rpt_ItemCommand">
                                        <HeaderTemplate>
                                            <table id="data-table1" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="Label2" runat="server" Text="Order Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label7" runat="server" Text="Order Season"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Order Qty"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label6" runat="server" Text="Delivery Date"></asp:Label></th>
                                                        <%-- <th>
                                                            <asp:Label ID="Label4" runat="server" Text="LC Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label1" runat="server" Text="LC Value"></asp:Label></th>--%>
                                                        <th>
                                                            <asp:Label ID="Label3" runat="server" Text="FOB Price"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label1" runat="server" Text="Item Description"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label9" runat="server" Text="Garments Weight"></asp:Label></th>
                                                         <th>
                                                            <asp:Label ID="Label13" runat="server" Text="Status"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label16" runat="server" Text="Actions"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblOrderNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderNumber")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblOrderSeason" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderSeason")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblOrderQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderQty")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblDeliveryDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "DeliveryDate"))%>'></asp:Label></td>
                                                <%-- <td>
                                                   

                                                <td>
                                                   </td>--%>
                                                <td>
                                                    <asp:Label ID="lblFOBPrice" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FOBPrice")%>'></asp:Label>
                                                    <asp:Label ID="lblLCNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCNumber")%>' Visible="false"></asp:Label></td>
                                                    <asp:Label ID="lblLCValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LCValue")%>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblCommissionOrCharge" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CommissionOrCharge")%>' Visible="false"></asp:Label>
                                                </td>
                                                 <td>
                                                     <asp:Label ID="lblItemDescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ItemDescription")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblGarmentsWt" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "GarmentsWeight")%>'></asp:Label></td>
                                                 <td>
                                                    <asp:Label ID="lblStatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CostingSheetStatus")%>'></asp:Label></td>
                                                <td>
                                                    <%--<asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete"  Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("KnittingUnitId")+","+ Eval("KnittingDate")+","+ Eval("ShiftId")%>' class="btn btn-danger btn-mini hidden-phone" OnCommand="lnkbtnDelete_Command" Text="Delete"></asp:LinkButton>--%>
                                                    <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit"  Visible='<%#ViewState["editEnabled"]%>' CommandArgument='<%#Eval("CostingInfoId")%>' class="btn btn-success btn-mini hidden-phone" Text="Edit"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="ViewDetails" CommandArgument='<%#Eval("CostingInfoId")%>' class="btn btn-success btn-mini hidden-phone" Text="View Details"></asp:LinkButton>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                         </table>
                            <div class="clearfix">
                                        </FooterTemplate>
                                    </asp:Repeater>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

<%--            <div class="span6">
            </div>--%>
        </div>
    </div>
    <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>



    <div class="row-fluid" runat="server">
        <div class="span6">

            <label for="inputOrder" style="text-align: center">
                <asp:Label ID="lblCostingSheetTitle" runat="server" Text="Costing Sheet - Masihata Sweater" Font-Bold="true" Font-Size="Large" Visible="false"></asp:Label>
            </label>

        </div>

    </div>


    <div class="row-fluid" runat="server" id="pnlDetails" visible="false">



        <div class="span6">
            <div class="widget">
                <div class="widget-body">
                    <div class="row-fluid" runat="server">
                        <div class="form-horizontal">



                            <div class="col-md-6">

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="Label11" runat="server" Text="Buyer:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblBuyerValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="Label4" runat="server" Text="Style:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblStyleValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="lblOrderNumber" runat="server" Text="Order Number:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblOrderNumberValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="lblOrderSeason" runat="server" Text="Order Season:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label"style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblOrderSeasonValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>
                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="lblOrderQty2" runat="server" Text="Order Quantity:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblOrderQtyValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="lblDeliveryDate" runat="server" Text="Delivery Date:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblDeliveryDateValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>


                            </div>
                            <div class="col-md-6">

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="Label10" runat="server" Text="Item:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblItemValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="lblGarmentsWeight" runat="server" Text="Garments Weight:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblGarmentsWeightValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="lblLCNumberMsg" runat="server" Text="LC Number:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblLCNumberValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>
                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="lblLCValue" runat="server" Text="LC Value:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblLCValueValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>
                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="lblFOBPrice2" runat="server" Text="FOB Price:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblFOBPriceValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="Label14" runat="server" Text="Commission/Charge(%):"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblCommissionCharge" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>


                            </div>



                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="row-fluid" runat="server" id="Div1" visible="false">
        <div class="span6">

            <div class="widget">
                <div class="widget-body">

                    <div id="dt_example" class="example_alt_pagination">
                        <div class="control-group">
                            <label for="inputOrder" class="control-label" style="line-height: 18px">
                                <asp:Label ID="lblDetails" runat="server" Text="Material Requirements and Costing Analysis:" Font-Bold="true"></asp:Label></label>
                            <div class="controls controls-row" style="width: 100%; overflow-x: auto">

                                <asp:Repeater ID="rptCostingItemInfo" runat="server" Visible="false">
                                    <HeaderTemplate>
                                        <table id="data-table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr style="font-size:9px;line-height: 1px;">
                                                    <th style="padding: 8px">
                                                        <asp:Label ID="lblItemType" runat="server" Text="Item<br/> Type"></asp:Label></th>
                                                   <th style="padding: 8px">
                                                        <asp:Label ID="lblDescription" runat="server" Text="Item<br/>Description"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lblOrderQty" runat="server" Text="Order Qty<br/> (in Pcs)"></asp:Label></th>
                                                   <th style="padding: 8px">
                                                        <asp:Label ID="lblConsumption" runat="server" Text="Consumption<br/>(Per Dzn)"></asp:Label></th>
                                                    <th style="padding: 8px">
                                                        <asp:Label ID="lblWastage" runat="server" Text="Wastage<br/>(%)"></asp:Label></th>
                                                    <th style="padding: 8px">
                                                        <asp:Label ID="lblBookingQty" runat="server" Text="Booking Qty<br/>(Per Dzn)"></asp:Label></th>
                                                    <th style="padding: 8px">
                                                        <asp:Label ID="lblConfirmedPrice" runat="server" Text="Confirmed <br/> Price"></asp:Label></th>
                                                     <th style="padding: 8px">
                                                        <asp:Label ID="lblPricePerDzn" runat="server" Text="Price<br/>(Per Dzn)"></asp:Label></th>
                                                    <th style="padding: 8px">
                                                        <asp:Label ID="lblSupplier" runat="server" Text="Item<br/>Supplier"></asp:Label></th>
                                                     <th style="padding: 8px">
                                                        <asp:Label ID="lblTotalValue" runat="server" Text="Total Value<br/>($)"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr style="font-size:9px">
                                            <td style="text-align: left;padding: 4px"><%#Eval("[RawMaterialItemTypeName]") %></td>
                                            <td style="text-align: left;padding: 4px"><%#Eval("ItemDescription") %></td>
                                            <td  style="text-align:center;padding: 4px"><%#Eval("OrderQty") %></td>
                                            <td style="text-align: center;padding: 4px"><%#Eval("ConsumptionPerDzn") %></td>
                                            <td style="text-align: center;padding: 4px"><%#Eval("WastagePercentage") %></td>
                                            <td style="text-align: center;padding: 4px"><%#Eval("BookingQtyPerDzn") %></td>
                                            <td style="text-align: center;padding: 4px"><%#Eval("ConfirmedPrice") %></td>

                                            <td style="text-align: center;padding: 4px"><%#Eval("PricePerDzn") %></td>
                                            <td style="text-align: center;padding: 4px"><%#Eval("SupplierName") %></td>
                                            <td style="text-align: center;padding: 4px"><%#Eval("TotalValue") %></td>

                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                 </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    </div>



    <div class="row-fluid" runat="server" id="pnlSummary" visible="false">

        <div class="span3">
            <div class="widget">
                <div class="widget-body">
                    <div class="row-fluid" runat="server">
                        <div class="form-horizontal">

                            <div class="col-md-12" >

                                <label for="inputBuyer" style="padding-bottom: 5px; text-align: center;line-height:10px">
                                    <asp:Label ID="Label12" runat="server" Text="Summary of Cost Per Dzn:" Font-Bold="true" Font-Underline="true"></asp:Label>
                                </label>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="Label25" runat="server" Text="FOB Per Dzn:"></asp:Label></label>
                                    <div class="controls controls-row" style="margin-bottom: 0px">
                                        <label for="inputBuyer" class="control-label" style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblFobPerDzn" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="Label27" runat="server" Text="Commission or Charge(%):"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblCommissionOrChargeValue" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>

                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="Label29" runat="server" Text="Yarn Cost:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblYarnCostPerDzn" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>
                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="Label31" runat="server" Text="Acc. Cost:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblAccCostPerDzn" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>
                                <div class="control-group" style="margin-bottom: 1px">
                                    <label for="inputBuyer" class="control-label" style="margin-bottom: 0px;line-height:10px;font-size:10px">
                                        <asp:Label ID="Label33" runat="server" Text="CM:"></asp:Label></label>
                                    <div class="controls controls-row">
                                        <label for="inputBuyer" class="control-label" style="text-align: left;margin-bottom: 0px;line-height:10px;font-size:10px">
                                            <asp:Label ID="lblCMPerDzn" runat="server" Text=""></asp:Label></label>
                                    </div>
                                </div>

                            </div>



                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>

        </div>

    </div>


<%--    <asp:LinkButton ID="lkbtnPrint" runat="server"  OnClick="lkbtnPrint_Click" Visible="false">Printable Version</asp:LinkButton>--%>

    <asp:Button ID="btnPrint" runat="server" ValidationGroup="save" Text="Printable Version" Visible="false" CssClass="btn btn-success btn-mini pull-left" OnClick="btnPrint_Click" />


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
