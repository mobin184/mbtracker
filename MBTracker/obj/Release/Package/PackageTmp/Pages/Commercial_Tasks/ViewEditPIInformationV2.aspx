﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewEditPIInformationV2.aspx.cs" Inherits="MBTracker.Pages.Commercial_Tasks.ViewEditPIInformationV2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row-fluid">
        <div class="span9">
            <div class="widget">

                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="lblTitle" Text="View PI Information:"></asp:Label>
                    </div>
                </div>

                <div class="widget-body">

                    <div class="row-fluid" runat="server" visible="true" id="FilterDiv">
                        <div class="pull-right" style="text-align: left; font-size: 12px; line-height: 12px; padding-bottom: 20px">
                        </div>
                        <div class="control-group">
                            <div class="col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                            </div>
                        </div>
                        <div class="col-md-12" style="padding-left: 0px">

                            <div class="col-md-6" style="padding-left: 0px">
                                <div class="form-horizontal">

                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="lblStyle" runat="server" Text="Select Style:"></asp:Label></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="FromDate" runat="server" Text="From Date:"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxFromDate" runat="server" placeholder="From date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                    
                                </div>
                            </div>

                                    <%--<div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="Label1" runat="server" Text="Select PI:"></asp:Label></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlPIs" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>--%>
                                </div>
                            </div>


                            <div class="col-md-6">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                            <asp:Label ID="Label3" runat="server" Text="Or type PI number:"></asp:Label></label>
                                        <div class="controls controls-row">
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="inputBuyer" class="control-label">
                                        </label>
                                        <div class="controls controls-row">
                                            <asp:TextBox ID="tbxPINumber" runat="server" Width="100%"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="ToDate" runat="server" Text="To Date:"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:TextBox ID="tbxToDate" runat="server" placeholder="From date" Width="100%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                    
                                </div>
                            </div>


                                    <div class="control-group">
                                        <label for="inputEmail3" class="control-label">
                                            <asp:Label ID="Label14" runat="server" Text="Status:"></asp:Label></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control" Width="70%">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="inputBuyerName" class="control-label" style="width: 150px; text-align: left">

                                            <br />
                                            <br />
                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="save" Text="View PI Info" CssClass="btn btn-info pull-center" OnClick="btnSearch_Click" />

                                        </label>
                                        <div class="controls controls-row">
                                        </div>
                                    </div>

                                    

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <asp:Label ID="lblNoDataFound" runat="server" Visible="false" Text="No data found." BackColor="#ffff00"></asp:Label>


    <div class="row-fluid" runat="server">
        <div class="span12">

            <div class="span12">
                <div class="widget" id="dataDiv" runat="server" visible="false">
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">

                            <div class="control-group">
                                <label for="inputOrder" class="control-label" style="padding-bottom: 12px">
                                    <asp:Label runat="server" ID="lblLabelMessage" Text="PI Information:" Font-Bold="true" Visible="false"></asp:Label>
                                </label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rptPISummary" runat="server" OnItemCommand="rptPISummary_ItemCommand">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <!--<th>
                                                           <asp:Label ID="lblSelect" runat="server" Text="Select Item"></asp:Label></th>-->
                                                        <th>
                                                            <asp:Label ID="Label2" runat="server" Text="PI Number"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label7" runat="server" Text="PI Date"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label11" runat="server" Text="Season"></asp:Label></th>
                                                         <th>
                                                            <asp:Label ID="Label13" runat="server" Text="Season Year"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label4" runat="server" Text="Beneficiary"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label8" runat="server" Text="Factory"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label6" runat="server" Text="Merchandiser"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label5" runat="server" Text="Merchandiser Leader"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label18" runat="server" Text="Waiting For"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label10" runat="server" Text="Status"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="Label16" runat="server" Text="Actions"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                               <!--<td style="text-align: center; vertical-align: middle" width="10%">
                                                   <asp:CheckBox ID="LinkButton1" runat="server" CommandName="Edit" CommandArgument=''></asp:CheckBox>
                                               </td>-->
                                                <td>
                                                    <asp:Label ID="lblPINumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PINumber")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblPIDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "PIDate"))%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblOrderSeason" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderSeason")%>'></asp:Label></td>
                                                 <td>
                                                    <asp:Label ID="lblSeasonYear" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SeasonYear")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblBeneficiary" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Beneficiary")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblItemBuyer" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ItemBuyer")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblMerchandiser" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Merchandiser")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblMerchandiserLeader" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MerchandiserLeader")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="Label19" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "WaitingForApprove")%>'></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblPIStatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PIStatus")%>'></asp:Label></td>
                                                <td>
                                                    <%--<asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" Visible='<%# ViewState["editEnabled"]%>' CommandArgument='<%#Eval("PIId")%>' class="btn btn-success btn-mini hidden-phone" Text="Edit"></asp:LinkButton>--%>
                                                    <asp:LinkButton ID="lnkbtnCopy" runat="server" CommandName="Copy" Visible='<%#ViewState["copyEnabled"]%>' CommandArgument='<%#Eval("PIId")%>' class="btn btn-warning btn-mini hidden-phone" Text="Copy"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" Visible='<%# Eval("CanEdit")%>' CommandArgument='<%#Eval("PIId")%>' class="btn btn-success btn-mini hidden-phone" Text="Edit"></asp:LinkButton>

                                                    <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="ViewDetails" CommandArgument='<%#Eval("PIId")%>' class="btn btn-success btn-mini hidden-phone" Text="View Details"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" Visible='<%#ViewState["deleteEnabled"]%>' CommandArgument='<%#Eval("PIId")%>' class="btn btn-danger btn-mini hidden-phone" Text="Delete"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnStatusChange" runat="server" CommandName="StatusChange" Visible='<%#ViewState["StatusChange"]%>'  CommandArgument='<%#Eval("PIId")%>' class="btn btn-warning btn-mini hidden-phone" Text="Status"></asp:LinkButton>
                                                    </td>

                                            </tr>
                                           
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            
                                            
                                            </tbody>
                                            
                         </table>
                                            
                            <div class="clearfix"></div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                   <!--<asp:LinkButton ID="ExcelExport" runat="server" CommandName="ExcelExport" class="btn btn-success btn-mini hidden-phone" Text="Export Excel" OnClick="ExcelExport_Click" ></asp:LinkButton>-->
                                    <!--<button id="ExcelExport" onclick="getAllDataforExcel" type="submit" <%--onclick="getAllSeclectedID();"--%> class="btn btn-success btn-mini hidden-phone" style="float: left;">Export Selected</button>-->
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="span6">
            </div>
        </div>
    </div>


    <div class="row-fluid" runat="server" id="pnlDetails" visible="false">

        <div class="span9">
            <div class="widget">
                <div class="widget-body">
                    <div class="row-fluid" runat="server">
                        <div class="form-horizontal">


                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="lblPINumber" runat="server" Text="PI Number:"></asp:Label></label>
                                <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                    <asp:Label ID="lblPINumberValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="lblPIDate2" runat="server" Text="PI Date:"></asp:Label></label>
                                <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                    <asp:Label ID="lblPIDateValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputBuyer" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="lblOrderSeason" runat="server" Text="Season:"></asp:Label></label>
                                <div class="controls controls-row">
                                    <label for="inputBuyer" class="control-label" style="text-align: left; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblOrderSeasonValue" runat="server" Text=""></asp:Label></label>
                                </div>
                            </div>
                             <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputBuyer" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="Label12" runat="server" Text="Season Year:"></asp:Label></label>
                                <div class="controls controls-row">
                                    <label for="inputBuyer" class="control-label" style="text-align: left; line-height: 12px; font-size: 12px; padding-top: 5px">
                                        <asp:Label ID="lblSeasonYearValue" runat="server" Text=""></asp:Label></label>
                                </div>
                            </div>

                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="Label1" runat="server" Text="Item Order Month:"></asp:Label></label>
                                <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                    <asp:Label ID="lblItemOrderDate" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            

                            <br />

                            <div class="control-group">
                                <label for="inputOrder" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="lblPIEntry" runat="server" Text="Item Info:"></asp:Label></label>
                                <div class="controls controls-row" style="overflow-x: auto">
                                    <asp:Repeater ID="rptPIItemEntryInfo" runat="server" OnItemDataBound="rptPIItemEntryInfo_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="data-table2" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr style="font-size: 10px; line-height: 1px;">
                                                        <th style="padding-top:0px">
                                                            <asp:Label ID="lblStyles" runat="server" Text="Style <br/>"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblOrders" runat="server" Text="PO <br/>"></asp:Label></th>
                                                        <%--<th>
                                                            <asp:Label ID="lblItemTypes" runat="server" Text="Item Type"></asp:Label></th>--%>
                                                        <th>
                                                            <asp:Label ID="lblItems" runat="server" Text="Item <br/>"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblHSCode" runat="server" Text="HS Code <br/>"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblQty" runat="server" Text="Qty (lbs<br/> or pc)"></asp:Label></th>
                                                        <th>
                                                                <asp:Label ID="lblUnit" runat="server" Text="Unit"></asp:Label></th>
                                                            
                                                        <th>
                                                            <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price<br/> ($/lbs or $/pc)"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lblAmount" runat="server" Text="Amount <br/>($)"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr style="font-size: 10px">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblStylesValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "StyleName")%>'></asp:Label>
                                                </td>
                                                <td style="width: 10%">
                                                    <asp:Label ID="lblPOValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderNumber")%>'></asp:Label>
                                                </td>
                                                     <td style="width: 25%">
                                                         <asp:Label ID="lblItemValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ItemName")%>'></asp:Label>
                                                     </td>
                                                <td style="width: 10%; padding-top: 18px">
                                                    <asp:Label ID="lblHSCodeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "HSCode")%>'></asp:Label>
                                                </td>
                                                <td style="width: 5%; padding-top: 18px">
                                                    <asp:Label ID="lblQtyValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity")%>'></asp:Label>
                                                </td>
                                                 <td style="width: 5%">
                                                         <asp:Label ID="Label15" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UnitName")%>'></asp:Label>
                                                     </td>
                                               
                                                <td style="width: 10%; padding-top: 18px">
                                                    <asp:Label ID="lblUnitPriceValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UnitPrice")%>'></asp:Label>
                                                </td>
                                                
                                                <td style="width: 5%; padding-top: 18px">
                                                    <asp:Label ID="lblTotalAmount" Width="100%" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TotalAmount")%>'></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>


                                            <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                <asp:Label ID="lblTotal" runat="server" Text="Total:" Font-Bold="true"></asp:Label></td>
                                            <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                            <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                            <%--  <td style="font-weight: 500; text-align: left; background-color: lavender"></td>--%>
                                            <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                            <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                <asp:Label ID="lblTotalQuantity" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                                            <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                             <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                            <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                <asp:Label ID="lblGrandTotalAmount" runat="server" Text="" Font-Bold="true"></asp:Label></td>


                                            </tbody>
                                 </table>
                                           
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>

                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="lblBeneficiaryName" runat="server" Text="Beneficiary:"></asp:Label></label>
                                <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                    <asp:Label ID="lblBeneficiaryNameValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="lblNominatedOrNotName" runat="server" Text="Nominated Or Not:"></asp:Label></label>
                                <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                    <asp:Label ID="lblNominatedOrNotNameValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="lblBeneficiaryPhone" runat="server" Text="Beneficiary Phone:"></asp:Label></label>
                                <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                    <asp:Label ID="lblBeneficiaryPhoneValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="lblBeneficiaryBank" runat="server" Text="Beneficiary Bank:"></asp:Label></label>
                                <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                    <asp:Label ID="lblBeneficialyBankInfo" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="lblTenor" runat="server" Text="Payment Mode:"></asp:Label></label>
                                <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                    <asp:Label ID="lblTenorValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>


                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="lblPaymentMode" runat="server" Text="Payment Remarks:"></asp:Label></label>
                                <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                    <asp:Label ID="lblPaymentModeValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="lblShipment" runat="server" Text="Shipment Info:"></asp:Label></label>
                                <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                    <asp:Label ID="lblShipmentInfoValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="lblPartialShipment" runat="server" Text="Partial Shipment:"></asp:Label></label>
                                <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                    <asp:Label ID="lblPartialShipmentValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="lblPIType" runat="server" Text="PI Type:"></asp:Label></label>
                                <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                    <asp:Label ID="lblPITypeValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>



                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="lblTolarance" runat="server" Text="Tolerance Info:"></asp:Label></label>
                                <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                    <asp:Label ID="lblTolaranceValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>




                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="lblOthers" runat="server" Text="Remarks:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                    <asp:Label ID="lblOthersValue" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="Label17" runat="server" Text="Approved Date By Md Sir:"></asp:Label></label>
                                <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                    <asp:Label ID="lblApprovedDateByMdSir" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}")%>'></asp:Label>
                                </div>
                            </div>

                            <div class="control-group" style="margin-bottom: 1px">
                                <label for="inputEmail3" class="control-label" style="margin-bottom: 0px; line-height: 12px; font-size: 12px">
                                    <asp:Label ID="Label9" runat="server" Text="Status:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                <div class="controls controls-row" style="margin-bottom: 0px; line-height: 12px; font-size: 12px; padding-top: 5px">
                                    <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>

        </div>

    </div>





</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">

    <script>
        function getAllSeclectedID() {
            //alert("Hello! I am an alert box!!");
            var pId = "";
            var total = 0;
            $("input[name='ExportCheckbox']").each(function () {
                if (!$(this).is(":checked")) {

                } else {
                    total = total + 1;
                    if (total > 1) {
                        pId = pId + "," + $(this).val();
                        //alert(pId);
                    } else {
                        pId = pId + $(this).val();
                    }

                }
            });
            //allPID = pId;
            alert(pId);
            debugger;

           // var postData = {
             //   Fname: fname
           // };
            //debugger;
            $.ajax({
                type: 'POST',
                url: 'ViewEditPIInformationV2.aspx/update',
                contentType: 'application/json',
                data: JSON.stringify(pId),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d) {
                        alert("Successfully added new item");
                    }
                },


            });
        };
    </script>>

</asp:Content>




