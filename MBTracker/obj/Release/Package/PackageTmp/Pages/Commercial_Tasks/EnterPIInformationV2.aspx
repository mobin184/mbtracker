﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="EnterPIInformationV2.aspx.cs" Inherits="MBTracker.Pages.Commercial_Tasks.EnterPIInformationV2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        input[type="radio"], input[type=checkbox] {
            margin: -2px 0 0;
        }


        .cblClass label {
            /*display: block;*/
            padding-left: 2px !important;
            padding-right: 14px !important;
        }
    </style>

    <asp:Panel ID="pnlNonEdit1" runat="server">

        <div class="row-fluid">

            <div class="span6">
                <div class="widget">
                    <div class="widget-header">
                        <div class="title">
                            <span class="fs1" aria-hidden="true" data-icon=""></span>
                            <asp:Label runat="server" ID="actionTitle" Text="Enter PI Information:"></asp:Label>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="form-horizontal">
                            <div class="pull-right" style="text-align: center; font-size: 12px; line-height: 20px; padding-bottom: 5px">
                                <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                            </div>
                            <div class="control-group">
                                <div class="col-md-12 col-sm-12">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                </div>
                            </div>

                            <div class="control-group" style="padding-bottom: 30px">
                                <label for="inputBuyer" class="control-label">
                                    <asp:Label ID="lblBuyer" runat="server" Text="Select Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                <div class="controls controls-row">
                                    <asp:DropDownList ID="ddlBuyers" runat="server" AutoPostBack="true" Display="Dynamic" Width="60%" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                        ControlToValidate="ddlBuyers"><span style="font-weight: 700; color: #CC0000">Please select a buyer.</span></asp:RequiredFieldValidator>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <br />
        <br />

        <div class="row-fluid">
            <div id="divPIOtherInfo" runat="server" visible="false">

                <div class="span12">

                    <div class="widget">

                        <div class="widget-body">
                            <div class="form-horizontal">

                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="lblPINumber" runat="server" Text="PI Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxPINumber" runat="server" placeholder="Enter PI Number" CssClass="form-control" Width="40%"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="Label1" runat="server" Text="PI Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxPIDate" runat="server" placeholder="Enter PI date" CssClass="form-control" Width="40%" TextMode="Date"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="lblOrderSeason" runat="server" Text="Order Season:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlSeason" runat="server" Display="Dynamic" CssClass="form-control" Width="40%"></asp:DropDownList>
                                    </div>
                                </div>

                                 <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="Label4" runat="server" Text="Order Year:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlYears" runat="server" Display="Dynamic" CssClass="form-control" Width="40%"></asp:DropDownList>
                                    </div>
                                </div>


                                <div class="control-group">
                                    <div class="controls controls-row" style="text-align: right">
                                        <span style="font-weight: 700; color: #CC0000">*</span>Row will not be saved if all the fields don't have values, except PO & HS Code.
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputOrder" class="control-label">
                                        <asp:Label ID="lblPIEntry" runat="server" Text="Item(s):"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls" style="overflow-x: auto; float:left; margin-left:0px;">
                                        <asp:Repeater ID="rptPIItemEntryInfo" runat="server" OnItemDataBound="rptPIItemEntryInfo_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="data-table22" class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th><asp:Label ID="lblSL" runat="server" Text="SN"></asp:Label></th>
                                                            <th><asp:Label ID="lblStyles" runat="server" Text="Style<br/>"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblOrders" runat="server" Text="PO"></asp:Label></th>
                                                            <%-- <th>
                                                            <asp:Label ID="lblItemTypes" runat="server" Text="Item Type"></asp:Label></th>--%>
                                                            <th>
                                                                <asp:Label ID="lblItems" runat="server" Text="Item Description"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblHSCode" runat="server" Text="HS Code"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblQty" runat="server" Text="Quantity"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblUnit" runat="server" Text="Unit"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label></th>
                                                            <th>
                                                                <asp:Label ID="lblAmount" runat="server" Text="Amount ($)<br/>"></asp:Label></th>
                                                             <th>
                                                                <asp:Label ID="Action" runat="server" Text="Action"></asp:Label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align:center;vertical-align:middle; font-weight:bold"><%# (Container.ItemIndex+1) %></td>
                                                    <td style="width: 20%; padding-top: 18px">
                                                        <asp:DropDownList ID="ddlStyles" runat="server" AutoPostBack="true" Display="Dynamic" Width="100%" OnSelectedIndexChanged="ddlStyles_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                                    </td>
                                                    <td style="width: 15%; padding-top: 18px">
                                                        <asp:DropDownList ID="ddlOrders" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </td>
                                                    <%-- <td style="width: 12%; padding-top: 18px">
                                                    <asp:DropDownList ID="ddlItemTypes" runat="server" AutoPostBack="true" Width="100%" OnSelectedIndexChanged="ddlItemTypes_SelectedIndexChanged"></asp:DropDownList>
                                                </td>--%>
                                                    <td style="width: 20%; padding-top: 18px">
                                                        <asp:DropDownList ID="ddlItems" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </td>

                                                    <td style="width: 10%; padding-top: 18px">
                                                        <asp:TextBox ID="tbxHSCode" Width="100%" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 10%; padding-top: 18px">
                                                        <asp:TextBox ID="tbxQty" Width="100%" runat="server" TextMode="Number" AutoPostBack="true" OnTextChanged="CalculatePrices"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 10%; padding-top: 18px">
                                                        <asp:DropDownList ID="ddlUnit" runat="server" Display="Dynamic" Width="100%" CssClass="form-control"></asp:DropDownList>
                                                    </td>
                                                    <td style="width: 15%; padding-top: 18px">
                                                        <asp:TextBox ID="tbxUnitPrice" Width="100%" runat="server" TextMode="Number" AutoPostBack="true" OnTextChanged="CalculatePrices"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 10%; padding-top: 18px">
                                                        <asp:Label ID="lblTotalAmount" Width="100%" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td style="min-width:100px; padding-top: 18px">
                                                        <asp:LinkButton ID="btnCopy" runat="server" Display="Dynamic" style="font-size:13px;padding: 4px 5px; line-height: 13px; " CssClass="btn btn-sm btn-warning" CommandArgument="<%# (Container.ItemIndex) %>"  AutoPostBack="true" OnCommand="btnCopy_Click" Text="Copy"   />
                                                        <asp:LinkButton ID="btnRemove" runat="server" Display="Dynamic" style="font-size:13px;padding: 4px 5px; line-height: 13px; " CssClass="btn btn-sm btn-danger" CommandArgument="<%# (Container.ItemIndex) %>"  AutoPostBack="true" OnCommand="btnRemove_Command" Text="Del."  />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <td style="font-weight: 500; text-align: center; background-color: lavender"></td>
                                                <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                    <asp:Label ID="lblTotal" runat="server" Text="Total:" Font-Bold="true"></asp:Label></td>
                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                <%--  <td style="font-weight: 500; text-align: left; background-color: lavender"></td>--%>
                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                    <asp:Label ID="lblTotalQuantity" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                    <asp:Label ID="lblGrandTotalAmount" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                                                <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                </tbody>
                                 </table>
                                           

                                            <div class="pull-right" style="margin-right: 0px">
                                                <asp:Button ID="btnAddRow" runat="server" class="btn btn-add btn-small btnStyle" Width="81px" Text="Add Row" OnClick="btnAddRow_Click" />
                                            </div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>

                            </div>
                            <%--       </div>--%>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </asp:Panel>


    <div class="row-fluid">

        <div id="divPIOtherInfo2" runat="server" visible="false">

            <div class="span12">
                <div class="widget">

                    <div class="widget-body">
                        <div class="row" style="padding-left: 15px; padding-right: 15px;">
                            <div class="control-group">
                                <div class="controls controls-row" style="text-align: left; line-height: 40px">
                                    <span style="font-weight: 700; padding-left: 30px">Additional Info:</span>
                                </div>
                            </div>

                            <asp:Panel ID="pnlNonEdit2" runat="server">

                                <div class="col-md-6">
                                    <div class="form-horizontal">

                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblBeneficiaryName" runat="server" Text="Beneficiary:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlYarnAndAccssSuppliers" runat="server" Display="Dynamic" Width="70%" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblNominatedOrNot" runat="server" Text="Nominated or Not?:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlNominateOrNot" runat="server" Display="Dynamic" Width="70%" CssClass="form-control">
                                                    <asp:ListItem Text="Nominated" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Non-Nominated" Value="2" Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblBeneficiaryPhone" runat="server" Text="Beneficiary Phone:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxBeneficiaryPhone" runat="server" placeholder="Enter beneficiary phone" CssClass="form-control" Width="70%"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblBeneficiaryBank" runat="server" Text="Beneficiary Bank:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxBeneficiaryBankInfo" runat="server" placeholder="Enter beneficiary bank info" CssClass="form-control" Width="70%" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblShipment" runat="server" Text="Shipment Info:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxShipment" runat="server" placeholder="Enter shipment info" CssClass="form-control" Width="70%"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblPartialShipment" runat="server" Text="Partial Shipment:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlPartialShipment" runat="server" CssClass="form-control" Width="70%">
                                                    <asp:ListItem Text="Yes" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <%--PI Type Like LCTypesBackToBack Table--%>

                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblPIType" runat="server" Text="PI Type:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlPIType" runat="server" CssClass="form-control" Width="70%">
                                                    <asp:ListItem Text="Regular" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Additional" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="Label8" runat="server" Text="Factory:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlItemBuyers" runat="server" CssClass="form-control" Style="min-width: 125px" Width="70%"></asp:DropDownList>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                                ControlToValidate="ddlItemBuyers"><span style="font-weight: 700; color: #CC0000">Please select item buyer.</span></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                       <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblTolarance" runat="server" Text="Tolerance Info:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxTolerance" runat="server" placeholder="Enter tolerance info" CssClass="form-control" Width="70%"></asp:TextBox>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </asp:Panel>


                            <div class="col-md-6">
                                <div class="form-horizontal">
                                    <div class="control-group" ID="DivApprovedDateByMdSir" runat="server">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblApprovedDateByMdSir" runat="server" Text="Approved Date By MD Sir:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxApprovedDateByMdSir" runat="server" CssClass="form-control" Width="70%" TextMode="Date"></asp:TextBox>
                                            </div>
                                        </div>
                                    <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblTenor" runat="server" Text="Payment Mode:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlTenor" runat="server" CssClass="form-control" Width="70%">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                    <asp:Panel ID="pnlNonEdit3" runat="server">

                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblPaymentMode" runat="server" Text="Payment Remarks:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxPaymentMode" runat="server" placeholder="Enter payment mode info" CssClass="form-control" Width="70%" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblItemOrderDate" runat="server" Text="Item Order Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxItemOrderDate" runat="server" CssClass="form-control" Width="70%" TextMode="Date"></asp:TextBox>
                                            </div>
                                        </div>

                                        

                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblMerchandiser" runat="server" Text="Merchandiser:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlMerchandiser" runat="server" CssClass="form-control" Width="70%">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="Label2" runat="server" Text="Merchandising Leader:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                            <div class="controls controls-row">
                                                <asp:DropDownList ID="ddlMerchandisingLeader" runat="server" CssClass="form-control" Width="70%">
                                                </asp:DropDownList>
                                            </div>
                                        </div>


                                        


                                        <div class="control-group">
                                            <label for="inputEmail3" class="control-label">
                                                <asp:Label ID="lblOthers" runat="server" Text="Remarks:"></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxOtherInfo" runat="server" placeholder="Enter other info" CssClass="form-control" Width="70%" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>

                                    </asp:Panel>

                                    <div class="control-group">
                                        <label for="inputEmail3" class="control-label">
                                            <asp:Label ID="Label3" runat="server" Text="Status:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control" Width="70%">
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <br />
                <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
                <asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>
                <asp:LinkButton ID="lnkbtnStatusChange" Visible="false" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Status Change&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnStatusChange_Click"></asp:LinkButton>



            </div>

        </div>
    </div>
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
