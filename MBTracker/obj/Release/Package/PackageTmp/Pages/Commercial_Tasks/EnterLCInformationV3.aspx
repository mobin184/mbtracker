﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="EnterLCInformationV3.aspx.cs" Inherits="MBTracker.Pages.Commercial_Tasks.EnterLCInformationV3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        input[type="radio"], input[type=checkbox] {
            margin: -2px 0 0;
        }


        .cblClass label {
            padding-left: 2px !important;
            padding-right: 14px !important;
            line-height: 20px !important;
        }
    </style>

    <div class="row-fluid">

        <div class="span9">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="actionTitle" Text="Enter L/C Information:"></asp:Label>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="widget-body">
                        <div class="control-group">
                            <div class="col-md-12">
                                <div class="pull-right" style="text-align: center; font-size: 12px; line-height: 20px; padding-bottom: 5px">
                                    <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                                </div>
                                <div class="control-group">
                                    <div class="col-md-12 col-sm-12">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                
                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="lblPINumber" runat="server" Text="L/C Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxLCNumber" runat="server" placeholder="Enter L/C Number" CssClass="form-control" Width="80%"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="Label1" runat="server" Text="L/C Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxLCDate" runat="server" placeholder="Enter L/C date" CssClass="form-control" Width="80%" TextMode="Date"></asp:TextBox>
                                    </div>
                                </div>
                                 <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="lblAdvisingBank" runat="server" Text="Applicant Bank:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlApplicantBanks" runat="server" CssClass="form-control" Style="min-width: 125px" Width="80%"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="lblNominatedOrNot" runat="server" Text="Shipment Mode:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlShipmentMode" runat="server" Display="Dynamic" Style="min-width: 125px" Width="80%" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlShipmentMode_SelectedIndexChanged">
                                    <asp:ListItem Text="---Select---" Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Sea" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Air" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Both" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="EPZ" Value="4"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label8" runat="server" Text="UD Date:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxUDDate" runat="server" CssClass="form-control" Width="80%" TextMode="Date"></asp:TextBox>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label12" runat="server" Text="UD Status:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlUDStatus" runat="server" Display="Dynamic" Width="80%" CssClass="form-control">
                                    <asp:ListItem Text="Not Completed" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Completed" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label9" runat="server" Text="ETD Date:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxETDDate" runat="server" CssClass="form-control" Width="80%" TextMode="Date"></asp:TextBox>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label11" runat="server" Text="ETA Date:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxETADate" runat="server" CssClass="form-control" Width="80%" TextMode="Date"></asp:TextBox>
                            </div>
                        </div>

                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="Label3" runat="server" Text="L/C Amount:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:TextBox ID="tbxLCAmount" runat="server" TextMode="Number" placeholder="Enter L/C Amount" CssClass="form-control" Width="80%"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                               

                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="lblBeneficiaryName" runat="server" Text="Beneficiary:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlYarnAndAccssSuppliers" runat="server" Display="Dynamic" Width="80%" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="Label18" runat="server" Text="Export LC:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlExportLC" runat="server" Display="Dynamic" Width="80%" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label13" runat="server" Text="Maturity Date:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxMaturityDate" runat="server" CssClass="form-control" Width="80%" TextMode="Date"></asp:TextBox>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label14" runat="server" Text="LC Type:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlLCType" runat="server" AutoPostBack="true" Display="Dynamic" CssClass="form-control" Width="80%" OnSelectedIndexChanged="ddlLCType_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>


                        <div class="control-group" id="divLCPurpose" runat="server">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label15" runat="server" Text="LC Purpose:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlLCPurpose" runat="server" CssClass="form-control" Width="80%">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label5" runat="server" Text="LC Status:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control" Width="80%">
                                </asp:DropDownList>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label2" runat="server" Text="Commercial Concerned:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:DropDownList ID="ddlCommercialConcerened" runat="server" CssClass="form-control" Width="80%">
                                </asp:DropDownList>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="lblOtherInfo" runat="server" Text="Other Info:"></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:TextBox ID="tbxOtherInfo" runat="server" placeholder="Enter Other Info" CssClass="form-control" Width="80%" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>

                                <br/>
                                <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label4" runat="server" Text=""></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                            <div class="controls controls-row" style="text-align: right; padding-top: 1px">
                                <%--<asp:Button ID="btnCalculsteTotal" runat="server" Text="Calculate Total:" class="btn btn-add btn-small btnStyle" OnClick="lnkbtnCalculateLCTotal_Click" />--%>
                                <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>

                            </div>
                        </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
