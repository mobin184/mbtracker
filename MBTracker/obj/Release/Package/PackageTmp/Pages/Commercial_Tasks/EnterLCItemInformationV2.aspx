﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MainMaster.Master" AutoEventWireup="true" CodeBehind="EnterLCItemInformationV2.aspx.cs" Inherits="MBTracker.Pages.Commercial_Tasks.EnterLCItemInformationV2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        input[type="radio"], input[type=checkbox] {
            margin: -2px 0 0;
        }


        .cblClass label {
            /*display: block;*/
            padding-left: 2px !important;
            padding-right: 14px !important;
            line-height: 20px !important;
        }
    </style>

    <div class="row-fluid">

        <div class="span9">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="actionTitle" Text="Enter L/C Information:"></asp:Label>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="widget-body">
                        <div class="control-group">
                            <div class="col-md-12">
                                <div class="pull-right" style="text-align: center; font-size: 12px; line-height: 20px; padding-bottom: 5px">
                                    <span style="font-weight: 700; color: #CC0000">*</span> Required Field
                                </div>
                                <div class="control-group">
                                    <div class="col-md-12 col-sm-12">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                 <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="lblFromDate" runat="server" Text="From Date:"></asp:Label></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxPIFromDate" runat="server" placeholder="Enter date" Width="80%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxPIFromDate"><span style="font-weight: 700; color: #CC0000">Please enter From Date.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                <div class="control-group">
                                            <label for="inputBuyer" class="control-label">
                                                <asp:Label ID="Label19" runat="server" Text="To Date:"></asp:Label></label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="tbxPIToDate" runat="server" placeholder="Enter date" Width="80%" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ValidationGroup="save"
                                    ControlToValidate="tbxPIToDate"><span style="font-weight: 700; color: #CC0000">Please enter To Date Date.</span></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        
                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="lblApplicantLCs" runat="server" Text="LC Number:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlApplicantLCs" runat="server" CssClass="form-control" Style="min-width: 125px" Width="80%"></asp:DropDownList>
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="Label1" runat="server" Text="Order Year:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlYears" runat="server" Display="Dynamic" CssClass="form-control" Width="80%"></asp:DropDownList>
                                    </div>
                                </div>

                                

                                <%--<div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="lblAdvisingBank" runat="server" Text="Applicant Bank:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlApplicantBanks" runat="server" CssClass="form-control" Style="min-width: 125px" Width="80%"></asp:DropDownList>
                                    </div>
                                </div>--%>

                                
                            </div>
                            <div class="col-md-6">
                                
                                
                                
                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="Label17" runat="server" Text="Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlBuyers" runat="server" Display="Dynamic" Width="80%" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlBuyers_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="lblOrderSeason" runat="server" Text="Order Season:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlSeason" runat="server" Display="Dynamic" CssClass="form-control" Width="80%"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="lblBeneficiaryName" runat="server" Text="Beneficiary:"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlYarnAndAccssSuppliers" runat="server" Display="Dynamic" Width="80%" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlYarnAndAccssSuppliers_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>

                                <%--<div class="control-group">
                                    <label for="inputEmail3" class="control-label">
                                        <asp:Label ID="Label18" runat="server" Text="Export LC:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                    <div class="controls controls-row">
                                        <asp:DropDownList ID="ddlExportLC" runat="server" Display="Dynamic" Width="80%" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <br />
    <br />

    <div class="row-fluid">




        <div class="span12" id="divPIAndOtherInfo" runat="server" visible="false">
            <div class="widget">

                <div class="widget-body">

                    <div class="form-horizontal">


                        <div class="control-group">

                            <div class="controls controls-row" style="text-align: center; padding-top: 10px">
                                <asp:Label runat="server" ID="lblLabelMessage" Text="Available PIs for beneficiary you selected:" Visible="false" Font-Bold="true"></asp:Label>
                                <%--<span style="font-weight: 700; color: #CC0000">
                                    <br />
                                    (You cannot select PIs from different beneficiaries)</span>--%>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label3" runat="server" Text="Select PI"></asp:Label><span style="font-weight: 700; color: #CC0000">*</span></label>
                            <div class="controls controls-row">
                                <asp:Repeater ID="rptPISummary" runat="server" OnItemDataBound="rptPISummary_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table1" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="Label16" runat="server" Text="Select"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label2" runat="server" Text="PI Number"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label7" runat="server" Text="PI Date"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="Label10" runat="server" Text="Beneficiary"></asp:Label></th>
                                                    <%-- <th>
                                                        <asp:Label ID="Label5" runat="server" Text="Style(s) in PI"></asp:Label></th>--%>
                                                    <th>
                                                        <asp:Label ID="Label6" runat="server" Text=""></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="vertical-align: middle">
                                                <asp:CheckBox ID="cbxPI" runat="server" AutoPostBack="true"/>
                                                <asp:Label ID="lblPIId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PIId")%>' Visible="false"></asp:Label>
                                            </td>
                                            <td style="vertical-align: middle">
                                                <asp:Label ID="lblPINumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PINumber")%>'></asp:Label></td>
                                            <td style="vertical-align: middle">
                                                <asp:Label ID="lblPIDate" runat="server" Text='<%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "PIDate"))%>'></asp:Label></td>
                                            <td style="vertical-align: middle">
                                                <asp:Label ID="lblBeneficiaryName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BeneficiaryName")%>'></asp:Label></td>
                                            <%-- <td style="vertical-align: middle">
                                                <asp:Label ID="lblPIStyles" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PIStyles")%>'></asp:Label></td>--%>
                                            <td style="vertical-align: middle">

                                                <asp:Repeater ID="rptPIItemInfo" runat="server" OnItemDataBound="rptPIItemInfo_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="data-table22" class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>
                                                                        <asp:Label ID="lblStyles" runat="server" Text="Style"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblOrders" runat="server" Text="PO"></asp:Label></th>
                                                                    <%-- <th>
                                                                        <asp:Label ID="lblItemTypes" runat="server" Text="Item Type"></asp:Label></th>--%>
                                                                    <th>
                                                                        <asp:Label ID="lblItems" runat="server" Text="Item Description"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblQty" runat="server" Text="Qty(Lbs)"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label></th>
                                                                    <th>
                                                                        <asp:Label ID="lblAmount" runat="server" Text="Amount(US$)"></asp:Label></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblStylesValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "StyleName")%>'></asp:Label>
                                                            </td>
                                                            <td style="width: 15%">
                                                                <asp:Label ID="lblPOValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderNumber")%>'></asp:Label>
                                                            </td>
                                                            <%-- <td style="width: 15%">
                                                                <asp:Label ID="lblItemTypeValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RawMaterialItemTypeName")%>'></asp:Label>
                                                            </td>--%>
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblItemValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ItemName")%>'></asp:Label>
                                                            </td>
                                                            <td style="width: 10%; padding-top: 18px">
                                                                <asp:Label ID="lblQtyValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity")%>'></asp:Label>
                                                            </td>
                                                            <td style="width: 10%; padding-top: 18px">
                                                                <asp:Label ID="lblUnitPriceValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UnitPrice")%>'></asp:Label>
                                                            </td>
                                                            <td style="width: 10%; padding-top: 18px">
                                                                <asp:Label ID="lblTotalAmount" Width="100%" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TotalAmount")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>


                                                        <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                            <asp:Label ID="lblTotal" runat="server" Text="Total:" Font-Bold="true"></asp:Label></td>
                                                        <%-- <td style="font-weight: 500; text-align: left; background-color: lavender"></td> --%>
                                                        <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                        <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                        <%--  <td style="font-weight: 500; text-align: left; background-color: lavender"></td>--%>
                                                        <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                            <asp:Label ID="lblTotalQuantity" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                                                        <td style="font-weight: 500; text-align: left; background-color: lavender"></td>
                                                        <td style="font-weight: 500; text-align: center; background-color: lavender">
                                                            <asp:Label ID="lblGrandTotalAmount" runat="server" Text="" Font-Bold="true"></asp:Label></td>


                                                        </tbody>
                                 </table>
                                           
                                                    </FooterTemplate>
                                                </asp:Repeater>

                                                <div style="text-align: center">
                                                    <asp:Label ID="lblNoItemFound" runat="server" Visible="false" Text="No Item found." BackColor="#ffff00"></asp:Label>
                                                </div>

                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                         </table>
                            <div class="clearfix">
                                    </FooterTemplate>
                                </asp:Repeater>

                            </div>
                        </div>





                        <div class="control-group">
                            <label for="inputEmail3" class="control-label">
                                <asp:Label ID="Label4" runat="server" Text=""></asp:Label><span style="font-weight: 700; color: #CC0000">&nbsp;</span></label>
                            <div class="controls controls-row" style="text-align: right; padding-top: 1px">
                                <%--<asp:Button ID="btnCalculsteTotal" runat="server" Text="Calculate Total:" class="btn btn-add btn-small btnStyle" OnClick="lnkbtnCalculateLCTotal_Click" />--%>
                                <asp:LinkButton ID="lnkbtnSaveEntries" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnSaveEntries_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnUpdateEntries" Visible="false" runat="server" class="btn btn-success btn-medium pull-right hidden-phone" Text="&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;" OnClick="lnkbtnUpdateEntries_Click"></asp:LinkButton>

                            </div>
                        </div>

                    </div>

                </div>


            </div>

        </div>


        <div style="text-align: left">
            <asp:Label ID="lblNoPIFound" runat="server" Visible="false" Text="No PI found." BackColor="#ffff00"></asp:Label>
        </div>

    </div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
