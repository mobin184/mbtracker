﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Master_Pages/MasterPageNoPageHeader.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="Dashboard.aspx.cs" Inherits="MBTracker.Pages.Dashboard" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpMain" runat="server">
    <style>
        .makecenter {
            padding-top: 0px !important
        }
    </style>


    <div class="row-fluid" id="divSearch" runat="server">
        <div class="col-md-12" style="padding-right: 0px; text-align: right">
             
            <div class="widget-body">
                <div class="form-horizontal">
                    <div class="control-group">
                        <label for="inputBuyer" class="control-label"></label>

                        <div class="controls controls-row">
                            <asp:Label runat="server" ID="lblDisplayOrders" Text="List Orders By:"></asp:Label>
                            <asp:DropDownList ID="ddlOrderDisplayOptions" runat="server" AutoPostBack="true" Width="195" OnSelectedIndexChanged="ddlOrderDisplayOptions_SelectedIndexChanged">
                                <asp:ListItem Text="--Select--" Value="" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Shipments in 60 days" Value="Shipmentsin60days"></asp:ListItem>
                                <asp:ListItem Text="Buyer" Value="Buyer"></asp:ListItem>
                                <asp:ListItem Text="Style" Value="Style"></asp:ListItem>
                                <%--  <asp:ListItem Text="Risky Orders" Value="RiskyOrders"></asp:ListItem>--%>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <asp:Panel ID="pnlOrderSearch" runat="server" Visible="false">

                        <div class="control-group">
                            <label for="inputBuyerDepartment" class="control-label">
                            </label>
                            <div class="controls controls-row">
                                <asp:Label runat="server" ID="lblDisplayInput" Text=""></asp:Label>
                                <asp:TextBox ID="tbxStyle" runat="server" Width="195" Visible="false"></asp:TextBox>
                                <asp:DropDownList ID="ddlBuyers" runat="server" Width="195" Visible="false"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="inputBuyerDepartment" class="control-label">
                                <asp:Label runat="server" ID="Label9" Text=""></asp:Label></label>
                            <div class="controls controls-row">
                                <asp:LinkButton ID="btnViewOrders2" runat="server" class="btn btn-warning2 btn-mini" Text="View Orders" OnClick="btnViewOrders2_Click"></asp:LinkButton>
                            </div>
                        </div>

                    </asp:Panel>

                </div>

            </div>

        </div>
    </div>



    <div class="row-fluid" runat="server" id="listOfOrder">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fs1" aria-hidden="true" data-icon=""></span>
                        <asp:Label runat="server" ID="LabelWidgetHeader" Text="List of orders:"></asp:Label>
                    </div>
                </div>
                <div class="widget-body">

                    <div id="dt_example" class="example_alt_pagination" style="overflow-x:auto">
                        <asp:Repeater ID="rptOrders" runat="server" OnItemCommand="rptOrders_ItemCommand" OnItemDataBound="rptOrders_ItemDataBound">
                            <HeaderTemplate>
                                <table id="data-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <%-- <th>
                                                <asp:Label ID="lblCol1" runat="server" Text="View<br/>Details"></asp:Label></th>--%>
                                            <th>
                                                <asp:Label ID="Label1" runat="server" Text="Buyer<br/>Name"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblCol2" runat="server" Text="Style<br/>Name"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblCol3" runat="server" Text="Order<br/>Number"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblCol4" runat="server" Text="Order<br/>Date"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblCol5" runat="server" Text="Order<br/>Quantity"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblCol6" runat="server" Text="Earliest<br/>Shipment Date"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblCol10" runat="server" Text="Shipment<br/> Quantity"></asp:Label></th>
                                            <%--  <th>
                                                <asp:Label ID="lblCol7" runat="server" Text="LC<br/> Value"></asp:Label></th>--%>
                                            <th>
                                                <asp:Label ID="lblCol8" runat="server" Text="Order<br/>Status"></asp:Label></th>
                                            <th>
                                                <asp:Label ID="lblCol9" runat="server" Text="Take<br/>Actions"></asp:Label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td width="8%"><%#Eval("BuyerName") %> </td>
                                    <td width="10%"><%#Eval("StyleName") %></td>
                                    <td width="8%"><%#Eval("OrderNumber") %></td>
                                   <%-- <td width="8%"><%#String.Format("{0:dd-MMM-yyyy}",Eval("OrderDate")) %> </td>--%>
                                    <td width="8%"><%#String.Format("{0:dd-MMM-yyyy}",Eval("OrderDate")) %> </td>

                                    <td width="8%"><%#Eval("OrderQuantity") %> </td>
                                      <td width="8%"><%# String.Format("{0:dd-MMM-yyyy}",Eval("ShipmentDate")) == DateTime.Now.ToString("dd-MMM-yyyy") ? "N/A" : String.Format("{0:dd-MMM-yyyy}",Eval("ShipmentDate")) %> </td>
                                    <td width="10%"><%#Eval("ShipmentQuantity") %> </td>
                                    <%-- <td width="8%"><%#Eval("LCValue") %> </td>--%>

                                    <td width="12%">
                                        <asp:Label ID="lblOrderStatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderStatusName")%>'></asp:Label>
                                        <asp:DropDownList ID="ddlOrderStatus" runat="server" Visible="false" Width="200" Height="25"></asp:DropDownList>
                                    </td>
                                    <td width="16%">
                                        <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' class="btn btn-success btn-mini" Text="Edit Status" Visible="false"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnUpdate" runat="server" Visible="false" CommandName="Update" CommandArgument='<%#Eval("Id") %>' class="btn btn-warning2 btn-mini" Text="Save Status"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" runat="server" Visible="false" CommandName="Cancel" CommandArgument='<%#Eval("Id") %>' class="btn btn-warning2 btn-mini" Text="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;&nbsp;"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="View" CommandArgument='<%#Eval("Id")+","+Eval("BuyerName")+","+Eval("StyleName")+","+Eval("OrderNumber")+","+Eval("ShipmentDate")+","+Eval("StyleId") %>' OnCommand="lnkbtnView_Command" class="btn btn-success btn-small hidden-phone" Text="View Details"></asp:LinkButton>
                                    </td>
                                </tr>

                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                         </table>
                            <div class="clearfix">
                            </FooterTemplate>
                        </asp:Repeater>

                        <asp:Label ID="lblNoOrderFound" runat="server" Visible="false" Text="No order found." BackColor="#ffff00"></asp:Label>

                    </div>

                </div>


                <div class="clearfix"></div>
            </div>

        </div>

        <div>
        </div>

    </div>

     <label for="inputStyle" class="control-label" style="font-weight:bold; padding-top:100px; margin-left:200px; text-align:left">
        <asp:Label ID="lblNotAuthorizedToView" Font-Bold="true" Visible="false" Font-Size="Large" runat="server" Text="You are not authorized to view this information."></asp:Label>
     </label>

    <div class="row-fluid" runat="server" visible="false" id="divBasicInfo">
        <div class="span12" style="">
            <div class="span6">
                <div class="widget">
                    <div class="widget-body" >

                        <label for="inputOrder" class="control-label" style="line-height: 10px; text-align: left; padding-bottom: 10px;">
                            <asp:Label ID="lblOrderInfo" runat="server" Visible="false" Text="Order Basic Info:" Font-Bold="true"></asp:Label>
                        </label>
                        <div class="row" style="padding-top: 1px">
                            <div class="col-md-6">
                                <div class="form-horizontal">

                                    <div class="control-group" style="height: 25px">
                                        <label for="inputBuyer" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label10" runat="server" Text="Buyer:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                        <div class="controls controls-row" style="padding-top: 8px">
                                            <asp:Label ID="lblBuyer" Text="" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="control-group" style="height: 25px">
                                        <label for="inputBuyerDepartment" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label11" runat="server" Text="Department:"></asp:Label></label>
                                        <div class="controls controls-row" style="padding-top: 8px">
                                            <asp:Label ID="lblDepartment" Text="" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="control-group" style="height: 25px">
                                        <label for="inputStyle" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label12" runat="server" Text="Style:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                        <div class="controls controls-row" style="padding-top: 8px">
                                            <asp:Label ID="lblStyle" Text="" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="control-group" style="height: 25px">
                                        <label for="inputOrderNumber" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label1" runat="server" Text="Order Number:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                        </label>
                                        <div class="controls controls-row" style="padding-top: 8px">
                                            <asp:Label ID="lblOrderNumber" Text="" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-horizontal">

                                    <div class="control-group" style="height: 25px">
                                        <label for="inputOrderDate" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label15" runat="server" Text="Order Date:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                        </label>
                                        <div class="controls controls-row" style="padding-top: 8px">
                                            <asp:Label ID="lblOrderDate" Text="" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="control-group" style="height: 25px">
                                        <label for="inputOrderSeason" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label13" runat="server" Text="Order Season:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                        </label>
                                        <div class="controls controls-row" style="padding-top: 8px">
                                            <asp:Label ID="lblSeason" Text="" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="control-group" style="height: 25px">
                                        <label for="inputOrderSeason" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label14" runat="server" Text="Order Year:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                        </label>
                                        <div class="controls controls-row" style="padding-top: 8px">
                                            <asp:Label ID="lblYear" Text="" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="control-group hidden">
                                        <label for="inputOrderItem" class="control-label">
                                            <asp:Label ID="Label17" runat="server" Text="Order Item:"></asp:Label>
                                        </label>
                                        <div class="controls controls-row" style="padding-top: 8px">
                                            <asp:Label ID="lblOrderItem" Text="" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="control-group hidden">
                                        <label for="inputPaymentTerm" class="control-label">
                                            <asp:Label ID="Label18" runat="server" Text="Payment Term:"></asp:Label></label>
                                        <div class="controls controls-row" style="padding-top: 8px">
                                            <asp:Label ID="lblPaymentTerm" Text="" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="control-group hidden">
                                        <label for="inputLCValue" class="control-label">
                                            <asp:Label ID="lblLCValue" runat="server" Text="LC Value:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                        <div class="controls controls-row" style="padding-top: 8px">
                                            <asp:Label ID="lblLcVlaue" Text="" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="control-group" style="height: 25px">
                                        <label for="inputOrderStatus" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label19" runat="server" Text="Order Status:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                        <div class="controls controls-row" style="padding-top: 8px">
                                            <asp:Label ID="lblOrderStatus" Text="" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        
                            <br />
                            <br />

 <%--                           <div class="span10" >--%>

                        

                            <div class="control-group" style="padding-left:75px">
                            <label for="inputOrder" class="control-label" style="line-height: 18px;padding-bottom: 15px;padding-top:50px">
                                <asp:Label ID="Label2" CssClass="" runat="server" Visible="true" Text="Color & Order Qty:" ></asp:Label>
                                </label>         
                            <div class="controls controls-row" style="overflow-x: auto;padding-left:120px;width:80%">
                                <asp:Repeater ID="rptColorInfo" runat="server" OnItemDataBound="rptColorInfo_ItemDataBound">
                                    <HeaderTemplate>
                                        <table  class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lbl2" runat="server" Text="Color"></asp:Label></th>
                                                    <th class="hidden">
                                                        <asp:Label ID="lbl3" runat="server" Text="Unit Cost"></asp:Label></th>
                                                    <th>
                                                        <asp:Label ID="lbl4" runat="server" Text="Order Quantity"></asp:Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <%#Eval("ColorDescription") %>
                                                <asp:Label ID="lblOrderId" Text='<%#Eval("OrderId") %>' runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblColorId" Text='<%#Eval("Id") %>' runat="server" Visible="false"></asp:Label>
                                            </td>
                                            <td class="hidden">
                                                <%#Eval("UnitCost") %>
                                            </td>
                                            <td>
                                                <%#Eval("OrderQuantity") %>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                 </table>
                                    <div class="clearfix"></div>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Label ID="lblNoDataFoundForColor" runat="server" Text="No data found." Visible="false" BackColor="#ffff00"></asp:Label>

                            </div>
                        </div>

<%--                          </div>--%>

                        </div>

                    </div>
                </div>
            </div>


            <div class="span6">

                <div class="widget">
                    <div class="widget-body">

                        <label for="inputOrder" class="control-label" style="line-height: 10px; text-align: left; padding-bottom: 10px;">
                            <asp:Label ID="Label3" runat="server" Text="Style Specific Info:" Font-Bold="true"></asp:Label>
                        </label>
                        <div class="row" style="padding-top: 1px">
                            <div class="col-md-6">
                                <div class="form-horizontal">

                                    <div class="control-group hidden" style="height: 25px">
                                        <label for="inputBuyer" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label4" runat="server" Text="Bookings:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                        <div class="controls controls-row" style="padding-top: 4px">
                                            <div style="width: 100%" id="divBookingDetails" runat="server" visible="false"></div>
                                        </div>
                                    </div>

                                     <div class="control-group" style="height: 25px">
                                        <label for="inputBuyer" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label26" runat="server" Text="Active POs:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                        <div class="controls controls-row" style="padding-top: 4px">
                                            <div style="width: 100%" id="divActivePOs" runat="server" visible="false"></div>
                                        </div>
                                    </div>

                                    <div class="control-group" style="height: 25px">
                                        <label for="inputBuyerDepartment" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label6" runat="server" Text="Consumption Info:"></asp:Label></label>
                                        <div class="controls controls-row" style="padding-top: 4px">
                                            <div style="width: 100%" id="divConsumptionInfo" runat="server" visible="false"></div>
                                        </div>
                                    </div>
                                    <div class="control-group" style="height: 25px; " >
                                        <label for="inputStyle" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label8" runat="server" Text="Yarn Received:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span></label>
                                        <div class="controls controls-row" style="padding-top: 4px">
                                            <div style="width: 100%" id="divYarnReceive" runat="server" visible="false"></div>
                                        </div>
                                    </div>
                                     <div class="control-group" style="height: 25px">
                                        <label for="inputOrderNumber" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label20" runat="server" Text="Yarn Issued:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                        </label>
                                        <div class="controls controls-row" style="padding-top: 4px">
                                            <div style="width: 100%" id="divYarnIssued" runat="server" visible="false"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-horizontal">
                                    
                                    <div class="control-group" style="height: 25px">
                                        <label for="inputOrderDate" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label22" runat="server" Text="Knitting Plans:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                        </label>
                                        <div class="controls controls-row" style="padding-top: 4px">
                                            <div style="width: 100%" id="divKnittingPlans" runat="server" visible="false"></div>
                                        </div>
                                    </div>
                                     <div class="control-group" style="height: 25px">
                                        <label for="inputOrderDate" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label28" runat="server" Text="Finishing Plans:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                        </label>
                                        <div class="controls controls-row" style="padding-top: 4px">
                                           <div style="width: 100%" id="divFinishingPlans" runat="server" visible="false"></div>
                                        </div>
                                    </div>
                                   <div class="control-group" style="height: 25px">
                                        <label for="inputOrderSeason" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label5" runat="server" Text="Knitting Details:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                        </label>
                                        <div class="controls controls-row" style="padding-top: 8px">
                                            <div style="width: 100%" id="divKnittingStatus" runat="server" visible="false"></div>
                                        </div>
                                    </div>
                                    <div class="control-group" style="height: 25px">
                                        <label for="inputOrderSeason" class="control-label" style="padding-top: 1px">
                                            <asp:Label ID="Label24" runat="server" Text="Delivery Details:"></asp:Label><span style="font-weight: 700; color: #CC0000"></span>
                                        </label>
                                        <div class="controls controls-row" style="padding-top: 8px">
                                            <div style="width: 100%" id="divProductionStatus" runat="server" visible="false"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div>


<%--    <div class="row-fluid" runat="server" visible="false" id="pnlDetails">
        <div class="span6" style="">
            <div class="widget">
                <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">



                        
                    
                    
                    
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>--%>



    

<div class="row-fluid">
        <div class="span12">
            <asp:Panel ID="pnlColorDeliveryCountryAndQuantity" runat="server" Visible="false">
                <div class="widget">
                    <div class="widget-body" >
                        <div class="form-horizontal">
                            <div style="text-align: left; line-height: 30px; padding-bottom: 15px">
                                <asp:Label ID="Label30" CssClass="" runat="server" Visible="true" Text="Shipment Info:" Font-Bold="true"></asp:Label>
                            </div>

                            <div id="dt_example2" class="example_alt_pagination" style="overflow-x:auto">
                                <asp:Repeater ID="rptOrderShipmentDates" runat="server" OnItemDataBound="rptOrderShipmentDates_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="data-table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="width: 100px">
                                                        <asp:Label ID="lblCol1" runat="server" Text="Shipment Dates"></asp:Label></th>
                                                    <th style="width: 100px">
                                                        <asp:Label ID="lblCol2" runat="server" Text="Shipment Mode"></asp:Label></th>
                                                    <th style="width: 100px">
                                                        <asp:Label ID="lblCol3" runat="server" Text="Color, Country, Size & Quantity"></asp:Label></th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>

                                    <ItemTemplate>
                                        <tr>
                                            <td style="text-align: center; vertical-align: middle">
                                                <asp:Label ID="lblShipmentDate" runat="server" Text=' <%# String.Format("{0:dd-MMM-yyyy}", DataBinder.Eval(Container.DataItem, "ShipmentDate"))%>'></asp:Label><asp:Label ID="lblShipmenDatetId" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Id")%>' Visible="false"></asp:Label></td>
                                            <td style="text-align: center; vertical-align: middle">
                                                <asp:Label ID="lblShipmentMode" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ShipmentMode")%>'></asp:Label></td>
                                            <td style="vertical-align: middle">
                                                <asp:Repeater ID="rptOrderColors" runat="server" OnItemDataBound="rptOrderColors_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="data-table" class="table table-bordered table-hover">

                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="text-align: center; vertical-align: middle">
                                                                <asp:Label ID="lblColor" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ColorDescription")%>'></asp:Label><asp:Label ID="lblOrderColorId" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "BuyerColorId")%>' Visible="false"></asp:Label></td>
                                                            <td style="text-align: center">

                                                                <asp:Repeater ID="rptShipmentCountry" runat="server" OnItemDataBound="rptShipmentCountry_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="data-table" class="table table-bordered table-hover">

                                                                            <tbody>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td style="text-align: center; vertical-align: middle">
                                                                                <asp:Label ID="lblCountry" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryCountryCode")%>'></asp:Label><asp:Label ID="lblDeliveryCountryId" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "OrderDeliveryCountryId")%>' Visible="false"></asp:Label></td>
                                                                            <td>
                                                                                <asp:GridView ID="gvSizeAndQty" RowStyle-Height="30" CssClass="GridViewClass" runat="server" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" OnRowCreated="gvSizeAndQty_RowCreated"></asp:GridView>
                                                                            </td>

                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </tbody>
                                                                                        </table>
                                                                                        <div class="clearfix">
                                                                    </FooterTemplate>
                                                                </asp:Repeater>

                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                                                    </table>
                                                                    <div class="clearfix">
                                                    </FooterTemplate>
                                                </asp:Repeater>

                                            </td>

                                        </tr>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        </tbody>
                                                    </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </asp:Panel>
        </div>
    </div>




    <div class="row-fluid" runat="server" visible="false" id="pnlDetails2">
        <div class="span12" style="">

            <%--<div class="span6" style="">
                <div class="widget" style="border: none">
                    <div class="widget-body">
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblDeliveryCountry" runat="server" Visible="false" Text="Delivery Country Information" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row">
                                    <asp:Repeater ID="rptDeliveryCountries" runat="server">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lbl1" runat="server" Text="Delivery Countries"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center">
                                                    <asp:Label ID="lblCountryName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DeliveryCountryName")%>' Width="300px"></asp:Label></td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                 </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <asp:Label ID="lblNoCountryFound" runat="server" Text="No country found!" Font-Bold="true" Visible="false" BackColor="#ffff00"></asp:Label>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>--%>



           <%-- <div class="span6" style="">
                <div class="widget" style="border: none">
                    <div class="widget-body" >
                        <div id="dt_example" class="example_alt_pagination">
                            <div class="control-group">
                                <label for="inputOrder" class="control-label">
                                    <asp:Label ID="lblOrderDocuments" runat="server" Visible="false" Text="PO Documents:" Font-Bold="true"></asp:Label></label>
                                <div class="controls controls-row" style="overflow-x:auto">
                                    <asp:Repeater ID="rptOrderDocument" runat="server">
                                        <HeaderTemplate>
                                            <table id="data-table" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lbl1" runat="server" Text="File Name"></asp:Label></th>
                                                        <th>
                                                            <asp:Label ID="lbl2" runat="server" Text="Action"></asp:Label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center">
                                                    <asp:Label ID="lblFileName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FileName")%>'></asp:Label></td>
                                                <td style="text-align: center">
                                                    <asp:HyperLink runat="server" NavigateUrl='<%# Eval("Id", "Buyers_and_Orders/GetFile.aspx?ID={0}&Type=OrderDocument") %>' Text="Download"></asp:HyperLink></td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                 </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <asp:Label ID="lblNoDocumnet" runat="server" Text="No document found." Visible="false" BackColor="#ffff00"></asp:Label>

                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>--%>



        </div>
    </div>






</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cpScript" runat="server">
    <script src="../../js/jquery.dataTables.js"></script>

   <%-- <script type="text/javascript">
        $("body").on("click", "[src*=plus]", function () {
            $(this).closest("tr").after("<tr><td></td><td colspan = '10'>" + $(this).next().html() + "</td></tr>")
            $(this).attr("src", "../images/minus.png");
        });
        $("body").on("click", "[src*=minus]", function () {
            $(this).attr("src", "../images/plus.png");
            $(this).closest("tr").next().remove();
        });
    </script>--%>

</asp:Content>
