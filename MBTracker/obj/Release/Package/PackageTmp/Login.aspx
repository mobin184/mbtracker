﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="MBTracker.Pages.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Login || MB Tracking System</title>
    <link href="~/css/main.css" rel="stylesheet" />
</head>

<body>
   
    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span4 offset4">
                    <div class="signin" style="border: 1px solid #e3e3e3; border-radius: 6px;">
                        <h2 class="center-align-text">User Login</h2>
                        <div class="signin-wrapper">
                             <p style="color: #ce0000;background-color: #fff;text-align: center;font-size: 14px;margin-left: 12px;margin-right: 12px;margin: 0px 0 -10px;"><asp:Literal ID="FailureText" runat="server">&nbsp;</asp:Literal> </p>
                            <div class="content">
                                <asp:TextBox ID="tbxUserId" runat="server"  CssClass="input input-block-level" placeholder="Enter Username"></asp:TextBox>
                                <asp:TextBox  ID="tbxPassword" runat="server" CssClass="input input-block-level" placeholder="Enter Password" TextMode="Password"></asp:TextBox>
                            </div>
                            <div class="actions">
                                <asp:Button ID="btnLogin" runat="server" class="btn btn-info pull-right" Text="Sign In" OnClick="LoginButton_Click" />
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
 </body>
</html>
