﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="MBTracker.User_Controls.Header" %>

<a href="/Pages/DashBoard.aspx" class="logo">Masihata Business Tracking System</a>

<div id="mini-nav">
    <ul>

      <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="fs1" aria-hidden="true" data-icon=""></span>&nbsp <asp:Literal runat="server" ID="ltrlUserName"></asp:Literal>
                <span class="caret icon-white"></span>
            </a>
            <ul class="dropdown-menu pull-right">
                 <li>
                    <a href="../../Pages/Admin_and_Setup/ChangePassword.aspx"><span class="fs1" aria-hidden="true" data-icon="&#xe090;"></span>&nbsp Change Password</a>
                </li>
                <li>
                    <a href="../../Login.aspx?logout=yes"><span class="fs1" aria-hidden="true" data-icon="&#xe0b1;"></span>&nbsp Log Out</a>
                </li>
            </ul>
        </li>
        
    </ul>
    <div class="clearfix"></div>
</div>


