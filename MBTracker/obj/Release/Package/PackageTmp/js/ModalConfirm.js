﻿
$("[id*=lnkbtnDelete]").live("click", function (e) {

    var uniqueID = e.currentTarget.id;
    var Id = e.currentTarget.dataset.id;
    $("#dialog").dialog({
        title: "Delete Confirmation",

        buttons: {
            Ok: function() {
                __doPostBack(uniqueID, Id);
            },
            Close: function() {
                $(this).dialog('close');
            }
        },
    });
    e.preventDefault();
});
 