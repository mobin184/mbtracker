//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MBTracker.EF
{
    using System;
    using Repositories;
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    public partial class DailyKnittings : EditDeleteBaseClass
    {
             [Key()]
           public int Id { get; set; }
           public System.DateTime KnittingDate { get; set; }
           public int ShiftId { get; set; }
           public int StyleId { get; set; }
           public int BuyerColorId { get; set; }
           public int KnittingUnitId { get; set; }
           public int SizeId { get; set; }
           public int MachineBrandId { get; set; }
           public string MachineGauge { get; set; }
           public string JacquardPortion { get; set; }
           public int NumOfMC { get; set; }
           public Nullable<int> ProductionQty { get; set; }
           public string CreatedBy { get; set; }
           public Nullable<System.DateTime> CreateDate { get; set; }
           public string UpdatedBy { get; set; }
           public Nullable<System.DateTime> UpdateDate { get; set; }
           public Nullable<int> FinishingUnitId { get; set; }
    }
}
