//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MBTracker.EF
{
    using System;
    using Repositories;
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    public partial class OrderYarnBookings : EditDeleteBaseClass
    {
             [Key()]
           public int Id { get; set; }
           public int BookingId { get; set; }
           public int StyleId { get; set; }
           public int BuyerColorId { get; set; }
           public string YarnDescription { get; set; }
           public int BookingQuantity { get; set; }
           public string Supplier { get; set; }
           public Nullable<decimal> ConsumptionPerDzn { get; set; }
           public int YarnProcurementStatus { get; set; }
           public string CreatedBy { get; set; }
           public Nullable<System.DateTime> CreateDate { get; set; }
           public string UpdatedBy { get; set; }
           public Nullable<System.DateTime> UpdateDate { get; set; }
    }
}
