//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MBTracker.EF
{
    using System;
    using Repositories;
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    public partial class KnittedItemDelvierdToFinishingUnit : EditDeleteBaseClass
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public KnittedItemDelvierdToFinishingUnit()
        {
            this.KnittedItemDelvierdToFinishingUnitColorSizes = new HashSet<KnittedItemDelvierdToFinishingUnitColorSizes>();
        }
    
             [Key()]
           public int Id { get; set; }
           public int KnittingUnitId { get; set; }
           public int BuyerId { get; set; }
           public int StyleId { get; set; }
           public int FinishingUnitId { get; set; }
           public string ChalanNo { get; set; }
           public string GatePassNo { get; set; }
           public string VehicleNo { get; set; }
           public System.DateTime DeliveredDate { get; set; }
           public int DeliveredQty { get; set; }
           public string CreatedBy { get; set; }
           public System.DateTime CreateDate { get; set; }
           public string UpdatedBy { get; set; }
           public Nullable<System.DateTime> UpdateDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KnittedItemDelvierdToFinishingUnitColorSizes> KnittedItemDelvierdToFinishingUnitColorSizes { get; set; }
    }
}
