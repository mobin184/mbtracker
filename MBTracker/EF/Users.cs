//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MBTracker.EF
{
    using System;
    using Repositories;
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    public partial class Users : EditDeleteBaseClass
    {
             [Key()]
           public int Id { get; set; }
           public string UserName { get; set; }
           public string Password { get; set; }
           public Nullable<bool> IsActive { get; set; }
           public string Email { get; set; }
           public string Phone { get; set; }
           public string FullName { get; set; }
           public Nullable<int> RoleId { get; set; }
           public Nullable<int> Department { get; set; }
           public Nullable<int> Designation { get; set; }
           public Nullable<int> EmployeeId { get; set; }
           public Nullable<System.DateTime> LastLoginDate { get; set; }
           public Nullable<int> WrongPasswordAttempt { get; set; }
           public string CreatedBy { get; set; }
           public Nullable<System.DateTime> CreateDate { get; set; }
           public string UpdatedBy { get; set; }
           public Nullable<System.DateTime> UpdateDate { get; set; }
    
        public virtual UserRoles UserRoles { get; set; }
    }
}
