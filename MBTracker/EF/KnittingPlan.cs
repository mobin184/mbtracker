//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MBTracker.EF
{
    using System;
    using Repositories;
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    public partial class KnittingPlan : EditDeleteBaseClass
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public KnittingPlan()
        {
            this.KnittingPlanDetails = new HashSet<KnittingPlanDetails>();
        }
    
             [Key()]
           public int PlanId { get; set; }
           public int StyleId { get; set; }
           public int KnittingUnitId { get; set; }
           public int MachineBrandId { get; set; }
           public int IssueQty { get; set; }
           public string CreatedBy { get; set; }
           public Nullable<System.DateTime> CreateDate { get; set; }
           public string UpdatedBy { get; set; }
           public Nullable<System.DateTime> UpdateDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KnittingPlanDetails> KnittingPlanDetails { get; set; }
    }
}
