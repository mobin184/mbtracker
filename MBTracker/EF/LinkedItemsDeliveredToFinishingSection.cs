//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MBTracker.EF
{
    using System;
    using Repositories;
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    public partial class LinkedItemsDeliveredToFinishingSection : EditDeleteBaseClass
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LinkedItemsDeliveredToFinishingSection()
        {
            this.LinkedItemsDeliveredToFinishingSectionColorSizes = new HashSet<LinkedItemsDeliveredToFinishingSectionColorSizes>();
        }
    
             [Key()]
           public int Id { get; set; }
           public System.DateTime DeliveryDate { get; set; }
           public int FinishingUnitId { get; set; }
           public int FinishingUnitFloorId { get; set; }
           public int BuyerId { get; set; }
           public int StyleId { get; set; }
           public int DeliveryQty { get; set; }
           public Nullable<bool> IsReceived { get; set; }
           public string ReceivedBy { get; set; }
           public Nullable<System.DateTime> ReceivedDate { get; set; }
           public string CreatedBy { get; set; }
           public Nullable<System.DateTime> CreateDate { get; set; }
           public string UpdatedBy { get; set; }
           public Nullable<System.DateTime> UpdateDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LinkedItemsDeliveredToFinishingSectionColorSizes> LinkedItemsDeliveredToFinishingSectionColorSizes { get; set; }
    }
}
