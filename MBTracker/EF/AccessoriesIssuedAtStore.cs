//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MBTracker.EF
{
    using System;
    using Repositories;
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    public partial class AccessoriesIssuedAtStore : EditDeleteBaseClass
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AccessoriesIssuedAtStore()
        {
            this.AccessoriesIssuedAtStoreItems = new HashSet<AccessoriesIssuedAtStoreItems>();
        }
    
             [Key()]
           public int IssueId { get; set; }
           public int StoreId { get; set; }
           public int KnittingUnitId { get; set; }
           public System.DateTime IssueDate { get; set; }
           public int BuyerId { get; set; }
           public int StyleId { get; set; }
           public int IssuedQuantity { get; set; }
           public string CreatedBy { get; set; }
           public Nullable<System.DateTime> CreateDate { get; set; }
           public string UpdatedBy { get; set; }
           public Nullable<System.DateTime> UpdateDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccessoriesIssuedAtStoreItems> AccessoriesIssuedAtStoreItems { get; set; }
    }
}
