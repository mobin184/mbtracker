//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MBTracker.EF
{
    using System;
    using Repositories;
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    public partial class BookingDetails : EditDeleteBaseClass
    {
             [Key()]
           public int Id { get; set; }
           public int BookingId { get; set; }
           public int ProductionMonthId { get; set; }
           public int MachineBrandId { get; set; }
           public int KnittingTime { get; set; }
           public int FinishingUnitId { get; set; }
           public int LinkingMachineGaugeId { get; set; }
           public int LinkingQtyPerDay { get; set; }
           public int BookingQuantity { get; set; }
           public Nullable<int> KnittingUnitId { get; set; }
           public int KnittingBookingQty { get; set; }
           public int LinkingBookingQty { get; set; }
    
        public virtual Booking Booking { get; set; }
    }
}
