﻿using MBTracker.EF;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Repositories
{
    public static class Tools
    {

        public static Control FindControl(this Repeater repeater, string controlName)
        {
            for (int i = 0; i < repeater.Controls.Count; i++)
                if (repeater.Controls[i].Controls[0].FindControl(controlName) != null)
                    return repeater.Controls[i].Controls[0].FindControl(controlName);
            return null;
        }
        public static void CopyClass(this Object Destination, object Source)
        {
            var srcT = Source.GetType();
            var dstT = Destination.GetType();
            foreach (var f in srcT.GetFields())
            {
                var dstF = dstT.GetField(f.Name);
                if (dstF == null)
                    continue;
                dstF.SetValue(Destination, f.GetValue(Source));
            }

            foreach (var f in srcT.GetProperties())
            {
                var dstF = dstT.GetProperty(f.Name);
                if (dstF == null)
                    continue;

                dstF.SetValue(Destination, f.GetValue(Source, null), null);
            }
        }

        public static object GetPrimaryKeyValue(object model) => model.GetPropertyValue(GetPrimaryKeyInfo(model).Name);
        public static PropertyInfo GetPrimaryKeyInfo(object model)
        {
            PropertyInfo[] properties = model.GetType().GetProperties();
            foreach (PropertyInfo pI in properties)
            {
                System.Object[] attributes = pI.GetCustomAttributes(true);
                foreach (object attribute in attributes)
                {
                    if (attribute.GetType().Name == "KeyAttribute")
                    {
                        return pI;
                    }
                }
            }
            return null;
        }

        public static object GetPropertyValue(this object src, string propertyName)
        {
            if (src == null) throw new ArgumentNullException($"Source object can not be null");
            return src.GetType().GetProperty(propertyName)?.GetValue(src, null);
        }

        public static void SetPropertyValue(this object p_object, string p_propertyName, object value)
        {
            PropertyInfo property = p_object.GetType().GetProperty(p_propertyName);
            Type t = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;
            object safeValue = (value == null) ? null : Convert.ChangeType(value, t);

            property.SetValue(p_object, safeValue, null);
        }


        public static DataTable ToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public static string GetErrorMessage(string ex)
        {
            return Regex.Replace(ex, @"[^0-9a-zA-Z_ ]+", "");
        }

        public static string UrlEncode(string st)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(st);
            var stEncoded = System.Convert.ToBase64String(plainTextBytes);
            return stEncoded;
        }

        public static string UrlDecode(string st)
        {
            st = st.Replace(" ", "+");
            var encriptedTextBytes = Convert.FromBase64String(st);
            var stDecoded = System.Text.Encoding.UTF8.GetString(encriptedTextBytes);
            return stDecoded;
        }

        public static int HoursToMiliseconds(float hours)
        {
            int milliseconds = int.Parse(Math.Round(hours * 60.0 * 60.0 * 1000.0).ToString());
            return milliseconds;
        }

        public static string DataTableToJson(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(table);
            return JSONString;
        }


        public static List<T> CheckEditDeletePermission<T>(this List<T> list, int roleId, string pageName, int ButtonNumberOnPage = 1)
        {
            try
            {

                var unitOfWOrk = new UnitOfWork();
                string url = HttpContext.Current.Request.Url.AbsolutePath;
                var task = unitOfWOrk.GenericRepositories<TaskItems>().Get(x => x.URL == url).FirstOrDefault();
                //For edit
                var permisisonEdit = unitOfWOrk.GenericRepositories<EditDeleteButtonsForRoles>().Get(x => x.RoleId == roleId && ((x.TaskId == task.Id) || (x.PageName.ToLower() == pageName.ToLower())) && x.ButtonType == 1 && x.ButtonNumberOnPage == ButtonNumberOnPage).FirstOrDefault();
                if (permisisonEdit != null)
                {
                    foreach (var item in list)
                    {
                        if (item.GetType().GetProperty("CanEdit") != null)
                        {
                            if (item.GetType().GetProperty("CreateDate") != null)
                            {
                                var createDate = (DateTime)item.GetPropertyValue("CreateDate");
                                if (createDate.AddDays(permisisonEdit.EnableDurationInDays ?? 0).Date >= DateTime.Now.Date)
                                {
                                    item.SetPropertyValue("CanEdit", true);
                                }
                                else
                                {
                                    item.SetPropertyValue("CanEdit", false);
                                }
                            }
                            else
                            {
                                throw new Exception("CreateDate property is not present");
                            }
                        }
                        else
                        {
                            throw new Exception("CanEdit property is not present");
                        }
                    }
                }
                else
                {
                    foreach (var item in list)
                    {
                        if (item.GetType().GetProperty("CanEdit") != null)
                        {
                            item.SetPropertyValue("CanEdit", false);
                        }
                        else
                        {
                            throw new Exception("CanEdit property is not present");
                        }
                    }
                }


                //For delete
                var permisisonDelete = unitOfWOrk.GenericRepositories<EditDeleteButtonsForRoles>().Get(x => x.RoleId == roleId && ((x.TaskId == task.Id) || (x.PageName.ToLower() == pageName.ToLower())) && x.ButtonType == 2 && x.ButtonNumberOnPage == ButtonNumberOnPage).FirstOrDefault();
                if (permisisonDelete != null)
                {
                    foreach (var item in list)
                    {
                        if (item.GetType().GetProperty("CanDelete") != null)
                        {
                            if (item.GetType().GetProperty("CreateDate") != null)
                            {
                                var createDate = (DateTime)item.GetPropertyValue("CreateDate");
                                if (createDate.AddDays(permisisonEdit.EnableDurationInDays ?? 0).Date >= DateTime.Now.Date)
                                {
                                    item.SetPropertyValue("CanDelete", true);
                                }
                                else
                                {
                                    item.SetPropertyValue("CanDelete", false);
                                }
                            }
                            else
                            {
                                throw new Exception("CreateDate property is not present");
                            }
                        }
                        else
                        {
                            throw new Exception("CanDelete property is not present");
                        }
                    }
                }
                else
                {
                    foreach (var item in list)
                    {
                        if (item.GetType().GetProperty("CanDelete") != null)
                        {
                            item.SetPropertyValue("CanDelete", false);
                        }
                        else
                        {
                            throw new Exception("CanDelete property is not present");
                        }
                    }
                }

                return list;
            }
            catch (Exception)
            {

                throw;
            }
        }


        public static DataTable CheckEditDeletePermission(this DataTable dt, int roleId, string pageName, int ButtonNumberOnPage = 1)
        {
            try
            {
                var taskId = 0;
                var unitOfWOrk = new UnitOfWork();
                string url = HttpContext.Current.Request.Url.AbsolutePath;
                var task = unitOfWOrk.GenericRepositories<TaskItems>().Get(x => x.URL == url).FirstOrDefault();
                if (task != null)
                    taskId = task.Id;
                //var list = dt.AsEnumerable().to

                if (dt.Columns.Contains("CanEdit") != true)
                {
                    dt.Columns.Add("CanEdit", typeof(bool));
                }
                if (dt.Columns.Contains("CanDelete") != true)
                {
                    dt.Columns.Add("CanDelete", typeof(bool));
                }
                //For edit
                var permisisonEdit = unitOfWOrk.GenericRepositories<EditDeleteButtonsForRoles>().Get(x => x.RoleId == roleId && ((x.TaskId == taskId) || (x.PageName.ToLower() == pageName.ToLower())) && x.ButtonType == 1 && x.ButtonNumberOnPage == ButtonNumberOnPage).FirstOrDefault();
                if (permisisonEdit != null)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        if (item.Table.Columns.Contains("CreateDate") == true)
                        {
                            var createDate = (DateTime)item["CreateDate"];
                            if (createDate.AddDays(permisisonEdit.EnableDurationInDays ?? 0).Date >= DateTime.Now.Date)
                            {
                                item["CanEdit"] = true;
                            }
                            else
                            {
                                item["CanEdit"] = false;
                            }
                        }
                        else
                        {
                            throw new Exception("CreateDate property is not present");
                        }
                    }
                }
                else
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        item["CanEdit"] = false;
                    }
                }


                //For delete
                var permisisonDelete = unitOfWOrk.GenericRepositories<EditDeleteButtonsForRoles>().Get(x => x.RoleId == roleId && ((x.TaskId == taskId) || (x.PageName.ToLower() == pageName.ToLower())) && x.ButtonType == 2 && x.ButtonNumberOnPage == ButtonNumberOnPage).FirstOrDefault();
                if (permisisonDelete != null)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        if (item.Table.Columns.Contains("CreateDate") == true)
                        {
                            var createDate = (DateTime)item["CreateDate"];
                            if (createDate.AddDays(permisisonDelete.EnableDurationInDays ?? 0).Date >= DateTime.Now.Date)
                            {
                                item["CanDelete"] = true;
                            }
                            else
                            {
                                item["CanDelete"] = false;
                            }
                        }
                        else
                        {
                            throw new Exception("CreateDate property is not present");
                        }

                    }
                }
                else
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        item["CanDelete"] = false;
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static bool HideReportViewerControl(Page page)
        {
            bool hideReport = false;
            Control control = null;
            string ctrlname = page.Request.Params.Get("__EVENTTARGET");
            if (ctrlname != null && ctrlname != string.Empty)
            {
                control = page.FindControl(ctrlname);
            }
            else
            {
                foreach (string ctl in page.Request.Form)
                {
                    Control c = page.FindControl(ctl);
                    if (c != null)
                    {
                        control = c;
                        break;
                    }
                }
            }

            if (control is System.Web.UI.WebControls.DropDownList)
            {
                hideReport = true;
            }
            return hideReport;
        }

        public static Control GetPostBackControl(Page page)
        {
            Control control = null;
            string ctrlname = page.Request.Params.Get("__EVENTTARGET");
            if (ctrlname != null && ctrlname != string.Empty)
            {
                control = page.FindControl(ctrlname);
            }
            else
            {
                foreach (string ctl in page.Request.Form)
                {
                    Control mycontrol = page.FindControl(ctl);
                    if (mycontrol is System.Web.UI.WebControls.Button)
                    {
                        control = mycontrol;
                        break;
                    }
                }
            }
            return control;
        }
    }

}