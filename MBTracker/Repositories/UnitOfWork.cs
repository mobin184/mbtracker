using MBTracker.Code_Folder;
using MBTracker.EF;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Core.Metadata.Edm;
using Newtonsoft.Json;

namespace Repositories
{
    public class UnitOfWork : IDisposable
    {
        private MBTrackerEntities context = new MBTrackerEntities();
        JsonSerializerSettings setting = new JsonSerializerSettings
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };

        private Dictionary<Type, object> repositories = new Dictionary<Type, object>();

        public GenericRepository<TEntity> GenericRepositories<TEntity>() where TEntity : class//, new()
        {
            if (repositories.ContainsKey(typeof(TEntity)) == true)
                return (GenericRepository<TEntity>)repositories[typeof(TEntity)];
            GenericRepository<TEntity> tRepositoryObject = new GenericRepository<TEntity>(context);
            repositories.Add(typeof(TEntity), tRepositoryObject);
            return tRepositoryObject;
        }


        public DataTable GetPiListByDateSearch(int nTransType, int nParam1, DateTime dateTime1, DateTime dateTime2, string sSearch)
        {
            DataTable objDataTable = new DataTable();
            try
            {
                SqlCommand Cmd = new SqlCommand();
                Cmd.CommandText = "Sp_PIwise_StoreReceive";
                Cmd.Parameters.AddWithValue("@TransType", nTransType);
                Cmd.Parameters.AddWithValue("@Param1", nParam1);
                Cmd.Parameters.AddWithValue("@Date1", dateTime1);
                Cmd.Parameters.AddWithValue("@Date2", dateTime2);
                Cmd.Parameters.AddWithValue("@Search", sSearch);

                objDataTable = GetTable(Cmd);
            }
            catch (Exception ex)
            {

            }
            return objDataTable;
        }

        //==============================end===============================//


        public void Save()
        {
            try
            {
                var entities = GetChangedData(context);
                context.SaveChanges();
                SaveAuditData(entities);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void SaveAuditLog(string eventType, string recordId, string tableName, object data)
        {
            string userId = CommonMethods.SessionInfo.UserId.ToString();
            var auditLog = new AuditLog() { AuditLogID = Guid.NewGuid(), EventDate = DateTime.Now, EventType = eventType, RecordID = recordId, TableName = tableName, UserID = userId, Data = JsonConvert.SerializeObject(data, setting) };
            context.AuditLog.Add(auditLog);
        }

        public void SaveAuditLogWhenSql(string eventType, string recordId, string tableName, object data)
        {
            string userId = CommonMethods.SessionInfo.UserId.ToString();
            var auditLog = new AuditLog() { AuditLogID = Guid.NewGuid(), EventDate = DateTime.Now, EventType = eventType, RecordID = recordId, TableName = tableName, UserID = userId, Data = JsonConvert.SerializeObject(data, setting) };
            context.AuditLog.Add(auditLog);
            context.SaveChanges();
        }

        private List<EntityChange> GetChangedData(DbContext context)
        {
            var ChangeTracker = context.ChangeTracker;
            var modifiedEntities = ChangeTracker.Entries().Where(p => p.State == EntityState.Modified || //p.State == EntityState.Added ||
                                                                    p.State == EntityState.Deleted).ToList();
            var now = DateTime.UtcNow;
            List<EntityChange> entities = new List<EntityChange>();
            foreach (var model in modifiedEntities)
            {
                var entityName = "";
                if (model.Entity.GetType().BaseType != null && model.Entity.GetType().Namespace == "System.Data.Entity.DynamicProxies")
                {
                    entityName = model.Entity.GetType().BaseType.Name;
                }
                else
                {
                    entityName = model.Entity.GetType().Name;
                }

                //string tableName = model.Entity.GetType().Name;
                if (entityName != "AuditLog")
                {
                    entities.Add(new EntityChange()
                    {
                        ChangeType = model.State.ToString(),
                        Entity = model.Entity,
                        Entry = model,
                        EntityName = entityName,
                    });
                }
            }
            return entities;
        }

        public DataTable GetPIinfoByStyleByuer(Int32 nBuyerId, Int32 nStyleId)
        {
            DataTable objDataTable = new DataTable();
            try
            {
                SqlCommand Cmd = new SqlCommand();
                Cmd.CommandText = "GetPIInfoByBuyerOrStyle";
                Cmd.Parameters.AddWithValue("@BuyerId", nBuyerId);
                Cmd.Parameters.AddWithValue("@StyleId", nStyleId);
                objDataTable = GetTable(Cmd);
            }
            catch (Exception ex)
            {

            }
            return objDataTable;
        }

        public DataTable GetPIinfoByStyleByuerSupplier(Int32 nBuyerId, Int32 nStyleId, Int32 nSupplierId)
        {
            DataTable objDataTable = new DataTable();
            try
            {
                SqlCommand Cmd = new SqlCommand();
                Cmd.CommandText = "GetPIInfoByBuyerOrStyleAndSupplier";
                Cmd.Parameters.AddWithValue("@BuyerId", nBuyerId);
                Cmd.Parameters.AddWithValue("@StyleId", nStyleId);
                Cmd.Parameters.AddWithValue("@SupplierId", nSupplierId);
                objDataTable = GetTable(Cmd);
            }
            catch (Exception ex)
            {

            }
            return objDataTable;
        }


        private void SaveAuditData(List<EntityChange> entityChanges)
        {
            var userId = CommonMethods.SessionInfo.UserId.ToString();
            foreach (var item in entityChanges)
            {
                string st = "";
                var entry = item.Entry;

                if (item.ChangeType == "Modified")
                {
                    var originalValues = entry.OriginalValues;
                    var currentValues = entry.CurrentValues;
                    foreach (var property in entry.OriginalValues.PropertyNames)
                    {
                        var original = originalValues[property];
                        var current = currentValues[property];
                        if (!Equals(original, current))
                        {
                            if (string.IsNullOrEmpty(st))
                                st += string.Format("{0} changed from {1} to {2}", property, original, current);
                            else
                                st += string.Format(", {0} changed from {1} to {2}", property, original, current);
                        }
                    }
                }
                context.AuditLog.Add(new AuditLog()
                {
                    AuditLogID = Guid.NewGuid(),
                    UserID = userId,
                    EventDate = DateTime.Now,
                    EventType = item.ChangeType,
                    TableName = item.EntityName,
                    RecordID = Tools.GetPrimaryKeyValue(item.Entity).ToString(),
                    Data = st == "" ? JsonConvert.SerializeObject(item.Entity, setting).ToString() : st
                });
            }
            context.SaveChanges();
        }


        public DataTable GetTable(SqlCommand Cmd)
        {
            DataTable objDataTable = new DataTable();
            try
            {
                SqlConnection sqlConn = (SqlConnection)context.Database.Connection;
                Cmd.CommandType = CommandType.StoredProcedure;
                sqlConn.Open();
                SqlDataAdapter adp = new SqlDataAdapter();
                adp.SelectCommand = Cmd;
                Cmd.CommandTimeout = 30000000;
                Cmd.Connection = sqlConn;
                adp.Fill(objDataTable);
                sqlConn.Close();
            }
            catch (Exception ex)
            {

            }
            return objDataTable;
        }

        public DataTable GetDataTableFromSql(string sql)
        {

            //using (context = new MBTrackerEntities())
            //{
            DataTable dt = new DataTable();
            SqlConnection sqlConn = (SqlConnection)context.Database.Connection;
            SqlDataAdapter da = new SqlDataAdapter(sql, sqlConn);
            da.SelectCommand.CommandTimeout = 600;
            da.Fill(dt);
            da.Dispose();
            return dt;
            //}

            //using (context = new MBTrackerEntities())
            //{
            //    var dt = new DataTable();
            //    var conn = context.Database.Connection;
            //    var connectionState = conn.State;
            //    try
            //    {
            //        if (connectionState != ConnectionState.Open) conn.Open();
            //        using (var cmd = conn.CreateCommand())
            //        {
            //            cmd.CommandText = sql;
            //            cmd.CommandType = CommandType.Text;
            //            using (var reader = cmd.ExecuteReader())
            //            {
            //                dt.Load(reader);
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        throw;
            //    }
            //    finally
            //    {
            //        if (connectionState != ConnectionState.Closed) conn.Close();
            //    }
            //    return dt;
            //}
        }

        public void ExeccuteRawQyery(string sql, SqlParameter[] _sqlParameter = null)
        {
            //using (context = new MBTrackerEntities())
            //{
            if (_sqlParameter != null)
            {
                context.Database.ExecuteSqlCommand(sql, _sqlParameter);
            }
            else
            {
                context.Database.ExecuteSqlCommand(sql);
            }
            //}
        }

        public List<T> GetRecordSet<T>(string sqlSyntex, SqlParameter[] param = null) where T : class
        {
            if (param == null)
                return context.Database.SqlQuery<T>(sqlSyntex).ToList();
            else
                return context.Database.SqlQuery<T>(sqlSyntex, param).ToList();
        }

        //public List<T> GetRecordSet<T>(string sqlSyntex, SqlParameter[] param = null) where T : class
        //{
        //    //using (context = new MBTrackerEntities())
        //    //{
        //    using (var command = context.Database.Connection.CreateCommand())
        //    {
        //        command.CommandText = sqlSyntex;
        //        command.CommandType = CommandType.Text;
        //        if (param != null)
        //        {
        //            command.Parameters.Add(param);
        //        }

        //        if (context.Database.Connection.State != ConnectionState.Open) context.Database.Connection.Open();
        //        using (var result = command.ExecuteReader())
        //        {
        //            return DataReaderMapToList<T>(result);
        //        }
        //    }
        //    //}
        //}

        private List<T> DataReaderMapToList<T>(IDataReader dr)
        {
            List<T> list = new List<T>();
            T obj = default(T);
            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();
                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (HasColumn(dr, prop.Name))
                    {
                        prop.SetValue(obj, dr[prop.Name]);
                    }
                    //obj.SetPropertyValue(prop.Name, dr[prop.Name]);

                }
                list.Add(obj);
            }
            return list;
        }

        public string GetSingleValue(string sql)
        {
            //((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 180;
            var res = context.Database.SqlQuery<int?>(sql).FirstOrDefault();
            res = res ?? 0;
            return res + "";
        }

        public string GetSingleStringValue(string sql)
        {
            //using (context = new MBTrackerEntities())
            //{
            using (var command = context.Database.Connection.CreateCommand())
            {
                var strValue = "";
                command.CommandText = sql;
                command.CommandType = CommandType.Text;
                command.CommandTimeout = 300;
                if (context.Database.Connection.State != ConnectionState.Open) context.Database.Connection.Open();
                using (var result = command.ExecuteReader())
                {
                    while (result.Read())
                    {
                        strValue = result[0].ToString();
                        break;
                    }
                }
                return strValue;
            }
            
        }


        private bool HasColumn(IDataReader dr, string columnName)
        {
            try
            {
                return dr.GetOrdinal(columnName) >= 0;
            }
            catch (IndexOutOfRangeException)
            {
                return false;
            }
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public class EntityChange
        {
            public string ChangeType { get; set; }
            public object Entity { get; set; }
            public DbEntityEntry Entry { get; set; }
            public string EntityName { get; set; }

        }



    }
}