﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;
using Repositories;
using MBTracker.EF;

namespace MBTracker.Pages
{
    public partial class Login : System.Web.UI.Page
    {

        UserManager objUser = new UserManager();
        UnitOfWork unitOfWork = new UnitOfWork();

        protected void Page_Load(object sender, EventArgs e)
        {
            string s = Request.QueryString["logout"];
            if (s != null)
            {
                Session.RemoveAll();
                CommonMethods.SessionInfo = null;
                // Response.Redirect("~/Pages/Admin/Dashboard.aspx");
                Response.Redirect("~/Login.aspx");

            }
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            DataTable dt = objUser.GetUserInfo(tbxUserId.Text.Trim(), EncryptionDecryption.Encrypt(tbxPassword.Text.Trim(), true));
            if (dt.Rows.Count > 0)
            {
                CommonMethods.SessionInfo = new MhSessionInfo();
                CommonMethods.SessionInfo.UserId = Convert.ToInt32(dt.Rows[0]["Id"]);
                CommonMethods.SessionInfo.UserName = dt.Rows[0]["UserName"].ToString();
                CommonMethods.SessionInfo.UserFullName = dt.Rows[0]["FullName"].ToString();
                CommonMethods.SessionInfo.RoleId = Convert.ToInt32(dt.Rows[0]["RoleId"]);
                CommonMethods.SessionInfo.DepartmentId = Convert.ToInt32(dt.Rows[0]["Department"]);
                CommonMethods.SessionInfo.RoleName = dt.Rows[0]["RoleName"].ToString();
                var returnUrl = (string)HttpContext.Current.Session["returnUrl"];
                if (!string.IsNullOrEmpty(returnUrl))
                {
                    Response.Redirect(returnUrl);
                }
                else
                {
                    var rolePermisison = unitOfWork.GenericRepositories<RolesDashboard>().Get(x => x.RoleId == CommonMethods.SessionInfo.RoleId).FirstOrDefault();
                    if (rolePermisison != null)
                    {
                        Response.Redirect(rolePermisison.Dashboards.DashboardURL);
                    }
                    else
                    {
                        Response.Redirect("~/Pages/Dashboard.aspx");
                    }
                    //if (CommonMethods.SessionInfo.RoleName.Contains("Knitting") || CommonMethods.SessionInfo.RoleName.Contains("Store"))
                    //{
                    //    Response.Redirect("~/Pages/DashboardKnitting.aspx");
                    //}
                    //else if (CommonMethods.SessionInfo.RoleName.Contains("Finishing") || CommonMethods.SessionInfo.RoleName.Contains("Commercial"))
                    //{
                    //    Response.Redirect("~/Pages/DashboardFinishing.aspx");
                    //}
                    //else
                    //{
                    //    Response.Redirect("~/Pages/Dashboard.aspx");
                    //}
                }                
            }
            else
            {
                FailureText.Text = "Invalid user name or password";
                ScriptManager.RegisterStartupScript(this,this.GetType(), "toastr_message", "toastr.error('Invalid user name or password', 'Error')", true);
            }
        }


    }
}