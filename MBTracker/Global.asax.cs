﻿using MBTracker.Code_Folder;
using Repositories;
using System;
using System.Configuration;
using System.Linq;
using System.Timers;

namespace MBTracker
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
           // Notification();
        }

        protected void Notification()
        {
            var ShipmentIn10DaysHourse = float.Parse(ConfigurationManager.AppSettings["ShipmentIn10Days"]);
            var ShipmentIn10DaysMiliseconds = Tools.HoursToMiliseconds(ShipmentIn10DaysHourse);
            //var ShipmentIn10DaysMiliseconds = 30 * 1000;
            System.Timers.Timer ShipmentInTenDaysTimer = new System.Timers.Timer();
            ShipmentInTenDaysTimer.Interval = ShipmentIn10DaysMiliseconds;
            ShipmentInTenDaysTimer.AutoReset = true;
            ShipmentInTenDaysTimer.Elapsed += new ElapsedEventHandler(ShipmentInTenDays);
            ShipmentInTenDaysTimer.Enabled = true;
        }

        public void ShipmentInTenDays(object source, System.Timers.ElapsedEventArgs e)
        {
            UnitOfWork unitOfWork = new UnitOfWork();
            var lstOrders = unitOfWork.GetRecordSet<ShipmentInfo>($"SELECT Id as ShipmentId,OrderId FROM OrderShipmentDates WHERE ShipmentDate <= DATEADD(DAY,10,GETDATE())").ToList();
            foreach (var item in lstOrders)
            {
                CommonMethods.SendEventNotification(10, 0, 0, item.OrderId, item.ShipmentId);
            }
        }


        public class ShipmentInfo
        {
            public int OrderId { get; set; }
            public int ShipmentId { get; set; }
        }
    }
}