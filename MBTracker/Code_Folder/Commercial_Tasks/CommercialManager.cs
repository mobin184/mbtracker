﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using MBTracker.Code_Folder.DAL;


namespace MBTracker.Code_Folder.Commercial_Tasks
{



    public class CommercialManager
    {

        DatabaseManager dm = new DatabaseManager();

        public int SavePIStyles(DataTable dt)
        {
            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_PIStyles");
        }

        public int SavePICostingSheets(DataTable dt)
        {
            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_PICostingSheets");

        }

    
        public int SaveLCStyles(DataTable dt)
        {
            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_LCStyles");
        }


        public DataTable GetAllYarnAndAccessoriesBuyers()
        {
            return dm.ExecuteQuery("usp_GetAllYarnAndAccessoriesBuyers");
        }


        public DataTable GetAllApplicantBanks()
        {
            return dm.ExecuteQuery("usp_GetAllApplicantBanks");
        }

        public DataTable GetAllLCs()
        {
            return dm.ExecuteQuery("usp_GetAllApplicantLCs");
        }

        public DataTable GetAllPisNotExistInLC()
        {
            return dm.ExecuteQuery("usp_GetAllApplicantPisNotExistinLC");
        }


        public DataTable GetPIsByStyle(string styleId)
        {
            dm.AddParameteres("@StyleIds", styleId);
            return dm.ExecuteQuery("usp_GetPIInfoByStyleIds");
        }

        public DataTable GetYarnAndAccessoriesItemTypes()
        {
            return dm.ExecuteQuery("usp_GetYarnAndAccessoriesItemTypes");
        }

        public DataTable GetYarnAndAccessoriesItemByType(int itemTypeId)
        {
            dm.AddParameteres("@ItemTypeId", itemTypeId);
            return dm.ExecuteQuery("usp_GetYarnAndAccessoriesItemByTypeId");
        }

        public DataTable GetYarnAndAccessoriesItemByOrderOrStyle(int styleId, int orderId)
        {
            dm.AddParameteres("@StyleId", styleId);
            dm.AddParameteres("@OrderId", orderId);
            return dm.ExecuteQuery("usp_GetYarnAndAccessoriesItemByOrderOrStyle");
        }



        public DataTable GetLCBackToBackStatus()
        {
            return dm.ExecuteQuery("usp_GetLCBackToBackStatus");
        }

        public DataTable GetLCBackToBackTypes()
        {
            return dm.ExecuteQuery("usp_GetLCBackToBackTypes");
        }


        public DataTable GetLCBackToBackPurpose()
        {
            return dm.ExecuteQuery("usp_GetLCBackToBackPurpose");
        }



        public DataTable GetPIStatus()
        {
            return dm.ExecuteQuery("usp_GetPIStatus");
        }



        public DataTable CopyCostingSheet(int costingInfoId, string userName)
        {
            dm.AddParameteres("@CostingInfoId", costingInfoId);
            dm.AddParameteres("@UserName", userName);
            return dm.ExecuteQuery("usp_CopyCostingSheetInfo");
        }


    }
}