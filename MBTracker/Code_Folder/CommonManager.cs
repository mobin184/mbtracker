﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using MBTracker.Code_Folder.DAL;


namespace MBTracker.Code_Folder
{
    public class CommonManager
    {
        DatabaseManager dm = new DatabaseManager();


        public DataTable LoadDropdown(string table)
        {
            dm.AddParameteres("@Table", table);
            return dm.ExecuteQuery("usp_GetAllRecords");
        }


        public DataTable LoadOrderStatusDropdown(int departmentId)
        {
            dm.AddParameteres("@DepartmentId", departmentId);
            return dm.ExecuteQuery("usp_GetOrderStatus_ByUserId");
        }

        public DataTable GetFinishingUnitsByUser(int userId)
        {
            dm.AddParameteres("@UserId", userId);
            return dm.ExecuteQuery("usp_GetFinishingUnitByUser");
        }

        public DataTable LoadDropdownById(int id, string table)
        {
            dm.AddParameteres("@Table", table);
            dm.AddParameteres("@BuyerId", id);
            return dm.ExecuteQuery("[usp_GetAllByBuyer]");
        }
        public DataTable LoadOnlyRunningStyleDropdownById(int id, string table)
        {
            dm.AddParameteres("@Table", table);
            dm.AddParameteres("@BuyerId", id);
            return dm.ExecuteQuery("[usp_GetAllRunningStyleByBuyer]");
        }

        public DataTable LoadDropdownSelectedSeasonById(int id, int StyleID)
        {
            dm.AddParameteres("@ID", StyleID);
            dm.AddParameteres("@BuyerId", id);
            return dm.ExecuteQuery("[usp_GetSEASONBYBUYERANDSTYLE]");
        }

        public DataTable LoadDropdownSelectedBookingNumberById(int id, int StyleID)
        {
            dm.AddParameteres("@ID", StyleID);
            dm.AddParameteres("@BuyerId", id);
            return dm.ExecuteQuery("[usp_GetBookingNumberByBuyerAndStyle]");
        }


        public DataTable LoadBuyersCheckBoxList()
        {
            return dm.ExecuteQuery("usp_GetAllBuyers");

        }


        public DataTable LoadSpareCheckBoxList(int machineBrandId)
        {
            dm.AddParameteres("@MachineBrandId", machineBrandId);
            return dm.ExecuteQuery("usp_GetKnittingSpares_ByMachineBrandId");
        }


        public DataTable LoadCostingItems()
        {
            return dm.ExecuteQuery("usp_GetAllCostingItems");
        }

        public DataTable LoadAllYarnFromMegaYarnList()
        {
            return dm.ExecuteQuery("usp_GetAllYarnListForMega");
        }

        public DataTable LoadAllLCFromMegaYarnList()
        {
            return dm.ExecuteQuery("usp_GetAllLCListForMega");
        }

        public DataTable LoadProductionUnits()
        {
            return dm.ExecuteQuery("usp_GetAllProductionUnits");
        }

               
        public DataTable LoadFinishingUnits()
        {
            return dm.ExecuteQuery("usp_GetAllFinishingUnits");
        }

        public DataTable LoadFinishingUnitFloorsCheckBoxList(int finishingUnitId)
        {
            dm.AddParameteres("@FinishingUnitId", finishingUnitId);
            return dm.ExecuteQuery("usp_GetFinishingUnitFloorsByFinishingUnitId");
        }



        public DataTable LoadDeliveryCountriesByBuyer(int buyerId)
        {
            dm.AddParameteres("@BuyerId", buyerId);
            return dm.ExecuteQuery("usp_GetDeliveryCountriesByBuyer");
        }


        public DataTable GetBuyersByUser(int userId)
        {
            dm.AddParameteres("@UserId", userId);
            return dm.ExecuteQuery("usp_GetBuyersByUser");
        }

        public DataTable GetButtonPermission(int userId)
        {
            dm.AddParameteres("@UserId", userId);
            return dm.ExecuteQuery("usp_GetEditDeleteButtonsForRoles");
        }


        public DataTable GetProductionUnitsByUser(int userId)
        {
            dm.AddParameteres("@UserId", userId);
            return dm.ExecuteQuery("usp_GetProductionUnitsByUser");
        }

        


        public DataTable GetFinishingUnitFloorsByUser(int userId)
        {
            dm.AddParameteres("@UserId", userId);
            return dm.ExecuteQuery("usp_GetFinishingUnitFloorsByUser");
        }

        public DataTable GetColorsByStyle(int styleId)
        {
            dm.AddParameteres("@styleId", styleId);
            return dm.ExecuteQuery("usp_GetStyleColorsByStyleId");
        }

        public DataTable GetStyleColorsByStyle(int styleId)
        {
            dm.AddParameteres("@StyleId", styleId);
            return dm.ExecuteQuery("usp_GetStyleColorsByStyleId");
        }


        public DataTable GetStyleSizesByStyle(int styleId)
        {
            dm.AddParameteres("@StyleId", styleId);
            return dm.ExecuteQuery("usp_GetStyleSizesByStyleId");
        }


        public DataTable BuyerWithUser(int? buyerId, int userId)
        {
            dm.AddParameteres("@BuyerId", buyerId);
            dm.AddParameteres("@UserId", userId);
            return dm.ExecuteQuery("usp_GetBuyerWithUser");
        }

        public DataTable GetKnittingEntryUser(int unitId, int shiftId, DateTime knittingDate)
        {
            dm.AddParameteres("@UnitId", unitId);
            dm.AddParameteres("@ShiftId", shiftId);
            dm.AddParameteres("@KnittingDate", knittingDate);
            return dm.ExecuteQuery("usp_GetKnittingEntryUser");
        }


        public DataTable GetAllMerchandisers()
        {
            return dm.ExecuteQuery("usp_GetMerchandisers");
        }

        public DataTable GetMerchandiserLeaders()
        {
            return dm.ExecuteQuery("usp_GetMerchandiserLeaders");
        }

        public DataTable GetCommercialConcerned()
        {
            return dm.ExecuteQuery("usp_GetCommercialConcerned");
        }

    }
}