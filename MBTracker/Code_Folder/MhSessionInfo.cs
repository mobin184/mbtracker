﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MBTracker.Code_Folder
{
    public class MhSessionInfo
    {

        int _DepartmentId;
        int _userId;
        string _userName;
        string _userFullName;
        int _roleId;
        string _roleName;
        int _EmployeeId;
        int _contactId;
        int _rolePriority;

        #region Common Properties

        public string SessionID
        {
            get { return HttpContext.Current.Session.SessionID; }
        }

        #endregion

        #region User Info


        public int UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
            }
        }

        public string UserFullName
        {
            get
            {
                return _userFullName;
            }
            set
            {
                _userFullName = value;
            }
        }


        public int RoleId
        {
            get
            {
                return _roleId;
            }
            set
            {
                _roleId = value;
            }
        }
        public string RoleName
        {
            get
            {
                return _roleName;
            }
            set
            {
                _roleName = value;
            }
        }
        public int RolePriority
        {
            get
            {
                return _rolePriority;
            }
            set
            {
                _rolePriority = value;
            }
        }

        public int EmployeeId
        {
            get
            {
                return _EmployeeId;
            }
            set
            {
                _EmployeeId = value;
            }
        }

        public int ContactId
        {
            get
            {
                return _contactId;
            }
            set
            {
                _contactId = value;
            }
        }

        public int DepartmentId
        {
            get { return _DepartmentId; }
            set { _DepartmentId = value; }
        }


        #endregion



    }
}