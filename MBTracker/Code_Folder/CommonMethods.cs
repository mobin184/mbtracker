﻿using EASendMail;
using MBTracker.Code_Folder.Buyers_and_Orders;
using MBTracker.Code_Folder.Commercial_Tasks;
using MBTracker.Code_Folder.Knitting_Parts;
using MBTracker.EF;
using Newtonsoft.Json;
using Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBTracker.Code_Folder
{
    public class CommonMethods : Page
    {

        UnitOfWork unitOfWork = new UnitOfWork();

        public static MhSessionInfo SessionInfo
        {
            get
            {
                try
                {
                    if (HttpContext.Current.Session["SessionInfo"] != null)
                        return HttpContext.Current.Session["SessionInfo"] as MhSessionInfo;
                    else
                        return null;
                }
                catch (Exception)
                {

                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["SessionInfo"] = value;
            }
        }


        public static void LoadDropdown(DropDownList ddl, string table, int dataTextField, int dataValueField, int selectedIndexValue = 0)
        {
            DataTable dt = new CommonManager().LoadDropdown(table);
            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = selectedIndexValue;
        }



        public static void LoadBuyerDropdownForUser(DropDownList ddl, int dataTextField, int dataValueField, int selectedIndexValue = 0)
        {
            DataTable dt = new CommonManager().GetBuyersByUser(CommonMethods.SessionInfo.UserId);

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = selectedIndexValue;
        }



        public static void LoadDropdownById(DropDownList ddl, int id, string table, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommonManager().LoadDropdownById(id, table);


            //if (dt.Rows.Count > 0)
            //{
            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;
            //}
            //else
            //{
            //    ddl.items.clear();
            //}
        }
        public static void LoadOnlyRunningStyleDropdownById(DropDownList ddl, int id, string table, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommonManager().LoadOnlyRunningStyleDropdownById(id, table);

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;
           
        }

        public static void LoadDropdownSeasonByBuyerAndStyleId(DropDownList ddl, int id, int StyleID, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommonManager().LoadDropdownSelectedSeasonById(id, StyleID);

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            if (ddl.Items.Count > 1)
            {
                ddl.SelectedIndex = 1;
            }
            else
            {
                ddl.SelectedIndex = 0;
            }
           
           
           
        }

        public static void LoadDropdownBookingNumberByBuyerAndStyleId(DropDownList ddl, int id, int StyleID, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommonManager().LoadDropdownSelectedBookingNumberById(id, StyleID);

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            if (ddl.Items.Count > 1)
            {
                ddl.SelectedIndex = 1;
            }
            else
            {
                ddl.SelectedIndex = 0;
            }

        }


        public static void LoadOrderDropdownByStyle(DropDownList ddl, int styleId, int dataTextField, int dataValueField)
        {
            DataTable dt = new OrderManager().GetOrdersByStyle(styleId);

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }


        public static void LoadYarnAndAccessoriesItemDropdownByType(DropDownList ddl, int itemTypeId, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommercialManager().GetYarnAndAccessoriesItemByType(itemTypeId);

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }


        public static void LoadYarnAndAccessoriesItemDropdownByOrderOrStyle(DropDownList ddl, int styleId, int orderId, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommercialManager().GetYarnAndAccessoriesItemByOrderOrStyle(styleId, orderId);

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }




        public static void LoadPIDropdownByStyle(DropDownList ddl, int styleId, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommercialManager().GetPIsByStyle(styleId.ToString());

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }

        public static void LoadLCBToBStatusDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommercialManager().GetLCBackToBackStatus();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }

        public static void LoadLCTypeDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommercialManager().GetLCBackToBackTypes();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            //ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }

        public static void LoadLCTypeDropdownForRpt(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommercialManager().GetLCBackToBackTypes();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }



        public static void LoadLCPurposeDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommercialManager().GetLCBackToBackPurpose();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }




        public static void PIStatusDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommercialManager().GetPIStatus();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
           // ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }

        public static void PIStatusDropdownOnlyOpenandapprove(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommercialManager().GetPIStatus();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.Items[3].Enabled = false;
            ddl.Items[4].Enabled = false;
            ddl.Items[5].Enabled = false;
            ddl.Items[6].Enabled = false;
            ddl.SelectedIndex = 1;

        }



        public static void LoadMachineBrandDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new OrderManager().GetMachineBrands();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }


        public static void LoadYarnAndAccessoriesSupplierDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new OrderManager().GetAllYarnAndAccessoriesSuppliers();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;
        }

        public static void LoadYarnAndAccessoriesPurchaseType(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new OrderManager().GetAllYarnAndAccessoriesPurchaseType();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }

        public static void LoadLCBySupplier(DropDownList ddl, int dataTextField, int dataValueField,int supplierId)
        {
            DataTable dt = new OrderManager().GetLCBySupplierId(supplierId);

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }


        public static void LoadYarnLCBySupplierAndStyle(DropDownList ddl, int dataTextField, int dataValueField, int supplierId, int styleId)
        {
            DataTable dt = new OrderManager().GetYarnLCBySupplierAndStyleId(supplierId, styleId);

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }

        public static void LoadDyeingOrderBySupplier(DropDownList ddl, int dataTextField, int dataValueField, int supplierId)
        {
            DataTable dt = new OrderManager().GetDyeingOrderBySupplierId(supplierId);

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }

        public static void LoadMerchandiserDropDown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommonManager().GetAllMerchandisers();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }

        public static void LoadMerchandiserLeaderDropDown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommonManager().GetMerchandiserLeaders();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }

        public static void LoadCommercialConceredDropDown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommonManager().GetCommercialConcerned();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }



        public static void LoadYarnAndAccessoriesBuyerDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommercialManager().GetAllYarnAndAccessoriesBuyers();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }





        public static void LoadApplicantBanksDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommercialManager().GetAllApplicantBanks();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }


        public static void LoadAllLCDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommercialManager().GetAllLCs();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }

        public static void LoadAllPIDropdownNotExistInLC(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommercialManager().GetAllPisNotExistInLC();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }


        public static void LoadYarnAndAccessoriesItemTypeDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new OrderManager().GetAllYarnAndAccessoriesRawMaterialItemTypes();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }


        public static void LoadYarnAndAccessoriesItemsDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommonManager().LoadCostingItems();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }

        public static void LoadYarnForMegaDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommonManager().LoadAllYarnFromMegaYarnList();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }
        public static void LoadLcNumberForMegaDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new CommonManager().LoadAllLCFromMegaYarnList();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }
        //public static void LoadYarnAndAccessoriesItemsUnitDropdown(DropDownList ddl)
        //{
        //    DataTable dt = unitOfWork.GetDataTableFromSql("");

        //    ddl.Items.Add(new ListItem("", ""));
        //    ddl.DataSource = dt;
        //    ddl.DataTextField = dt.Columns[dataTextField].ToString();
        //    ddl.DataValueField = dt.Columns[dataValueField].ToString();
        //    ddl.DataBind();
        //    ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
        //    ddl.SelectedIndex = 0;

        //}




        public static void LoadKnittingUnitDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            //DataTable dt = new CommonManager().LoadProductionUnits();

            DataTable dt = new CommonManager().GetProductionUnitsByUser(Convert.ToInt32(CommonMethods.SessionInfo.UserId));

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }


        public static void LoadPartTypeDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new PartsManager().GetKnittingPartTypes();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;
        }

        public static void LoadSpareNamesDropdown(DropDownList ddl, int dataTextField, int dataValueField, int spareTypeId, int machineBrandId)
        {
            DataTable dt = new PartsManager().GetExistingKnittingSparesByTypeAndMachineBrand(spareTypeId, machineBrandId);

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;
        }


        public static void LoadAllTypesSpareNamesDropdown(DropDownList ddl, int dataTextField, int dataValueField, int machineBrandId)
        {
            DataTable dt = new PartsManager().GetExistingKnittingSparesByMachineBrand(machineBrandId);

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;
        }


        public static void LoadKnittingMachinesDropdown(DropDownList ddl, int dataTextField, int dataValueField, int machineBrandId, int knittingUnitId)
        {
            DataTable dt = new PartsManager().GetExistingKnittingMachines(machineBrandId, knittingUnitId);

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;
        }


        public static void LoadPartSuppliersDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new PartsManager().GetKnittingMachineSpareSuppliers();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;
        }

        public static void LoadSpareBrandsDropdown(DropDownList ddl, int dataTextField, int dataValueField, int knittingSpareTypeId)
        {
            DataTable dt = new PartsManager().GetKnittingMachineSpareBrands(knittingSpareTypeId);

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            //ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;
        }




        public static void LoadPartManufacturersDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new PartsManager().GetPartManufacturers();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;
        }


        public static void LoadWhyNeedleChangeDropdown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new PartsManager().GetNeedleChangeReasons();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select---", string.Empty));
            ddl.SelectedIndex = 0;

        }


        public static void LoadProductionMonthDropDown(DropDownList ddl, int dataTextField, int dataValueField)
        {
            DataTable dt = new OrderManager().GetProductionMonths();

            ddl.Items.Add(new ListItem("", ""));
            ddl.DataSource = dt;
            ddl.DataTextField = dt.Columns[dataTextField].ToString();
            ddl.DataValueField = dt.Columns[dataValueField].ToString();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("---Select Month---", string.Empty));
            ddl.SelectedIndex = 0;

        }


        public static void LoadFinishingFloorDropDown(DropDownList ddlUserFinishingUnitFloor)
        {
            DataTable dtUsersFinishingFloor = new CommonManager().GetFinishingUnitFloorsByUser(CommonMethods.SessionInfo.UserId);
            ddlUserFinishingUnitFloor.DataSource = dtUsersFinishingFloor;
            ddlUserFinishingUnitFloor.DataTextField = "FloorName";
            ddlUserFinishingUnitFloor.DataValueField = "FinishingUnitFloorId";
            ddlUserFinishingUnitFloor.DataBind();

        }



        public static void LoadTNADropDown(DropDownList ddlTNA, int buyerId, int styleId, int yearId)
        {
            //var userId = CommonMethods.SessionInfo.UserId;
            //DataTable dt = unitOfWork.GetDataTableFromSql($"EXEC usp_GetAllTimeAndActionPlans '{buyerId}','{styleId}','{yearId}',{userId}");


            //ddlTNA.DataSource = dtUsersFinishingFloor;
            //ddlTNA.DataTextField = "FloorName";
            //ddlTNA.DataValueField = "FinishingUnitFloorId";
            //ddlTNA.DataBind();

        }



        public static void LoadSpareCheckBoxList(CheckBoxList cbl, int machineBrandId, Label lblListOfSpareMessage, Button addRows)
        {
            DataTable dt = new CommonManager().LoadSpareCheckBoxList(machineBrandId);

            if (dt.Rows.Count > 0)
            {


                addRows.Visible = true;
                lblListOfSpareMessage.Text = "List of spares:";
                cbl.DataValueField = "Id";
                cbl.DataTextField = "SpareName";

                cbl.DataSource = dt;
                cbl.DataBind();

            }
            else
            {
                addRows.Visible = false;
                lblListOfSpareMessage.Text = "No spare found.";
            }
        }


        public static void LoadCostingItemsCheckBoxList(CheckBoxList cbl)
        {
            DataTable dt = new CommonManager().LoadCostingItems();

            if (dt.Rows.Count > 0)
            {
                cbl.DataValueField = "Id";
                cbl.DataTextField = "ItemName";

                cbl.DataSource = dt;
                cbl.DataBind();

            }
            //else
            //{
            //    addRows.Visible = false;
            //    lblListOfSpareMessage.Text = "No spare found.";
            //}
        }




        public static void LoadBuyersCheckBoxList(CheckBoxList cbl)
        {
            DataTable dt = new CommonManager().LoadBuyersCheckBoxList();
            if (dt.Rows.Count > 0)
            {

                cbl.DataValueField = "Id";
                cbl.DataTextField = "BuyerName";
                cbl.DataSource = dt;
                cbl.DataBind();

            }
        }



        public static void LoadProductionUnitsCheckBoxList(CheckBoxList cbl)
        {
            DataTable dt = new CommonManager().LoadProductionUnits();
            if (dt.Rows.Count > 0)
            {

                cbl.DataValueField = "Id";
                cbl.DataTextField = "UnitName";
                cbl.DataSource = dt;
                cbl.DataBind();


            }
        }


        public static void LoadFinishingUnits(Repeater rpt)
        {
            DataTable dt = new CommonManager().LoadFinishingUnits();

            if (dt.Rows.Count > 0)
            {
                rpt.DataSource = dt;
                rpt.DataBind();
            }
        }



        //public static void LoadStyleCheckBoxList(CheckBoxList cbl, int buyerId)
        //{
        //    DataTable dt = buyerManager.LoadStylesByBuyer(int.Parse(ddlBuyers.SelectedValue));


        //    if (dt.Rows.Count > 0)
        //    {

        //        //for (int i = 0; i < dt.Rows.Count; i++)
        //        //{
        //        //    dt.Rows[i]["FloorName"] = "&nbsp;&nbsp;" + dt.Rows[i]["FloorName"] + "&nbsp;&nbsp;&nbsp;";
        //        //}

        //        cbl.DataValueField = "Id";
        //        cbl.DataTextField = "FloorName";
        //        cbl.DataSource = dt;
        //        cbl.DataBind();


        //    }
        //}



        public static void LoadFinishingUnitFloorsCheckBoxList(CheckBoxList cbl, int finishingUnitId)
        {
            DataTable dt = new CommonManager().LoadFinishingUnitFloorsCheckBoxList(finishingUnitId);
            if (dt.Rows.Count > 0)
            {

                cbl.DataValueField = "Id";
                cbl.DataTextField = "FloorName";
                cbl.DataSource = dt;
                cbl.DataBind();
            }
        }






        public static void LoadStyleSizeCheckBoxList(CheckBoxList cbl, int styleId, Label notFound, Panel panelStyleSizes)
        {
            DataTable dt = new CommonManager().GetStyleSizesByStyle(styleId);


            if (dt.Rows.Count > 0)
            {

                cbl.DataValueField = "Id";
                cbl.DataTextField = "SizeName";
                cbl.DataSource = dt;
                cbl.DataBind();
                notFound.Visible = false;
                panelStyleSizes.Visible = true;

                cbl.Visible = true;

            }
            else
            {
                notFound.Visible = true;
                cbl.Visible = false;
            }
        }


        public static void LoadDeliveryCountriesCheckBoxList(CheckBoxList cbl, int buyerId, Label notFound, Button selectAll, Button unSelectAll, Button saveOrder)
        {
            DataTable dt = new CommonManager().LoadDeliveryCountriesByBuyer(buyerId);
            if (dt.Rows.Count > 0)
            {

                cbl.DataValueField = "Id";
                cbl.DataTextField = "DeliveryCountryCode";
                cbl.DataSource = dt;
                cbl.DataBind();
                notFound.Visible = false;
                selectAll.Visible = true;
                unSelectAll.Visible = true;
                cbl.Visible = true;
                saveOrder.Visible = true;
            }
            else
            {
                notFound.Visible = true;
                selectAll.Visible = false;
                unSelectAll.Visible = false;
                cbl.Visible = false;
                saveOrder.Visible = false;


            }
        }

        public static void SelectBuyersCheckBoxListItems(CheckBoxList cbl, int userId)
        {
            DataTable dt = new CommonManager().GetBuyersByUser(userId);

            foreach (ListItem item in cbl.Items)
            {

                foreach (DataRow row in dt.Rows)
                {
                    if (item.Value.ToString() == row["BuyerId"].ToString())
                    {
                        item.Selected = true;
                    }
                }
            }

        }


        public static void SelectProductionUnitsCheckBoxListItems(CheckBoxList cbl, int userId)
        {
            DataTable dt = new CommonManager().GetProductionUnitsByUser(userId);

            foreach (ListItem item in cbl.Items)
            {

                foreach (DataRow row in dt.Rows)
                {
                    if (item.Value.ToString() == row["ProductionUnitId"].ToString())
                    {
                        item.Selected = true;
                    }
                }
            }

        }


        public static void SelectFinishingUnitFloorsCheckBoxListItems(Repeater rptFinishingUnits, int userId)
        {
            DataTable dt = new CommonManager().GetFinishingUnitFloorsByUser(userId);


            for (int i = 0; i < rptFinishingUnits.Items.Count; i++)
            {
                CheckBoxList cbl = (CheckBoxList)rptFinishingUnits.Items[i].FindControl("cblFinishingUnitFloors");

                foreach (ListItem item in cbl.Items)
                {

                    foreach (DataRow row in dt.Rows)
                    {
                        if (item.Value.ToString() == row["FinishingUnitFloorId"].ToString())
                        {
                            item.Selected = true;
                        }
                    }
                }
            }
        }





        public static int MatchDropDownItem(DropDownList ddl, int itemValue)
        {
            int dropDownIndexValue = 0;

            foreach (ListItem li in ddl.Items)
            {
                if (li.Value == itemValue.ToString())
                {
                    break;
                }
                else
                    dropDownIndexValue++;
            }
            if (dropDownIndexValue == ddl.Items.Count)
            {
                dropDownIndexValue = 0;
            }
            return dropDownIndexValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userRoles">comma separated user roles</param>
        /// <returns></returns>
        public static bool CurrentUserRollIn(string UserRoles)
        {
            var isExist = false;
            string[] userRoles = UserRoles.Split(',');
            var currentUserRole = SessionInfo.RoleName;
            foreach (var item in userRoles)
            {
                if (currentUserRole.Contains(item.Trim()))
                { 
                    isExist = true;
                    break;
                }
            }
            return isExist;
        }

        public static bool CurrentUserRollIn2(string pageName, int controlType, int buttonNumberOnpage)
        {
            var isExist = false;
            var userId = CommonMethods.SessionInfo.UserId;
            string sql = $"Exec usp_GetRoleIdsForEditDeleteButton '{pageName}', {controlType}, {buttonNumberOnpage}";

            UnitOfWork unitOfWork = new UnitOfWork();
            var dt = unitOfWork.GetDataTableFromSql(sql);
            var currentUserRoleId = SessionInfo.RoleId;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (int.Parse(dt.Rows[i][0].ToString()) == currentUserRoleId)
                {
                    isExist = true;
                    break;
                }
            }
            
            return isExist;
        }

        public static int GetSizeQuantityByStyleAndColor(int styleId, int buyerColorId, int sizeId)
        {
            UnitOfWork unitOfWork = new UnitOfWork();
            //var res = unitOfWork.GetSingleValue($"SELECT MAX(ISNULL(ocs.Quantity,0)) FROM  Orders o INNER JOIN OrderColors oc ON o.Id = oc.OrderId INNER JOIN OrderColorSizes ocs ON oc.Id = ocs.OrderColorId WHERE o.StyleId = {styleId} AND o.IsActive = 1 AND oc.BuyerColorId = {buyerColorId} AND ocs.SizeId = {sizeId}");
            var res = unitOfWork.GetSingleValue($"SELECT MAX(ISNULL(kidtfucs.DeliveredQty,0)) FROM KnittedItemDelvierdToFinishingUnit kidtfu INNER JOIN KnittedItemDelvierdToFinishingUnitColorSizes kidtfucs ON kidtfu.Id = kidtfucs.KnittedDeliveredId WHERE  kidtfucs.BuyerColorId = {buyerColorId} AND kidtfu.StyleId = {styleId}  AND kidtfucs.SizeId = {sizeId} ");

            return int.Parse(string.IsNullOrEmpty(res) ? "0" : res);
        }

        public void SendEmailAsync(string subject, List<string> listTo, List<string> listCC, List<System.Net.Mail.Attachment> listAttachments, string body)
        {
            try
            {
                UnitOfWork unitOfWork = new UnitOfWork();
                MailMessage mail = new MailMessage();
                System.Net.Mail.SmtpClient SmtpServer = new System.Net.Mail.SmtpClient("smtp.masihata.com");

                mail.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["EmailAddress"]);
                foreach (var to in listTo)
                {
                    mail.To.Add(to);
                }
                foreach (var cc in listCC)
                {
                    mail.CC.Add(cc);
                }
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                foreach (var attachment in listAttachments)
                {
                    mail.Attachments.Add(attachment);
                }
                SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"], ConfigurationManager.AppSettings["EmailPassword"]);
                SmtpServer.EnableSsl = (SmtpServer.Port == 465);
                if (listTo.Count > 0)
                {
                    //SmtpServer.Send(mail);
                    SmtpServer.SendMailAsync(mail);
                    var eh = new EmailHistory()
                    {
                        Subject = subject,
                        To = string.Join(",", listTo.ToArray()),
                        Message = body,
                        CreatedBy = SessionInfo != null ? SessionInfo.UserName : "System",
                        CreateDate = DateTime.Now
                    };
                    unitOfWork.GenericRepositories<EmailHistory>().Insert(eh);
                    unitOfWork.Save();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void SendEmailWithES(string subject, List<string> listTo, List<string> listCC, List<EASendMail.Attachment> listAttachments, string body)
        {
            try
            {
                UnitOfWork unitOfWork = new UnitOfWork();
                MailMessage mail = new MailMessage();
                SmtpMail oMail = new SmtpMail("TryIt");

                // Set sender email address, please change it to yours
                oMail.From = "shohid@masihata.com";
                // Set recipient email address, please change it to yours
                //oMail.To = "support@emailarchitect.net";
                foreach (var to in listTo)
                {
                    oMail.To.Add(to);
                }
                // Set email subject
                oMail.Subject = subject;
                // Set email body
                oMail.HtmlBody = body;

                // SMTP server address
                SmtpServer oServer = new SmtpServer("smtp.masihata.com");

                // User and password for ESMTP authentication
                oServer.User = "mbtracker@masihata.com";
                oServer.Password = "Password@123";

                // Most mordern SMTP servers require SSL/TLS connection now.
                // ConnectTryTLS means if server supports SSL/TLS, SSL/TLS will be used automatically.
                //oServer.ConnectType = SmtpConnectType.ConnectTryTLS;

                // If your SMTP server uses 587 port
                // oServer.Port = 587;

                // If your SMTP server requires SSL/TLS connection on 25/587/465 port
                // oServer.Port = 25; // 25 or 587 or 465
                oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;

                Console.WriteLine("start to send email ...");

                EASendMail.SmtpClient oSmtp = new EASendMail.SmtpClient();



                if (listTo.Count > 0)
                {
                    oSmtp.SendMail(oServer, oMail);
                    var eh = new EmailHistory()
                    {
                        Subject = subject,
                        To = string.Join(",", listTo.ToArray()),
                        Message = body,
                        CreatedBy = SessionInfo.UserName,
                        CreateDate = DateTime.Now
                    };
                    unitOfWork.GenericRepositories<EmailHistory>().Insert(eh);
                    unitOfWork.Save();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void SendSpareRequestNotification(int eventId, string knittingUnitName, string machineBrandName, int recordId)
        {
            UnitOfWork unitOfWork = new UnitOfWork();
            List<string> listTo = new List<string>();
            List<string> listMobileNo = new List<string>();
            var smsTest = "";
            string subject = "";
            string body = "";
            var userFullName = SessionInfo != null ? SessionInfo.UserFullName : "";
            string baseUrl = HttpContext.Current != null ? (HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/") : ConfigurationManager.AppSettings["AppBaseUrl"];

            CommonMethods commonMethods = new CommonMethods();


            var eventInfo = unitOfWork.GenericRepositories<NotificationEvents>().GetByID(eventId);
            subject = eventInfo.EmailSubject;
            body = eventInfo.EmailBody;
            smsTest = eventInfo.SMSText;

            listTo = unitOfWork.GetRecordSet<string>($"EXEC usp_GetEmailListByEvent {eventId}, 1").ToList();
            listMobileNo = unitOfWork.GetRecordSet<string>($"EXEC usp_GetPhoneListByEvent {eventId}, 1").ToList();


            subject = subject.Replace("<Unit:>", $"{knittingUnitName}").Replace("<MachineBrand:>", $"Machine Brand: {machineBrandName}");
            body = body.Replace("<User Full Name>", $"{userFullName}").Replace("<URL>", $"<a href='{baseUrl}/Pages/Knitting_Spares/ViewKnittingMachineSpareRequests.aspx?showInfo={Tools.UrlEncode(recordId + "")}' target='_blank'> here</a>");
            smsTest = subject;

            string notificaitonMode = ConfigurationManager.AppSettings["NotificationMode"];
            if (notificaitonMode == "EMAIL")
            {
                commonMethods.SendEmailAsync(subject, listTo, new List<string>(), new List<System.Net.Mail.Attachment>(), body);
            }
            else if (notificaitonMode == "SMS")
            {
                foreach (var mobileNo in listMobileNo)
                {
                    SendSMS(mobileNo, smsTest);
                }
            }
            else if (notificaitonMode == "BOTH")
            {
                foreach (var mobileNo in listMobileNo)
                {
                    SendSMS(mobileNo, smsTest);
                }
                commonMethods.SendEmailAsync(subject, listTo, new List<string>(), new List<System.Net.Mail.Attachment>(), body);
            }


        }


        public static void SendUDDateReminderNotification(int eventId, int buyerId, string lcNumber, DateTime udDate, int udStatus)
        {
            UnitOfWork unitOfWork = new UnitOfWork();
            List<string> listTo = new List<string>();
            List<string> listMobileNo = new List<string>();
            var smsTest = "";
            string subject = "";
            string body = "";
            var userFullName = SessionInfo != null ? SessionInfo.UserFullName : "";
            string baseUrl = HttpContext.Current != null ? (HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/") : ConfigurationManager.AppSettings["AppBaseUrl"];

            CommonMethods commonMethods = new CommonMethods();


            var eventInfo = unitOfWork.GenericRepositories<NotificationEvents>().GetByID(eventId);
            subject = eventInfo.EmailSubject;
            body = eventInfo.EmailBody;
            smsTest = eventInfo.SMSText;

            listTo = unitOfWork.GetRecordSet<string>($"EXEC usp_GetEmailListByEvent {eventId}, {buyerId}").ToList();
            listMobileNo = unitOfWork.GetRecordSet<string>($"EXEC usp_GetPhoneListByEvent {eventId}, {buyerId}").ToList();


            subject = subject.Replace("<LC Number>", $"{lcNumber}").Replace("<UDDeadLine>", $"{String.Format("{0:dd-MMM-yyyy}", udDate)}");

            if (udStatus == 1)
            {
                body = body.Replace("<date>", $"{String.Format("{0:dd-MMM-yyyy}", udDate)}").Replace("<URL>", $"<a href='{baseUrl}/Pages/Commercial_Tasks/ViewEditLCInformation.aspx?showInfo={Tools.UrlEncode(lcNumber + "")}' target='_blank'> here</a>");
            }
            else
            {
                body = "Utilization Declaration (UD) for the above mentioned LC number was completed. If necessary, please review the LC information at: " + $"<a href='{baseUrl}/Pages/Commercial_Tasks/ViewEditLCInformation.aspx?showInfo={Tools.UrlEncode(lcNumber + "")}' target='_blank'> here</a>";
            }
            
            smsTest = subject;

            string notificaitonMode = ConfigurationManager.AppSettings["NotificationMode"];
            if (notificaitonMode == "EMAIL")
            {
                commonMethods.SendEmailAsync(subject, listTo, new List<string>(), new List<System.Net.Mail.Attachment>(), body);
            }
            else if (notificaitonMode == "SMS")
            {
                foreach (var mobileNo in listMobileNo)
                {
                    SendSMS(mobileNo, smsTest);
                }
            }
            else if (notificaitonMode == "BOTH")
            {
                foreach (var mobileNo in listMobileNo)
                {
                    SendSMS(mobileNo, smsTest);
                }
                commonMethods.SendEmailAsync(subject, listTo, new List<string>(), new List<System.Net.Mail.Attachment>(), body);
            }


        }



        public static void SendEventNotification(int eventId, int? buyerId, int? styleId, int? orderId, int recordId)
        {
            UnitOfWork unitOfWork = new UnitOfWork();
            List<string> listTo = new List<string>();
            List<string> listMobileNo = new List<string>();
            var smsTest = "";
            string subject = "";
            string body = "";
            var buyerName = "";
            var styleName = "";
            var orderNumber = "";
            var userFullName = SessionInfo != null ? SessionInfo.UserFullName : "";
            string baseUrl = HttpContext.Current != null ? (HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/") : ConfigurationManager.AppSettings["AppBaseUrl"];

            if (buyerId != null && buyerId != 0)
            {
                var buyerInfo = unitOfWork.GenericRepositories<Buyers>().GetByID(buyerId);
                buyerName = buyerInfo.BuyerName;
            }

            if (styleId != null && styleId != 0)
            {
                var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(styleId);
                styleName = styleInfo.StyleName;

                if (buyerName == "")
                {
                    var buyerInfo = unitOfWork.GenericRepositories<Buyers>().GetByID(styleInfo.BuyerId);
                    buyerName = buyerInfo.BuyerName;
                }
            }

            if (orderId != null && orderId != 0)
            {
                var orderInfo = unitOfWork.GenericRepositories<Orders>().GetByID(orderId);
                orderNumber = orderInfo.OrderNumber;

                if (styleName == "")
                {
                    var styleInfo = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(orderInfo.StyleId);
                    styleName = styleInfo.StyleName;
                }
                if (buyerName == "")
                {
                    var buyerInfo = unitOfWork.GenericRepositories<Buyers>().GetByID(orderInfo.BuyerId);
                    buyerName = buyerInfo.BuyerName;
                }
            }
            var eventInfo = unitOfWork.GenericRepositories<NotificationEvents>().GetByID(eventId);
            subject = eventInfo.EmailSubject;
            body = eventInfo.EmailBody;
            smsTest = eventInfo.SMSText;



            listTo = unitOfWork.GetRecordSet<string>($"EXEC usp_GetEmailListByEvent {eventId}, {buyerId}").ToList();
            listMobileNo = unitOfWork.GetRecordSet<string>($"EXEC usp_GetPhoneListByEvent {eventId}, {buyerId}").ToList();


            //listTo = unitOfWork.GetRecordSet<string>($"SELECT DISTINCT Email FROM UserNotificationEvents une INNER JOIN Users u ON une.UserId = u.Id WHERE une.NotificationEventId = {eventId}").ToList();
            //listMobileNo = unitOfWork.GetRecordSet<string>($"SELECT DISTINCT Phone FROM UserNotificationEvents une INNER JOIN Users u ON une.UserId = u.Id WHERE une.NotificationEventId = {eventId}").ToList();



            CommonMethods commonMethods = new CommonMethods();

            if (eventId == 1) //Update Order
            {
                subject = subject.Replace("<Buyer:>", $"Buyer:{buyerName}").Replace("<Style:>", $"Style: {styleName}").Replace("<Order Number:>", $"Order Number: {orderNumber}");
                body = body.Replace("<User Full Name>", $"{userFullName}").Replace("<URL>", $"<a href='{baseUrl}/Pages/DashBoard.aspx?showInfo={Tools.UrlEncode(recordId + "")}' target='_blank'> here</a>");
                smsTest = subject;
            }
            else if (eventId == 2)//Update Shipment Date
            {
                subject = subject.Replace("<Buyer:>", $"Buyer:{buyerName}").Replace("<Style:>", $"Style: {styleName}").Replace("<Order Number:>", $"Order Number: {orderNumber}");
                body = body.Replace("<User Full Name>", $"{userFullName}").Replace("<URL>", $"<a href='{baseUrl}/Pages/Buyers_and_Orders/ViewShipments.aspx?showInfo={Tools.UrlEncode(recordId + "")}' target='_blank'> here</a>");
                smsTest = subject;
            }
            else if (eventId == 3)//Update Consumption Info
            {
                subject = subject.Replace("<Buyer:>", $"Buyer:{buyerName}").Replace("<Style:>", $"Style: {styleName}");
                body = body.Replace("<User Full Name>", $"{userFullName}").Replace("<URL>", $"<a href='{baseUrl}/Pages/Buyers_and_Orders/ViewConsumptionSheets.aspx?showInfo={Tools.UrlEncode(recordId + "")}' target='_blank'> here</a>");
                smsTest = subject;
            }
            else if (eventId == 4)//Update Booking
            {

                subject = subject.Replace("<Buyer:>", $"Buyer:{buyerName}").Replace("<Style:>", $"Style: {styleName}");
                body = body.Replace("<User Full Name>", $"{userFullName}").Replace("<URL>", $"<a href='{baseUrl}/Pages/Target_and_Bookings/ViewBookings.aspx?showInfo={Tools.UrlEncode(recordId + "")}' target='_blank'> here</a>");
                smsTest = subject;
            }
            else if (eventId == 5)//Update TNA
            {

                subject = subject.Replace("<Buyer:>", $"Buyer:{buyerName}").Replace("<Style:>", $"Style: {styleName}");
                body = body.Replace("<User Full Name>", $"{userFullName}").Replace("<URL>", $"<a href='{baseUrl}/Pages/Buyers_and_Orders/ViewTimeAndActionPlans.aspx?showInfo={Tools.UrlEncode(recordId + "")}' target='_blank'> here</a>");
                smsTest = subject;
            }
            else if (eventId == 7)//Update Knitting plan
            {

                subject = subject.Replace("<Buyer:>", $"Buyer:{buyerName}").Replace("<Style:>", $"Style: {styleName}");
                body = body.Replace("<User Full Name>", $"{userFullName}").Replace("<URL>", $"<a href='{baseUrl}/Pages/Productions/ViewKnittingPlans.aspx?showInfo={Tools.UrlEncode(styleId + "")}' target='_blank'> here</a>");
                smsTest = subject;
            }
            else if (eventId == 9) //Problem Reporting
            {
                var problemInfo = unitOfWork.GenericRepositories<ReportedProblems>().GetByID(recordId);
                var reportingSatage = unitOfWork.GenericRepositories<PossibleProblemReportingStages>().GetByID(problemInfo.ReportedStageId);
                subject = subject.Replace("<Reporting Stage>", $"{reportingSatage.StageName}").Replace("<Buyer:>", $"Buyer:{buyerName}").Replace("<Style:>", $"Style: {styleName}");
                body = body.Replace("<Problem Description>", $"{problemInfo.ProblemDescription}").Replace("<User Full Name>", $"{userFullName}").Replace("<URL>", $"<a href='{baseUrl}/Pages/Productions/ViewReportedProblems.aspx?showInfo={Tools.UrlEncode(recordId + "")}' target='_blank'> here</a>");
                smsTest = subject;
            }
            else if (eventId == 10) //Shipment in 10 days
            {
                subject = subject.Replace("<Buyer:>", $"Buyer:{buyerName}").Replace("<Style:>", $"Style: {styleName}").Replace("<Order Number:>", $"Order Number: {orderNumber}");
                body = body.Replace("<User Full Name>", $"{userFullName}").Replace("<URL>", $"<a href='{baseUrl}/Pages/Buyers_and_Orders/ViewShipments.aspx?showInfo={Tools.UrlEncode(recordId + "")}' target='_blank'> here</a>");
                smsTest = subject;
            }
            else if (eventId == 11) //Note to TNA was added
            {
                subject = subject.Replace("<Buyer:>", $"Buyer:{buyerName}").Replace("<Style:>", $"Style: {styleName}");
                body = body.Replace("<User Full Name>", $"{userFullName}").Replace("<URL>", $"<a href='{baseUrl}/Pages/Productions/AddTimeAndActionPlanNote.aspx?TNAId={recordId}' target='_blank'> here</a>");
                smsTest = subject;
            }

            string notificaitonMode = ConfigurationManager.AppSettings["NotificationMode"];
            if (notificaitonMode == "EMAIL")
            {
                commonMethods.SendEmailAsync(subject, listTo, new List<string>(), new List<System.Net.Mail.Attachment>(), body);
            }
            else if (notificaitonMode == "SMS")
            {
                foreach (var mobileNo in listMobileNo)
                {
                    SendSMS(mobileNo, smsTest);
                }
            }
            else if (notificaitonMode == "BOTH")
            {
                foreach (var mobileNo in listMobileNo)
                {
                    SendSMS(mobileNo, smsTest);
                }
                commonMethods.SendEmailAsync(subject, listTo, new List<string>(), new List<System.Net.Mail.Attachment>(), body);
            }


            //using EASendMail
            //commonMethods.SendEmailWithES(subject, listTo, new List<string>(), new List<EASendMail.Attachment>(), body);
        }



        public static void SendSMS(string MobileNo, string Message)
        {
            try
            {
                UnitOfWork unitOfWork = new UnitOfWork();
                // var template = new SMSTemplate { to = "88" + MobileNo, text = Message };
                var template = new SMSTemplate
                {
                    api_key = "C20052915df8a7e8622971.51154194",
                    type = "sms",
                    senderid = "30958",
                    contacts = "88" + MobileNo,
                    msg = Message
                };
                string key = ConfigurationManager.AppSettings["SMSKey"];
                string url = ConfigurationManager.AppSettings["SMSurl"];
                using (var httpClient = new HttpClient())
                {
                    string contents = JsonConvert.SerializeObject(template);
                    url += "?api_key=C20052915df8a7e8622971.51154194&type=unicode&contacts=88" + MobileNo + "&senderid=30958&msg=" + HttpUtility.UrlEncode(Message);
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("api_key", "C20052915df8a7e8622971.51154194");
                    var response = httpClient.GetAsync(url);
                    response.Wait();
                    if (response.Result.IsSuccessStatusCode)
                    {
                        //SmS Log
                        SMSHistory history = new SMSHistory()
                        {
                            To = MobileNo,
                            SMSText = Message,
                            CreateDate = DateTime.Now,
                            CreatedBy = SessionInfo != null ? SessionInfo.UserName : "System"
                        };
                        unitOfWork.GenericRepositories<SMSHistory>().Insert(history);
                        unitOfWork.Save();
                    }
                    else
                    {
                        //Message Error Log
                        SendingFailedSMS failedSMS = new SendingFailedSMS()
                        {
                            To = MobileNo,
                            SMSText = Message,
                            Error = response.Result.StatusCode + "",
                            CreateDate = DateTime.Now,
                            CreatedBy = SessionInfo != null ? SessionInfo.UserName : "System"
                        };
                        unitOfWork.GenericRepositories<SendingFailedSMS>().Insert(failedSMS);
                        unitOfWork.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public class SMSTemplate
        //{
        //    public string from = "Masihata";
        //    public string to { get; set; }
        //    public string text { get; set; }
        //}
        public class SMSTemplate
        {
            public string api_key { get; set; }
            public string type { get; set; }
            public string contacts { get; set; }
            public string senderid { get; set; }
            public string msg { get; set; }
        }



        public static bool HasBuyerAssignedToUser(int? buyerId, int userId)
        {
            DataTable dt = new CommonManager().BuyerWithUser(buyerId, userId);

            if (dt.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void LoadProductionUnitsByUser(DropDownList ddlUserProductionUnit)
        {

            var dtKnittingUnit = new CommonManager().GetProductionUnitsByUser(CommonMethods.SessionInfo.UserId);

            ddlUserProductionUnit.DataTextField = "UnitName";
            ddlUserProductionUnit.DataValueField = "ProductionUnitId";
            ddlUserProductionUnit.DataSource = dtKnittingUnit;
            ddlUserProductionUnit.DataBind();
        }


        public static void LoadProductionUnitsForIssueByUser(DropDownList ddlUserProductionUnit)
        {

            var dtKnittingUnit = new CommonManager().GetProductionUnitsByUser(CommonMethods.SessionInfo.UserId);
            var dr = dtKnittingUnit.NewRow();
            dr["ProductionUnitId"] = 99;
            dr["UnitName"] = "Leftover";
            dtKnittingUnit.Rows.Add(dr);
            ddlUserProductionUnit.DataTextField = "UnitName";
            ddlUserProductionUnit.DataValueField = "ProductionUnitId";
            ddlUserProductionUnit.DataSource = dtKnittingUnit;
            ddlUserProductionUnit.DataBind();
        }

    }
}