﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MBTracker.Code_Folder.DAL;
using MBTracker.EF;


namespace MBTracker.Code_Folder.Reports
{
    public class ReportManager
    {

        DatabaseManager dm = new DatabaseManager();

        public DataTable GetDailyKnittingInfo(DateTime knittingDate)
        {
            dm.AddParameteres("@KnittingDate", knittingDate);
            return dm.ExecuteQuery("[usp_Rpt_GetDailyKnittingInfo]");
        }


        public DataTable GetBookingByShipmentMonthAndUnit(int yearId)
        {
            dm.AddParameteres("@yearId", yearId);
            //dm.AddParameteres("@KnittingDate", knittingDate);
            return dm.ExecuteQuery("usp_Rpt_GetBookingByUnitsAndProductionMonth");
        }

        public DataTable GetPIWiseStoreReceived(string dateTime1, string dateTime2, String AllParameter)
        {
            dm.AddParameteres("@DateTime1", dateTime1);
            dm.AddParameteres("@DateTime2", dateTime2);
            dm.AddParameteres("@AllPIValue", AllParameter);
            return dm.ExecuteQuery("Sp_PIwise_StoreReceive");
        }
        public DataTable GetPIWiseStoreReceivedWithoutMega(string dateTime1, string dateTime2, String AllParameter)
        {
            dm.AddParameteres("@DateTime1", dateTime1);
            dm.AddParameteres("@DateTime2", dateTime2);
            dm.AddParameteres("@AllPIValue", AllParameter);
            return dm.ExecuteQuery("Sp_PIwise_StoreReceive_WithoutMega");
        }

        public DataTable GetStyleWiseDetailsReport(int BuyerId, int StyleId, string dateTime1, string dateTime2)
        {
            dm.AddParameteres("@BuyerId", BuyerId);
            dm.AddParameteres("@StyleId", StyleId);
            dm.AddParameteres("@DateTime1", dateTime1);
            dm.AddParameteres("@DateTime2", dateTime2);
            return dm.ExecuteQuery("Sp_Stylewise_DetailsReport");
        }

        public DataTable GetStyleWisePIAndLCDetailsReport(int BuyerId, int StyleId, string dateTime1, string dateTime2, int SeasonId, int YearsId, int OrdersId, int ItemsId)
        {
            dm.AddParameteres("@BuyerId", BuyerId);
            dm.AddParameteres("@StyleId", StyleId);
            dm.AddParameteres("@DateTime1", dateTime1);
            dm.AddParameteres("@DateTime2", dateTime2);
            dm.AddParameteres("@SeasonId", SeasonId);
            dm.AddParameteres("@YearsId", YearsId);
            dm.AddParameteres("@OrdersId", OrdersId);
            dm.AddParameteres("@ItemsId", ItemsId);
            return dm.ExecuteQuery("Sp_Stylewise_PIANDLC_DetailsReport");
        }
        public DataTable GetPIWiseStoreReceivedUpdatedReport(string dateTime1, string dateTime2, String AllParameter)
        {
            dm.AddParameteres("@DateTime1", dateTime1);
            dm.AddParameteres("@DateTime2", dateTime2);
            dm.AddParameteres("@AllPIValue", AllParameter);
            return dm.ExecuteQuery("Sp_PIwise_StoreReceive_UpdatedReport");
        }


        public DataTable GetBookingByLinkingGauge(int yearId)
        {
            dm.AddParameteres("@yearId", yearId);
            return dm.ExecuteQuery("[usp_Rpt_GetBookingByLinkingGauges]");
        }

        public DataTable GetUnbookedLinkingGauges()
        {
            return dm.ExecuteQuery("[usp_Rpt_GetUnbookedLinkingGauges]");
        }


        public DataTable GetBookingByKnittingMachineBrands(int yearId)
        {
            dm.AddParameteres("@yearId", yearId);
            return dm.ExecuteQuery("usp_Rpt_GetBookingByKnittingMachineBrands");
        }

        public DataTable GetUnbookedKnittingMachines()
        {
            return dm.ExecuteQuery("usp_Rpt_GetUnbookedKnittingMachines");
        }

    }
}