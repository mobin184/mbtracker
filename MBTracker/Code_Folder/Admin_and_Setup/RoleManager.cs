﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using MBTracker.Code_Folder.DAL;


namespace MBTracker.Code_Folder.Admin_and_Setup
{
    public class RoleManager
    {

        DatabaseManager dm = new DatabaseManager();

        public int InsertRole(string roleName, string description, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@RoleName", roleName);
            dm.AddParameteres("@Description", description);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Insert_Role");
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public int UpdateRole(int id, string roleName, string description, string updatedBy, DateTime updateDate)
        {
            dm.AddParameteres("@RoleId", id);
            dm.AddParameteres("@RoleName", roleName);
            dm.AddParameteres("@Description", description);
            dm.AddParameteres("@UpdatedBy", updatedBy);
            dm.AddParameteres("@UpdateDate", updateDate);
            DataTable dt = dm.ExecuteQuery("usp_Update_Role");
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public DataTable GetAllRoles(string table)
        {
            dm.AddParameteres("@Table", table);
            return dm.ExecuteQuery("usp_GetAllRecords");
        }

        public DataTable GetRoleById(int roleId)
        {

            dm.AddParameteres("@ID", roleId);
            return dm.ExecuteQuery("usp_GetRole_ById");

        }

        public int DeleteRole(int id, string actionTaker, DateTime currentTime)
        {
            dm.AddParameteres("@RoleId", id.ToString());
            dm.AddParameteres("@UpdatedBy", actionTaker);
            dm.AddParameteres("@UpdateDate", currentTime);
            return dm.ExecuteNonQuery("usp_Delete_Role");
        }



    }
}