﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MBTracker.Code_Folder.DAL;
using Repositories;
using MBTracker.EF;

namespace MBTracker.Code_Folder.Admin_and_Setup
{
    public class MenuTaskManager
    {

        DatabaseManager dm = new DatabaseManager();
        UnitOfWork unitOfWork = new UnitOfWork();
        public int InsertTaskMenu(string TextEng, string URL, int ParentId, int Order, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@TextEng", TextEng);
            dm.AddParameteres("@URL", URL);
            dm.AddParameteres("@ParentId", ParentId);
            dm.AddParameteres("@Order", Order);
            dm.AddParameteres("@Status", 1);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Insert_Menu");
            return Convert.ToInt32(dt.Rows[0][0]);
        }

        public int DeleteMenu(int id, string actionTaker, DateTime currentTime)
        {
            var obj = unitOfWork.GenericRepositories<TaskItems>().GetByID(id);
            dm.AddParameteres("@BuyerId", id.ToString());
            dm.AddParameteres("@UpdatedBy", actionTaker);
            dm.AddParameteres("@UpdateDate", currentTime);
            var res =  unitOfWork.GetSingleStringValue($"DELETE FROM TaskItems WHERE Id = " +id);

            unitOfWork.SaveAuditLog("Deleted", id + "", "TaskItems", obj);
            unitOfWork.Save();
            return 1;
        }

        public DataTable GetTaskParentItems(int role)
        {
            dm.AddParameteres("@Role", role);
            DataTable dt = dm.ExecuteQuery("usp_GetTask_ParentItems");
            return dt;
        }


        public DataTable GetTaskParentItemsForPermissions(int role)
        {
            dm.AddParameteres("@Role", role);
            DataTable dt = dm.ExecuteQuery("usp_GetTask_ParentItemsForPermissions");
            return dt;
        }


        public DataTable GetTaskChildItems(int ParentId, int roleId)
        {
            dm.AddParameteres("@ParentId", ParentId);
            dm.AddParameteres("@RoleId", roleId);
            return dm.ExecuteQuery("usp_GetTask_ChildItems");
        }


        public int DeleteTaskToRole(int roleId)
        {
            dm.AddParameteres("RoleId", roleId);
            return dm.ExecuteNonQuery("usp_Delete_TaskPermissions");
        }

        public int TaskToRoleInsert(DataTable dt)
        {
            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_TaskPermissions");
        }


    }
}