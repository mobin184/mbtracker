﻿using MBTracker.Code_Folder.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace MBTracker.Code_Folder.Admin_and_Setup
{
    
    public class ButtonManager
    {
        DatabaseManager dm = new DatabaseManager();

        public int InsertButtonPermission(int RoleId, string PageName, int ButtonType, int ButtonNumberOnPage, int EnableDurationInDays, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@RoleId", RoleId);
            dm.AddParameteres("@PageName", PageName);
            dm.AddParameteres("@ButtonType", ButtonType);
            dm.AddParameteres("@ButtonNumberOnPage", ButtonNumberOnPage);
            dm.AddParameteres("@EnableDurationInDays", EnableDurationInDays);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Insert_Buyer");
            return Convert.ToInt32(dt.Rows[0][0]);
        }
    }
}