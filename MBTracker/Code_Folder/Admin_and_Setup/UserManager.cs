﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using MBTracker.Code_Folder.DAL;



namespace MBTracker.Code_Folder.Admin_and_Setup
{
    
    
    public class UserManager
    {

    
    DatabaseManager dm = new DatabaseManager();

    public UserManager()
    {
        
    }

    public DataTable GetUserInfo(string userName, string password)
    {
        dm.AddParameteres("@UserName", userName);
        dm.AddParameteres("@Password", password);
        return dm.ExecuteQuery("usp_GetUserInfo");
    }


    public int CreateUser(string userName, string password, string email, string phoneNo, string name, int roleId, int department, int designaiton, int employeeId, string createBy, DateTime createDate)
    {
        dm.AddParameteres("@UserName", userName);
        dm.AddParameteres("@Password", password);
        dm.AddParameteres("@Email", email);
        dm.AddParameteres("@PhoneNo", phoneNo);
        dm.AddParameteres("@UserFullName", name);
        dm.AddParameteres("@RoleId", roleId);
        dm.AddParameteres("@department", department);
        dm.AddParameteres("@designation", designaiton);
        dm.AddParameteres("@EmployeeId", employeeId);
        dm.AddParameteres("@CreatedBy", createBy);
        dm.AddParameteres("@CreateDate", createDate);
  
        DataTable dt = dm.ExecuteQuery("usp_Insert_User");
        return Convert.ToInt32(dt.Rows[0][0]);
    }



    public int DeleteUserBuyers(int userId)
    {
        dm.AddParameteres("@UserId", userId);
        return dm.ExecuteNonQuery("usp_Delete_UserBuyers");

    }


        public int DeleteUserProductionUnits(int userId)
        {
            dm.AddParameteres("@UserId", userId);
            return dm.ExecuteNonQuery("usp_Delete_UserProductionUnits");

        }

        public int DeleteUserFinishingUnitFloors(int userId)
        {
            dm.AddParameteres("@UserId", userId);
            return dm.ExecuteNonQuery("usp_Delete_UserFinishingUnitFloors");

        }



        public int SaveUserBuyers(DataTable dt )
    {

        DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_UserBuyers");
    }



        public int SaveUserProductionUnits(DataTable dt)
        {

            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_UserProductionUnits");
        }

        public int SaveUserFinishingUnitFloors(DataTable dt)
        {

            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_UserFinishingUnitFloors");
        }




        public DataTable GetUsers()
    {
        return dm.ExecuteQuery("usp_GetAll_Users");
    }


    public int DeleteUser(int userId, string actionTaker, string actionTime )
    {

        dm.AddParameteres("@UserId", userId);
        dm.AddParameteres("@UpdatedBy", actionTaker);
        dm.AddParameteres("@UpdateDate", actionTime);
        return dm.ExecuteNonQuery("usp_Delete_User");

    }


    public DataTable GetUserById(int userId)
        {

            dm.AddParameteres("@ID", userId);
            return dm.ExecuteQuery("usp_GetUser_ById");

        }


    public int UpdateUser(int userId, string mobileNumber, string emailAddress, int roleId, int departmentId, int designationId, string updateBy, DateTime updateDate)
    {
        dm.AddParameteres("@UserId", userId);
        dm.AddParameteres("@MobileNumber", mobileNumber);
        dm.AddParameteres("@EmailAddress", emailAddress);
        dm.AddParameteres("@RoleId", roleId);
        dm.AddParameteres("@DepartmentId", departmentId);
        dm.AddParameteres("@DesignationId", designationId);
        dm.AddParameteres("@UpdatedBy", updateBy);
        dm.AddParameteres("@UpdateDate", updateDate);

        DataTable dt = dm.ExecuteQuery("usp_Update_User");
        return Convert.ToInt32(dt.Rows[0][0]);
    }


    public int ChangePassword(int userId, string password)
    {
        dm.AddParameteres("@UserId", userId);
        dm.AddParameteres("@Password", password);
        DataTable dt = dm.ExecuteQuery("usp_Change_UserPassword");
        return Convert.ToInt32(dt.Rows[0][0]);

    }


    }
}