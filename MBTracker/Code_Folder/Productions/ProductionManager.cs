﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using MBTracker.Code_Folder.DAL;
using System.IO;

namespace MBTracker.Code_Folder.Productions
{
    public class ProductionManager
    {

        DatabaseManager dm = new DatabaseManager();

        public int InsertOrderProductionMachineInfo(int styleId, int knittingMachineId, string knittingMachineGauge, string needlePerCM, string machineSpeed, string knittingTime, string stage, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@StyleId", styleId);
            dm.AddParameteres("@KnittingMachineBrandId", knittingMachineId);
            dm.AddParameteres("@KnittingMachineGauge", knittingMachineGauge);
            dm.AddParameteres("@NeedlePerCM", needlePerCM);
            dm.AddParameteres("@MachineSpeed", machineSpeed);
            dm.AddParameteres("@KnittingTime", knittingTime);
            dm.AddParameteres("@Stage", stage);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);

            DataTable dt = dm.ExecuteQuery("usp_Insert_OrderProductionMachineInfo");
            return Convert.ToInt32(dt.Rows[0][0]);
        }


       


        public int SaveMachinesAssignedToOrder(DataTable dt)
        {
            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_MachinesAssignedToOrder");
        }


        public int SaveYarnIssuedForKnitting(DataTable dt)
        {
            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_YarnIssuedForKnitting");
        }



        public int SaveDalilyKnittingInfo(DataTable dt)
        {
            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_DailyKnittingInfo");
        }



        public DataTable GetProductionPlanningInfo(int styleId)
        {
            dm.AddParameteres("@StyleId", styleId);
            return dm.ExecuteQuery("usp_GetMachineInfoForProduction_ByStyleId");
        }

        //public DataTable GetAvailableMachines(int machineBrandId, DateTime productionDate, int orderId)
        //{
        //    dm.AddParameteres("@MachineBrandId", machineBrandId);
        //    dm.AddParameteres("@ProductionDate", productionDate);
        //    dm.AddParameteres("@OrderId", orderId);
        //    return dm.ExecuteQuery("usp_GetAvailableMachines_PerProductionUnit");

        //}
        public DataTable GetAvailableMachines(int machineBrandId, DateTime productionDate, int styleId)
        {
            dm.AddParameteres("@MachineBrandId", machineBrandId);
            dm.AddParameteres("@ProductionDate", productionDate);
            dm.AddParameteres("@StyleId", styleId);
            return dm.ExecuteQuery("usp_GetAvailableMachines_PerProductionUnitByStyle");

        }

        public DataTable GetProductionUnitsByMachineBrand(int machineBrandId)
        {
            dm.AddParameteres("@MachineBrandId", machineBrandId);
            return dm.ExecuteQuery("usp_ProductionUnits_ByMachineBrand");
        }


        public DataTable GetProductionUnitsByMachineBrandOfAStyle(int styleId)
        {
            dm.AddParameteres("@StyleId", styleId);
            return dm.ExecuteQuery("usp_ProductionUnits_ByMachineBrand_of_A_Style");
        }


        public DataTable GetMachinesAssignedToOrder(int orderId)
        {
            dm.AddParameteres("@OrderId", orderId);
            return dm.ExecuteQuery("usp_GetMachinesAssingedToOrder_ByOrderId");
        }
        public DataTable GetMachinesAssignedToStyle(int styleId)
        {
            dm.AddParameteres("@StyleId", styleId);
            return dm.ExecuteQuery("usp_GetMachinesAssingedToOrder_ByStyleId");
        }


        //public DataTable GetYarnIssuedForSizes(int orderId, int unitId, int orderColorId)
        //{
        //    dm.AddParameteres("@OrderId", orderId);
        //    dm.AddParameteres("@UnitId", unitId);
        //    dm.AddParameteres("@OrderColorId", orderColorId);
        //    return dm.ExecuteQuery("usp_GetYarnIssuedForSizes_ByOrderUnitAndOrderColorIds");
        //}

        public DataTable GetYarnIssuedForSizesByStyle(int styleId, int unitId, int buyerColorId)
        {
            dm.AddParameteres("@StyleId", styleId);
            dm.AddParameteres("@UnitId", unitId);
            dm.AddParameteres("@BuyerColorId", buyerColorId);
            return dm.ExecuteQuery("usp_GetYarnIssuedForSizes_ByStyleUnitAndOrderColor");
        }




        public int DeleteAssignedMachine(int machineAssignedId)
        {
            dm.AddParameteres("@MachineAssignedId", machineAssignedId);
            DataTable dt = dm.ExecuteQuery("usp_Delete_MachinesAssignedToOrder");
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public DataTable GetProductionEntryInfo(int unitId,DateTime productionDate)
        {
            dm.AddParameteres("@UnitId", unitId);
            dm.AddParameteres("@ProductionDate", productionDate);
            return dm.ExecuteQuery("usp_GetProductionEntryInfo_ByUnit");

        }


        public int SaveReportedProblemFiles(int reportedProblemId, string fileName, string contentType, Int64 fileSize, byte[] bytes, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@ReportedProblemId", reportedProblemId);
            dm.AddParameteres("@FileName", fileName);
            dm.AddParameteres("@ContentType", contentType);
            dm.AddParameteres("@FileSize", fileSize);
            dm.AddParameteres("@FileData", bytes);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Insert_ReportedProblemFile");
            return Convert.ToInt32(dt.Rows[0][0]);
        }

        public DataTable GetReportedProblemFileInfo(int reportedProblemId)
        {
            dm.AddParameteres("@ReportedProblemId", reportedProblemId);
            return dm.ExecuteQuery("usp_GetReportedProblemFileInfo_ByReportedProblemId");
        }

        public DataTable GetReportedProblemFile(int fileId)
        {
            dm.AddParameteres("@ReportedProblemFileId", fileId);
            return dm.ExecuteQuery("usp_GetReportedProblemFile_ByFileId");
        }


        public int InsertProductRejection(int buyerId, int styleId, int orderId, int finishingFloorId, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@BuyerId", buyerId);
            dm.AddParameteres("@StyleId", styleId);
            dm.AddParameteres("@OrderId", orderId);
            dm.AddParameteres("@FinishingFloorId", finishingFloorId);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Insert_ProductRejections");
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public int SaveProductRejections(DataTable dt)
        {
            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_ProductRejectionColorAndSizes");
        }



        public DataTable GetRejectionInfoByOrderId(int orderId)
        {
            dm.AddParameteres("@OrderId", orderId);
            return dm.ExecuteQuery("usp_GetRejectionInfo_ByOrderId");

        }



        public DataTable GetRejectionColorsByRejectionId(int rejectionId)
        {
            dm.AddParameteres("@RejectionId", rejectionId);
            return dm.ExecuteQuery("usp_GetRejectionColors_ByRejectionId");

        }


        public DataTable GetRejectionSizeAndQuantity(int productRejectionId, int rejectionColorId, int sizeId)
        {
            dm.AddParameteres("@ProductRejectionId", productRejectionId);
            dm.AddParameteres("@BuyerColorId", rejectionColorId);
            dm.AddParameteres("@SizeId", sizeId);

            return dm.ExecuteQuery("usp_GetRejectionSizeAndQuantity_ByRejectionColorAndSizeId");
        }


        public DataTable GetRejectionNotesByColor(int productRejectionId, int rejectionColorId)
        {
            dm.AddParameteres("@ProductRejectionId", productRejectionId);
            dm.AddParameteres("@BuyerColorId", rejectionColorId);
            return dm.ExecuteQuery("usp_GetRejectionNotes_ByRejectionColorId");
        }


        public DataTable GetTNANotes(int TNAId)
        {
            dm.AddParameteres("@TNAId", TNAId);
            return dm.ExecuteQuery("usp_GetTNANotes_ByTNAId");
        }

        public DataTable LoadElectricityOutagesByUnitAndDate(int unitId, int shiftId, DateTime selectedDate)
        {
            dm.AddParameteres("@KnittingUnitId", unitId);
            dm.AddParameteres("@ShiftId", shiftId);
            dm.AddParameteres("@OutageDate", selectedDate);
            return dm.ExecuteQuery("usp_GetElectricityOutageByUnitAndDate");
        }


    }
}

