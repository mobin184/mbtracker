﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using MBTracker.Code_Folder.DAL;
using Repositories;
using MBTracker.EF;

namespace MBTracker.Code_Folder.Buyers_and_Orders
{


    public class OrderManager
    {

        DatabaseManager dm = new DatabaseManager();
        UnitOfWork unitOfWork = new UnitOfWork();


        public int InsertOrderBasicInfo(int buyerId, int BookingId, int buyerDeptId, int styleId, int orderSeasonId, int orderYearId, string orderNumber, DateTime orderDate, DateTime exFactoryDate, string orderItem, string paymentTerms, double lcValue, int statusId, string createdBy, DateTime createDate, int ApprovedStatus)
        {
            dm.AddParameteres("@BuyerId", buyerId);
            dm.AddParameteres("@BookingId", BookingId);
            dm.AddParameteres("@BuyerDepartmentId", buyerDeptId);
            dm.AddParameteres("@StyleId", styleId);
            dm.AddParameteres("@OrderSeasonId", orderSeasonId);
            dm.AddParameteres("@OrderYearId", orderYearId);
            dm.AddParameteres("@OrderNumber", orderNumber);
            dm.AddParameteres("@OrderDate", orderDate);
            dm.AddParameteres("@ExFactoryDate", exFactoryDate);
            dm.AddParameteres("@OrderItem", orderItem);
            dm.AddParameteres("@PaymentTerms", paymentTerms);
            dm.AddParameteres("@LCValue", lcValue);
            dm.AddParameteres("@StatusId", statusId);
            dm.AddParameteres("@ApprovedStatus", ApprovedStatus);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Insert_OrderBasicInfo");
            return Convert.ToInt32(dt.Rows[0][0]);
        }

        public int SaveOrderSizes(DataTable dt)
        {
            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_OrderSizes");
        }



        public int SaveOrderDeliveryCountries(DataTable dt)
        {

            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_OrderDeliveryCountries");
        }


        public int SaveOrderColor(int orderId, int buyerColorId, decimal unitCost, int orderQuantity, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@OrderId", orderId);
            dm.AddParameteres("@BuyerColorId", buyerColorId);

            //dm.AddParameteres("@ColorCode", colorCode);
            //dm.AddParameteres("@ColorDescription", colorDescription);
            dm.AddParameteres("@UnitCost", unitCost);
            dm.AddParameteres("@OrderQuantity", orderQuantity);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Insert_OrderColor");
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public int InsertShipmentDate(int orderId, string shipmentDate, string shipmentMode, string shipmentCancelDate, string inspectionAvailDate, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@OrderId", orderId);
            dm.AddParameteres("@ShipmentDate", shipmentDate);
            dm.AddParameteres("@ShipmentMode", shipmentMode);
            dm.AddParameteres("@ShipmentCancelDate", shipmentCancelDate);
            dm.AddParameteres("@InspectionAvailDate", inspectionAvailDate);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Insert_ShipmentDate");
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public int InsertConsumptionSheet(int styleId, string stage, int machineBrandId, string machineGauge, string needlePerCM, string machineSpeed, string knittingTime, string designerName, string designerPhone, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@StyleId", styleId);
            dm.AddParameteres("@Stage", stage);
            dm.AddParameteres("@MachineBrandId", machineBrandId);
            dm.AddParameteres("@MachineGauge", machineGauge);
            dm.AddParameteres("@NeedlePerCM", needlePerCM);
            dm.AddParameteres("@MachineSpeed", machineSpeed);
            dm.AddParameteres("@KnittingTime", knittingTime);
            dm.AddParameteres("@DesignerName", designerName);
            dm.AddParameteres("@DesignerPhone", designerPhone);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Insert_ConsumptionSheet");
            return Convert.ToInt32(dt.Rows[0][0]);
        }





        public int SaveOrderColorSizes(DataTable dt)
        {
            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_OrderColorSizes");
        }




        public int SaveShipmentColorCountrySizes(DataTable dt)
        {
            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_ShipmentColorCountrySizes");
        }


        public int SaveYarnConsumptionByParts(DataTable dt)
        {
            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_YarnConsumptionByParts");
        }


        public int SaveYarnBooking(DataTable dt)
        {
            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_YarnBooking");
        }







        public int SaveOrderFiles(int orderId, string fileName, string contentType, Int64 fileSize, byte[] bytes, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@OrderId", orderId);
            dm.AddParameteres("@FileName", fileName);
            dm.AddParameteres("@ContentType", contentType);
            dm.AddParameteres("@FileSize", fileSize);
            dm.AddParameteres("@FileData", bytes);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Insert_OrderFile");
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public int SaveConsumptionSheetFiles(int consumptionSheetId, string fileName, string contentType, Int64 fileSize, byte[] bytes, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@ConsumptionSheetId", consumptionSheetId);
            dm.AddParameteres("@FileName", fileName);
            dm.AddParameteres("@ContentType", contentType);
            dm.AddParameteres("@FileSize", fileSize);
            dm.AddParameteres("@FileData", bytes);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Insert_ConsumptionSheetFile");
            return Convert.ToInt32(dt.Rows[0][0]);
        }



        public DataTable GetOrders(int userId)
        {
            dm.AddParameteres("@UserId", userId);
            return dm.ExecuteQuery("usp_GetAllOrders_ShipmentIn60Days");
        }

        public DataTable GetOrdersByBuyerId(int buyerId)
        {
            dm.AddParameteres("@BuyerId", buyerId);
            return dm.ExecuteQuery("usp_GetAllOrders_ByBuyerId");
        }

        public DataTable GetOrdersByStyleName(string styleName, int userId)
        {
            dm.AddParameteres("@StyleName", styleName);
            dm.AddParameteres("@UserId", userId);
            return dm.ExecuteQuery("usp_GetAllOrders_ByStyleName");
        }

        public DataTable GetOrdersByStyle(int styleId)
        {
            dm.AddParameteres("@StyleId", styleId);
            return dm.ExecuteQuery("usp_GetOrders_ByStyleId");
        }


        public DataTable GetDeliveryCountrisByOrderId(int orderId)
        {
            dm.AddParameteres("@OrderId", orderId);
            return dm.ExecuteQuery("usp_GetDeliveryCountries_ByOrderId");
        }

        public DataTable GetSelectedDeliveryCountrisByOrderId(int orderId, string countryIds)
        {
            dm.AddParameteres("@OrderId", orderId);
            dm.AddParameteres("@CountryIds", countryIds);
            return dm.ExecuteQuery("usp_GetDeliveryCountries_ByOrderId_And_CountryIds");
        }


        public DataTable GetDeliveryCountrisByShipmentDateId(int shipmentDateId)
        {
            dm.AddParameteres("@ShipmentDateId", shipmentDateId);
            return dm.ExecuteQuery("usp_GetDeliveryCountries_ByShipmentDateId");
        }



        public DataTable GetOrderColorsByOrderId(int orderId)
        {
            dm.AddParameteres("@OrderId", orderId);
            return dm.ExecuteQuery("usp_GetOrderColors_ByOrderId");
        }


        public DataTable GetShipmentColorsByShipmentDateId(int orderShipmentDateId)
        {
            dm.AddParameteres("@shipmentDateId", orderShipmentDateId);
            return dm.ExecuteQuery("usp_GetShipmentColorsByShipmentDateId");
        }


        public DataTable GetColorsByStyleId(int styleId)
        {
            dm.AddParameteres("@StyleId", styleId);
            return dm.ExecuteQuery("usp_GetStyleColorsByStyleId");
        }

        //public DataTable GetColorsByOrderId(int orderId)
        //{
        //    dm.AddParameteres("@OrderId", orderId);
        //    return dm.ExecuteQuery("usp_GetOrderColorsByOrderId");
        //}


        public DataTable GetOrderShipmentDatesByOrderId(int orderId)
        {
            dm.AddParameteres("@OrderId", orderId);
            return dm.ExecuteQuery("usp_GetOrderShipmentDates_ByOrderId");
        }


        public DataTable GetShipmentCountriesByDateAndColor(int shipmentDateId, int buyerColorId)
        {
            dm.AddParameteres("@ShipmentDateId", shipmentDateId);
            dm.AddParameteres("@BuyerColorId", buyerColorId);
            return dm.ExecuteQuery("usp_GetDeliveryCountries_ByShipmentAndColor");
        }

        public DataTable GetSizesAndQuantityByShipmentColorCountry(int shipmentDateId, int buyerColorId, int deliveryCountryId)
        {
            dm.AddParameteres("@ShipmentDateId", shipmentDateId);
            dm.AddParameteres("@BuyerColorId", buyerColorId);
            dm.AddParameteres("@DeliveryCountryId", deliveryCountryId);
            return dm.ExecuteQuery("usp_GetSizeAndQuantity_ByShipmentColorAndCountry");
        }


        public DataTable GetSweaterParts()
        {
            //dm.AddParameteres("@StyleId", styleId);
            return dm.ExecuteQuery("usp_GetSweatherParts");
        }


        public DataTable GetMachineBrands()
        {
            return dm.ExecuteQuery("usp_GetAllMachineBrands");
        }

        public DataTable GetAllYarnAndAccessoriesSuppliers()
        {
            return dm.ExecuteQuery("usp_GetAllYarnAndAccessoriesSuppliers");
        }

        public DataTable GetAllYarnAndAccessoriesPurchaseType()
        {
            return dm.ExecuteQuery("usp_GetAllYarnAndAccessoriesPurchaseType");
        }

        public DataTable GetLCBySupplierId(int supplierId)
        {
            dm.AddParameteres("@supplierId", supplierId);
            return dm.ExecuteQuery($"usp_GetAllLCBySupplier");
        }

        public DataTable GetYarnLCBySupplierAndStyleId(int supplierId, int styleId)
        {
            dm.AddParameteres("@supplierId", supplierId);
            dm.AddParameteres("@styleId", styleId);
            return dm.ExecuteQuery($"usp_GetAllYarnLCBySupplier");
        }

        public DataTable GetDyeingOrderBySupplierId(int supplierId)
        {
            dm.AddParameteres("@supplierId", supplierId);
            return dm.ExecuteQuery($"usp_GetAllDyeingOrderBySupplier");
        }

        public DataTable GetAllYarnAndAccessoriesRawMaterialItemTypes()
        {
            return dm.ExecuteQuery("usp_GetAllYarnAndAccessoriesItemTypes");
        }


        public DataTable GetProductionMonths()
        {
            return dm.ExecuteQuery("usp_GetAllYearMonths");
        }




        public DataTable GetProductionPlanningInfo(int styleId)
        {
            dm.AddParameteres("@StyleId", styleId);
            return dm.ExecuteQuery("usp_GetProductionPlanningInfo_ByStyleId");
        }


        public DataTable GetShipmentInfo(int orderId)
        {
            dm.AddParameteres("@OrderId", orderId);
            return dm.ExecuteQuery("usp_GetShipmentInfo_ByOrderId");
        }

        public DataTable GetShipmentInfoByStyleId(int styleId)
        {
            dm.AddParameteres("@StyleId", styleId);
            return dm.ExecuteQuery("usp_GetShipmentInfo_ByStyleId");
        }

        public DataTable GetMonthWiseShipmentInfoByStyleId(int styleId)
        {
            dm.AddParameteres("@StyleId", styleId);
            return dm.ExecuteQuery("usp_GetMonthWiseShipmentInfo_ByStyleId");
        }

        public DataTable GetShipmentInfoByColor(int shipmentDateId)
        {
            dm.AddParameteres("@ShipmentDateId", shipmentDateId);
            return dm.ExecuteQuery("usp_GetOrderQuantityByColor_ByShipmentDate");
        }


        public DataTable GetMonthYearInfo(int monthYearId)
        {
            dm.AddParameteres("@MonthYearId", monthYearId);
            return dm.ExecuteQuery("usp_GetMonthYearInfo_ByMonthYearId");
        }



        public DataTable GetExistingConsumptionSheets(int styleId)
        {
            dm.AddParameteres("@StyleId", styleId);
            return dm.ExecuteQuery("usp_GetConsumptionSheets_ByStyleId");
        }


        public DataTable GetConsumptionSheetFileInfo(int consumptionSheetId)
        {
            dm.AddParameteres("@ConsumptionSheetId", consumptionSheetId);
            return dm.ExecuteQuery("usp_GetConsumptionSheetFileInfo_ByConsumptionSheetId");
        }


        public DataTable GetConsumptionSheetFile(int fileId)
        {
            dm.AddParameteres("@ConsumptionSheetFileId", fileId);
            return dm.ExecuteQuery("usp_GetConsumptionSheetFile_ByFileId");
        }



        public DataTable GetYarnProcurementStatus()
        {
            return dm.ExecuteQuery("usp_GetYarnProcurementStatus");
        }




        public int UpdateOrderStatus(int orderId, int statusId, string updatedBy, DateTime updateDate)
        {
            var obj = unitOfWork.GenericRepositories<Orders>().GetByID(orderId);
            dm.AddParameteres("@OrderId", orderId);
            dm.AddParameteres("@StatusId", statusId);
            dm.AddParameteres("@UpdatedBy", updatedBy);
            dm.AddParameteres("@UpdateDate", updateDate);
            DataTable dt = dm.ExecuteQuery("usp_Update_OrderStatus");
            //for audit log
            unitOfWork.SaveAuditLog("Modified", orderId + "", "Orders", obj);
            unitOfWork.Save();
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public int DeleteOrderShipmentItems(int shipmentDateId)
        {
            dm.AddParameteres("@ShipmentDateId", shipmentDateId);
            return dm.ExecuteNonQuery("usp_Delete_ShipmentItemsByShipmentDateId");

        }


    }
}