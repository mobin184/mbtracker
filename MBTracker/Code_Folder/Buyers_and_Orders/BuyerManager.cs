﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using MBTracker.Code_Folder.DAL;
using Repositories;
using MBTracker.Code_Folder.Admin_and_Setup;
using MBTracker.EF;
using Newtonsoft.Json;

namespace MBTracker.Code_Folder.Buyers_and_Orders
{
    public class BuyerManager
    {

        DatabaseManager dm = new DatabaseManager();
        UnitOfWork unitOfWork = new UnitOfWork();
       
        public DataTable GetSizes(int buyerId)
        {
            dm.AddParameteres("@BuyerId", buyerId);
            return dm.ExecuteQuery("usp_GetSizesByBuyer");
        }


        public DataTable GetStyleSizes(int styleId)
        {
            dm.AddParameteres("@StyleId", styleId);
            return dm.ExecuteQuery("usp_GetStyleSizesByStyleId");
        }

        public DataTable GetBoothReceiveSizes(int styleId)
        {
            dm.AddParameteres("@StyleId", styleId);
            return dm.ExecuteQuery("usp_GetReceivedStyleSizesAndQuantityByStyleId");
        }

        public DataTable GetOrderSizes(int orderId)
        {
            dm.AddParameteres("@OrderId", orderId);
            return dm.ExecuteQuery("usp_GetOrderSizesByOrderId");
        }


        public DataTable GetColors(int buyerId)
        {
            dm.AddParameteres("@BuyerId", buyerId);
            return dm.ExecuteQuery("usp_GetColorsByBuyer");
        }

        public int InsertDepartment(string departmentName, int buyerId, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@departmentName", departmentName);
            dm.AddParameteres("@BuyerId", buyerId);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Insert_BuyerDepartment");
            return Convert.ToInt32(dt.Rows[0][0]);
        }

        public int InsertStyle(string styleName, int buyerId, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@StyleName", styleName);
            dm.AddParameteres("@BuyerId", buyerId);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Insert_BuyerStyle");
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public int InsertSize(string sizeName, int buyerId, string createdBy, DateTime createDate, int sizeTypeId, int sequenceNo)
        {
            dm.AddParameteres("@SizeName", sizeName);
            dm.AddParameteres("@BuyerId", buyerId);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            dm.AddParameteres("@SizeTypeId", sizeTypeId);
            dm.AddParameteres("@sequenceNo", sequenceNo);
            DataTable dt = dm.ExecuteQuery("usp_Insert_BuyerSize");
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public int InsertSeason(string seasonName, int buyerId, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@SeasonName", seasonName);
            dm.AddParameteres("@BuyerId", buyerId);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Insert_BuyerSeason");
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        


        public int InsertDeliveryCountry(string countryName, string countryCode, int buyerId, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@CountryName", countryName);
            dm.AddParameteres("@CountryCode", countryCode);
            dm.AddParameteres("@BuyerId", buyerId);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Insert_BuyerDeliveryCountry");
            return Convert.ToInt32(dt.Rows[0][0]);
        }



        public int InsertBuyer(string buyerName, string description, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@BuyerName", buyerName);
            dm.AddParameteres("@Description", description);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Insert_Buyer");
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public int SetBuyerPermissionForUser(int userId, int buyerId, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@UserId", userId);
            dm.AddParameteres("@BuyerId", buyerId);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            DataTable dt = dm.ExecuteQuery("usp_Set_BuyerPermissionForUser");
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        


        public DataTable GetAllBuyers(string table)
        {
            dm.AddParameteres("@Table", table);
            return dm.ExecuteQuery("usp_GetAllRecords");
        }


        public int UpdateBuyer(int id, string buyerName, string description, string updatedBy, DateTime updateDate)
        {
            var obj = unitOfWork.GenericRepositories<Buyers>().GetByID(id);
            dm.AddParameteres("@BuyerId", id);
            dm.AddParameteres("@BuyerName", buyerName);
            dm.AddParameteres("@Description", description);
            dm.AddParameteres("@UpdatedBy", updatedBy);
            dm.AddParameteres("@UpdateDate", updateDate);
            DataTable dt = dm.ExecuteQuery("usp_Update_Buyer");
            //for audit log
            unitOfWork.SaveAuditLog("Modified", id+"", "Buyers", obj);
            unitOfWork.Save();
            return Convert.ToInt32(dt.Rows[0][0]);
        }

        public int DeleteBuyer(int id, string actionTaker, DateTime currentTime)
        {
            var obj = unitOfWork.GenericRepositories<Buyers>().GetByID(id);
            dm.AddParameteres("@BuyerId", id.ToString());
            dm.AddParameteres("@UpdatedBy", actionTaker);
            dm.AddParameteres("@UpdateDate", currentTime);
            var res = dm.ExecuteNonQuery("usp_Delete_Buyer");
            //for audit log
            unitOfWork.SaveAuditLog("Deleted", id + "", "Buyers", obj);
            unitOfWork.Save();
            return res;
        }



        public DataTable LoadDepartmentsByBuyer(int buyerId)
        {
            dm.AddParameteres("@BuyerId", buyerId);
            return dm.ExecuteQuery("[usp_GetDepartmentsByBuyer]");
        }

        public DataTable LoadStylesByBuyer(int buyerId)
        {
            dm.AddParameteres("@BuyerId", buyerId);
            return dm.ExecuteQuery("[usp_GetStylesByBuyer]");
        }

        public DataTable LoadColorsByBuyer(int buyerId)
        {
            dm.AddParameteres("@BuyerId", buyerId);
            return dm.ExecuteQuery("[usp_GetColorsByBuyer]");
        }

        public DataTable LoadSizesByBuyer(int buyerId)
        {
            dm.AddParameteres("@BuyerId", buyerId);
            return dm.ExecuteQuery("[usp_GetSizesByBuyer]");
        }

        public DataTable LoadSeasonsByBuyer(int buyerId)
        {
            dm.AddParameteres("@BuyerId", buyerId);
            return dm.ExecuteQuery("[usp_GetSeasonsByBuyer]");
        }


        public DataTable LoadDeliveryCountriesByBuyer(int buyerId)
        {
            dm.AddParameteres("@BuyerId", buyerId);
            return dm.ExecuteQuery("[usp_GetDeliveryCountriesByBuyer]");
        }


        public DataTable GetDepartmentById(int departmentId)
        {
            dm.AddParameteres("@Id", departmentId);
            return dm.ExecuteQuery("usp_GetBuyerDepartment_ByDepartmentId");
        }

        public DataTable GetStyleById(int styleId)
        {
            dm.AddParameteres("@Id", styleId);
            return dm.ExecuteQuery("usp_GetBuyerStyle_ByStyleId");
        }


        public DataTable GetSizeById(int sizeId)
        {
            dm.AddParameteres("@Id", sizeId);
            return dm.ExecuteQuery("usp_GetBuyerSize_BySizeId");
        }


        public DataTable GetSeasonById(int seasonId)
        {
            dm.AddParameteres("@Id", seasonId);
            return dm.ExecuteQuery("usp_GetBuyerSeason_BySeasonId");
        }


        public DataTable GetDeliveryCountryById(int buyerDeliveryCountryId)
        {
            dm.AddParameteres("@Id", buyerDeliveryCountryId);
            return dm.ExecuteQuery("usp_GetBuyerDeliveryCountry_ByDeliveryCountryId");
        }


        public DataTable GetBuyerById(int buyerId)
        {
            dm.AddParameteres("@BuyerId", buyerId);
            return dm.ExecuteQuery("usp_GetBuyer_ByBuyerId");
        }


        public int UpdateBuyerDepartment(int departmentId, string departmentName, int buyerId, string updatedBy, DateTime updateDate)
        {
            var obj = unitOfWork.GenericRepositories<BuyerDepartments>().GetByID(departmentId);
            dm.AddParameteres("@DepartmentId", departmentId);
            dm.AddParameteres("@DepartmentName", departmentName);
            dm.AddParameteres("@BuyerId", buyerId);
            dm.AddParameteres("@UpdatedBy", updatedBy);
            dm.AddParameteres("@UpdateDate", updateDate);
            DataTable dt = dm.ExecuteQuery("usp_Update_BuyerDepartment");
            //for audit log
            unitOfWork.SaveAuditLog("Modified", departmentId + "", "BuyerDepartments", obj);
            unitOfWork.Save();
            return Convert.ToInt32(dt.Rows[0][0]);
        }



        public int UpdateBuyerStyle(int styleId, string styleName, int buyerId, string updatedBy, DateTime updateDate)
        {
            var obj = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(styleId);
            dm.AddParameteres("@StyleId", styleId);
            dm.AddParameteres("@StyleName", styleName);
            dm.AddParameteres("@BuyerId", buyerId);
            dm.AddParameteres("@UpdatedBy", updatedBy);
            dm.AddParameteres("@UpdateDate", updateDate);
            DataTable dt = dm.ExecuteQuery("usp_Update_BuyerStyle");
            //for audit log
            unitOfWork.SaveAuditLog("Modified", styleId + "", "BuyerStyles", obj);
            unitOfWork.Save();
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public int UpdateBuyerSize(int sizeId, string sizeName, int buyerId, string updatedBy, DateTime updateDate, int sizeTypeId, int sequenceNo)
        {
            var obj = unitOfWork.GenericRepositories<BuyerSizes>().GetByID(sizeId);
            dm.AddParameteres("@SizeId", sizeId);
            dm.AddParameteres("@SizeName", sizeName);
            dm.AddParameteres("@BuyerId", buyerId);
            dm.AddParameteres("@UpdatedBy", updatedBy);
            dm.AddParameteres("@UpdateDate", updateDate);
            dm.AddParameteres("@SizeTypeId", sizeTypeId);
            dm.AddParameteres("@sequenceNo", sequenceNo);
            DataTable dt = dm.ExecuteQuery("usp_Update_BuyerSize");
            //for audit log
            unitOfWork.SaveAuditLog("Modified", sizeId + "", "BuyerSizes", obj);
            unitOfWork.Save();
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public int UpdateBuyerSeason(int seasonId, string seasonName, int buyerId, string updatedBy, DateTime updateDate)
        {
            var obj = unitOfWork.GenericRepositories<BuyerSeasons>().GetByID(seasonId);
            dm.AddParameteres("@SeasonId", seasonId);
            dm.AddParameteres("@SeasonName", seasonName);
            dm.AddParameteres("@BuyerId", buyerId);
            dm.AddParameteres("@UpdatedBy", updatedBy);
            dm.AddParameteres("@UpdateDate", updateDate);
            DataTable dt = dm.ExecuteQuery("usp_Update_BuyerSeason");
            //for audit log
            unitOfWork.SaveAuditLog("Modified", seasonId + "", "BuyerSeasons", obj);
            unitOfWork.Save();
            return Convert.ToInt32(dt.Rows[0][0]);
        }

        


        public int UpdateBuyerDeliveryCountry(int deliveryCountryId, string countryName, string countryCode, int buyerId, string updatedBy, DateTime updateDate)
        {
            var obj = unitOfWork.GenericRepositories<BuyerDeliveryCountries>().GetByID(deliveryCountryId);
            dm.AddParameteres("@DeliveryCountryId", deliveryCountryId);
            dm.AddParameteres("@DeliveryCountryName", countryName);
            dm.AddParameteres("@DeliveryCountryCode", countryCode);
            dm.AddParameteres("@BuyerId", buyerId);
            dm.AddParameteres("@UpdatedBy", updatedBy);
            dm.AddParameteres("@UpdateDate", updateDate);
            DataTable dt = dm.ExecuteQuery("usp_Update_BuyerDeliveryCountry");
            //for audit log
            unitOfWork.SaveAuditLog("Modified", deliveryCountryId + "", "BuyerDeliveryCountries", obj);
            unitOfWork.Save();
            return Convert.ToInt32(dt.Rows[0][0]);
        }

        


        public int DeleteBuyerDepartment(int departmentId, string actionTaker, DateTime currentTime)
        {
            var obj = unitOfWork.GenericRepositories<BuyerDepartments>().GetByID(departmentId);
            dm.AddParameteres("@DepartmentId", departmentId.ToString());
            dm.AddParameteres("@UpdatedBy", actionTaker);
            dm.AddParameteres("@UpdateDate", currentTime);
            DataTable dt = dm.ExecuteQuery("usp_Delete_BuyerDepartment");
            //for audit log
            unitOfWork.SaveAuditLog("Deleted", departmentId + "", "BuyerDepartments", obj);
            unitOfWork.Save();
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public int DeleteBuyerStyle(int styleId, string actionTaker, DateTime currentTime)
        {
            var obj = unitOfWork.GenericRepositories<BuyerStyles>().GetByID(styleId);
            dm.AddParameteres("@StyleId", styleId.ToString());
            dm.AddParameteres("@UpdatedBy", actionTaker);
            dm.AddParameteres("@UpdateDate", currentTime);
            DataTable dt = dm.ExecuteQuery("usp_Delete_BuyerStyle");
            //for audit log
            unitOfWork.SaveAuditLog("Deleted", styleId + "", "BuyerStyles", obj);
            unitOfWork.Save();
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public int DeleteBuyerSize(int sizeId, string actionTaker, DateTime currentTime)
        {
            var obj = unitOfWork.GenericRepositories<BuyerSizes>().GetByID(sizeId);
            dm.AddParameteres("@SizeId", sizeId);
            dm.AddParameteres("@UpdatedBy", actionTaker);
            dm.AddParameteres("@UpdateDate", currentTime);
            DataTable dt = dm.ExecuteQuery("usp_Delete_BuyerSize");
            //for audit log
            unitOfWork.SaveAuditLog("Deleted", sizeId + "", "BuyerSizes", obj);
            unitOfWork.Save();
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public int DeleteBuyerSeason(int seasonId, string actionTaker, DateTime currentTime)
        {
            var obj = unitOfWork.GenericRepositories<BuyerSeasons>().GetByID(seasonId);
            dm.AddParameteres("@SeasonId", seasonId);
            dm.AddParameteres("@UpdatedBy", actionTaker);
            dm.AddParameteres("@UpdateDate", currentTime);
            DataTable dt = dm.ExecuteQuery("usp_Delete_BuyerSeason");
            //for audit log
            unitOfWork.SaveAuditLog("Deleted", seasonId + "", "BuyerSeasons", obj);
            unitOfWork.Save();
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        

        public int DeleteBuyerDeliveryCountry(int deliveryCountryId, string actionTaker, DateTime currentTime)
        {
            var obj = unitOfWork.GenericRepositories<BuyerDeliveryCountries>().GetByID(deliveryCountryId);
            dm.AddParameteres("@DeliveryCountryId", deliveryCountryId);
            dm.AddParameteres("@UpdatedBy", actionTaker);
            dm.AddParameteres("@UpdateDate", currentTime);
            DataTable dt = dm.ExecuteQuery("usp_Delete_BuyerDeliveryCountry");
            //for audit log
            unitOfWork.SaveAuditLog("Deleted", deliveryCountryId + "", "BuyerDeliveryCountries", obj);
            unitOfWork.Save();
            return Convert.ToInt32(dt.Rows[0][0]);
        }


        public int DeleteStyleColors(int styleId)
        {
            var lstColors = unitOfWork.GenericRepositories<StyleColors>().Get(x => x.StyleId == styleId).ToList();
            dm.AddParameteres("@StyleId", styleId);
            var res= dm.ExecuteNonQuery("usp_Delete_StyleColors");
            //for audit log
            foreach (var obj in lstColors)
            {
                unitOfWork.SaveAuditLog("Deleted", obj.Id + "", "StyleColors", obj);
                unitOfWork.Save();
            }          
            return res;
        }


        public int SaveStyleColors(DataTable dt)
        {

            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_StyleColors");
        }

        public int DeleteStyleSizes(int styleId)
        {
            var lstSizes = unitOfWork.GenericRepositories<StyleSizes>().Get(x => x.StyleId == styleId).ToList();
            dm.AddParameteres("@StyleId", styleId);
            var res = dm.ExecuteNonQuery("usp_Delete_StyleSizes");
            //for audit log
            foreach (var obj in lstSizes)
            {
                unitOfWork.SaveAuditLog("Deleted", obj.Id + "", "StyleSizes", obj);
                unitOfWork.Save();
            }
            return res;

        }


        public int SaveStyleSizes(DataTable dt)
        {

            DataSet ds = new DataSet("ds");
            ds.Tables.Add(dt);
            string xml = ds.GetXml();
            dm.AddParameteres("@Xml", xml);
            return dm.ExecuteNonQuery("usp_Insert_StyleSizes");
        }


    }
}