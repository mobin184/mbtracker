﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using MBTracker.Code_Folder.DAL;
using MBTracker.EF;

namespace MBTracker.Code_Folder.Knitting_Parts
{
    public class PartsManager
    {
        DatabaseManager dm = new DatabaseManager();

        public DataTable GetKnittingPartTypes()
        {
            return dm.ExecuteQuery("usp_GetKnittingSpareTypes");
        }


        public int DeleteSpareReceiveDetails(int KnittingSpareReceiveDetailsId, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@SpareReceiveDetailsId", KnittingSpareReceiveDetailsId);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);
            return dm.ExecuteNonQuery("usp_Delete_SpareReceiveDetailsByReceiveDetailsId");
        }


        public DataTable GetExistingKnittingSparesByTypeAndMachineBrand(int spareTypeId, int machineBrandId)
        {
            dm.AddParameteres("@SpareTypeId", spareTypeId);
            dm.AddParameteres("@MachineBrandId", machineBrandId);
            return dm.ExecuteQuery("usp_GetKnittingSpares_BySpareTypeAndMachineBrandId");
        }

        public DataTable GetExistingKnittingSparesByMachineBrand(int machineBrandId)
        {
            dm.AddParameteres("@MachineBrandId", machineBrandId);
            return dm.ExecuteQuery("usp_GetKnittingSpares_ByMachineBrandId");
        }


        public DataTable GetKnittingSpareRequests(int unitId, int machineBrandId, DateTime fromDate)
        {
            dm.AddParameteres("@UnitId", unitId);
            dm.AddParameteres("@MachineBrandId", machineBrandId);
            dm.AddParameteres("@FromDate", fromDate);
            return dm.ExecuteQuery("usp_GetKnittingSpareRequests_ByUnitMachineBrandIdAndDate");
        }

        public DataTable GetKnittingSpareRequestsById(int spareRequestId)
        {
            dm.AddParameteres("@SpareRequestId", spareRequestId);
            return dm.ExecuteQuery("usp_GetKnittingSpareRequestsById");
        }



        public DataTable GetVerifiedSpareRequests(int unitId, int machineBrandId, DateTime fromDate)
        {
            dm.AddParameteres("@UnitId", unitId);
            dm.AddParameteres("@MachineBrandId", machineBrandId);
            dm.AddParameteres("@FromDate", fromDate);
            return dm.ExecuteQuery("usp_GetVerifiedSpareRequests_ByUnitMachineBrandIdAndDate");
        }

        public DataTable GetIssuedSpares(int unitId, int machineBrandId, DateTime fromDate)
        {
            dm.AddParameteres("@UnitId", unitId);
            dm.AddParameteres("@MachineBrandId", machineBrandId);
            dm.AddParameteres("@FromDate", fromDate);
            return dm.ExecuteQuery("usp_GetIssuedSpares_ByUnitMachineBrandIdAndDate");
        }


        public DataTable GetKnittingSpareReceives(DateTime fromDate, DateTime toDate)
        {
            dm.AddParameteres("@FromDate", fromDate);
            dm.AddParameteres("@ToDate", toDate);
            return dm.ExecuteQuery("usp_GetKnittingSpareReceivesByDates");
        }



        public DataTable GetExistingKnittingMachines(int machineBrandId, int knittingUnitId)
        {
            dm.AddParameteres("@MachineBrandId", machineBrandId);
            dm.AddParameteres("@KnittingUnitId", knittingUnitId);
            return dm.ExecuteQuery("usp_GetKnittingMachines_ByMachineBrandIdAndUnitId");
        }

        public DataTable GetExistingProductionMachines(int machineBrandId, int knittingUnitId)
        {
            dm.AddParameteres("@MachineBrandId", machineBrandId);
            dm.AddParameteres("@KnittingUnitId", knittingUnitId);
            return dm.ExecuteQuery("usp_GetUnitWiseMachines_ByMachineBrandIdAndUnitId");
        }

        public DataTable GetNeedleUpdatesHistory(int machineId)
        {
            dm.AddParameteres("@MachineId", machineId);
            return dm.ExecuteQuery("usp_GetNeedleUpdateHistory_ByMachineId");
        }


        public DataTable GetKnittingMachineSpareSuppliers()
        {
            return dm.ExecuteQuery("usp_GetKnittingSpareSuppliers");
        }

        public DataTable AuditLogList(DateTime dateTime1, DateTime dateTime2)
        {
            dm.AddParameteres("@dateTime1", dateTime1);
            dm.AddParameteres("@dateTime2", dateTime2);
            return dm.ExecuteQuery("usp_GetAuditLogInformation");
        }


        public DataTable GetKnittingMachineSpareBrands(int knittingSpareTypeId)
        {
            dm.AddParameteres("@KnittingSpareTypeId", knittingSpareTypeId);
            return dm.ExecuteQuery("usp_GetKnittingMachineSpareBrands");
        }


        public DataTable GetPartManufacturers()
        {
            return dm.ExecuteQuery("usp_GetKnittingSpareManufactures");
        }

        public DataTable GetNeedleChangeReasons()
        {
            return dm.ExecuteQuery("usp_GetNeedleChangeReasons");
        }


        public DataTable GetSpareTypeBySpareId(int spareId)
        {
            dm.AddParameteres("@SpareId", spareId);
            return dm.ExecuteQuery("usp_GetSpareType_BySpareId");
        }


        public int UpdateSpareSummaryForSpareReceive(int spareId, int? spareBrandId, int supplierId, string supplierCode, int quantity, string unitPrice, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@SpareId", spareId);
            dm.AddParameteres("@SpareBrandId", spareBrandId);
            dm.AddParameteres("@SupplierId", supplierId);
            dm.AddParameteres("@SupplierCode", supplierCode);
            dm.AddParameteres("@TransactionQuantity", quantity);
            dm.AddParameteres("@SpareUnitPrice", unitPrice);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);

            DataTable dt = dm.ExecuteQuery("usp_Update_KnittingSparesCurrentSummaryForSpareReceive");
            return Convert.ToInt32(dt.Rows[0][0]);

        }


        public int UpdateSpareSummaryForSpareIssue(int knittingSpareRequestDetailsId, int quantity, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@KnittingSpareRequestDetailsId", knittingSpareRequestDetailsId);
            dm.AddParameteres("@IssueQuantity", quantity);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);

            DataTable dt = dm.ExecuteQuery("usp_Update_KnittingSparesCurrentSummaryForSpareIssue");
            return Convert.ToInt32(dt.Rows[0][0]);

        }

        public int UpdateSpareSummaryForSpareIssueUpdate(int knittingSpareIssuedId, int previousQuantity, int newQuantity, string createdBy, DateTime createDate)
        {
            dm.AddParameteres("@KnittingSpareIssuedId", knittingSpareIssuedId);
            dm.AddParameteres("@PreviousQuantity", previousQuantity);
            dm.AddParameteres("@NewQuantity", newQuantity);
            dm.AddParameteres("@CreatedBy", createdBy);
            dm.AddParameteres("@CreateDate", createDate);

            DataTable dt = dm.ExecuteQuery("usp_Update_KnittingSparesCurrentSummaryForSpareIssueUpdate");
            return Convert.ToInt32(dt.Rows[0][0]);

        }


    }
}