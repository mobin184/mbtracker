﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using MBTracker.Code_Folder;
using MBTracker.Code_Folder.Admin_and_Setup;


namespace MBTracker.User_Controls
{
    public partial class MainMenu : System.Web.UI.UserControl
    {



        MenuTaskManager objTask = new MenuTaskManager();



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            LoadMenu();
            }

        }

        private void LoadMenu()
        {
            StringBuilder sb = new StringBuilder();
            int roleId = CommonMethods.SessionInfo.RoleId;

            //DataTable dt = objTask.GetMenuParentItems(CommonMethod.SessionInfo.RoleId);
            DataTable dt = objTask.GetTaskParentItems(roleId);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sb.AppendFormat(@" <li> <a href='" + dt.Rows[i]["URL"].ToString() + "'> <span class='fs1' aria-hidden='true' data-icon='" + dt.Rows[i]["Icon"].ToString() + "'></span>" + dt.Rows[i]["TextEng"].ToString() + " <span> &#11206;</span> </a>");
                    //DataTable dtchild = objTask.GetChild(Convert.ToInt32(dt.Rows[i]["ID"].ToString()), CommonMethod.SessionInfo.RoleId);
                    DataTable dtchild = objTask.GetTaskChildItems(Convert.ToInt32(dt.Rows[i]["ID"].ToString()), roleId);
                    if (dt.Rows.Count > 0)
                    {
                        sb.Append(" <ul>");
                        for (int j = 0; j < dtchild.Rows.Count; j++)
                        {
                            //DataTable nestedClield = objTask.GetChild(Convert.ToInt32(dtchild.Rows[j]["ID"].ToString()), CommonMethod.SessionInfo.RoleId);
                            DataTable nestedClield = objTask.GetTaskChildItems(Convert.ToInt32(dtchild.Rows[j]["ID"].ToString()), roleId);

                            if (nestedClield.Rows.Count > 0)
                            {
                                sb.AppendFormat(@"<li class='test-menu'>
                                    <a href='" + dtchild.Rows[j]["URL"].ToString() + "' class='fs1'>" + dtchild.Rows[j]["TextEng"].ToString() + "<span class='pull-right'>&#11208;</span>" + @"</a>");


                                sb.Append(" <ul class='test-SubMenu'>");
                                for (int k = 0; k < nestedClield.Rows.Count; k++)
                                {
                                    sb.AppendFormat(@"<li>
                                    <a href='" + nestedClield.Rows[k]["URL"].ToString() + "'>" +
                                                    nestedClield.Rows[k]["TextEng"].ToString() + @"</a>

                            </li>");
                                }
                                sb.Append(" </ul>");
                            }
                            else
                            {
                                sb.AppendFormat(@"<li>
                                    <a href='" + dtchild.Rows[j]["URL"].ToString() + "'> <span class='fs1' aria-hidden='true'></span>" + dtchild.Rows[j]["TextEng"].ToString() + @"</a>");


                            }

                            sb.Append(" </li>");
                        }
                        sb.Append(" </ul>");
                    }
                }
                sb.Append(" </li>");
            }
            ltrlMenu.Text = sb.ToString();
        }

    }



}