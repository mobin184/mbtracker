﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MBTracker.Code_Folder;


namespace MBTracker.User_Controls
{
    public partial class Header : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //if (CommonMethods.SessionInfo == null)
                //{
                //    Response.Redirect("~/Login.aspx"); 
                //}
                ltrlUserName.Text = CommonMethods.SessionInfo.UserFullName;

            }
        }
    }
}